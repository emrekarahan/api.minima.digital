dotnet clean src\Minima.Api.GittiGidiyor.Consumer\Minima.Api.GittiGidiyor.Consumer.csproj
dotnet restore src\Minima.Api.GittiGidiyor.Consumer\Minima.Api.GittiGidiyor.Consumer.csproj
dotnet build src\Minima.Api.GittiGidiyor.Consumer\Minima.Api.GittiGidiyor.Consumer.csproj
%windir%\system32\inetsrv\appcmd stop apppool /apppool.name:"ggconsumer.minima.int"
dotnet publish src\Minima.Api.GittiGidiyor.Consumer\Minima.Api.GittiGidiyor.Consumer.csproj -c Release -o D:\wwwroot\ggconsumer.minima.int /p:EnvironmentName=Production
%windir%\system32\inetsrv\appcmd start apppool /apppool.name:"ggconsumer.minima.int"