dotnet clean src\Minima.Api.Integrator\Minima.Api.Integrator.csproj
dotnet restore src\Minima.Api.Integrator\Minima.Api.Integrator.csproj
dotnet build src\Minima.Api.Integrator\Minima.Api.Integrator.csproj
%windir%\system32\inetsrv\appcmd stop apppool /apppool.name:"integratorapi.minima.digital"
dotnet publish src\Minima.Api.Integrator\Minima.Api.Integrator.csproj -c Release  -o D:\wwwroot\integratorapi.minima.digital /p:EnvironmentName=Production
%windir%\system32\inetsrv\appcmd start apppool /apppool.name:"integratorapi.minima.digital"