dotnet clean src\Minima.Gittigidiyor.Taskscheduler\Minima.Gittigidiyor.Taskscheduler.csproj
dotnet restore src\Minima.Gittigidiyor.Taskscheduler\Minima.Gittigidiyor.Taskscheduler.csproj
dotnet build src\Minima.Gittigidiyor.Taskscheduler\Minima.Gittigidiyor.Taskscheduler.csproj -f net472 --force --no-incremental
%windir%\system32\inetsrv\appcmd stop apppool /apppool.name:"ggtasks.minima.int"
dotnet publish src\Minima.Gittigidiyor.Taskscheduler\Minima.Gittigidiyor.Taskscheduler.csproj -c Release  -o D:\wwwroot\ggtasks.minima.int -f net472 --force  /p:EnvironmentName=Production
COPY D:\wwwroot\libs\FluentScheduler.dll D:\wwwroot\ggtasks.minima.int\
%windir%\system32\inetsrv\appcmd start apppool /apppool.name:"ggtasks.minima.int"