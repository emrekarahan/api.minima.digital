dotnet clean src\Minima.Integration.Api.Proxy.Gittigidiyor2\Minima.Integration.Api.Proxy.Gittigidiyor.csproj
dotnet restore src\Minima.Integration.Api.Proxy.Gittigidiyor2\Minima.Integration.Api.Proxy.Gittigidiyor.csproj
dotnet build src\Minima.Integration.Api.Proxy.Gittigidiyor2\Minima.Integration.Api.Proxy.Gittigidiyor.csproj -f net472 
%windir%\system32\inetsrv\appcmd stop apppool /apppool.name:"ggapi.minima.int"
dotnet publish src\Minima.Integration.Api.Proxy.Gittigidiyor2\Minima.Integration.Api.Proxy.Gittigidiyor.csproj -c Release -f net472   -o D:\wwwroot\ggapi.minima.int /p:EnvironmentName=Production
%windir%\system32\inetsrv\appcmd start apppool /apppool.name:"ggapi.minima.int"