dotnet clean src\Minima.TaskScheduler.Console\Minima.TaskScheduler.Console.csproj
dotnet restore src\Minima.TaskScheduler.Console\Minima.TaskScheduler.Console.csproj
dotnet build src\Minima.TaskScheduler.Console\Minima.TaskScheduler.Console.csproj
%windir%\system32\inetsrv\appcmd stop apppool /apppool.name:"tasks.minima.int"
dotnet publish src\Minima.TaskScheduler.Console\Minima.TaskScheduler.Console.csproj -c Release  -o D:\wwwroot\tasks.minima.int /p:EnvironmentName=Production
%windir%\system32\inetsrv\appcmd start apppool /apppool.name:"tasks.minima.int"