﻿namespace Gittigidiyor.Batch.Models
{
    public partial class GGSpec
    {
        #region Generated Properties

        public int Id { get; set; }
        public string Name { get; set; }

        public bool Required { get; set; }

        public int TypeId { get; set; }

        public int ValueId { get; set; }

        #endregion


    }
}
