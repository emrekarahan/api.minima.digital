﻿namespace Gittigidiyor.Batch.Models
{
    public class GGSpec_GGCategory_Mapping
    {
        public int Id { get; set; }
        public int GGCategoryId { get; set; }
        public int GGSpecID { get; set; }
    }
}