﻿namespace Gittigidiyor.Batch.Models
{
    public partial class GGSpecValue
    {
        public GGSpecValue()
        {
            #region Generated Constructor
            #endregion
        }

        #region Generated Properties

        public int Id { get; set; }
        public int SpecId { get; set; }

        public string Value { get; set; }

        #endregion

        

    }
}
