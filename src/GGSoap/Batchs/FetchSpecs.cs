﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ApiV2Client.GittiGidiyor;
using ApiV2Client.GittiGidiyor.Category;
using Dapper;
using Gittigidiyor.Batch.Domain.Mongo;
using Gittigidiyor.Batch.Models;
using Gittigidiyor.Batch.MongoDb.Repository;


namespace Gittigidiyor.Batch.Batchs
{
    public class FetchSpecs
    {
        private readonly CategoryService categoryService;
        private readonly string connectionString;
        private readonly IMongoRepository<CategorySpec> categorySpecRepository;
        public FetchSpecs(IMongoRepository<CategorySpec> categorySpecRepository)
        {

            connectionString =
                @"Data Source=localhost\SQLExpress;Initial Catalog=gokce;Integrated Security=False;Persist Security Info=False;User ID=sa;Password=be02062012@A";//ConfigurationManager.ConnectionStrings["NopCommerce"].ConnectionString;
            this.categorySpecRepository = categorySpecRepository;
            AuthConfig config = new AuthConfig
            {
                SecretKey = "XQyAvX8pUSz8RpeE",
                RolePass = "9TVvFz9te2uA9mheFKzp6ppkpjpdVUsv",
                RoleName = "frost_majere",
                ApiKey = "RhqsGzm8exNebfkhc5ZU3bAbj7WMffDT"
            };
            ApiV2Client.GittiGidiyor.ConfigurationManager.setAuthParameters(config);
            categoryService = ServiceProvider.getCategoryService();
        }

        public void Init()
        {
            var categoryList = GetCategories();

            foreach (var category in categoryList)
            {
                Console.WriteLine($"<<< {category.CategoryName} Start >>>");
                Console.WriteLine($"{category.CategoryName} specs fetching...");
                categorySpecsServiceResponse categorySpecsServiceResponse = categoryService.getCategorySpecs(category.CategoryCode, "tr");

                if (categorySpecsServiceResponse.specs == null)
                    continue;

                ProcessSpecType(categorySpecsServiceResponse.specs, category);
                Console.WriteLine($"<<< {category.CategoryName} End >>>");
            }
        }

        private void ProcessSpecType(categorySpecType[] specs, GGCategory category)
        {
            Console.WriteLine($"{category.CategoryName} found {specs.Length} specs.");
            var categorySpec = new CategorySpec
            {
                CategoryId = category.Id,
                CategoryCode = category.CategoryCode
            };
            foreach (categorySpecType categorySpecType in specs)
            {

                var spec = new Spec
                {
                    Name = categorySpecType.name,
                    Required = categorySpecType.required,
                    RequiredSpecified = categorySpecType.requiredSpecified,
                    Type = new SpecType(categorySpecType.type),
                    Values = categorySpecType.values.Select(s => new SpecValue
                    {
                        Value = s
                    }).ToList()
                };
                
                categorySpec.Spec.Add(spec);
            }
            categorySpecRepository.InsertOne(categorySpec);
            Console.WriteLine($"{category.CategoryName} specs added to mongodb.");
        }


        private List<GGCategory> GetCategories()
        {
            using (var conn = new SqlConnection(connectionString))
            {
                var category = conn.Query<GGCategory>("SELECT * FROM [GG_Category]").ToList();
                return category;
            }
        }
    }
}