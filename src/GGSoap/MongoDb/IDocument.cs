﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using MongoDB.Bson;

namespace Gittigidiyor.Batch.MongoDb
{
    public interface IDocument
    {
        [BsonId]
        [BsonRepresentation(BsonType.String)]
        ObjectId Id { get; set; }

        DateTime CreatedAt { get; }
    }
}