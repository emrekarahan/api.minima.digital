﻿using System;
using MongoDB.Bson;

namespace Gittigidiyor.Batch.MongoDb.Impl
{
    public abstract class Document : IDocument
    {
        public ObjectId Id { get; set; }

        public DateTime CreatedAt => Id.CreationTime;
    }
}