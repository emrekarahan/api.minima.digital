﻿using System;
using System.Configuration;
using System.IO;
using Gittigidiyor.Batch.Batchs;
using Gittigidiyor.Batch.MongoDb;
using Gittigidiyor.Batch.MongoDb.Impl;
using Gittigidiyor.Batch.MongoDb.Repository;
using Gittigidiyor.Batch.MongoDb.Repository.Impl;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace Gittigidiyor.Batch
{
    class Program
    {
        static void Main(string[] args)
        {
            var services = ConfigureServices();
            var serviceProvider = services.BuildServiceProvider();

            Console.WriteLine("Görev Seçiniz:");
            Console.WriteLine("1. Kategori Specleri Çek");

            if (Console.ReadKey().Key == ConsoleKey.D1)
            {
                serviceProvider.GetService<FetchSpecs>().Init();
            }
        }

        private static IServiceCollection ConfigureServices()
        {
            IServiceCollection services = new ServiceCollection();

            var config = LoadConfiguration();
            services.AddSingleton(config);


            services.Configure<MongoDbSettings>(config.GetSection("MongoDbSettings"));

            services.AddSingleton<IMongoDbSettings>(serviceProvider =>
                serviceProvider.GetRequiredService<IOptions<MongoDbSettings>>().Value);


            services.AddScoped(typeof(IMongoRepository<>), typeof(MongoRepository<>));
            // required to run the application
            services.AddTransient<FetchSpecs>();

            return services;
        }

        public static IConfiguration LoadConfiguration()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

            return builder.Build();
        }
    }
}
