﻿using System.Collections.Generic;
using Gittigidiyor.Batch.MongoDb.Attribute;
using Gittigidiyor.Batch.MongoDb.Impl;

namespace Gittigidiyor.Batch.Domain.Mongo
{
    [BsonCollection("CategorySpec")]
    public class CategorySpec : Document
    {
        public CategorySpec()
        {
            Spec = new List<Spec>();
        }
        public int CategoryId { get; set; }
        public List<Spec> Spec { get; set; }
        public string CategoryCode { get; set; }
    }

    public class Spec
    {
        public string Name { get; set; }
        public bool Required { get; set; }
        public bool RequiredSpecified { get; set; }
        public SpecType Type { get; set; }
        public List<SpecValue> Values { get; set; }
    }

    public class SpecValue
    {
        public string Value { get; set; }
    }

    public class SpecType
    {
        public SpecType(string name)
        {
            Name = name;
        }

        public string Name { get; set; }
    }
}