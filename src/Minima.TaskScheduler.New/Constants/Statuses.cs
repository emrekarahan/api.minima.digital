﻿namespace Minima.TaskScheduler.Console.Constants
{
    public enum Statuses
    {
        Stopped = 1,
        Running = 2
    }
}