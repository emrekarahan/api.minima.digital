﻿namespace Minima.TaskScheduler.Console.Constants
{
    public enum TaskType
    {
        UpdateProduct = 1,
        UpdateCategory = 2
    }
}