using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Minima.TaskScheduler.Console.Models.Request;
using Padawan.Attributes;
using RestSharp;

namespace Minima.TaskScheduler.Console.Proxies.IntegrationApi
{

    [Singleton]
    public class IntegrationApiClient
    {

        private readonly IntegrationApiConfiguration _configuration;
        private readonly ILogger<IntegrationApiClient> _logger;

        public IntegrationApiClient(IntegrationApiConfiguration configuration, ILogger<IntegrationApiClient> logger)
        {
            _configuration = configuration;
            _logger = logger;
        }

        public async Task UpdateProduct(ProductRequestModel productMessageModel)
        {
            try
            {
                var client = new RestClient($"{_configuration.Url}/api/Integration/new-products");
                var request = new RestRequest(Method.POST);
                request.AddJsonBody(productMessageModel);
                var response = await client.ExecuteAsync(request);
                if (response.IsSuccessful)
                {
                    
                }
            }
            catch (Exception e)
            {
                _logger.LogError(new EventId(666), "Response is not successful", new
                {
                    IsSuccess = false,
                    Message = "failed",
                    Task = "UpdateProduct"
                }, "Product");
            }
        }

        public async Task UpdateCategory(CategoryRequestModel requestModel)
        {
            try
            {
                var client = new RestClient($"{_configuration.Url}/api/Integration/new-categories");
                var request = new RestRequest(Method.POST);
                request.AddJsonBody(requestModel);
                var response = await client.ExecuteAsync(request);
                if (response.IsSuccessful)
                {

                }
            }
            catch (Exception e)
            {
                _logger.LogError(new EventId(666), "Response is not successful", new
                {
                    IsSuccess = false,
                    Message = "failed",
                    Task = "UpdateCategory"
                }, "Category");
            }
        }

        public async Task GetOrderList(string startDate, string endDate)
        {
            var url = $"{_configuration.Url}/api/Integration/UpdateGittigidiyorStock";
            _logger.LogInformation($"{url} executing...");
            var client = new RestClient(url);
            var request = new RestRequest(Method.GET);
            request.AddQueryParameter("startDate", startDate);
            request.AddQueryParameter("endDate", endDate);
            request.AddQueryParameter("status", "approved");
            request.AddQueryParameter("paymentStatus", "success");
            var response = await client.ExecuteAsync(request);
            _logger.LogInformation($"UpdateGittigidiyorStock {response.StatusCode}");
        }

        public async Task UpdateProductStock(ProductRequestModel productMessageModel)
        {
            try
            {
                var client = new RestClient($"{_configuration.Url}/api/Integration/product-stock");
                var request = new RestRequest(Method.POST);
                request.AddJsonBody(productMessageModel);
                var response = await client.ExecuteAsync(request);
                if (response.IsSuccessful)
                {

                }
            }
            catch (Exception e)
            {
                _logger.LogError(new EventId(666), "Response is not successful", new
                {
                    IsSuccess = false,
                    Message = "failed",
                    Task = "UpdateProductStock"
                }, "ProductStock");
            }
        }
    }
}