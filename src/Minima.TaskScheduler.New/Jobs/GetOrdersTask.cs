﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;
//using MassTransit;
//using Microsoft.Extensions.Logging;
//using Minima.TaskScheduler.Console.Proxies.IntegrationApi;
//using Padawan.Abstractions;
//using Padawan.Attributes;
//using RestSharp;

//namespace Minima.TaskScheduler.Console.Jobs
//{
//    public class GetOrdersTask : ISchedulerJob
//    {
//        private IntegrationApiClient _integrationApiClient;
//        ILogger<GetOrdersTask> _logger;


//        public GetOrdersTask(IntegrationApiClient integrationApiClient, ILogger<GetOrdersTask> logger)
//        {
//            _integrationApiClient = integrationApiClient;
//            _logger = logger;
//        }

//        [RunNow]
//        [RunEvery(2, Period.Minute)]
//        public async Task  GetOrderList()
//        {
//            try
//            {
//                var startDate = DateTime.Now.ToString("yyyy-MM-dd");
//                var endDate = DateTime.Now.ToString("yyyy-MM-dd");
//                _logger.LogInformation($"GetOrderList started {startDate} - {endDate}");
//                await _integrationApiClient.GetOrderList(startDate, endDate);
//            }
//            catch (Exception e)
//            {
//                _logger.LogError(new EventId(500), e, e.Message);
//            }
 
//        }
//    }
//}
