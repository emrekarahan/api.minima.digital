﻿using Dapper;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Products.Requests;
using Falcon.Package.MarketPlace.Gittigidiyor.Services;
using Minima.Data.MongoDb.Repository;
using Minima.TaskScheduler.Console.Domain;
using Minima.TaskScheduler.New.Domain;
using Padawan.Abstractions;
using Padawan.Attributes;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Minima.TaskScheduler.New.Jobs
{
    public class NewJob : ISchedulerJob
    {
        private readonly IMongoRepository<Product> _mongoRepository;
        readonly IProductApiService _productApiService;

        public NewJob(
            IMongoRepository<Product> mongoRepository,
            IProductApiService productApiService)
        {
            _mongoRepository = mongoRepository;
            _productApiService = productApiService;
        }

        //[RunNow]
        //[RunEvery(60, Period.Minute)]
        public void ClearOldProductRecords()
        {
            var ggProducts = _productApiService.GetProducts(new GetProductsRequestReturn
            {
                Lang = "tr",
                RowCount = 1,
                StartOffset = 1,
                Status = "A",
                WithData = true
            });
            var pageCount = Math.Ceiling(ggProducts.ProductCount / 100d);



            for (int i = 0; i < pageCount; i++)
            {
                var productList = _productApiService.GetProducts(new GetProductsRequestReturn
                {
                    Lang = "tr",
                    RowCount = 100,
                    StartOffset = i * 100,
                    Status = "A",
                    WithData = true
                });

                if (productList.AckCode != "success")
                    continue;

                foreach (var product in productList.Products)
                {
                    var mongoProduct = _mongoRepository.AsQueryable().FirstOrDefault(f => f.ProductId == int.Parse(product.ItemId));

                    if (mongoProduct == null)
                    {
                        var result1 = _productApiService.DeleteProduct(new DeleteProductRequestReturn
                        {
                            ItemIdList = new string[] { product.ItemId },
                            Lang = "tr",
                            ProductIdList = new int?[] { }
                        });
                    }
                    else
                    {
                        if (mongoProduct.StockQuantity == 0)
                        {
                            var result2 = _productApiService.DeleteProduct(new DeleteProductRequestReturn
                            {
                                ItemIdList = new string[] { product.ItemId },
                                Lang = "tr",
                                ProductIdList = new int?[] { }
                            });
                        }
                    }





                    //var result = productService.calculatePriceForShoppingCart(new List<int> { product.productId }, new List<string>(), "tr");
                }

            }
        }

        [RunNow]
        [RunEvery(60, Period.Minute)]
        public void UpdateMongo()
        {
            using (var connection = new SqlConnection("Data Source=localhost;Initial Catalog=gokce_live;Integrated Security=False;Persist Security Info=False;User ID=sa;Password=be02062012@A"))
            {
                var nopList = connection.Query<NopProduct>("SELECT Id, StockQuantity FROM Product").ToList();

                foreach (var product in nopList)
                {
                    var mongoProduct = _mongoRepository.AsQueryable().FirstOrDefault(f => f.ProductId == product.Id);

                    if (mongoProduct != null)
                    {
                        if (mongoProduct.StockQuantity != product.StockQuantity)
                        {
                            mongoProduct.StockQuantity = product.StockQuantity;
                            _mongoRepository.ReplaceOne(mongoProduct);
                        }
                   
                    }
             





                    //var result = productService.calculatePriceForShoppingCart(new List<int> { product.productId }, new List<string>(), "tr");
                }
            }



     
        }
    }
}
