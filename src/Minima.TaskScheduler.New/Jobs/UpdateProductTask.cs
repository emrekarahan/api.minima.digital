﻿//using System;
//using System.Threading.Tasks;
//using Microsoft.Extensions.Logging;
//using Minima.Data.MongoDb.Repository;
//using Minima.TaskScheduler.Console.Domain;
//using Minima.TaskScheduler.Console.Models.Request;
//using Minima.TaskScheduler.Console.Proxies.IntegrationApi;
//using Padawan.Abstractions;
//using Padawan.Attributes;

//namespace Minima.TaskScheduler.Console.jobs
//{
    
//    public class UpdateProductTask : ISchedulerJob
//    {
        
//        private readonly ILogger<UpdateProductTask> _logger;
//        private readonly IntegrationApiClient _integrationApiClient;
//        private readonly IMongoRepository<DailyUpdatedProducts> _dailyUpdatedProductsRepository;

//        public UpdateProductTask(
//            ILogger<UpdateProductTask> logger,
//            IntegrationApiClient integrationApiClient, 
//            IMongoRepository<DailyUpdatedProducts> dailyUpdatedProductsRepository)
//        {
//            _logger = logger;
//            _integrationApiClient = integrationApiClient;
//            _dailyUpdatedProductsRepository = dailyUpdatedProductsRepository;
//        }

//        [RunNow]
//        [RunEvery(3, Period.Minute)]
//        public async Task UpdateProducts()
//        {
//            try
//            {
//                _logger.LogInformation($"Update Product Job is Started : {DateTime.Now}");
//                var lastUpdate = DateTime.Now;
//                var requestModel = new ProductRequestModel
//                {
//                    StartUpdatedAt = lastUpdate
//                };
//                await _integrationApiClient.UpdateProduct(requestModel);
//            }
//            catch (Exception e)
//            {
//                _logger.LogError(e, e.Message);
//            }
//        }


//        [RunNow]
//        [RunEvery(60, Period.Minute)]
//        public async Task ClearOldProductRecords()
//        {
//            try
//            {
//                _logger.LogInformation($"Clear Old Product Records Job  is Started : {DateTime.Now}");
//                var deleteDate = DateTime.Now.AddDays(-1);
//                await _dailyUpdatedProductsRepository.DeleteManyAsync(p => p.UpdatedAt <= deleteDate);
//            }
//            catch (Exception e)
//            {
//                _logger.LogError(e, e.Message);
//            }
//        }
//    }
//}