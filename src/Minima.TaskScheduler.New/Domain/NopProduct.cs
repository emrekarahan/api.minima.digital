﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Minima.TaskScheduler.New.Domain
{
    public class NopProduct
    {
        public int Id { get; set; }
        public int StockQuantity { get; set; }
    }
}
