﻿using System;

namespace Minima.TaskScheduler.Console.Models.Request
{
    public class ProductRequestModel
    {
        public ProductRequestModel()
        {
            Page = 1;
            Limit = 100;
            Sort = "id";
        }
        public string Sort { get; set; }
        public int Limit { get; set; }
        public int Page { get; set; }
        public int? SinceId { get; set; }
        public string Ids { get; set; }
        public string Parent { get; set; }
        public int Brand { get; set; }
        public string Sku { get; set; }
        public string Name { get; set; }
        public string Distributor { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? StartUpdatedAt { get; set; }
        public DateTime? EndUpdatedAt { get; set; }


    }
}