﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Padawan.Extensions;

namespace Minima.TaskScheduler.Console
{
    class Program
    {
        public static void Main(string[] args)
        {            
            CreateWebHostBuilder(args).Build().Run();
        }


        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .UsePadawan<Startup>("ggtasks.minima.int");
                
    }
}


    

