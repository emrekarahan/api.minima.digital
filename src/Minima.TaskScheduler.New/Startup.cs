﻿using Falcon.Package.MarketPlace.Gittigidiyor;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Minima.Core.Ioc.Impl;
using Minima.Data.MongoDb;
using Minima.Data.MongoDb.Impl;
using Minima.Data.MongoDb.Repository;
using Minima.Data.MongoDb.Repository.Impl;
using Minima.Logging.Extensions;

namespace Minima.TaskScheduler.Console
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            // required to run the application
            services.AddMinimaLogging(Configuration.GetSection("Logging"));

            services.Configure<MongoDbSettings>(Configuration.GetSection("MongoDbSettings"));
            services.AddSingleton<IMongoDbSettings>(serviceProvider =>
                serviceProvider.GetRequiredService<IOptions<MongoDbSettings>>().Value);
            services.AddSingleton(typeof(IMongoRepository<>), typeof(MongoRepository<>));

            services.AddGittigidiyor(new GittigidiyorSettings { 
                ApiKey = "DpGQhA9jKqnXcA4VEhU4z6VR6gEDvQ7D",
                ApiSecret= "eTfk9TeYWAj6kkNs",
                ApiUrl = "https://dev.gittigidiyor.com:8443/listingapi/ws",
                RoleName = "gokcekoleksiyon",
                RolePassword = "xZzDUYDv5aF7G9kpy5ZwTbCBaFRqhtrH"


            });

            ServiceLocator.Instance = services.BuildServiceProvider();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseMinimaLogging();
            var logger = app.ApplicationServices.GetService<ILogger<Startup>>();
            logger.LogInformation($"'Minima.TaskScheduler.Console' app running on '{env.EnvironmentName}' mode");

            app.UseMvc();
        }

    }
}