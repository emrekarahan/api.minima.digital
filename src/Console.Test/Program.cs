﻿using RestSharp;
using System;
using System.Security.Cryptography;
using System.Text;
using System.Threading;

namespace Console.Test
{
    class Program
    {
        static void Main(string[] args)
        {
            var apiKey = "DpGQhA9jKqnXcA4VEhU4z6VR6gEDvQ7D";
            var secret = "eTfk9TeYWAj6kkNs";
            //var time = cevirTimeSpan(DateTime.Now);
            //var sign = EncryptWithMd5(string.Concat(apiKey,   secret, time));
            var unixTimestamp = (long)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            
            //DateTime dt = new DateTime() + epoch;
            var time = unixTimestamp;//(long)(DateTime.UtcNow - unixTimestamp).TotalMilliseconds;
            var sign = EncryptWithMd5(apiKey + secret + time);
            
         

            //webRequest.ContentLength = bytes.Length;
            //webRequest.Headers.Add("SOAP:Action");
            //webRequest.ContentType = "text/xml;charset=\"utf-8\"";
            //webRequest.Accept = "text/xml";
            //webRequest.Method = "POST";
            var sb = new StringBuilder();
            sb.AppendLine("<prod:getProducts>")
.AppendLine($"<apiKey>{apiKey}</apiKey>")
.AppendLine($"<sign>{sign}</sign>")
.AppendLine($"<time>{time}</time>")
.AppendLine("<startOffSet>0</startOffSet>")
.AppendLine("<rowCount>5</rowCount>")
.AppendLine("<status>L</status>")
.AppendLine("<withData>true</withData>")
.AppendLine("<lang>tr</lang>")
.AppendLine("</prod:getProducts>");



            try
            {
                var body = sb.ToString();
                IRestClient client = new RestClient("https://dev.gittigidiyor.com:8443/listingapi/ws/IndividualProductService");
                client.AddDefaultHeader("SOAP", "Action");
                IRestRequest request = new RestRequest(Method.POST);
                request.AddParameter("text/xml", body, "text/xml", ParameterType.RequestBody);
                var response = client.Execute(request);
            }
            catch (Exception e)
            {
                System.Console.WriteLine(e.Message);
                throw;
            }




            //IndividualProductService service = new IndividualProductServiceClient();
            //try
            //{
            //    var data = service.getProducts(new getProductsRequest(apiKey, sign, time, 0, 100, "A", true, "tr"));
            //    System.Console.WriteLine("aa");
            //}
            //catch (Exception e)
            //{
            //    System.Console.WriteLine(e.Message);

            //    throw;
            //}

        }


        private static string EncryptWithMd5(string clearString)
        {
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(clearString);
            byte[] hashBytes = md5.ComputeHash(inputBytes);

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hashBytes.Length; i++)
            {
                sb.Append(hashBytes[i].ToString("X2"));
            }
            return sb.ToString();
        }

        public static long cevirTimeSpan(DateTime value)
        {
            TimeSpan span = DateTime.UtcNow - new DateTime(0x7b2, 1, 1);
            return (span.Ticks / 0x2710L);
        }
    }
}
