﻿namespace Minima.Api.GittiGidiyor.Consumer.Models.Product
{
    public class SpecType
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public string Type { get; set; }
        public bool Required { get; set; }
        public bool RequiredSpecified { get; set; }
    }
}