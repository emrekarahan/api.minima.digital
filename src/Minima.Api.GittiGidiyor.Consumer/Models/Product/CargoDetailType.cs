﻿namespace Minima.Api.GittiGidiyor.Consumer.Models.Product
{
    public class CargoDetailType
    {
        public string City { get; set; }
        public string[] CargoCompanies { get; set; }
        public string ShippingPayment { get; set; }
        public string CargoDescription { get; set; }
        public string ShippingWhere { get; set; }
        public CargoCompanyDetailType[] CargoCompanyDetails { get; set; }
        public CargoTimeType ShippingTime { get; set; }
    }
}