﻿namespace Minima.Api.GittiGidiyor.Consumer.Models.Product
{
    public class CargoTimeType
    {
        public string Days { get; set; }

        public string BeforeTime { get; set; }
    }
}