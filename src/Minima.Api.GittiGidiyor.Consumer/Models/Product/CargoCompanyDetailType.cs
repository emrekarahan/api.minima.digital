﻿namespace Minima.Api.GittiGidiyor.Consumer.Models.Product
{
    public class CargoCompanyDetailType
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public string CityPrice { get; set; }
        public string CountryPrice { get; set; }
    }
}