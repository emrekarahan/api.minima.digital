﻿namespace Minima.Api.GittiGidiyor.Consumer.Models.Product
{
    public class VariantGroupType
    {
        public VariantType[] Variants { get; set; }
        public PhotoType[] Photos { get; set; }
        public long NameId { get; set; }
        public bool NameIdSpecified { get; set; }
        public long ValueId { get; set; }
        public bool ValueIdSpecified { get; set; }
        public string Alias { get; set; }
    }
}