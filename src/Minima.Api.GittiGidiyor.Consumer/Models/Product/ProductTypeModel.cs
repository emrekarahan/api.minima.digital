﻿namespace Minima.Api.GittiGidiyor.Consumer.Models.Product
{
    public class ProductTypeModel
    {
        public int ProductId { get; set; }
        public string Sku { get; set; }
        public string CategoryCode { get; set; }
        public int StoreCategoryId { get; set; }
        public bool StoreCategoryIdSpecified { get; set; }
        public string Title { get; set; }
        public string Subtitle { get; set; }
        public SpecType[] Specs { get; set; }
        public PhotoType[] Photos { get; set; }
        public int PageTemplate { get; set; }
        public bool PageTemplateSpecified { get; set; }
        public string Description { get; set; }
        public string StartDate { get; set; }
        public int CatalogId { get; set; }
        public bool CatalogIdSpecified { get; set; }
        public int NewCatalogId { get; set; }
        public bool NewCatalogIdSpecified { get; set; }
        public int CatalogDetail { get; set; }
        public bool CatalogDetailSpecified { get; set; }
        public string CatalogFilter { get; set; }
        public string Format { get; set; }
        public double StartPrice { get; set; }
        public bool StartPriceSpecified { get; set; }
        public double BuyNowPrice { get; set; }
        public bool BuyNowPriceSpecified { get; set; }
        public double NetEarning { get; set; }
        public bool NetEarningSpecified { get; set; }
        public int ListingDays { get; set; }
        public bool ListingDaysSpecified { get; set; }
        public int ProductCount { get; set; }
        public bool ProductCountSpecified { get; set; }
        public CargoDetailType CargoDetail { get; set; }
        public bool AffiliateOption { get; set; }
        public bool AffiliateOptionSpecified { get; set; }
        public bool BoldOption { get; set; }
        public bool BoldOptionSpecified { get; set; }
        public bool CatalogOption { get; set; }
        public bool CatalogOptionSpecified { get; set; }
        public bool VitrineOption { get; set; }
        public bool VitrineOptionSpecified { get; set; }
        public VariantGroupType[] VariantGroups { get; set; }
        public int AuctionProfilePercentage { get; set; }
        public bool AuctionProfilePercentageSpecified { get; set; }
        public double MarketPrice { get; set; }
        public bool MarketPriceSpecified { get; set; }
        public string GlobalTradeItemNo { get; set; }
        public string ManufacturerPartNo { get; set; }
    }
}