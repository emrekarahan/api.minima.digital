﻿namespace Minima.Api.GittiGidiyor.Consumer.Models.Product
{
    public class VariantSpecType
    {
        public long NameId { get; set; }
        public bool NameIdSpecified { get; set; }
        public string Name { get; set; }
        public long ValueId { get; set; }
        public bool ValueIdSpecified { get; set; }
        public string Value { get; set; }
        public int OrderNumber { get; set; }
        public bool OrderNumberSpecified { get; set; }
        public int SpecDataOrderNumber { get; set; }
        public bool SpecDataOrderNumberSpecified { get; set; }
    }
}