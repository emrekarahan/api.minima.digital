﻿namespace Minima.Api.GittiGidiyor.Consumer.Models.Product
{
    public class VariantType
    {
        public VariantSpecType[] VariantSpecs { get; set; }
        public int Quantity { get; set; }
        public bool QuantitySpecified { get; set; }
        public string StockCode { get; set; }
        public int SoldCount { get; set; }
        public bool SoldCountSpecified { get; set; }
        public int NewCatalogId { get; set; }
        public bool NewCatalogIdSpecified { get; set; }
        public long VariantId { get; set; }
        public bool VariantIdSpecified { get; set; }
        public string Operation { get; set; }
    }
}