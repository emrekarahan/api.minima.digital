﻿namespace Minima.Api.GittiGidiyor.Consumer.Models.Product
{
    public class PhotoType
    {
        public string Url { get; set; }
        public string Base64 { get; set; }
        public int PhotoId { get; set; }
        public bool PhotoIdSpecified { get; set; }
    }
}