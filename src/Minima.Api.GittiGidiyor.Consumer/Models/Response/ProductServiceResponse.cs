﻿using Newtonsoft.Json;

namespace Minima.Api.GittiGidiyor.Consumer.Models.Response
{
    public class ProductServiceResponse : BaseResponse
    {
        [JsonProperty("productid")]
        public int ProductId;

        [JsonProperty("result")]
        public string Result;
    }
}
