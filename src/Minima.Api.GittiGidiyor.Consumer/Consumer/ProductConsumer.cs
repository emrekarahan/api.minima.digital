﻿using System;
using System.Threading.Tasks;
using MassTransit;
using Microsoft.Extensions.Logging;
using Minima.Api.GittiGidiyor.Consumer.Proxies.IntegrationApi;
using Padawan.RabbitMq.Attributes;
using Minima.Logging.Extensions;

namespace Minima.Api.GittiGidiyor.Consumer.Consumer
{
    [Consumer(QueueName = "gg_product_updated", ExchangeName = "gg_product_updated", PrefetchCount = 1)]
    public class ProductConsumer : IConsumer<Integration.MessageModel>
    {
        private readonly IntegrationApiClient integrationApiClient;
        private readonly ILogger<ProductConsumer> _logger;

        public ProductConsumer(IntegrationApiClient integrationApiClient, ILogger<ProductConsumer> logger)
        {
            this.integrationApiClient = integrationApiClient;
            _logger = logger;
        }

        public async Task Consume(ConsumeContext<Integration.MessageModel> context)
        {
            try
            {
                await integrationApiClient.InsertProduct(context.Message.Payload.ProductId);
            }
            catch (Exception e)
            {
               _logger.LogError(e, e.Message, context, "Gittigidiyor");
            }
     
        }
    }
}
