﻿namespace Minima.Api.GittiGidiyor.Consumer.MessageModel
{
    public class ProductOutModel
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
    }
}