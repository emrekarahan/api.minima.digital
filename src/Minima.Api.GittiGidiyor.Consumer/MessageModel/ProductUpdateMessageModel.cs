﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Minima.Api.GittiGidiyor.Consumer.Models.Product;

// ReSharper disable once CheckNamespace
namespace Minima.Integration
{
    public class MessageModel
    {
        public DateTime DateTime { get; set; }
        public Payload Payload { get; set; }
        public string Type { get; set; }
    }

    public class Payload
    {
        public int ProductId { get; set; }
    }
}
