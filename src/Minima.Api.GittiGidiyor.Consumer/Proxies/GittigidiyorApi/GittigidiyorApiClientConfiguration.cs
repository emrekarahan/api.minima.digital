﻿using Padawan.Attributes;

namespace Minima.Api.GittiGidiyor.Consumer.Proxies.GittigidiyorApi
{
    [Configuration("GittigidiyorService")]
    public class GittigidiyorApiClientConfiguration
    {
        public string Url { get; set; }
    }
}