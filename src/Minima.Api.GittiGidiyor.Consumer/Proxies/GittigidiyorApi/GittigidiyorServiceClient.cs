﻿namespace Minima.Api.GittiGidiyor.Consumer.Proxies.GittigidiyorApi
{
    public class GittigidiyorServiceClient
    {
        private GittigidiyorApiClientConfiguration _apiClientConfiguration;

        public GittigidiyorServiceClient(GittigidiyorApiClientConfiguration apiClientConfiguration)
        {
            _apiClientConfiguration = apiClientConfiguration;
        }
    }
}