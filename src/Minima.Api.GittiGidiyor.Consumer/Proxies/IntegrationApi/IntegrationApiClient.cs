using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Minima.Api.GittiGidiyor.Consumer.MessageModel;
using Padawan.Attributes;
using RestSharp;

namespace Minima.Api.GittiGidiyor.Consumer.Proxies.IntegrationApi
{

    [Singleton]
    public class IntegrationApiClient
    {

        private readonly IntegrationApiConfiguration _configuration;
        private readonly ILogger<IntegrationApiClient> _logger;

        public IntegrationApiClient(IntegrationApiConfiguration configuration, ILogger<IntegrationApiClient> logger)
        {
            _configuration = configuration;
            _logger = logger;
        }

        public async Task InsertProduct(int productId)
        {
            try
            {
                var client = new RestClient($"{_configuration.Url}/api/Gittigidiyor/InsertProduct");

                var request = new RestRequest(Method.GET);
                request.AddQueryParameter("productId", productId.ToString());
                var response = await client.ExecuteAsync(request);


                if (!response.IsSuccessful)
                {
                    _logger.LogError(new EventId(666), "Response is not successful", new
                    {
                        IsSuccess = false,
                        Message = response.StatusDescription,
                        Task = "UpdatePicture"
                    }, "Product");
                    throw new Exception();
                }
            }
            catch (Exception e)
            {
                _logger.LogError(new EventId(666), "Response is not successful", new
                {
                    IsSuccess = false,
                    Message = "failed",
                    Task = "UpdateProduct"
                }, "Product");
                throw;
            }

            
        }
    }
}