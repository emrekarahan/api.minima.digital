﻿using Padawan.Attributes;

namespace Minima.Api.GittiGidiyor.Consumer.Proxies.IntegrationApi
{

    [Configuration("IntegrationService")]
    public class IntegrationApiConfiguration
    {
        public string Url { get; set; }
    }
}
