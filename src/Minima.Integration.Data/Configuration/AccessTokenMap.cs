using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Minima.Integration.Data.Domain;

namespace Minima.Integration.Data.Configuration
{
    public class AccessTokenMap : IEntityTypeConfiguration<AccessToken>
    {
        public void Configure(EntityTypeBuilder<AccessToken> builder)
        {
            #region Generated Configure
            // table
            builder.ToTable("AccessToken", "dbo");

            // key
            builder.HasKey(t => t.Id);

            // properties
            builder.Property(t => t.Id)
                .IsRequired()
                .HasColumnName("Id")
                .HasColumnType("int")
                .ValueGeneratedOnAdd();

            builder.Property(t => t.AccessTokenMember)
                .IsRequired()
                .HasColumnName("AccessToken")
                .HasColumnType("nvarchar(500)")
                .HasMaxLength(500);

            builder.Property(t => t.RefreshToken)
                .IsRequired()
                .HasColumnName("RefreshToken")
                .HasColumnType("nvarchar(500)")
                .HasMaxLength(500);

            builder.Property(t => t.ExpireDate)
                .IsRequired()
                .HasColumnName("ExpireDate")
                .HasColumnType("datetime2");

            // relationships
            #endregion
        }
    }
}
