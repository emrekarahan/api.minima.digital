using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Minima.Integration.Data.Domain;

namespace Minima.Integration.Data.Configuration
{
    public class JobLogMap
        : IEntityTypeConfiguration<JobLog>
    {
        public void Configure(EntityTypeBuilder<JobLog> builder)
        {
            #region Generated Configure
            // table
            builder.ToTable("JobLog", "dbo");

            // key
            builder.HasKey(t => t.Id);

            // properties
            builder.Property(t => t.Id)
                .IsRequired()
                .HasColumnName("Id")
                .HasColumnType("int")
                .ValueGeneratedOnAdd();

            builder.Property(t => t.JobId)
                .IsRequired()
                .HasColumnName("JobId")
                .HasColumnType("int");

            builder.Property(t => t.StartTime)
                .IsRequired()
                .HasColumnName("StartTime")
                .HasColumnType("datetime2");

            builder.Property(t => t.StopTime)
                .IsRequired()
                .HasColumnName("StopTime")
                .HasColumnType("datetime2");

            builder.Property(t => t.IsSuccess)
                .IsRequired()
                .HasColumnName("IsSuccess")
                .HasColumnType("bit");

            builder.Property(t => t.ErrorMessage)
                .HasColumnName("ErrorMessage")
                .HasColumnType("nvarchar(max)");

            builder.Property(t => t.CreatedDate)
                .IsRequired()
                .HasColumnName("CreatedDate")
                .HasColumnType("datetime2")
                .HasDefaultValueSql("(getdate())");

            // relationships
            builder.HasOne(t => t.Job)
                .WithMany(t => t.JobLogs)
                .HasForeignKey(d => d.JobId)
                .HasConstraintName("FK_JobLog_Job_JobId");

            #endregion
        }
    }
}
