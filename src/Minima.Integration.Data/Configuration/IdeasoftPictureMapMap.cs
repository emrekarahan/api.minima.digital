using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Minima.Integration.Data.Domain;

namespace Minima.Integration.Data.Configuration
{
    public class IdeasoftPictureMapMap
        : IEntityTypeConfiguration<IdeasoftPictureMap>
    {
        public void Configure(EntityTypeBuilder<IdeasoftPictureMap> builder)
        {
            #region Generated Configure
            // table
            builder.ToTable("IdeasoftPictureMap", "dbo");

            // key
            builder.HasKey(t => t.Id);

            // properties
            builder.Property(t => t.Id)
                .IsRequired()
                .HasColumnName("Id")
                .HasColumnType("int")
                .ValueGeneratedOnAdd();

            builder.Property(t => t.IdeaPictureId)
                .IsRequired()
                .HasColumnName("IdeaPictureId")
                .HasColumnType("int");

            builder.Property(t => t.InternalPictureId)
                .IsRequired()
                .HasColumnName("InternalPictureId")
                .HasColumnType("int");

            builder.Property(t => t.CreatedDateUtc)
                .HasColumnName("CreatedDateUtc")
                .HasColumnType("datetime2");

            builder.Property(t => t.UpdatedDateUtc)
                .HasColumnName("UpdatedDateUtc")
                .HasColumnType("datetime2");

            builder.Property(t => t.LastUpdatedDateUtc)
                .HasColumnName("LastUpdatedDateUtc")
                .HasColumnType("datetime2");

            // relationships
            #endregion
        }
    }
}
