using System;

namespace Minima.Integration.Data.Domain
{
    public partial class IdeasoftProductMap
    {
        public IdeasoftProductMap()
        {
            #region Generated Constructor
            #endregion
        }

        #region Generated Properties
        public int Id { get; set; }

        public int IdeasoftProductId { get; set; }

        public int InternalProductId { get; set; }

        public DateTime? CreatedDateUtc { get; set; }

        public DateTime? UpdatedDateUtc { get; set; }

        public DateTime? LastUpdatedDateUtc { get; set; }

        #endregion

        #region Generated Relationships
        #endregion

    }
}
