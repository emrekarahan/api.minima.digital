using System;

namespace Minima.Integration.Data.Domain
{
    public class AccessToken
    {
        public AccessToken()
        {
            #region Generated Constructor
            #endregion
        }

        #region Generated Properties
        public int Id { get; set; }

        public string AccessTokenMember { get; set; }

        public string RefreshToken { get; set; }

        public DateTime ExpireDate { get; set; }

        #endregion
    }
}
