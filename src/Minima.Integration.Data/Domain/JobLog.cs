using System;

namespace Minima.Integration.Data.Domain
{
    public partial class JobLog
    {
        public JobLog()
        {
            #region Generated Constructor
            #endregion
        }

        #region Generated Properties
        public int Id { get; set; }

        public int JobId { get; set; }

        public DateTime StartTime { get; set; }

        public DateTime StopTime { get; set; }

        public bool IsSuccess { get; set; }

        public string ErrorMessage { get; set; }

        public DateTime CreatedDate { get; set; }

        #endregion

        #region Generated Relationships
        public virtual Job Job { get; set; }

        #endregion

    }
}
