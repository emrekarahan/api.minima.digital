using System;
using System.Collections.Generic;

namespace Minima.Integration.Data.Domain
{
    public partial class Job
    {
        public Job()
        {
            #region Generated Constructor
            JobLogs = new HashSet<JobLog>();
            #endregion
        }

        #region Generated Properties
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime? LastStartDate { get; set; }
        public DateTime? LastEndDate { get; set; }
        public bool IsActive { get; set; }

        #endregion

        #region Generated Relationships
        public virtual ICollection<JobLog> JobLogs { get; set; }

        #endregion

    }
}
