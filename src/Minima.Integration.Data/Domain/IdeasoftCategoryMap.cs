using System;

namespace Minima.Integration.Data.Domain
{
    public class IdeasoftCategoryMap
    {
        public IdeasoftCategoryMap()
        {
            #region Generated Constructor
            #endregion
        }

        #region Generated Properties
        public int Id { get; set; }

        public int IdeaCategoryId { get; set; }

        public int InternalCategoryId { get; set; }

        public DateTime? CreatedDateUtc { get; set; }

        public DateTime? UpdatedDateUtc { get; set; }

        public DateTime? LastUpdatedDateUtc { get; set; }

        #endregion

        #region Generated Relationships
        #endregion

    }
}
