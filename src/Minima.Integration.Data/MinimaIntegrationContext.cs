using Microsoft.EntityFrameworkCore;
using Minima.Integration.Data.Domain;

namespace Minima.Integration.Data
{
    public class IntegrationContext : DbContext
    {
        public IntegrationContext(DbContextOptions<IntegrationContext> options)
            : base(options)
        {
        }

        #region Generated Properties
        public virtual DbSet<IdeasoftCategoryMap> IdeasoftCategoryMaps { get; set; }
        public virtual DbSet<IdeasoftProductMap> IdeasoftProductMaps { get; set; }
        public virtual DbSet<IdeasoftPictureMap> IdeasoftPictureMap { get; set; }
        public virtual DbSet<AccessToken> AccessToken { get; set; }
        public virtual DbSet<Job> Job { get; set; }
        public virtual DbSet<JobLog> JobLog { get; set; }

        #endregion

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(IntegrationContext).Assembly);
        }
    }
}
