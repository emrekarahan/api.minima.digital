﻿namespace Minima.Api.Integrator.Models.Product
{
    public class ProductOutModel
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
    }
}