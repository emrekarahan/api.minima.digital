using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Minima.Api.Integrator.Models.Product;
using Minima.Api.Integrator.Models.Response;
using Minima.Core.MessageModels;
using Padawan.Attributes;
using RestSharp;
using Minima.Logging.Extensions;

namespace Minima.Api.Integrator.Proxies.IntegrationApi
{

    [Singleton]
    public class IntegrationApiClient
    {

        private readonly IntegrationApiConfiguration _configuration;
        private readonly ILogger<IntegrationApiClient> _logger;

        public IntegrationApiClient(IntegrationApiConfiguration configuration, ILogger<IntegrationApiClient> logger)
        {
            _configuration = configuration;
            _logger = logger;
        }

        public async Task<Response> UpdateProduct(ProductMessageModel productMessageModel)
        {
            IRestResponse<Response> response = null;
            try
            {
                var client = new RestClient($"{_configuration.Url}/api/Product/UpdateProduct");
                var request = new RestRequest(Method.POST);
                request.AddJsonBody(productMessageModel);
                 response = await client.ExecuteAsync<Response>(request);

                if (response.IsSuccessful && response.Data.Code == 200)
                {
                    return response.Data;
                }

                _logger.LogError(new EventId(500), new Exception(), "Response is not successful1", new
                {
                    ProductId = productMessageModel.Id,
                    IsSuccess = false,
                    Message = response.StatusDescription,
                    Statuscode = response.StatusCode,
                    Task = "UpdateProduct_Response",
                    Model  = productMessageModel
                }, "Product");

                return new Response
                {
                    Message = "Response is not successful",
                    Code = 500
                };

            }
            catch (Exception e)
            {
                _logger.LogError(new EventId(500), e,e.Message, new
                {
                    ProductId = productMessageModel.Id,
                    IsSuccess = false,
                    Message = e.Message,
                    Statuscode = response.StatusCode,
                    Task = "UpdateProduct_Error",
                    Model = productMessageModel
                }, "Product");

                return new Response
                {
                    Message = e.Message,
                    InternalMessage = e.InnerException?.Message,
                    Code = 500
                };
            }
            
        }

        public async Task<ProductOutModel> UpdateCategories(List<CategoryMessageModel> categoryMessageModel)
        {
            try
            {
                var client = new RestClient($"{_configuration.Url}/api/Category/UpdateCategory");

                var request = new RestRequest(Method.POST);

                request.AddJsonBody(categoryMessageModel);

                var response = await client.ExecuteAsync<ProductOutModel>(request);

                if (response.IsSuccessful)
                {
                    return response.Data;
                }

                _logger.LogError(new EventId(666), "Response is not successful", new
                {
                    IsSuccess = false,
                    Message = response.StatusDescription,
                    Task = "UpdateCategory"
                }, "Product");

                return new ProductOutModel
                {
                    IsSuccess = false,
                    Message = response.StatusDescription,
                };
            }
            catch (Exception e)
            {
                _logger.LogError(new EventId(666), "Response is not successful", new
                {
                    IsSuccess = false,
                    Message = "failed",
                    Task = "UpdateCategory"
                }, "Product");
            }
            return new ProductOutModel
            {
                IsSuccess = false,
            };
        }

        public async Task<Response> UpdateProductStock(ProductMessageModel productMessageModel)
        {
            IRestResponse<Response> response = null;

            try
            {
                var client = new RestClient($"{_configuration.Url}/api/Product/UpdateProductStock");
                var request = new RestRequest(Method.POST);
                request.AddJsonBody(productMessageModel);
                response = await client.ExecuteAsync<Response>(request);

                if (response.IsSuccessful && response.Data.Code == 200)
                {
                    return response.Data;
                }

                _logger.LogError(new EventId(500), new Exception(), "Response is not successful UpdateProductStock1", new
                {
                    ProductId = productMessageModel.Id,
                    IsSuccess = false,
                    Message = response.StatusDescription,
                    Task = "UpdateProductStock"                    
                }, "Product");

                return new Response
                {
                    Message = "Response is not successful UpdateProductStock2",
                    Code = 500
                };

            }
            catch (Exception e)
            {
                _logger.LogError(new EventId(500), e, e.Message, new
                {
                    ProductId = productMessageModel.Id,
                    IsSuccess = false,
                    Message = e.Message,
                    Task = "UpdateProductStock"                    

                }, "Product");

                return new Response
                {
                    Message = e.Message,
                    InternalMessage = e.InnerException?.Message,
                    Code = 500
                };
            }
        }
    }
}