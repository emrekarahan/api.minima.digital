﻿using Padawan.Attributes;

namespace Minima.Api.Integrator.Proxies.IntegrationApi
{

    [Configuration("IntegrationService")]
    public class IntegrationApiConfiguration
    {
        public string Url { get; set; }
    }
}
