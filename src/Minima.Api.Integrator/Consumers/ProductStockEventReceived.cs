﻿using System;
using System.Threading.Tasks;
using MassTransit;
using Microsoft.Extensions.Logging;
using Minima.Api.Integrator.Proxies.IntegrationApi;
using Minima.Core.Contants;
using Minima.Core.MessageModels;
using Padawan.RabbitMq.Attributes;

namespace Minima.Api.Integrator.Consumers
{
    [Consumer(QueueName = "integration_product_save_stock", ExchangeName = "integration_product_save_stock", PrefetchCount = 25)]
    public class ProductStockEventReceived : IConsumer<ProductMessageModel>
    {
        private readonly ILogger<ProductEventReceived> _logger;
        private readonly IntegrationApiClient _integrationApiClient;

        public ProductStockEventReceived(
            ILogger<ProductEventReceived> logger,
            IntegrationApiClient integrationApiClient)
        {
            _logger = logger;
            _integrationApiClient = integrationApiClient;
        }


        public async Task Consume(ConsumeContext<ProductMessageModel> context)
        {
            try
            {
                /*      var respone = */
                await _integrationApiClient.UpdateProductStock(context.Message);
                //if (respone.Code != 200)
                //{
                //    throw new Exception("Product services response is failed!");
                //}
            }
            catch (Exception e)
            {
                _logger.LogCritical(e, $"Critical error on order service. Retry count is {context.GetRetryCount()}.");
                throw;
            }
        }
    }
}