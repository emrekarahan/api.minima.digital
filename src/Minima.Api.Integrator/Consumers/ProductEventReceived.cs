﻿using System;
using System.Threading.Tasks;
using MassTransit;
using Microsoft.Extensions.Logging;
using Minima.Api.Integrator.Proxies.IntegrationApi;
using Minima.Core.Contants;
using Minima.Core.MessageModels;
using Padawan.RabbitMq.Attributes;

namespace Minima.Api.Integrator.Consumers
{
    [Consumer(QueueName = "integration_product_save", ExchangeName = "integration_product_save", PrefetchCount = 2)]
    public class ProductEventReceived : IConsumer<ProductMessageModel>
    {
        private readonly ILogger<ProductEventReceived> _logger;
        private readonly IntegrationApiClient _integrationApiClient;

        public ProductEventReceived(
            ILogger<ProductEventReceived> logger,
            IntegrationApiClient integrationApiClient)
        {
            _logger = logger;
            _integrationApiClient = integrationApiClient;
        }


        public async Task Consume(ConsumeContext<ProductMessageModel> context)
        {
            try
            {
                var respone = await _integrationApiClient.UpdateProduct(context.Message);
                if (respone.Code != 200)
                {
                    throw new Exception("Product services response is failed!");
                }
            }
            catch (Exception e)
            {
                _logger.LogCritical(e, $"Critical error on order service. Retry count is {context.GetRetryCount()}.");
                throw;
            }
        }
    }
}