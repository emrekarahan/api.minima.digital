﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MassTransit;
using Microsoft.Extensions.Logging;
using Minima.Api.Integrator.Proxies.IntegrationApi;
using Minima.Core.MessageModels;
using Padawan.RabbitMq.Attributes;

namespace Minima.Api.Integrator.Consumers
{


    [Consumer(QueueName = "integration_category_save", ExchangeName = "integration_category_save", PrefetchCount = 1)]
    public class CategoryEventReceived : IConsumer<CategoryMessageModel>
    {
        private readonly ILogger<CategoryEventReceived> _logger;
        private readonly IntegrationApiClient _integrationApiClient;

        public CategoryEventReceived(
            ILogger<CategoryEventReceived> logger, IntegrationApiClient integrationApiClient)
        {
            _logger = logger;
            _integrationApiClient = integrationApiClient;
        }


        public async Task Consume(ConsumeContext<CategoryMessageModel> context)
        {
            try
            {
                var category = GetParetCategories(context.Message);
                await _integrationApiClient.UpdateCategories(category);
            }
            catch (Exception e)
            {
                _logger.LogCritical(e, $"Critical error on order service. Retry count is {context.GetRetryCount()}.");
            }
        }

        private List<CategoryMessageModel> GetParetCategories(CategoryMessageModel categoryInModel)
        {
            List<CategoryMessageModel> result = new List<CategoryMessageModel>();
            var dd = categoryInModel;

            var depth = categoryInModel.Depth;
            while (dd != null)
            {
                dd = dd.Parent;
                if (dd != null)
                {
                    result.Add(dd);
                }
                //return dd;
            }
            result.Add(categoryInModel);
            return result.OrderBy(o => o.Depth).ToList();
        }

    }
}