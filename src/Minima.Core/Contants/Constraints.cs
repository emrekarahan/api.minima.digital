﻿namespace Minima.Core.Contants
{
    public class ApiUrl
    {
        private static string BASE_URL = "https://gokcekoleksiyon.myideasoft.com/api/";
        // ReSharper disable once InconsistentNaming
        public static string CATEGORY = $"{BASE_URL}categories";
        public static string PRODUCT = $"{BASE_URL}products";
        public static string ORDER = $"{BASE_URL}orders";
    }

}