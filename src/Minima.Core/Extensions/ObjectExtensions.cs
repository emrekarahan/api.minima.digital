﻿using System;

namespace Minima.Core.Extensions
{
    public static class ObjectExtensions
    {
        public static bool IsInteger(this object value)
        {
            if (value == null)
            {
                throw new ArgumentNullException(nameof(value));
            }
            return int.TryParse(value.ToString(), out _);
        }

    }
}