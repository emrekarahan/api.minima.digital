﻿using System;
using RestSharp;

namespace Minima.Core.Extensions
{
    public static class RestsharpExtensions
    {
        public static int ToTotalPage(this Parameter parameter, int pageSize)
        {
            if (parameter == null)
                throw new ArgumentNullException(nameof(parameter));

            if (!parameter.Value.IsInteger())
                throw new DivideByZeroException(nameof(parameter.Value));

            return (int.Parse(parameter.Value.ToString()) / pageSize);
        }
    }
}