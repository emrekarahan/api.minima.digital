﻿using System;

namespace Minima.Core.MessageModels
{
    public class CategoryMessageModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Slug { get; set; }
        public int SortOrder { get; set; }
        public int Status { get; set; }
        public string DistributorCode { get; set; }
        public double Percent { get; set; }
        public string ImageFile { get; set; }
        public object Distributor { get; set; }
        public int DisplayShowcaseContent { get; set; }
        public string ShowcaseContent { get; set; }
        public int ShowcaseContentDisplayType { get; set; }
        public int DisplayShowcaseFooterContent { get; set; }
        public object ShowcaseFooterContent { get; set; }
        public int ShowcaseFooterContentDisplayType { get; set; }
        public int HasChildren { get; set; }
        public string MetaKeywords { get; set; }
        public string MetaDescription { get; set; }
        public string PageTitle { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }

        public CategoryMessageModel Parent { get; set; }


        public int Depth => 1 + (Parent?.Depth ?? 0);

    }
}