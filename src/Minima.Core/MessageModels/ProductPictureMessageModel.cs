﻿using System.Collections.Generic;

namespace Minima.Core.MessageModels
{
    public class ProductPictureMessageModel
    {
        public int ProductId { get; set; }
        public List<ProductPicture> ProductPictures { get; set; }
    }

    public class ProductPicture
    {
        public int Id { get; set; }
        public string Filename { get; set; }
        public string Extension { get; set; }
        public string DirectoryName { get; set; }
        public string Revision { get; set; }
        public int SortOrder { get; set; }
    }
}