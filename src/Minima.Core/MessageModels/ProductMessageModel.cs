﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Minima.Core.MessageModels
{
    public class Currency
    {

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("label")]
        public string Label { get; set; }

        [JsonProperty("abbr")]
        public string Abbr { get; set; }
    }

    public class Image
    {

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("filename")]
        public string Filename { get; set; }

        [JsonProperty("extension")]
        public string Extension { get; set; }

        [JsonProperty("directoryName")]
        public string DirectoryName { get; set; }

        [JsonProperty("revision")]
        public string Revision { get; set; }

        [JsonProperty("sortOrder")]
        public int SortOrder { get; set; }
    }

    public class Detail
    {

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("details")]
        public string Details { get; set; }

        [JsonProperty("extraDetails")]
        public string ExtraDetails { get; set; }
    }

    public class Category
    {

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("distributorCode")]
        public string DistributorCode { get; set; }
    }

    public class ProductToCategory
    {

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("sortOrder")]
        public object SortOrder { get; set; }

        [JsonProperty("category")]
        public Category Category { get; set; }
    }

    public class ProductMessageModel
    {

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("slug")]
        public string Slug { get; set; }

        [JsonProperty("fullName")]
        public string FullName { get; set; }

        [JsonProperty("sku")]
        public string Sku { get; set; }

        [JsonProperty("barcode")]
        public string Barcode { get; set; }

        [JsonProperty("price1")]
        public double Price1 { get; set; }

        [JsonProperty("warranty")]
        public int Warranty { get; set; }

        [JsonProperty("tax")]
        public int Tax { get; set; }

        [JsonProperty("stockAmount")]
        public double StockAmount { get; set; }

        [JsonProperty("volumetricWeight")]
        public double VolumetricWeight { get; set; }

        [JsonProperty("buyingPrice")]
        public double BuyingPrice { get; set; }

        [JsonProperty("stockTypeLabel")]
        public string StockTypeLabel { get; set; }

        [JsonProperty("discount")]
        public double Discount { get; set; }

        [JsonProperty("discountType")]
        public int DiscountType { get; set; }

        [JsonProperty("moneyOrderDiscount")]
        public double MoneyOrderDiscount { get; set; }

        [JsonProperty("status")]
        public int Status { get; set; }

        [JsonProperty("taxIncluded")]
        public int TaxIncluded { get; set; }

        [JsonProperty("distributor")]
        public string Distributor { get; set; }

        [JsonProperty("isGifted")]
        public int IsGifted { get; set; }

        [JsonProperty("gift")]
        public string Gift { get; set; }

        [JsonProperty("customShippingDisabled")]
        public int CustomShippingDisabled { get; set; }

        [JsonProperty("customShippingCost")]
        public double CustomShippingCost { get; set; }

        [JsonProperty("marketPriceDetail")]
        public string MarketPriceDetail { get; set; }

        [JsonProperty("createdAt")]
        public DateTime CreatedAt { get; set; }

        [JsonProperty("updatedAt")]
        public DateTime UpdatedAt { get; set; }

        [JsonProperty("metaKeywords")]
        public string MetaKeywords { get; set; }

        [JsonProperty("metaDescription")]
        public string MetaDescription { get; set; }

        [JsonProperty("pageTitle")]
        public string PageTitle { get; set; }

        [JsonProperty("hasOption")]
        public int HasOption { get; set; }

        [JsonProperty("shortDetails")]
        public string ShortDetails { get; set; }

        [JsonProperty("searchKeywords")]
        public string SearchKeywords { get; set; }

        [JsonProperty("installmentThreshold")]
        public string InstallmentThreshold { get; set; }

        [JsonProperty("homeSortOrder")]
        public object HomeSortOrder { get; set; }

        [JsonProperty("popularSortOrder")]
        public object PopularSortOrder { get; set; }

        [JsonProperty("brandSortOrder")]
        public object BrandSortOrder { get; set; }

        [JsonProperty("featuredSortOrder")]
        public object FeaturedSortOrder { get; set; }

        [JsonProperty("campaignedSortOrder")]
        public object CampaignedSortOrder { get; set; }

        [JsonProperty("newSortOrder")]
        public object NewSortOrder { get; set; }

        [JsonProperty("discountedSortOrder")]
        public object DiscountedSortOrder { get; set; }

        [JsonProperty("brand")]
        public object Brand { get; set; }

        [JsonProperty("currency")]
        public Currency Currency { get; set; }

        [JsonProperty("parent")]
        public object Parent { get; set; }

        [JsonProperty("countdown")]
        public object Countdown { get; set; }

        [JsonProperty("prices")]
        public IList<object> Prices { get; set; }

        [JsonProperty("images")]
        public IList<Image> Images { get; set; }

        [JsonProperty("details")]
        public IList<Detail> Details { get; set; }

        [JsonProperty("productToCategories")]
        public IList<ProductToCategory> ProductToCategories { get; set; }
    }


}