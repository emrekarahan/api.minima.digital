﻿using Newtonsoft.Json;

namespace Minima.Core
{
    public class CurrentToken
    {
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }

        [JsonProperty("expires_in")]
        public string ExpireIn { get; set; }

        [JsonProperty("token_type")]
        public string TokenType { get; set; }
        
        [JsonProperty("scope")]
        public string Scope { get; set; }

        [JsonProperty("refresh_token")]
        public string RefreshToken { get; set; }

    }
}