﻿using System;

namespace Minima.Core.Models.Request
{
    public class CategoryRequestModel
    {
        public CategoryRequestModel()
        {
            Page = 1;
            Limit = 100;
        }
        public int Page { get; set; }
        public int Limit { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? StartUpdatedAt { get; set; }
        public DateTime? EndUpdatedAt { get; set; }
        public int? SinceId { get; set; }
    }
}