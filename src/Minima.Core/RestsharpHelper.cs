﻿using System.Collections.Generic;
using System.Threading.Tasks;
using RestSharp;

namespace Minima.Core
{

    public class RestsharpHelper
    {
        public static async Task<IRestResponse> GetAsync(string url, List<Parameter> parameters = null)
        {
            var client = new RestClient(url);

            if (parameters != null)
            {
                foreach (var parameter in parameters)
                    client.AddDefaultParameter(parameter);
            }

            client.AddDefaultHeader("Authorization", $"Bearer {CurrentTokenStatic.AccessToken}");
            var request = new RestRequest(Method.GET);
            var response = await client.ExecuteAsync(request);
            return response;
        }

        public static async Task<IRestResponse<T>> GetAsync<T>(string url)
        {
            var client = new RestClient(url);
            client.AddDefaultHeader("Authorization", $"Bearer {CurrentTokenStatic.AccessToken}");
            var request = new RestRequest(Method.GET);
            var response = await client.ExecuteAsync<T>(request);
            return response;
        }
    }
}