﻿
using System;

namespace Minima.Core.Ioc.Impl
{
    public static class ServiceLocator
    {
        public static IServiceProvider Instance { get; set; }
    }
}