﻿using Microsoft.Extensions.DependencyInjection;

namespace Minima.Core.Ioc.Impl
{
    public class Resolver
    {
        public static T Resolve<T>() where T : class
        {
            return ServiceLocator.Instance.GetService<T>();
        }
    }
}