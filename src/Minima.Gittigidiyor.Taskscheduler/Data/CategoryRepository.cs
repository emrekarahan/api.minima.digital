﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Dapper;
using Dapper.Contrib.Extensions;
using Microsoft.Extensions.Logging;
using Minima.Gittigidiyor.Taskscheduler.Configuration;
using Minima.Gittigidiyor.Taskscheduler.Domain;
using Padawan.Attributes;

namespace Minima.Gittigidiyor.Taskscheduler.Data
{
    [Singleton]
    public class CategoryRepository
    {
        private readonly AppConfiguration _appConfiguration;

        public CategoryRepository(AppConfiguration appConfiguration)
        {
            _appConfiguration = appConfiguration;
        }

        public int UpsertCategory(Category category)
        {
            var sqlStatement = new StringBuilder()
                .AppendLine("BEGIN ")
                .AppendLine("       IF NOT EXISTS(SELECT * FROM Category WHERE CategoryCode = @CategoryCode) ")
                .AppendLine("   BEGIN ")
                .AppendLine("       INSERT INTO Category(CategoryCode, CategoryName, HasCatalog, Deepest, Depth) ")
                .AppendLine("       VALUES(@CategoryCode, @CategoryName, @HasCatalog, @Deepest, @Depth) ")
                .AppendLine("   END ")
                .AppendLine("END").ToString();

            using (var conn = new SqlConnection(_appConfiguration.GittigidiyorDatabase.ConnectionString))
            {
                return conn.Execute(sqlStatement, new
                {
                    CategoryCode = category.CategoryCode,
                    CategoryName = category.CategoryName,
                    HasCatalog = category.HasCatalog,
                    Deepest = category.Deepest,
                    Depth = category.Depth
                });
            }
        }


        public List<Category> GetAll()
        {

            using (var conn = new SqlConnection(_appConfiguration.GittigidiyorDatabase.ConnectionString))
            {
                return conn.GetAll<Category>().ToList();
            }
        }

        public Category GetCategoryByCategoryCode(string code)
        {
            using (var conn = new SqlConnection(_appConfiguration.GittigidiyorDatabase.ConnectionString))
            {
                var category = conn.QueryFirstOrDefault<Category>("SELECT * FROM [Category] WHERE CategoryCode = @code", new { code });
                return category;
            }

        }
    }
}