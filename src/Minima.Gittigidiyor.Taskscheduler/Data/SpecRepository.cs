﻿using System.Data.SqlClient;
using System.Text;
using Dapper;
using Minima.Gittigidiyor.Taskscheduler.Configuration;
using Minima.Gittigidiyor.Taskscheduler.Domain;
using Padawan.Attributes;

namespace Minima.Gittigidiyor.Taskscheduler.Data
{
    [Singleton]
    public class SpecRepository
    {
        private AppConfiguration _appConfiguration;

        public SpecRepository(AppConfiguration appConfiguration)
        {
            _appConfiguration = appConfiguration;
        }

        public int UpsertSpec(Spec spec)
        {
            var sqlStatement = new StringBuilder()
                .AppendLine("BEGIN ")
                .AppendLine("       IF EXISTS(SELECT * FROM Spec WHERE CategoryId = @CategoryId AND Name = @Name) ")
                .AppendLine("       SELECT Id FROM Spec WHERE CategoryId = @CategoryId AND Name = @Name")
                .AppendLine("ELSE")
                .AppendLine("   BEGIN ")
                .AppendLine("       INSERT INTO Spec(CategoryId, Name, Required, Type) ")
                .AppendLine("       VALUES(@CategoryId, @Name, @Required, @Type) ")
                .AppendLine("       SELECT SCOPE_IDENTITY() AS Id ")
                .AppendLine("   END ")
                .AppendLine("END").ToString();

            using (var conn = new SqlConnection(_appConfiguration.GittigidiyorDatabase.ConnectionString))
            {
                var result = conn.QueryFirstOrDefault<int>(sqlStatement, new
                {
                    CategoryId = spec.CategoryId,
                    Name = spec.Name,
                    Required = spec.Required,
                    Type = spec.Type
                });

                return result;
            }
        }

        public void UpsertSpecValue(SpecValue specValue)
        {
            var sqlStatement = new StringBuilder()
                .AppendLine("BEGIN ")
                .AppendLine("       IF NOT EXISTS(SELECT * FROM SpecValue WHERE SpecId = @SpecId AND Value  = @Value) ")
                .AppendLine("   BEGIN ")
                .AppendLine("       INSERT INTO SpecValue (SpecId, Value) ")
                .AppendLine("       VALUES(@SpecId, @Value) ")
                .AppendLine("   END ")
                .AppendLine("END").ToString();

            using (var conn = new SqlConnection(_appConfiguration.GittigidiyorDatabase.ConnectionString))
            {
                conn.Execute(sqlStatement, new
                {
                    SpecId = specValue.SpecId,
                    Value = specValue.Value
                });
            }
        }
    }
}