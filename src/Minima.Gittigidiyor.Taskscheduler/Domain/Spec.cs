﻿using Dapper.Contrib.Extensions;

namespace Minima.Gittigidiyor.Taskscheduler.Domain
{
    [Table("Spec")]
    public  class Spec
    {
        [Key]
        public int Id { get; set; }
        public int CategoryId { get; set; }
        public string Name { get; set; }
        public bool Required { get; set; }
        public string Type { get; set; }
        

        


    }
}
