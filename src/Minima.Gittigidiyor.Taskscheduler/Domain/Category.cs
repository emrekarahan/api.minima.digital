﻿using Dapper.Contrib.Extensions;

namespace Minima.Gittigidiyor.Taskscheduler.Domain
{
    [Table("Category")]
    public class Category
    {
        [Key]
        public int Id { get; set; }
        public int ParentId { get; set; }
        public string CategoryCode { get; set; }
        public string CategoryName { get; set; }
        public bool HasCatalog { get; set; }
        public bool Deepest { get; set; }
        public int Depth { get; set; }
    }
}