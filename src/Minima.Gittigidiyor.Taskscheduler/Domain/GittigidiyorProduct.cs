﻿using Minima.Data.MongoDb.Attribute;
using Minima.Data.MongoDb.Impl;

namespace Minima.Gittigidiyor.Taskscheduler.Domain
{
    [BsonCollection("GittigidiyorProducts")]
    public class GittigidiyorProduct : Document
    {
        public int ProductId { get; set; }
        public string ItemId { get; set; }
        public int StockQuantity { get; set; }
        public string Status { get; set; }
    }

}