﻿using Dapper.Contrib.Extensions;

namespace Minima.Gittigidiyor.Taskscheduler.Domain
{
    [Table("SpecValue")]
    public  class SpecValue
    {
        [Key]
        public int Id { get; set; }
        public int SpecId { get; set; }
        public string Value { get; set; }
    }
}
