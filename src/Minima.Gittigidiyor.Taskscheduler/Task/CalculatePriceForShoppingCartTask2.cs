﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiV2Client.GittiGidiyor;
using Microsoft.Extensions.Logging;
using Minima.Data.MongoDb.Repository;
using Minima.Gittigidiyor.Taskscheduler.Configuration;
using Minima.Gittigidiyor.Taskscheduler.Domain;
using Padawan.Abstractions;
using Padawan.Attributes;
using RestSharp;

namespace Minima.Gittigidiyor.Taskscheduler.Task
{
    public class CalculatePriceForShoppingCartTask2 : ISchedulerJob
    {
        private ILogger<CalculatePriceForShoppingCartTask2> _logger;
        private IntegrationApiConfiguration _configuration;

        public CalculatePriceForShoppingCartTask2(ILogger<CalculatePriceForShoppingCartTask2> logger, IntegrationApiConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
        }


        //[RunNow]
        //[RunEvery(30, Period.Second)]
        public void FetchCategories()
        {
            var productService = ServiceProvider.getProductService();

            var products = productService.getProducts(0, 100, "L", true, "tr");

            if (products == null || products.products.Length == 0)
                return;



            foreach (var product in products.products)
            {
                if (product.product.productCount <= 0)
                    continue;
                
                var result = productService.calculatePriceForShoppingCart(new List<int> { product.productId }, new List<string>(), "tr");
                if (result.ackCode == "success")
                {
                    UpdateProductStatus(new List<int>{ product.productId }, "A").GetAwaiter().GetResult();
                }
                else
                {
                    Console.WriteLine(result.error.message);
                }
            }
        }


        private async Task<List<int>> GetProductList()
        {
            try
            {
                var client = new RestClient($"{_configuration.Url}/api/Gittigidiyor/GetListingReadyProducts");
                var request = new RestRequest(Method.POST);
                var response = await client.ExecuteAsync<List<int>>(request);
                return response.Data;
            }
            catch
            {
                // ignored
            }

            return null;
        }

        private async System.Threading.Tasks.Task UpdateProductStatus(List<int> products, string s)
        {
            try
            {
                var client = new RestClient($"{_configuration.Url}/api/Gittigidiyor/UpdateProductStatus");
                var request = new RestRequest(Method.POST);
                request.AddJsonBody(products);
                request.AddParameter("status", s, ParameterType.HttpHeader);
                var response = await client.ExecuteAsync(request);
            }
            catch
            {
                // ignored
            }
        }
    }
}