﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.ExceptionServices;
using System.Threading.Tasks;
using ApiV2Client.GittiGidiyor;
using Minima.Data.MongoDb.Repository;
using Minima.Gittigidiyor.Taskscheduler.Domain;
using Padawan.Abstractions;
using Padawan.Attributes;

namespace Minima.Gittigidiyor.Taskscheduler.Task
{
    public class FixGittigidiyorProducts : ISchedulerJob
    {
        private IMongoRepository<Product> _productRepository;
        private IMongoRepository<GittigidiyorProduct> _gittigidiyorProductRepository;

        public FixGittigidiyorProducts(IMongoRepository<Product> productRepository, IMongoRepository<GittigidiyorProduct> gittigidiyorProductRepository)
        {
            _productRepository = productRepository;
            _gittigidiyorProductRepository = gittigidiyorProductRepository;
        }

        //[RunNow]
        //[RunEveryDayAt(4, 0, 0)]
        public void FixProducts()
        {
            var productService = ServiceProvider.getProductService();



            try
            {
                var gittigidiyorProducts = _gittigidiyorProductRepository.AsQueryable().ToList();
                foreach (GittigidiyorProduct gittigidiyorProduct in gittigidiyorProducts)
                {
                    var product = _productRepository.FilterBy(f => f.ProductId == int.Parse(gittigidiyorProduct.ItemId)).ToList();

                    var firstProduct = product.FirstOrDefault();

                    if (firstProduct == null)
                        continue;

                    if (gittigidiyorProduct.ProductId != firstProduct.GittigidiyorProductId)
                    {
                        firstProduct.GittigidiyorProductId = gittigidiyorProduct.ProductId;
                    }

                    if (gittigidiyorProduct.StockQuantity != firstProduct.StockQuantity)
                    {
                        productService.updateStock(gittigidiyorProduct.ProductId.ToString(), "", firstProduct.StockQuantity,
                            true, "tr");

                        productService.updateStock(gittigidiyorProduct.ProductId.ToString(), "", firstProduct.StockQuantity,
                            true, "tr");


                        if (firstProduct.StockQuantity == 0)
                        {
                            firstProduct.Status = "U";
                        }
                    }

                    _productRepository.ReplaceOne(firstProduct);
                    _gittigidiyorProductRepository.DeleteOne(f=> f.ProductId == gittigidiyorProduct.ProductId);

                    var removedList = _gittigidiyorProductRepository.AsQueryable().Where(w => w.ItemId == firstProduct.ProductId.ToString() && w.ProductId != firstProduct.GittigidiyorProductId).ToList();

                    if (removedList != null && removedList.Count > 0)
                    {
                        var intList = removedList.Select(s => s.ProductId).ToList();
                        var response = productService.finishEarly(intList, new List<string>(), "tr");

                        foreach (var item in intList)
                        {
                            //var prd = _productRepository.FilterBy(f => f.GittigidiyorProductId == item);
                            _productRepository.DeleteOne(f=> f.GittigidiyorProductId == item);

                        }
                    }
                      

                    
                    
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

        }
    }
}
