﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiV2Client.GittiGidiyor;
using ApiV2Client.GittiGidiyor.Activity;
using Microsoft.Extensions.Logging;
using Minima.Data.MongoDb.Repository;
using Minima.Gittigidiyor.Taskscheduler.Configuration;
using Minima.Gittigidiyor.Taskscheduler.Domain;
using Padawan.Abstractions;
using Padawan.Attributes;
using RestSharp;

namespace Minima.Gittigidiyor.Taskscheduler.Task
{
    public class CalculatePriceForShoppingCartTask : ISchedulerJob
    {
        private ILogger<CalculatePriceForShoppingCartTask> _logger;
        private IntegrationApiConfiguration _configuration;

        public CalculatePriceForShoppingCartTask(ILogger<CalculatePriceForShoppingCartTask> logger, IntegrationApiConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
        }


        //[RunNow]
        //[RunEvery(10, Period.Minute)]
        //public void FetchCategories()
        //{

        //    var products = GetProductList().GetAwaiter().GetResult();

        //    if (products == null || products.Count == 0)
        //        return;

        //    var productService = ServiceProvider.getProductService();

        //    foreach (var product in products)
        //    {
        //        var result = productService.calculatePriceForShoppingCart(new List<int> { product }, new List<string>(), "tr");
        //        if (result.ackCode == "success")
        //        {
        //            UpdateProductStatus(products, "A").GetAwaiter().GetResult();
        //        }
        //        if (result.error.errorId == "466")
        //        {
        //            UpdateProductStatus(products, "A").GetAwaiter().GetResult();
        //        }
        //    }
        //}


        //private async Task<List<int>> GetProductList()
        //{
        //    try
        //    {
        //        var client = new RestClient($"{_configuration.Url}/api/Gittigidiyor/GetListingReadyProducts");
        //        var request = new RestRequest(Method.POST);
        //        var response = await client.ExecuteAsync<List<int>>(request);
        //        return response.Data;
        //    }
        //    catch
        //    {
        //        // ignored
        //    }

        //    return null;
        //}

        //private async System.Threading.Tasks.Task UpdateProductStatus(List<int> products, string s)
        //{
        //    try
        //    {
        //        var client = new RestClient($"{_configuration.Url}/api/Gittigidiyor/UpdateProductStatus");
        //        var request = new RestRequest(Method.POST);
        //        request.AddJsonBody(products);
        //        request.AddParameter("status", s, ParameterType.HttpHeader);
        //        var response = await client.ExecuteAsync(request);
        //    }
        //    catch
        //    {
        //        // ignored
        //    }
        //}
    }
}