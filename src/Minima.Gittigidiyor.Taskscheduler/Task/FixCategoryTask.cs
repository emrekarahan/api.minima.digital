﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Dapper;
using Microsoft.Extensions.Logging;
using Minima.Gittigidiyor.Taskscheduler.Configuration;
using Minima.Gittigidiyor.Taskscheduler.Data;
using Minima.Gittigidiyor.Taskscheduler.Domain;
using Padawan.Abstractions;
using Padawan.Attributes;

namespace Minima.Gittigidiyor.Taskscheduler.Task
{
    public class FixCategoryTask : ISchedulerJob
    {
        private readonly AppConfiguration _appConfiguration;
        private readonly CategoryRepository _categoryRepository;
        private readonly ILogger<FetchCategoryTask> _logger;

        public FixCategoryTask(
            AppConfiguration appConfiguration,
            CategoryRepository categoryRepository,
            ILogger<FetchCategoryTask> logger)
        {
            _appConfiguration = appConfiguration;
            _categoryRepository = categoryRepository;
            _logger = logger;
        }

        [RunEveryDayAt(2, 0, 0)]
        public void FixCategory()
        {
            var result = _categoryRepository.GetAll();
            FixCategory(result);
        }

        private void FixCategory(List<Category> categories)
        {
            try
            {
                foreach (var item in categories)
                {
                    if (item.Depth == 1)
                        continue;

                    var itemParentCategory = item.CategoryCode.Substring(0, item.CategoryCode.Length - 1);
                    var parentCategory = _categoryRepository.GetCategoryByCategoryCode(itemParentCategory);
                    if (parentCategory != null)
                    {
                        UpdateParentCategory(parentCategory.Id, item.Id);
                    }
                }
            }
            catch (Exception e)
            {
                _logger.LogError(new EventId(500), e, e.Message); ;
            }
        }



        private void UpdateParentCategory(in int parentCategoryId, in int itemId)
        {
            try
            {
                using (var conn = new SqlConnection(_appConfiguration.GittigidiyorDatabase.ConnectionString))
                {
                    conn.Execute("UPDATE [Category] SET ParentId = @ParentId WHERE Id = @Id",
                        new
                        {
                            ParentId = parentCategoryId,
                            Id = itemId
                        });
                }
            }
            catch (Exception e)
            {
                _logger.LogError(new EventId(500), e, e.Message); ;
            }

        }
    }
}