﻿using System;
using ApiV2Client.GittiGidiyor;
using ApiV2Client.GittiGidiyor.Category;
using Microsoft.Extensions.Logging;
using Minima.Gittigidiyor.Taskscheduler.Configuration;
using Minima.Gittigidiyor.Taskscheduler.Data;
using Minima.Gittigidiyor.Taskscheduler.Domain;
using Padawan.Abstractions;
using Padawan.Attributes;
using Minima.Logging.Extensions;

namespace Minima.Gittigidiyor.Taskscheduler.Task
{
    public class FetchCategoryTask : ISchedulerJob
    {
        private readonly AppConfiguration _appConfiguration;
        private readonly ICategoryService _categoryService;
        private readonly CategoryRepository _categoryRepository;
        private readonly ILogger<FetchCategoryTask> _logger;
        private readonly SpecRepository _specRepository;
        
        public FetchCategoryTask(
            AppConfiguration appConfiguration, 
            CategoryRepository categoryRepository, ILogger<FetchCategoryTask> logger, SpecRepository specRepository)
        {
            _appConfiguration = appConfiguration;
            _categoryRepository = categoryRepository;
            _logger = logger;
            _specRepository = specRepository;

            AuthConfig config = new AuthConfig()
            {
                ApiKey = _appConfiguration.GittigidiyorSettings.ApiKey,
                RoleName = _appConfiguration.GittigidiyorSettings.RoleName,
                RolePass = _appConfiguration.GittigidiyorSettings.RolePass,
                SecretKey = _appConfiguration.GittigidiyorSettings.SecretKey
            };
            ConfigurationManager.setAuthParameters(config);

            _categoryService = ServiceProvider.getCategoryService();
        }

        //[RunNow]
        //[RunEveryDayAt(0,0,0)]
        public void FetchCategories()
        {
            _logger.LogInformation("FetchCategories task started");

            var pageSize = 100d;
            var rowCount = Convert.ToInt32(pageSize);
            var categoryCountResponse = _categoryService.getCategories(0, rowCount, true, true, true,
                _appConfiguration.GittigidiyorSettings.Lang);
            var categoryCount = categoryCountResponse.categoryCount;
            var pageCount = Math.Ceiling(categoryCount / pageSize);

            try
            {
                for (int i = 0; i < pageCount; i++)
                {
                    var startOffset = Convert.ToInt32(i * pageSize);
                    var categoryResponse = _categoryService.getCategories(startOffset, rowCount, true, true, true,
                        _appConfiguration.GittigidiyorSettings.Lang);

                    if (categoryResponse.ackCode == "success")
                    {
                        UpsertCategory(categoryResponse);
                    }
                    else
                    {
                        _logger.LogError(new EventId(500), new Exception("Categories cannot fetch"), "Categories cannot fetch",new
                        {
                            StartOffset = startOffset,
                            RowCount = rowCount,
                            ErrorMessage = categoryResponse.error.message,
                            ErrorCode = categoryResponse.error.errorCode
                        }, "FetchCategory");
                    }
                }
                _logger.LogInformation("Categories Fetched", "FetchCategory");
            }
            catch (Exception e)
            {
                _logger.LogError(new EventId(500), e, e.Message, tags: "FetchCategory");
            }

        }

        private void UpsertCategory(categoryServiceResponse categoryResponse)
        {
            foreach (var item  in categoryResponse.categories)
            {
                try
                {
                    var category = new Category()
                    {
                        CategoryCode = item.categoryCode,
                        CategoryName = item.categoryName,
                        Deepest = item.deepest,
                        HasCatalog = item.hasCatalog,
                        Depth = item.categoryCode.Length
                    };
                   var result =  _categoryRepository.UpsertCategory(category);
                 
                   if (result == 1)
                   {
                       _logger.LogInformation($"{category.CategoryName} added to database", "FetchCategory");
                   }

                   UpsertSpecs(item);
                }
                catch (Exception e)
                {
                    _logger.LogError(new EventId(500), e, e.Message, tags: "FetchCategory");
                }
     
            }
        }

        private void UpsertSpecs(categoryType categoryType)
        {
            if (categoryType.specs == null)
                return;
            
            var category = _categoryRepository.GetCategoryByCategoryCode(categoryType.categoryCode);
            if (category == null)
                return;

            foreach (var item in categoryType.specs)
            {
                var spec = new Spec
                {
                    Name = item.name,
                    CategoryId = category.Id,
                    Required = item.required,
                    Type = item.type
                };
                var specId = _specRepository.UpsertSpec(spec);

                UpsertSpecValue(item.values, category.Id, specId);
            }
        }

        private void UpsertSpecValue(string[] itemValues, int categoryId, int specId)
        {
            foreach (var item in itemValues)
            {
                var specValue = new SpecValue
                {
                    SpecId = specId,
                    Value = item,
                };
                _specRepository.UpsertSpecValue(specValue);
            }
        }
    }
}