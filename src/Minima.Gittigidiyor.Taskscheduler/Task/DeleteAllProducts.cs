﻿using ApiV2Client.GittiGidiyor;
using Padawan.Abstractions;
using Padawan.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Minima.Gittigidiyor.Taskscheduler.Task
{
    public class DeleteAllProducts : ISchedulerJob
    {
        [RunNow]
        public void DeleteAll()
        {
            try
            {
                var productService = ServiceProvider.getProductService();
                var products = productService.getProducts(0, 100, "U", false, "tr");

                if (products.ackCode != "success")
                    DeleteAll();

                var productIdList = products.products.Select(s => s.productId).ToList();
                var itemList = products.products.Select(s => s.itemId).ToList();
                //var finisEarhl = productService.finishEarly(productIdList, new List<string>(), "tr");
                //Console.WriteLine(finisEarhl.result);
                //if (finisEarhl.ackCode == "success")
                //{
                var delREsult = productService.deleteProduct(productIdList, new List<string>(), "tr");
                Console.WriteLine(delREsult.result);
                //}


                DeleteAll();
            }
            catch
            {
                DeleteAll();
            }
        }
    }
}
