﻿using System;
using System.Globalization;
using System.Linq;
using ApiV2Client.GittiGidiyor;
using Microsoft.Extensions.Logging;
using Minima.Gittigidiyor.Taskscheduler.Configuration;
using Padawan.Abstractions;
using Padawan.Attributes;
using RestSharp;

namespace Minima.Gittigidiyor.Taskscheduler.Task
{
    public class GetSalesTask : ISchedulerJob
    {
        private IntegrationApiConfiguration _configuration;
        public GetSalesTask(IntegrationApiConfiguration configuration)
        {
            _configuration = configuration;
        }

        //[RunNow]
        //[RunEvery(2, Period.Minute)]
        public void GetSales()
        {
            try
            {
                string startDate = DateTime.Now.ToString("dd/MM/yyyy 00:00:00", CultureInfo.InvariantCulture);
                string endDate = DateTime.Now.ToString("dd/MM/yyyy 23:59:00", CultureInfo.InvariantCulture);

                var salesService = ServiceProvider.getSaleService();
                var sales = salesService
                    .getSalesByDateRange(true, "S", string.Empty, "A", "A",
                        startDate, endDate, 1,
                        100, "tr");

                if (sales.ackCode == "success")
                {
                    foreach (var sale in sales.sales)
                    {
                        UpdateStock(sale.saleCode, sale.itemId, sale.amount);
                    }
                }

            }

            catch (Exception e)
            {
                //ignored
            }
        }


        private void UpdateStock(string salesCode, string productId, int amount)
        {

            var client = new RestClient($"{_configuration.Url}/api/Integration/update-stock");
            var request = new RestRequest(Method.GET);
            request.AddParameter("salesCode", salesCode);
            request.AddParameter("productId", productId);
            request.AddParameter("stockAmount", amount);

            var response = client.ExecuteAsync(request).GetAwaiter().GetResult();

        }
    }
}