﻿using ApiV2Client.GittiGidiyor;
using Minima.Data.MongoDb.Repository;
using Minima.Gittigidiyor.Taskscheduler.Domain;
using Padawan.Abstractions;
using Padawan.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Minima.Gittigidiyor.Taskscheduler.Task
{
    public class FixCargoPrice : ISchedulerJob
    {
        private readonly IMongoRepository<Product> _productRepository;

        public FixCargoPrice(IMongoRepository<Product> productRepository)
        {
            _productRepository = productRepository;
        }

        //[RunEveryDayAt(5, 0, 0)]
        //[RunNow]
        public void FixProductsDb()
        {
            var products = _productRepository.AsQueryable().ToList();
            var productService = ServiceProvider.getProductService();

            foreach (var product in products)
            {
                if (product.GittigidiyorProductId == null)
                    continue;

                var ggStatus = productService.getProduct(product.GittigidiyorProductId.Value.ToString(), "", "tr");

                if (ggStatus.ackCode == "success")
                {
                    
                    var productType = ggStatus.productDetail.product;

                    if (productType.cargoDetail.cargoCompanyDetails[0].cityPrice == "13.5")
                    {
                        continue;
                    }

                    productType.cargoDetail.cargoCompanyDetails[0].cityPrice = "13.50";
                    productType.cargoDetail.cargoCompanyDetails[0].countryPrice = "13.50";
                    var response = productService.updateProductWithNewCargoDetail(ggStatus.productDetail.itemId, ggStatus.productDetail.productId.ToString(), productType, true, false, false, "tr");

                    if (response.ackCode != "success")
                    {
                        Console.WriteLine("hata");
                    }

                    //_productRepository.ReplaceOne(product);
                }
            }
        }
    }
}
