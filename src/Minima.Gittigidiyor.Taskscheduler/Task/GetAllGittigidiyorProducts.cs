﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiV2Client.GittiGidiyor;
using Minima.Data.MongoDb.Repository;
using Minima.Gittigidiyor.Taskscheduler.Domain;
using Padawan.Abstractions;
using Padawan.Attributes;

namespace Minima.Gittigidiyor.Taskscheduler.Task
{

    public class GetAllGittigidiyorProducts : ISchedulerJob
    {
        private readonly IMongoRepository<GittigidiyorProduct> _mongoRepository;

        public GetAllGittigidiyorProducts(IMongoRepository<GittigidiyorProduct> mongoRepository)
        {
            _mongoRepository = mongoRepository;
        }

        //[RunNow]
        //[RunEveryDayAt(2,0,0)]
        public void GetAllProducts()
        {
            var productService = ServiceProvider.getProductService();

            var productCount = productService.getProducts(0, 1, "A", true, "tr");
            if (productCount.ackCode != "success")
                return;

            var pageCount = Math.Ceiling(productCount.productCount / 100d);

            for (int i = 0; i < pageCount; i++)
            {
                var productList = productService.getProducts(i * 100, 100, "A", true, "tr");
                if (productList.ackCode != "success")
                    continue;

                foreach (var product in productList.products)
                {

                    var isExist = _mongoRepository.FilterBy(f => f.ProductId == product.productId)
                        .FirstOrDefault();

                    if (isExist != null)
                    {
                        isExist.StockQuantity = product.product.productCount;
                        _mongoRepository.ReplaceOneAsync(isExist).GetAwaiter().GetResult();
                    }
                   


                    var ggProduct = new GittigidiyorProduct
                    {
                        Status = "A",
                        StockQuantity = product.product.productCount,
                        ItemId = product.itemId,
                        ProductId = product.productId,
                    };
                    _mongoRepository.InsertOneAsync(ggProduct).GetAwaiter().GetResult();
                }
            }

        }
    }
}
