﻿using ApiV2Client.GittiGidiyor;
using Microsoft.Extensions.Logging;
using Minima.Gittigidiyor.Taskscheduler.Configuration;
using Padawan.Abstractions;
using Padawan.Attributes;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Minima.Gittigidiyor.Taskscheduler.Task
{
    public class SatilmayanStoklulariAktifEt : ISchedulerJob
    {
        private ILogger<CalculatePriceForShoppingCartTask2> _logger;
        private readonly IntegrationApiConfiguration _configuration;

        public SatilmayanStoklulariAktifEt(ILogger<CalculatePriceForShoppingCartTask2> logger, IntegrationApiConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
        }


        [RunNow]
        //[RunEvery(30, Period.Second)]
        public void FetchCategories()
        {
            var productService = ServiceProvider.getProductService();

            var products = productService.getProducts(0, 100, "U", true, "tr");

            var pageCount = Math.Ceiling(products.productCount / 100d);

            if (products == null || products.products.Length == 0)
                return;

            for (int i = 0; i < pageCount; i++)
            {
                var productList = productService.getProducts(i * 100, 100, "U", true, "tr");
                if (productList.ackCode != "success")
                    continue;

                foreach (var product in productList.products)
                {
                    if (product.product.productCount == 0)
                    {
                        var delREsult = productService.deleteProduct(new List<int> { product.productId }, new List<string>(), "tr");
                        continue;
                    }


                    if (product.product.productCount == 0 && product.product.photos.Length == 0)
                        continue;

                  
                    var result = productService.calculatePriceForShoppingCart(new List<int> { product.productId }, new List<string>(), "tr");
                }

            }
              

        }


        private async System.Threading.Tasks.Task UpdateProductStatus(List<int> products, string s)
        {
            try
            {
                var client = new RestClient($"{_configuration.Url}/api/Gittigidiyor/UpdateProductStatus");
                var request = new RestRequest(Method.POST);
                request.AddJsonBody(products);
                request.AddParameter("status", s, ParameterType.HttpHeader);
                var response = await client.ExecuteAsync(request);
            }
            catch
            {
                // ignored
            }
        }

    }
}
