﻿using System;
using ApiV2Client.GittiGidiyor;
using ApiV2Client.GittiGidiyor.Sale;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Sale.Requests;
using Falcon.Package.MarketPlace.Gittigidiyor.Services;
using Microsoft.Extensions.Logging;
using Minima.Gittigidiyor.Taskscheduler.Configuration;
using Padawan.Abstractions;
using Padawan.Attributes;

namespace Minima.Gittigidiyor.Taskscheduler.Task
{
    public class FetchSalesTask : ISchedulerJob
    {
        private readonly AppConfiguration _appConfiguration;
        private readonly ILogger<FetchSalesTask> _logger;
        private readonly SaleService _saleService;
        private ISalesApiService _salesApiService;

        public FetchSalesTask(
            AppConfiguration appConfiguration,
            ILogger<FetchSalesTask> logger,
            ISalesApiService salesApiService)
        {
            _appConfiguration = appConfiguration;
            _logger = logger;
            _salesApiService = salesApiService;

            _saleService = ServiceProvider.getSaleService();
        }

        //[RunNow]
        //[RunEvery(30, Period.Minute)]
        public void FetchSales()
        {
            _logger.LogInformation("Fetch Sales Task started.");

            var startDate = DateTime.Now.AddDays(-1).ToString("dd/MM/yyyy hh:mm:ss");
            var endDate = DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss");

            var response = _saleService.getSalesByDateRange(true, "S", string.Empty, "V", "A", startDate, endDate, 1, 100,
                 _appConfiguration.GittigidiyorSettings.Lang);

            var response2 = _salesApiService.GetSalesByDateRange(new GetSalesByDateRangeRequestReturn
            {
                WithData = true,
                ByStatus = "S",
                ByUser = string.Empty,
                OrderBy = "V",
                OrderType = "A",
                StartDate = startDate,
                EndDate = endDate,
                PageNumber = 1,
                PageSize = 100,
                Lang = _appConfiguration.GittigidiyorSettings.Lang
            });
        }
    }
}