﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiV2Client.GittiGidiyor;
using Minima.Data.MongoDb.Repository;
using Minima.Gittigidiyor.Taskscheduler.Domain;
using Padawan.Abstractions;
using Padawan.Attributes;

namespace Minima.Gittigidiyor.Taskscheduler.Task
{
    public class FixProductsFromGittigidiyor : ISchedulerJob
    {
        private IMongoRepository<Product> _productRepository;

        public FixProductsFromGittigidiyor(IMongoRepository<Product> productRepository)
        {
            _productRepository = productRepository;
        }

        //[RunNow]
        //[RunEveryDayAt(5, 0, 0)]
        public void FixProductsDb()
        {
            var products = _productRepository.AsQueryable().ToList();
            var productService = ServiceProvider.getProductService();

            foreach (var product in products)
            {
                if (product.GittigidiyorProductId == null)
                    continue;

                var ggStatus = productService.getProduct(product.GittigidiyorProductId.Value.ToString(), "", "tr");

                if (ggStatus.ackCode == "success")
                {
                    product.Status = ggStatus.productDetail.summary.listingStatus;
                    _productRepository.ReplaceOne(product);
                }
            }
        }
    }
}
