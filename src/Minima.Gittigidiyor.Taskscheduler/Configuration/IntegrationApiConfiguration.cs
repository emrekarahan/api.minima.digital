﻿using Padawan.Attributes;

namespace Minima.Gittigidiyor.Taskscheduler.Configuration
{
    [Configuration("IntegrationService")]
    public class IntegrationApiConfiguration
    {
        public string Url { get; set; }
    }
}
