﻿using Padawan.Attributes;

namespace Minima.Gittigidiyor.Taskscheduler.Configuration
{
    [Configuration("AppConfiguration")]
    public class AppConfiguration
    {
        public GittigidiyorSettings GittigidiyorSettings { get; set; }
        public DatabaseSettings GittigidiyorDatabase { get; set; }
    }

    public class GittigidiyorSettings
    {
        public string ApiKey { get; set; }
        public string SecretKey { get; set; }
        public string RolePass { get; set; }
        public string RoleName { get; set; }
        public string Lang { get; set; }
        public bool ForceToSpecEntry { get; set; }
        public bool NextDateOption { get; set; }
    }

    public class DatabaseSettings
    {
        public string ConnectionString { get; set; }
    }
}