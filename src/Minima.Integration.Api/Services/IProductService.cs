﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Products.Response;
using Minima.Core.MessageModels;
using Minima.Integration.Api.Domain.Product;
using Minima.Integration.Api.Model.Response;
using Minima.Integration.Api.Model.Response.Gittigidiyor;
using Minima.Nop.Client.Domain.Product;

namespace Minima.Integration.Api.Services
{
    public interface IProductService
    {
        Task<Response> ImportProduct(ProductMessageModel productMessageModel);

        Task<ProductServiceResponseReturn> ImportMongoDb(int productId);
        Task UpdateProductMongoDb(int productId, int syncedProductId);
        Task InsertAvaliableProducts();
        List<int> GetListingReadyProducts();
        Task UpdateProductStatus(List<int> products, string status);

        Task<int> GetIdeaProductId(int productId);
        Task<int> GetInteralProductId(int productId);
        Task UpdateProductStock(int internalProductId, int ideaStockAmount);
        Task UpdateProductError(int productId, string errorMessage);
        Task UpdateProductStock(ProductMessageModel productMessageModel);
    }
}