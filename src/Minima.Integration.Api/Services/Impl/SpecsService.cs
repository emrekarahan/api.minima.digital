﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Minima.Integration.Api.Configurations;
using Minima.Integration.Api.Model.Gittigidiyor.Requests;
using Padawan.Attributes;

namespace Minima.Integration.Api.Services.Impl
{
    [Singleton]
    public class SpecsService
    {
        private readonly GittigidiyorIntegrationDbSettings _databaseSettings;

        public SpecsService(GittigidiyorIntegrationDbSettings databaseSettings)
        {
            _databaseSettings = databaseSettings;
        }

        public async Task<int> GetCategoryByCode(string code)
        {
            using (var conn = new SqlConnection(_databaseSettings.ConnectionString))
            {
                var sql = "SELECT Id FROM Category Where CategoryCode = @Code";
                var result = await conn.QueryFirstOrDefaultAsync<int>(sql, new {Code = code});
                return result;
            }
        }

        public async Task UpdateCategorySpecs(int categoryId, List<SpecsModel> specs)
        {
            foreach (var specsModel in specs)
            {
                using (var conn = new SqlConnection(_databaseSettings.ConnectionString))
                {
                    var sqlStatement = new StringBuilder()
                        .AppendLine("BEGIN ")
                        .AppendLine("       IF NOT EXISTS(SELECT * FROM Spec WHERE CategoryId = @CategoryId AND Name = @Name) ")
                        .AppendLine("   BEGIN ")
                        .AppendLine("       INSERT INTO Spec(CategoryId, Name, Required, Type) ")
                        .AppendLine("       VALUES(@CategoryId, @Name, @Required, @Type) ")
                        .AppendLine("   END ")
                        .AppendLine("SELECT Id FROM Spec WHERE CategoryId = @CategoryId AND Name = @Name")
                        .AppendLine("END").ToString();
                    var specId = await conn.QueryFirstOrDefaultAsync<int>(sqlStatement, new
                    {
                        CategoryId = categoryId,
                        Name = specsModel.Name,
                        specsModel.Required,
                        specsModel.Type
                    });

                    foreach (var model in specsModel.Values)
                    {
                        var sqlStatement2 = new StringBuilder()
                            .AppendLine("BEGIN ")
                            .AppendLine("       IF NOT EXISTS(SELECT * FROM SpecValue WHERE SpecId = @SpecId AND Value = @Value) ")
                            .AppendLine("   BEGIN ")
                            .AppendLine("       INSERT INTO SpecValue(SpecId, Value) ")
                            .AppendLine("       VALUES(@SpecId, @Value) ")
                            .AppendLine("   END ")
                            .AppendLine("END").ToString();

                        await conn.QueryFirstOrDefaultAsync<int>(sqlStatement2, new
                        {
                            SpecId = specId,
                            Value = model.Name
                        });
                    }
                }
            }


        }
    }
}