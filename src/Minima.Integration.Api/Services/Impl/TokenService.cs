﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using IdentityModel.Client;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Minima.Core;
using Minima.Core.Repositories;
using Minima.Integration.Api.Configurations;
using Minima.Integration.Api.Model.Response;
using Minima.Integration.Api.Model.Token;
using Minima.Integration.Data;
using Minima.Integration.Data.Domain;
using Newtonsoft.Json;
using Padawan.Attributes;
using RestSharp;

namespace Minima.Integration.Api.Services.Impl
{

    [Scoped]
    public class TokenService
    {

        private readonly ILogger<TokenService> _logger;
        private readonly IdeasoftConfiguration _ideasoftConfiguration;
        private readonly IRepository<AccessToken, IntegrationContext> _repository;

        public TokenService(
            ILogger<TokenService> logger, 
            IdeasoftConfiguration ideasoftConfiguration, 
            IRepository<AccessToken, IntegrationContext> repository)
        {
            _logger = logger;
            _ideasoftConfiguration = ideasoftConfiguration;
            _repository = repository;
        }


        public async Task<TokenResponse> Auth()
        {

            var client = new RestClient(_ideasoftConfiguration.AuthorizationUrl);

            var request = new RestRequest(Method.GET);
            request.AddQueryParameter("client_id", _ideasoftConfiguration.ClientId);
            request.AddQueryParameter("response_type", "code");
            request.AddQueryParameter("state", Guid.NewGuid().ToString());

            var response = await client.ExecuteAsync(request);

            //if (response.IsSuccessful)
            //{
            //    return response.Data;
            //}

            //_logger.LogCritical(response.ErrorException.Message, response.ErrorException);
            //return new ShippingStatusOutModel
            //{
            //    IsSuccess = false,
            //    Message = response.ErrorException.Message
            //};
            //var client = new HttpClient();


            //var token = await client.GetAsync(_ideasoftConfiguration.AuthorizationUrl);


            ////if (disco.IsError) throw new Exception(disco.Error);
            //var token = await client.RequestAuthorizationCodeTokenAsync(new AuthorizationCodeTokenRequest()
            //{
            //    Address = _ideasoftConfiguration.AuthorizationUrl,
            //    ClientSecret = _ideasoftConfiguration.ClientSecret,
            //    ClientId = _ideasoftConfiguration.ClientId,
            //    Code = "code"

            //    //UserName = userName,
            //    //Password = password,
            //    //Scope = _ideasoftConfiguration.IdentityScopes
            //});

            return new TokenResponse();
        }

        public async Task<AccessToken> CurrentToken()
        {
            return await _repository.QueryAble().FirstOrDefaultAsync();
        }

        public async Task<AccessToken> RefreshToken(AccessToken accessToken)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append($"{_ideasoftConfiguration.TokenUrl}")
                .Append("?grant_type=refresh_token")
                .Append($"&client_id={_ideasoftConfiguration.ClientId}")
                .Append($"&client_secret={_ideasoftConfiguration.ClientSecret}")
                .Append($"&refresh_token={accessToken.RefreshToken}");

            var refreshTokenUrl = sb.ToString();
            HttpClient httpClient = new HttpClient();
            var response = await httpClient.GetStringAsync(refreshTokenUrl);

            var currentToken = JsonConvert.DeserializeObject<CurrentToken>(response);


            accessToken.RefreshToken = currentToken.RefreshToken;
            accessToken.AccessTokenMember = currentToken.AccessToken;
            accessToken.ExpireDate = DateTime.Now.AddHours(6);

            await _repository.UpdateAsync(accessToken);

            return accessToken;
        }

        public async Task<Response<TokenOutModel>> GetToken(string state, string code)
        {
            try
            {
                var client = new RestClient(_ideasoftConfiguration.TokenUrl);

                var request = new RestRequest(Method.GET);
                request.AddQueryParameter("client_id", _ideasoftConfiguration.ClientId);
                request.AddQueryParameter("grant_type", "authorization_code");
                request.AddQueryParameter("client_secret", _ideasoftConfiguration.ClientSecret);
                request.AddQueryParameter("code", code);
                request.AddQueryParameter("redirect_uri", _ideasoftConfiguration.RedirectUri);

                var response = await client.ExecuteAsync<TokenOutModel>(request);
                if (response.IsSuccessful)
                {
                    return new Response<TokenOutModel>
                    {
                        Data = response.Data,
                        Code = 200
                    };
                }
            }
            catch (Exception e)
            {
                return new Response<TokenOutModel>
                {
                    Message = e.Message,
                    InternalMessage = e.InnerException?.Message
                };
            }
      

            return null;
        }
    }
}