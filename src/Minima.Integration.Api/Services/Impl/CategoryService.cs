﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Dapper;
using Microsoft.Extensions.Logging;
using Minima.Core.MessageModels;
using Minima.Integration.Api.Configurations;
using Minima.Integration.Data.Domain;
using Minima.Nop.Client.Services;




namespace Minima.Integration.Api.Services.Impl
{
    public class CategoryService : ICategoryService
    {
        private readonly ILogger<CategoryService> _logger;
        private readonly IntegrationSettings _integrationSettings;
        private readonly DatabaseSettings _databaseSettings;
        private readonly CategoryRepository _categoryRepository;


        public CategoryService(
            ILogger<CategoryService> logger,
            IntegrationSettings integrationSettings,
            DatabaseSettings databaseSettings,
            CategoryRepository categoryRepository)
        {
            _logger = logger;
            _integrationSettings = integrationSettings;
            _databaseSettings = databaseSettings;
            _categoryRepository = categoryRepository;
            }

        public async Task ImportCategories(List<CategoryMessageModel> categoryInModels)
        {
            try
            {
                int? parentCategoryId = null;
                foreach (var categoryInModel in categoryInModels)
                {
                    var dbCategory = await GetCategoryMapByIdeaCategoryId(categoryInModel.Id);

                    if (dbCategory != null)
                    {
                        parentCategoryId = dbCategory.InternalCategoryId;
                        continue;
                    }

                    var newCategoryId = await _categoryRepository.InsertCategory(categoryInModel.Name, categoryInModel.MetaDescription,
                        categoryInModel.MetaKeywords, categoryInModel.MetaDescription, string.Empty, parentCategoryId ?? 0,
                        categoryInModel.SortOrder, categoryInModel.CreatedAt, categoryInModel.UpdatedAt);
                    await InsertCategoryMapping(categoryInModel, newCategoryId);
                }
            }
            catch (Exception e)
            {
                _logger.LogError(new EventId(500), e.Message, e);
            }
        }

        private async Task<IdeasoftCategoryMap> GetCategoryMapByIdeaCategoryId(int id)
        {
            var sqlCommand = "SELECT * FROM IdeasoftCategoryMap WHERE IdeaCategoryId =@IdeaCateogryId";
            using (var conn = new SqlConnection(_databaseSettings.Integration.ConnectionString))
            {
                var result = await conn.QueryFirstOrDefaultAsync<IdeasoftCategoryMap>(sqlCommand, new
                {
                    @IdeaCateogryId = id
                });
                return result;
            }
        }

        //private async Task InsertUrlRecord(int newCategoryId, string slug)
        //{
        //    await WebApiServices.AddUrlRecord(new UrlRecord
        //    {
        //        EntityId = newCategoryId,
        //        EntityName = "Category",
        //        IsActive = true,
        //        LanguageId = 0,
        //        Slug = slug
        //    });
        //}

        private async Task InsertCategoryMapping(CategoryMessageModel categoryMessageModel, int newCategoryId)
        {
            var insertSql =
                "INSERT IdeasoftCategoryMap (IdeaCategoryId, InternalCategoryId, CreatedDateUtc, UpdatedDateUtc, LastUpdatedDateUtc) " +
                "VALUES(@IdeaCategoryId, @InternalCategoryId, @CreatedDateUtc, @UpdatedDateUtc, @LastUpdatedDateUtc);";

            using (var conn = new SqlConnection(_databaseSettings.Integration.ConnectionString))
            {
                await conn.ExecuteAsync(insertSql, new
                {
                    CreatedDateUtc = categoryMessageModel.CreatedAt,
                    UpdatedDateUtc = categoryMessageModel.UpdatedAt,
                    LastUpdatedDateUtc = DateTime.UtcNow,
                    IdeaCategoryId = categoryMessageModel.Id,
                    InternalCategoryId = newCategoryId
                });
            }
        }

        public async Task<int> GetMaxCategoryId()
        {
            var sqlCommand = "SELECT MAX(IdeaCategoryId) FROM IdeasoftCategoryMap";
            using (var conn = new SqlConnection(_databaseSettings.Integration.ConnectionString))
            {
                var result = await conn.ExecuteScalarAsync<int>(sqlCommand);
                return result;
            }
        }
    }
}