﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Dapper;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Products.Requests;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Products.Response;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Products.Types;
using Falcon.Package.MarketPlace.Gittigidiyor.Services;
using Microsoft.Extensions.Logging;
using Minima.Core.MessageModels;
using Minima.Integration.Api.Configurations;
using Minima.Integration.Api.Domain;
using Minima.Integration.Api.Domain.Product;
using Minima.Integration.Api.Model.Response;
using Minima.Integration.Api.Model.Response.Gittigidiyor;
using Minima.Integration.Api.Proxies.GGProxy;
using Minima.Integration.Data.Domain;
using Minima.Data.MongoDb.Repository;
using Minima.Nop.Client.Domain.Product;
using Minima.Nop.Client.Models;
using Minima.Nop.Client.Services;
using MongoDB.Driver;
using Padawan.Extensions;
using Padawan.RabbitMq.Extensions;
using Padawan.RabbitMq.Provider;
using RestSharp;


namespace Minima.Integration.Api.Services.Impl
{
    public class ProductService : IProductService
    {
        private readonly ILogger<ProductService> _logger;
        private readonly IBusProvider _busProvider;
        private readonly DatabaseSettings _databaseSettings;
        private readonly IPictureService _pictureService;
        private readonly ProductRepository _dapperProductService;
        private readonly NopApiSettings _nopApiSettings;
        private readonly IMongoRepository<Product> _mongoRepository;
        private readonly IMapper _mapper;
        private readonly IMongoRepository<GittigidiyorSettings> _gittigidiyorSettingsRepository;
        //private readonly GittigidiyorProxyClient _gittigidiyorProxyClient;
        private readonly IProductApiService _productApiService;
        public ProductService(
            IntegrationSettings integrationSettings,
            ILogger<ProductService> logger,
            IBusProvider busProvider,
            DatabaseSettings databaseSettings,
            IPictureService pictureService,
            ProductRepository dapperProductService,
            NopApiSettings nopApiSettings,
            IMongoRepository<Product> mongoRepository,
            IMapper mapper,
            IMongoRepository<GittigidiyorSettings> gittigidiyorSettingsRepository,
            //GittigidiyorProxyClient gittigidiyorProxyClient, 
            IProductApiService productApiService)
        {
            _logger = logger;
            _busProvider = busProvider;
            _databaseSettings = databaseSettings;
            _pictureService = pictureService;
            _dapperProductService = dapperProductService;
            _nopApiSettings = nopApiSettings;
            _mongoRepository = mongoRepository;
            _mapper = mapper;
            _gittigidiyorSettingsRepository = gittigidiyorSettingsRepository;
            //_gittigidiyorProxyClient = gittigidiyorProxyClient;
            _productApiService = productApiService;
        }

        public async Task<Response> ImportProduct(ProductMessageModel productMessageModel)
        {
            try
            {
                int.TryParse(productMessageModel.StockAmount.ToString(CultureInfo.InvariantCulture), out var stockQuantity);
                var price = Convert.ToDecimal(productMessageModel.Price1);
                var publised = Convert.ToBoolean(productMessageModel.Status);
                decimal.TryParse(productMessageModel.BuyingPrice.ToString(CultureInfo.InvariantCulture),
                    out var buyingPrice);



                //Check or Insert TaxCategory
                var taxCategoryId = await InsertTaxCategory(productMessageModel.Tax);

                var dapperProductModel = new DapperProductModel
                {
                    Name = productMessageModel.Name,
                    ShortDescription = productMessageModel.ShortDetails,
                    Sku = productMessageModel.Sku,
                    Price = price,
                    ProductTypeId = 5,
                    ProductCost = buyingPrice,
                    StockQuantity = stockQuantity,
                    Published = publised,
                    CreatedOnUtc = productMessageModel.CreatedAt,
                    UpdatedOnUtc = productMessageModel.UpdatedAt,
                    MetaDescription = productMessageModel.MetaDescription,
                    MetaKeywords = productMessageModel.MetaKeywords,
                    TaxCategoryId = taxCategoryId,
                    IsShipEnabled = true,
                    AdditionalShippingCharge = Convert.ToDecimal(productMessageModel.CustomShippingCost),
                    Gtin = productMessageModel.Barcode,
                    MetaTitle = string.Empty
                };

                if (productMessageModel.Details != null && productMessageModel.Details.Count > 0)
                    dapperProductModel.FullDescription = productMessageModel.Details.FirstOrDefault()?.Details;

                var mappedProduct = await GetProductMapGetById(productMessageModel.Id);

                if (mappedProduct == null)
                    await InsertProduct(dapperProductModel, productMessageModel);
                else
                    await UpdateProduct(dapperProductModel, productMessageModel);


                //Product Images Send To Bus for Update
                await PicturesSendToBus("product_picture_save", dapperProductModel.Id, productMessageModel.Images);

                return new Response { Code = 200 };
            }
            catch (Exception e)
            {
                _logger.LogError(new EventId(500), e, e.Message);
                return new Response
                {
                    Message = e.Message,
                    InternalMessage = e.InnerException?.Message,
                    Code = 500
                };
            }
        }

        public async Task<ProductServiceResponseReturn> ImportMongoDb(int productId)
        {
            try
            {

                var productDto = await _dapperProductService.PrepareProductForIntegration(productId);

                if (productDto.GittigidiyorCategoryList == null || productDto.GittigidiyorCategoryList.Count == 0)
                {
                    return await Task.FromCanceled<ProductServiceResponseReturn>(new CancellationToken());
                }

                var settings = _gittigidiyorSettingsRepository.AsQueryable().FirstOrDefault();
                var commission = Convert.ToDecimal(settings.Commission);

                foreach (var picture in productDto.PictureModelList)
                {
                    var fileExtension = SelectFileExtension(picture.MimeType);
                    var fileName = $"{picture.Id:0000000}_0.{fileExtension}";
                    picture.ImageUrl = $"{settings.StoreLocation}images/{fileName}";
                }

                var rawPrice = productDto.Price + ((productDto.Price * commission) / 100);
                productDto.GittigidiyorPrice = Math.Round(Convert.ToDecimal(rawPrice), 2);
                productDto.CityCode = settings.CityCode;
                productDto.CityPrice = settings.CityPrice;
                productDto.ShippingWhere = settings.ShippingWhere;
                productDto.CargoCompany = settings.CargoCompanies;

                var product = _mapper.Map<Product>(productDto);



                var isExist = _mongoRepository.FilterBy(f => f.ProductId == productId).FirstOrDefault();
                if (isExist == null)
                {
                    await _mongoRepository.InsertOneAsync(product);
                    productDto = _mapper.Map<ProductDto>(product);

                    if (productDto.IsSyncable && !productDto.Synced && string.IsNullOrEmpty(productDto.Status))
                    {
                        var response = _productApiService.InsertAndActivateProduct(new InsertAndActivateProductRequestReturn
                        {
                            Lang = "tr",
                            ForceToSpecEntry = true,
                            NextDateOption = true,
                            ItemId = productDto.Id.ToString(),
                            Product = ProductInsert(productDto)


                        });
                        //var response = await _gittigidiyorProxyClient.InsertProduct(productDto);
                        return response;
                    }
                }

                product.Id = isExist.Id;
                product.Synced = isExist.Synced;
                product.SyncedAt = isExist.SyncedAt;
                product.GittigidiyorProductId = isExist.GittigidiyorProductId;
                product.StockQuantity = productDto.StockQuantity;
                await _mongoRepository.ReplaceOneAsync(product);
                await _dapperProductService.UpdateProductStock(product.ProductId, productDto.StockQuantity);
                productDto = _mapper.Map<ProductDto>(product);

                if (productDto.StockQuantity != isExist.StockQuantity)
                {
                    var stockResponse =
                        _productApiService.UpdateStock(new UpdateStockRequestReturn
                        {
                            ProductId = product.GittigidiyorProductId.Value.ToString(),
                            Lang = "tr",
                            Stock = productDto.StockQuantity,
                        });
                    //await _gittigidiyorProxyClient.UpdateProductStock(product.GittigidiyorProductId.Value,
                    //    productDto.StockQuantity);

                    if (stockResponse.AckCode == "success")
                    {
                        _logger.LogInformation($"[{productId}] {productDto.Name} => stok bilgisi güncellendi");
                    }
                }

                if (productDto.GittigidiyorPrice != isExist.GittigidiyorPrice)
                {
                    var response = _productApiService.UpdatePrice(new UpdatePriceRequestReturn
                    {
                        Lang = "tr",
                        ProductId = productDto.GittigidiyorProductId.ToString(),
                        Price = (double)productDto.Price
                    });
                    //await _gittigidiyorProxyClient.UpdatePrice(productDto);
                    if (response.AckCode == "success")
                    {
                        _logger.LogInformation($"[{productId}] {productDto.Name} => fiyat bilgisi güncellendi");
                    }
                    return response;
                }
            }
            catch (Exception e)
            {
                _logger.LogError(new EventId(500), e, e.Message);
            }
            return null;
        }

        private ProductTypeReturn ProductInsert(ProductDto product)
        {
            var productTypeModel = new ProductTypeReturn();
            var specTypeList = new List<SpecTypeReturn>();

            foreach (SpecDto dto in product.SpecList)
            {
                var spec = new SpecTypeReturn()
                { Name = dto.Name, Type = dto.Type, Value = dto.Value, Required = dto.Required };
                specTypeList.Add(spec);
            }

            var photoTypeList = new List<PhotoTypeReturn>();

            var pictureList = product.PictureModelList.OrderBy(o => o.DisplayOrder).ToList();
            for (int i = 0; i < pictureList.Count; i++)
            {
                var photo = new PhotoTypeReturn { PhotoId = i, Url = pictureList[i].ImageUrl };
                photoTypeList.Add(photo);
            }




            var cargoDetail = new CargoDetailTypeReturn
            { City = product.CityCode, CargoCompanies = new[] { product.CargoCompany } };

            var type = new CargoCompanyDetailTypeReturn()
            {
                Name = product.CargoCompany,
                //cityPrice = product.CityPrice, 
                CityPrice = "0",
                //countryPrice = product.CityPrice
                CountryPrice = "0"
            };
            cargoDetail.CargoCompanyDetails = new CargoCompanyDetailTypeReturn[1];
            cargoDetail.CargoCompanyDetails[0] = type;


            if (productTypeModel.BuyNowPrice >= 300)
            {
                cargoDetail.ShippingPayment = "S";
            }
            else
            {
                cargoDetail.ShippingPayment = "B";
            }


            cargoDetail.ShippingFeePaymentType = "PAY_IN_THE_BASKET";
            cargoDetail.ShippingWhere = "country";
            cargoDetail.ShippingTime = new CargoTimeTypeReturn() { Days = "today", BeforeTime = "16:30" };

            if (product.Name.Length > 100)
            {
                product.Name = product.Name.Substring(0, 100);
            }


            productTypeModel.Specs = specTypeList.ToArray();
            productTypeModel.Photos = photoTypeList.ToArray();
            productTypeModel.PageTemplate = 3;
            //productTypeModel.PageTemplateSpecified = true;
            productTypeModel.Description = product.FullDescription;
            productTypeModel.Format = "S";
            productTypeModel.BuyNowPrice = (double)product.GittigidiyorPrice;
            //productTypeModel.BuyNowPriceSpecified = true;
            productTypeModel.ListingDays = 360;
            //productTypeModel.ListingDaysSpecified = true;
            productTypeModel.ProductCount = product.StockQuantity;
            //productTypeModel.ProductCountSpecified = true;
            productTypeModel.CargoDetail = cargoDetail;
            productTypeModel.StartDate = DateTime.Now.AddMinutes(10).ToString("yyyy-MM-dd HH':'mm':'ss");
            productTypeModel.AffiliateOption = false;
            productTypeModel.BoldOption = false;
            productTypeModel.CatalogOption = false;
            productTypeModel.VitrineOption = false;
            productTypeModel.CategoryCode = product.GittigidiyorCategoryList.First().CategoryCode;

            return productTypeModel;
        }
        public async Task UpdateProductMongoDb(int productId, int syncedProductId)
        {
            try
            {
                var product = _mongoRepository.FilterBy(f => f.ProductId == productId).FirstOrDefault();
                if (product != null)
                {
                    product.GittigidiyorProductId = syncedProductId;
                    product.SyncedAt = DateTime.Now;
                    product.Synced = true;
                    if (string.IsNullOrEmpty(product.Status))
                        product.Status = "L";

                    await _mongoRepository.ReplaceOneAsync(product);
                    _logger.LogInformation($"[{productId}] - {product.Name}  ==> [{syncedProductId}] {DateTime.Now} olarak Gittigidiyor'a gonderildi");
                }
            }
            catch (Exception e)
            {
                _logger.LogError(new EventId(500), e, e.Message);
            }

        }

        public async Task InsertAvaliableProducts()
        {
            try
            {
                var products = await _dapperProductService.GetAvaliabledProducts();
                foreach (var productGroup in products.PageIterator(100))
                {
                    try
                    {
                        foreach (var product in productGroup)
                        {
                            var isExists = _mongoRepository.FilterBy(f => f.ProductId == product && f.Synced == true).FirstOrDefault();
                            if (isExists != null)
                                continue;

                            await ImportMongoDb(product);
                        }
                    }
                    catch (Exception)
                    {

                        continue;
                    }

                }
            }
            catch (Exception e)
            {
                _logger.LogError(new EventId(500), e, e.Message);
            }

        }

        public List<int> GetListingReadyProducts()
        {

            var result = _mongoRepository
                .FilterBy(f => f.Synced && f.Status == "L" && f.GittigidiyorProductId != null)
                .Select(s => s.GittigidiyorProductId)
                .Take(100)
                .ToList().Cast<int>().ToList();
            ;
            return result;
        }

        public async Task UpdateProductStatus(List<int> products, string status)
        {
            var updatedProducts = _mongoRepository.AsQueryable()
                .Where(w => products.Contains(w.GittigidiyorProductId.Value));
            foreach (var updatedProduct in updatedProducts)
            {
                updatedProduct.Status = status;
                await _mongoRepository.ReplaceOneAsync(updatedProduct);
            }
        }

        public async Task<int> GetIdeaProductId(int productId)
        {
            using (var conn = new SqlConnection(_databaseSettings.Integration.ConnectionString))
            {
                var ideaProductId = await conn.QueryFirstOrDefaultAsync<int>(
                    "SELECT IdeasoftProductId FROM IdeasoftProductMap WHERE InternalProductId = @productId", new { productId });
                return ideaProductId;
            }
        }

        public async Task<int> GetInteralProductId(int productId)
        {
            using (var conn = new SqlConnection(_databaseSettings.Integration.ConnectionString))
            {
                var ideaProductId = await conn.QueryFirstOrDefaultAsync<int>(
                    "SELECT InternalProductId FROM IdeasoftProductMap WHERE IdeasoftProductId = @productId", new { productId });
                return ideaProductId;
            }
        }

        public async Task UpdateProductStock(int internalProductId, int ideaStockAmount)
        {
            await _dapperProductService.UpdateProductStock(internalProductId, ideaStockAmount);
        }

        public async Task UpdateProductError(int productId, string errorMessage)
        {
            var product = _mongoRepository.FilterBy(f => f.GittigidiyorProductId == productId)
                .FirstOrDefault();

            if (product == null)
                return;

            product.HasError = true;
            product.ErrorMessage = errorMessage;

            await _mongoRepository.ReplaceOneAsync(product);
        }

        public async Task UpdateProductStock(ProductMessageModel productMessageModel)
        {
            try
            {
                var internalProductId = await GetInteralProductId(productMessageModel.Id);

                if (internalProductId == 0)
                    return;
                await _dapperProductService.UpdateProductStock(internalProductId, (int)productMessageModel.StockAmount);
                var mongoProduct = _mongoRepository.FilterBy(f => f.ProductId == internalProductId).FirstOrDefault();

                if (mongoProduct != null)
                {
                    mongoProduct.StockQuantity = (int)productMessageModel.StockAmount;
                    await _mongoRepository.ReplaceOneAsync(mongoProduct);
                    var productDto = _mapper.Map<ProductDto>(mongoProduct);
                    var stockResponse =
                        _productApiService.UpdateStock(new UpdateStockRequestReturn
                        {
                            ProductId = mongoProduct.GittigidiyorProductId.ToString(),
                            Lang = "tr",
                            Stock = (int)productMessageModel.StockAmount
                        });

                    //await _gittigidiyorProxyClient.UpdateProductStock(mongoProduct.GittigidiyorProductId.Value, (int)productMessageModel.StockAmount);
                    _logger.LogInformation($"{stockResponse.ProductId} {stockResponse.Result}");
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
            }


        }

        #region Product Mapping
        private async Task UpdateProduct(DapperProductModel product, ProductMessageModel productMessageModel)
        {
            try
            {
                var productMap = await GetProductMapGetById(productMessageModel.Id);
                var productSku = await _dapperProductService.CheckProductBySku(product.Sku);

                //if (productMap.UpdatedDateUtc == productMessageModel.UpdatedAt)
                //    return;

                if (productMessageModel.UpdatedAt == productSku.UpdatedOnUtc)
                    return;


                product.Id = productMap.InternalProductId;
                await _dapperProductService.UpdateProduct(product);


                var categories = await GetProductMappedCategories(productMessageModel, product);
                var productCategoryModel = categories.Select(s => new ProductCategoryModel
                {
                    CategoryId = s.InternalCategoryId,
                    ProductId = product.Id
                }).ToList();

                await _dapperProductService.ClearProductCategory(product.Id);
                await _dapperProductService.InsertProductCategory(productCategoryModel);

                ////Update URL Record
                //if (!string.IsNullOrEmpty(productMessageModel.Slug))
                //    await InsertUrlRecord(product.Id, productMessageModel.Slug);

                //Product Map Update
                await UpdateProductMapping(productMessageModel, product.Id);

                await UpdateTriggerNopApi(product.Id);

                // _logger.LogInformation(new EventId(200), $"{product.Name} is updated");

                //Product Images Send To Bus for Update
                //await PicturesSendToBus("product_picture_save", product.Id, productMessageModel.Images);
            }
            catch (Exception e)
            {
                _logger.LogError(new EventId(500), e.Message, e);
                throw;
            }

        }
        private async Task InsertProduct(DapperProductModel product, ProductMessageModel productMessageModel)
        {
            try
            {
                var productId = await _dapperProductService.InsertProduct(product);
                product.Id = productId;

                var categories = await GetProductMappedCategories(productMessageModel, product);
                var productCategoryModel = categories.Select(s => new ProductCategoryModel
                {
                    CategoryId = s.InternalCategoryId,
                    ProductId = product.Id
                }).ToList();

                await _dapperProductService.InsertProductCategory(productCategoryModel);

                //if (!string.IsNullOrEmpty(productMessageModel.Slug))
                //    await InsertUrlRecord(product.Id, productMessageModel.Slug);

                await UpdateTriggerNopApi(productId);

                await InsertProductMapping(productMessageModel, product.Id);

                _logger.LogInformation(new EventId(200), $"{product.Name} is inserted");
            }
            catch (Exception e)
            {
                _logger.LogError(new EventId(500), e, e.Message);
                throw;
            }


        }

        private async Task UpdateTriggerNopApi(int productId)
        {
            //await GetProductFromNopApi(productId);
            await UpdateProductWithNopApi(productId);

        }

        private async Task UpdateProductWithNopApi(int productId)
        {
            try
            {
                var client = new RestClient($"{_nopApiSettings.Url}/api/products/{productId}");
                var request = new RestRequest(Method.PUT);
                var json = new { product = new { id = productId } };
                request.AddJsonBody(json);
                var response = await client.ExecuteAsync(request);
            }
            catch (Exception e)
            {
                _logger.LogError(new EventId(500), e.Message, e);
            }
        }

        private async Task GetProductFromNopApi(int productId)
        {
            try
            {
                var client = new RestClient($"{_nopApiSettings.Url}/api/products");
                var request = new RestRequest(Method.GET);
                request.AddQueryParameter("id", productId.ToString());
                var response = await client.ExecuteAsync(request);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        private async Task<List<IdeasoftCategoryMap>> GetProductMappedCategories(ProductMessageModel productMessageModel, DapperProductModel product)
        {
            var selectQuery = "SELECT * FROM IdeasoftCategoryMap WHERE IdeaCategoryId IN @CategoryIds";
            var categoryList = productMessageModel.ProductToCategories.Select(s => s.Category.Id).ToList();
            using (var conn = new SqlConnection(_databaseSettings.Integration.ConnectionString))
            {
                var categories = await conn.QueryAsync<IdeasoftCategoryMap>(selectQuery, new { @CategoryIds = categoryList });
                return categories.ToList();
            }
        }
        private async Task<int> InsertTaxCategory(int tax)
        {
            var taxCategoryId = await _dapperProductService.InsertTaxCategory(tax);
            return taxCategoryId;

        }
        private async Task PicturesSendToBus(string type, int productId, IList<Image> images)
        {
            var message = new ProductPictureMessageModel
            {
                ProductId = productId,
                ProductPictures = images.Select(s => new Minima.Core.MessageModels.ProductPicture
                {
                    Id = s.Id,
                    DirectoryName = s.DirectoryName,
                    Extension = s.Extension,
                    Filename = s.Filename,
                    Revision = s.Revision,
                    SortOrder = s.SortOrder
                }).ToList()
            };

            try
            {
                var culture = new CultureInfo("en-US");

                await _busProvider.GetInstance().Send(message, $"integration_{type.ToLower(culture)}");
            }
            catch (Exception e)
            {
                _logger.LogError(new EventId(500), e, $"Error on {type} queue messaging");
                throw;
            }
        }
        //private async Task InsertUrlRecord(int newProductId, string slug)
        //{
        //    await WebApiServices.AddUrlRecord(new UrlRecord
        //    {
        //        EntityId = newProductId,
        //        EntityName = "Product",
        //        IsActive = true,
        //        LanguageId = 0,
        //        Slug = slug
        //    });
        //}
        private async Task InsertProductMapping(ProductMessageModel productMessageModel, int newProductId)
        {
            var insertSql =
                "INSERT IdeasoftProductMap (IdeasoftProductId, InternalProductId, CreatedDateUtc, UpdatedDateUtc, LastUpdatedDateUtc)" +
                " VALUES(@ideaProductId, @newProductId, @CreatedDateUtc, @UpdatedDateUtc, SYSDATETIME())";

            using (var conn = new SqlConnection(_databaseSettings.Integration.ConnectionString))
            {
                await conn.ExecuteAsync(insertSql, new
                {
                    @ideaProductId = productMessageModel.Id,
                    @newProductId = newProductId,
                    @UpdatedDateUtc = productMessageModel.UpdatedAt,
                    @CreatedDateUtc = productMessageModel.CreatedAt
                });
            }
        }
        private async Task UpdateProductMapping(ProductMessageModel productMessageModel, int productId)
        {
            var updatetSql =
                "UPDATE IdeasoftProductMap SET UpdatedDateUtc = @UpdatedDateUtc, " +
                "LastUpdatedDateUtc = SYSDATETIME() WHERE IdeasoftProductId = @IdeasoftProductId;";

            using (var conn = new SqlConnection(_databaseSettings.Integration.ConnectionString))
            {
                await conn.ExecuteAsync(updatetSql, new
                {
                    @IdeasoftProductId = productMessageModel.Id,
                    @UpdatedDateUtc = productMessageModel.UpdatedAt,
                });
            }
        }
        private async Task<IdeasoftProductMap> GetProductMapGetById(int productId)
        {
            var selectQuery = "SELECT * FROM IdeasoftProductMap WHERE IdeasoftProductId = @productId";
            using (var conn = new SqlConnection(_databaseSettings.Integration.ConnectionString))
            {
                var product = await conn.QueryFirstOrDefaultAsync<IdeasoftProductMap>(selectQuery, new { productId });
                return product;
            }
        }
        private IdeasoftCategoryMap GetIdeaCategoryById(int categoryId)
        {
            using (var conn = new SqlConnection(_databaseSettings.Integration.ConnectionString))
            {
                var category = conn.QueryFirstOrDefault<IdeasoftCategoryMap>(
                    "SELECT * FROM IdeasoftCategoryMap WHERE IdeaCategoryId = @categoryId", new { categoryId });
                return category;
            }
        }


        private object SelectFileExtension(string mimeType)
        {
            switch (mimeType.ToLower())
            {
                case "image/jpeg":
                    return "jpeg";
                case "image/gif":
                    return "gif";
                case "image/png":
                    return "png";
                default:
                    return mimeType;
            }
        }
        #endregion
    }
}
