﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using Dapper;
using Microsoft.Extensions.Logging;
using Minima.Core.Contants;
using Minima.Core.MessageModels;
using Minima.Integration.Api.Configurations;
using Minima.Integration.Api.Model.Response;
using Minima.Integration.Data.Domain;
using Minima.Logging.Extensions;
using Minima.Nop.Client.Services;
using RestSharp;

namespace Minima.Integration.Api.Services.Impl
{
    public class PictureService : IPictureService
    {
        private readonly IntegrationSettings _integrationSettings;
        private readonly ILogger<PictureService> _logger;
        private readonly DatabaseSettings _databaseSettings;
        private readonly PictureRepository _pictureRepository;
        private readonly NopConfiguration _nopConfiguration;
        private readonly NopApiSettings _nopApiSettings;

        public PictureService(
            IntegrationSettings integrationSettings, 
            ILogger<PictureService> logger, 
            DatabaseSettings databaseSettings, 
            PictureRepository pictureRepository, 
            NopConfiguration nopConfiguration, 
            NopApiSettings nopApiSettings)
        {
            _integrationSettings = integrationSettings;
            _logger = logger;
            _databaseSettings = databaseSettings;
            _pictureRepository = pictureRepository;
            _nopConfiguration = nopConfiguration;
            _nopApiSettings = nopApiSettings;
        }

        public async Task<Response> UpdatePicture(ProductPictureMessageModel message)
        {
            try
            {
                foreach (var item in message.ProductPictures)
                {
                    var pictureMap = await GetPictureMapByPictureId(item.Id);
                    if (pictureMap != null)
                        continue;
                
                    var downloadedFile = await DownloadFileAsync(item, message.ProductId);
                    var mimeType = MimeTypeMap.GetExtension($".{item.Extension}");
                    var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(downloadedFile);
                    var fileExtension = SelectFileExtension(mimeType);
                    
                    var pictureId = await _pictureRepository.InsertPicture(mimeType, fileNameWithoutExtension);
                    var newFileName = $"{pictureId:0000000}_0.{fileExtension}";
                    File.Copy(@downloadedFile, $"{_nopConfiguration.ImagePath}{newFileName}", true);
                    await _pictureRepository.InsertPictureProductMapping(message.ProductId, pictureId, item.SortOrder);
                    await InsertIdeasoftPictureMap(item.Id, pictureId);
                    
                }
                await UpdateProductWithNopApi(message.ProductId);

                return new Response { Code = 200 };
            }
            catch (Exception e)
            {
                _logger.LogError(new EventId(500), e, new { Payload = message });
                return new Response
                {
                    Code = 500,
                    Message = e.Message,
                    InternalMessage = e.InnerException?.Message
                };
            }
        }

        private object SelectFileExtension(string mimeType)
        {
            switch (mimeType.ToLower())
            {
                case "image/jpeg":
                    return "jpeg";
                case "image/gif":
                    return "gif";
                case "image/png":
                    return "png";
                default:
                    return mimeType;
            }
        }

        public async Task UpdatePicture(int productId, List<Image> images)
        {
            try
            {
                foreach (var item in images)
                {
                    var pictureMap = await GetPictureMapByPictureId(item.Id);
                    if (pictureMap != null)
                        continue;

                    var downloadedFile = await DownloadFileAsync(item);
                    var mimeType = MimeTypeMap.GetExtension($".{item.Extension}");
                    var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(downloadedFile);
                    var fileExtension = SelectFileExtension(mimeType);

                    var pictureId = await _pictureRepository.InsertPicture(mimeType, fileNameWithoutExtension);
                    var newFileName = $"{pictureId:0000000}_0.{fileExtension}";
                    File.Copy(@downloadedFile, $"{_nopConfiguration.ImagePath}{newFileName}",true);
                    await _pictureRepository.InsertPictureProductMapping(productId, pictureId, item.SortOrder);
                    

                    await InsertIdeasoftPictureMap(item.Id, pictureId);

                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
            }
        }

        #region private methods
        private async Task<string> DownloadFileAsync(ProductPicture image, int productId)
        {
            try
            {
                var downloadUrl =
                    $"{_integrationSettings.ImageBaseUrl}products/{image.DirectoryName}/{image.Filename}.{image.Extension}?revision={image.Revision}";
                
                var productImageFolder =
                    $"{_integrationSettings.DownloadPath}product_images\\{productId:0000000}";
                
                if (!Directory.Exists(productImageFolder))
                    Directory.CreateDirectory(productImageFolder);


                string downloadToDirectory = $"{_integrationSettings.DownloadPath}product_images\\{productId:0000000}\\{image.Filename}.{image.Extension}";

                if (File.Exists(downloadToDirectory))
                {
                    return @downloadToDirectory;
                }

                using (WebClient webClient = new WebClient())
                {
                    webClient.Credentials = CredentialCache.DefaultNetworkCredentials;
                    await webClient.DownloadFileTaskAsync(new Uri(downloadUrl), @downloadToDirectory);
                    return @downloadToDirectory;
                }
            }

            catch (Exception e)
            {
                _logger.LogError("Failed to download File: " + image.Filename, e);
            }

            return string.Empty;
        }

        private async Task<string> DownloadFileAsync(Image image)
        {
            try
            {
                var downloadUrl =
                    $"{_integrationSettings.ImageBaseUrl}products/{image.DirectoryName}/{image.Filename}.{image.Extension}?{image.Revision}";
                string downloadToDirectory = $"{_integrationSettings.DownloadPath}product_images\\{image.Filename}.{image.Extension}";

                if (File.Exists(downloadToDirectory))
                {
                    return @downloadToDirectory;
                }

                using (WebClient webClient = new WebClient())
                {
                    webClient.Credentials = CredentialCache.DefaultNetworkCredentials;
                    await webClient.DownloadFileTaskAsync(new Uri(downloadUrl), @downloadToDirectory);
                    return @downloadToDirectory;
                }
            }

            catch (Exception e)
            {
                _logger.LogError("Failed to download File: " + image.Filename);
            }

            return string.Empty;
        }

        private async Task<IdeasoftPictureMap> GetPictureMapByPictureId(int pictureId)
        {
            var sqlCommand = "SELECT * FROM IdeasoftPictureMap WHERE IdeaPictureId = @IdeaPictureId";
            using (var conn = new SqlConnection(_databaseSettings.Integration.ConnectionString))
            {
                var result = await conn.QueryFirstOrDefaultAsync<IdeasoftPictureMap>(sqlCommand, new { IdeaPictureId = pictureId });
                return result;
            }
        }

        private async Task InsertIdeasoftPictureMap(int itemId, int pictureId)
        {
            var sqlCommand =
                "INSERT IdeasoftPictureMap (IdeaPictureId, InternalPictureId, CreatedDateUtc, UpdatedDateUtc, LastUpdatedDateUtc) " +
                "VALUES(@IdeaPictureId, @InternalPictureId, @CreatedDateUtc, @UpdatedDateUtc, @LastUpdatedDateUtc);";

            using (var conn = new SqlConnection(_databaseSettings.Integration.ConnectionString))
            {
                await conn.ExecuteAsync(sqlCommand, new
                {
                    @IdeaPictureId = itemId,
                    @InternalPictureId = pictureId,
                    @CreatedDateUtc = DateTime.Now,
                    @UpdatedDateUtc = DateTime.Now,
                    @LastUpdatedDateUtc = DateTime.Now,
                });
            }
        }

        private async Task UpdateProductWithNopApi(int productId)
        {
            try
            {
                var client = new RestClient($"{_nopApiSettings.Url}/api/products/{productId}");
                var request = new RestRequest(Method.PUT);
                var json = new { product = new { id = productId } };
                request.AddJsonBody(json);
                var response = await client.ExecuteAsync(request);
            }
            catch (Exception e)
            {
                _logger.LogError(new EventId(500), e.Message, e);
            }
        }

        //private async Task InsertIdeasoftPictureMap(Image item, PictureModel pictureModel)
        //{
        //    var sqlCommand =
        //        "INSERT IdeasoftPictureMap (IdeaPictureId, InternalPictureId, CreatedDateUtc, UpdatedDateUtc, LastUpdatedDateUtc) " +
        //        "VALUES(@IdeaPictureId, @InternalPictureId, @CreatedDateUtc, @UpdatedDateUtc, @LastUpdatedDateUtc);";

        //    using (var conn = new SqlConnection(_databaseSettings.Integration.ConnectionString))
        //    {
        //        await conn.ExecuteAsync(sqlCommand, new
        //        {
        //            @IdeaPictureId = item.Id,
        //            @InternalPictureId = pictureModel.Id,
        //            @CreatedDateUtc = DateTime.Now,
        //            @UpdatedDateUtc = DateTime.Now,
        //            @LastUpdatedDateUtc = DateTime.Now,
        //        });
        //    }
        //}

        #endregion


    }
}