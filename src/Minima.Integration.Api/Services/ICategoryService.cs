﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Minima.Core.MessageModels;

namespace Minima.Integration.Api.Services
{
    public interface ICategoryService
    {
        Task ImportCategories(List<CategoryMessageModel> categoryInModels);
        Task<int> GetMaxCategoryId();
    }
}