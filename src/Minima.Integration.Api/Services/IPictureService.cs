﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Minima.Core.MessageModels;
using Minima.Integration.Api.Model.Response;

namespace Minima.Integration.Api.Services
{
    public interface IPictureService
    {
        Task<Response> UpdatePicture(ProductPictureMessageModel contextMessage);
        Task UpdatePicture(int productId, List<Image> images);
    }
}