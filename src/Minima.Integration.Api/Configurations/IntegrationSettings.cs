﻿using Padawan.Attributes;

namespace Minima.Integration.Api.Configurations
{
    [Configuration("IntegrationSettings")]
    public class IntegrationSettings
    {
        public string ImageBaseUrl { get; set; }
        public string DownloadPath { get; set; }
    }
}