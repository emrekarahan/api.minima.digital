﻿using Padawan.Attributes;

namespace Minima.Integration.Api.Configurations
{
    [Configuration("NopConfiguration")]
    public class NopConfiguration
    {
        public string ImagePath { get; set; }
    }
}