﻿using Padawan.Attributes;

namespace Minima.Integration.Api.Configurations
{
    [Configuration("NopApiSettings")]
    public class NopApiSettings
    {
        public string Url { get; set; }
    }
}