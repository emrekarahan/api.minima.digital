﻿using Padawan.Attributes;

namespace Minima.Integration.Api.Configurations
{
    [Configuration("GittigidiyorIntegrationDbSettings")]
    public class GittigidiyorIntegrationDbSettings
    {
        public string ConnectionString { get; set; }
    }
}
