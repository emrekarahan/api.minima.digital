﻿using Padawan.Attributes;

namespace Minima.Integration.Api.Configurations
{
    [Configuration("DatabaseSettings")]
    public class DatabaseSettings
    {
        public DatabaseConfiguration MongoDb { get; set; }
        public DatabaseConfiguration Integration { get; set; }
    }

    public class DatabaseConfiguration
    {
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }

    [Configuration("Ideasoft")]
    public class IdeasoftConfiguration
    {
        public string AuthorizationUrl { get; set; }
        public string TokenUrl { get; set; }
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
        public string RedirectUri { get; set; }
        public string ApiUrl { get; set; }
    }


}
