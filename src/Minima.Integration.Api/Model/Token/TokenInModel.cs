﻿using System.ComponentModel.DataAnnotations;

namespace Minima.Integration.Api.Model.Token
{
    public class TokenInModel
    {

        [Required(ErrorMessage = "UserName is required")]
        public string Username { get; set; }

        [Required(ErrorMessage = "PassWord is required")]
        public string Password { get; set; }
    }

    public class RefreshTokenInModel
    {

        [Required(ErrorMessage = "PassWord is required")]
        public string RefreshToken { get; set; }
    }


}
