﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Minima.Integration.Api.Model.Response.NopCommerce
{
    public class BillingAddress
    {

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("first_name")]
        public string FirstName { get; set; }

        [JsonProperty("last_name")]
        public string LastName { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("company")]
        public string Company { get; set; }

        [JsonProperty("country_id")]
        public int CountryId { get; set; }

        [JsonProperty("country")]
        public string Country { get; set; }

        [JsonProperty("state_province_id")]
        public int StateProvinceId { get; set; }

        [JsonProperty("city")]
        public string City { get; set; }

        [JsonProperty("address1")]
        public string Address1 { get; set; }

        [JsonProperty("address2")]
        public string Address2 { get; set; }

        [JsonProperty("zip_postal_code")]
        public string ZipPostalCode { get; set; }

        [JsonProperty("phone_number")]
        public string PhoneNumber { get; set; }

        [JsonProperty("fax_number")]
        public string FaxNumber { get; set; }

        [JsonProperty("customer_attributes")]
        public object CustomerAttributes { get; set; }

        [JsonProperty("created_on_utc")]
        public DateTime CreatedOnUtc { get; set; }

        [JsonProperty("province")]
        public string Province { get; set; }
    }

    public class ShippingAddress
    {

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("first_name")]
        public string FirstName { get; set; }

        [JsonProperty("last_name")]
        public string LastName { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("company")]
        public string Company { get; set; }

        [JsonProperty("country_id")]
        public int CountryId { get; set; }

        [JsonProperty("country")]
        public string Country { get; set; }

        [JsonProperty("state_province_id")]
        public int StateProvinceId { get; set; }

        [JsonProperty("city")]
        public string City { get; set; }

        [JsonProperty("address1")]
        public string Address1 { get; set; }

        [JsonProperty("address2")]
        public string Address2 { get; set; }

        [JsonProperty("zip_postal_code")]
        public string ZipPostalCode { get; set; }

        [JsonProperty("phone_number")]
        public string PhoneNumber { get; set; }

        [JsonProperty("fax_number")]
        public string FaxNumber { get; set; }

        [JsonProperty("customer_attributes")]
        public object CustomerAttributes { get; set; }

        [JsonProperty("created_on_utc")]
        public DateTime CreatedOnUtc { get; set; }

        [JsonProperty("province")]
        public string Province { get; set; }
    }

    public class Address
    {

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("first_name")]
        public string FirstName { get; set; }

        [JsonProperty("last_name")]
        public string LastName { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("company")]
        public string Company { get; set; }

        [JsonProperty("country_id")]
        public int CountryId { get; set; }

        [JsonProperty("country")]
        public string Country { get; set; }

        [JsonProperty("state_province_id")]
        public int StateProvinceId { get; set; }

        [JsonProperty("city")]
        public string City { get; set; }

        [JsonProperty("address1")]
        public string Address1 { get; set; }

        [JsonProperty("address2")]
        public string Address2 { get; set; }

        [JsonProperty("zip_postal_code")]
        public string ZipPostalCode { get; set; }

        [JsonProperty("phone_number")]
        public string PhoneNumber { get; set; }

        [JsonProperty("fax_number")]
        public string FaxNumber { get; set; }

        [JsonProperty("customer_attributes")]
        public object CustomerAttributes { get; set; }

        [JsonProperty("created_on_utc")]
        public DateTime CreatedOnUtc { get; set; }

        [JsonProperty("province")]
        public string Province { get; set; }
    }

    public class Customer
    {
        [JsonProperty("billing_address")]
        public BillingAddress BillingAddress { get; set; }

        [JsonProperty("shipping_address")]
        public ShippingAddress ShippingAddress { get; set; }

        [JsonProperty("addresses")]
        public IList<Address> Addresses { get; set; }

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("username")]
        public string Username { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("first_name")]
        public string FirstName { get; set; }

        [JsonProperty("last_name")]
        public string LastName { get; set; }

        [JsonProperty("admin_comment")]
        public object AdminComment { get; set; }

        [JsonProperty("is_tax_exempt")]
        public bool IsTaxExempt { get; set; }

        [JsonProperty("has_shopping_cart_items")]
        public bool HasShoppingCartItems { get; set; }

        [JsonProperty("active")]
        public bool Active { get; set; }

        [JsonProperty("deleted")]
        public bool Deleted { get; set; }

        [JsonProperty("is_system_account")]
        public bool IsSystemAccount { get; set; }

        [JsonProperty("system_name")]
        public object SystemName { get; set; }

        [JsonProperty("last_ip_address")]
        public string LastIpAddress { get; set; }

        [JsonProperty("created_on_utc")]
        public DateTime CreatedOnUtc { get; set; }

        [JsonProperty("last_login_date_utc")]
        public DateTime LastLoginDateUtc { get; set; }

        [JsonProperty("last_activity_date_utc")]
        public DateTime LastActivityDateUtc { get; set; }

        [JsonProperty("role_ids")]
        public IList<object> RoleIds { get; set; }
    }

    public class CustomerResponse
    {

        [JsonProperty("customers")]
        public IList<Customer> Customers { get; set; }
    }


}