﻿using System.Xml.Serialization;

namespace Minima.Integration.Api.Model.Gittigidiyor.Requests
{
    [XmlRoot("getProducts")]
    public class GetProduct : BaseRequest
    {
        [XmlElement(ElementName = "startOffSet")]
        public string StartOffSet { get; set; }
        [XmlElement(ElementName = "rowCount")]
        public string RowCount { get; set; }
        [XmlElement(ElementName = "status")]
        public string Status { get; set; }
        [XmlElement(ElementName = "withData")]
        public string WithData { get; set; }
    }


}