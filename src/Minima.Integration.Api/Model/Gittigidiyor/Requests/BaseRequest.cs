﻿using System.Xml.Serialization;

namespace Minima.Integration.Api.Model.Gittigidiyor.Requests
{
    public class BaseRequest
    {
        [XmlElement(ElementName = "apiKey")]
        public string ApiKey { get; set; }
        [XmlElement(ElementName = "sign")]
        public string Sign { get; set; }
        [XmlElement(ElementName = "time")]
        public long Time { get; set; }
        [XmlElement(ElementName = "lang")]
        public string Lang { get; set; }
    }
}