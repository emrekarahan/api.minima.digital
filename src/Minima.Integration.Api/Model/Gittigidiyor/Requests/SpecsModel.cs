﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Minima.Integration.Api.Model.Gittigidiyor.Requests
{
    public class Value
    {

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("specId")]
        public int SpecId { get; set; }

        [JsonProperty("specIdSpecified")]
        public bool SpecIdSpecified { get; set; }

        [JsonProperty("parentSpecId")]
        public int ParentSpecId { get; set; }

        [JsonProperty("parentSpecIdSpecified")]
        public bool ParentSpecIdSpecified { get; set; }
    }

    public class SpecsModel
    {

        [JsonProperty("values")]
        public IList<Value> Values { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("required")]
        public bool Required { get; set; }

        [JsonProperty("requiredSpecified")]
        public bool RequiredSpecified { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("specId")]
        public int SpecId { get; set; }

        [JsonProperty("specIdSpecified")]
        public bool SpecIdSpecified { get; set; }

        [JsonProperty("childSpecId")]
        public int ChildSpecId { get; set; }

        [JsonProperty("childSpecIdSpecified")]
        public bool ChildSpecIdSpecified { get; set; }
    }
}
