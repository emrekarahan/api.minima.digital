﻿using System;
using System.Collections.Generic;
using Minima.Core.MessageModels;

namespace Minima.Integration.Api.Model.Product
{
    public class ProductMessageModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Slug { get; set; }
        public string FullName { get; set; }
        public string Sku { get; set; }
        public string Barcode { get; set; }
        public double Price1 { get; set; }
        public int Warranty { get; set; }
        public int Tax { get; set; }
        public double StockAmount { get; set; }
        public double VolumetricWeight { get; set; }
        public double BuyingPrice { get; set; }
        public string StockTypeLabel { get; set; }
        public double Discount { get; set; }
        public int DiscountType { get; set; }
        public double MoneyOrderDiscount { get; set; }
        public int Status { get; set; }
        public int TaxIncluded { get; set; }
        public string Distributor { get; set; }
        public int IsGifted { get; set; }
        public string Gift { get; set; }
        public int CustomShippingDisabled { get; set; }
        public double CustomShippingCost { get; set; }
        public string MarketPriceDetail { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public string MetaKeywords { get; set; }
        public string MetaDescription { get; set; }
        public string PageTitle { get; set; }
        public int HasOption { get; set; }
        public string ShortDetails { get; set; }
        public string SearchKeywords { get; set; }
        public string InstallmentThreshold { get; set; }
        public int HomeSortOrder { get; set; }
        public int PopularSortOrder { get; set; }
        public int BrandSortOrder { get; set; }
        public int FeaturedSortOrder { get; set; }
        public int CampaignedSortOrder { get; set; }
        public int NewSortOrder { get; set; }
        public int DiscountedSortOrder { get; set; }
        public object Brand { get; set; }
        public Currency Currency { get; set; }
        public object Parent { get; set; }
        public object Countdown { get; set; }
        public IList<object> Prices { get; set; }
        public IList<Image> Images { get; set; }
        public IList<Detail> Details { get; set; }
        public IList<ProductToCategory> ProductToCategories { get; set; }
    }
}