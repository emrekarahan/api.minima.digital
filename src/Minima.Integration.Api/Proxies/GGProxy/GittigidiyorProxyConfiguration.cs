﻿using Padawan.Attributes;

namespace Minima.Integration.Api.Proxies.GGProxy
{
    [Configuration("GittigidiyorProxyConfiguration")]
    public class GittigidiyorProxyConfiguration
    {
        public string ApiUrl { get; set; }
    }
}