﻿//using System;
//using System.Threading.Tasks;
//using Microsoft.Extensions.Logging;
//using Minima.Integration.Api.Domain.Product;
//using Minima.Integration.Api.Model.Response.Gittigidiyor;
//using Minima.Nop.Client.Domain.Product;
//using Padawan.Attributes;
//using RestSharp;
//using Minima.Logging.Extensions;

//namespace Minima.Integration.Api.Proxies.GGProxy
//{
//    [Singleton]
//    public class GittigidiyorProxyClient
//    {
//        private readonly GittigidiyorProxyConfiguration _configuration;
//        private readonly ILogger<GittigidiyorProxyClient> _logger;

//        public GittigidiyorProxyClient(
//            GittigidiyorProxyConfiguration gittigidiyorProxyConfiguration,
//            ILogger<GittigidiyorProxyClient> logger)
//        {
//            _configuration = gittigidiyorProxyConfiguration;
//            _logger = logger;
//        }

//        public async Task<ProductServiceResponse> InsertProduct(ProductDto productDto)
//        {
//            try
//            {
//                var client = new RestClient($"{_configuration.ApiUrl}/api/Product/InsertProduct");

//                var request = new RestRequest(Method.POST);
//                request.AddJsonBody(productDto);

//                var response = await client.ExecuteAsync<ProductServiceResponse>(request);

//                if (!response.IsSuccessful)
//                    _logger.LogError(new EventId(500), "Response is not successful", new { ProductId = productDto.Id },
//                            "Product");

//                if (response.Data.AckCode == "success") return response.Data;

//                _logger.LogError(new EventId(500), new Exception(), "Response is not successful", extraData:new { response.Data }, "Product");
//                return response.Data;

//            }
//            catch (Exception e)
//            {
//                _logger.LogError(new EventId(666), "Response is not successful", new
//                {
//                    ProductId = productDto.Id,
//                    IsSuccess = false,
//                    Message = "failed",
//                    Task = "UpdateProduct"
//                }, "Product");
//                return new ProductServiceResponse()
//                {
//                    AckCode = "failed",
//                    ProductId = productDto.Id,
//                    Exception = new ProductServiceResponse.Exceptions
//                    {
//                        Message = e.Message
//                    }
//                };
//            }
//        }



//        public async Task<ProductServiceResponse> UpdatePrice(ProductDto productDto)
//        {
//            try
//            {
//                var client = new RestClient($"{_configuration.ApiUrl}/api/Product/UpdatePrice");

//                var request = new RestRequest(Method.POST);
//                request.AddJsonBody(productDto);

//                var response = await client.ExecuteAsync<ProductServiceResponse>(request);

//                if (!response.IsSuccessful)
//                    _logger.LogError(new EventId(500), "Response is not successful", new { ProductId = productDto.Id },
//                        "Product");

//                if (response.Data.AckCode == "success") return response.Data;

//                _logger.LogError(new EventId(500), new Exception(), "Response is not successful", extraData: new { response.Data }, "Product");
//                return response.Data;

//            }
//            catch (Exception e)
//            {
//                _logger.LogError(new EventId(666), "Response is not successful", new
//                {
//                    ProductId = productDto.Id,
//                    IsSuccess = false,
//                    Message = "failed",
//                    Task = "UpdateProduct"
//                }, "Product");
//                return new ProductServiceResponse()
//                {
//                    AckCode = "failed",
//                    ProductId = productDto.Id,
//                    Exception = new ProductServiceResponse.Exceptions
//                    {
//                        Message = e.Message
//                    }
//                };
//            }
//        }

//        public async Task<ProductServiceResponse> UpdateStock(ProductDto productDto)
//        {
//            try
//            {
//                var client = new RestClient($"{_configuration.ApiUrl}/api/Product/UpdateStock");

//                var request = new RestRequest(Method.POST);
//                request.AddJsonBody(productDto);

//                var response = await client.ExecuteAsync<ProductServiceResponse>(request);

//                if (!response.IsSuccessful)
//                    _logger.LogError(new EventId(500), "Response is not successful", new { ProductId = productDto.Id },
//                        "Product");

//                if (response.Data.AckCode == "success") return response.Data;

//                _logger.LogError(new EventId(500), new Exception(), "Response is not successful", extraData: new { response.Data }, "Product");
//                return response.Data;

//            }
//            catch (Exception e)
//            {
//                _logger.LogError(new EventId(666), "Response is not successful", new
//                {
//                    ProductId = productDto.Id,
//                    IsSuccess = false,
//                    Message = "failed",
//                    Task = "UpdateProduct"
//                }, "Product");
//                return new ProductServiceResponse()
//                {
//                    AckCode = "failed",
//                    ProductId = productDto.Id,
//                    Exception = new ProductServiceResponse.Exceptions
//                    {
//                        Message = e.Message
//                    }
//                };
//            }
//        }

//        public async Task<ProductServiceResponse> UpdateProductStock(int gittigidiyorId, int stock)
//        {
//            try
//            {
//                var client = new RestClient($"{_configuration.ApiUrl}/api/Product/UpdateProductStock");

//                var request = new RestRequest(Method.GET);
//                request.AddParameter("gittigidiyorProductId", gittigidiyorId);
//                request.AddParameter("stockQuantity", stock);

//                var response = await client.ExecuteAsync<ProductServiceResponse>(request);

//                if (!response.IsSuccessful)
//                    _logger.LogError(new EventId(500), "Response is not successful",
//                        "Product");

//                if (response.Data.AckCode == "success") return response.Data;

//                _logger.LogError(new EventId(500), new Exception(), "Response is not successful", extraData: new { response.Data }, "Product");
//                return response.Data;

//            }
//            catch (Exception e)
//            {
//                _logger.LogError(new EventId(666), "Response is not successful", new
//                {
//                    IsSuccess = false,
//                    Message = "failed",
//                    Task = "UpdateProduct"
//                }, "Product");
//                return new ProductServiceResponse()
//                {
//                    AckCode = "failed",
//                    Exception = new ProductServiceResponse.Exceptions
//                    {
//                        Message = e.Message
//                    }
//                };
//            }
//        }
//    }
//}