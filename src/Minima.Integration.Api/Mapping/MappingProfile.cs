﻿using AutoMapper;
using Minima.Ideasoft.Client.MongoDb;
using Minima.Integration.Api.Domain.Product;
using Minima.Nop.Client.Domain.Product;

namespace Minima.Integration.Api.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {

            CreateMap<Product, ProductDto>()
                .ForMember(f => f.Id, map => map.MapFrom(f => f.ProductId));
            
            CreateMap<Domain.Product.Category, CategoryDto>();
            CreateMap<GittigidiyorCategory, GittigidiyorCategoryDto>();
            CreateMap<Picture, PictureDto>();
            CreateMap<Spec, SpecDto>();

            CreateMap<ProductDto, Product>()
                .ForMember(f => f.ProductId, map => map.MapFrom(f => f.Id))
                .ForMember(f => f.Id, f => f.Ignore())
                .ForMember(f => f.IsSyncable, f => f.Ignore());

            CreateMap<CategoryDto, Domain.Product.Category>();
            CreateMap<GittigidiyorCategoryDto, GittigidiyorCategory>();
            CreateMap<PictureDto, Picture>();
            CreateMap<SpecDto, Spec>();

        }
    }
}