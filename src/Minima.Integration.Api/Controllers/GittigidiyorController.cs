﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Minima.Integration.Api.Model.Gittigidiyor.Requests;
using Minima.Integration.Api.Proxies.GGProxy;
using Minima.Integration.Api.Services;
using Minima.Integration.Api.Services.Impl;

namespace Minima.Integration.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    public class GittigidiyorController : ControllerBase
    {
        //private readonly GittigidiyorProxyClient _gittigidiyorProxyClient;
        private readonly IProductService _productService;
        private readonly SpecsService _specsService;

        public GittigidiyorController(
            //GittigidiyorProxyClient gittigidiyorProxyClient, 
            IProductService productService, SpecsService specsService)
        {
            _productService = productService;
            _specsService = specsService;
        }

        [HttpGet]
        [Route("InsertProduct")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> InsertProduct(int productId)
        {
            var result = await _productService.ImportMongoDb(productId);
            return Ok(result);

        }


        [HttpPost]
        [Route("InsertAvaliableProducts")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task InsertAvaliableProducts()
        {
            await _productService.InsertAvaliableProducts();
        }

        [HttpPost]
        [Route("InsertAvaliableMongoProducts")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task InsertAvaliableMongoProducts()
        {
            await _productService.InsertAvaliableProducts();
        }

        [HttpGet]
        [Route("UpdateProductSyncStatus")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> UpdateProductSyncStatus(int productId, int syncedProductId)
        {
            await _productService.UpdateProductMongoDb(productId, syncedProductId);
            return Ok();
        }

        [HttpPost]
        [Route("UpdateCategorySpecs")]
        [Consumes("application/json")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> UpdateCategorySpecs(List<SpecsModel> specs)
        {
            var code = this.Request.Headers["code"].FirstOrDefault();
            var categoryId = await _specsService.GetCategoryByCode(code);

            if (categoryId == 0)
                return BadRequest();

            await _specsService.UpdateCategorySpecs(categoryId, specs);

            return Ok();
        }

        [HttpPost]
        [Route("GetListingReadyProducts")]
        [Consumes("application/json")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public IActionResult GetListingReadyProducts()
        {
            var result = _productService.GetListingReadyProducts();
            return Ok(result);
        }

        
        [HttpPost]
        [Route("UpdateProductStatus")]
        [Consumes("application/json")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> UpdateProductStatus(List<int> products)
        {
            var status = Request.Headers["status"].FirstOrDefault();

            await _productService.UpdateProductStatus(products, status);
            return Ok();
        }

        
        [HttpGet]
        [Route("UpdateProductError")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> UpdateProductError(int productId, string errorMessage)
        {
            await _productService.UpdateProductError(productId, errorMessage);
            return Ok();
        }


    }
}