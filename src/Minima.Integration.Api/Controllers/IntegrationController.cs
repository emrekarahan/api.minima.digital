﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Products.Requests;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Products.Types;
using Falcon.Package.MarketPlace.Gittigidiyor.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.ResponseCaching.Internal;
using Microsoft.Extensions.Logging;
using Minima.Core.Models.Request;
using Minima.Data.MongoDb.Repository;
using Minima.Ideasoft.Client;
using Minima.Integration.Api.Domain.Product;
using Minima.Integration.Api.Filters;
using Minima.Integration.Api.Proxies.GGProxy;
using Minima.Integration.Api.Services;
using Minima.Logging.Extensions;
using Minima.Nop.Client.Domain.Product;
using MongoDB.Driver;
using Newtonsoft.Json;
using RestSharp;

namespace Minima.Integration.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    //[Produces("application/json")]
    public class IntegrationController : ControllerBase
    {
        private ILogger<IntegrationController> _logger;
        private readonly IProductClient _productClient;
        private readonly ICategoryClient _categoryClient;
        private readonly IProductService _productService;
        private IMongoRepository<Sales> _salesRepository;
        private IMongoRepository<IdeaSales> _ideaSalesRepository;
        private IMongoRepository<Product> _prodMongoRepository;
        private IOrderClient _orderClient;
        //private GittigidiyorProxyClient _gittigidiyorProxyClient;
        private IMapper _mapper;
        private IProductApiService _productApiService;

        public IntegrationController(
            ILogger<IntegrationController>
            logger,
            IProductClient productClient,
            ICategoryClient categoryClient,
            IProductService productService,
            IMongoRepository<Sales> salesRepository,
            IOrderClient orderClient,
            IMongoRepository<IdeaSales> ideaSalesRepository,
            IMongoRepository<Product> prodMongoRepository,
            IMapper mapper, IProductApiService productApiService)
        {
            _logger = logger;
            _productClient = productClient;
            _categoryClient = categoryClient;
            _productService = productService;
            _salesRepository = salesRepository;
            _orderClient = orderClient;
            _ideaSalesRepository = ideaSalesRepository;
            _prodMongoRepository = prodMongoRepository;
            _mapper = mapper;
            _productApiService = productApiService;
        }

        [ServiceFilter(typeof(TokenFilter))]
        [HttpPost("category")]
        public async Task<IActionResult> Category([FromBody] CategoryRequestModel categoryRequestModel)
        {
            var response = await _categoryClient.GetCategoryList(categoryRequestModel);
            return Ok(response);
        }

        [ServiceFilter(typeof(TokenFilter))]
        [HttpPost("new-categories")]
        public async Task<IActionResult> UpdatedCategory([FromBody] CategoryRequestModel categoryRequestModel)
        {
            await _categoryClient.GetNewCategories(categoryRequestModel);
            return Ok();
        }

        [ServiceFilter(typeof(TokenFilter))]
        [HttpPost("product")]
        public async Task<IActionResult> Product([FromBody] ProductRequestModel productRequestModel)
        {
            var response = await _productClient.GetProductList(productRequestModel);
            return Ok(response);
        }

        [ServiceFilter(typeof(TokenFilter))]
        [HttpPost("product-stock")]
        public async Task<IActionResult> ProductStock([FromBody] ProductRequestModel productRequestModel)
        {
            var response = await _productClient.GetProductListStock(productRequestModel);
            return Ok(response);
        }

        [ServiceFilter(typeof(TokenFilter))]
        [HttpPost("new-products")]
        public async Task NewProducts([FromBody] ProductRequestModel productRequestModel)
        {
            _logger.LogInformation("Güncellenen Ürünler güncelleniyor.", extraData: productRequestModel, "new-products");
            await _productClient.GetNewProducts(productRequestModel);
            //return Ok();
        }

        [ServiceFilter(typeof(TokenFilter))]
        [HttpGet("order")]
        public async Task<IActionResult> Order()
        {
            //var response = await _ideasoftClient.GetProductList();
            //return Ok(response);
            return Ok();
        }


        [ServiceFilter(typeof(TokenFilter))]
        [HttpGet("update-stock")]
        public async Task UpdateStock(string salesCode, int productId, int stockAmount)
        {

            var sale = _salesRepository.FilterBy(f => f.SalesCode == salesCode && f.ProductId == productId).FirstOrDefault();
            if (sale != null)
                return;

            int ideaProductId = await _productService.GetIdeaProductId(productId);
            var ideaStockAmount = _productClient.GetProductDetail(ideaProductId);
            if (ideaStockAmount == null)
                return;
            await _productClient.UpdateStock(ideaProductId, Convert.ToInt32(ideaStockAmount.Value - stockAmount));

            await _salesRepository.InsertOneAsync(new Sales
            {
                Amount = stockAmount,
                ProductId = productId,
                SalesCode = salesCode
            });


            //await _productClient.UpdateStock(productId, stock);
            //return Ok();
        }

        [ServiceFilter(typeof(TokenFilter))]
        [HttpGet("UpdateGittigidiyorStock")]
        public async Task<IActionResult> UpdateGittigidiyorStock(DateTime? startDate, DateTime? endDate, string status = "approved", string paymentStatus = "success")
        {
            IRestResponse response = null;
            try
            {
                response = await _orderClient.GetOrderList(startDate, endDate, status, paymentStatus);
                if (response == null || string.IsNullOrEmpty(response.Content))
                {
                    return BadRequest();
                }
                var orders = JsonConvert.DeserializeObject<List<Order>>(response.Content);
                _logger.LogInformation("Orderlar güncelleniyor.", extraData: new { startDate, endDate });
                foreach (var order in orders)
                {
                    if (order.Status != "approved" && order.Status != "fulfilled" && order.Status != "delivered")
                    {
                        _logger.LogInformation($"Order pas geçildi- {order.Id}, status : {order.Status}", extraData: new
                        {
                            orderId = order.Id,
                            orderStatus = order.Status
                        });
                        continue;
                    }


                    foreach (var orderItem in order.OrderItems)
                    {
                        var ideaSales = _ideaSalesRepository.FilterBy(f => f.OrderId == order.Id && f.OrderItemId == orderItem.Id && f.ProductId == orderItem.Product.Id).FirstOrDefault();
                        if (ideaSales != null)
                            continue;

                        int internalProductId = await _productService.GetIdeaProductId(orderItem.Product.Id);
                        var ideaStockAmount = _productClient.GetProductDetail(orderItem.Product.Id);
                        var gittigidiyorProdut = _prodMongoRepository.FilterBy(f => f.ProductId == internalProductId)
                            .FirstOrDefault();
                        if (gittigidiyorProdut == null)
                            continue;

                        if (ideaStockAmount == null)
                            continue;


                        gittigidiyorProdut.StockQuantity = (int)ideaStockAmount;
                        await _prodMongoRepository.ReplaceOneAsync(gittigidiyorProdut);

                        await _productService.UpdateProductStock(internalProductId, (int)ideaStockAmount);

                        if (gittigidiyorProdut.GittigidiyorProductId == null)
                            continue;
                        _productApiService.UpdateStock(new UpdateStockRequestReturn
                        {
                            Lang = "tr",
                            ProductId = gittigidiyorProdut.GittigidiyorProductId.ToString(),
                            Stock = (int)ideaStockAmount

                        });
                        // _gittigidiyorProxyClient.UpdateProductStock(gittigidiyorProdut.GittigidiyorProductId.Value, (int)ideaStockAmount);

                        await _ideaSalesRepository.InsertOneAsync(new IdeaSales
                        {
                            OrderId = order.Id,
                            OrderItemId = orderItem.Id,
                            ProductId = orderItem.Product.Id,
                            GittigidiyorProductId = gittigidiyorProdut.GittigidiyorProductId,
                            InternalProductId = internalProductId,
                            StockQuantity = ideaStockAmount,
                            OrderDate = order.CreatedAt,
                            PaymentStatus = order.PaymentStatus
                        });

                    }
                }
                return Ok(response.Content);
            }
            catch (Exception e)
            {

                _logger.LogError(e, e.Message);
                if (response != null)
                    return Ok(response.Content);
                return null;
            }
        }


        [HttpPost("UpdateGittigidiyorStock2")]
        public async Task<IActionResult> UpdateGittigidiyorStock2(List<Order> orders, string status = "approved", string paymentStatus = "success")
        {
            try
            {

                foreach (var order in orders)
                {
                    if (order.Status != "approved" && order.Status != "fulfilled" && order.Status != "delivered")
                        continue;

                    foreach (var orderItem in order.OrderItems)
                    {
                        var ideaSales = _ideaSalesRepository.FilterBy(f => f.OrderId == order.Id && f.OrderItemId == orderItem.Id && f.ProductId == orderItem.Product.Id).FirstOrDefault();
                        if (ideaSales != null)
                            continue;

                        int internalProductId = await _productService.GetIdeaProductId(orderItem.Product.Id);
                        var ideaStockAmount = _productClient.GetProductDetail(orderItem.Product.Id);
                        var gittigidiyorProdut = _prodMongoRepository.FilterBy(f => f.ProductId == internalProductId)
                            .FirstOrDefault();
                        if (gittigidiyorProdut == null)
                            continue;

                        if (ideaStockAmount == null)
                            continue;

                        gittigidiyorProdut.StockQuantity = (int)ideaStockAmount;
                        await _prodMongoRepository.ReplaceOneAsync(gittigidiyorProdut);

                        await _productService.UpdateProductStock(internalProductId, (int)ideaStockAmount);

                        if (gittigidiyorProdut.GittigidiyorProductId == null)
                            continue;

                        _productApiService.UpdateStock(new UpdateStockRequestReturn
                        {
                            Lang = "tr",
                            ProductId = gittigidiyorProdut.GittigidiyorProductId.ToString(),
                            Stock = (int)ideaStockAmount

                        });

                        //await _gittigidiyorProxyClient.UpdateProductStock(gittigidiyorProdut.GittigidiyorProductId.Value, (int)ideaStockAmount);

                        await _ideaSalesRepository.InsertOneAsync(new IdeaSales
                        {
                            OrderId = order.Id,
                            OrderItemId = orderItem.Id,
                            ProductId = orderItem.Product.Id,
                            GittigidiyorProductId = gittigidiyorProdut.GittigidiyorProductId,
                            InternalProductId = internalProductId,
                            StockQuantity = ideaStockAmount,
                            OrderDate = order.CreatedAt,
                            PaymentStatus = order.PaymentStatus
                        });

                    }
                }


                return Ok(orders);
            }
            catch (Exception e)
            {

                _logger.LogError(e, e.Message);
            }

            return null;
        }

        [ServiceFilter(typeof(TokenFilter))]
        [HttpGet("InsertMissingProducts")]
        public async Task InsertMissingProducts()
        {
            var missingProducts = _prodMongoRepository.FilterBy(f => string.IsNullOrEmpty(f.Status) && !f.Synced)
                .ToList();

            if (missingProducts.Count == 0)
                return;

            foreach (var product in missingProducts)
            {
                var productDto = _mapper.Map<ProductDto>(product);
                var response = _productApiService.InsertAndActivateProduct(new InsertAndActivateProductRequestReturn
                {
                    Lang = "tr",
                    ForceToSpecEntry = true,
                    NextDateOption = true,
                    ItemId = productDto.Id.ToString(),
                    Product = ProductInsert(productDto)
                });


                //await _gittigidiyorProxyClient.InsertProduct(productDto);
            }


        }
        private ProductTypeReturn ProductInsert(ProductDto product)
        {
            var productTypeModel = new ProductTypeReturn();
            var specTypeList = new List<SpecTypeReturn>();

            foreach (SpecDto dto in product.SpecList)
            {
                var spec = new SpecTypeReturn()
                { Name = dto.Name, Type = dto.Type, Value = dto.Value, Required = dto.Required };
                specTypeList.Add(spec);
            }

            var photoTypeList = new List<PhotoTypeReturn>();

            var pictureList = product.PictureModelList.OrderBy(o => o.DisplayOrder).ToList();
            for (int i = 0; i < pictureList.Count; i++)
            {
                var photo = new PhotoTypeReturn { PhotoId = i, Url = pictureList[i].ImageUrl };
                photoTypeList.Add(photo);
            }




            var cargoDetail = new CargoDetailTypeReturn
            { City = product.CityCode, CargoCompanies = new[] { product.CargoCompany } };

            var type = new CargoCompanyDetailTypeReturn()
            {
                Name = product.CargoCompany,
                //cityPrice = product.CityPrice, 
                CityPrice = "0",
                //countryPrice = product.CityPrice
                CountryPrice = "0"
            };
            cargoDetail.CargoCompanyDetails = new CargoCompanyDetailTypeReturn[1];
            cargoDetail.CargoCompanyDetails[0] = type;


            if (productTypeModel.BuyNowPrice >= 300)
            {
                cargoDetail.ShippingPayment = "S";
            }
            else
            {
                cargoDetail.ShippingPayment = "B";
            }


            cargoDetail.ShippingFeePaymentType = "PAY_IN_THE_BASKET";
            cargoDetail.ShippingWhere = "country";
            cargoDetail.ShippingTime = new CargoTimeTypeReturn() { Days = "today", BeforeTime = "16:30" };

            if (product.Name.Length > 100)
            {
                product.Name = product.Name.Substring(0, 100);
            }


            productTypeModel.Specs = specTypeList.ToArray();
            productTypeModel.Photos = photoTypeList.ToArray();
            productTypeModel.PageTemplate = 3;
            //productTypeModel.PageTemplateSpecified = true;
            productTypeModel.Description = product.FullDescription;
            productTypeModel.Format = "S";
            productTypeModel.BuyNowPrice = (double)product.GittigidiyorPrice;
            //productTypeModel.BuyNowPriceSpecified = true;
            productTypeModel.ListingDays = 360;
            //productTypeModel.ListingDaysSpecified = true;
            productTypeModel.ProductCount = product.StockQuantity;
            //productTypeModel.ProductCountSpecified = true;
            productTypeModel.CargoDetail = cargoDetail;
            productTypeModel.StartDate = DateTime.Now.AddMinutes(10).ToString("yyyy-MM-dd HH':'mm':'ss");
            productTypeModel.AffiliateOption = false;
            productTypeModel.BoldOption = false;
            productTypeModel.CatalogOption = false;
            productTypeModel.VitrineOption = false;
            productTypeModel.CategoryCode = product.GittigidiyorCategoryList.First().CategoryCode;

            return productTypeModel;
        }
        //[ServiceFilter(typeof(TokenFilter))]
        //[HttpGet("UpdateAllStocks")]
        //public async Task<IActionResult> UpdateAllStocks(DateTime? startDate, DateTime? endDate)
        //{
        //    var products = await _productClient.GetProductList(new ProductRequestModel
        //    {
        //        StartDate = startDate,
        //        EndDate = endDate
        //    });

        //    foreach (var product in products)
        //    foreach (var product in products)
        //    {
        //        var gittigidiyorId = _prodMongoRepository.FilterBy(f => f.ProductId == internalProductId)
        //            .FirstOrDefault();
        //        if (gittigidiyorId == null)
        //        {
        //            continue;

        //        }

        //        int internalProductId = await _productService.GetInteralProductId(product.Id);
        //        await _gittigidiyorProxyClient.UpdateProductStock(gittigidiyorId.GittigidiyorProductId.Value, (int)ideaStockAmount);
        //    }

        //}


    }
}
