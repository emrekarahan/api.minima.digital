﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Minima.Core.MessageModels;
using Minima.Integration.Api.Services;

namespace Minima.Integration.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    public class ProductController : ControllerBase
    {
        private readonly IProductService _productService;
        private readonly IPictureService _pictureService;
        private ILogger<ProductController> _logger;

        public ProductController(
            IProductService productService,
            IPictureService pictureService, ILogger<ProductController> logger)
        {
            _productService = productService;
            _pictureService = pictureService;
            _logger = logger;
        }

        [HttpPost]
        [Route("UpdateProduct")]
        [Consumes("application/json")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<Model.Response.Response> Post([FromBody] ProductMessageModel productMessageModel)
        {
            var response = await _productService.ImportProduct(productMessageModel);
            return response;
        }

        [HttpPost]
        [Route("UpdatePicture")]
        [Consumes("application/json")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> Post([FromBody] ProductPictureMessageModel productMessageModel)
        {
            var response = await _pictureService.UpdatePicture(productMessageModel);
            return Ok(response);
        }

        [HttpPost]
        [Route("UpdateProductStock")]
        [Consumes("application/json")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> UpdateProductStock([FromBody] ProductMessageModel productMessageModel)
        {
            try
            {
                await _productService.UpdateProductStock(productMessageModel);
                return Ok();
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
            }

            return BadRequest();
        }
    }
}