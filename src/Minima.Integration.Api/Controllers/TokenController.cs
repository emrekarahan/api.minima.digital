﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Minima.Core;
using Minima.Core.Repositories;
using Minima.Integration.Api.Model;
using Minima.Integration.Api.Model.Response;
using Minima.Integration.Api.Model.Token;
using Minima.Integration.Api.Services;
using Minima.Integration.Api.Services.Impl;
using Minima.Integration.Data;
using Minima.Integration.Data.Domain;

namespace Minima.Integration.Api.Controllers
{

    [Produces("application/json")]
    public class TokenController : Controller
    {
        private readonly TokenService _tokenService;
        private readonly ILogger<TokenController> _logger;
        private readonly IRepository<AccessToken, IntegrationContext> _accessTokenRepository;

        public TokenController(
            ILogger<TokenController> logger,
            TokenService tokenService,
            IRepository<AccessToken, IntegrationContext> accessTokenRepository)
        {
            _logger = logger;
            _tokenService = tokenService;
            _accessTokenRepository = accessTokenRepository;
        }


        [HttpGet]
        [AllowAnonymous]
        [Route("Security/Login")]
        //[Consumes("application/json")]
        //[TransformException(typeof(AuthenticationException), HttpStatusCode.Unauthorized, "Authentication failed")]
        public IActionResult Login(string returnUrl = "/")
        {
            return Challenge(new AuthenticationProperties() { RedirectUri = returnUrl });
        }


        [HttpGet]
        [AllowAnonymous]
        [Route("Security/Auth")]
        //[Consumes("application/json")]
        //[TransformException(typeof(AuthenticationException), HttpStatusCode.Unauthorized, "Authentication failed")]
        public async Task<Response<TokenOutModel>> GetToken([FromQuery]string state, string code)
        {
            var result = await _tokenService.GetToken(state, code);

            await _accessTokenRepository.InsertAsync(new AccessToken
            {
                AccessTokenMember = result.Data.AccessToken,
                RefreshToken = result.Data.RefreshToken,
                ExpireDate = DateTime.Now.AddHours(6)
            });
            return result;

        }

    }
}
