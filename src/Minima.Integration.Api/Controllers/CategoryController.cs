﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Minima.Core.MessageModels;
using Minima.Integration.Api.Services;

namespace Minima.Integration.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    public class CategoryController : ControllerBase
    {
        private readonly ICategoryService _categoryService;

        public CategoryController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }

        [HttpPost]
        [Route("UpdateCategory")]
        [Consumes("application/json")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task Post([FromBody]List<CategoryMessageModel> categoryMessageModel)
        {
            await _categoryService.ImportCategories(categoryMessageModel);
        }
    }
}