﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.EntityFrameworkCore;
using Minima.Core;
using Minima.Core.Repositories;
using Minima.Integration.Api.Services;
using Minima.Integration.Api.Services.Impl;
using Minima.Integration.Data;
using Minima.Integration.Data.Domain;

namespace Minima.Integration.Api.Filters
{
    public class TokenFilter : IAsyncActionFilter
    {
        private readonly IRepository<AccessToken, IntegrationContext> _accessTokenRepository;
        private readonly TokenService _tokenService;

        public TokenFilter(
            IRepository<AccessToken, IntegrationContext> accessTokenRepository,
            TokenService tokenService)
        {
            _accessTokenRepository = accessTokenRepository;
            _tokenService = tokenService;
        }

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            var accessToken = await _accessTokenRepository.QueryAble().FirstOrDefaultAsync();
            //if (accessToken == null)
            //    //await GetAccessToken();

            if (accessToken != null && DateTime.Now >= accessToken.ExpireDate)
            {
                var token = await RefreshToken(accessToken);
                CurrentTokenStatic.AccessToken = token.AccessTokenMember;
                await next();
            }
            else if (accessToken != null && DateTime.Now < accessToken.ExpireDate)
            {
                CurrentTokenStatic.AccessToken = accessToken.AccessTokenMember;
                await next();
            }

        }

        private async Task<AccessToken> RefreshToken(AccessToken accessToken)
        {
            accessToken = await _tokenService.RefreshToken(accessToken);
            return accessToken;
        }
    }
}