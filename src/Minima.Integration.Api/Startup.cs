﻿using System.Globalization;
using AutoMapper;
using Falcon.Package.MarketPlace.Gittigidiyor;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Minima.Core.Repositories;
using Minima.Core.Repositories.Impl;
using Minima.Integration.Api.Filters;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Minima.Ideasoft.Client;
using Minima.Integration.Api.Configurations;
using Minima.Integration.Api.Services;
using Minima.Integration.Api.Services.Impl;
using Minima.Integration.Data;
using Minima.Logging.Extensions;
using Minima.Data.MongoDb;
using Minima.Data.MongoDb.Impl;
using Minima.Data.MongoDb.Repository;
using Minima.Data.MongoDb.Repository.Impl;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;

namespace Minima.Integration.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc()
                .AddJsonOptions(o =>
                {
                    o.SerializerSettings.MetadataPropertyHandling = MetadataPropertyHandling.Ignore;
                    o.SerializerSettings.DateParseHandling = DateParseHandling.None;
                    o.SerializerSettings.Converters.Add(new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal });
                })
               .SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddAutoMapper(typeof(Startup));

            services.AddSwaggerGen(c =>
                {
                    c.SwaggerDoc("v1", new Info { Title = "Minima.Integration.Api", Version = "v1" });
                });

            //services.AddJobModule();

            services.AddDbContext<IntegrationContext>(o =>
            {
                o.UseSqlServer(Configuration.GetConnectionString("IntegrationDb"));
            });

            services
                .AddScoped(typeof(IRepository<,>), typeof(Repository<,>));

            services.AddScoped<TokenFilter>();

            services.AddIdeasoftClient();


            services.Configure<MongoDbSettings>(Configuration.GetSection("MongoDbSettings"));

            services.AddSingleton<IMongoDbSettings>(serviceProvider =>
                serviceProvider.GetRequiredService<IOptions<MongoDbSettings>>().Value);


            services.AddScoped(typeof(IMongoRepository<>), typeof(MongoRepository<>));


            services.AddScoped<ICategoryService, CategoryService>()
                .AddScoped<IProductService, ProductService>()
                .AddScoped<IPictureService, PictureService>();

            services.AddGittigidiyor(new GittigidiyorSettings
            {
                ApiKey = "RhqsGzm8exNebfkhc5ZU3bAbj7WMffDT",
                ApiSecret = "eTfk9TeYWAj6kkNs",
                RoleName = "gokcekoleksiyon",
                RolePassword = "xZzDUYDv5aF7G9kpy5ZwTbCBaFRqhtrH",
                ApiUrl = "https://dev.gittigidiyor.com:8443/listingapi/ws"

            });

            services.AddMinimaLogging(Configuration.GetSection("Logging"));

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseDeveloperExceptionPage();
            app.UseAuthentication();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Minima.Integration.Api");
            });

            app.UseMinimaLogging();
            var logger = app.ApplicationServices.GetService<ILogger<Startup>>();
            logger.LogInformation($"'Minima.Integration.Api' app running on '{env.EnvironmentName}' mode");

            //var b2BSettings = app.ApplicationServices.GetRequiredService<B2BSettings>();
            //Token token = new Token(b2BSettings.StoreUrl, b2BSettings.Username, b2BSettings.Password);
            //WebApiServices.InitContainer(token.ApiToken, b2BSettings.StoreUrl);

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
