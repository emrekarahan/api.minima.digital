﻿using Minima.Data.MongoDb.Attribute;
using Minima.Data.MongoDb.Impl;


namespace Minima.Integration.Api.Domain
{
    [BsonCollection("GittigidiyorSettings")]
    public class GittigidiyorSettings : Document
    {
        public string Commission { get; set; }
        public string ShippingWhere { get; set; }
        public string CityCode { get; set; }
        public string CargoCompanies { get; set; }
        public string CityPrice { get; set; }
        public string CountryPrice { get; set; }
        public string StoreLocation { get; set; }
    }
}