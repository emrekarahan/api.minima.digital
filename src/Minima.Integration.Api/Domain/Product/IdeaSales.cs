﻿using System;
using Minima.Data.MongoDb.Attribute;
using Minima.Data.MongoDb.Impl;

namespace Minima.Integration.Api.Domain.Product
{
    [BsonCollection("IdeaSales")]
    public class IdeaSales : Document
    {
        public int OrderId { get; set; }
        public DateTime OrderDate { get; set; }
        public int OrderItemId { get; set; }
        public int ProductId { get; set; }
        public int? GittigidiyorProductId { get; set; }
        public int InternalProductId { get; set; }
        public double? StockQuantity { get; set; }
        public string PaymentStatus { get; set; }
    }

}