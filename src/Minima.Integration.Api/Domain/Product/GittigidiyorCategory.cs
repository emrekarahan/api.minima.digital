﻿namespace Minima.Integration.Api.Domain.Product
{
    public class GittigidiyorCategory
    {
        public string CategoryCode { get; set; }
        public string CategoryName { get; set; }

    }
}