﻿namespace Minima.Integration.Api.Domain.Product
{
    public class Picture
    {
        public int Id { get; set; }
        public string MimeType { get; set; }
        public string ImageUrl { get; set; }
        public int DisplayOrder { get; set; }
    }
}