﻿using System;
using Minima.Data.MongoDb.Attribute;
using Minima.Data.MongoDb.Impl;

namespace Minima.Integration.Api.Domain.Product
{
    [BsonCollection("Sales")]
    public class Sales : Document
    {
        public string SalesCode { get; set; }
        public int ProductId { get; set; }
        public int Amount { get; set; }
        public DateTime OrderDate { get; set; }
    }


}