﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Minima.Integration.Api.Domain.Product
{
    public class MemberGroup
    {

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }

    //public class Member
    //{

    //    [JsonProperty("id")]
    //    public int Id { get; set; }

    //    [JsonProperty("firstname")]
    //    public string Firstname { get; set; }

    //    [JsonProperty("surname")]
    //    public string Surname { get; set; }

    //    [JsonProperty("email")]
    //    public string Email { get; set; }

    //    [JsonProperty("gender")]
    //    public string Gender { get; set; }

    //    [JsonProperty("phoneNumber")]
    //    public string PhoneNumber { get; set; }

    //    [JsonProperty("mobilePhoneNumber")]
    //    public string MobilePhoneNumber { get; set; }

    //    [JsonProperty("address")]
    //    public string Address { get; set; }

    //    [JsonProperty("status")]
    //    public string Status { get; set; }

    //    [JsonProperty("memberGroup")]
    //    public MemberGroup MemberGroup { get; set; }
    //}

    //public class OrderDetail
    //{

    //    [JsonProperty("id")]
    //    public int Id { get; set; }

    //    [JsonProperty("varKey")]
    //    public string VarKey { get; set; }

    //    [JsonProperty("varValue")]
    //    public string VarValue { get; set; }
    //}

    public class IdeaProduct
    {

        [JsonProperty("id")]
        public int Id { get; set; }
    }

    public class OrderItem
    {

        [JsonProperty("id")]
        public int Id { get; set; }

        //[JsonProperty("productName")]
        //public string ProductName { get; set; }

        //[JsonProperty("productSku")]
        //public string ProductSku { get; set; }

        //[JsonProperty("productBarcode")]
        //public string ProductBarcode { get; set; }

        //[JsonProperty("productPrice")]
        //public int ProductPrice { get; set; }

        //[JsonProperty("productCurrency")]
        //public string ProductCurrency { get; set; }

        //[JsonProperty("productQuantity")]
        //public int ProductQuantity { get; set; }

        //[JsonProperty("productTax")]
        //public int ProductTax { get; set; }

        //[JsonProperty("productDiscount")]
        //public int ProductDiscount { get; set; }

        //[JsonProperty("productMoneyOrderDiscount")]
        //public int ProductMoneyOrderDiscount { get; set; }

        //[JsonProperty("productWeight")]
        //public int ProductWeight { get; set; }

        //[JsonProperty("productStockTypeLabel")]
        //public string ProductStockTypeLabel { get; set; }

        //[JsonProperty("isProductPromotioned")]
        //public int IsProductPromotioned { get; set; }

        //[JsonProperty("discount")]
        //public int Discount { get; set; }

        [JsonProperty("product")]
        public IdeaProduct Product { get; set; }

        //[JsonProperty("orderItemCustomizations")]
        //public IList<object> OrderItemCustomizations { get; set; }

        //[JsonProperty("orderItemSubscription")]
        //public object OrderItemSubscription { get; set; }
    }

    //public class ShippingAddress
    //{

    //    [JsonProperty("id")]
    //    public int Id { get; set; }

    //    [JsonProperty("firstname")]
    //    public string Firstname { get; set; }

    //    [JsonProperty("surname")]
    //    public string Surname { get; set; }

    //    [JsonProperty("country")]
    //    public string Country { get; set; }

    //    [JsonProperty("location")]
    //    public string Location { get; set; }

    //    [JsonProperty("subLocation")]
    //    public string SubLocation { get; set; }

    //    [JsonProperty("address")]
    //    public string Address { get; set; }

    //    [JsonProperty("phoneNumber")]
    //    public string PhoneNumber { get; set; }

    //    [JsonProperty("mobilePhoneNumber")]
    //    public string MobilePhoneNumber { get; set; }
    //}

    //public class BillingAddress
    //{

    //    [JsonProperty("id")]
    //    public int Id { get; set; }

    //    [JsonProperty("firstname")]
    //    public string Firstname { get; set; }

    //    [JsonProperty("surname")]
    //    public string Surname { get; set; }

    //    [JsonProperty("country")]
    //    public string Country { get; set; }

    //    [JsonProperty("location")]
    //    public string Location { get; set; }

    //    [JsonProperty("subLocation")]
    //    public string SubLocation { get; set; }

    //    [JsonProperty("address")]
    //    public string Address { get; set; }

    //    [JsonProperty("phoneNumber")]
    //    public string PhoneNumber { get; set; }

    //    [JsonProperty("mobilePhoneNumber")]
    //    public string MobilePhoneNumber { get; set; }

    //    [JsonProperty("invoiceType")]
    //    public string InvoiceType { get; set; }

    //    [JsonProperty("taxNo")]
    //    public object TaxNo { get; set; }

    //    [JsonProperty("taxOffice")]
    //    public object TaxOffice { get; set; }

    //    [JsonProperty("identityRegistrationNumber")]
    //    public string IdentityRegistrationNumber { get; set; }
    //}

    public class Order
    {

        [JsonProperty("id")]
        public int Id { get; set; }

        //[JsonProperty("customerFirstname")]
        //public string CustomerFirstname { get; set; }

        //[JsonProperty("customerSurname")]
        //public string CustomerSurname { get; set; }

        //[JsonProperty("customerEmail")]
        //public string CustomerEmail { get; set; }

        //[JsonProperty("customerPhone")]
        //public string CustomerPhone { get; set; }

        //[JsonProperty("paymentTypeName")]
        //public string PaymentTypeName { get; set; }

        //[JsonProperty("paymentProviderCode")]
        //public string PaymentProviderCode { get; set; }

        //[JsonProperty("paymentProviderName")]
        //public string PaymentProviderName { get; set; }

        //[JsonProperty("paymentGatewayCode")]
        //public string PaymentGatewayCode { get; set; }

        //[JsonProperty("paymentGatewayName")]
        //public string PaymentGatewayName { get; set; }

        //[JsonProperty("bankName")]
        //public string BankName { get; set; }

        //[JsonProperty("clientIp")]
        //public string ClientIp { get; set; }

        //[JsonProperty("userAgent")]
        //public string UserAgent { get; set; }

        //[JsonProperty("currency")]
        //public string Currency { get; set; }

        //[JsonProperty("currencyRates")]
        //public string CurrencyRates { get; set; }

        //[JsonProperty("amount")]
        //public double Amount { get; set; }

        //[JsonProperty("couponDiscount")]
        //public int CouponDiscount { get; set; }

        //[JsonProperty("taxAmount")]
        //public double TaxAmount { get; set; }

        //[JsonProperty("totalCustomTaxAmount")]
        //public double TotalCustomTaxAmount { get; set; }

        //[JsonProperty("promotionDiscount")]
        //public int PromotionDiscount { get; set; }

        //[JsonProperty("generalAmount")]
        //public double GeneralAmount { get; set; }

        //[JsonProperty("shippingAmount")]
        //public double ShippingAmount { get; set; }

        //[JsonProperty("additionalServiceAmount")]
        //public double AdditionalServiceAmount { get; set; }

        //[JsonProperty("finalAmount")]
        //public double FinalAmount { get; set; }

        //[JsonProperty("sumOfGainedPoints")]
        //public int SumOfGainedPoints { get; set; }

        //[JsonProperty("installment")]
        //public int Installment { get; set; }

        //[JsonProperty("installmentRate")]
        //public double InstallmentRate { get; set; }

        //[JsonProperty("extraInstallment")]
        //public int ExtraInstallment { get; set; }

        //[JsonProperty("transactionId")]
        //public string TransactionId { get; set; }

        //[JsonProperty("hasUserNote")]
        //public int HasUserNote { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("paymentStatus")]
        public string PaymentStatus { get; set; }

        //[JsonProperty("errorMessage")]
        //public string ErrorMessage { get; set; }

        //[JsonProperty("deviceType")]
        //public string DeviceType { get; set; }

        //[JsonProperty("referrer")]
        //public string Referrer { get; set; }

        //[JsonProperty("invoicePrintCount")]
        //public int InvoicePrintCount { get; set; }

        //[JsonProperty("useGiftPackage")]
        //public int UseGiftPackage { get; set; }

        //[JsonProperty("giftNote")]
        //public string GiftNote { get; set; }

        //[JsonProperty("memberGroupName")]
        //public string MemberGroupName { get; set; }

        //[JsonProperty("usePromotion")]
        //public int UsePromotion { get; set; }

        //[JsonProperty("shippingProviderCode")]
        //public string ShippingProviderCode { get; set; }

        //[JsonProperty("shippingProviderName")]
        //public string ShippingProviderName { get; set; }

        //[JsonProperty("shippingCompanyName")]
        //public string ShippingCompanyName { get; set; }

        //[JsonProperty("shippingPaymentType")]
        //public string ShippingPaymentType { get; set; }

        //[JsonProperty("shippingTrackingCode")]
        //public string ShippingTrackingCode { get; set; }

        //[JsonProperty("source")]
        //public string Source { get; set; }

        [JsonProperty("createdAt")]
        public DateTime CreatedAt { get; set; }

        //[JsonProperty("updatedAt")]
        //public DateTime UpdatedAt { get; set; }

        //[JsonProperty("maillist")]
        //public Maillist Maillist { get; set; }

        //[JsonProperty("member")]
        //public Member Member { get; set; }

        //[JsonProperty("orderDetails")]
        //public IList<OrderDetail> OrderDetails { get; set; }

        [JsonProperty("orderItems")]
        public IList<OrderItem> OrderItems { get; set; }

        //[JsonProperty("orderCustomTaxLines")]
        //public IList<object> OrderCustomTaxLines { get; set; }

        //[JsonProperty("shippingAddress")]
        //public ShippingAddress ShippingAddress { get; set; }

        //[JsonProperty("billingAddress")]
        //public BillingAddress BillingAddress { get; set; }
    }




}
