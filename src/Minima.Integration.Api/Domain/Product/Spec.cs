﻿namespace Minima.Integration.Api.Domain.Product
{
    public class Spec
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public string Value { get; set; }
        public bool Required { get; set; }

    }
}
