﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Padawan.Extensions;
using Padawan.RabbitMq.Extensions;

namespace Minima.Integration.Api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .UsePadawan<Startup>()
                .UseRabbitMq();

        //.UseUtcTime();
    }
}
