﻿using Padawan.Attributes;

namespace Minima.TaskScheduler.Console.Proxies.IntegrationApi
{

    [Configuration("IntegrationService")]
    public class IntegrationApiConfiguration
    {
        public string Url { get; set; }
    }
}
