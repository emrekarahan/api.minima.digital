﻿using System;
using Minima.Data.MongoDb.Attribute;
using Minima.Data.MongoDb.Impl;

namespace Minima.TaskScheduler.Console.Domain
{
    [BsonCollection("DailyRawProducts")] 
    public class DailyUpdatedProducts : Document
    {
        public int ProductId { get; set; }
        public DateTimeOffset UpdatedAt { get; set; }

    }
}