﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Minima.TaskScheduler.Console.Models.Request;
using Minima.TaskScheduler.Console.Proxies.IntegrationApi;
using Padawan.Abstractions;
using Padawan.Attributes;

namespace Minima.TaskScheduler.Console.Jobs
{
    public class UpdateStockTask : ISchedulerJob
    {
        private readonly IntegrationApiClient _integrationApiClient;
        private readonly ILogger<UpdateStockTask> _logger;

        public UpdateStockTask(IntegrationApiClient integrationApiClient, ILogger<UpdateStockTask> logger)
        {
            _integrationApiClient = integrationApiClient;
            _logger = logger;
        }


        [RunNow]
        [RunEvery(2, Period.Minute)]
        public async Task UpdateStocks()
        {
            try
            {
                _logger.LogInformation($"Update Product Stock Task is Started : {DateTime.Now}");
                var lastUpdate = DateTime.Now;
                var requestModel = new ProductRequestModel
                {
                    StartUpdatedAt = lastUpdate
                };
                await _integrationApiClient.UpdateProductStock(requestModel);
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
            }
        }


    }
}