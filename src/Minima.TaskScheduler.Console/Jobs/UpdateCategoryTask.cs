﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Minima.Data.MongoDb.Repository;
using Minima.TaskScheduler.Console.Domain;
using Minima.TaskScheduler.Console.Models.Request;
using Minima.TaskScheduler.Console.Proxies.IntegrationApi;
using Padawan.Abstractions;
using Padawan.Attributes;

namespace Minima.TaskScheduler.Console.jobs
{
    public class UpdateCategoryTask : ISchedulerJob
    {
        private readonly ILogger<UpdateCategoryTask> _logger;
        private readonly IntegrationApiClient _integrationApiClient;
        private readonly IMongoRepository<DailyUpdatedCategories> _dailyUpdatedCategoryRepository;

        public UpdateCategoryTask(
            ILogger<UpdateCategoryTask> logger, 
            IntegrationApiClient integrationApiClient, 
            IMongoRepository<DailyUpdatedCategories> dailyUpdatedCategoryRepository)
        {
            _logger = logger;
            _integrationApiClient = integrationApiClient;
            _dailyUpdatedCategoryRepository = dailyUpdatedCategoryRepository;
        }


        [RunNow]
        [RunEvery(30, Period.Minute)]
        public async Task UpdateCategories()
        {
            try
            {
                _logger.LogInformation($"Update Category Job is Started : {DateTime.Now}");
                var lastUpdate = DateTime.Now;
                var requestModel = new CategoryRequestModel
                {
                    StartUpdatedAt = lastUpdate
                };
                await _integrationApiClient.UpdateCategory(requestModel);
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
            }
        }


        [RunNow]
        [RunEvery(60, Period.Minute)]
        public async Task ClearOldProductRecords()
        {
            try
            {
                _logger.LogInformation($"Clear Old Categories  Records Job  is Started : {DateTime.Now}");
                var deleteDate = DateTime.Now.AddDays(-1);
                await _dailyUpdatedCategoryRepository.DeleteManyAsync(p => p.UpdatedAt <= deleteDate);
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
            }
        }
    }

}