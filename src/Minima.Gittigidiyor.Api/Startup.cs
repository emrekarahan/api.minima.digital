﻿using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Minima.Data.MongoDb;
using Minima.Data.MongoDb.Impl;
using Minima.Data.MongoDb.Repository;
using Minima.Data.MongoDb.Repository.Impl;
using Minima.Logging.Extensions;
using Swashbuckle.AspNetCore.Swagger;

namespace Minima.Gittigidiyor.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "Minima.Integration.Api", Version = "v1" });
            });


            services.AddMinimaLogging(Configuration.GetSection("Logging"));

            services.AddResponseCaching();


            services.Configure<MongoDbSettings>(Configuration.GetSection("MongoDbSettings"));

            services.AddSingleton<IMongoDbSettings>(serviceProvider =>
                serviceProvider.GetRequiredService<IOptions<MongoDbSettings>>().Value);

            services.AddAutoMapper(typeof(Startup));

            services.AddScoped(typeof(IMongoRepository<>), typeof(MongoRepository<>));

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Minima.Integration.Api");
            });


            app.UseMinimaLogging();
            var logger = app.ApplicationServices.GetService<ILogger<Startup>>();
            logger.LogInformation($"'Minima.Gittigidiyor.Api' app running on '{env.EnvironmentName}' mode");

            app.UseResponseCaching();
            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
