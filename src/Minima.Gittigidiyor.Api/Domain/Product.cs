﻿using System;
using System.Collections.Generic;
using Minima.Data.MongoDb.Attribute;
using Minima.Data.MongoDb.Impl;

namespace Minima.Gittigidiyor.Api.Domain
{
    [BsonCollection("Products")]
    public class Product : Document
    {
        public int ProductId { get; set; }
        public string Name { get; set; }
        public string ShortDescription { get; set; }
        public string FullDescription { get; set; }
        public string MetaKeywords { get; set; }
        public string MetaDescription { get; set; }
        public string MetaTitle { get; set; }
        public string Sku { get; set; }
        public string Gtin { get; set; }
        public int StockQuantity { get; set; }
        public decimal Price { get; set; }
        public bool Published { get; set; }

        public List<Category> CategoryModelList { get; set; }
        public List<GittigidiyorCategory> GittigidiyorCategoryList { get; set; }
        public List<Picture> PictureModelList { get; set; }
        public List<Spec> SpecList { get; set; }
        public decimal GittigidiyorPrice { get; set; }
        public string CityPrice { get; set; }
        public decimal Commission { get; set; }
        public string ShippingWhere { get; set; }
        public string CityCode { get; set; }
        public long? GittigidiyorProductId { get; set; }
        public DateTime? SyncedAt { get; set; }
        public bool Synced { get; set; }
        public string CargoCompany { get; set; }
        public bool PreventSync { get; set; }
        public string Status { get; set; }
    }

    public class Category
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class Picture
    {
        public int Id { get; set; }
        public string MimeType { get; set; }
        public string ImageUrl { get; set; }
        public int DisplayOrder { get; set; }
    }

    public class Spec
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public string Value { get; set; }
        public bool Required { get; set; }

    }

    public class GittigidiyorCategory
    {
        public string CategoryCode { get; set; }
        public string CategoryName { get; set; }

    }
}
