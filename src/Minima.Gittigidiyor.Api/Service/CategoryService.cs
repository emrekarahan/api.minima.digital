﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Minima.Gittigidiyor.Api.Data;
using Minima.Gittigidiyor.Api.Dto;
using Minima.Gittigidiyor.Api.Models.Request;
using Minima.Gittigidiyor.Api.Models.Response;
using Minima.Gittigidiyor.Api.Models.Vm;
using Padawan.Attributes;

namespace Minima.Gittigidiyor.Api.Service
{
    [Singleton]
    public class CategoryService
    {
        private readonly CategoryRepository _categoryRepository;
        private readonly ILogger<CategoryService> _logger;

        public CategoryService(CategoryRepository categoryRepository, ILogger<CategoryService> logger)
        {
            _categoryRepository = categoryRepository;
            _logger = logger;
        }

        public async Task<GridModel<CategoryDto>>  GetAllCategoriesByParams(string categoryName, string categoryCode,
            int pageIndex, int pageSize, string draw)
        {
            try
            {
                var result = await _categoryRepository.GetAllCategoriesByParams(categoryName, categoryCode, pageIndex, pageSize, draw);

                foreach (var categoryDto in result.data)
                {
                    //var mappedCategoies = await _categoryRepository.GetMappedCategory(categoryDto.Id, 0, 0);
                    //var stringCategories = mappedCategoies.Select(s => ">" + s.CategoryName).ToList();
                    categoryDto.MappedCategories = string.Empty; //string.Join("\r\n", stringCategories);
                }

                return result;
            }
            catch (Exception e)
            {
                _logger.LogError(new EventId(500), e, e.Message);
            }

            return null;
        }

        public async Task<CategoryDetailVm> GetCategoryDetail(int id)
        {
            try
            {
                var result = await _categoryRepository.GetCategoryDetail(id);
                return result;
            }
            catch (Exception e)
            {
                _logger.LogError(new EventId(500), e, e.Message);
            }
            return null;
        }

        public async Task UpdateCategorySpecs(CategorySpecRequestModel model)
        {
            try
            {
                foreach (var nopCategory in model.NopCategory)
                {
                    foreach (var spec in nopCategory.Spec)
                    {
                        spec.NopCategoryId = nopCategory.Id;
                        await _categoryRepository.UpdateCategorySpecs(spec.NopCategoryId, spec.Id,
                            Convert.ToInt32(spec.SelectedValue));
                    }
                }
            }
            catch (Exception e)
            {
                _logger.LogError(new EventId(500), e, e.Message);
            }
        }

        public async Task<GridModel<NopCategoryMappingDto>> GetMappedCategory(int categoryId, int page, int pageSize, string draw)
        {
            try
            {
                var result = await _categoryRepository.GetMappedCategory(categoryId, page, pageSize);
                return new GridModel<NopCategoryMappingDto>
                {
                    Data = result,
                    data = result,
                    Draw = draw,
                    RecordsFiltered = 0,
                    RecordsTotal = 0,
                    Total = 0
                };
            }
            catch (Exception e)
            {
                _logger.LogError(new EventId(500), e, e.Message);
            }

            return null;
        }

        public async Task AddCategoryMap(CategoryMappingRequestModel model)
        {
            foreach (var s in model.NopCategoryIds)
            {
                await _categoryRepository.AddCategoryMap(model.CategoryId, s);
            }
        }

        public async Task DeleteCategoryMap(int id)
        {
            await _categoryRepository.DeleteCategoryMap(id);
        }
    }
}