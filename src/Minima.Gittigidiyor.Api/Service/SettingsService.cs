﻿using System.Linq;
using System.Threading.Tasks;
using Minima.Data.MongoDb.Repository;
using Minima.Gittigidiyor.Api.Domain;
using Padawan.Attributes;

namespace Minima.Gittigidiyor.Api.Service
{
    [Scoped]
    public class SettingsService
    {
        private readonly IMongoRepository<GittigidiyorSettings> _mongoRepository;

        public SettingsService(IMongoRepository<GittigidiyorSettings> mongoRepository)
        {
            _mongoRepository = mongoRepository;
        }

        public async Task UpdateSettings(GittigidiyorSettings model)
        {
            var data = _mongoRepository.AsQueryable().ToList();

            foreach (GittigidiyorSettings settings in data)
            {
                await _mongoRepository.DeleteByIdAsync(settings.Id.ToString());
            }
            await _mongoRepository.InsertOneAsync(model);
        }
    }
}