﻿using Padawan.Attributes;

namespace Minima.Gittigidiyor.Api.Configurations
{
    [Configuration("AppConfiguration")]
    public class AppConfiguration
    {
        public DatabaseSettings DatabaseSettings { get; set; }
    }

    public class DatabaseSettings
    {
        public string ConnectionString { get; set; }
    }
}