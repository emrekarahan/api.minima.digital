﻿using System.Collections.Generic;

namespace Minima.Gittigidiyor.Api.Dto
{
    public class SpecDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Required { get; set; }
        public string Type { get; set; }
        public int SelectedValue { get; set; }

        public List<SpecValueDto> SpecValue { get; set; }

        public int CategoryId { get; set; }
        public int NopCategoryId { get; set; }
    }
}