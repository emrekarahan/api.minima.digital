﻿using System.Collections.Generic;

namespace Minima.Gittigidiyor.Api.Dto
{
    public class CategoryDto
    {

        public int Id { get; set; }
        public string CategoryCode { get; set; }
        public string CategoryName { get; set; }
        public bool HasCatalog { get; set; }
        public bool Deepest { get; set; }
        public string TreePath { get; set; } 
        public int? ParentId { get; set; }
        public string MappedCategories { get; set; }


    }

    public class NopCategoryDto
    {

        public int Id { get; set; }
        public string Name { get; set; }
        public int ParentCategoryId { get; set; }
        public int Level { get; set; }
        public string TreePath { get; set; }
        public List<SpecDto> Spec { get; set; }
        public int MappingId { get; set; }
    }
}