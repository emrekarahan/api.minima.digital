﻿using System;

namespace Minima.Gittigidiyor.Api.Dto
{
    public class ProductDto
    {
        public string Id { get; set; }
        public int ProductId { get; set; }
        public string Name { get; set; }

        public string ThumbUrl { get; set; }
        public bool Synced { get; set; }
        public DateTime? SyncedAt { get; set; }
        public string Sku { get; set; }
        public int StockQuantity { get; set; }
        public decimal Price { get; set; }
        public decimal GittigidiyorPrice { get; set; }
        public bool Published { get; set; }
        public string GittigidiyorCategory { get; set; }
        public long? GittigidiyorProductId { get; set; }
        public string Status { get; set; }
    }
}