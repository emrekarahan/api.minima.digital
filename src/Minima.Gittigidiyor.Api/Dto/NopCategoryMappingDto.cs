﻿namespace Minima.Gittigidiyor.Api.Dto
{
    public class NopCategoryMappingDto
    {
        public int Id { get; set; }
        public int CategoryId { get; set; }
        public int NopCategoryId { get; set; }
        public string CategoryName { get; set; }
    }
}