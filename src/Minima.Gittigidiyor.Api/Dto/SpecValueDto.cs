﻿namespace Minima.Gittigidiyor.Api.Dto
{
    public class SpecValueDto
    {
        public int Id { get; set; }
        public int SpecId { get; set; }
        public string Value { get; set; }
    }
}