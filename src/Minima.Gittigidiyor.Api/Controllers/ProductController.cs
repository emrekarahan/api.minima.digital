﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Minima.Gittigidiyor.Api.Data;
using Minima.Gittigidiyor.Api.Domain;
using Minima.Data.MongoDb.Repository;

namespace Minima.Gittigidiyor.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly ProductRepository _productRepository;

        public ProductController(ProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        [HttpGet]
        [ResponseCache(Duration = 30, VaryByQueryKeys = new[] { "*" })]
        public IActionResult Get(string productName, string sku, int page, int pageSize, string draw)
        {
            var result = _productRepository.GetProducts(productName, sku, page, pageSize, draw);
            return Ok(result);
        }
    }
}