﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Minima.Gittigidiyor.Api.Domain;
using Minima.Gittigidiyor.Api.Dto;
using Minima.Gittigidiyor.Api.Models.Request;
using Minima.Gittigidiyor.Api.Service;

namespace Minima.Gittigidiyor.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GittigidiyorController : ControllerBase
    {
        private readonly CategoryService _categoryService;
        private readonly SettingsService _settingsService;

        public GittigidiyorController(CategoryService categoryService, SettingsService settingsService)
        {
            _categoryService = categoryService;
            _settingsService = settingsService;
        }

        // GET api/values0
        [HttpGet]

        public async Task<IActionResult> Get(string categoryName, string categoryCode, string draw, int pageIndex = 0, int pageSize = 50)
        {
            var result =
                await _categoryService.GetAllCategoriesByParams(categoryName, categoryCode, pageIndex, pageSize, draw);
            return Ok(result);
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var result = await _categoryService.GetCategoryDetail(id);
            return Ok(result);
        }

        [HttpPost]
        [Consumes("application/json")]
        [Route("UpdateCategorySpecs")]
        public async Task<IActionResult> Post([FromBody] CategorySpecRequestModel model)
        {
            await _categoryService.UpdateCategorySpecs(model);
            return Ok(model);
        }

        [HttpGet]
        [Route("GetMappedCategory")]
        public async Task<IActionResult> GetMappedCategory(int categoryId, int page, int pageSize, string draw)
        {
            var model = await _categoryService.GetMappedCategory(categoryId, page, pageSize, draw);
            return Ok(model);
        }

        [HttpPost]
        [Route("AddCategoryMap")]
        [Consumes("application/json")]
        public async Task<IActionResult> AddCategoryMap([FromBody]CategoryMappingRequestModel model)
        {
            await _categoryService.AddCategoryMap(model);
            return Ok();
        }

        [HttpGet]
        [Route("DeleteCategoryMap")]
        public async Task<IActionResult> AddCategoryMap(int id)
        {
            await _categoryService.DeleteCategoryMap(id);
            return Ok();
        }

        [HttpPost]
        [Route("UpdateSettings")]
        [Consumes("application/json")]
        public async Task<IActionResult> UpdateSettings([FromBody] GittigidiyorSettings model)
        {
            await _settingsService.UpdateSettings(model);
            return Ok();
        }

    }
}
