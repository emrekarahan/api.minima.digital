﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Minima.Gittigidiyor.Api.Models.Response
{
    public class GridModel<T>
    {

        /// <summary>
        /// Gets or sets data records
        /// </summary>
        [JsonProperty("Data")]
        public IEnumerable<T> Data { get; set; }

        [JsonProperty("data")]
        public IEnumerable<T> data { get; set; }

        /// <summary>
        /// Gets or sets draw
        /// </summary>
        [JsonProperty("draw")]
        public string Draw { get; set; }

        /// <summary>
        /// Gets or sets a number of filtered data records
        /// </summary>
        [JsonProperty("recordsFiltered")]
        public int RecordsFiltered { get; set; }

        /// <summary>
        /// Gets or sets a number of total data records
        /// </summary>
        [JsonProperty("recordsTotal")]
        public int RecordsTotal { get; set; }

        //TODO: remove after moving to DataTables grids
        [JsonProperty("total")]
        public int Total { get; set; }

    }
}