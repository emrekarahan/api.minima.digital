﻿using System.Collections.Generic;
using Minima.Gittigidiyor.Api.Dto;

namespace Minima.Gittigidiyor.Api.Models.Vm
{
    public class CategoryDetailVm
    {
        public CategoryDto Category { get; set; }
        public List<NopCategoryDto> NopCategory { get; set; }
        
        public List<NopCategoryMappingDto> NopCategoryMapping { get; set; }
    }
}