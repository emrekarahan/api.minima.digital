﻿namespace Minima.Gittigidiyor.Api.Models.Request
{
    public class CategoryMappingRequestModel
    {
        public int CategoryId { get; set; }
        public int[] NopCategoryIds { get; set; }
    }
}