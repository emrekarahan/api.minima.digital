﻿using System.Collections.Generic;
using Minima.Gittigidiyor.Api.Dto;
using Newtonsoft.Json;

namespace Minima.Gittigidiyor.Api.Models.Request
{
    public class Category
    {

        [JsonProperty("Id")]
        public int Id { get; set; }

        [JsonProperty("CategoryCode")]
        public string CategoryCode { get; set; }

        [JsonProperty("CategoryName")]
        public string CategoryName { get; set; }

        [JsonProperty("HasCatalog")]
        public bool HasCatalog { get; set; }

        [JsonProperty("Deepest")]
        public bool Deepest { get; set; }
    }

    public class Spec
    {

        [JsonProperty("Id")]
        public int Id { get; set; }

        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("Required")]
        public bool Required { get; set; }

        [JsonProperty("Type")]
        public string Type { get; set; }

        [JsonProperty("SelectedValue")]
        public string SelectedValue { get; set; }

        [JsonProperty("SpecValue")]
        public string SpecValue { get; set; }
        
        [JsonProperty("NopCategoryId")]
        public int NopCategoryId { get; set; }
    }

    public class CategorySpecRequestModel
    {

        [JsonProperty("Category")]
        public Category Category { get; set; }
        
        [JsonProperty("NopCategory")]
        public IList<NopCategoryDto> NopCategory { get; set; }
    }
}