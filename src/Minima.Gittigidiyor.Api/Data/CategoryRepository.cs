﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Minima.Gittigidiyor.Api.Configurations;
using Minima.Gittigidiyor.Api.Domain;
using Minima.Gittigidiyor.Api.Dto;
using Minima.Gittigidiyor.Api.Models.Request;
using Minima.Gittigidiyor.Api.Models.Response;
using Minima.Gittigidiyor.Api.Models.Vm;
using Padawan.Attributes;

namespace Minima.Gittigidiyor.Api.Data
{
    [Singleton]
    public class CategoryRepository
    {
        private readonly AppConfiguration _appConfiguration;

        public CategoryRepository(AppConfiguration appConfiguration)
        {
            _appConfiguration = appConfiguration;
        }


        public async Task<GridModel<CategoryDto>> GetAllCategoriesByParams(string categoryName, string categoryCode,
            int pageIndex, int pageSize, string draw)
        {
            using (var conn = new SqlConnection(_appConfiguration.DatabaseSettings.ConnectionString))
            {
                var data = await conn.QueryMultipleAsync(sql: "sp_GetCategoriesByParams",
                    param: new
                    {
                        @PageNumber = pageIndex,
                        @PageSize = pageSize,
                        @CategoryCode = categoryCode,
                        @CategoryName = categoryName
                    }, commandType: CommandType.StoredProcedure);

                var resultData = (await data.ReadAsync<CategoryDto>()).ToList();
                var dataCount = (await data.ReadAsync<int>()).FirstOrDefault();

                var gridModel = new GridModel<CategoryDto>
                {
                    Data = resultData,
                    Draw = draw,
                    RecordsFiltered = dataCount,
                    RecordsTotal = dataCount,
                };

                gridModel.data = gridModel.Data;
                return gridModel;
            }
        }

        public async Task<CategoryDetailVm> GetCategoryDetail(int categoryId)
        {
            using (var conn = new SqlConnection(_appConfiguration.DatabaseSettings.ConnectionString))
            {
                var data = await conn.QueryMultipleAsync(sql: "GetCategoryDetail_New",
                    param: new { @CategoryId = categoryId }, commandType: CommandType.StoredProcedure);

                var categoryDto = (await data.ReadAsync<CategoryDto>()).FirstOrDefault();
                var nopCategoryDto = (await data.ReadAsync<NopCategoryDto>()).ToList();
                var specDto = (await data.ReadAsync<SpecDto>()).ToList();
                var specValueDto = (await data.ReadAsync<SpecValueDto>()).ToList();
                //var nopCategoryMapping = (await data.ReadAsync<NopCategoryMappingDto>()).ToList();

                foreach (NopCategoryDto dto in nopCategoryDto)
                {
                    dto.Spec = specDto.Where(w => w.NopCategoryId == dto.Id).ToList();
                }


                foreach (var dto in specDto)
                {
                    dto.SpecValue = specValueDto.Where(w => w.SpecId == dto.Id).ToList();
                }

           
                var categoryDetailVm = new CategoryDetailVm
                {
                    NopCategory = nopCategoryDto,
                    Category = categoryDto
                };
                return categoryDetailVm;
            }
        }

        public async Task UpdateCategorySpecs(int categoryId, int specId, int valueId)
        {
            using (var conn = new SqlConnection(_appConfiguration.DatabaseSettings.ConnectionString))
            {
                var data = await conn.ExecuteAsync(sql: "sp_UpdateCategorySpecs",
                    param: new { @NopCategoryId = categoryId, @SpecId = specId, @ValueId = valueId },
                    commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<List<NopCategoryMappingDto>> GetMappedCategory(int categoryId, int page, int pageSize)
        {
            using (var conn = new SqlConnection(_appConfiguration.DatabaseSettings.ConnectionString))
            {
                var data = await conn.QueryAsync<NopCategoryMappingDto>(sql: "sp_GetNopCategoriesByPArams",
                    param: new { @CategoryId = categoryId, @PageNumber = page, @PageSize = pageSize },
                    commandType: CommandType.StoredProcedure);
                return data.ToList();
            }
        }

        public async Task AddCategoryMap(int categoryId, int nopCategoryId)
        {
            using (var conn = new SqlConnection(_appConfiguration.DatabaseSettings.ConnectionString))
            {
                await conn.ExecuteAsync(sql: "sp_InsertCategoryNopCategoryMapping",
                    param: new { @CategoryId = categoryId, @NopCategoryId = nopCategoryId },
                    commandType: CommandType.StoredProcedure);
            }
        }

        public async Task DeleteCategoryMap(int id)
        {
            var sqlStatement = "DELETE FROM Category_NopCategory_Mapping WHERE Id =@Id";
            using (var conn = new SqlConnection(_appConfiguration.DatabaseSettings.ConnectionString))
            {
                await conn.ExecuteAsync(sql: sqlStatement,
                    param: new { Id = id},
                    commandType: CommandType.Text);
            }
        }
    }
}