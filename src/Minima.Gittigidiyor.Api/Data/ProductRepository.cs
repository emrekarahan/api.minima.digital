﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Minima.Gittigidiyor.Api.Domain;
using Minima.Gittigidiyor.Api.Dto;
using Minima.Gittigidiyor.Api.Models.Response;
using Minima.Data.MongoDb.Repository;
using Padawan.Attributes;

namespace Minima.Gittigidiyor.Api.Data
{
    [Scoped]
    public class ProductRepository
    {
        private IMongoRepository<Product> _productRepository;

        public ProductRepository(IMongoRepository<Product> productRepository)
        {
            _productRepository = productRepository;
        }

        public GridModel<ProductDto> GetProducts(string productName, string sku, int page, int pageSize, string draw)
        {

            var countQuery = _productRepository.AsQueryable();

            var query = _productRepository.AsQueryable();

            if (!string.IsNullOrEmpty(productName))
            {
                query = query.Where(w => w.Name.ToLower().StartsWith(productName.ToLower()));
                countQuery = countQuery.Where(w => w.Name.ToLower().StartsWith(productName.ToLower()));
            }


            if (!string.IsNullOrEmpty(sku))
            {
                query = query.Where(w => w.Sku == sku);
                countQuery = countQuery.Where(w => w.Sku == sku);
            }
            
            query = query.Skip(pageSize * (page - 1))
                .Take(pageSize);

            var result = query.ToList();

            var response = MapDomainToDto(result);

            var dataCount = countQuery.Count();

            var gridModel = new GridModel<ProductDto>
            {
                Data = response,
                Draw = draw,
                RecordsFiltered = dataCount,
                RecordsTotal = dataCount,
            };

            return gridModel;
        }

        private List<ProductDto> MapDomainToDto(List<Product> result)
        {
            var response = result.Select(s => new ProductDto()
            {
                ProductId = s.ProductId,
                Name = s.Name,
                Id = s.Id.ToString(),
                ThumbUrl = (s.PictureModelList != null && s.PictureModelList.Count>0) ? s.PictureModelList.First().ImageUrl : null,
                Synced = s.Synced,
                SyncedAt = s.SyncedAt,
                Sku = s.Sku,
                StockQuantity = s.StockQuantity,
                Price = s.Price,
                GittigidiyorPrice = s.GittigidiyorPrice,
                Published = s.Published,
                GittigidiyorProductId = s.GittigidiyorProductId,
                GittigidiyorCategory =  (s.GittigidiyorCategoryList != null && s.GittigidiyorCategoryList.Count > 0) ? $"[{s.GittigidiyorCategoryList.First().CategoryCode}] - {s.GittigidiyorCategoryList.First().CategoryName}" : string.Empty,
                Status = s.Status

            });

            return response.ToList();
        }
    }
}