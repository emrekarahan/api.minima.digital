﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.OData.Client;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Media;
using Nop.Core.Domain.Seo;
using Nop.Core.Domain.Tax;
using Nop.Plugin.Misc.OData.Models;

namespace Minima.B2B.Client
{
    public class WebApiServices
    {
        private static string Token { get; set; }
        private static string StoreUrl { get; set; }
        private static Default.Container _container;

        public static void OnBuildingRequest(object sender, BuildingRequestEventArgs e)
        {
            e.Headers.Add("Authorization", "Bearer " + Token);
        }

        public static void InitContainer(string token, string storeurl)
        {
            Token = token;
            StoreUrl = storeurl;
            _container = new Default.Container(new Uri(StoreUrl + "/odata/"));
            _container.BuildingRequest += OnBuildingRequest;
            //container.MergeOption = MergeOption.NoTracking;
        }

        #region Customer
        public static async Task<IEnumerable<Customer>> GetCustomers()
        {
            var list = await _container.Customer.ExecuteAsync();
            return list;
        }
        #endregion

        #region Product
        public static async Task<Product> AddProduct(Product p)
        {
            _container.AddToProduct(p);

            await _container.SaveChangesAsync();
            return p;
        }
        public static async Task<Product> GetProduct()
        {
            var list = await _container.Product.ExecuteAsync();
            return list.FirstOrDefault();
        }
        public static async Task<Product> GetProduct(int id)
        {
            var list = await _container.Product.ExecuteAsync();
            return list.FirstOrDefault(x => x.Id == id);
        }

        //public static async Task<Product> GetProduct(string sku)
        //{
        //    var list = await _container.Product.Where(w => w.Sku == sku);
        //    return list.FirstOrDefault(x => x.Sku == sku);
        //}

        public static async Task UpdateProduct(Product product)
        {
            var productToUpdate = new DataServiceCollection<Product>(_container.Product.ExecuteAsync().Result);
            productToUpdate.FirstOrDefault(x => x.Id == product.Id).ProductCategories = product.ProductCategories;
            await _container.SaveChangesAsync(SaveChangesOptions.PostOnlySetProperties);
        }

        public static async Task InsertProductCategory(DataServiceCollection<ProductCategory> productCategory)
        {
            foreach (ProductCategory category in productCategory)
            {
                _container.AddToProductCategory(category);
            }
            await _container.SaveChangesAsync();
        }
        public static async Task AddToProductPicture(ProductPicture productPicture)
        {
            _container.AddToProductPicture(productPicture);
            await _container.SaveChangesAsync();
        }
        public static async Task<IEnumerable<Product>> GetProducts()
        {
            return await _container.Product.ExecuteAsync();
        }
        public static async Task DeleteProduct(Product model)
        {
            _container.DeleteObject(model);
            await _container.SaveChangesAsync();
        }
        #endregion

        #region Category
        public static async Task<IEnumerable<Category>> GetCategories()
        {
            return await _container.Category.ExecuteAsync();
        }
        public static async Task AddCategory(Category category)
        {
            _container.AddToCategory(category);
            await _container.SaveChangesAsync();
        }
        public static async Task UpdateCategory(Category category)
        {
            _container.UpdateObject(category);
            await _container.SaveChangesAsync();
        }
        public static async Task DeleteCategory(Category category)
        {
            _container.DeleteObject(category);
            await _container.SaveChangesAsync();
        }
        #endregion

        #region UrlRecord
        public static async Task AddUrlRecord(UrlRecord urlRecord)
        {
            _container.AddToUrlRecord(urlRecord);
            await _container.SaveChangesAsync();
        }
        #endregion

        #region Picture
        public static async Task AddPicture(PictureModel pictureModel)
        {
            _container.AddToPicture(pictureModel);
            await _container.SaveChangesAsync();
        }

        #endregion

        #region Tax

        public static async Task<TaxCategory> AddTaxCategory(TaxCategory taxCategory)
        {
            var taxCat = await GetTaxCategory(taxCategory.Name);
            if (taxCat != null)
                return taxCat;
            taxCategory.Name  = $"KDV %{taxCategory.Name}";
            _container.AddToTaxCategory(taxCategory);
            await _container.SaveChangesAsync();
            return taxCategory;
        }

        public static async Task<TaxCategory> GetTaxCategory(string taxtCategoryName)
        {
            string name = $"KDV %{taxtCategoryName}";
            var list = await _container.TaxCategory.ExecuteAsync();
            return list.FirstOrDefault(x => x.Name == name);
        }
        #endregion


    }
}
