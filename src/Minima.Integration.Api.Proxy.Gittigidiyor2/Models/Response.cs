﻿namespace Minima.Integration.Api.Proxy.Gittigidiyor.Models
{
    public class Response
    {
        public string AckCode { get; set; }
        public int ProductId { get; set; }
        public string Result { get; set; }
        public bool PayRequired { get; set; }
        public int DescriptionFilterStatus { get; set; }
        public bool ProductIdSpecified { get; set; }
        public Exceptions Exception { get; set; }
        public Errors Error { get; set; }
        public object Model { get; set; }

        public class Errors
        {
            public string ErrorId { get; set; }
            public string ErrorCode { get; set; }
            public string Message { get; set; }
            public string ViewMessage { get; set; }
        }

        public class Exceptions
        {
            public string Message { get; set; }
        }
    }

    
}