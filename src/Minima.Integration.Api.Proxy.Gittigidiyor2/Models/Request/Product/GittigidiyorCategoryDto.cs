﻿namespace Minima.Integration.Api.Proxy.Gittigidiyor.Models.Request.Product
{
    public class GittigidiyorCategoryDto
    {
        public string CategoryCode { get; set; }
        public string CategoryName { get; set; }

    }
}