﻿

namespace Minima.Integration.Api.Proxy.Gittigidiyor.Models.Request.Product
{
    public class CategoryDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}