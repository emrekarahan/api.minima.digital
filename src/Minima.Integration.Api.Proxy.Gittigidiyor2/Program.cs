﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Padawan.Extensions;

namespace Minima.Integration.Api.Proxy.Gittigidiyor
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .UsePadawan<Startup>("Minima.Integration.Api.Proxy.Gittigidiyor");
    }
}
