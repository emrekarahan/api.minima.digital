﻿using Padawan.Attributes;

namespace Minima.Integration.Api.Proxy.Gittigidiyor.Configurations
{
    [Configuration("GittigidiyorConfiguration")]
    public class GittigidiyorConfiguration
    {
        public string ApiKey { get; set; }
        public string Lang { get; set; }
        public bool ForceToSpecEntry { get; set; }
        public bool NextDateOption { get; set; }
        public string RoleName { get; set; }
        public string RolePass { get; set; }
        public string SecretKey { get; set; }
    }
}