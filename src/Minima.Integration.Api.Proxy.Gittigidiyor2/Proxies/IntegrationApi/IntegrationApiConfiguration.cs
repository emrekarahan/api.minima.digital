﻿using Padawan.Attributes;

namespace Minima.Integration.Api.Proxy.Gittigidiyor.Proxies.IntegrationApi
{
    [Configuration("IntegrationService")]
    public class IntegrationApiConfiguration
    {
        public string Url { get; set; }
    }
}
