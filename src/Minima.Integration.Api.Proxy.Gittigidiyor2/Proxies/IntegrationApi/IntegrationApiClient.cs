using System.Threading.Tasks;
using ApiV2Client.GittiGidiyor.Category;
using Microsoft.Extensions.Options;
using Padawan.Attributes;
using RestSharp;

namespace Minima.Integration.Api.Proxy.Gittigidiyor.Proxies.IntegrationApi
{

    [Singleton]
    public class IntegrationApiClient
    {
        private readonly IntegrationApiConfiguration _configuration;        

        public IntegrationApiClient(IntegrationApiConfiguration configuration)
        {
            _configuration = configuration;
            
        }

        public async Task UpdateProductSyncStatus(int productId, long syncedProductId)
        {
            try
            {
                var client = new RestClient($"{_configuration.Url}/api/Gittigidiyor/UpdateProductSyncStatus");
                var request = new RestRequest(Method.GET);
                request.AddQueryParameter("productId", productId.ToString())
                    .AddQueryParameter("syncedProductId", syncedProductId.ToString());
                var response = await client.ExecuteAsync(request);
            }
            catch
            {
                // ignored
            }
        }

        public async Task UpdateCategorySpecs(categorySpecWithDetailType[] resultSpecs, string code)
        {
            try
            {
                var client = new RestClient($"{_configuration.Url}/api/Gittigidiyor/UpdateCategorySpecs");
                var request = new RestRequest(Method.POST);
                request.AddHeader("code", code);
                request.AddJsonBody(resultSpecs);
            
                var response = await client.ExecuteAsync(request);
            }
            catch
            {
                // ignored
            }
        }

        public async Task UpdateProductError(int productId, string errorMessage)
        {
            try
            {
                var client = new RestClient($"{_configuration.Url}/api/Gittigidiyor/UpdateProductError");
                var request = new RestRequest(Method.GET);
                request.AddQueryParameter("productId", productId.ToString())
                    .AddQueryParameter("errorMessage", errorMessage);
                var response = await client.ExecuteAsync(request);
            }
            catch
            {
                // ignored
            }
        }
    }
}