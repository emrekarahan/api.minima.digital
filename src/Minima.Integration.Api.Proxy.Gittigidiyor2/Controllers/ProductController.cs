﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ApiV2Client.GittiGidiyor;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Minima.Integration.Api.Proxy.Gittigidiyor.Configurations;
using Minima.Integration.Api.Proxy.Gittigidiyor.Models;
using Minima.Integration.Api.Proxy.Gittigidiyor.Models.Request.Product;
using Minima.Integration.Api.Proxy.Gittigidiyor.Proxies.IntegrationApi;
using ProductService;
using cargoCompanyDetailType = ApiV2Client.GittiGidiyor.Product.cargoCompanyDetailType;
using cargoDetailType = ApiV2Client.GittiGidiyor.Product.cargoDetailType;
using cargoTimeType = ApiV2Client.GittiGidiyor.Product.cargoTimeType;
using photoType = ApiV2Client.GittiGidiyor.Product.photoType;
using productType = ApiV2Client.GittiGidiyor.Product.productType;
using specType = ApiV2Client.GittiGidiyor.Product.specType;

namespace Minima.Integration.Api.Proxy.Gittigidiyor.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    public class ProductController : Controller
    {
        private ILogger<ProductController> _logger;
        private readonly IntegrationApiClient _integrationApiClient;
        private readonly GittigidiyorConfiguration _gittigidiyorConfiguration;

        public ProductController(ILogger<ProductController> logger, IntegrationApiClient integrationApiClient, GittigidiyorConfiguration gittigidiyorConfiguration)
        {
            _logger = logger;
            _integrationApiClient = integrationApiClient;
            _gittigidiyorConfiguration = gittigidiyorConfiguration;
        }

        [Consumes("application/json")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        [HttpPost("InsertProduct")]
        // GET api/values
        public async Task<Response> InsertProduct([FromBody] ProductDto productDto)
        {

            if (productDto.PreventSync)
                return new Response
                {
                    Result = "Ürün gerekli şartları sağlamıyor"
                };


            IndividualProductServiceClient service = new IndividualProductServiceClient();
            Response response = new Response();

            var model = ProductInsert(productDto);
            var model2 = ProductInsert2(productDto);

            var time = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
            var sign = EncryptWithMd5(string.Concat(_gittigidiyorConfiguration.ApiKey,
                _gittigidiyorConfiguration.SecretKey, time));

            await service.insertProductWithNewCargoDetailAsync(_gittigidiyorConfiguration.ApiKey, sign, time,
                productDto.Id.ToString(), model, false, false, "tr");

            try
            {
                //var productService = ServiceProvider.getProductService();
                //var productServiceResponse = productService.insertProductWithNewCargoDetail(productDto.Id.ToString(), model, false, false, "tr");
                //response = new Response
                //{
                //    AckCode = productServiceResponse.ackCode,
                //    ProductId = productServiceResponse.productId,
                //    Result = productServiceResponse.result,
                //    PayRequired = productServiceResponse.payRequired,
                //    DescriptionFilterStatus = productServiceResponse.descriptionFilterStatus,
                //    ProductIdSpecified = productServiceResponse.productIdSpecified,
                //};

                //if (productServiceResponse.error == null)
                //{
                //    if (productServiceResponse.ackCode == "success")
                //    {
                //        await _integrationApiClient.UpdateProductSyncStatus(productDto.Id,
                //            productServiceResponse.productId);
                //    }
                //    else
                //    {
                //        await _integrationApiClient.UpdateProductError(productDto.Id,
                //            productServiceResponse.error.message);
                //    }


                //    return response;
                //};

                //response.Error = new Response.Errors
                //{
                //    Message = productServiceResponse.error.message,
                //    ErrorCode = productServiceResponse.error.errorCode,
                //    ErrorId = productServiceResponse.error.errorId,
                //    ViewMessage = productServiceResponse.error.viewMessage
                //};
                //response.Model = model;

                return response;
            }
            catch (Exception e)
            {
                response.AckCode = "failed";
                response.Exception = new Response.Exceptions { Message = e.Message };
                response.Model = model;
                return response;
            }
        }



        private string EncryptWithMd5(string clearString)
        {
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(clearString);
            byte[] hashBytes = md5.ComputeHash(inputBytes);

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hashBytes.Length; i++)
            {
                sb.Append(hashBytes[i].ToString("X2"));
            }
            return sb.ToString();
        }
        private string ProductInsert2(ProductDto productDto)
        {
            var sb = new StringBuilder();
            sb.AppendLine("<prod:insertProductWithNewCargoDetail>")
                .AppendLine($"<apiKey>{_gittigidiyorConfiguration.ApiKey}</apiKey>")
                .AppendLine("<sign>7c12f49d66da0567776488ef3675ca97</sign>")
                .AppendLine("<time>1290599997142</time>")
                .AppendLine($"<itemId>{productDto.Id}</itemId>")
                .AppendLine("<product>")
                .AppendLine($"<categoryCode>{productDto.GittigidiyorCategoryList.First().CategoryCode}</categoryCode>")
                .AppendLine("<storeCategoryId></storeCategoryId>");

            if (productDto.Name.Length > 100)
                sb.AppendLine($"<title>{productDto.Name.Substring(0, 100)}</title>");
            else
                sb.AppendLine($"<title>{productDto.Name}</title>");


            sb.AppendLine("<subtitle></subtitle>")
            .AppendLine("<specs>");
            foreach (SpecDto dto in productDto.SpecList)
                sb.AppendLine(
                    $"<spec name=\"{dto.Name}\" value=\"{dto.Value}\" type=\"{dto.Type}\" required=\"{dto.Required}\" />");
            sb.AppendLine("</specs>");

            sb.AppendLine("<photos>");
            var pictureList = productDto.PictureModelList.OrderBy(o => o.DisplayOrder).ToList();
            for (var i = 0; i < pictureList.Count; i++)
                sb.AppendLine($"<photo photoId=\"{i}\">")
                    .AppendLine(
                        $"<url>{pictureList[i].ImageUrl}</url>")
                    .AppendLine("<base64></base64>")
                    .AppendLine("</photo>");
            sb.AppendLine("</photos>");

            sb.AppendLine("<pageTemplate>3</pageTemplate>")
                .AppendLine($"<description>{productDto.FullDescription}</description>")
                .AppendLine($"<startDate>{DateTime.Now.AddMinutes(10):yyyy-MM-dd HH':'mm':'ss}</startDate>")
                .AppendLine("<catalogId></catalogId>")
                .AppendLine("<catalogDetail></catalogDetail>")
                .AppendLine("<catalogFilter></catalogFilter>")
                .AppendLine("<format>S</format>")
                .AppendLine("<startPrice></startPrice>")
                .AppendLine($"<buyNowPrice>{productDto.GittigidiyorPrice}</buyNowPrice>")
                .AppendLine("<netEarning></netEarning>")
                .AppendLine("<listingDays>360</listingDays>")
                .AppendLine($"<productCount>{productDto.StockQuantity}</productCount>")
                .AppendLine("<cargoDetail>")
                .AppendLine($"<city>{productDto.CityCode}</city>");


            if (productDto.GittigidiyorPrice >= 300)
            {
                sb.AppendLine("<shippingPayment>S</shippingPayment>");
            }
            else
            {
                sb.AppendLine("<shippingPayment>B</shippingPayment>");
            }


            sb.AppendLine("<shippingWhere>country</shippingWhere>")
            .AppendLine("<cargoCompanyDetails>")
            .AppendLine("<cargoCompanyDetail>")
            .AppendLine("<shippingFeePaymentType>PAY_IN_THE_BASKET</shippingFeePaymentType>")
            .AppendLine($"<name>{productDto.CargoCompany}</name>")
            //.AppendLine($"<cityPrice>{productDto.CityPrice}</cityPrice>")
            //.AppendLine($"<countryPrice>{productDto.CityPrice}</countryPrice>")
            .AppendLine($"<cityPrice>0</cityPrice>")
            .AppendLine($"<countryPrice>0</countryPrice>")
            .AppendLine("</cargoCompanyDetail>")
            .AppendLine("</cargoCompanyDetails>")
            .AppendLine("<shippingTime>")
            .AppendLine("<days>today</days>")
            .AppendLine("<beforeTime>16:30</beforeTime>")
            .AppendLine("</shippingTime>")
            .AppendLine("</cargoDetail>")
            .AppendLine("<affiliateOption>false</affiliateOption>")
            .AppendLine("<boldOption>false</boldOption>")
            .AppendLine("<catalogOption>false</catalogOption>")
            .AppendLine("<vitrineOption>false</vitrineOption>")
            .AppendLine("</product>")
            .AppendLine("<forceToSpecEntry>false</forceToSpecEntry>")
            .AppendLine("<nextDateOption>false</nextDateOption>")
            .AppendLine("<lang>tr</lang>")
            .AppendLine("</prod:insertProductWithNewCargoDetail>");
            return sb.ToString();
        }


        [Consumes("application/json")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        [HttpPost("UpdateStock")]
        public async Task<Response> UpdateStock([FromBody] ProductDto productDto)
        {
            Response response = new Response();

            var model = ProductInsert(productDto);

            try
            {
                var aaService = ServiceProvider.getProductService();


                var productServiceResponse = aaService.updateStock(productDto.GittigidiyorProductId.ToString(), "",
                     productDto.StockQuantity, true, _gittigidiyorConfiguration.Lang);

                response = new Response
                {
                    AckCode = productServiceResponse.ackCode,
                    ProductId = productServiceResponse.productId,
                    Result = productServiceResponse.result,
                    PayRequired = productServiceResponse.payRequired,
                    DescriptionFilterStatus = productServiceResponse.descriptionFilterStatus,
                    ProductIdSpecified = productServiceResponse.productIdSpecified,
                };

                if (productServiceResponse.error == null)
                {
                    if (productServiceResponse.ackCode == "success")
                        await _integrationApiClient.UpdateProductSyncStatus(productDto.Id,
                            productServiceResponse.productId);

                    return response;
                };

                response.Error = new Response.Errors
                {
                    Message = productServiceResponse.error.message,
                    ErrorCode = productServiceResponse.error.errorCode,
                    ErrorId = productServiceResponse.error.errorId,
                    ViewMessage = productServiceResponse.error.viewMessage
                };
                response.Model = model;

                return response;
            }
            catch (Exception e)
            {
                response.AckCode = "failed";
                response.Exception = new Response.Exceptions { Message = e.Message };
                response.Model = model;
                return response;
            }
        }



        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        [HttpGet("UpdateProductStock")]
        public async Task<Response> UpdateProductStock(long? gittigidiyorProductId, int stockQuantity)
        {
            Response response = new Response();

            try
            {
                var aaService = ServiceProvider.getProductService();


                var productServiceResponse = aaService.updateStock(gittigidiyorProductId.ToString(), "",
                    stockQuantity, true, _gittigidiyorConfiguration.Lang);

                response = new Response
                {
                    AckCode = productServiceResponse.ackCode,
                    ProductId = productServiceResponse.productId,
                    Result = productServiceResponse.result,
                    PayRequired = productServiceResponse.payRequired,
                    DescriptionFilterStatus = productServiceResponse.descriptionFilterStatus,
                    ProductIdSpecified = productServiceResponse.productIdSpecified,
                };

                //if (productServiceResponse.error == null)
                //{
                //    if (productServiceResponse.ackCode == "success")
                //        await _integrationApiClient.UpdateProductSyncStatus(productDto.Id,
                //            productServiceResponse.productId);

                //    return response;
                //};



                return response;
            }
            catch (Exception e)
            {
                response.AckCode = "failed";
                response.Exception = new Response.Exceptions { Message = e.Message };
                return response;
            }
        }

        [Consumes("application/json")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        [HttpPost("UpdatePrice")]
        public async Task<Response> UpdatePrice([FromBody] ProductDto productDto)
        {
            Response response = new Response();

            var model = ProductInsert(productDto);

            try
            {
                var productService = ServiceProvider.getProductService();


                var productServiceResponse = productService.updatePrice(productDto.GittigidiyorProductId.ToString(), "",
                     productDto.GittigidiyorPrice, false, _gittigidiyorConfiguration.Lang);

                response = new Response
                {
                    AckCode = productServiceResponse.ackCode,
                    ProductId = productServiceResponse.productId,
                    Result = productServiceResponse.result,
                    PayRequired = productServiceResponse.payRequired,
                    DescriptionFilterStatus = productServiceResponse.descriptionFilterStatus,
                    ProductIdSpecified = productServiceResponse.productIdSpecified,
                };

                if (productServiceResponse.error == null)
                {
                    if (productServiceResponse.ackCode == "success")
                        await _integrationApiClient.UpdateProductSyncStatus(productDto.Id,
                            productServiceResponse.productId);

                    return response;
                };

                response.Error = new Response.Errors
                {
                    Message = productServiceResponse.error.message,
                    ErrorCode = productServiceResponse.error.errorCode,
                    ErrorId = productServiceResponse.error.errorId,
                    ViewMessage = productServiceResponse.error.viewMessage
                };
                response.Model = model;

                return response;
            }
            catch (Exception e)
            {
                response.AckCode = "failed";
                response.Exception = new Response.Exceptions { Message = e.Message };
                response.Model = model;
                return response;
            }
        }

        private ProductService.productType ProductInsert(ProductDto product)
        {
            var productTypeModel = new ProductService.productType();
            var specTypeList = new List<ProductService.specType>();

            foreach (SpecDto dto in product.SpecList)
            {
                var spec = new ProductService.specType()
                { name = dto.Name, type = dto.Type, value = dto.Value, required = dto.Required };
                specTypeList.Add(spec);
            }

            var photoTypeList = new List<ProductService.photoType>();

            var pictureList = product.PictureModelList.OrderBy(o => o.DisplayOrder).ToList();
            for (int i = 0; i < pictureList.Count; i++)
            {
                var photo = new ProductService.photoType { photoId = i, url = pictureList[i].ImageUrl, photoIdSpecified = true };
                photoTypeList.Add(photo);
            }




            var cargoDetail = new ProductService.cargoDetailType { city = product.CityCode, cargoCompanies = new[] { product.CargoCompany } };

            var type = new ProductService.cargoCompanyDetailType
            {
                name = product.CargoCompany,
                //cityPrice = product.CityPrice, 
                cityPrice = "0",
                //countryPrice = product.CityPrice
                countryPrice = "0"
            };
            cargoDetail.cargoCompanyDetails = new ProductService.cargoCompanyDetailType[1];
            cargoDetail.cargoCompanyDetails[0] = type;


            if (productTypeModel.buyNowPrice >= 300)
            {
                cargoDetail.shippingPayment = "S";
            }
            else
            {
                cargoDetail.shippingPayment = "B";
            }

            
            cargoDetail.shippingFeePaymentType = "PAY_IN_THE_BASKET";
            cargoDetail.shippingWhere = "country";
            cargoDetail.shippingTime = new ProductService.cargoTimeType { days = "today", beforeTime = "16:30" };

            if (product.Name.Length > 100)
            {
                product.Name = product.Name.Substring(0, 100);
            }

            
            productTypeModel.specs = specTypeList.ToArray();
            productTypeModel.photos = photoTypeList.ToArray();
            productTypeModel.pageTemplate = 3;
            productTypeModel.pageTemplateSpecified = true;
            productTypeModel.description = product.FullDescription;
            productTypeModel.format = "S";
            productTypeModel.buyNowPrice = product.GittigidiyorPrice;
            productTypeModel.buyNowPriceSpecified = true;
            productTypeModel.listingDays = 360;
            productTypeModel.listingDaysSpecified = true;
            productTypeModel.productCount = product.StockQuantity;
            productTypeModel.productCountSpecified = true;
            productTypeModel.cargoDetail = cargoDetail;
            productTypeModel.startDate = DateTime.Now.AddMinutes(10).ToString("yyyy-MM-dd HH':'mm':'ss");
            productTypeModel.affiliateOption = false;
            productTypeModel.boldOption = false;
            productTypeModel.catalogOption = false;
            productTypeModel.vitrineOption = false;
            productTypeModel.categoryCode = product.GittigidiyorCategoryList.First().CategoryCode;

            return productTypeModel;
        }


        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        [HttpGet("getCategoryService")]
        public async Task<IActionResult> GetCategorySpecs(string code)
        {
            var categoryService = ServiceProvider.getCategoryService();
            var result = categoryService.getCategorySpecsWithDetail(code, "tr");
            if (result.ackCode == "success")
            {
                await _integrationApiClient.UpdateCategorySpecs(result.specs, code);
                return Ok(result.specs);
            }
            return Ok(result.ackCode);
        }

        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        [HttpGet("GetProductList")]
        public IActionResult GetProductList(string code)
        {
            var productService = ServiceProvider.getProductService();
            var result = productService.getProducts(0, 100, "L", true, _gittigidiyorConfiguration.Lang);
            if (result.ackCode == "success")
            {
                //await _integrationApiClient.UpdateCategorySpecs(result.specs, code);
                //return Ok(result.specs);
            }
            return Ok(result.ackCode);
        }

        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        [HttpGet("GetProduct")]
        public IActionResult GetProduct(string id)
        {
            var productService = ServiceProvider.getProductService();
            var result = productService.getProduct(string.Empty, id, "tr");
            if (result.ackCode == "success")
            {
                //await _integrationApiClient.UpdateCategorySpecs(result.specs, code);
                //return Ok(result.specs);
            }
            return Ok(result.ackCode);
        }

        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        [HttpGet("CalculatePriceForShoppingCart")]
        public IActionResult CalculatePriceForShoppingCart(List<int> productIds, List<string> itemIds)
        {
            var productService = ServiceProvider.getProductService();
            var result =
                productService.calculatePriceForShoppingCart(productIds, itemIds, _gittigidiyorConfiguration.Lang);
            if (result.ackCode == "success")
            {
                //await _integrationApiClient.UpdateCategorySpecs(result.specs, code);
                //return Ok(result.specs);
            }
            return Ok(result.ackCode);
        }

        //public IActionResult GetProduct(string id)
        //{
        //    var productService = ServiceProvider.getProductService();
        //    var result = productService.getProduct(string.Empty, id, "tr");
        //    if (result.ackCode == "success")
        //    {
        //        //await _integrationApiClient.UpdateCategorySpecs(result.specs, code);
        //        //return Ok(result.specs);
        //    }
        //    return Ok(result.ackCode);
        //}



        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        [HttpGet("FinishEarly")]
        public IActionResult FinishEarly()
        {
            var productService = ServiceProvider.getProductService();
            var result =
                productService.getProducts(0, 100, "A", false, "tr");
            if (result.ackCode == "success")
            {
                var pageCount = Math.Ceiling((result.productCount / 100d));
                for (int i = 0; i < pageCount; i++)
                {
                    var result2 =
                        productService.getProducts(i * 100, 100, "A", false, "tr");
                    var productList = result2.products.Select(s => s.productId).ToList();

                    Console.WriteLine($"{result2.productCount} ürün kaldı");

                    var vv = productService.finishEarly(productList, new List<string>(),
                        "tr");

                    if (vv.ackCode != "success")
                    {
                        Console.Write(vv);
                    }
                    else
                    {
                        Console.WriteLine(vv.result);
                    }
                    //else
                    //{

                    //    var aa = productService.deleteProduct(vv.productIdList.ToList().Select(s=> s.Value).ToList(), new List<string>(),
                    //        "tr");
                    //    Console.WriteLine($"{result2.productCount} üründen {aa.productCount} ürün silindi.");
                    //}

                    //Thread.Sleep(4000);
                }


            }
            return Ok(result.ackCode);
        }

        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        [HttpGet("DeleteListProducts")]
        public IActionResult ListProducts(string status)
        {
            var productService = ServiceProvider.getProductService();
            var result =
                productService.getProducts(0, 100, status, false, "tr");
            if (result.ackCode == "success")
            {
                var pageCount = Math.Ceiling((result.productCount / 100d));
                for (int i = 0; i < pageCount; i++)
                {
                    var result2 =
                        productService.getProducts(0, 100, status, false, "tr");
                    var productList = result2.products.Select(s => s.productId).ToList();

                    //var vv = productService.finishEarly(productList, new List<string>(),
                    //    "tr");

                    var aa = productService.deleteProduct(productList, new List<string>(),
                        "tr");
                    Console.WriteLine($"{result2.productCount} ürün kaldı");

                    if (aa.ackCode != "success")
                    {
                        Console.Write(aa);
                    }
                    else
                    {
                        Console.WriteLine(aa.result);
                    }
                }


            }
            return Ok(result.ackCode);
        }

        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        [HttpGet("GetSales")]
        public IActionResult GetSales(string status)
        {
            try
            {
                string startDate = DateTime.Now.ToString("dd/MM/yyyy 00:00:00", CultureInfo.InvariantCulture);
                string endDate = DateTime.Now.ToString("dd/MM/yyyy 23:59:00", CultureInfo.InvariantCulture);

                var salesService = ServiceProvider.getSaleService();
                var sales = salesService
                    .getSalesByDateRange(true, status, string.Empty, "A", "A", startDate,
                        endDate, 1, 100, "tr");

                return Ok(sales);
            }

            catch (Exception e)
            {
                throw;
            }
        }

        [HttpGet("MassUpdate")]
        public async Task<IActionResult> MassUpdate()
        {


            try
            {
                var time = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
                var sign = EncryptWithMd5(string.Concat(_gittigidiyorConfiguration.ApiKey,
                    _gittigidiyorConfiguration.SecretKey, time));
                IndividualProductServiceClient service = new IndividualProductServiceClient();

                var data = await service.getProductsAsync(_gittigidiyorConfiguration.ApiKey, sign, time, 0, 100, "A", true,
                    "tr");

                var response = data.products.First();
                response.product.cargoDetail.shippingFeePaymentType = "PAY_IN_THE_BASKET";
                response.product.cargoDetail.cargoCompanyDetails[0].cityPrice = "0";
                response.product.cargoDetail.cargoCompanyDetails[0].countryPrice = "0";

                time = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
                sign = EncryptWithMd5(string.Concat(_gittigidiyorConfiguration.ApiKey,
                    _gittigidiyorConfiguration.SecretKey, time));

                var updateResponse  = await service.updateProductWithNewCargoDetailAsync(_gittigidiyorConfiguration.ApiKey, sign, time,
                    response.itemId, response.productId.ToString(), response.product, true, false, false, "tr");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return Ok();
        }


        //[HttpGet("MassUpdate")]
        //public async Task<IActionResult> MassUpdate()
        //{


        //    try
        //    {


        //        var salesService = ServiceProvider.getProductService();
        //        var products = salesService.getProducts();
                
        //    }
        //    catch (Exception e)
        //    {
        //        Console.WriteLine(e);
        //        throw;
        //    }

        //    return Ok();
        //}
    }
}
