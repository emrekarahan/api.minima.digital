﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using ApiV2Client.GittiGidiyor;
using ApiV2Client.GittiGidiyor.Product;
using Dapper;
using GGCategoryFix.Model;
using Minima.Core.Toolkit;
using RestSharp;
using RestSharp.Authenticators;
using RestSharp.Serializers;

namespace GGCategoryFix
{
    class Program
    {
        private static List<GGCategory> categories;
        private static string connection =
            "Data Source=159.69.47.176;Initial Catalog=gokce;Integrated Security=False;Persist Security Info=False;User ID=sa;Password=be02062012@A";

        private static GittigidiyorSettings _gittigidiyorSettings;
        private static string ApiUrl = "https://dev.gittigidiyor.com:8443/listingapi/ws/IndividualProductService?wsdl";

        static void Main(string[] args)
        {
            _gittigidiyorSettings = new GittigidiyorSettings
            {
                ApiKey = "RhqsGzm8exNebfkhc5ZU3bAbj7WMffDT",
                Lang = "tr",
                ForceToSpecEntry = false,
                NextDateOption = false,
                RoleName = "frost_majere",
                RolePass = "9TVvFz9te2uA9mheFKzp6ppkpjpdVUsv",
                SecretKey = "XQyAvX8pUSz8RpeE"
            };

            //CategoryUpdater();
            var model = ProductInsert();



            

            var time = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
            var sign = HashHelper.EncryptWithMd5(string.Concat(_gittigidiyorSettings.ApiKey,
                _gittigidiyorSettings.SecretKey, time));


           
            //IServiceProvider serviceProvider = InitServiceProvider();

            //IndividualProductService ss = new IndividualProductServiceClient(new BasicHttpBinding(BasicHttpSecurityMode.Transport),
            //    new EndpointAddress(
            //        new Uri("https://dev.gittigidiyor.com:8443/listingapi/ws/IndividualProductService?wsdl")));
            //{
            //    Endpoint =
            //    {
            //        Binding = new BasicHttpBinding
            //        {
            //            Security = HttpTransportSecurity()

            //        }
            //    }
            //};




            try
            {
                ProductService aaService = ServiceProvider.getProductService();
                var response = aaService.insertProductWithNewCargoDetail("21032", model, false, false, "tr");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
           


            //var stringwriter = new System.IO.StringWriter();
            //var serializer = new System.Xml.Serialization.XmlSerializer(model.GetType());
            //serializer.Serialize(stringwriter, model);
            //var aa = stringwriter.ToString();



            //var client = new RestClient(ApiUrl)
            //{
            //    Authenticator =
            //        new HttpBasicAuthenticator(_gittigidiyorSettings.RoleName, _gittigidiyorSettings.RolePass)

            //};
            //client.AddHandler("application/json", () => NewtonsoftJsonSerializer.Default);


            //var builder = new StringBuilder().Append("/listingapi/rlws/individual/product")
            //    .Append("?method=insertProduct")
            //    .Append("&outputCT=json")
            //    .Append("&inputCT=xml")
            //    .Append($"&apiKey={_gittigidiyorSettings.ApiKey}")
            //    .Append($"&sign={sign}")
            //    .Append($"&time={time}")
            //    //.Append($"&itemId={model.ProductId}")
            //    .Append($"&lang={_gittigidiyorSettings.Lang}")
            //    .Append("&forceToSpecEntry=false")
            //    .Append("&nextDateOption=false");

            //var request = new RestRequest(new Uri($"{ApiUrl}{builder}"), Method.POST)
            //{
            //    RequestFormat = DataFormat.Xml,
            //    XmlSerializer = new DotNetXmlSerializer(),
            //    OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; }
            //};
            //request.AddXmlBody(model);
            //var response = client.Post(request);
        }

        private static productType ProductInsert()
        {
            var productTypeModel = new productType();
            //var request = new Request();
            var brandSpec = new specType() { name = "Marka", type = "Combo", value = "", required = true };

            var statusSpec = new specType { name = "Durum", type = "Combo", value = "Sıfır", required = true };

            var specTypeList = new specType[] { brandSpec, statusSpec };

            var photoTypeList = new photoType[]
            {
                new photoType()
                {
                    photoId = 0,
                    url = "https://1766772022.rsc.cdn77.org/images/thumbs/0021626_19-agustos-2005-yarim-bilet-pyb1179.jpeg"
                }
            };

            var cargoDetail = new cargoDetailType()
            {
                city = "34",
                cargoCompanies = new[] { "aras" },
                cargoCompanyDetails = new cargoCompanyDetailType[]
                {
                    new cargoCompanyDetailType()
                    {
                        name = "aras",
                        cityPrice = "8.00",
                        countryPrice = "10.00"
                    }
                },
                shippingPayment = "B",
                shippingWhere = "city",
                shippingTime = new cargoTimeType()
                {
                    days = "today"
                }
            };

            var product = new productType()
            {
                categoryCode = "fdp",
                title = "19 Ağustos 2005 Yarım Bilet PYB1179",
                specs = specTypeList,
                photos = photoTypeList,
                pageTemplate = 1,
                description = "asdsajhdsaj hjdksahdjk ashdjkhsajkdhsajkdhkas",
                format = "S",
                buyNowPrice = 4d,
                listingDays = 30,
                productCount = 1,
                cargoDetail = cargoDetail,
                startDate = DateTime.Now.AddMinutes(1).ToString("yyyy-MM-dd HH':'mm':'ss"),
                affiliateOption = false,
                boldOption = false,
                catalogOption = false,
                vitrineOption = false,
            };


            return productTypeModel;
        }

        static void FixCategory()
        {
            foreach (var item in categories)
            {
                if (item.Depth == 1)
                    continue;

                var itemParentCategory = item.CategoryCode.Substring(0, item.CategoryCode.Length - 1);
                var parentCategory = GetCategoryByCategoryCode(itemParentCategory);
                if (parentCategory != null)
                {
                    UpdateParentCategory(parentCategory.Id, item.Id);
                }
            }
        }

        private static void UpdateParentCategory(in int parentCategoryId, in int itemId)
        {
            try
            {
                using (var conn = new SqlConnection(connection))
                {
                    conn.Execute("UPDATE [GG_Category] SET ParentId = @ParentId WHERE Id = @Id",
                        new
                        {
                            ParentId = parentCategoryId,
                            Id = itemId
                        });
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

        }

        private static void CategoryUpdater()
        {
            using (var conn = new SqlConnection(connection))
            {
                categories = conn.Query<GGCategory>("SELECT * FROM [GG_Category] WHERE ParentId IS NULL").ToList();
            }
            FixCategory();
        }

        private static GGCategory GetCategoryByCategoryCode(string code)
        {
            using (var conn = new SqlConnection(connection))
            {
                var category = conn.QueryFirstOrDefault<GGCategory>("SELECT * FROM [GG_Category] WHERE CategoryCode = @code", new { code });
                return category;
            }
        }
    }
}
