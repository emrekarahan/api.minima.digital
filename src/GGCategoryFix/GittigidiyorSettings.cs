﻿using Padawan.Attributes;

namespace GGCategoryFix
{
    [Configuration("GittigidiyorSettings")]
    public class GittigidiyorSettings
    {
        public string ApiKey { get; set; }
        public string SecretKey { get; set; }
        public string RolePass { get; set; }
        public string RoleName { get; set; }
        public string Lang { get; set; }
        public object ForceToSpecEntry { get; set; }
        public object NextDateOption { get; set; }
    }
}