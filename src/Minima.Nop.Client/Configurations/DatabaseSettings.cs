﻿using Padawan.Attributes;

namespace Minima.Nop.Client.Configurations
{
    [Configuration("NopDatabaseSettings")]
    public class NopCommerceSettings
    {
        public string Connection { get; set; }
    }
}