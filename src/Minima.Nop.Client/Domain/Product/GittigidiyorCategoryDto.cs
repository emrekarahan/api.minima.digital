﻿namespace Minima.Nop.Client.Domain.Product
{
    public class GittigidiyorCategoryDto
    {
        public string CategoryCode { get; set; }
        public string CategoryName { get; set; }

    }
}