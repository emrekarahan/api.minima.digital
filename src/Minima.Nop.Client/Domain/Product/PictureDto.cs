﻿namespace Minima.Nop.Client.Domain.Product
{
    public class PictureDto
    {
        public int Id { get; set; }
        public string MimeType { get; set; }
        public string ImageUrl { get; set; }
        public int DisplayOrder { get; set; }
    }
}