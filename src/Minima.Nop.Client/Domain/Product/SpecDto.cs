﻿namespace Minima.Nop.Client.Domain.Product
{
    public class SpecDto
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public string Value { get; set; }
        public bool Required { get; set; }

    }
}
