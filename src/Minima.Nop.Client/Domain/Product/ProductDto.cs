﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Minima.Nop.Client.Domain.Product
{
    
    public class ProductDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ShortDescription { get; set; }
        public string FullDescription { get; set; }
        public string MetaKeywords { get; set; }
        public string MetaDescription { get; set; }
        public string MetaTitle { get; set; }
        public string Sku { get; set; }
        public string Gtin { get; set; }
        public int StockQuantity { get; set; }
        public decimal Price { get; set; }
        public bool Published { get; set; }

        public List<CategoryDto> CategoryModelList { get; set; }
        public List<GittigidiyorCategoryDto> GittigidiyorCategoryList { get; set; }
        public List<PictureDto> PictureModelList { get; set; }
        public List<SpecDto> SpecList { get; set; }
        public decimal GittigidiyorPrice { get; set; }
        public string CityPrice { get; set; }
        public decimal Commission { get; set; }
        public string ShippingWhere { get; set; }
        public string CityCode { get; set; }
        public string CargoCompany { get; set; }
        public long? GittigidiyorProductId { get; set; }
        public bool Synced { get; set; }
        public DateTime? SyncedAt { get; set; }
        public bool PreventSync { get; set; }
        public bool IsSyncable =>
            !PreventSync ||
            StockQuantity > 1 ||
            (PictureModelList != null && PictureModelList.Any()) ||
            (GittigidiyorCategoryList != null && GittigidiyorCategoryList.Any());

        public string Status { get; set; }
    }
}
