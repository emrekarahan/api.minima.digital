﻿

namespace Minima.Nop.Client.Domain.Product
{
    public class CategoryDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}