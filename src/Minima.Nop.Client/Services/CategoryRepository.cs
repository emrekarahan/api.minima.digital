﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Dapper;
using Minima.Nop.Client.Configurations;
using Minima.Nop.Client.Constants;
using Minima.Nop.Client.Models;
using Padawan.Attributes;

namespace Minima.Nop.Client.Services
{
    [Singleton]
    public class CategoryRepository
    {
        private readonly NopCommerceSettings _nopCommerceSettings;

        public CategoryRepository(NopCommerceSettings nopCommerceSettings)
        {
            _nopCommerceSettings = nopCommerceSettings;
        }

        public async Task<int> InsertCategory(string name, string description, string metaKeywords, string metaDescription, string metaTitle, int parentCategoryId, int displayOrder, DateTime createdOnUtc, DateTime updatedOnUtc)
        {
            using (var conn = new SqlConnection(_nopCommerceSettings.Connection))
            {
                return await conn.QueryFirstAsync<int>(SqlStatements.InsertCategorySql, new
                {
                    @Name = name,
                    @Description = description,
                    @MetaKeywords = metaKeywords,
                    @MetaDescription = metaDescription,
                    @MetaTitle = metaTitle,
                    @ParentCategoryId = parentCategoryId,
                    @DisplayOrder = displayOrder,
                    @CreatedOnUtc = createdOnUtc,
                    @UpdatedOnUtc = updatedOnUtc
                }, null, null, CommandType.Text);
            }
        }
    }
}