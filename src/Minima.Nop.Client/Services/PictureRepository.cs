﻿using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Dapper;
using Minima.Nop.Client.Configurations;
using Minima.Nop.Client.Constants;
using Padawan.Attributes;

namespace Minima.Nop.Client.Services
{
    [Singleton]
    public class PictureRepository
    {
        private readonly NopCommerceSettings _databaseSettings;

        public PictureRepository(NopCommerceSettings databaseSettings)
        {
            _databaseSettings = databaseSettings;
        }

        public async Task<int> InsertPicture(string mimeType, string seoFilename)
        {
            int id;
            using (var conn = new SqlConnection(_databaseSettings.Connection))
            {
                id = await conn.QuerySingleAsync<int>(SqlStatements.InsertPictureSql, new
                    {
                        @MimeType = mimeType,
                        @SeoFilename = seoFilename

                }, null, null,
                    CommandType.Text);
            }
            return id;
        }

        public async Task InsertPictureProductMapping(int productId, int pictureId, int displayOrder)
        {
            using (var conn = new SqlConnection(_databaseSettings.Connection))
            {
                await conn.ExecuteAsync(SqlStatements.InsertPictureProductMappingSql, new
                    {
                        @ProductId = productId,
                        @PictureId = pictureId,
                        @DisplayOrder = displayOrder

                    }, null, null,
                    CommandType.Text);
            }
         
        }
    }
}