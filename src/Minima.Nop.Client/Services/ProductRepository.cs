﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Minima.Nop.Client.Configurations;
using Minima.Nop.Client.Constants;
using Minima.Nop.Client.Domain.Product;
using Minima.Nop.Client.Models;
using Padawan.Attributes;

namespace Minima.Nop.Client.Services
{
    [Singleton]
    public class ProductRepository
    {
        private readonly NopCommerceSettings _databaseSettings;

        public ProductRepository(NopCommerceSettings databaseSettings)
        {
            _databaseSettings = databaseSettings;
        }

        public async Task<int> InsertProduct(DapperProductModel model)
        {
            int id;
            using (var conn = new SqlConnection(_databaseSettings.Connection))
            {
                id = await conn.QuerySingleAsync<int>(SqlStatements.InsertProductSql, model, null, null,
                    CommandType.Text);
            }
            return id;
        }

        public async Task<DapperProductModel> CheckProductBySku(string sku)
        {
            using (var conn = new SqlConnection(_databaseSettings.Connection))
            {
                return await conn.QueryFirstAsync<DapperProductModel>(SqlStatements.CheckProductBySkuSql, new { @Sku = sku }, null, null, CommandType.Text);
            }
        }

        public async Task UpdateProduct(DapperProductModel model)
        {
            using (var conn = new SqlConnection(_databaseSettings.Connection))
            {
                await conn.ExecuteAsync(SqlStatements.UpdateProductSql, new
                {
                    @Name = model.Name,
                    @ShortDescription = model.ShortDescription,
                    @FullDescription = model.FullDescription,
                    @MetaKeywords = model.MetaKeywords,
                    @MetaDescription = model.MetaDescription,
                    @MetaTitle = model.MetaTitle,
                    @Sku = model.Sku,
                    @Gtin = model.Gtin,
                    @StockQuantity = model.StockQuantity,
                    @Price = model.Price,
                    @UpdatedOnUtc = model.UpdatedOnUtc,
                    @Id = model.Id
                }, null, null, CommandType.Text);
            }
        }

        public async Task InsertProductCategory(List<ProductCategoryModel> productCategoryModel)
        {
            using (var conn = new SqlConnection(_databaseSettings.Connection))
            {
                foreach (ProductCategoryModel model in productCategoryModel)
                {
                    await conn.ExecuteAsync(SqlStatements.InsertProductCategoryMappingSql,
                        new { ProductId = model.ProductId, CategoryId = model.CategoryId }, null, null,
                        CommandType.Text);
                }
            }
        }

        public async Task ClearProductCategory(int productId)
        {
            using (var conn = new SqlConnection(_databaseSettings.Connection))
            {
                await conn.ExecuteAsync(SqlStatements.ClearProductCategorySql, new { Id = productId }, null, null,
                    CommandType.Text);
            }
        }

        public async Task<int> InsertTaxCategory(int tax)
        {
            var taxCategoryName = $"KDV %{tax}";

            using (var conn = new SqlConnection(_databaseSettings.Connection))
            {
                var id = await conn.QuerySingleOrDefaultAsync<int?>(SqlStatements.CheckTaxCategorySql, new { name = taxCategoryName }, null, null,
                      CommandType.Text);


                if (id.HasValue && id.Value != 0) return id.Value;

                var newId = await conn.QuerySingleAsync<int>(SqlStatements.InsertTaxCategorySql, new { name = taxCategoryName }, null, null,
                    CommandType.Text);
                return newId;

            }
        }



        public async Task<List<int>> GetAvaliabledProducts()
        {
            using (var conn = new SqlConnection(_databaseSettings.Connection))
            {
                var sql = "SELECT p.Id FROM dbo.Product p WHERE p.StockQuantity >=1";
                var result = await conn.QueryAsync<int>(sql);
                return result.ToList();
            }
        }

        public async Task<ProductDto> PrepareProductForIntegration(int productId)
        {
            using (var conn = new SqlConnection(_databaseSettings.Connection))
            {
                var sqlStatement = "sp_GetProductForMongo";
                var result = await conn.QueryMultipleAsync(sqlStatement, new { ProductId = productId },
                    null, null, commandType: CommandType.StoredProcedure);

                ProductDto product = await MapGridReaderToProduct(result);
                return product;
            }
        }

        private async Task<ProductDto> MapGridReaderToProduct(SqlMapper.GridReader result)
        {
            var product = await result.ReadFirstOrDefaultAsync<ProductDto>();
            if (product == null)
                return null;

            var categoryDto = await result.ReadAsync<CategoryDto>();

            if (categoryDto != null)
                product.CategoryModelList = categoryDto.ToList();

            var pictureDto = await result.ReadAsync<PictureDto>();
            if (pictureDto != null)
                product.PictureModelList = pictureDto.ToList();
            

            var gittiGidiyorCategory = await result.ReadAsync<GittigidiyorCategoryDto>();
            if (gittiGidiyorCategory != null)
                product.GittigidiyorCategoryList = gittiGidiyorCategory.ToList();

            var spec = await result.ReadAsync<SpecDto>();
            if (spec != null)
                product.SpecList = spec.ToList();

            return product;
        }


        public async Task UpdateProductStock(int internalProductId, int ideaStockAmount)
        {
            using (var conn = new SqlConnection(_databaseSettings.Connection))
            {
                var sqlStatement = "UPDATE Product SET StockQuantity = @stock WHERE Id = @id";
                var result = await conn.ExecuteAsync(sqlStatement, new { stock = internalProductId, id = ideaStockAmount },
                    null, null, commandType: CommandType.Text);
            }
        }
    }
}
