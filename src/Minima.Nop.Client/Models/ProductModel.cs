﻿using System;

namespace Minima.Nop.Client.Models
{
    public class DapperProductModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ShortDescription { get; set; }
        public string Sku { get; set; }
        public decimal Price { get; set; }
        public int ProductTypeId { get; set; }
        public decimal ProductCost { get; set; }
        public int StockQuantity { get; set; }
        public bool Published { get; set; }
        public int ProductType { get; set; }
        public DateTimeOffset CreatedOnUtc { get; set; }
        public DateTimeOffset UpdatedOnUtc { get; set; }
        public string MetaDescription { get; set; }
        public string MetaKeywords { get; set; }
        public int TaxCategoryId { get; set; }
        public bool IsShipEnabled { get; set; }
        public decimal AdditionalShippingCharge { get; set; }
        public string Gtin { get; set; }
        public string FullDescription { get; set; }
        public string MetaTitle { get; set; }
    }
}