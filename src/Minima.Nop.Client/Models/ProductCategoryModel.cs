﻿namespace Minima.Nop.Client.Models
{
    public class ProductCategoryModel
    {
        public int ProductId { get; set; }
        public int CategoryId { get; set; }
    }
}