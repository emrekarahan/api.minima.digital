﻿using System.Xml.Schema;
using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Catalog.Types
{
    [XmlType(Namespace = "http://catalogv2.anonymous.ws.listingapi.gg.com")]
    public class catalogSearchCriteriaType
    {
        [XmlElement(Form = XmlSchemaForm.Unqualified, Order = 0)]
        public string keyword { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified, Order = 1)]
        public string categoryCode { get; set; }

        [XmlArray(Form = XmlSchemaForm.Unqualified, Order = 2)]
        [XmlArrayItem("specCriteria", Form = XmlSchemaForm.Unqualified, IsNullable = false)]
        public specType[] specCriterias { get; set; }

        [XmlArray(Form = XmlSchemaForm.Unqualified, Order = 3)]
        [XmlArrayItem("specFacetName", Form = XmlSchemaForm.Unqualified, IsNullable = false)]
        public string[] specFacetNames { get; set; }
    }
}