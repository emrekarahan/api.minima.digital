﻿using System.Xml.Schema;
using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Catalog.Types
{
    [XmlType(Namespace = "http://catalogv2.anonymous.ws.listingapi.gg.com")]
    public class simpleCatalogType
    {
        /// <remarks />
        [XmlElement(Form = XmlSchemaForm.Unqualified, Order = 0)]
        public int catalogId { get; set; }

        /// <remarks />
        [XmlIgnore]
        public bool catalogIdSpecified { get; set; }

        /// <remarks />
        [XmlElement(Form = XmlSchemaForm.Unqualified, Order = 1)]
        public string catalogName { get; set; }

        /// <remarks />
        [XmlElement(Form = XmlSchemaForm.Unqualified, Order = 2)]
        public categoryType category { get; set; }
    }
}