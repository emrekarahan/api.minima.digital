﻿using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Catalog.Types
{
    [XmlType(Namespace = "http://catalogv2.anonymous.ws.listingapi.gg.com")]
    public class specValueFacetType
    {
        [XmlAttribute()]
        public string value { get; set; }

        [XmlAttribute()]
        public long count { get; set; }

        [XmlIgnore()]
        public bool countSpecified { get; set; }
    }
}
