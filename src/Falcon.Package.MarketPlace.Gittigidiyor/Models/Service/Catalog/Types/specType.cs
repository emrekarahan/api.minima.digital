﻿using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Catalog.Types
{
    [XmlType(Namespace = "http://catalogv2.anonymous.ws.listingapi.gg.com")]
    public class specType
    {
        [XmlAttribute]
        public string name { get; set; }

        [XmlAttribute]
        public string value { get; set; }

        [XmlAttribute]
        public string type { get; set; }

        [XmlAttribute]
        public bool required { get; set; }

        [XmlIgnore]
        public bool requiredSpecified { get; set; }
    }
}