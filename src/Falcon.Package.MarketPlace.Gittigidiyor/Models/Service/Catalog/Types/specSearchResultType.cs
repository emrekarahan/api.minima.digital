﻿using System.Xml.Schema;
using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Catalog.Types
{
    
    [XmlType(Namespace = "http://catalogv2.anonymous.ws.listingapi.gg.com")]
    public class specSearchResultType
    {
        [XmlArray(Form = XmlSchemaForm.Unqualified, Order = 0)]
        [XmlArrayItem("facet", Form = XmlSchemaForm.Unqualified)]
        public specValueFacetType[] facets { get; set; }
        
        [XmlAttribute()]
        public string name { get; set; }
    }
}
