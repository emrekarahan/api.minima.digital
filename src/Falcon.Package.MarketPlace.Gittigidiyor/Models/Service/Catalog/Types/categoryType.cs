﻿using System.Xml.Schema;
using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Catalog.Types
{
    [XmlType(Namespace = "http://catalogv2.anonymous.ws.listingapi.gg.com")]
    public class categoryType
    {
        [XmlElement(Form = XmlSchemaForm.Unqualified, Order = 0)]
        public int categoryAttributeId { get; set; }
        
        [XmlIgnore]
        public bool categoryAttributeIdSpecified { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified, Order = 1)]
        public string categoryCode { get; set; }
        
        [XmlElement(Form = XmlSchemaForm.Unqualified, Order = 2)]
        public string categoryName { get; set; }
        
        [XmlElement(Form = XmlSchemaForm.Unqualified, Order = 3)]
        public string categoryPerma { get; set; }
        
        [XmlElement(Form = XmlSchemaForm.Unqualified, Order = 4)]
        public bool active { get; set; }
        
        [XmlIgnore]
        public bool activeSpecified { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified, Order = 5)]
        public bool hasCatalog { get; set; }

        
        [XmlIgnore]
        public bool hasCatalogSpecified { get; set; }
        
        [XmlElement(Form = XmlSchemaForm.Unqualified, Order = 6)]
        public bool hasSpec { get; set; }
        
        [XmlIgnore]
        public bool hasSpecSpecified { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified, Order = 7)]
        public int orderNumber { get; set; }
        
        [XmlIgnore]
        public bool orderNumberSpecified { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified, Order = 8)]
        public int installmentNumber { get; set; }
        
        [XmlIgnore]
        public bool installmentNumberSpecified { get; set; }
    }
}