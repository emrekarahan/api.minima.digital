﻿using System.Xml.Serialization;
using Falcon.Package.MarketPlace.Gittigidiyor.Attributes;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Catalog.Responses;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Catalog.Types;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Catalog.Requests
{
    [UrlFragment("CatalogV2Service")]
    [ResponseType(typeof(SearchCatalogResponse))]
    [XmlRoot("searchCatalog", Namespace = "http://catalogv2.anonymous.ws.listingapi.gg.com", IsNullable = true)]

    public class SearchCatalogRequest : BaseRequest
    {

        [XmlElement("page", Namespace = "")]
        public int Page;

        [XmlElement("rowCount", Namespace = "")]
        public int RowCount;

        [XmlElement("criteria", Namespace = "")]
        public catalogSearchCriteriaType Criteria;

        
        public SearchCatalogRequest()
        {
        }

        public SearchCatalogRequest(int page, int rowCount, catalogSearchCriteriaType criteria, string lang)
        {
            this.Page = page;
            this.RowCount = rowCount;
            this.Criteria = criteria;
            this.Lang = lang;
        }
    }
}
