﻿using System.Xml.Schema;
using System.Xml.Serialization;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Catalog.Types;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Catalog.Responses
{
    [XmlType("catalogSearchServiceResponse", Namespace = "http://catalogv2.anonymous.ws.listingapi.gg.com")]
    public class CatalogSearchServiceResponse : BaseResponseService
    {
        [XmlElement("totalCount", Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 0)]
        public int? TotalCount { get; set; }

        [XmlIgnore()]
        // ReSharper disable once InconsistentNaming
        public bool totalCountSpecified { get; set; }

        [XmlArray("catalogs", Form = XmlSchemaForm.Unqualified, Order = 1)]
        [XmlArrayItem("catalog", Form = XmlSchemaForm.Unqualified)]
        public simpleCatalogType[] Catalogs { get; set; }

        [XmlArray("specFacets", Form = XmlSchemaForm.Unqualified, Order = 2)]
        [XmlArrayItem("specFacet", Form = XmlSchemaForm.Unqualified)]
        public specSearchResultType[] SpecFacets { get; set; }
    }
}
