﻿using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Catalog.Responses
{
    [XmlRoot("searchCatalogResponse", Namespace = "http://category.anonymous.ws.listingapi.gg.com")]
    public class SearchCatalogResponse
    {

        [XmlElement("return", Namespace = "", Order = 0)]
        public CatalogSearchServiceResponse CatalogSearchServiceResponse;

        public SearchCatalogResponse()
        {
        }

        public SearchCatalogResponse(CatalogSearchServiceResponse @return)
        {
            this.CatalogSearchServiceResponse = @return;
        }
    }
}
