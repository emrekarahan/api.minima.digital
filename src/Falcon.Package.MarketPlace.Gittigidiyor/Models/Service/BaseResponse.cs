﻿using System.Xml.Schema;
using System.Xml.Serialization;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Responses;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service
{
    //[System.Xml.Serialization.XmlIncludeAttribute(typeof(productUpdateSalesResponse))]
    //[System.Xml.Serialization.XmlIncludeAttribute(typeof(productFinishReasonListResponse))]
    //[System.Xml.Serialization.XmlIncludeAttribute(typeof(productServiceImageStatusResponse))]
    //[System.Xml.Serialization.XmlIncludeAttribute(typeof(itemIdDetailResponse))]
    //[System.Xml.Serialization.XmlIncludeAttribute(typeof(productServiceBooleanResponse))]
    //[System.Xml.Serialization.XmlIncludeAttribute(typeof(productServiceVariantResponse))]
    //[System.Xml.Serialization.XmlIncludeAttribute(typeof(productServiceRetailResponse))]
    //[System.Xml.Serialization.XmlIncludeAttribute(typeof(productServiceStatusResponse))]
    [XmlInclude(typeof(ProductServiceListResponse))]
    //[System.Xml.Serialization.XmlIncludeAttribute(typeof(productServiceDetailResponse))]
    //[System.Xml.Serialization.XmlIncludeAttribute(typeof(productServiceResponse))]
    //[System.Xml.Serialization.XmlIncludeAttribute(typeof(productServiceSpecResponse))]
    //[System.Xml.Serialization.XmlIncludeAttribute(typeof(productServiceDescResponse))]
    //[System.Xml.Serialization.XmlIncludeAttribute(typeof(productServiceStockResponse))]
    //[System.Xml.Serialization.XmlIncludeAttribute(typeof(productServicePaymentResponse))]
    //[System.Xml.Serialization.XmlIncludeAttribute(typeof(productServicePriceResponse))]
    //[System.Xml.Serialization.XmlIncludeAttribute(typeof(productServiceIdResponse))]
    //[System.Xml.Serialization.XmlIncludeAttribute(typeof(productServiceProcessResponse))]
    [XmlType(Namespace = "https://product.individual.ws.listingapi.gg.com")]
    public class BaseResponseService
    {
        [XmlElement("ackCode", Form = XmlSchemaForm.Unqualified, Order = 0)]
        public string AckCode { get; set; }

        [XmlElement("responseTime", Form = XmlSchemaForm.Unqualified, Order = 1)]
        public string ResponseTime { get; set; }

        [XmlElement("error", Form = XmlSchemaForm.Unqualified, Order = 2)]
        public ErrorTypeService Error { get; set; }


        [XmlElement("timeElapsed", Form = XmlSchemaForm.Unqualified, Order = 3)]
        public string TimeElapsed { get; set; }
    }
}
