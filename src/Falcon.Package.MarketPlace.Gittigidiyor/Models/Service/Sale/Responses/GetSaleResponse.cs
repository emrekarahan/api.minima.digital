﻿using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Sale.Responses
{
    [XmlRoot("getSaleResponse", Namespace = "http://sale.individual.ws.listingapi.gg.com")]
    public class GetSaleResponse
    {

        [XmlElement("return", Namespace = "", Order = 0)]
        public SaleServiceResponse SaleServiceResponse;

        public GetSaleResponse()
        {
        }

        public GetSaleResponse(SaleServiceResponse @return)
        {
            this.SaleServiceResponse = @return;
        }
    }
}
