﻿using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Sale.Responses
{
    [XmlRoot("getSalesByDateRangeResponse", Namespace = "http://sale.individual.ws.listingapi.gg.com")]
    public class GetSalesByDateRangeResponse
    {

        [XmlElement("return", Namespace = "", Order = 0)]
        public SaleServicePagingResponse SaleServicePagingResponse;

        public GetSalesByDateRangeResponse()
        {
        }

        public GetSalesByDateRangeResponse(SaleServicePagingResponse @return)
        {
            this.SaleServicePagingResponse = @return;
        }
    }
}
