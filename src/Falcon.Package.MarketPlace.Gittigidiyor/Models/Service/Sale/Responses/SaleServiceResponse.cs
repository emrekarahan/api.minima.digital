﻿using System.Xml.Schema;
using System.Xml.Serialization;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Sale.Types;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Sale.Responses
{
    [XmlInclude(typeof(SaleServicePagingResponse))]
    [XmlType("saleServiceResponse", Namespace = "http://sale.individual.ws.listingapi.gg.com")]
    public class SaleServiceResponse : BaseResponseService
    {
        [XmlElement("saleCount", Form = XmlSchemaForm.Unqualified, Order = 0)]
        public int SaleCount { get; set; }

        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool saleCountSpecified { get; set; }

        [XmlArray("sales", Form = XmlSchemaForm.Unqualified, Order = 1)]
        [XmlArrayItem("sale", Form = XmlSchemaForm.Unqualified)]
        public SaleType[] Sales { get; set; }

        [XmlArray("sellerPromotionInfos", Form = XmlSchemaForm.Unqualified, Order = 2)]
        [XmlArrayItem("sellerPromotionInfo", Form = XmlSchemaForm.Unqualified)]
        public SellerPromotionInfoType[] SellerPromotionInfos { get; set; }
    }
}