﻿using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Sale.Responses
{
    [XmlRoot("getPagedSalesResponse", Namespace = "http://sale.individual.ws.listingapi.gg.com")]
    public class GetPagedSalesResponse
    {
        [XmlElement("return", Namespace = "", Order = 0)]
        public SaleServicePagingResponse SaleServicePagingResponse;

        public GetPagedSalesResponse()
        {
            
        }

        public GetPagedSalesResponse(SaleServicePagingResponse @return)
        {
            SaleServicePagingResponse = @return;
        }
    }
}
