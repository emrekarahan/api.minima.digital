﻿using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Sale.Responses
{
    [XmlType("saleServicePagingResponse", Namespace = "http://sale.individual.ws.listingapi.gg.com")]
    public class SaleServicePagingResponse : SaleServiceResponse
    {
        [XmlElement("nextPageAvailable", Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 0)]
        public bool NextPageAvailable { get; set; }
    }
}
