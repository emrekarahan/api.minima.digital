﻿using System.Xml.Schema;
using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Sale.Types
{
    [XmlType("sellerPromotionItem", Namespace = "http://sale.individual.ws.listingapi.gg.com")]
    public class SellerPromotionItem
    {
        [XmlElement("itemId", Form = XmlSchemaForm.Unqualified, Order = 0)]
        public string ItemId { get; set; }

        [XmlArray("mainProducts", Form = XmlSchemaForm.Unqualified, Order = 1)]
        [XmlArrayItem("mainProduct", Form = XmlSchemaForm.Unqualified, IsNullable = false)]
        public MainProductInfoDto[] MainProducts { get; set; }

        [XmlArray("gifts", Form = XmlSchemaForm.Unqualified, Order = 2)]
        [XmlArrayItem("gift", Form = XmlSchemaForm.Unqualified, IsNullable = false)]
        public GiftProductInfoDto[] Gifts { get; set; }
    }
}