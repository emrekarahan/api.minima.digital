﻿using System.Xml.Schema;
using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Sale.Types
{
    [XmlType("variantSpecDto", Namespace = "http://sale.individual.ws.listingapi.gg.com")]
    public class VariantSpecDto
    {
        [XmlElement("soldQuantity", Form = XmlSchemaForm.Unqualified, Order = 0)]
        public int SoldQuantity { get; set; }

        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool soldQuantitySpecified { get; set; }

        [XmlElement("quantity", Form = XmlSchemaForm.Unqualified, Order = 1)]
        public int Quantity { get; set; }

        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool quantitySpecified { get; set; }

        [XmlElement("name", Form = XmlSchemaForm.Unqualified, Order = 2)]
        public string Name { get; set; }

        [XmlElement("value", Form = XmlSchemaForm.Unqualified, Order = 3)]
        public string Value { get; set; }

        [XmlElement("stockCode", Form = XmlSchemaForm.Unqualified, Order = 4)]
        public string StockCode { get; set; }

        [XmlElement("order", Form = XmlSchemaForm.Unqualified, Order = 5)]
        public int Order { get; set; }

        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool orderSpecified { get; set; }

        [XmlElement("specDataId", Form = XmlSchemaForm.Unqualified, Order = 6)]
        public int SpecDataId { get; set; }

        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool specDataIdSpecified { get; set; }

        [XmlElement("specId", Form = XmlSchemaForm.Unqualified, Order = 7)]
        public int SpecId { get; set; }

        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool specIdSpecified { get; set; }
    }
}