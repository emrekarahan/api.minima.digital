﻿using System.Xml.Schema;
using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Sale.Types
{
    [XmlType("variantDto", Namespace = "http://sale.individual.ws.listingapi.gg.com")]
    public class VariantDto
    {
        [XmlElement("image", Form = XmlSchemaForm.Unqualified, Order = 0)]
        public ImageDto Image { get; set; }

        [XmlElement("productId", Form = XmlSchemaForm.Unqualified, Order = 1)]
        public int ProductId { get; set; }

        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool productIdSpecified { get; set; }

        [XmlElement("quantity", Form = XmlSchemaForm.Unqualified, Order = 2)]
        public int Quantity { get; set; }

        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool quantitySpecified { get; set; }

        [XmlElement("soldQuantity", Form = XmlSchemaForm.Unqualified, Order = 3)]
        public int SoldQuantity { get; set; }

        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool soldQuantitySpecified { get; set; }

        [XmlArray("specs", Form = XmlSchemaForm.Unqualified, Order = 4)]
        [XmlArrayItem("spec", Form = XmlSchemaForm.Unqualified, IsNullable = false)]
        public VariantSpecDto[] Specs { get; set; }

        [XmlElement("stockCode", Form = XmlSchemaForm.Unqualified, Order = 5)]
        public string StockCode { get; set; }

        [XmlElement("variantId", Form = XmlSchemaForm.Unqualified, Order = 6)]
        public long VariantId { get; set; }

        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool variantIdSpecified { get; set; }

        [XmlElement("catalog", Form = XmlSchemaForm.Unqualified, Order = 7)]
        public ProductCatalogDto Catalog { get; set; }
    }
}