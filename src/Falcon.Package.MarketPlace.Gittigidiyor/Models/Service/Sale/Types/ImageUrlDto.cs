﻿using System.Xml.Schema;
using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Sale.Types
{
    [XmlType("imageUrlDto", Namespace = "http://sale.individual.ws.listingapi.gg.com")]
    public class ImageUrlDto
    {
        [XmlElement("imageUrl", Form = XmlSchemaForm.Unqualified, Order = 0)]
        public string ImageUrl { get; set; }

        [XmlElement("size", Form = XmlSchemaForm.Unqualified, Order = 1)]
        public string Size { get; set; }
    }
}