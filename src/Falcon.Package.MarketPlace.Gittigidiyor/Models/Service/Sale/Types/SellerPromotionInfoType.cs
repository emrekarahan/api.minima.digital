﻿using System.Xml.Schema;
using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Sale.Types
{
    [XmlType("sellerPromotionInfoType", Namespace = "http://sale.individual.ws.listingapi.gg.com")]
    public class SellerPromotionInfoType
    {
        [XmlArray("sales", Form = XmlSchemaForm.Unqualified, Order = 0)]
        [XmlArrayItem("sale", Form = XmlSchemaForm.Unqualified)]
        public SaleType[] Sales { get; set; }

        [XmlArray("saleSellerPromotions", Form = XmlSchemaForm.Unqualified, Order = 1)]
        [XmlArrayItem("saleSellerPromotion", Form = XmlSchemaForm.Unqualified)]
        public SaleSellerPromotionType[] SaleSellerPromotions { get; set; }
    }
}