﻿using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Sale.Types
{
    [XmlType("giftPromotionType", Namespace = "http://sale.individual.ws.listingapi.gg.com")]
    public enum GiftPromotionType
    {
        COUNT,
        PERCENTAGE,
        PRICE,
        FIXED_PRICE
    }
}