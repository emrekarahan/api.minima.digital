﻿using System;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Sale.Types
{
    [XmlType("mainProductInfoDto", Namespace = "http://sale.individual.ws.listingapi.gg.com")]
    public class MainProductInfoDto
    {
        [XmlElement("productId", Form = XmlSchemaForm.Unqualified, Order = 0)]
        public int ProductId { get; set; }

        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool productIdSpecified { get; set; }

        [XmlElement("quantity", Form = XmlSchemaForm.Unqualified, Order = 1)]
        public int Quantity { get; set; }

        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool quantitySpecified { get; set; }

        [XmlElement("retailVariantId", Form = XmlSchemaForm.Unqualified, Order = 2)]
        public long RetailVariantId { get; set; }

        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool retailVariantIdSpecified { get; set; }

        [XmlElement("title", Form = XmlSchemaForm.Unqualified, Order = 3)]
        public string Title { get; set; }

        [XmlArray("images", Form = XmlSchemaForm.Unqualified, Order = 4)]
        [XmlArrayItem("image", Form = XmlSchemaForm.Unqualified, IsNullable = false)]
        public ImageDto[] Images { get; set; }

        [XmlElement("startDate", Form = XmlSchemaForm.Unqualified, Order = 5)]
        public DateTime StartDate { get; set; }

        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool startDateSpecified { get; set; }

        [XmlElement("endDate", Form = XmlSchemaForm.Unqualified, Order = 6)]
        public DateTime EndDate { get; set; }

        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool endDateSpecified { get; set; }

        [XmlElement("stockQuantity", Form = XmlSchemaForm.Unqualified, Order = 7)]
        public int StockQuantity { get; set; }

        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool stockQuantitySpecified { get; set; }

        [XmlElement("productUrl", Form = XmlSchemaForm.Unqualified, Order = 8)]
        public string ProductUrl { get; set; }
    }
}