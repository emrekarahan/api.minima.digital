﻿using System.Xml.Schema;
using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Sale.Types
{
    [XmlType("saleSellerPromotionType", Namespace = "http://sale.individual.ws.listingapi.gg.com")]
    public class SaleSellerPromotionType
    {
        [XmlElement("discountedPrice", Form = XmlSchemaForm.Unqualified, Order = 0)]
        public decimal DiscountedPrice { get; set; }

        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool discountedPriceSpecified { get; set; }

        [XmlElement("price", Form = XmlSchemaForm.Unqualified, Order = 1)]
        public decimal Price { get; set; }

        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool priceSpecified { get; set; }

        [XmlElement("productId", Form = XmlSchemaForm.Unqualified, Order = 2)]
        public int ProductId { get; set; }

        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool productIdSpecified { get; set; }

        /// <remarks />
        [XmlElement("productUrl", Form = XmlSchemaForm.Unqualified, Order = 3)]
        public string ProductUrl { get; set; }

        [XmlElement("title", Form = XmlSchemaForm.Unqualified, Order = 4)]
        public string Title { get; set; }

        [XmlElement("quantity", Form = XmlSchemaForm.Unqualified, Order = 5)]
        public int Quantity { get; set; }

        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool quantitySpecified { get; set; }

        [XmlElement("retailVariantId", Form = XmlSchemaForm.Unqualified, Order = 6)]
        public int RetailVariantId { get; set; }

        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool retailVariantIdSpecified { get; set; }

        [XmlElement("saleSellerPromotionId", Form = XmlSchemaForm.Unqualified, Order = 7)]
        public int SaleSellerPromotionId { get; set; }

        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool saleSellerPromotionIdSpecified { get; set; }

        /// <remarks />
        [XmlElement("sellerPromotion", Form = XmlSchemaForm.Unqualified, Order = 8)]
        public SellerPromotion SellerPromotion { get; set; }
    }
}