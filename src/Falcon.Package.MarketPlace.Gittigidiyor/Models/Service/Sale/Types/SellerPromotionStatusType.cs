﻿using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Sale.Types
{
    [XmlType("sellerPromotionStatusType", Namespace = "http://sale.individual.ws.listingapi.gg.com")]
    public enum SellerPromotionStatusType
    {

        ACTIVE,
        PASSIVE,
        DRAFT,
        PLANNED,
    }
}
