﻿using System;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Sale.Types
{
    [XmlType("giftProductInfoDto", Namespace = "http://sale.individual.ws.listingapi.gg.com")]
    public class GiftProductInfoDto
    {
        [XmlElement("productId", Form = XmlSchemaForm.Unqualified, Order = 0)]
        public int ProductId { get; set; }

        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool productIdSpecified { get; set; }

        [XmlElement("retailVariantId", Form = XmlSchemaForm.Unqualified, Order = 1)]
        public long RetailVariantId { get; set; }

        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool retailVariantIdSpecified { get; set; }

        [XmlElement("retailVariant", Form = XmlSchemaForm.Unqualified, Order = 2)]
        public VariantDto RetailVariant { get; set; }

        [XmlElement("type", Form = XmlSchemaForm.Unqualified, Order = 3)]
        public GiftPromotionType Type { get; set; }

        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool typeSpecified { get; set; }

        /// <remarks />
        [XmlElement("value", Form = XmlSchemaForm.Unqualified, Order = 4)]
        public decimal Value { get; set; }

        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool valueSpecified { get; set; }

        [XmlElement("title", Form = XmlSchemaForm.Unqualified, Order = 5)]
        public string Title { get; set; }

        [XmlElement("images", Form = XmlSchemaForm.Unqualified, Order = 6)]
        public ImageDto[] Images { get; set; }

        [XmlElement("startDate", Form = XmlSchemaForm.Unqualified, Order = 7)]
        public DateTime StartDate { get; set; }

        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool startDateSpecified { get; set; }

        /// <remarks />
        [XmlElement("endDate", Form = XmlSchemaForm.Unqualified, Order = 8)]
        public DateTime EndDate { get; set; }

        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool endDateSpecified { get; set; }

        [XmlElement("stockQuantity", Form = XmlSchemaForm.Unqualified, Order = 9)]
        public int StockQuantity { get; set; }

        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool stockQuantitySpecified { get; set; }

        [XmlElement("productUrl", Form = XmlSchemaForm.Unqualified, Order = 10)]
        public string ProductUrl { get; set; }

        
        [XmlElement("possibleGiftVariants", Form = XmlSchemaForm.Unqualified, Order = 11)]
        public VariantDto[] PossibleGiftVariants { get; set; }
    }
}