﻿using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Sale.Types
{
    [XmlType(Namespace = "http://sale.individual.ws.listingapi.gg.com")]
    public class ItemVariantSpecType
    {
        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlAttribute("value")]
        public string Value { get; set; }

        [XmlAttribute("quantity")]
        public int Quantity { get; set; }

        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool quantitySpecified { get; set; }

        [XmlAttribute("soldQuantity")]
        public int SoldQuantity { get; set; }

        /// <remarks />
        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool soldQuantitySpecified { get; set; }

        /// <remarks />
        [XmlAttribute("stockCode")]
        public string StockCode { get; set; }
    }
}