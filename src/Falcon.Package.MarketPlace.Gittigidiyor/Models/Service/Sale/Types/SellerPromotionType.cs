﻿using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Sale.Types
{
    [XmlType("sellerPromotionType", Namespace = "http://sale.individual.ws.listingapi.gg.com")]
    public enum SellerPromotionType
    {

        SAME_PRODUCT,
        DIFFERENT_PRODUCT,
        SAME_OR_DIFFERENT_PRODUCT,
    }
}
