﻿using System.Xml.Schema;
using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Sale.Types
{
    [XmlType("neighborhoodType", Namespace = "http://sale.individual.ws.listingapi.gg.com")]
    public class NeighborhoodType
    {
        [XmlElement("neighborhoodId", Form = XmlSchemaForm.Unqualified, Order = 0)]
        public int NeighborhoodId { get; set; }

        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool neighborhoodIdSpecified { get; set; }

        [XmlElement("neighborhoodName", Form = XmlSchemaForm.Unqualified, Order = 1)]
        public string NeighborhoodName { get; set; }

        [XmlElement("districtId", Form = XmlSchemaForm.Unqualified, Order = 2)]
        public int DistrictId { get; set; }

        /// <remarks />
        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool districtIdSpecified { get; set; }

        /// <remarks />
        [XmlElement("active", Form = XmlSchemaForm.Unqualified, Order = 3)]
        public bool Active { get; set; }

        /// <remarks />
        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool activeSpecified { get; set; }
    }
}