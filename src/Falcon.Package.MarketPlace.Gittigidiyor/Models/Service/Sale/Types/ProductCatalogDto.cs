﻿using System.Xml.Schema;
using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Sale.Types
{
    [XmlType("productCatalogDto", Namespace = "http://sale.individual.ws.listingapi.gg.com")]
    public class ProductCatalogDto
    {
        [XmlElement("newCatalogId", Form = XmlSchemaForm.Unqualified, Order = 0)]
        public int NewCatalogId { get; set; }

        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool newCatalogIdSpecified { get; set; }
    }
}