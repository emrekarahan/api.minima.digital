﻿using System.Xml.Schema;
using System.Xml.Serialization;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Sale.Types;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Sale.Types
{
    [XmlType("saleType", Namespace = "http://sale.individual.ws.listingapi.gg.com")]
    public class SaleType
    {
        [XmlElement("saleCode", Form = XmlSchemaForm.Unqualified, Order = 0)]
        public string SaleCode { get; set; }

        [XmlElement("status", Form = XmlSchemaForm.Unqualified, Order = 1)]
        public string Status { get; set; }

        [XmlElement("statusCode", Form = XmlSchemaForm.Unqualified, Order = 2)]
        public string StatusCode { get; set; }

        [XmlElement("productId", Form = XmlSchemaForm.Unqualified, Order = 3)]
        public int ProductId { get; set; }

        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool productIdSpecified { get; set; }

        [XmlElement("productTitle", Form = XmlSchemaForm.Unqualified, Order = 4)]
        public string ProductTitle { get; set; }

        [XmlElement("price", Form = XmlSchemaForm.Unqualified, Order = 5)]
        public string Price { get; set; }

        [XmlElement("cargoPayment", Form = XmlSchemaForm.Unqualified, Order = 6)]
        public string CargoPayment { get; set; }

        [XmlElement("cargoCode", Form = XmlSchemaForm.Unqualified, Order = 7)]
        public string CargoCode { get; set; }

        [XmlElement("amount", Form = XmlSchemaForm.Unqualified, Order = 8)]
        public int Amount { get; set; }

        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool amountSpecified { get; set; }

        [XmlElement("endDate", Form = XmlSchemaForm.Unqualified, Order = 9)]
        public string EndDate { get; set; }

        [XmlElement("buyerInfo", Form = XmlSchemaForm.Unqualified, Order = 10)]
        public OrderBuyerInfoType BuyerInfo { get; set; }

        [XmlElement("changeStatus", Form = XmlSchemaForm.Unqualified, Order = 11)]
        public string ChangeStatus { get; set; }

        [XmlElement("thumbImageLink", Form = XmlSchemaForm.Unqualified, Order = 12)]
        public string ThumbImageLink { get; set; }

        [XmlElement("lastActionDate", Form = XmlSchemaForm.Unqualified, Order = 13)]
        public string LastActionDate { get; set; }

        [XmlArray("waitingProcesses", Form = XmlSchemaForm.Unqualified, Order = 14)]
        [XmlArrayItem("waitingProcess", Form = XmlSchemaForm.Unqualified)]
        public int?[] WaitingProcesses { get; set; }

        [XmlElement("invoiceInfo", Form = XmlSchemaForm.Unqualified, Order = 15)]
        public SaleInvoiceType InvoiceInfo { get; set; }

        [XmlElement("variantId", Form = XmlSchemaForm.Unqualified, Order = 16)]
        public long VariantId { get; set; }

        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool variantIdSpecified { get; set; }

        [XmlArray("variantSpecs", Form = XmlSchemaForm.Unqualified, Order = 17)]
        [XmlArrayItem("variantSpec", Form = XmlSchemaForm.Unqualified)]
        public ItemVariantSpecType[] VariantSpecs { get; set; }

        [XmlElement("moneyDate", Form = XmlSchemaForm.Unqualified, Order = 18)]
        public string MoneyDate { get; set; }

        [XmlElement("cargoApprovementDate", Form = XmlSchemaForm.Unqualified, Order = 19)]
        public string CargoApprovementDate { get; set; }

        [XmlElement("itemId", Form = XmlSchemaForm.Unqualified, Order = 20)]
        public string ItemId { get; set; }

        [XmlElement("shippingInfo", Form = XmlSchemaForm.Unqualified, Order = 21)]
        public ShippingInfoTypeReturn ShippingInfo { get; set; }

        [XmlElement("commissionRate",Form = XmlSchemaForm.Unqualified, Order = 22)]
        public double CommissionRate { get; set; }

        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool commissionRateSpecified { get; set; }
    }
}