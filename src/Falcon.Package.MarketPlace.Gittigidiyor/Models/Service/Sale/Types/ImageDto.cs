﻿using System.Xml.Schema;
using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Sale.Types
{
    [XmlType("imageDto", Namespace = "http://sale.individual.ws.listingapi.gg.com")]
    public class ImageDto
    {
        [XmlElement("imageId", Form = XmlSchemaForm.Unqualified, Order = 0)]
        public int ImageId { get; set; }

        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool imageIdSpecified { get; set; }

        [XmlArray("imageUrls", Form = XmlSchemaForm.Unqualified, Order = 1)]
        [XmlArrayItem("imageUrl", Form = XmlSchemaForm.Unqualified, IsNullable = false)]
        public ImageUrlDto[] ImageUrls { get; set; }

        [XmlElement("zoomInSupport", Form = XmlSchemaForm.Unqualified, Order = 2)]
        public bool ZoomInSupport { get; set; }

        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool zoomInSupportSpecified { get; set; }
    }
}