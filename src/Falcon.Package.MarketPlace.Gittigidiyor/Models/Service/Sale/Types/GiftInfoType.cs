﻿using System.Xml.Schema;
using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Sale.Types
{
    [XmlType("giftInfoType", Namespace = "http://sale.individual.ws.listingapi.gg.com")]
    public class GiftInfoType
    {
        [XmlElement("appliesTo", Form = XmlSchemaForm.Unqualified, Order = 0)]
        public AppliesTo AppliesTo { get; set; }

        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool appliesToSpecified { get; set; }

        [XmlElement("type", Form = XmlSchemaForm.Unqualified, Order = 1)]
        public Type Type { get; set; }

        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool typeSpecified { get; set; }

        [XmlElement("value", Form = XmlSchemaForm.Unqualified, Order = 2)]
        public decimal Value { get; set; }

        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool valueSpecified { get; set; }
    }
}