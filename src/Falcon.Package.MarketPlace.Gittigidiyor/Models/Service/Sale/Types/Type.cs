﻿using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Sale.Types
{
    [XmlType("type", Namespace = "http://sale.individual.ws.listingapi.gg.com")]
    public enum Type
    {
        COUNT,
        PERCENTAGE,
        PRICE,
        FIXED_PRICE,
    }
}
