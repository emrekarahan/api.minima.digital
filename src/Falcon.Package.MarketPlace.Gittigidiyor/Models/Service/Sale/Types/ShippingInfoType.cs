﻿using System.Xml.Schema;
using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Sale.Types
{
    [XmlType("shippingInfoType", Namespace = "http://sale.individual.ws.listingapi.gg.com")]
    public class ShippingInfoType
    {
        [XmlElement("deliveryOption", Form = XmlSchemaForm.Unqualified, Order = 0)]
        public string DeliveryOption { get; set; }

        [XmlElement("shippingFirmName", Form = XmlSchemaForm.Unqualified, Order = 1)]
        public string ShippingFirmName { get; set; }

        [XmlElement("shippingFirmId", Form = XmlSchemaForm.Unqualified, Order = 2)]
        public int ShippingFirmId { get; set; }

        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool shippingFirmIdSpecified { get; set; }

        [XmlElement("combinedShipping", Form = XmlSchemaForm.Unqualified, Order = 3)]
        public string CombinedShipping { get; set; }

        [XmlElement("shippingPaymentType", Form = XmlSchemaForm.Unqualified, Order = 4)]
        public ShippingPaymentType ShippingPaymentType { get; set; }

        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool shippingPaymentTypeSpecified { get; set; }

        [XmlElement("cargoCode", Form = XmlSchemaForm.Unqualified, Order = 5)]
        public string CargoCode { get; set; }

        [XmlElement("shippingNotice", Form = XmlSchemaForm.Unqualified, Order = 6)]
        public string ShippingNotice { get; set; }

        [XmlArray("combinedSaleCodes", Form = XmlSchemaForm.Unqualified, Order = 7)]
        [XmlArrayItem("saleCode", Form = XmlSchemaForm.Unqualified, IsNullable = false)]
        public string[] CombinedSaleCodes { get; set; }

        [XmlElement("shippingExpireDate", Form = XmlSchemaForm.Unqualified, Order = 8)]
        public string ShippingExpireDate { get; set; }
    }
}