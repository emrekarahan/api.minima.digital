﻿using System;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Sale.Types
{
    [XmlType("sellerPromotion", Namespace = "http://sale.individual.ws.listingapi.gg.com")]
    public class SellerPromotion
    {
        [XmlElement("basketLimit", Form = XmlSchemaForm.Unqualified, Order = 0)]
        public int BasketLimit { get; set; }

        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool basketLimitSpecified { get; set; }

        [XmlElement("endDate", Form = XmlSchemaForm.Unqualified, Order = 1)]
        public DateTime EndDate { get; set; }

        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool endDateSpecified { get; set; }

        [XmlElement("giftInfo", Form = XmlSchemaForm.Unqualified, Order = 2)]
        public GiftInfoType GiftInfo { get; set; }

        [XmlElement("giftQuantity", Form = XmlSchemaForm.Unqualified, Order = 3)]
        public int GiftQuantity { get; set; }

        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool giftQuantitySpecified { get; set; }

        [XmlElement("insertedAt", Form = XmlSchemaForm.Unqualified, Order = 4)]
        public DateTime InsertedAt { get; set; }

        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool insertedAtSpecified { get; set; }

        [XmlElement("items", Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 5)]
        public SellerPromotionItem[] Items { get; set; }

        [XmlElement("mainProductQuantity", Form = XmlSchemaForm.Unqualified, Order = 6)]
        public int MainProductQuantity { get; set; }

        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool mainProductQuantitySpecified { get; set; }

        [XmlElement("name", Form = XmlSchemaForm.Unqualified, Order = 7)]
        public string Name { get; set; }

        [XmlElement("selectedGiftVariantId", Form = XmlSchemaForm.Unqualified, Order = 8)]
        public int SelectedGiftVariantId { get; set; }

        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool selectedGiftVariantIdSpecified { get; set; }

        [XmlElement("sellerIds", Form = XmlSchemaForm.Unqualified, IsNullable = true, Order = 9)]
        public int?[] SellerIds { get; set; }

        [XmlElement("sellerPromotionType", Form = XmlSchemaForm.Unqualified, Order = 10)]
        public SellerPromotionType SellerPromotionType { get; set; }

        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool sellerPromotionTypeSpecified { get; set; }

        [XmlElement("startDate", Form = XmlSchemaForm.Unqualified, Order = 11)]
        public DateTime StartDate { get; set; }

        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool startDateSpecified { get; set; }

        [XmlElement("status", Form = XmlSchemaForm.Unqualified, Order = 12)]
        public SellerPromotionStatusType Status { get; set; }

        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool statusSpecified { get; set; }

        [XmlElement("updatedAt", Form = XmlSchemaForm.Unqualified, Order = 13)]
        public DateTime UpdatedAt { get; set; }

        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool updatedAtSpecified { get; set; }

        [XmlElement("usageLimit", Form = XmlSchemaForm.Unqualified, Order = 14)]
        public int UsageLimit { get; set; }

        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool usageLimitSpecified { get; set; }
    }
}