﻿using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Sale.Types
{
    [XmlType("appliesTo", Namespace = "http://sale.individual.ws.listingapi.gg.com")]
    public enum AppliesTo
    {
        TOTAL,
        GIFT,
    }
}
