﻿using System.Xml.Schema;
using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Sale.Types
{
    [XmlType("saleInvoiceType", Namespace = "http://sale.individual.ws.listingapi.gg.com")]
    public class SaleInvoiceType
    {
        [XmlElement("fullname", Form = XmlSchemaForm.Unqualified, Order = 0)]
        public string Fullname { get; set; }

        [XmlElement("address", Form = XmlSchemaForm.Unqualified, Order = 1)]
        public string Address { get; set; }

        [XmlElement("district", Form = XmlSchemaForm.Unqualified, Order = 2)]
        public string District { get; set; }

        [XmlElement("cityCode", Form = XmlSchemaForm.Unqualified, Order = 3)]
        public int CityCode { get; set; }

        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool cityCodeSpecified { get; set; }

        [XmlElement("zipCode", Form = XmlSchemaForm.Unqualified, Order = 4)]
        public string ZipCode { get; set; }

        [XmlElement("phoneNumber", Form = XmlSchemaForm.Unqualified, Order = 5)]
        public string PhoneNumber { get; set; }

        [XmlElement("taxOffice", Form = XmlSchemaForm.Unqualified, Order = 6)]
        public string TaxOffice { get; set; }

        [XmlElement("taxNumber", Form = XmlSchemaForm.Unqualified, Order = 7)]
        public string TaxNumber { get; set; }

        [XmlElement("companyTitle", Form = XmlSchemaForm.Unqualified, Order = 8)]
        public string CompanyTitle { get; set; }

        [XmlElement("tcCertificate", Form = XmlSchemaForm.Unqualified, Order = 9)]
        public string TcCertificate { get; set; }
    }
}