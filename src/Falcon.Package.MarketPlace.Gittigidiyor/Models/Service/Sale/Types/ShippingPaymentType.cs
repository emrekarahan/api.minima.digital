﻿using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Sale.Types
{
    [XmlType("shippingPaymentType", Namespace = "http://sale.individual.ws.listingapi.gg.com")]
    public enum ShippingPaymentType
    {

        BUYER,
        SELLER,
        PAY_IN_THE_BASKET,
        PLATFORM,
    }
}
