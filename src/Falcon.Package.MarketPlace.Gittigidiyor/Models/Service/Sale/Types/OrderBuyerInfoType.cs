﻿using System.Xml.Schema;
using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Sale.Types
{
    [XmlType("orderBuyerInfoType", Namespace = "http://sale.individual.ws.listingapi.gg.com")]
    public class OrderBuyerInfoType
    {
        [XmlElement("username", Form = XmlSchemaForm.Unqualified, Order = 0)]
        public string Username { get; set; }

        [XmlElement("name", Form = XmlSchemaForm.Unqualified, Order = 1)]
        public string Name { get; set; }

        [XmlElement("surname", Form = XmlSchemaForm.Unqualified, Order = 2)]
        public string Surname { get; set; }

        [XmlElement("phone", Form = XmlSchemaForm.Unqualified, Order = 3)]
        public string Phone { get; set; }

        [XmlElement("mobilePhone", Form = XmlSchemaForm.Unqualified, Order = 4)]
        public string MobilePhone { get; set; }

        [XmlElement("email", Form = XmlSchemaForm.Unqualified, Order = 5)]
        public string Email { get; set; }

        [XmlElement("address", Form = XmlSchemaForm.Unqualified, Order = 6)]
        public string Address { get; set; }

        [XmlElement("district", Form = XmlSchemaForm.Unqualified, Order = 7)]
        public string District { get; set; }

        [XmlElement("city", Form = XmlSchemaForm.Unqualified, Order = 8)]
        public string City { get; set; }

        [XmlElement("neighborhoodType", Form = XmlSchemaForm.Unqualified, Order = 9)]
        public NeighborhoodType NeighborhoodType { get; set; }

        [XmlElement("zipCode", Form = XmlSchemaForm.Unqualified, Order = 10)]
        public string ZipCode { get; set; }

        [XmlElement("processCount", Form = XmlSchemaForm.Unqualified, Order = 11)]
        public int ProcessCount { get; set; }

        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool processCountSpecified { get; set; }

        [XmlElement("ratePercentage", Form = XmlSchemaForm.Unqualified, Order = 12)]
        public int RatePercentage { get; set; }

        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool ratePercentageSpecified { get; set; }
    }
}