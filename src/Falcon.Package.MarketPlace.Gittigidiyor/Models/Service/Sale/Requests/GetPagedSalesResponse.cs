﻿using System.Xml.Serialization;
using Falcon.Package.MarketPlace.Gittigidiyor.Attributes;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Sale.Responses;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Sale.Requests
{
    [UrlFragment("IndividualSaleService")]
    [ResponseType(typeof(GetPagedSalesResponse))]
    [XmlRoot("getPagedSales", Namespace = "http://sale.individual.ws.listingapi.gg.com", IsNullable = true)]
    public class GetPagedSalesRequest : BaseAuthRequest
    {
        [XmlElement("withData", Namespace = "")]
        public bool WithData;

        [XmlElement("byStatus", Namespace = "")]
        public string ByStatus;

        [XmlElement("byUser", Namespace = "")]
        public string ByUser;

        [XmlElement("orderBy", Namespace = "")]
        public string OrderBy;

        [XmlElement("orderType", Namespace = "")]
        public string OrderType;

        [XmlElement("pageNumber", Namespace = "")]
        public int PageNumber;

        [XmlElement("pageSize", Namespace = "")]
        public int PageSize;
        
        public GetPagedSalesRequest()
        {
        }

        public GetPagedSalesRequest(bool withData, string byStatus, string byUser, string orderBy, string orderType, int pageNumber, int pageSize, string lang)
        {
            this.WithData = withData;
            this.ByStatus = byStatus;
            this.ByUser = byUser;
            this.OrderBy = orderBy;
            this.OrderType = orderType;
            this.PageNumber = pageNumber;
            this.PageSize = pageSize;
            this.Lang = lang;
        }
    }
}
