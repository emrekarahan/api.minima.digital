﻿using System.Xml.Serialization;
using Falcon.Package.MarketPlace.Gittigidiyor.Attributes;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Sale.Responses;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Sale.Requests
{
    [UrlFragment("IndividualSaleService")]
    [ResponseType(typeof(GetSalesByDateRangeResponse))]
    [XmlRoot("getSalesByDateRange", Namespace = "http://sale.individual.ws.listingapi.gg.com", IsNullable = true)]
    public class GetSalesByDateRangeRequest : BaseAuthRequest
    {
        [XmlElement("withData", Namespace = "")]
        public bool WithData;

        [XmlElement("byStatus", Namespace = "")]
        public string ByStatus;

        [XmlElement("withData", Namespace = "")]
        public string ByUser;

        [XmlElement("orderBy", Namespace = "")]
        public string OrderBy;

        [XmlElement("withData", Namespace = "")]
        public string OrderType;

        [XmlElement("startDate", Namespace = "")]
        public string StartDate;

        [XmlElement("endDate", Namespace = "")]
        public string EndDate;

        [XmlElement("pageNumber", Namespace = "")]
        public int PageNumber;

        [XmlElement("withData", Namespace = "")]
        public int PageSize;


        public GetSalesByDateRangeRequest()
        {
        }

        public GetSalesByDateRangeRequest(bool withData, string byStatus, string byUser, string orderBy, string orderType, string startDate, string endDate, int pageNumber, int pageSize, string lang)
        {
            WithData = withData;
            ByStatus = byStatus;
            ByUser = byUser;
            OrderBy = orderBy;
            OrderType = orderType;
            StartDate = startDate;
            EndDate = endDate;
            PageNumber = pageNumber;
            PageSize = pageSize;
            Lang = lang;
        }
    }
}
