﻿using System.Xml.Serialization;
using Falcon.Package.MarketPlace.Gittigidiyor.Attributes;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Sale.Responses;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Sale.Requests
{
    [UrlFragment("IndividualSaleService")]
    [ResponseType(typeof(GetSaleResponse))]
    [XmlRoot("getSale", Namespace = "http://sale.individual.ws.listingapi.gg.com", IsNullable = true)]
    public class GetSaleRequest : BaseAuthRequest
    {
        [XmlElement("saleCode", Namespace = "")]
        public string SaleCode;

        public GetSaleRequest()
        {
        }

        public GetSaleRequest(string saleCode, string lang)
        {
            SaleCode = saleCode;
            Lang = lang;
        }
    }
}
