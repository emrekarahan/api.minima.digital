﻿using System.Xml.Schema;
using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Types
{
    [XmlType("productType", Namespace = "https://product.individual.ws.listingapi.gg.com")]
    public class ProductType
    {
        [XmlElement("categoryCode", Form = XmlSchemaForm.Unqualified, Order = 0)]
        public string CategoryCode { get; set; }

        [XmlElement("storeCategoryId", Form = XmlSchemaForm.Unqualified, Order = 1)]
        public int StoreCategoryId { get; set; }

        [XmlIgnore()]
        // ReSharper disable once InconsistentNaming
        public bool storeCategoryIdSpecified { get; set; }

        [XmlElement("title", Form = XmlSchemaForm.Unqualified, Order = 2)]
        public string Title { get; set; }

        [XmlElement("subtitle", Form = XmlSchemaForm.Unqualified, Order = 3)]
        public string Subtitle { get; set; }

        [XmlArray("specs", Form = XmlSchemaForm.Unqualified, Order = 4)]
        [XmlArrayItem("spec", Form = XmlSchemaForm.Unqualified, IsNullable = false)]
        public SpecType[] Specs { get; set; }

        [XmlArray("photos", Form = XmlSchemaForm.Unqualified, Order = 5)]
        [XmlArrayItem("photo", Form = XmlSchemaForm.Unqualified, IsNullable = false)]
        public PhotoType[] Photos { get; set; }

        [XmlElement("pageTemplate", Form = XmlSchemaForm.Unqualified, Order = 6)]
        public int PageTemplate { get; set; }

        [XmlIgnore()]
        // ReSharper disable once InconsistentNaming
        public bool pageTemplateSpecified { get; set; }

        [XmlElement("description", Form = XmlSchemaForm.Unqualified, Order = 7)]
        public string Description { get; set; }

        [XmlElement("startDate", Form = XmlSchemaForm.Unqualified, Order = 8)]
        public string StartDate { get; set; }

        [XmlElement("catalogId", Form = XmlSchemaForm.Unqualified, Order = 9)]
        public int CatalogId { get; set; }

        [XmlIgnore()]
        // ReSharper disable once InconsistentNaming
        public bool catalogIdSpecified { get; set; }

        [XmlElement("newCatalogId", Form = XmlSchemaForm.Unqualified, Order = 10)]
        public int NewCatalogId { get; set; }

        [XmlIgnore()]
        // ReSharper disable once InconsistentNaming
        public bool newCatalogIdSpecified { get; set; }

        [XmlElement("catalogDetail", Form = XmlSchemaForm.Unqualified, Order = 11)]
        public int CatalogDetail { get; set; }

        [XmlIgnore()]
        // ReSharper disable once InconsistentNaming
        public bool catalogDetailSpecified { get; set; }

        [XmlElement("catalogFilter", Form = XmlSchemaForm.Unqualified, Order = 12)]
        public string CatalogFilter { get; set; }

        [XmlElement("format", Form = XmlSchemaForm.Unqualified, Order = 13)]
        public string Format { get; set; }

        [XmlElement("startPrice", Form = XmlSchemaForm.Unqualified, Order = 14)]
        public double StartPrice { get; set; }

        [XmlIgnore()]
        // ReSharper disable once InconsistentNaming
        public bool startPriceSpecified { get; set; }

        [XmlElement("buyNowPrice", Form = XmlSchemaForm.Unqualified, Order = 15)]
        public double BuyNowPrice { get; set; }

        [XmlIgnore()]
        // ReSharper disable once InconsistentNaming
        public bool buyNowPriceSpecified { get; set; }

        [XmlElement("netEarning", Form = XmlSchemaForm.Unqualified, Order = 16)]
        public double NetEarning { get; set; }

        [XmlIgnore()]
        // ReSharper disable once InconsistentNaming
        public bool netEarningSpecified { get; set; }

        [XmlElement("listingDays", Form = XmlSchemaForm.Unqualified, Order = 17)]
        public int ListingDays { get; set; }

        [XmlIgnore()]
        // ReSharper disable once InconsistentNaming
        public bool listingDaysSpecified { get; set; }

        [XmlElement("productCount", Form = XmlSchemaForm.Unqualified, Order = 18)]
        public int ProductCount { get; set; }

        [XmlIgnore()]
        // ReSharper disable once InconsistentNaming
        public bool productCountSpecified { get; set; }

        [XmlElement("cargoDetail", Form = XmlSchemaForm.Unqualified, Order = 19)]
        public CargoDetailType CargoDetail { get; set; }

        [XmlElement("affiliateOption", Form = XmlSchemaForm.Unqualified, Order = 20)]
        public bool AffiliateOption { get; set; }

        [XmlIgnore()]
        // ReSharper disable once InconsistentNaming
        public bool affiliateOptionSpecified { get; set; }

        [XmlElement("boldOption", Form = XmlSchemaForm.Unqualified, Order = 21)]
        public bool BoldOption { get; set; }

        [XmlIgnore()]
        // ReSharper disable once InconsistentNaming
        public bool boldOptionSpecified { get; set; }

        [XmlElement("catalogOption", Form = XmlSchemaForm.Unqualified, Order = 22)]
        public bool CatalogOption { get; set; }

        [XmlIgnore()]
        // ReSharper disable once InconsistentNaming
        public bool catalogOptionSpecified { get; set; }

        [XmlElement("vitrineOption", Form = XmlSchemaForm.Unqualified, Order = 23)]
        public bool VitrineOption { get; set; }

        [XmlIgnore()]
        // ReSharper disable once InconsistentNaming
        public bool vitrineOptionSpecified { get; set; }

        [XmlArray("variantGroups", Form = XmlSchemaForm.Unqualified, Order = 24)]
        [XmlArrayItem("variantGroup", Form = XmlSchemaForm.Unqualified, IsNullable = false)]
        public VariantGroupType[] VariantGroups { get; set; }

        [XmlElement("auctionProfilePercentage", Form = XmlSchemaForm.Unqualified, Order = 25)]
        public int AuctionProfilePercentage { get; set; }

        [XmlIgnore()]
        // ReSharper disable once InconsistentNaming
        public bool auctionProfilePercentageSpecified { get; set; }

        [XmlElement("marketPrice", Form = XmlSchemaForm.Unqualified, Order = 26)]
        public double MarketPrice { get; set; }

        [XmlIgnore()]
        // ReSharper disable once InconsistentNaming
        public bool marketPriceSpecified { get; set; }

        [XmlElement("globalTradeItemNo", Form = XmlSchemaForm.Unqualified, DataType = "integer", Order = 27)]
        public string GlobalTradeItemNo { get; set; }

        [XmlElement("manufacturerPartNo", Form = XmlSchemaForm.Unqualified, Order = 28)]
        public string ManufacturerPartNo { get; set; }

        [XmlArray("sameDayDeliveryTypes", Form = XmlSchemaForm.Unqualified, Order = 29)]
        [XmlArrayItem(Form = XmlSchemaForm.Unqualified, IsNullable = false)]
        public SameDayDeliveryType[] SameDayDeliveryTypes { get; set; }
    }
}
