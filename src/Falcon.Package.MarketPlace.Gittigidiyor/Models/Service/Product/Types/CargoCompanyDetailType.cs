﻿using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Types
{
    [XmlType("cargoCompanyDetailType", Namespace = "https://product.individual.ws.listingapi.gg.com")]
    public class CargoCompanyDetailType
    {
        [XmlElement("name")]
        public string Name { get; set; }

        [XmlElement("value")]
        public string Value { get; set; }

        [XmlElement("cityPrice")]
        public string CityPrice { get; set; }

        [XmlElement("countryPrice")]
        public string CountryPrice { get; set; }
    }
}
