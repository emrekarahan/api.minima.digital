﻿using System.Xml.Schema;
using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Types
{
    [XmlType("variantType", Namespace = "https://product.individual.ws.listingapi.gg.com")]
    public class VariantType
    {
        [XmlArray("variantSpecs", Form = XmlSchemaForm.Unqualified, Order = 0)]
        [XmlArrayItem("variantSpec", Form = XmlSchemaForm.Unqualified, IsNullable = false)]
        public VariantSpecType[] VariantSpecs { get; set; }

        [XmlElement("quantity", Form = XmlSchemaForm.Unqualified, Order = 1)]
        public int Quantity { get; set; }

        [XmlIgnore()]
        // ReSharper disable once InconsistentNaming
        public bool quantitySpecified { get; set; }

        [XmlElement("stockCode", Form = XmlSchemaForm.Unqualified, Order = 2)]
        public string StockCode { get; set; }

        [XmlElement("soldCount", Form = XmlSchemaForm.Unqualified, Order = 3)]
        public int SoldCount { get; set; }

        [XmlIgnore()]
        // ReSharper disable once InconsistentNaming
        public bool soldCountSpecified { get; set; }

        [XmlElement("newCatalogId", Form = XmlSchemaForm.Unqualified, Order = 4)]
        public int NewCatalogId { get; set; }

        [XmlIgnore()]
        // ReSharper disable once InconsistentNaming
        public bool newCatalogIdSpecified { get; set; }

        [XmlAttribute("variantId")]
        public long VariantId { get; set; }

        [XmlIgnore()]
        // ReSharper disable once InconsistentNaming
        public bool variantIdSpecified { get; set; }

        [XmlAttribute("operation")]
        public string Operation { get; set; }
    }
}
