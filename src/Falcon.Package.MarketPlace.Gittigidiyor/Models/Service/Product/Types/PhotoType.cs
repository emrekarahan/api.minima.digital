﻿using System.Xml.Schema;
using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Types
{
    [XmlType("photoType", Namespace = "https://product.individual.ws.listingapi.gg.com")]
    public class PhotoType
    {
        [XmlElement("url", Form = XmlSchemaForm.Unqualified, Order = 0)]
        public string Url { get; set; }

        [XmlElement("base64", Form = XmlSchemaForm.Unqualified, Order = 1)]
        public string Base64 { get; set; }

        [XmlAttribute("photoId")]
        public int PhotoId { get; set; }

        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool photoIdSpecified { get; set; }
    }
}
