﻿using System.Xml.Schema;
using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Types
{
    [XmlType("sameDayDeliveryType", Namespace = "https://product.individual.ws.listingapi.gg.com")]
    public class SameDayDeliveryType
    {
        [XmlElement("lastReceivingTime", Form = XmlSchemaForm.Unqualified, Order = 0)]
        public string LastReceivingTime { get; set; }
        
        [XmlElement("shippingFirmId", Form = XmlSchemaForm.Unqualified, Order = 1)]
        public int ShippingFirmId { get; set; }
        
        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool shippingFirmIdSpecified { get; set; }
    }
}