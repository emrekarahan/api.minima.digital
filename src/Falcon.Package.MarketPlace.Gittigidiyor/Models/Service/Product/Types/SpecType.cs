﻿using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Types
{
    [XmlType("specType", Namespace = "https://product.individual.ws.listingapi.gg.com")]
    public class SpecType
    {
        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlAttribute("value")]
        public string Value { get; set; }

        [XmlAttribute("type")]
        public string Type { get; set; }

        [XmlAttribute("required")]
        public bool Required { get; set; }

        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool requiredSpecified { get; set; }
    }
}
