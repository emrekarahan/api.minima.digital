﻿using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Types
{
    [XmlType("cargoTimeType", Namespace = "https://product.individual.ws.listingapi.gg.com")]
    public class CargoTimeType
    {
        [XmlElement("days")]
        public string Days { get; set; }
        
        [XmlElement("beforeTime")]
        public string BeforeTime { get; set; }
    }
}
