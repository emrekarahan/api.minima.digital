﻿using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Types
{
    [XmlType("variantSpecType", Namespace = "https://product.individual.ws.listingapi.gg.com")]
    public class VariantSpecType
    {
        [XmlAttribute("nameId")]
        public long NameId { get; set; }

        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool nameIdSpecified { get; set; }

        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlAttribute("valueId")]
        public long ValueId { get; set; }

        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool valueIdSpecified { get; set; }

        [XmlAttribute("value")]
        public string Value { get; set; }

        [XmlAttribute("orderNumber")]
        public int OrderNumber { get; set; }

        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool orderNumberSpecified { get; set; }

        [XmlAttribute("specDataOrderNumber")]
        public int SpecDataOrderNumber { get; set; }

        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool specDataOrderNumberSpecified { get; set; }
    }
}