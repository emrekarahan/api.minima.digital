﻿using System.Xml.Schema;
using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Types
{
    [XmlType("productConcerningType", Namespace = "https://product.individual.ws.listingapi.gg.com")]
    public class ProductConcerningType
    {
        [XmlElement("soldCount", Form = XmlSchemaForm.Unqualified, Order = 0)]
        public int SoldCount { get; set; }

        // ReSharper disable once InconsistentNaming
        [XmlIgnore] public bool soldCountSpecified { get; set; }

        [XmlElement("endDate", Form = XmlSchemaForm.Unqualified, Order = 1)]
        public string EndDate { get; set; }

        [XmlElement("listingStatus", Form = XmlSchemaForm.Unqualified, Order = 2)]
        public string ListingStatus { get; set; }

        [XmlElement("currentPrice", Form = XmlSchemaForm.Unqualified, Order = 3)]
        public string CurrentPrice { get; set; }

        [XmlElement("hasRelistOption", Form = XmlSchemaForm.Unqualified, Order = 4)]
        public bool HasRelistOption { get; set; }

        // ReSharper disable once InconsistentNaming
        [XmlIgnore] public bool hasRelistOptionSpecified { get; set; }

        [XmlElement("bidCount", Form = XmlSchemaForm.Unqualified, Order = 5)]
        public int BidCount { get; set; }

        // ReSharper disable once InconsistentNaming
        [XmlIgnore] public bool bidCountSpecified { get; set; }
    }
}