﻿using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Types
{
    [XmlType("productPackageSizeType", Namespace = "https://product.individual.ws.listingapi.gg.com")]
    public class ProductPackageSizeType
    {
        [XmlElement("width")]
        public int Width { get; set; }

        [XmlIgnore()]
        // ReSharper disable once InconsistentNaming
        public bool widthSpecified { get; set; }

        [XmlElement("height")]
        public int Height { get; set; }

        [XmlIgnore()]
        // ReSharper disable once InconsistentNaming
        public bool heightSpecified { get; set; }

        [XmlElement("depth")]
        public int Depth { get; set; }

        [XmlIgnore()]
        // ReSharper disable once InconsistentNaming
        public bool depthSpecified { get; set; }

        [XmlElement("weight")]
        public decimal Weight { get; set; }

        [XmlIgnore()]
        // ReSharper disable once InconsistentNaming
        public bool weightSpecified { get; set; }

        [XmlElement("desi")]
        public decimal Desi { get; set; }

        [XmlIgnore()]
        // ReSharper disable once InconsistentNaming
        public bool desiSpecified { get; set; }
    }
}
