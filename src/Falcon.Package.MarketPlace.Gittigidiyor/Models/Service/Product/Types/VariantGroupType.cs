﻿using System.Xml.Schema;
using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Types
{
    [XmlType("variantGroupType", Namespace = "https://product.individual.ws.listingapi.gg.com")]
    public class VariantGroupType
    {
       
        [XmlArray("variants", Form = XmlSchemaForm.Unqualified, Order = 0)]
        [XmlArrayItem("variant", Form = XmlSchemaForm.Unqualified, IsNullable = false)]
        public VariantType[] Variants { get; set; }

        
        [XmlArray("photos", Form = XmlSchemaForm.Unqualified, Order = 1)]
        [XmlArrayItem("photo", Form = XmlSchemaForm.Unqualified, IsNullable = false)]
        public PhotoType[] Photos { get; set; }
        
        [XmlAttribute("nameId")]
        public long NameId { get; set; }
        
        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool nameIdSpecified { get; set; }
        
        [XmlAttribute("valueId")]
        public long ValueId { get; set; }
        
        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool valueIdSpecified { get; set; }
        
        [XmlAttribute("alias")]
        public string Alias { get; set; }
    }
}