﻿using System.Xml.Schema;
using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Types
{
    [XmlType("productDetailType", Namespace = "https://product.individual.ws.listingapi.gg.com")]
    public class ProductDetailType
    {
        [XmlElement("itemId", Form = XmlSchemaForm.Unqualified, Order = 0)]
        public string ItemId { get; set; }

        [XmlElement("productId", Form = XmlSchemaForm.Unqualified, Order = 1)]
        public int ProductId { get; set; }

        [XmlIgnore()]
        public bool productIdSpecified { get; set; }

        [XmlElement("product", Form = XmlSchemaForm.Unqualified, Order = 2)]
        public ProductType Product { get; set; }

        [XmlElement("productConcerningType", Form = XmlSchemaForm.Unqualified, Order = 3)]
        public ProductConcerningType Summary { get; set; }

        [XmlElement("ees", Form = XmlSchemaForm.Unqualified, Order = 4)]
        public string Ees { get; set; }
    }
}
