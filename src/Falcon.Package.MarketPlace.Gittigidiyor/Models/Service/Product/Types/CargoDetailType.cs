﻿using System.Xml.Schema;
using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Types
{
    [XmlType("cargoDetailType", Namespace = "https://product.individual.ws.listingapi.gg.com")]
    public class CargoDetailType
    {
        [XmlElement("city", Form = XmlSchemaForm.Unqualified, Order = 0)]
        public string City { get; set; }

        [XmlArray("cargoCompanies", Form = XmlSchemaForm.Unqualified, Order = 1)]
        [XmlArrayItem("cargoCompany", Form = XmlSchemaForm.Unqualified, IsNullable = false)]
        public string[] CargoCompanies { get; set; }

        [XmlElement("shippingPayment", Form = XmlSchemaForm.Unqualified, Order = 2)]
        public string ShippingPayment { get; set; }

        [XmlElement("cargoDescription", Form = XmlSchemaForm.Unqualified, Order = 3)]
        public string CargoDescription { get; set; }

        [XmlElement("shippingWhere", Form = XmlSchemaForm.Unqualified, Order = 4)]
        public string ShippingWhere { get; set; }

        [XmlElement("shippingFeePaymentType", Form = XmlSchemaForm.Unqualified, Order = 5)]
        public string ShippingFeePaymentType { get; set; }

        [XmlArray("cargoCompanyDetails", Form = XmlSchemaForm.Unqualified, Order = 6)]
        [XmlArrayItem("cargoCompanyDetail", Form = XmlSchemaForm.Unqualified, IsNullable = false)]
        public CargoCompanyDetailType[] CargoCompanyDetails { get; set; }

        [XmlElement("shippingTime", Form = XmlSchemaForm.Unqualified, Order = 7)]
        public CargoTimeType ShippingTime { get; set; }

        [XmlElement("productPackageSize", Form = XmlSchemaForm.Unqualified, Order = 8)]
        public ProductPackageSizeType ProductPackageSize { get; set; }
    }
}
