﻿using System.Xml.Schema;
using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Types
{
    [XmlType("stockDetailType", Namespace = "https://product.individual.ws.listingapi.gg.com")]
    public class StockDetailType
    {
        [XmlElement("itemId", Form = XmlSchemaForm.Unqualified, Order = 0)]
        public string ItemId { get; set; }

        [XmlElement("productId", Form = XmlSchemaForm.Unqualified, Order = 1)]
        public int ProductId { get; set; }

        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool productIdSpecified { get; set; }

        [XmlElement("format", Form = XmlSchemaForm.Unqualified, Order = 2)]
        public string Format { get; set; }

        [XmlElement("amount", Form = XmlSchemaForm.Unqualified, Order = 3)]
        public int Amount { get; set; }

        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool amountSpecified { get; set; }

        [XmlElement("soldItemCount", Form = XmlSchemaForm.Unqualified, Order = 4)]
        public int SoldItemCount { get; set; }

        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool soldItemCountSpecified { get; set; }

        [XmlElement("startPrice", Form = XmlSchemaForm.Unqualified, Order = 5)]
        public double StartPrice { get; set; }

        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool startPriceSpecified { get; set; }

        [XmlElement("buyNowPrice", Form = XmlSchemaForm.Unqualified, Order = 6)]
        public double BuyNowPrice { get; set; }

        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool buyNowPriceSpecified { get; set; }
    }
}