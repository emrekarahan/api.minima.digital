﻿using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Responses
{
    [XmlRoot("getStockAndPriceResponse", Namespace = "https://product.individual.ws.listingapi.gg.com", IsNullable = true)]
    public class GetStockAndPriceResponse
    {

        [XmlElement("return", Namespace = "", Order = 0)]
        public ProductServiceStockResponse ProductServiceStockResponse;

        public GetStockAndPriceResponse()
        {
        }

        public GetStockAndPriceResponse(ProductServiceStockResponse @return)
        {
            this.ProductServiceStockResponse = @return;
        }
    }
}
