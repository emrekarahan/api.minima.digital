﻿using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Responses
{
    [XmlRoot("getProductDescriptionResponse", Namespace = "https://product.individual.ws.listingapi.gg.com")]
    public class GetProductDescriptionResponse
    {

        [XmlElement("return", Namespace = "", Order = 0)]
        public ProductServiceDescResponse ProductServiceDescResponse;

        public GetProductDescriptionResponse()
        {
        }

        public GetProductDescriptionResponse(ProductServiceDescResponse @return)
        {
            this.ProductServiceDescResponse = @return;
        }
    }
}
