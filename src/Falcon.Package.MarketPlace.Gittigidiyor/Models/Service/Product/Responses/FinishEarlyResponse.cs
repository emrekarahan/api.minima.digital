﻿using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Responses
{
    [XmlRoot("finishEarlyResponse", Namespace = "https://product.individual.ws.listingapi.gg.com")]
    public class FinishEarlyResponse
    {

        [XmlElement("return", Namespace = "", Order = 0)]
        public ProductServiceIdResponse ProductServiceIdResponse;

        public FinishEarlyResponse()
        {
        }

        public FinishEarlyResponse(ProductServiceIdResponse @return)
        {
            this.ProductServiceIdResponse = @return;
        }
    }
}
