﻿using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Responses
{
    [XmlRoot("insertAndActivateProductResponse", Namespace = "https://product.individual.ws.listingapi.gg.com", IsNullable = true)]
    public class InsertAndActivateProductResponse
    {

        [XmlElement("return", Namespace = "", Order = 0)]
        public ProductServiceResponse ProductServiceResponse;

        public InsertAndActivateProductResponse()
        {
        }

        public InsertAndActivateProductResponse(ProductServiceResponse @return)
        {
            this.ProductServiceResponse = @return;
        }
    }
}
