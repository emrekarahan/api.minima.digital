﻿using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Responses
{
    [XmlRoot("getProductsResponse", Namespace = "https://product.individual.ws.listingapi.gg.com", IsNullable = true)]
    public class GetProductsResponse
    {

        [XmlElement("return", Namespace = "", Order = 0)]
        public ProductServiceListResponse ProductServiceListResponse;

        public GetProductsResponse()
        {
        }

        public GetProductsResponse(ProductServiceListResponse @return)
        {
            this.ProductServiceListResponse = @return;
        }
    }
}
