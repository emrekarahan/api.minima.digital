﻿using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Responses
{
    [XmlRoot("updatePriceResponse", Namespace = "https://product.individual.ws.listingapi.gg.com", IsNullable = true)]
    public class UpdatePriceResponse
    {

        [XmlElement("return", Namespace = "", Order = 0)]
        public ProductServiceResponse ProductServiceResponse;

        public UpdatePriceResponse()
        {
        }

        public UpdatePriceResponse(ProductServiceResponse @return)
        {
            this.ProductServiceResponse = @return;
        }
    }
}
