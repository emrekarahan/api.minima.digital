﻿using System.Xml.Schema;
using System.Xml.Serialization;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Types;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Responses
{

    [XmlType("productServiceListResponse", Namespace = "https://product.individual.ws.listingapi.gg.com")]
    public class ProductServiceListResponse : BaseResponseService
    {
        [XmlElement("productCount", Form = XmlSchemaForm.Unqualified, Order = 0)]
        public long ProductCount { get; set; }

        [XmlIgnore]
        public bool productCountSpecified { get; set; }

        [XmlArray("products", Form = XmlSchemaForm.Unqualified, Order = 1)]
        [XmlArrayItem("product", Form = XmlSchemaForm.Unqualified)]
        public ProductDetailType[] Products { get; set; }
    }
}
