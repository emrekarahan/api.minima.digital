﻿using System.Xml.Schema;
using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Responses
{
    [XmlType("productServicePriceResponse", Namespace = "https://product.individual.ws.listingapi.gg.com")]
    public class ProductServicePriceResponse : BaseResponseService
    {
        [XmlElement("paymentVoucher", Form = XmlSchemaForm.Unqualified, Order = 0)]
        public string PaymentVoucher { get; set; }

        [XmlElement("price", Form = XmlSchemaForm.Unqualified, Order = 1)]
        public double Price { get; set; }

        [XmlIgnore()]
        // ReSharper disable once InconsistentNaming
        public bool priceSpecified { get; set; }

        [XmlElement("message", Form = XmlSchemaForm.Unqualified, Order = 2)]
        public string Message { get; set; }

        [XmlElement("payRequired", Form = XmlSchemaForm.Unqualified, Order = 3)]
        public bool PayRequired { get; set; }

        [XmlIgnore()]
        // ReSharper disable once InconsistentNaming
        public bool payRequiredSpecified { get; set; }

        [XmlElement("productCount", Form = XmlSchemaForm.Unqualified, Order = 4)]
        public int ProductCount { get; set; }

        [XmlIgnore()]
        // ReSharper disable once InconsistentNaming
        public bool productCountSpecified { get; set; }
    }
}
