﻿using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Responses
{
    [XmlRoot("deleteProductResponse", Namespace = "https://product.individual.ws.listingapi.gg.com")]
    public class DeleteProductResponse
    {

        [XmlElement("return", Namespace = "", Order = 0)]
        public ProductServiceIdResponse ProductServiceIdResponse;

        public DeleteProductResponse()
        {
        }

        public DeleteProductResponse(ProductServiceIdResponse @return)
        {
            ProductServiceIdResponse = @return;
        }
    }
}
