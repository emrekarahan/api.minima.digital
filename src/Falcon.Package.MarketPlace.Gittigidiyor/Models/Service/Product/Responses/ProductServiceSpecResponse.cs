﻿using System.Xml.Schema;
using System.Xml.Serialization;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Types;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Responses
{
    [XmlType("productServiceSpecResponse", Namespace = "https://product.individual.ws.listingapi.gg.com")]
    public class ProductServiceSpecResponse : BaseResponseService
    {
        [XmlElement("productId", Form = XmlSchemaForm.Unqualified, Order = 0)]
        public int ProductId { get; set; }

        [XmlIgnore]
        public bool productIdSpecified { get; set; }

        [XmlElement("itemId", Form = XmlSchemaForm.Unqualified, Order = 1)]
        public string ItemId { get; set; }

        [XmlArray("specs", Form = XmlSchemaForm.Unqualified, Order = 2)]
        [XmlArrayItem("spec", Form = XmlSchemaForm.Unqualified, IsNullable = false)]
        public SpecType[] Specs { get; set; }
    }
}