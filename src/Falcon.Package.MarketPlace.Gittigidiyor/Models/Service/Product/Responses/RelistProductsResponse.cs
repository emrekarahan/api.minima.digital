﻿using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Responses
{
    [XmlRoot("relistProductsResponse", Namespace = "https://product.individual.ws.listingapi.gg.com", IsNullable = true)]
    public class RelistProductsResponse
    {

        [XmlElement("return", Namespace = "", Order = 0)]
        public ProductServiceIdResponse ProductServiceIdResponse;

        public RelistProductsResponse()
        {
        }

        public RelistProductsResponse(ProductServiceIdResponse @return)
        {
            this.ProductServiceIdResponse = @return;
        }
    }
}
