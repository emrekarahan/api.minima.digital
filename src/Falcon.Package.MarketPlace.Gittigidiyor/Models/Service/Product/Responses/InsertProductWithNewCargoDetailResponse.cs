﻿using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Responses
{
    [XmlRoot("insertProductWithNewCargoDetailResponse", Namespace = "https://product.individual.ws.listingapi.gg.com", IsNullable = true)]
    public class InsertProductWithNewCargoDetailResponse
    {

        [XmlElement("return", Namespace = "", Order = 0)]
        public ProductServiceResponse ProductServiceResponse;

        public InsertProductWithNewCargoDetailResponse()
        {
        }

        public InsertProductWithNewCargoDetailResponse(ProductServiceResponse @return)
        {
            this.ProductServiceResponse = @return;
        }
    }
}
