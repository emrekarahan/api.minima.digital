﻿using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Responses
{
    [XmlRoot("getProductsByIdsResponse", Namespace = "https://product.individual.ws.listingapi.gg.com")]
    public class GetProductsByIdsResponse
    {
        [XmlElement("return", Namespace = "", Order = 0)]
        public ProductServiceListResponse ProductServiceListResponse;

        public GetProductsByIdsResponse()
        {
        }

        public GetProductsByIdsResponse(ProductServiceListResponse @return)
        {
            this.ProductServiceListResponse = @return;
        }
    }
}
