﻿using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Responses
{
    [XmlRoot("getProductSpecsResponse", Namespace = "https://product.individual.ws.listingapi.gg.com")]
    public class GetProductSpecsResponse
    {

        [XmlElement("return", Namespace = "", Order = 0)]
        public ProductServiceSpecResponse ProductServiceSpecResponse;

        public GetProductSpecsResponse()
        {
        }

        public GetProductSpecsResponse(ProductServiceSpecResponse @return)
        {
            this.ProductServiceSpecResponse = @return;
        }
    }
}
