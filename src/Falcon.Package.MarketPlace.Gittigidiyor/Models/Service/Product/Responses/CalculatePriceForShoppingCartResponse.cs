﻿using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Responses
{
    [XmlRoot("calculatePriceForShoppingCartResponse", Namespace = "https://product.individual.ws.listingapi.gg.com", IsNullable = true)]
    public class CalculatePriceForShoppingCartResponse
    {

        [XmlElement("return", Namespace = "", Order = 0)]
        public ProductServicePriceResponse ProductServicePriceResponse;

        public CalculatePriceForShoppingCartResponse()
        {
        }

        public CalculatePriceForShoppingCartResponse(ProductServicePriceResponse @return)
        {
            this.ProductServicePriceResponse = @return;
        }
    }

}
