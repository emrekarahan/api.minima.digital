﻿using System.Xml.Schema;
using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Responses
{
    [XmlType("productServiceIdResponse", Namespace = "https://product.individual.ws.listingapi.gg.com")]
    public class ProductServiceIdResponse : BaseResponseService
    {
        [XmlElement("productCount", Form = XmlSchemaForm.Unqualified, Order = 0)]
        public long ProductCount { get; set; }

        [XmlIgnore()]
        // ReSharper disable once InconsistentNaming
        public bool productCountSpecified { get; set; }

        [XmlArray("productIdList", Form = XmlSchemaForm.Unqualified, Order = 1)]
        [XmlArrayItem("productId", Form = XmlSchemaForm.Unqualified)]
        public int?[] ProductIdList { get; set; }

        [XmlElement("voucher", Form = XmlSchemaForm.Unqualified, Order = 2)]
        public string Voucher { get; set; }

        [XmlElement("price", Form = XmlSchemaForm.Unqualified, Order = 3)]
        public double Price { get; set; }

        [XmlIgnore()]
        // ReSharper disable once InconsistentNaming
        public bool priceSpecified { get; set; }

        [XmlElement("result", Form = XmlSchemaForm.Unqualified, Order = 4)]
        public string Result { get; set; }
    }
}
