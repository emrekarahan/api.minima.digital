﻿using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Responses
{
    [XmlRoot("getNewlyListedProductIdListResponse", Namespace = "https://product.individual.ws.listingapi.gg.com")]
    public class GetNewlyListedProductIdListResponse
    {

        [XmlElement("return", Namespace = "", Order = 0)]
        public ProductServiceIdResponse ProductServiceIdResponse;

        public GetNewlyListedProductIdListResponse()
        {
        }

        public GetNewlyListedProductIdListResponse(ProductServiceIdResponse @return)
        {
            this.ProductServiceIdResponse = @return;
        }
    }
}
