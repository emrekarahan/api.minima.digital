﻿using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Responses
{
    [XmlRoot("getProductIdsResponse", Namespace = "https://product.individual.ws.listingapi.gg.com")]
    public class GetProductIdsResponse
    {
        [XmlElement("return", Namespace = "", Order = 0)]
        public ProductServiceIdResponse ProductServiceIdResponse;

        public GetProductIdsResponse()
        {
        }

        public GetProductIdsResponse(ProductServiceIdResponse @return)
        {
            this.ProductServiceIdResponse = @return;
        }
    }

}
