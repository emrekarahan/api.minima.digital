﻿using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Responses
{
    [XmlType("cloneProductResponse", Namespace = "https://product.individual.ws.listingapi.gg.com")]
    public class CloneProductResponse
    {

        [XmlElement("return", Namespace = "", Order = 0)]
        public ProductServiceResponse ProductServiceResponse;

        public CloneProductResponse()
        {
        }

        public CloneProductResponse(ProductServiceResponse @return)
        {
            this.ProductServiceResponse = @return;
        }
    }
}
