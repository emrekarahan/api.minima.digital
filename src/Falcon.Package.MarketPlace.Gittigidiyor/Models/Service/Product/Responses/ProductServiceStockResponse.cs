﻿using System.Xml.Schema;
using System.Xml.Serialization;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Types;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Responses
{
    [XmlType("productServiceStockResponse",Namespace = "https://product.individual.ws.listingapi.gg.com")]
    public class ProductServiceStockResponse : BaseResponseService
    {
        [XmlArray("products", Form = XmlSchemaForm.Unqualified, Order = 0)]
        [XmlArrayItem("product", Form = XmlSchemaForm.Unqualified)]
        public StockDetailType[] Products { get; set; }
    }
}