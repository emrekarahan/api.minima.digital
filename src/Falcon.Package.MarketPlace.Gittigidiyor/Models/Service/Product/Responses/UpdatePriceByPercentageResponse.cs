﻿using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Responses
{
    [XmlRoot("updatePriceByPercentageResponse", Namespace = "https://product.individual.ws.listingapi.gg.com")]
    public class UpdatePriceByPercentageResponse
    {

        [XmlElement("return", Namespace = "", Order = 0)]
        public ProductServiceResponse ProductServiceResponse;

        public UpdatePriceByPercentageResponse()
        {
        }

        public UpdatePriceByPercentageResponse(ProductServiceResponse @return)
        {
            this.ProductServiceResponse = @return;
        }
    }
}
