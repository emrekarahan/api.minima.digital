﻿using System.Xml.Schema;
using System.Xml.Serialization;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Types;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Responses
{
    [XmlType(Namespace = "https://product.individual.ws.listingapi.gg.com")]
    public class ProductServiceDetailResponse : BaseResponseService
    {
        [XmlElement("productDetail", Form = XmlSchemaForm.Unqualified, Order = 0)]
        public ProductDetailType ProductDetail { get; set; }
    }
}