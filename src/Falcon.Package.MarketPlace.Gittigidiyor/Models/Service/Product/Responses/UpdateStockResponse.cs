﻿using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Responses
{
    [XmlRoot("updateStockResponse", Namespace = "https://product.individual.ws.listingapi.gg.com", IsNullable = true)]
    public class UpdateStockResponse
    {

        [XmlElement("return", Namespace = "", Order = 0)]
        public ProductServiceResponse @return;

        public UpdateStockResponse()
        {
        }

        public UpdateStockResponse(ProductServiceResponse @return)
        {
            this.@return = @return;
        }
    }
}
