﻿using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Responses
{
    [XmlRoot("updateVariantStockResponse", Namespace = "https://product.individual.ws.listingapi.gg.com", IsNullable = true)]
    public class UpdateVariantStockResponse
    {

        [XmlElement("return", Namespace = "", Order = 0)]
        public ProductServiceResponse ProductServiceResponse;

        public UpdateVariantStockResponse()
        {
        }

        public UpdateVariantStockResponse(ProductServiceResponse @return)
        {
            this.ProductServiceResponse = @return;
        }
    }
}
