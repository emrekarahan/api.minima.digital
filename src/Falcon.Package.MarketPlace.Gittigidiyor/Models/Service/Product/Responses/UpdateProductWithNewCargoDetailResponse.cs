﻿using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Responses
{
    [XmlRoot("updateProductWithNewCargoDetailResponse", Namespace = "https://product.individual.ws.listingapi.gg.com", IsNullable = true)]
    public class UpdateProductWithNewCargoDetailResponse
    {

        [XmlElement("return", Namespace = "", Order = 0)]
        public ProductServiceResponse ProductServiceResponse;

        public UpdateProductWithNewCargoDetailResponse()
        {
        }

        public UpdateProductWithNewCargoDetailResponse(ProductServiceResponse @return)
        {
            this.ProductServiceResponse = @return;
        }
    }
}
