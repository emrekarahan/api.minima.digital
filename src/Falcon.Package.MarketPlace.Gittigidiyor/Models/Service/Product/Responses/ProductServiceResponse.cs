﻿using System.Xml.Schema;
using System.Xml.Serialization;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Types;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Responses
{
    [XmlType("productServiceResponse", Namespace = "https://product.individual.ws.listingapi.gg.com")]
    public class ProductServiceResponse : BaseResponseService
    {
        [XmlElement("productId", Form = XmlSchemaForm.Unqualified, Order = 0)]
        public int ProductId { get; set; }

        [XmlIgnore()]
        public bool productIdSpecified { get; set; }

        [XmlElement("payRequired", Form = XmlSchemaForm.Unqualified, Order = 1)]
        public bool PayRequired { get; set; }

        [XmlIgnore()]
        public bool payRequiredSpecified { get; set; }

        [XmlElement("result", Form = XmlSchemaForm.Unqualified, Order = 2)]
        public string Result { get; set; }

        [XmlElement("descriptionFilterStatus", Form = XmlSchemaForm.Unqualified, Order = 3)]
        public int DescriptionFilterStatus { get; set; }

        [XmlIgnore()]
        public bool descriptionFilterStatusSpecified { get; set; }

        [XmlArray("variantGroups", Form = XmlSchemaForm.Unqualified, Order = 4)]
        [XmlArrayItem("variantGroup", Form = XmlSchemaForm.Unqualified, IsNullable = false)]
        public VariantGroupType[] VariantGroups { get; set; }
    }
}
