﻿using System.Xml.Schema;
using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Responses
{
    [XmlType("productServiceDescResponse", Namespace = "https://product.individual.ws.listingapi.gg.com")]
    public class ProductServiceDescResponse : BaseResponseService
    {
        [XmlElement("productId", Form = XmlSchemaForm.Unqualified, Order = 0)]
        public int ProductId { get; set; }

        [XmlIgnore()]
        public bool productIdSpecified { get; set; }

        [XmlElement("itemId", Form = XmlSchemaForm.Unqualified, Order = 1)]
        public string ItemId { get; set; }

        [XmlElement("description", Form = XmlSchemaForm.Unqualified, Order = 2)]
        public string Description { get; set; }
    }
}
