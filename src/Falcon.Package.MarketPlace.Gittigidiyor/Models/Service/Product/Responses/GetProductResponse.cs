﻿using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Responses
{
    [XmlRoot("getProductResponse", Namespace = "https://product.individual.ws.listingapi.gg.com")]
    public class GetProductResponse
    {

        [XmlElement("return", Namespace = "", Order = 0)]
        public ProductServiceDetailResponse ProductServiceDetailResponse;

        public GetProductResponse()
        {
        }

        public GetProductResponse(ProductServiceDetailResponse @return)
        {
            this.ProductServiceDetailResponse = @return;
        }
    }
}
