﻿using System.Xml.Serialization;
using Falcon.Package.MarketPlace.Gittigidiyor.Attributes;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Responses;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Requests
{
    [UrlFragment("IndividualProductService")]
    [ResponseType(typeof(CloneProductResponse))]
    [XmlRoot("cloneProduct", Namespace = "https://product.individual.ws.listingapi.gg.com", IsNullable = true)]
    public class CloneProductRequest : BaseAuthRequest
    {
        [XmlElement("productId", Namespace = "")]
        public string ProductId;

        [XmlElement("itemId", Namespace = "")]
        public string ItemId;

        
        public CloneProductRequest()
        {
        }

        public CloneProductRequest(string productId, string itemId, string lang)
        {
            this.ProductId = productId;
            this.ItemId = itemId;
            this.Lang = lang;
        }
    }
}
