﻿using System.Xml.Serialization;
using Falcon.Package.MarketPlace.Gittigidiyor.Attributes;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Responses;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Requests
{
    [UrlFragment("IndividualProductService")]
    [ResponseType(typeof(GetProductResponse))]
    [XmlRoot("getProduct", Namespace = "https://product.individual.ws.listingapi.gg.com", IsNullable = true)]
    public class GetProductRequest : BaseAuthRequest
    {
        [XmlElement("productId", Namespace = "")]
        public string ProductId;

        [XmlElement("itemId", Namespace = "")]
        public string ItemId;


        public GetProductRequest()
        {
        }

        public GetProductRequest(string productId, string itemId, string lang)
        {
            this.ProductId = productId;
            this.ItemId = itemId;
            this.Lang = lang;
        }
    }

}
