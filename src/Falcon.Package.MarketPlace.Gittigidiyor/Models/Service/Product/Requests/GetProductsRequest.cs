﻿using System.Xml.Serialization;
using Falcon.Package.MarketPlace.Gittigidiyor.Attributes;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Responses;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Requests
{
    [UrlFragment("IndividualProductService")]
    [ResponseType(typeof(GetProductsResponse))]
    [XmlRoot("getProducts", Namespace = "https://product.individual.ws.listingapi.gg.com", IsNullable = true)]
    public class GetProductsRequest : BaseAuthRequest
    {
        [XmlElement("startOffSet", Namespace = "")]
        public int StartOffSet;
        [XmlElement("rowCount", Namespace = "")]
        public int RowCount;
        [XmlElement("status", Namespace = "")]
        public string Status;
        [XmlElement("withData", Namespace = "")]
        public bool WithData;

        public GetProductsRequest()
        {
        }

        public GetProductsRequest(int startOffSet, int rowCount, string status, bool withData, string lang)
        {
            this.StartOffSet = startOffSet;
            this.RowCount = rowCount;
            this.Status = status;
            this.WithData = withData;
            this.Lang = lang;


        }
    }
}
