﻿using System.Xml.Schema;
using System.Xml.Serialization;
using Falcon.Package.MarketPlace.Gittigidiyor.Attributes;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Responses;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Requests
{
    [UrlFragment("IndividualProductService")]
    [ResponseType(typeof(GetProductsByIdsResponse))]
    [XmlRoot("getProductsByIds", Namespace = "https://product.individual.ws.listingapi.gg.com", IsNullable = true)]
    public class GetProductsByIdsRequest : BaseAuthRequest
    {
        [XmlArray("itemIdList", Namespace = "")]
        [XmlArrayItem("item", Form = XmlSchemaForm.Unqualified)]
        public string[] ItemIdList;

        [XmlArray("productIdList", Namespace = "")]
        [XmlArrayItem("item", Form = XmlSchemaForm.Unqualified)]
        public int?[] ProductIdList;

        [XmlElement("withData", Namespace = "", Order = 5)]
        public bool WithData;

        public GetProductsByIdsRequest(int?[] productIdList, string[] itemIdList,
                 bool withData, string lang)
        {
            this.ProductIdList = productIdList;
            this.ItemIdList = itemIdList;
            this.WithData = withData;
            this.Lang = lang;
        }
    }
}