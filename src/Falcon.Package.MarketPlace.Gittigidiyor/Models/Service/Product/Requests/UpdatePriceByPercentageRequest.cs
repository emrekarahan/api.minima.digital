﻿using System.Xml.Serialization;
using Falcon.Package.MarketPlace.Gittigidiyor.Attributes;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Responses;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Requests
{
    [ResponseType(typeof(UpdatePriceByPercentageResponse))]
    [UrlFragment("IndividualProductService")]
    [XmlRoot("updatePriceByPercentage", Namespace = "https://product.individual.ws.listingapi.gg.com", IsNullable = true)]
    public class UpdatePriceByPercentageRequest : BaseAuthRequest
    {
        [XmlElement("productId", Namespace = "")]
        public string ProductId;

        [XmlElement("itemId", Namespace = "")]
        public string ItemId;

        [XmlElement("operatorType", Namespace = "")]
        public string OperatorType;

        [XmlElement("percentage", Namespace = "")]
        public int Percentage;

        public UpdatePriceByPercentageRequest()
        {
        }

        public UpdatePriceByPercentageRequest(string productId, string itemId, string operatorType, int percentage, string lang)
        {
            this.ProductId = productId;
            this.ItemId = itemId;
            this.OperatorType = operatorType;
            this.Percentage = percentage;
            this.Lang = lang;
        }
    }
}
