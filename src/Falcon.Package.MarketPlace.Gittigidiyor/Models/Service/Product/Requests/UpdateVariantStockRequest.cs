﻿using System.Xml.Serialization;
using Falcon.Package.MarketPlace.Gittigidiyor.Attributes;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Responses;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Requests
{

    [ResponseType(typeof(UpdateVariantStockResponse))]
    [UrlFragment("IndividualProductService")]
    [XmlRoot("updateVariantStock", Namespace = "https://product.individual.ws.listingapi.gg.com", IsNullable = true)]
    public class UpdateVariantStockRequest : BaseAuthRequest
    {
        [XmlElement("productId", Namespace = "")]
        public string ProductId;

        [XmlElement("itemId", Namespace = "")]
        public string ItemId;

        [XmlElement("variantId", Namespace = "")]
        public string VariantId;

        [XmlElement("stock", Namespace = "")]
        public int Stock;


        public UpdateVariantStockRequest()
        {
        }

        public UpdateVariantStockRequest(string productId, string itemId, string variantId, int stock, string lang)
        {
            this.ProductId = productId;
            this.ItemId = itemId;
            this.VariantId = variantId;
            this.Stock = stock;
            this.Lang = lang;
        }
    }
}
