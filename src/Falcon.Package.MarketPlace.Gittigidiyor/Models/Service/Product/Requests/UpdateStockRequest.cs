﻿using System.Xml.Serialization;
using Falcon.Package.MarketPlace.Gittigidiyor.Attributes;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Responses;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Requests
{

    [ResponseType(typeof(UpdateStockResponse))]
    [UrlFragment("IndividualProductService")]
    [XmlRoot("updateStock", Namespace = "https://product.individual.ws.listingapi.gg.com", IsNullable = true)]
    public class UpdateStockRequest : BaseAuthRequest
    {
        [XmlElement("productId", Namespace = "")]
        public string ProductId;

        [XmlElement("itemId", Namespace = "")]
        public string ItemId;

        [XmlElement("stock", Namespace = "")]
        public int Stock;

        [XmlElement("cancelBid", Namespace = "")]
        public bool CancelBid;

        //[XmlElement(Namespace = "")]
        //public string lang;

        public UpdateStockRequest()
        {
        }

        public UpdateStockRequest(string productId, string itemId, int stock, bool cancelBid, string lang)
        {
            this.ProductId = productId;
            this.ItemId = itemId;
            this.Stock = stock;
            this.CancelBid = cancelBid;
            this.Lang = lang;
        }
    }
}
