﻿using System.Xml.Serialization;
using Falcon.Package.MarketPlace.Gittigidiyor.Attributes;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Responses;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Requests
{
    [UrlFragment("IndividualProductService")]
    [ResponseType(typeof(GetNewlyListedProductIdListResponse))]
    [XmlRoot("getNewlyListedProductIdList", Namespace = "https://product.individual.ws.listingapi.gg.com", IsNullable = true)]
  
    public class GetNewlyListedProductIdListRequest: BaseAuthRequest
    {

        [XmlElement("startOffSet", Namespace = "")]
        public int StartOffSet;

        [XmlElement("rowCount", Namespace = "")]
        public int RowCount;

        [XmlElement("viaApi", Namespace = "")]
        public bool ViaApi;

        public GetNewlyListedProductIdListRequest()
        {
        }

        public GetNewlyListedProductIdListRequest(int startOffSet, int rowCount, bool viaApi, string lang)
        {
            this.StartOffSet = startOffSet;
            this.RowCount = rowCount;
            this.ViaApi = viaApi;
            this.Lang = lang;
        }
    }
}
