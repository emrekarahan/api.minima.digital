﻿using System.Xml.Serialization;
using Falcon.Package.MarketPlace.Gittigidiyor.Attributes;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Responses;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Types;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Requests
{
    [ResponseType(typeof(InsertProductWithNewCargoDetailResponse))]
    [UrlFragment("IndividualProductService")]
    [XmlRoot("insertProductWithNewCargoDetail", Namespace = "https://product.individual.ws.listingapi.gg.com", IsNullable = true)]
    public class InsertProductWithNewCargoDetailRequest : BaseAuthRequest
    {

        [XmlElement("itemId", Namespace = "")]
        public string ItemId;

        [XmlElement("product", Namespace = "")]
        public ProductType Product;

        [XmlElement("forceToSpecEntry", Namespace = "")]
        public bool ForceToSpecEntry;

        [XmlElement("nextDateOption", Namespace = "")]
        public bool NextDateOption;

        //[XmlElement(Namespace = "")]
        //public string lang;

        public InsertProductWithNewCargoDetailRequest()
        {
        }

        public InsertProductWithNewCargoDetailRequest(string itemId, ProductType product, bool forceToSpecEntry, bool nextDateOption, string lang)
        {
            this.ItemId = itemId;
            this.Product = product;
            this.ForceToSpecEntry = forceToSpecEntry;
            this.NextDateOption = nextDateOption;
            this.Lang = lang;
        }
    }
}
