﻿using System.Xml.Serialization;
using Falcon.Package.MarketPlace.Gittigidiyor.Attributes;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Products.Requests;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Requests
{
    [UrlFragment("IndividualProductService")]
    [ResponseType(typeof(GetProductsByIdsRequestReturn))]
    [XmlRoot("getProductIds", Namespace = "https://product.individual.ws.listingapi.gg.com", IsNullable = true)]
    public class GetProductIdsRequest : BaseAuthRequest
    {

        [XmlElement("startOffSet", Namespace = "")]
        public int StartOffSet;

        [XmlElement("rowCount", Namespace = "")]
        public int RowCount;

        [XmlArray("statusTypes", Namespace = "")]
        [XmlArrayItem("item", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string[] StatusTypes;

        public GetProductIdsRequest(int startOffSet, int rowCount, string[] statusTypes, string lang)
        {
            this.StartOffSet = startOffSet;
            this.RowCount = rowCount;
            this.StatusTypes = statusTypes;
            this.Lang = lang;
        }
    }
}
