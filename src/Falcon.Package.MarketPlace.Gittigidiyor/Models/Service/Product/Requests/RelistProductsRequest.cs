﻿using System.Xml.Schema;
using System.Xml.Serialization;
using Falcon.Package.MarketPlace.Gittigidiyor.Attributes;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Responses;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Requests
{

    [ResponseType(typeof(RelistProductsResponse))]
    [UrlFragment("IndividualProductService")]
    [XmlRoot("relistProducts", Namespace = "https://product.individual.ws.listingapi.gg.com", IsNullable = true)]
    public class RelistProductsRequest : BaseAuthRequest
    {

        [XmlArray("productIdList", Namespace = "")]
        [XmlArrayItem("item", Form = XmlSchemaForm.Unqualified)]
        public int?[] ProductIdList;

        [XmlArray("itemIdList", Namespace = "")]
        [XmlArrayItem("item", Form = XmlSchemaForm.Unqualified)]
        public string[] ItemIdList;


        public RelistProductsRequest()
        {
        }

        public RelistProductsRequest(int?[] productIdList, string[] itemIdList, string lang)
        {
            this.ProductIdList = productIdList;
            this.ItemIdList = itemIdList;
            this.Lang = lang;
        }
    }
}
