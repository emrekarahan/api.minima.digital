﻿using System.Xml.Serialization;
using Falcon.Package.MarketPlace.Gittigidiyor.Attributes;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Responses;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Requests
{

    [ResponseType(typeof(UpdatePriceResponse))]
    [UrlFragment("IndividualProductService")]
    [XmlRoot("updatePrice", Namespace = "https://product.individual.ws.listingapi.gg.com", IsNullable = true)]
    public class UpdatePriceRequest : BaseAuthRequest
    {
        [XmlElement("productId", Namespace = "")]
        public string ProductId;

        [XmlElement("itemId", Namespace = "")]
        public string ItemId;

        [XmlElement("price", Namespace = "")]
        public double Price;

        [XmlElement("cancelBid", Namespace = "")]
        public bool CancelBid;

        //[XmlElement(Namespace = "")]
        //public string lang;

        public UpdatePriceRequest()
        {
        }

        public UpdatePriceRequest(string productId, string itemId, double price, bool cancelBid, string lang)
        {
            this.ProductId = productId;
            this.ItemId = itemId;
            this.Price = price;
            this.CancelBid = cancelBid;
            this.Lang = lang;
        }
    }
}
