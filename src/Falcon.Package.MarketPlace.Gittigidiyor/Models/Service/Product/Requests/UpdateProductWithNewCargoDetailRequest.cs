﻿using System.Xml.Serialization;
using Falcon.Package.MarketPlace.Gittigidiyor.Attributes;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Responses;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Types;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Requests
{
    [ResponseType(typeof(UpdateProductWithNewCargoDetailResponse))]
    [UrlFragment("IndividualProductService")]
    [XmlRoot("updateProductWithNewCargoDetail", Namespace = "https://product.individual.ws.listingapi.gg.com", IsNullable = true)]
    public class UpdateProductWithNewCargoDetailRequest : BaseAuthRequest
    {
        [XmlElement("itemId", Namespace = "")]
        public string ItemId;

        [XmlElement("productId", Namespace = "")]
        public string ProductId;

        [XmlElement("product", Namespace = "")]
        public ProductType Product;

        [XmlElement("onSale", Namespace = "")]
        public bool OnSale;

        [XmlElement("forceToSpecEntry", Namespace = "")]
        public bool ForceToSpecEntry;

        [XmlElement("nextDateOption", Namespace = "")]
        public bool NextDateOption;

        public UpdateProductWithNewCargoDetailRequest()
        {
        }

        public UpdateProductWithNewCargoDetailRequest(string itemId, string productId, ProductType product, bool onSale, bool forceToSpecEntry, bool nextDateOption, string lang)
        {
            this.ItemId = itemId;
            this.ProductId = productId;
            this.Product = product;
            this.OnSale = onSale;
            this.ForceToSpecEntry = forceToSpecEntry;
            this.NextDateOption = nextDateOption;
            this.Lang = lang;
        }
    }
}
