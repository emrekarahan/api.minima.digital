﻿using System.Xml.Schema;
using System.Xml.Serialization;
using Falcon.Package.MarketPlace.Gittigidiyor.Attributes;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Responses;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Requests
{

    [UrlFragment("IndividualProductService")]
    [ResponseType(typeof(CalculatePriceForShoppingCartResponse))]
    [XmlRoot("calculatePriceForShoppingCart", Namespace = "https://product.individual.ws.listingapi.gg.com", IsNullable = true)]

    public class CalculatePriceForShoppingCartRequest : BaseAuthRequest
    {

        [XmlArray("productIdList", Namespace = "")]
        [XmlArrayItem("item", Form = XmlSchemaForm.Unqualified)]
        public int?[] ProductIdList;

        [XmlArray("itemIdList", Namespace = "")]
        [XmlArrayItem("item", Form = XmlSchemaForm.Unqualified)]
        public string[] ItemIdList;
        
        public CalculatePriceForShoppingCartRequest()
        {
        }

        public CalculatePriceForShoppingCartRequest(int?[] productIdList, string[] itemIdList, string lang)
        {
            this.ProductIdList = productIdList;
            this.ItemIdList = itemIdList;
            this.Lang = lang;
        }
    }
}
