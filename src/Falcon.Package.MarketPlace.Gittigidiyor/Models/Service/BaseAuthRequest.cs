﻿using System;
using System.Xml.Serialization;
using Falcon.Package.MarketPlace.Gittigidiyor.Helpers;
using Minima.Core.Ioc.Impl;
using Microsoft.Extensions.DependencyInjection;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service
{
    public class BaseAuthRequest : BaseRequest
    {
        public BaseAuthRequest()
        {
            var gittigidiyorSettings = ServiceLocator.Instance.GetService<GittigidiyorSettings>();
            time = CommonHelper.convertToTimestamp(DateTime.UtcNow);
            sign = CommonHelper.getSignature(gittigidiyorSettings.ApiKey, gittigidiyorSettings.ApiSecret, time);
            apiKey = gittigidiyorSettings.ApiKey;
        }

        [XmlElement(Namespace = "")] public string apiKey;
        [XmlElement(Namespace = "")] public string sign;
        [XmlElement(Namespace = "")] public long time;
    }
}
