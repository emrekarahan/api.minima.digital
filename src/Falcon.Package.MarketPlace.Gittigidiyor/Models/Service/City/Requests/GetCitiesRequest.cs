﻿using System.Xml.Serialization;
using Falcon.Package.MarketPlace.Gittigidiyor.Attributes;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.City.Responses;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.City.Requests
{
    [UrlFragment("CityService")]
    [ResponseType(typeof(GetCitiesResponse))]
    [XmlRoot("getCities", Namespace = "http://city.anonymous.ws.listingapi.gg.com", IsNullable = true)]
    public class GetCitiesRequest : BaseRequest
    {

        [XmlElement("startOffSet", Namespace = "")]
        public int StartOffSet;

        [XmlElement("rowCount", Namespace = "")]
        public int RowCount;

        public GetCitiesRequest()
        {
        }

        public GetCitiesRequest(int startOffSet, int rowCount, string lang)
        {
            this.StartOffSet = startOffSet;
            this.RowCount = rowCount;
            this.Lang = lang;
        }
    }

}
