﻿using System.Xml.Schema;
using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.City.Types
{
    [XmlType("cityType", Namespace = "http://city.anonymous.ws.listingapi.gg.com")]
    public class CityType
    {
        [XmlElement("trCode",Form = XmlSchemaForm.Unqualified, Order = 0)]
        public int TrCode { get; set; }

        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool trCodeSpecified { get; set; }

        /// <remarks />
        [XmlElement("cityName", Form = XmlSchemaForm.Unqualified, Order = 1)]
        public string CityName { get; set; }
    }
}