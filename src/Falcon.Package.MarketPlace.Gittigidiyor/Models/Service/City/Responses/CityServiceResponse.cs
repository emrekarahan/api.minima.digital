﻿using System.Xml.Schema;
using System.Xml.Serialization;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.City.Types;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.City.Responses
{
    [XmlType("cityServiceResponse", Namespace = "http://city.anonymous.ws.listingapi.gg.com")]
    public class CityServiceResponse : BaseResponseService
    {
        [XmlElement("cityCount", Form = XmlSchemaForm.Unqualified, Order = 0)]
        public int CityCount { get; set; }

        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool cityCountSpecified { get; set; }

        [XmlArray("cities", Form = XmlSchemaForm.Unqualified, Order = 1)]
        [XmlArrayItem("city", Form = XmlSchemaForm.Unqualified)]
        public CityType[] Cities { get; set; }
    }
}