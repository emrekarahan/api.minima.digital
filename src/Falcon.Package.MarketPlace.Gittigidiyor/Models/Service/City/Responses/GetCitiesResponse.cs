﻿using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.City.Responses
{
    [XmlRoot("getCitiesResponse", Namespace = "http://city.anonymous.ws.listingapi.gg.com")]
    public class GetCitiesResponse
    {

        [XmlElement("return", Namespace = "", Order = 0)]
        public CityServiceResponse CityServiceResponse;

        public GetCitiesResponse()
        {
        }

        public GetCitiesResponse(CityServiceResponse @return)
        {
            this.CityServiceResponse = @return;
        }
    }
}
