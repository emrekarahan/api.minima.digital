﻿using System.Xml.Serialization;
using Falcon.Package.MarketPlace.Gittigidiyor.Attributes;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Category.Responses;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Category.Requests
{
    [UrlFragment("CategoryService")]
    [ResponseType(typeof(GetCategoriesResponse))]
    [XmlRoot("getCategoriesHavingVariantSpecs", Namespace = "http://category.anonymous.ws.listingapi.gg.com", IsNullable = true)]
    public class GetCategoriesHavingVariantSpecsRequest
    {

        [XmlElement("lang", Namespace = "")]
        public string Lang;

        public GetCategoriesHavingVariantSpecsRequest()
        {
        }

        public GetCategoriesHavingVariantSpecsRequest(string lang)
        {
            this.Lang = lang;
        }
    }
}
