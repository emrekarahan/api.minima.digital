﻿using System.Xml.Serialization;
using Falcon.Package.MarketPlace.Gittigidiyor.Attributes;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Category.Responses;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Category.Requests
{
    [UrlFragment("CategoryService")]
    [ResponseType(typeof(GetSubCategoriesResponse))]
    [XmlRoot("getSubCategories", Namespace = "http://category.anonymous.ws.listingapi.gg.com", IsNullable = true)]
    public class GetSubCategoriesRequest
    {

        [XmlElement("categoryCode", Namespace = "")]
        public string CategoryCode;

        [XmlElement("withSpecs", Namespace = "")]
        public bool WithSpecs;

        [XmlElement("withDeepest", Namespace = "")]
        public bool WithDeepest;

        [XmlElement("withCatalog", Namespace = "")]
        public bool WithCatalog;

        [XmlElement("lang", Namespace = "")]
        public string Lang;

        public GetSubCategoriesRequest()
        {
        }

        public GetSubCategoriesRequest(string categoryCode, bool withSpecs, bool withDeepest, bool withCatalog, string lang)
        {
            this.CategoryCode = categoryCode;
            this.WithSpecs = withSpecs;
            this.WithDeepest = withDeepest;
            this.WithCatalog = withCatalog;
            this.Lang = lang;
        }
    }
}
