﻿using System.Xml.Serialization;
using Falcon.Package.MarketPlace.Gittigidiyor.Attributes;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Category.Responses;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Category.Requests
{

    [UrlFragment("CategoryService")]
    [ResponseType(typeof(GetDeepestCategoriesResponse))]
    [XmlRoot("getDeepestCategories", Namespace = "http://category.anonymous.ws.listingapi.gg.com", IsNullable = true)]
    public class GetDeepestCategoriesRequest
    {

        [XmlElement("startOffSet", Namespace = "")]
        public int StartOffSet;

        [XmlElement("rowCount", Namespace = "")]
        public int RowCount;

        [XmlElement("withSpecs", Namespace = "")]
        public bool WithSpecs;

        [XmlElement("lang", Namespace = "")]
        public string Lang;

        public GetDeepestCategoriesRequest()
        {
        }

        public GetDeepestCategoriesRequest(int startOffSet, int rowCount, bool withSpecs, string lang)
        {
            this.StartOffSet = startOffSet;
            this.RowCount = rowCount;
            this.WithSpecs = withSpecs;
            this.Lang = lang;
        }
    }
}
