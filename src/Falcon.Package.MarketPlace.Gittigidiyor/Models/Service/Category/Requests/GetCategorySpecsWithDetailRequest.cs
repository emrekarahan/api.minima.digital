﻿using System.Xml.Serialization;
using Falcon.Package.MarketPlace.Gittigidiyor.Attributes;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Category.Responses;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Category.Requests
{

    [UrlFragment("CategoryService")]
    [ResponseType(typeof(GetCategorySpecsWithDetailResponse))]
    [XmlRoot("getCategorySpecsWithDetail", Namespace = "http://category.anonymous.ws.listingapi.gg.com", IsNullable = true)]
    public class GetCategorySpecsWithDetailRequest
    {

        [XmlElement("categoryCode", Namespace = "")]
        public string CategoryCode;

        [XmlElement("lang", Namespace = "")]
        public string Lang;

        public GetCategorySpecsWithDetailRequest()
        {
        }

        public GetCategorySpecsWithDetailRequest(string categoryCode, string lang)
        {
            CategoryCode = categoryCode;
            Lang = lang;
        }
    }
}
