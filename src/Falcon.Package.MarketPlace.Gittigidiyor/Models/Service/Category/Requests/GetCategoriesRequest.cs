﻿using System.Xml.Serialization;
using Falcon.Package.MarketPlace.Gittigidiyor.Attributes;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Category.Responses;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Category.Requests
{
    [UrlFragment("CategoryService")]
    [ResponseType(typeof(GetCategoriesResponse))]
    [XmlRoot("getCategories", Namespace = "http://category.anonymous.ws.listingapi.gg.com", IsNullable = true)]
    public class GetCategoriesRequest : BaseRequest
    {

        [XmlElement("startOffSet", Namespace = "")]
        public int StartOffSet;

        [XmlElement("rowCount", Namespace = "")]
        public int RowCount;

        [XmlElement("withSpecs", Namespace = "")]
        public bool WithSpecs;

        [XmlElement("withDeepest", Namespace = "")]
        public bool WithDeepest;

        [XmlElement("withCatalog", Namespace = "")]
        public bool WithCatalog;



        public GetCategoriesRequest()
        {
        }

        public GetCategoriesRequest(int startOffSet, int rowCount, bool withSpecs, bool withDeepest, bool withCatalog, string lang)
        {
            this.StartOffSet = startOffSet;
            this.RowCount = rowCount;
            this.WithSpecs = withSpecs;
            this.WithDeepest = withDeepest;
            this.WithCatalog = withCatalog;
            this.Lang = lang;
        }
    }
}
