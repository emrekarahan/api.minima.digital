﻿using System.Xml.Serialization;
using Falcon.Package.MarketPlace.Gittigidiyor.Attributes;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Category.Responses;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Category.Requests
{
    [UrlFragment("CategoryService")]
    [ResponseType(typeof(GetCategoriesResponse))]
    [XmlRoot("getParentCategories", Namespace = "http://category.anonymous.ws.listingapi.gg.com", IsNullable = true)]
    public class GetParentCategoriesRequest
    {

        [XmlElement("withSpecs", Namespace = "")]
        public bool WithSpecs;

        [XmlElement("withDeepest", Namespace = "")]
        public bool WithDeepest;

        [XmlElement("withCatalog", Namespace = "")]
        public bool WithCatalog;

        [XmlElement("lang", Namespace = "")]
        public string Lang;

        public GetParentCategoriesRequest()
        {
        }

        public GetParentCategoriesRequest(bool withSpecs, bool withDeepest, bool withCatalog, string lang)
        {
            this.WithSpecs = withSpecs;
            this.WithDeepest = withDeepest;
            this.WithCatalog = withCatalog;
            this.Lang = lang;
        }
    }
}
