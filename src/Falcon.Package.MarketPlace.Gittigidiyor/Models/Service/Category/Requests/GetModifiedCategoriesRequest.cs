﻿using System.Xml.Serialization;
using Falcon.Package.MarketPlace.Gittigidiyor.Attributes;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Category.Responses;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Category.Requests
{

    [UrlFragment("CategoryService")]
    [ResponseType(typeof(GetModifiedCategoriesResponse))]
    [XmlRoot("getModifiedCategories", Namespace = "http://category.anonymous.ws.listingapi.gg.com", IsNullable = true)]

    public class GetModifiedCategoriesRequest
    {

        [XmlElement("changeTime", Namespace = "")]
        public long ChangeTime;

        [XmlElement("startOffSet", Namespace = "")]
        public int StartOffSet;

        [XmlElement("rowCount", Namespace = "")]
        public int RowCount;

        [XmlElement("lang", Namespace = "")]
        public string Lang;

        public GetModifiedCategoriesRequest()
        {
        }

        public GetModifiedCategoriesRequest(long changeTime, int startOffSet, int rowCount, string lang)
        {
            this.ChangeTime = changeTime;
            this.StartOffSet = startOffSet;
            this.RowCount = rowCount;
            this.Lang = lang;
        }
    }
}
