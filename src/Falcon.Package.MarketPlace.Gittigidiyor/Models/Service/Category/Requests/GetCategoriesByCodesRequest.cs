﻿using System.Xml.Schema;
using System.Xml.Serialization;
using Falcon.Package.MarketPlace.Gittigidiyor.Attributes;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Category.Responses;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Category.Requests
{
    [UrlFragment("CategoryService")]
    [ResponseType(typeof(GetCategoriesResponse))]
    [XmlRoot("getCategoriesByCodes", Namespace = "http://category.anonymous.ws.listingapi.gg.com", IsNullable = true)]
    public class GetCategoriesByCodesRequest
    {

        [XmlArray("categoryCodes", Namespace = "")]
        [XmlArrayItem("item", Form = XmlSchemaForm.Unqualified)]
        public string[] CategoryCodes;

        [XmlElement("withSpecs", Namespace = "")]
        public bool WithSpecs;

        [XmlElement("withDeepest", Namespace = "")]
        public bool WithDeepest;

        [XmlElement("withCatalog", Namespace = "")]
        public bool WithCatalog;

        [XmlElement("lang", Namespace = "")]
        public string Lang;

        public GetCategoriesByCodesRequest()
        {
        }

        public GetCategoriesByCodesRequest(string[] categoryCodes, bool withSpecs, bool withDeepest, bool withCatalog, string lang)
        {
            this.CategoryCodes = categoryCodes;
            this.WithSpecs = withSpecs;
            this.WithDeepest = withDeepest;
            this.WithCatalog = withCatalog;
            this.Lang = lang;
        }
    }
}
