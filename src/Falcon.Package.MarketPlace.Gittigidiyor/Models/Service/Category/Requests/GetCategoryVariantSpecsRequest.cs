﻿using System.Xml.Serialization;
using Falcon.Package.MarketPlace.Gittigidiyor.Attributes;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Category.Responses;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Category.Requests
{
    [UrlFragment("CategoryService")]
    [ResponseType(typeof(GetCategoryVariantSpecsResponse))]
    [XmlRoot("getCategoryVariantSpecs", Namespace = "http://category.anonymous.ws.listingapi.gg.com", IsNullable = true)]
    public class GetCategoryVariantSpecsRequest
    {
        [XmlElement("categoryCode", Namespace = "")]
        public string CategoryCode;

        [XmlElement("lang", Namespace = "")]
        public string Lang;

        public GetCategoryVariantSpecsRequest()
        {
        }

        public GetCategoryVariantSpecsRequest(string categoryCode, string lang)
        {
            this.CategoryCode = categoryCode;
            this.Lang = lang;
        }
    }
}
