﻿using System.Xml.Serialization;
using Falcon.Package.MarketPlace.Gittigidiyor.Attributes;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Category.Responses;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Category.Requests
{
    [UrlFragment("CategoryService")]
    [ResponseType(typeof(GetCategorySpecsResponse))]
    [XmlRoot("getCategorySpecs", Namespace = "http://category.anonymous.ws.listingapi.gg.com", IsNullable = true)]
    public class GetCategorySpecsRequest
    {

        [XmlElement("categoryCode",Namespace = "")]
        public string CategoryCode;

        [XmlElement("lang", Namespace = "")]
        public string Lang;

        public GetCategorySpecsRequest()
        {
        }

        public GetCategorySpecsRequest(string categoryCode, string lang)
        {
            CategoryCode = categoryCode;
            Lang = lang;
        }
    }
}
