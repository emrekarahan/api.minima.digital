﻿using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Category.Responses
{
    [XmlRoot("getCategoriesByCodesResponse", Namespace = "http://category.anonymous.ws.listingapi.gg.com")]
    public class GetCategoriesByCodesResponse
    {

        [XmlElement("return", Namespace = "", Order = 0)]
        public CategoryServiceResponse CategoryServiceResponse;

        public GetCategoriesByCodesResponse()
        {
        }

        public GetCategoriesByCodesResponse(CategoryServiceResponse @return)
        {
            CategoryServiceResponse = @return;
        }
    }
}
