﻿using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Category.Responses
{
    [XmlRoot("getSubCategoriesResponse", Namespace = "http://category.anonymous.ws.listingapi.gg.com")]
    public class GetSubCategoriesResponse
    {

        [XmlElement("return", Namespace = "", Order = 0)]
        public CategoryServiceResponse CategoryServiceResponse;

        public GetSubCategoriesResponse()
        {
        }

        public GetSubCategoriesResponse(CategoryServiceResponse @return)
        {
            this.CategoryServiceResponse = @return;
        }
    }
}
