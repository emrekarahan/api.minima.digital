﻿using System.Xml.Schema;
using System.Xml.Serialization;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Category.Types;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Category.Responses
{
    [XmlType("categoryServiceResponse", Namespace = "http://category.anonymous.ws.listingapi.gg.com")]
    public class CategoryServiceResponse : BaseResponseService
    {
        [XmlElement("categoryCount", Form = XmlSchemaForm.Unqualified, Order = 0)]
        public int CategoryCount { get; set; }

        [XmlIgnore]
        public bool categoryCountSpecified { get; set; }

        [XmlArray("categories", Form = XmlSchemaForm.Unqualified, Order = 1)]
        [XmlArrayItem("category", Form = XmlSchemaForm.Unqualified)]
        public CategoryType[] Categories { get; set; }
    }
}
