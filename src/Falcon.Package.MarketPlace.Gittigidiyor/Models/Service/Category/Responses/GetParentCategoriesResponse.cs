﻿using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Category.Responses
{
    [XmlRoot("getParentCategoriesResponse", Namespace = "http://category.anonymous.ws.listingapi.gg.com")]
    public class GetParentCategoriesResponse
    {

        [XmlElement("return", Namespace = "", Order = 0)]
        public CategoryServiceResponse CategoryServiceResponse;

        public GetParentCategoriesResponse()
        {
        }

        public GetParentCategoriesResponse(CategoryServiceResponse @return)
        {
            this.CategoryServiceResponse = @return;
        }
    }
}
