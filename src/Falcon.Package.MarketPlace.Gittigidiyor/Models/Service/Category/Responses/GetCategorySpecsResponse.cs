﻿
using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Category.Responses
{
    [XmlRoot("getCategorySpecsResponse", Namespace = "http://category.anonymous.ws.listingapi.gg.com")]
    public class GetCategorySpecsResponse
    {

        [XmlElement("return", Namespace = "", Order = 0)]
        public CategorySpecsServiceResponse CategorySpecsServiceResponse;

        public GetCategorySpecsResponse()
        {
        }

        public GetCategorySpecsResponse(CategorySpecsServiceResponse @return)
        {
            this.CategorySpecsServiceResponse = @return;
        }
    }
}
