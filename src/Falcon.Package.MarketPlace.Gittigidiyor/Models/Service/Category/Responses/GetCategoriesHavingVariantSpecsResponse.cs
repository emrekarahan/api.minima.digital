﻿using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Category.Responses
{
    [XmlRoot("getCategoriesHavingVariantSpecsResponse", Namespace = "http://category.anonymous.ws.listingapi.gg.com")]
    public class GetCategoriesHavingVariantSpecsResponse
    {

        [XmlElement("return", Namespace = "", Order = 0)]
        public CategoryServiceResponse CategoryServiceResponse;

        public GetCategoriesHavingVariantSpecsResponse()
        {
        }

        public GetCategoriesHavingVariantSpecsResponse(CategoryServiceResponse @return)
        {
            this.CategoryServiceResponse = @return;
        }
    }
}
