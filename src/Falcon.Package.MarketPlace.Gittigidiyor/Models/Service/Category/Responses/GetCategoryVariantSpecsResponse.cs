﻿using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Category.Responses
{
    [XmlRoot("getCategoryVariantSpecsResponse", Namespace = "http://category.anonymous.ws.listingapi.gg.com")]
    public class GetCategoryVariantSpecsResponse
    {

        [XmlElement("return", Namespace = "", Order = 0)]
        public CategoryServiceVariantResponse CategoryServiceVariantResponse;

        public GetCategoryVariantSpecsResponse()
        {
        }

        public GetCategoryVariantSpecsResponse(CategoryServiceVariantResponse @return)
        {
            CategoryServiceVariantResponse = @return;
        }
    }
}
