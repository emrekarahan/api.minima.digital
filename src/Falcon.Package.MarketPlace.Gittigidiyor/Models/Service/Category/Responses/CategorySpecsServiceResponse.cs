﻿using System.Xml.Schema;
using System.Xml.Serialization;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Category.Types;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Category.Responses
{
    [XmlType("categorySpecsServiceResponse", Namespace = "http://category.anonymous.ws.listingapi.gg.com")]
    public class CategorySpecsServiceResponse : BaseResponseService
    {
        [XmlArray("specs", Form = XmlSchemaForm.Unqualified, Order = 0)]
        [XmlArrayItem("spec", Form = XmlSchemaForm.Unqualified)]
        public CategorySpecType[] Specs { get; set; }
    }
}
