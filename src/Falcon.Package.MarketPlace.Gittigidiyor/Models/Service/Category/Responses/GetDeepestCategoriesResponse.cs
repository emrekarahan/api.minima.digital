﻿using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Category.Responses
{
    [XmlRoot("getDeepestCategoriesResponse", Namespace = "http://category.anonymous.ws.listingapi.gg.com")]
    public class GetDeepestCategoriesResponse
    {

        [XmlElement("return", Namespace = "", Order = 0)]
        public CategoryServiceResponse CategoryServiceResponse;

        public GetDeepestCategoriesResponse()
        {
        }

        public GetDeepestCategoriesResponse(CategoryServiceResponse @return)
        {
            CategoryServiceResponse = @return;
        }
    }
}
