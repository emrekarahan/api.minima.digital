﻿using System.Xml.Schema;
using System.Xml.Serialization;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Category.Types;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Category.Responses
{
    [XmlType("categoryServiceVariantResponse", Namespace = "http://category.anonymous.ws.listingapi.gg.com")]
    public class CategoryServiceVariantResponse : BaseResponseService
    {
        [XmlElement("specCount", Form = XmlSchemaForm.Unqualified, Order = 0)]
        public long SpecCount { get; set; }

        [XmlIgnore()]
        public bool specCountSpecified { get; set; }

        [XmlArray("specs", Form = XmlSchemaForm.Unqualified, Order = 1)]
        [XmlArrayItem("spec", Form = XmlSchemaForm.Unqualified)]
        public CategoryVariantSpecType[] Specs { get; set; }
    }
}
