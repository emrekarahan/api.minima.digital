﻿using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Category.Responses
{
    [XmlRoot("getCategoryResponse", Namespace = "http://category.anonymous.ws.listingapi.gg.com")]
    public class GetCategoryResponse
    {
        [XmlElement("return", Namespace = "", Order = 0)]
        public CategoryServiceResponse CategoryServiceResponse;

        public GetCategoryResponse()
        {
        }

        public GetCategoryResponse(CategoryServiceResponse @return)
        {
            CategoryServiceResponse = @return;
        }
    }
}
