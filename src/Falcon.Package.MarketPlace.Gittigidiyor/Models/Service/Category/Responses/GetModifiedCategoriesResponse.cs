﻿using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Category.Responses
{
    [XmlRoot("getModifiedCategoriesResponse", Namespace = "http://category.anonymous.ws.listingapi.gg.com")]
    public class GetModifiedCategoriesResponse
    {

        [XmlElement("return", Namespace = "", Order = 0)]
        public CategoryServiceAuditResponse CategoryServiceAuditResponse;

        public GetModifiedCategoriesResponse()
        {
        }

        public GetModifiedCategoriesResponse(CategoryServiceAuditResponse @return)
        {
            CategoryServiceAuditResponse = @return;
        }
    }
}
