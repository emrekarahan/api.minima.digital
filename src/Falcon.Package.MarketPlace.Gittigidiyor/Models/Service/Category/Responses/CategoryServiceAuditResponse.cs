﻿using System.Xml.Schema;
using System.Xml.Serialization;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Category.Types;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Category.Responses
{
    [XmlType("categoryServiceAuditResponse", Namespace = "http://category.anonymous.ws.listingapi.gg.com")]
    public class CategoryServiceAuditResponse : BaseResponseService
    {
        [XmlElement("totalCount", Form = XmlSchemaForm.Unqualified, Order = 0)]
        public int TotalCount { get; set; }

        [XmlIgnore]
        public bool totalCountSpecified { get; set; }

        [XmlElement("count", Form = XmlSchemaForm.Unqualified, Order = 1)]
        public int Count { get; set; }

        [XmlIgnore()]
        public bool countSpecified { get; set; }

        [XmlArray("categoryAudits", Form = XmlSchemaForm.Unqualified, Order = 2)]
        [XmlArrayItem("categoryAudit", Form = XmlSchemaForm.Unqualified)]
        public CategoryAuditType[] CategoryAudits { get; set; }
    }
}
