﻿using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Category.Responses
{
    [XmlRoot("getCategorySpecsWithDetailResponse", Namespace = "http://category.anonymous.ws.listingapi.gg.com")]
    public class GetCategorySpecsWithDetailResponse
    {

        [XmlElement("return", Namespace = "", Order = 0)]
        public CategorySpecsWithDetailServiceResponse CategorySpecsWithDetailServiceResponse;

        public GetCategorySpecsWithDetailResponse()
        {
        }

        public GetCategorySpecsWithDetailResponse(CategorySpecsWithDetailServiceResponse @return)
        {
            this.CategorySpecsWithDetailServiceResponse = @return;
        }
    }
}
