﻿using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Category.Responses
{
    [XmlRoot("getCategoriesResponse", Namespace = "http://category.anonymous.ws.listingapi.gg.com")]
    public class GetCategoriesResponse
    {

        [XmlElement("return", Namespace = "", Order = 0)]
        public CategoryServiceResponse CategoryServiceResponse;

        public GetCategoriesResponse()
        {
        }

        public GetCategoriesResponse(CategoryServiceResponse @return)
        {
            CategoryServiceResponse = @return;
        }
    }
}
