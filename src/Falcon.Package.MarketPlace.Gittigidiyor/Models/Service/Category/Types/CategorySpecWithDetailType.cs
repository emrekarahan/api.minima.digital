﻿using System.Xml.Schema;
using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Category.Types
{
    [XmlType("categorySpecWithDetailType", Namespace = "http://category.anonymous.ws.listingapi.gg.com")]
    public class CategorySpecWithDetailType
    {
        [XmlArray("values", Form = XmlSchemaForm.Unqualified, Order = 0)]
        [XmlArrayItem("value", Form = XmlSchemaForm.Unqualified)]
        public CategorySpecValueType[] Values { get; set; }

        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlAttribute("required")]
        public bool Required { get; set; }

        [XmlIgnore()]
        public bool requiredSpecified { get; set; }

        [XmlAttribute("type")]
        public string Type { get; set; }

        [XmlAttribute("specId")]
        public int SpecId { get; set; }

        [XmlIgnore()]
        public bool specIdSpecified { get; set; }

        [XmlAttribute("childSpecId")]
        public int ChildSpecId { get; set; }
        
        [XmlIgnore()]
        public bool childSpecIdSpecified { get; set; }
    }
}
