﻿using System.Xml.Schema;
using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Category.Types
{
    [XmlType("categoryAuditType", Namespace = "http://category.anonymous.ws.listingapi.gg.com")]
    public class CategoryAuditType : BaseAuditType
    {
        [XmlElement("categoryCode", Form = XmlSchemaForm.Unqualified, Order = 0)]
        public string CategoryCode { get; set; }
    }
}
