﻿using System.Xml.Schema;
using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Category.Types
{
    [XmlType("categorySpecValueType", Namespace = "http://category.anonymous.ws.listingapi.gg.com")]
    public class CategorySpecValueType
    {
        [XmlElement("name", Form = XmlSchemaForm.Unqualified, Order = 0)]
        public string Name { get; set; }

        [XmlAttribute("specId")]
        public int SpecId { get; set; }

        [XmlIgnore]
        public bool specIdSpecified { get; set; }
        
        [XmlAttribute("parentSpecId")]
        public int ParentSpecId { get; set; }

        [XmlIgnore]
        public bool parentSpecIdSpecified { get; set; }
    }
}
