﻿using System.Xml.Schema;
using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Category.Types
{
    [XmlInclude(typeof(CategoryAuditType))]
    [XmlType("baseAuditType", Namespace = "http://category.anonymous.ws.listingapi.gg.com")]
    public abstract class BaseAuditType
    {
        [XmlElement("changeType", Form = XmlSchemaForm.Unqualified, Order = 0)]
        public string ChangeType { get; set; }
    }
}
