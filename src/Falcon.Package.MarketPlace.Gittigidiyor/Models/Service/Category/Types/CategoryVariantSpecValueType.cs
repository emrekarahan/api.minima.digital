﻿using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Category.Types
{
    [XmlType("categoryVariantSpecValueType", Namespace = "http://category.anonymous.ws.listingapi.gg.com")]
    public class CategoryVariantSpecValueType
    {
        [XmlAttribute("valueId")]
        public long ValueId { get; set; }

        [XmlIgnore()]
        public bool valueIdSpecified { get; set; }

        [XmlAttribute("value")]
        public string Value { get; set; }

        [XmlAttribute("orderNumber")]
        public int OrderNumber { get; set; }

        [XmlIgnore()]
        public bool orderNumberSpecified { get; set; }
    }
}
