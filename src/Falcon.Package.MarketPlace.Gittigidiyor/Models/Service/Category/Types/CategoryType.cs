﻿using System.Xml.Schema;
using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Category.Types
{
    [XmlType("categoryType", Namespace = "http://category.anonymous.ws.listingapi.gg.com")]
    public class CategoryType
    {
        [XmlElement("categoryCode", Form = XmlSchemaForm.Unqualified, Order = 0)]
        public string CategoryCode { get; set; }

        [XmlElement("categoryName", Form = XmlSchemaForm.Unqualified, Order = 1)]
        public string CategoryName { get; set; }

        [XmlArray("specs", Form = XmlSchemaForm.Unqualified, Order = 2)]
        [XmlArrayItem("spec", Form = XmlSchemaForm.Unqualified)]
        public CategorySpecType[] Specs { get; set; }

        [XmlAttribute("deepest")] 
        public bool Deepest { get; set; }

        [XmlIgnore] public bool deepestSpecified { get; set; }

        [XmlAttribute("hasCatalog")] 
        public bool HasCatalog { get; set; }

        [XmlIgnore] 
        public bool hasCatalogSpecified { get; set; }
    }
}