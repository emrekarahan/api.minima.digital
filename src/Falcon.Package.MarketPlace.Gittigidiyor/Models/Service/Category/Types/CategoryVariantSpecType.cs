﻿using System.Xml.Schema;
using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Category.Types
{
    [XmlType("categoryVariantSpecType", Namespace = "http://category.anonymous.ws.listingapi.gg.com")]
    public class CategoryVariantSpecType
    {
        [XmlArray("specValues", Form = XmlSchemaForm.Unqualified, Order = 0)]
        [XmlArrayItem("specValue", Form = XmlSchemaForm.Unqualified)]
        public CategoryVariantSpecValueType[] SpecValues { get; set; }

        [XmlAttribute("nameId")]
        public long NameId { get; set; }

        [XmlIgnore()]
        public bool nameIdSpecified { get; set; }

        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlAttribute("type")]
        public string Type { get; set; }

        [XmlAttribute("orderNumber")]
        public int OrderNumber { get; set; }

        [XmlIgnore()]
        public bool orderNumberSpecified { get; set; }

        [XmlAttribute("base")]
        public bool @Base { get; set; }

        [XmlIgnore()]
        public bool baseSpecified { get; set; }
    }
}
