﻿using System.Xml.Schema;
using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service
{
    public class ErrorTypeService
    {
        [XmlElement("errorId", Form = XmlSchemaForm.Unqualified, Order = 0)]
        public string ErrorId { get; set; }

        [XmlElement("errorCode", Form = XmlSchemaForm.Unqualified, Order = 1)]
        public string ErrorCode { get; set; }

        [XmlElement("message", Form = XmlSchemaForm.Unqualified, Order = 2)]
        public string Message { get; set; }

        [XmlElement("viewMessage", Form = XmlSchemaForm.Unqualified, Order = 3)]
        public string ViewMessage { get; set; }
    }
}