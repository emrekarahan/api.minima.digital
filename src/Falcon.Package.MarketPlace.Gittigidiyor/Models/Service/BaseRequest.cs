﻿using System.Xml.Serialization;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.Service
{
    public class BaseRequest
    {
        [XmlElement("lang", Namespace = "")]
        public string Lang;
    }
}
