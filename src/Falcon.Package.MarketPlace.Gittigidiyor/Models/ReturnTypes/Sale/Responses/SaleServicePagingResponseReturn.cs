﻿namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Sale.Responses
{
    public class SaleServicePagingResponseReturn : BaseResponseReturn
    {
        public bool NextPageAvailable { get; set; }
    }
}
