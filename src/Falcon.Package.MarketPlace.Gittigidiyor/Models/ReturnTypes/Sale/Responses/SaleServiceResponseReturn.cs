﻿using Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Sale.Types;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Sale.Responses
{
    public class SaleServiceResponseReturn : BaseResponseReturn
    {
        public int SaleCount { get; set; }
        public SaleTypeReturn[] Sales { get; set; }
        public SellerPromotionInfoTypeReturn[] SellerPromotionInfos { get; set; }
    }
}
