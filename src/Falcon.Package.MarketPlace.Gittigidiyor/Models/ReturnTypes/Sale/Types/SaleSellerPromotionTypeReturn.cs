﻿namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Sale.Types
{
    public class SaleSellerPromotionTypeReturn
    {
        public decimal DiscountedPrice { get; set; }
        public decimal Price { get; set; }
        public int ProductId { get; set; }
        public string ProductUrl { get; set; }
        public string Title { get; set; }
        public int Quantity { get; set; }
        public int RetailVariantId { get; set; }
        public int SaleSellerPromotionId { get; set; }
        public SellerPromotionReturn SellerPromotion { get; set; }
    }
}