﻿namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Sale.Types
{
    public class SellerPromotionItemReturn
    {
        public string ItemId { get; set; }
        public MainProductInfoDtoReturn[] MainProducts { get; set; }
        public GiftProductInfoDtoReturn[] Gifts { get; set; }
    }
}