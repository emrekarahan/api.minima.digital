﻿namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Sale.Types
{
    public class ImageDtoReturn
    {
        public int ImageId { get; set; }
        public ImageUrlDtoReturn[] ImageUrls { get; set; }
        public bool ZoomInSupport { get; set; }
    }
}