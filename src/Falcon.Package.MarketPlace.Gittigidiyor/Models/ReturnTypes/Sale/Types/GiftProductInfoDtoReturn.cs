﻿using System;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Sale.Types
{
    public class GiftProductInfoDtoReturn
    {
        public int ProductId { get; set; }
        public long RetailVariantId { get; set; }
        public VariantDtoReturn RetailVariant { get; set; }
        public GiftPromotionTypeReturn Type { get; set; }
        public decimal Value { get; set; }
        public string Title { get; set; }
        public ImageDtoReturn[] Images { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int StockQuantity { get; set; }
        public string ProductUrl { get; set; }
        public VariantDtoReturn[] PossibleGiftVariants { get; set; }
    }
}