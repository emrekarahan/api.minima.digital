﻿namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Sale.Types
{
    public class SaleInvoiceTypeReturn
    {
        public string Fullname { get; set; }
        public string Address { get; set; }
        public string District { get; set; }
        public int CityCode { get; set; }
        public string ZipCode { get; set; }
        public string PhoneNumber { get; set; }
        public string TaxOffice { get; set; }
        public string TaxNumber { get; set; }
        public string CompanyTitle { get; set; }
        public string TcCertificate { get; set; }
    }
}