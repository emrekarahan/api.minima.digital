﻿namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Sale.Types
{
    public class SaleTypeReturn
    {
        public string SaleCode { get; set; }
        public string Status { get; set; }
        public string StatusCode { get; set; }
        public int ProductId { get; set; }
        public string ProductTitle { get; set; }
        public string Price { get; set; }
        public string CargoPayment { get; set; }
        public string CargoCode { get; set; }
        public int Amount { get; set; }
        public string EndDate { get; set; }
        public OrderBuyerInfoTypeReturn BuyerInfo { get; set; }
        public string ChangeStatus { get; set; }
        public string ThumbImageLink { get; set; }
        public string LastActionDate { get; set; }
        public int?[] WaitingProcesses { get; set; }
        public SaleInvoiceTypeReturn InvoiceInfo { get; set; }
        public long VariantId { get; set; }
        public ItemVariantSpecTypeReturn[] VariantSpecs { get; set; }
        public string MoneyDate { get; set; }
        public string CargoApprovementDate { get; set; }
        public string ItemId { get; set; }
        public ShippingInfoTypeReturn ShippingInfo { get; set; }
        public double CommissionRate { get; set; }
    }
}