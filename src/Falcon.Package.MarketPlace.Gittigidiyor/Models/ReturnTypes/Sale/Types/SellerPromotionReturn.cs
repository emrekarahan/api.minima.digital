﻿using System;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Sale.Types;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Sale.Types
{
    public class SellerPromotionReturn
    {
        public int BasketLimit { get; set; }
        public DateTime EndDate { get; set; }
        public GiftInfoTypeReturn GiftInfo { get; set; }
        public int GiftQuantity { get; set; }
        public DateTime InsertedAt { get; set; }
        public SellerPromotionItemReturn[] Items { get; set; }
        public int MainProductQuantity { get; set; }
        public string Name { get; set; }
        public int SelectedGiftVariantId { get; set; }
        public int?[] SellerIds { get; set; }
        public SellerPromotionTypeReturn SellerPromotionType { get; set; }
        public DateTime StartDate { get; set; }
        public SellerPromotionStatusType Status { get; set; }
        public DateTime UpdatedAt { get; set; }
        public int UsageLimit { get; set; }
    }
}