﻿namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Sale.Types
{
    public class OrderBuyerInfoTypeReturn
    {
        public string Username { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Phone { get; set; }
        public string MobilePhone { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string District { get; set; }
        public string City { get; set; }
        public NeighborhoodTypeReturn NeighborhoodType { get; set; }
        public string ZipCode { get; set; }
        public int ProcessCount { get; set; }
        public int RatePercentage { get; set; }

    }
}