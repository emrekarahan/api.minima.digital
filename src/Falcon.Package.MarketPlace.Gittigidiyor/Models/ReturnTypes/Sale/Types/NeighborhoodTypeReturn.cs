﻿namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Sale.Types
{
    public class NeighborhoodTypeReturn
    {
        public int NeighborhoodId { get; set; }
        public string NeighborhoodName { get; set; }
        public int DistrictId { get; set; }
        public bool Active { get; set; }
    }
}