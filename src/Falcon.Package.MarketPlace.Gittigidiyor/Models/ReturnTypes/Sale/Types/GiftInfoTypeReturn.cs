﻿namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Sale.Types
{
    public class GiftInfoTypeReturn
    {
        public AppliesToReturn AppliesTo { get; set; }
        public TypeReturn Type { get; set; }
        public decimal Value { get; set; }
    }
}