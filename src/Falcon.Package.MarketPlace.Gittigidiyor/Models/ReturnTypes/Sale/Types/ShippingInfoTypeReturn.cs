﻿namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Sale.Types
{
    public class ShippingInfoTypeReturn
    {
        public string DeliveryOption { get; set; }
        public string ShippingFirmName { get; set; }
        public int ShippingFirmId { get; set; }
        public string CombinedShipping { get; set; }
        public ShippingPaymentTypeReturn ShippingPaymentType { get; set; }
        public string CargoCode { get; set; }
        public string ShippingNotice { get; set; }
        public string[] CombinedSaleCodes { get; set; }
        public string ShippingExpireDate { get; set; }
    }
}