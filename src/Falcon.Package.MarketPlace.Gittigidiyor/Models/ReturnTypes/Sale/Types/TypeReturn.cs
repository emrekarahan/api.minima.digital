﻿namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Sale.Types
{
    public enum TypeReturn
    {
        COUNT,
        PERCENTAGE,
        PRICE,
        FIXED_PRICE,
    }
}
