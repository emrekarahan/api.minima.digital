﻿namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Sale.Types
{
    public class ProductCatalogDtoReturn
    {
        public int NewCatalogId { get; set; }
    }
}