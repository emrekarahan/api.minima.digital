﻿namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Sale.Types
{
    public class SellerPromotionInfoTypeReturn
    {
        public SaleInvoiceTypeReturn[] Sales { get; set; }

        public SaleSellerPromotionTypeReturn[] SaleSellerPromotions { get; set; }
    }
}