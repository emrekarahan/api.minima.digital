﻿namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Sale.Types
{
    public enum ShippingPaymentTypeReturn
    {
        BUYER,
        SELLER,
        PAY_IN_THE_BASKET,
        PLATFORM,
    }
}
