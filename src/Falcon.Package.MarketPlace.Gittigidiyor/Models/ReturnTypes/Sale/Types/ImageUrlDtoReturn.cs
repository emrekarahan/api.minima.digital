﻿namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Sale.Types
{
    public class ImageUrlDtoReturn
    {
        public string ImageUrl { get; set; }
        public string Size { get; set; }
    }
}