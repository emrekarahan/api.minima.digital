﻿namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Sale.Types
{
    public enum GiftPromotionTypeReturn
    {
        COUNT,
        PERCENTAGE,
        PRICE,
        FIXED_PRICE
    }
}