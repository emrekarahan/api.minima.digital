﻿namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Sale.Types
{
    public enum SellerPromotionTypeReturn
    {
        SAME_PRODUCT,
        DIFFERENT_PRODUCT,
        SAME_OR_DIFFERENT_PRODUCT,
    }
}
