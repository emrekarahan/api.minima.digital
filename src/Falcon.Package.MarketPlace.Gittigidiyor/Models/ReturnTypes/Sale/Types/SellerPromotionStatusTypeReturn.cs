﻿namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Sale.Types
{
    public enum SellerPromotionStatusTypeReturn
    {
        ACTIVE,
        PASSIVE,
        DRAFT,
        PLANNED,
    }
}
