﻿namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Sale.Types
{
    public class ItemVariantSpecTypeReturn
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public int Quantity { get; set; }
        public int SoldQuantity { get; set; }
        public string StockCode { get; set; }
    }
}