﻿namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Sale.Types
{
    public class VariantSpecDtoReturn
    {
        public int SoldQuantity { get; set; }
        public int Quantity { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public string StockCode { get; set; }
        public int Order { get; set; }
        public int SpecDataId { get; set; }
        public int SpecId { get; set; }
    }
}