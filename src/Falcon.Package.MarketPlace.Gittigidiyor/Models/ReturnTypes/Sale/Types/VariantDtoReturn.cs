﻿namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Sale.Types
{
    public class VariantDtoReturn
    {
        public ImageDtoReturn Image { get; set; }
        public int ProductId { get; set; }
        public int Quantity { get; set; }
        public int SoldQuantity { get; set; }
        public VariantSpecDtoReturn[] Specs { get; set; }
        public string StockCode { get; set; }
        public long VariantId { get; set; }
        public ProductCatalogDtoReturn Catalog { get; set; }
    }
}