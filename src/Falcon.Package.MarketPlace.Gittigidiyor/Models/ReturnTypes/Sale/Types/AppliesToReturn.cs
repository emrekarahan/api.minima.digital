﻿namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Sale.Types
{
    public enum AppliesToReturn
    {
        TOTAL,
        GIFT,
    }
}
