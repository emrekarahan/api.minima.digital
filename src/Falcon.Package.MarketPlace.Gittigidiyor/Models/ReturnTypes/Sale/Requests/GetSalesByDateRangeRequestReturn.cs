﻿namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Sale.Requests
{
    public class GetSalesByDateRangeRequestReturn : BaseRequestReturn
    {
        public bool WithData { get; set; }
        public string ByStatus { get; set; }
        public string ByUser { get; set; }
        public string OrderBy { get; set; }
        public string OrderType { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
}
