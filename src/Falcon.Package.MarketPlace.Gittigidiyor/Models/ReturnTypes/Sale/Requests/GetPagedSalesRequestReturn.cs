﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Sale.Requests
{
    public class GetPagedSalesRequestReturn : BaseRequestReturn
    {
        public bool WithData { get; set; }
        public string ByStatus { get; set; }
        public string ByUser { get; set; }
        public string OrderBy { get; set; }
        public string OrderType { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
}
