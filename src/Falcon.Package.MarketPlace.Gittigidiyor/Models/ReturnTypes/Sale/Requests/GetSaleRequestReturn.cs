﻿namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Sale.Requests
{
    public class GetSaleRequestReturn : BaseRequestReturn
    {
        public string SaleCode { get; set; }
    }
}
