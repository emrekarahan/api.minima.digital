﻿using Newtonsoft.Json;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes
{
    public class BaseResponseReturn
    {
        [JsonProperty("ackCode")]
        public string AckCode { get; set; }

        public bool IsSuccess => AckCode == "success";

        [JsonProperty("responseTime")]
        public string ResponseTime { get; set; }

        [JsonProperty("error")]
        public ErrorTypeReturn Error { get; set; }


        [JsonProperty("timeElapsed")]
        public string TimeElapsed { get; set; }
    }
}
