﻿namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Products.Requests
{
    public class GetProductRequestReturn : BaseRequestReturn
    {
        public string ProductId { get; set; }

        public string ItemId { get; set; }
    }
}
