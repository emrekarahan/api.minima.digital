﻿namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Products.Requests
{
    public class GetProductIdsRequestReturn : BaseRequestReturn
    {
        public int StartOffSet { get; set; }
        public int RowCount { get; set; }
        public string[] StatusTypes { get; set; }
    }
}
