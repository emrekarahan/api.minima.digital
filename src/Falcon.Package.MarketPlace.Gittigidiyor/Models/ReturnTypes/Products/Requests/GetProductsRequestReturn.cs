﻿namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Products.Requests
{
    public class GetProductsRequestReturn : BaseRequestReturn
    {
        public GetProductsRequestReturn()
        {
            WithData = true;
        }
        public int StartOffset { get; set; }
        public int RowCount { get; set; }
        //public ProductStatus Status { get; set; }
        public string Status { get; set; }
        public bool WithData { get; set; }
    }
}
