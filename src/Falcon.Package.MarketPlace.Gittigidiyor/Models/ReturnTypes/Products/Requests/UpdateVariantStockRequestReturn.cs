﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Products.Requests
{
    public class UpdateVariantStockRequestReturn : BaseRequestReturn
    {
        public string ProductId { get; set; }
        public string ItemId { get; set; }
        public string VariantId { get; set; }
        public int Stock { get; set; }
    }
}
