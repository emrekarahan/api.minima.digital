﻿namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Products.Requests
{
    public class GetNewlyListedProductIdListRequestReturn : BaseRequestReturn
    {
        public int StartOffSet { get; set; }
        public int RowCount { get; set; }
        public bool ViaApi { get; set; }
    }
}
