﻿namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Products.Requests
{
    public class UpdatePriceRequestReturn : BaseRequestReturn
    {
        public string ProductId { get; set; }
        public string ItemId { get; set; }
        public double Price { get; set; }
        public bool CancelBid { get; set; }

    }
}
