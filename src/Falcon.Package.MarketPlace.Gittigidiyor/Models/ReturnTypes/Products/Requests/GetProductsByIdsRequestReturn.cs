﻿namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Products.Requests
{
    public class GetProductsByIdsRequestReturn : BaseRequestReturn
    {
        public string[] ItemIdList { get; set; }
        public int?[] ProductIdList { get; set; }
        public bool WithData { get; set; }
    }
}
