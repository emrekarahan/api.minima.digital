﻿namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Products.Requests
{
    public class GetStockAndPriceRequestReturn : BaseRequestReturn
    {
        public int?[] ProductIdList { get; set; }
        public string[] ItemIdList { get; set; }
    }
}
