﻿namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Products.Requests
{
    public class UpdatePriceByPercentageRequestReturn : BaseRequestReturn
    {
        public string ProductId { get; set; }
        public string ItemId { get; set; }
        public string OperatorType { get; set; }
        public int Percentage { get; set; }
    }
}
