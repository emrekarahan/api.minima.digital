﻿using Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Products.Types;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Products.Requests
{
    public class UpdateProductWithNewCargoDetailRequestReturn : BaseRequestReturn
    {
        public string ItemId { get; set; }
        public string ProductId { get; set; }
        public ProductTypeReturn Product { get; set; }
    }
}
