﻿namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Products.Requests
{
    public class DeleteProductRequestReturn : BaseRequestReturn
    {
        public int?[] ProductIdList { get; set; }
        public string[] ItemIdList { get; set; }


    }
}
