﻿using Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Products.Types;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Products.Requests
{
    public class InsertAndActivateProductRequestReturn : BaseRequestReturn
    {
        public string ItemId  { get; set; }
        public ProductTypeReturn Product { get; set; }
        public bool ForceToSpecEntry { get; set; }
        public bool NextDateOption { get; set; }
    }
}
