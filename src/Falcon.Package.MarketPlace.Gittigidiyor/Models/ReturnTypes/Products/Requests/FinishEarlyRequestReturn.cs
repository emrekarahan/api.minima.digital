﻿namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Products.Requests
{
    public class FinishEarlyRequestReturn : BaseRequestReturn
    {
        public int?[] ProductIdArray { get; set; }
        public string?[] ItemIdArray { get; set; }
    }
}


