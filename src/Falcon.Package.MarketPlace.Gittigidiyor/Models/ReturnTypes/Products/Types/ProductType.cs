﻿using Newtonsoft.Json;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Products.Types
{
    public class ProductTypeReturn
    {
        [JsonProperty("categoryCode")]
        public string CategoryCode { get; set; }

        [JsonProperty("storeCategoryId")]
        public int StoreCategoryId { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("subtitle")]
        public string Subtitle { get; set; }

        [JsonProperty("specs")]
        public SpecTypeReturn[] Specs { get; set; }

        [JsonProperty("photos")]
        public PhotoTypeReturn[] Photos { get; set; }

        [JsonProperty("pageTemplate")]
        public int PageTemplate { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("startDate")]
        public string StartDate { get; set; }

        [JsonProperty("catalogId")]
        public int CatalogId { get; set; }
        
        [JsonProperty("newCatalogId")]
        public int NewCatalogId { get; set; }

        [JsonProperty("catalogDetail")]
        public int CatalogDetail { get; set; }
        
        [JsonProperty("catalogFilter")]
        public string CatalogFilter { get; set; }

        [JsonProperty("format")]
        public string Format { get; set; }

        [JsonProperty("startPrice")]
        public double StartPrice { get; set; }
        
        [JsonProperty("buyNowPrice")]
        public double BuyNowPrice { get; set; }
        
        [JsonProperty("netEarning")]
        public double NetEarning { get; set; }

        [JsonProperty("listingDays")]
        public int ListingDays { get; set; }

        [JsonProperty("productCount")]
        public int ProductCount { get; set; }

        [JsonProperty("cargoDetail")]
        public CargoDetailTypeReturn CargoDetail { get; set; }

        [JsonProperty("affiliateOption")]
        public bool AffiliateOption { get; set; }

        [JsonProperty("boldOption")]
        public bool BoldOption { get; set; }

        [JsonProperty("catalogOption")]
        public bool CatalogOption { get; set; }

        [JsonProperty("vitrineOption")]
        public bool VitrineOption { get; set; }

        [JsonProperty("variantGroups")]
        public VariantGroupTypeReturn[] VariantGroups { get; set; }

        [JsonProperty("auctionProfilePercentage")]
        public int AuctionProfilePercentage { get; set; }

        [JsonProperty("marketPrice")]
        public double MarketPrice { get; set; }

        [JsonProperty("globalTradeItemNo")]
        public string GlobalTradeItemNo { get; set; }

        [JsonProperty("manufacturerPartNo")]
        public string ManufacturerPartNo { get; set; }

        [JsonProperty("sameDayDeliveryTypes")]
        public SameDayDeliveryTypeReturn[] SameDayDeliveryTypes { get; set; }
    }
}
