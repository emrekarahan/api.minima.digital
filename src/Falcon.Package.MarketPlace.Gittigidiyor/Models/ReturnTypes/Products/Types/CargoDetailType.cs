﻿using Newtonsoft.Json;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Products.Types
{
    public class CargoDetailTypeReturn
    {
        [JsonProperty("city")]
        public string City { get; set; }

        [JsonProperty("cargoCompanies")]
        public string[] CargoCompanies { get; set; }

        [JsonProperty("shippingPayment")]
        public string ShippingPayment { get; set; }

        [JsonProperty("cargoDescription")]
        public string CargoDescription { get; set; }

        [JsonProperty("shippingWhere")]
        public string ShippingWhere { get; set; }

        [JsonProperty("shippingFeePaymentType")]
        public string ShippingFeePaymentType { get; set; }

        [JsonProperty("cargoCompanyDetails")]
        public CargoCompanyDetailTypeReturn[] CargoCompanyDetails { get; set; }

        [JsonProperty("shippingTime")]
        public CargoTimeTypeReturn ShippingTime { get; set; }

        [JsonProperty("productPackageSize")]
        public ProductPackageSizeTypeReturn ProductPackageSize { get; set; }
    }
}
