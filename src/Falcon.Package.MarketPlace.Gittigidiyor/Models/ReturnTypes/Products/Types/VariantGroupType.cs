﻿using Newtonsoft.Json;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Products.Types
{
    public class VariantGroupTypeReturn
    {
        [JsonProperty("variants")]
        public VariantTypeReturn[] Variants { get; set; }

        [JsonProperty("photos")]
        public PhotoTypeReturn[] Photos { get; set; }

        [JsonProperty("nameId")]
        public long NameId { get; set; }

        [JsonProperty("valueId")]
        public long ValueId { get; set; }

        [JsonProperty("alias")]
        public string Alias { get; set; }
    }
}
