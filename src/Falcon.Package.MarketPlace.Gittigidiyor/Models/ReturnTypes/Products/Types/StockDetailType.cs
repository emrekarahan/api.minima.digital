﻿namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Products.Types
{
    public class StockDetailTypeReturn
    {
        public string ItemId { get; set; }

        public int ProductId { get; set; }

        public string Format { get; set; }

        public int Amount { get; set; }

        public int SoldItemCount { get; set; }

        public double StartPrice { get; set; }
        
        public double BuyNowPrice { get; set; }
    }
}
