﻿using Newtonsoft.Json;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Products.Types
{
    public class SameDayDeliveryTypeReturn
    {
        [JsonProperty("lastReceivingTime")]
        public string LastReceivingTime { get; set; }

        [JsonProperty("shippingFirmId")]
        public int ShippingFirmId { get; set; }
    }
}
