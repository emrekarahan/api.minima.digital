﻿using Newtonsoft.Json;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Products.Types
{
    public class VariantSpecTypeReturn
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("valueId")]
        public long ValueId { get; set; }

        [JsonProperty("value")]
        public string Value { get; set; }

        [JsonProperty("orderNumber")]
        public int OrderNumber { get; set; }

        [JsonProperty("specDataOrderNumber")]
        public int SpecDataOrderNumber { get; set; }

    }
}
