﻿using Newtonsoft.Json;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Products.Types
{
    public class CargoCompanyDetailTypeReturn
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("value")]
        public string Value { get; set; }

        [JsonProperty("cityPrice")]
        public string CityPrice { get; set; }

        [JsonProperty("countryPrice")]
        public string CountryPrice { get; set; }
    }
}
