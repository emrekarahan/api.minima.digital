﻿using Newtonsoft.Json;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Products.Types
{
    public class CargoTimeTypeReturn
    {
        [JsonProperty("days")]
        public string Days { get; set; }

        [JsonProperty("beforeTime")]
        public string BeforeTime { get; set; }
    }
}
