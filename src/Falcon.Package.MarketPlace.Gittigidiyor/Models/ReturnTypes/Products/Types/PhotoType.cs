﻿using Newtonsoft.Json;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Products.Types
{
    public class PhotoTypeReturn
    {
        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("base64")]
        public string Base64 { get; set; }

        [JsonProperty("photoId")]
        public int PhotoId { get; set; }
    }
}
