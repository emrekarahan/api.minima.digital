﻿using Newtonsoft.Json;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Products.Types
{
    public class ProductDetailTypeReturn
    {
        [JsonProperty("itemId")]
        public string ItemId { get; set; }

        [JsonProperty("productId")]
        public int ProductId { get; set; }

        [JsonProperty("product")]
        public ProductTypeReturn Product { get; set; }

        [JsonProperty("summary")]
        public ProductConcerningTypeReturn Summary { get; set; }

        [JsonProperty("ees")]
        public string Ees { get; set; }
    }
}
