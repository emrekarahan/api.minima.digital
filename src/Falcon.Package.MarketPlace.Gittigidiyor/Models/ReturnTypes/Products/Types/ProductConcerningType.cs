﻿using Newtonsoft.Json;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Products.Types
{
    
    public class ProductConcerningTypeReturn
    {
        [JsonProperty("soldCount")]
        public int SoldCount { get; set; }

        [JsonProperty("endDate")]
        public string EndDate { get; set; }

        [JsonProperty("listingStatus")]
        public string ListingStatus { get; set; }

        [JsonProperty("currentPrice")]
        public string CurrentPrice { get; set; }

        [JsonProperty("hasRelistOption")]
        public bool HasRelistOption { get; set; }

        [JsonProperty("bidCount")]
        public int BidCount { get; set; }
    }
}
