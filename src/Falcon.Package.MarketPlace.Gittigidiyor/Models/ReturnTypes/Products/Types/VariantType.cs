﻿namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Products.Types
{
    public class VariantTypeReturn
    {
        public VariantSpecTypeReturn[] VariantSpecs { get; set; }
        public int Quantity { get; set; }
        public string StockCode { get; set; }
        public int SoldCount { get; set; }
        public int NewCatalogId { get; set; }
        public long VariantId { get; set; }
        public string Operation { get; set; }
    }
}
