﻿using Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Products.Types;
using Newtonsoft.Json;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Products.Response
{
    public class ProductServiceResponseReturn : BaseResponseReturn
    {
        [JsonProperty("productId")]
        public int ProductId { get; set; }

        [JsonProperty("payRequired")]
        public bool PayRequired { get; set; }

        [JsonProperty("result")]
        public string Result { get; set; }

        [JsonProperty("descriptionFilterStatus")]
        public int DescriptionFilterStatus { get; set; }

        [JsonProperty("variantGroups")]
        public VariantGroupTypeReturn[] VariantGroups { get; set; }
    }
}
