﻿namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Products.Response
{
    public class ProductServicePriceResponseReturn : BaseResponseReturn
    {
        public string PaymentVoucher { get; set; }

        public double Price { get; set; }

        public string Message { get; set; }

        public bool PayRequired { get; set; }

        public int ProductCount { get; set; }
    }
}
