﻿using Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Products.Types;
using Newtonsoft.Json;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Products.Response
{

    public class ProductServiceListResponseReturn : BaseResponseReturn
    {
        [JsonProperty("productCount")]
        public long ProductCount { get; set; }

        [JsonProperty("products")]
        public ProductDetailTypeReturn[] Products { get; set; }
    }
}
