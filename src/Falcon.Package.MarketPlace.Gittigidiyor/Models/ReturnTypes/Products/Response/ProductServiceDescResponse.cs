﻿namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Products.Response
{
    public class ProductServiceDescResponseReturn : BaseResponseReturn
    {
        public int ProductId { get; set; }
        public string ItemId { get; set; }
        public string Description { get; set; }
    }
}
