﻿using Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Products.Types;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Products.Response
{
    public class ProductServiceSpecResponseReturn : BaseResponseReturn
    {
        public int ProductId { get; set; }
        public string ItemId { get; set; }
        public SpecTypeReturn[] Specs { get; set; }
    }
}
