﻿using Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Products.Types;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Products.Response
{
    public class ProductServiceDetailResponseReturn : BaseResponseReturn
    {
        public ProductDetailTypeReturn ProductDetail { get; set; }
    }
}
