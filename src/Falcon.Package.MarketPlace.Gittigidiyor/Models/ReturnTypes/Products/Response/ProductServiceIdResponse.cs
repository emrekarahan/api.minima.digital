﻿namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Products.Response
{
    public class ProductServiceIdResponseReturn : BaseResponseReturn
    {
        public long ProductCount { get; set; }
        public int?[] ProductIdList { get; set; }
        public string Voucher { get; set; }
        public double Price { get; set; }
        public string Result { get; set; }
    }
}
