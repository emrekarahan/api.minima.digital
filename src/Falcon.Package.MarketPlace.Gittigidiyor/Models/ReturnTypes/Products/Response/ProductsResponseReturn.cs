﻿namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Products.Response
{
    public class ProductsResponseReturn
    {
        public ProductServiceListResponseReturn Return;
        public ProductsResponseReturn()
        {
        }
        public ProductsResponseReturn(ProductServiceListResponseReturn @return)
        {
            Return = @return;
        }
    }
}
