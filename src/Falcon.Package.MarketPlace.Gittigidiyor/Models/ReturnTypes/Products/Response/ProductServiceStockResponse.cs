﻿using Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Products.Types;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Products.Response
{
    public class ProductServiceStockResponseReturn : BaseResponseReturn
    {
        public StockDetailTypeReturn[] Products { get; set; }
    }
}
