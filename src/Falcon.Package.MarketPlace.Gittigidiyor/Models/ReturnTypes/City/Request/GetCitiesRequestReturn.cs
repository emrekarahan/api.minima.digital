﻿using Falcon.Package.MarketPlace.Gittigidiyor.Models.Service;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.City.Request
{
    public class GetCitiesRequestReturn : BaseRequestReturn
    {
        public int StartOffSet { get; set; }

        public int RowCount { get; set; }
    }

}
