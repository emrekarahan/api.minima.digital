﻿using Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.City.Types;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.City.Response
{
    public class CityServiceResponseReturn : BaseResponseReturn
    {
        public int CityCount { get; set; }
        public CityTypeReturn[] Cities { get; set; }
    }
}