﻿namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.City.Types
{
    public class CityTypeReturn
    {
        public int TrCode { get; set; }
        public string CityName { get; set; }
    }
}