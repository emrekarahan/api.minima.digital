﻿namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Catalog.Request
{
    public class SearchCatalogRequestReturn
    {
        public int Page { get; set; }
        public int RowCount { get; set; }

        //public CatalogSearchCriteriaType Criteria { get; set; }
        public string Lang { get; set; }


        public class CatalogSearchCriteriaType
        {
            public string Keyword { get; set; }
            public string CategoryCode { get; set; }
            public SpecType[] SpecCriterias { get; set; }
            public string[] SpecFacetNames { get; set; }

            public class SpecType
            {
                public string Name { get; set; }

                public string Value { get; set; }

                public string Type { get; set; }

                public bool Required { get; set; }

                public bool RequiredSpecified { get; set; }
            }
        }

    }
}
