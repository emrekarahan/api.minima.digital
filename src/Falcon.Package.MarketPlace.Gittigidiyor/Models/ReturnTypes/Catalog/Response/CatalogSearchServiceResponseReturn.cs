﻿using Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Catalog.Types;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Catalog.Response
{
    public class CatalogSearchServiceResponseReturn : BaseResponseReturn
    {
        public int? TotalCount { get; set; }
        
        public SimpleCatalogTypeReturn[] Catalogs { get; set; }

        public SpecSearchResultTypeReturn[] SpecFacets { get; set; }
    }
}
