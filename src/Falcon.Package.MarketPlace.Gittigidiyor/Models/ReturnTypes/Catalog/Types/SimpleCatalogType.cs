﻿namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Catalog.Types
{
    public class SimpleCatalogTypeReturn
    {
        public int CatalogId { get; set; }
        public string CatalogName { get; set; }
        public CategoryTypeReturn Category { get; set; }
    }
}