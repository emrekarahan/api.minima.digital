﻿namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Catalog.Types
{
    public class ReturnCatalogSearchCriteriaTypeReturn
    {
        public string Keyword { get; set; }

        public string CategoryCode { get; set; }
        
        public SpecTypeReturn[] SpecCriterias { get; set; }

        public string[] SpecFacetNames { get; set; }
    }
}