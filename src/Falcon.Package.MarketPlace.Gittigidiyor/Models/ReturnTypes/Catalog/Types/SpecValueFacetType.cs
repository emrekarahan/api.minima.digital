﻿namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Catalog.Types
{
    public class SpecValueFacetTypeReturn
    {
        public string Value { get; set; }

        public long Count { get; set; }
    }
}
