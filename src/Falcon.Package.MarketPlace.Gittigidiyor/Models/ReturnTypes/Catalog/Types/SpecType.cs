﻿namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Catalog.Types
{
    public class SpecTypeReturn
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public string Type { get; set; }
        public bool Required { get; set; }
    }
}