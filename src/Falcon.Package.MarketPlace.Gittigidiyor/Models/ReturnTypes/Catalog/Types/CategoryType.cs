﻿namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Catalog.Types
{
    public class CategoryTypeReturn
    {
        public int CategoryAttributeId { get; set; }
        
        public string CategoryCode { get; set; }

        public string CategoryName { get; set; }

        public string CategoryPerma { get; set; }

        public bool Active { get; set; }

        public bool HasCatalog { get; set; }

        public bool HasSpec { get; set; }
        
        public int OrderNumber { get; set; }
        public int InstallmentNumber { get; set; }

    }
}