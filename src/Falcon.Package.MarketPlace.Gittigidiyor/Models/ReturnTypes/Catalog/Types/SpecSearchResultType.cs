﻿namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Catalog.Types
{
    public class SpecSearchResultTypeReturn
    {
        public SpecValueFacetTypeReturn[] Facets { get; set; }

        public string Name { get; set; }
    }
}
