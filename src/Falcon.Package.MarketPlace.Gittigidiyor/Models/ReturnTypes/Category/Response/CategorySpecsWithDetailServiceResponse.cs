﻿using Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Category.Types;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Category.Response
{
    public class CategorySpecsWithDetailServiceResponseReturn : BaseResponseReturn
    {
        public CategorySpecWithDetailType[] Specs { get; set; }
    }
}
