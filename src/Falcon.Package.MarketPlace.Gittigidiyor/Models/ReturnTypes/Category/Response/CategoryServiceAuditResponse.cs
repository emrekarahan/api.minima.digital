﻿using Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Category.Types;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Category.Response
{
    public class CategoryServiceAuditResponseReturn : BaseResponseReturn
    {
        public int TotalCount { get; set; }
        public int Count { get; set; }
        public CategoryAuditTypeReturn[] CategoryAudits { get; set; }
    }
}
