﻿namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Category.Response
{
    public class CategoriesResponseReturn
    {

        public CategoryServiceResponseReturn Return;

        public CategoriesResponseReturn()
        {
        }

        public CategoriesResponseReturn(CategoryServiceResponseReturn @return)
        {
            Return = @return;
        }
    }
}
