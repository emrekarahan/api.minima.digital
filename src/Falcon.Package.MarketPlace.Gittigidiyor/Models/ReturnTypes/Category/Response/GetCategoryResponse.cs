﻿namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Category.Response
{
    public class GetCategoryResponseReturn
    {

        public CategoryServiceResponseReturn Return;

        public GetCategoryResponseReturn()
        {
        }

        public GetCategoryResponseReturn(CategoryServiceResponseReturn @return)
        {
            Return = @return;
        }
    }
}
