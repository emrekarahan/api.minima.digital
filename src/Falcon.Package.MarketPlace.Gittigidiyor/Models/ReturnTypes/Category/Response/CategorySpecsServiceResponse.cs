﻿using Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Category.Types;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Category.Response
{
    public class CategorySpecsServiceResponseReturn : BaseResponseReturn
    {
        public CategorySpecTypeReturn[] Specs { get; set; }
    }
}
