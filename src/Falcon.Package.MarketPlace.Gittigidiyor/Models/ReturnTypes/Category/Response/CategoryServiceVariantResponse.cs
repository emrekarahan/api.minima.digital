﻿using Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Category.Types;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Category.Response
{
    public class CategoryServiceVariantResponseReturn : BaseResponseReturn
    {
        public long SpecCount { get; set; }

        public bool SpecCountSpecified { get; set; }

        public CategoryVariantSpecTypeReturn[] Specs { get; set; }
    }
}
