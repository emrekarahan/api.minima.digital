﻿namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Category.Response
{
    public class GetModifiedCategoriesResponseReturn
    {

        public CategoryServiceAuditResponseReturn Return;

        public GetModifiedCategoriesResponseReturn()
        {
        }

        public GetModifiedCategoriesResponseReturn(CategoryServiceAuditResponseReturn @return)
        {
            this.Return = @return;
        }
    }
}
