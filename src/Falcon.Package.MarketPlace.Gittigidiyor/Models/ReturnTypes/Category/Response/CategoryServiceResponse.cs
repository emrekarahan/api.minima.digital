﻿using Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Category.Types;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Category.Response
{
    public class CategoryServiceResponseReturn : BaseResponseReturn
    {
        public int CategoryCount { get; set; }
        public CategoryTypeReturn[] Categories { get; set; }
    }
}
