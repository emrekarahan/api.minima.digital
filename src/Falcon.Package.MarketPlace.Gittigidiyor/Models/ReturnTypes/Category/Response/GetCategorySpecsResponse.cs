﻿
namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Category.Response
{
    public class GetCategorySpecsResponseReturn
    {

        public CategorySpecsServiceResponseReturn Return;

        public GetCategorySpecsResponseReturn()
        {
        }

        public GetCategorySpecsResponseReturn(CategorySpecsServiceResponseReturn @return)
        {
            Return = @return;
        }
    }
}
