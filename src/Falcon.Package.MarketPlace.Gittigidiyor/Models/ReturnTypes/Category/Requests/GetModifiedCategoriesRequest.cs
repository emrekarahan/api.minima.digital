﻿using System;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Category.Requests
{
    public class GetModifiedCategoriesRequestReturn : BaseRequestReturn
    {
        public GetModifiedCategoriesRequestReturn()
        {
            
        }

        public DateTime StartDate { get; set; }
        public int StartOffset { get; set; }
        public int RowCount { get; set; }
        


    }

}
