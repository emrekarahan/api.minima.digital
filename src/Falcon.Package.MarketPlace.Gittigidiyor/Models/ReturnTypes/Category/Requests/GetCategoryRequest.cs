﻿namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Category.Requests
{
    public class GetCategoryRequestReturn : BaseRequestReturn
    {
        public GetCategoryRequestReturn()
        {
            WithSpecs = false;
            WithDeepest = false;
            WithCatalog = false;
        }
        public string CategoryCode { get; set; }
        public bool WithSpecs { get; set; }
        public bool WithDeepest { get; set; }
        public bool WithCatalog { get; set; }
    }
}
