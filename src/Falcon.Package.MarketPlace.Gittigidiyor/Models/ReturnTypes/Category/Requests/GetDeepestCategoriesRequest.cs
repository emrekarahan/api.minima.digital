﻿namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Category.Requests
{
    public class GetDeepestCategoriesRequestReturn : BaseRequestReturn
    {
        public GetDeepestCategoriesRequestReturn()
        {
            WithSpecs = false;
        }
        public int StartOffset { get; set; }
        public int RowCount { get; set; }
        public bool WithSpecs { get; set; }
    }
}
