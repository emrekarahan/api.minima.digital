﻿namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Category.Requests
{
    public class CategoryCodeRequestReturn : BaseRequestReturn
    {
        public string CategoryCode { get; set; }
    }
}
