﻿namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Category.Requests
{
    public class GetCategoriesByCodesRequestReturn
    {
        public GetCategoriesByCodesRequestReturn()
        {
            WithSpecs = false;
            WithDeepest = false;
            WithCatalog = false;
            Lang = "tr";
        }
        public string[] CategoryCodes { get; set; }
        public bool WithSpecs { get; set; }
        public bool WithDeepest { get; set; }
        public bool WithCatalog { get; set; }
        public string Lang { get; set; }

        
    }
}
