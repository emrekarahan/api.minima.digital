﻿namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Category.Requests
{
    public class GetCategoriesRequestReturn : BaseRequestReturn
    {
        public GetCategoriesRequestReturn()
        {
            WithSpecs = false;
            WithDeepest = false;
            WithCatalog = false;
        }
        public int StartOffset { get; set; }
        public int RowCount { get; set; }
        public bool WithSpecs { get; set; }
        public bool WithDeepest { get; set; }
        public bool WithCatalog { get; set; }
    }
}
