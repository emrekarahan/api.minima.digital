﻿namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Category.Requests
{
    public class GetParentCategoriesRequestReturn : BaseRequestReturn
    {
        public GetParentCategoriesRequestReturn()
        {
            WithSpecs = false;
            WithDeepest = false;
            WithCatalog = false;
        }
        public bool WithSpecs { get; set; }
        public bool WithDeepest { get; set; }
        public bool WithCatalog { get; set; }
    }
}
