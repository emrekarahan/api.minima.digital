﻿namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Category.Types
{
    public class CategorySpecTypeReturn
    {
        public string[] Values { get; set; }
        public string Name { get; set; }
        public bool Required { get; set; }
        public string Type { get; set; }
    }
}
