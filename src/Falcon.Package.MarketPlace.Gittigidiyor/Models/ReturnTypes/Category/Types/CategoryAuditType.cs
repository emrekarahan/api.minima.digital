﻿namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Category.Types
{
    public class CategoryAuditTypeReturn : BaseAuditTypeReturn
    {
        public string CategoryCode { get; set; }
    }
}
