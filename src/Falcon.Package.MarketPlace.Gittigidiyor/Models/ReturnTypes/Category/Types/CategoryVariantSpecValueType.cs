﻿namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Category.Types
{
    public class CategoryVariantSpecValueTypeReturn
    {
        public long ValueId { get; set; }
        public string Value { get; set; }
        public int OrderNumber { get; set; }
    }
}
