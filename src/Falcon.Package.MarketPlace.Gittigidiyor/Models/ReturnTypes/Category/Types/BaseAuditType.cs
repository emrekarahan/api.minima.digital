﻿namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Category.Types
{
    public abstract class BaseAuditTypeReturn
    {
        public string ChangeType { get; set; }
    }
}
