﻿namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Category.Types
{
    public class CategoryVariantSpecTypeReturn
    {
        public CategoryVariantSpecValueTypeReturn[] SpecValues { get; set; }
        public long NameId { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public int OrderNumber { get; set; }
        public bool @Base { get; set; }
        
    }
}
