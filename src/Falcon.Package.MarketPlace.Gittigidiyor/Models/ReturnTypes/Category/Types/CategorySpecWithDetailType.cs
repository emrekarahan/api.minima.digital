﻿namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Category.Types
{
    public class CategorySpecWithDetailTypeReturn
    {
        public CategorySpecValueTypeReturn[] Values { get; set; }
        public string Name { get; set; }
        public bool Required { get; set; }
        public string Type { get; set; }
        public int SpecId { get; set; }
        public int ChildSpecId { get; set; }

    }
}
