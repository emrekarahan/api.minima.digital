﻿namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Category.Types
{
    public class CategorySpecValueTypeReturn
    {
        public string Name { get; set; }
        public int SpecId { get; set; }
        public int ParentSpecId { get; set; }
    }
}
