﻿namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Category.Types
{
    public class CategoryTypeReturn
    {
        public string CategoryCode { get; set; }
        public string CategoryName { get; set; }
        public CategorySpecTypeReturn[] Specs { get; set; }
        public bool Deepest { get; set; }
        public bool HasCatalog { get; set; }
    }
}
