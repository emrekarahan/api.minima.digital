﻿namespace Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes
{
    public class BaseRequestReturn
    {
        public BaseRequestReturn()
        {
            Lang = "tr";
        }
        public string Lang { get; set; }
    }
}
