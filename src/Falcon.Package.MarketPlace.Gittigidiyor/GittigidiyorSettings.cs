﻿namespace Falcon.Package.MarketPlace.Gittigidiyor
{
    public class GittigidiyorSettings
    {
        public string ApiUrl { get; set; }
        public string RoleName { get; set; }
        public string RolePassword { get; set; }
        public string ApiKey { get; set; }
        public string ApiSecret { get; set; }
    }
}
