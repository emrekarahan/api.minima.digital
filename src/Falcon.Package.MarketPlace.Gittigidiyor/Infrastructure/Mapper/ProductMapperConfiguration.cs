﻿using AutoMapper;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Products.Requests;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Products.Response;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Products.Types;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Requests;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Responses;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Types;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Infrastructure.Mapper
{
    public class ProductMapperConfiguration : Profile
    {
        #region Properties

        /// <summary>
        ///     Order of this mapper implementation
        /// </summary>
        public int Order => 0;

        #endregion

        #region Ctor

        public ProductMapperConfiguration()
        {
            CreateProductMaps();
        }

        private void CreateProductMaps()
        {
            CreateMap<InsertAndActivateProductRequestReturn, InsertAndActivateProductRequest>()
                .ForAllOtherMembers(x => x.Ignore());
            //CreateMap<InsertAndActivateProductRequest, InsertAndActivateProductRequestReturn>();

            //CreateMap<CargoCompanyDetailType, CargoCompanyDetailTypeReturn>();
            CreateMap<CargoCompanyDetailTypeReturn, CargoCompanyDetailType>()
                .ForAllOtherMembers(x => x.Ignore());

            //CreateMap<CargoDetailType, CargoDetailTypeReturn>();
            CreateMap<CargoDetailTypeReturn, CargoDetailType>()
                .ForAllOtherMembers(x => x.Ignore());

            //CreateMap<CargoTimeType, CargoTimeTypeReturn>();
            CreateMap<CargoTimeTypeReturn, CargoTimeType>()
                .ForAllOtherMembers(x => x.Ignore());

            //CreateMap<PhotoType, PhotoTypeReturn>();
            CreateMap<PhotoTypeReturn, PhotoType>()
                .ForAllOtherMembers(x=> x.Ignore());

            //CreateMap<ProductConcerningType, ProductConcerningTypeReturn>();
            CreateMap<ProductConcerningTypeReturn, ProductConcerningType>()
                .ForAllOtherMembers(x => x.Ignore());

            //CreateMap<ProductDetailType, ProductDetailTypeReturn>();
            CreateMap<ProductDetailTypeReturn, ProductDetailType>()
                .ForAllOtherMembers(x => x.Ignore());

            //CreateMap<ProductPackageSizeType, ProductPackageSizeTypeReturn>();
            CreateMap<ProductPackageSizeTypeReturn, ProductPackageSizeType>()
                .ForAllOtherMembers(x => x.Ignore());

            //CreateMap<ProductType, ProductTypeReturn>();
            CreateMap<ProductTypeReturn, ProductType>()
                .ForAllOtherMembers(x => x.Ignore());

            //CreateMap<SpecType, SpecTypeReturn>();
            CreateMap<SpecTypeReturn, SpecType>()
                .ForAllOtherMembers(x => x.Ignore());

            //CreateMap<SameDayDeliveryType, SameDayDeliveryTypeReturn>();
            CreateMap<SameDayDeliveryTypeReturn, SameDayDeliveryType>()
                .ForAllOtherMembers(x => x.Ignore());

            //CreateMap<VariantGroupType, VariantGroupTypeReturn>();
            CreateMap<VariantGroupTypeReturn, VariantGroupType>()
                .ForAllOtherMembers(x => x.Ignore());

            //CreateMap<VariantSpecType, VariantSpecTypeReturn>();
            CreateMap<VariantSpecTypeReturn, VariantSpecType>()
                .ForAllOtherMembers(x => x.Ignore());

            //CreateMap<VariantType, VariantTypeReturn>();
            CreateMap<VariantTypeReturn, VariantType>()
                .ForAllOtherMembers(x => x.Ignore());

            //CreateMap<StockDetailType, StockDetailTypeReturn>();
            CreateMap<StockDetailTypeReturn, StockDetailType>()
                .ForAllOtherMembers(x => x.Ignore());

            CreateMap<UpdateProductWithNewCargoDetailRequestReturn, UpdateProductWithNewCargoDetailRequest>()
                .ForAllOtherMembers(x => x.Ignore());
            //CreateMap<UpdateProductWithNewCargoDetailRequest, UpdateProductWithNewCargoDetailRequestReturn>();

            CreateMap<ProductServiceListResponse, ProductServiceListResponseReturn>();
            CreateMap<ProductServiceListResponseReturn, ProductServiceListResponse>();

            CreateMap<InsertProductWithNewCargoDetailRequestReturn, InsertProductWithNewCargoDetailRequest>();
            CreateMap<InsertProductWithNewCargoDetailRequest, InsertProductWithNewCargoDetailRequestReturn>();

            CreateMap<DeleteProductRequestReturn, DeleteProductRequest>();
            CreateMap<DeleteProductRequest, DeleteProductRequestReturn>();

            CreateMap<GetNewlyListedProductIdListRequestReturn, GetNewlyListedProductIdListRequest>();
            CreateMap<GetNewlyListedProductIdListRequest, GetNewlyListedProductIdListRequestReturn>();

            CreateMap<ProductServicePriceResponse, ProductServicePriceResponseReturn>();
            CreateMap<ProductServicePriceResponseReturn, ProductServicePriceResponse>();
            
            CreateMap<ProductServiceStockResponse, ProductServiceStockResponseReturn>();
            CreateMap<ProductServiceStockResponseReturn, ProductServiceStockResponse>();

            CreateMap<ProductServiceDescResponse, ProductServiceDescResponseReturn>();
            CreateMap<ProductServiceDescResponseReturn, ProductServiceDescResponse>();

            CreateMap<GetProductsResponse, ProductsResponseReturn>();
            CreateMap<ProductServiceResponse, ProductServiceResponseReturn>();
            CreateMap<ProductServiceIdResponse, ProductServiceIdResponseReturn>();
            CreateMap<ProductServiceDetailResponse, ProductServiceDetailResponseReturn>();
            
            CreateMap<ProductServiceSpecResponse, ProductServiceSpecResponseReturn>();
        }


        #endregion

    }
}