﻿using AutoMapper;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Sale.Responses;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Sale.Types;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Sale.Responses;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Sale.Types;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Infrastructure.Mapper
{
    public class SaleMapperConfiguration : Profile
    {
        #region Properties

        /// <summary>
        ///     Order of this mapper implementation
        /// </summary>
        public int Order => 0;

        #endregion

        #region Ctor

        public SaleMapperConfiguration()
        {
            CreateSaleMaps();
        }

        private void CreateSaleMaps()
        {
            CreateMap<AppliesToReturn, AppliesTo>();
            CreateMap<AppliesTo, AppliesToReturn>();

            CreateMap<GiftPromotionTypeReturn, GiftPromotionType>();
            CreateMap<GiftPromotionType, GiftPromotionTypeReturn>();

            CreateMap<SellerPromotionStatusTypeReturn, SellerPromotionStatusType>();
            CreateMap<SellerPromotionStatusType, SellerPromotionStatusTypeReturn>();

            CreateMap<SellerPromotionTypeReturn, SellerPromotionType>();
            CreateMap<SellerPromotionType, SellerPromotionTypeReturn>();

            CreateMap<ShippingPaymentTypeReturn, ShippingPaymentType>();
            CreateMap<ShippingPaymentType, ShippingPaymentTypeReturn>();

            CreateMap<TypeReturn, Type>();
            CreateMap<Type, TypeReturn>();

            CreateMap<GiftInfoTypeReturn, GiftInfoType>();
            CreateMap<GiftInfoType, GiftInfoTypeReturn>();

            CreateMap<GiftProductInfoDtoReturn, GiftProductInfoDto>();
            CreateMap<GiftProductInfoDto, GiftProductInfoDtoReturn>();
            
            CreateMap<ImageDtoReturn, ImageDto>();
            CreateMap<ImageDto, ImageDtoReturn>();

            CreateMap<ImageUrlDtoReturn, ImageUrlDto>();
            CreateMap<ImageUrlDto, ImageUrlDtoReturn>();

            CreateMap<ItemVariantSpecTypeReturn, ItemVariantSpecType>();
            CreateMap<ItemVariantSpecType, ItemVariantSpecTypeReturn>();

            CreateMap<MainProductInfoDtoReturn, MainProductInfoDto>();
            CreateMap<MainProductInfoDto, MainProductInfoDtoReturn>();

            CreateMap<NeighborhoodTypeReturn, NeighborhoodType>();
            CreateMap<NeighborhoodType, NeighborhoodTypeReturn>();

            CreateMap<OrderBuyerInfoTypeReturn, OrderBuyerInfoType>();
            CreateMap<OrderBuyerInfoType, OrderBuyerInfoTypeReturn>();

            CreateMap<ProductCatalogDtoReturn, ProductCatalogDto>();
            CreateMap<ProductCatalogDto, ProductCatalogDtoReturn>();

            CreateMap<SaleInvoiceTypeReturn, SaleInvoiceTypeReturn>();
            CreateMap<SaleInvoiceType, SaleInvoiceTypeReturn>();

            CreateMap<SaleSellerPromotionTypeReturn, SaleSellerPromotionType>();
            CreateMap<SaleSellerPromotionType, SaleSellerPromotionTypeReturn>();

            CreateMap<SaleType, SaleTypeReturn>();
            CreateMap<SaleTypeReturn, SaleType>();

            CreateMap<SellerPromotionInfoTypeReturn, SellerPromotionInfoType>();
            CreateMap<SellerPromotionInfoType, SellerPromotionInfoTypeReturn>();

            CreateMap<SellerPromotionItemReturn, SellerPromotionItem>();
            CreateMap<SellerPromotionItem, SellerPromotionItemReturn>();

            CreateMap<SellerPromotionReturn, SellerPromotion>();
            CreateMap<SellerPromotion, SellerPromotionReturn>();

            CreateMap<ShippingInfoTypeReturn, ShippingInfoType>();
            CreateMap<ShippingInfoType, ShippingInfoTypeReturn>();
            
            CreateMap<VariantDtoReturn, VariantDto>();
            CreateMap<VariantDto, VariantDtoReturn>();

            CreateMap<VariantSpecDtoReturn, VariantSpecDto>();
            CreateMap<VariantSpecDto, VariantSpecDtoReturn>();

            CreateMap<SaleServiceResponseReturn, SaleServiceResponse>();
            CreateMap<SaleServiceResponse, SaleServiceResponseReturn>();

            CreateMap<SaleServicePagingResponse, SaleServicePagingResponseReturn>();
            CreateMap<SaleServicePagingResponseReturn, SaleServicePagingResponse>();
        }


        #endregion

    }
}