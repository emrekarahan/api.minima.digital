﻿using AutoMapper;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Catalog.Request;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Catalog.Response;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Catalog.Types;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Catalog.Requests;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Catalog.Responses;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Catalog.Types;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Infrastructure.Mapper
{
    public class CatalogMapperConfiguration : Profile
    {
        #region Ctor

        public CatalogMapperConfiguration()
        {

            CreateMap<categoryType, CategoryTypeReturn>();
            CreateMap<simpleCatalogType, SimpleCatalogTypeReturn>();
            CreateMap<specSearchResultType, SpecSearchResultTypeReturn>();
            CreateMap<specValueFacetType, SpecValueFacetTypeReturn>();
            CreateMap<specType, SpecTypeReturn>();
            CreateMap<catalogSearchCriteriaType, ReturnCatalogSearchCriteriaTypeReturn>();

            CreateMap<CatalogSearchServiceResponse, CatalogSearchServiceResponseReturn>();
                

            CreateMap<SpecTypeReturn, specType>();
            CreateMap<specType, SpecTypeReturn>();

            CreateMap<ReturnCatalogSearchCriteriaTypeReturn, catalogSearchCriteriaType>();
            CreateMap<catalogSearchCriteriaType, ReturnCatalogSearchCriteriaTypeReturn>();

            CreateMap<SearchCatalogRequestReturn, SearchCatalogRequest>();
            CreateMap<SearchCatalogRequest, SearchCatalogRequestReturn>();

        }


        #endregion

    }
}
