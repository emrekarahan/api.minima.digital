﻿using AutoMapper;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Category.Response;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Category.Types;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Category.Responses;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Category.Types;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Infrastructure.Mapper
{
    public class CategoryMapperConfiguration : Profile
    {
        #region Properties

        /// <summary>
        ///     Order of this mapper implementation
        /// </summary>
        public int Order => 0;

        #endregion

        #region Ctor

        public CategoryMapperConfiguration()
        {
            CreateCategoryMaps();
            
        }
        
        private void CreateCategoryMaps()
        {
            CreateMap<CategoryType, CategoryTypeReturn>();
            CreateMap<CategorySpecType, CategorySpecTypeReturn>();
            CreateMap<CategoryAuditType, CategoryAuditTypeReturn>();
            CreateMap<CategoryVariantSpecType, CategoryVariantSpecTypeReturn>();
            CreateMap<CategoryVariantSpecValueType, CategoryVariantSpecValueTypeReturn>();
            CreateMap<CategorySpecValueType, CategorySpecValueTypeReturn>();
            CreateMap<CategorySpecWithDetailType, CategorySpecWithDetailTypeReturn>();

            CreateMap<CategoryServiceResponse, CategoryServiceResponseReturn>();
            CreateMap<CategoryServiceAuditResponse, CategoryServiceAuditResponseReturn>();
            CreateMap<GetCategoriesResponse, CategoriesResponseReturn>();
            CreateMap<GetCategoryResponse, GetCategoryResponseReturn>();
            CreateMap<GetModifiedCategoriesResponse, GetModifiedCategoriesResponseReturn>();
            CreateMap<CategorySpecsServiceResponse, CategorySpecsServiceResponseReturn>();
            CreateMap<GetCategorySpecsResponse, GetCategorySpecsResponseReturn>();
            CreateMap<CategoryServiceVariantResponse, CategoryServiceVariantResponseReturn>();
            CreateMap<CategorySpecsWithDetailServiceResponse, CategorySpecsWithDetailServiceResponseReturn>();
        }
        

        #endregion
    }
}