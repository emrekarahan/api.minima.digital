﻿using AutoMapper;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Category.Response;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Category.Types;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.City.Response;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.City.Types;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.Service;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Category.Responses;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Category.Types;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.City.Responses;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.City.Types;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Infrastructure.Mapper
{
    public class GittigidiyorMapperConfiguration : Profile
    {
        #region Properties

        /// <summary>
        ///     Order of this mapper implementation
        /// </summary>
        public int Order => 0;

        #endregion

        #region Ctor

        public GittigidiyorMapperConfiguration()
        {
            CreateCoreMaps();
            CreateCategoryMaps();
            CreateCityMaps();
        }

        private void CreateCityMaps()
        {
            CreateMap<CityServiceResponse, CityServiceResponseReturn>();
            CreateMap<CityServiceResponseReturn, CityServiceResponse>();

            CreateMap<CityType, CityTypeReturn>();
            CreateMap<CityTypeReturn, CityType>();
        }


        private void CreateCategoryMaps()
        {
            CreateMap<CategoryType, CategoryTypeReturn>();
            CreateMap<CategorySpecType, CategorySpecTypeReturn>();
            CreateMap<CategoryAuditType, CategoryAuditTypeReturn>();
            CreateMap<CategoryVariantSpecType, CategoryVariantSpecTypeReturn>();
            CreateMap<CategoryVariantSpecValueType, CategoryVariantSpecValueTypeReturn>();
            CreateMap<CategorySpecValueType, CategorySpecValueTypeReturn>();
            CreateMap<CategorySpecWithDetailType, CategorySpecWithDetailTypeReturn>();

            CreateMap<CategoryServiceResponse, CategoryServiceResponseReturn>();
            CreateMap<CategoryServiceAuditResponse, CategoryServiceAuditResponseReturn>();
            CreateMap<GetCategoriesResponse, CategoriesResponseReturn>();
            CreateMap<GetCategoryResponse, GetCategoryResponseReturn>();
            CreateMap<GetModifiedCategoriesResponse, GetModifiedCategoriesResponseReturn>();
            CreateMap<CategorySpecsServiceResponse, CategorySpecsServiceResponseReturn>();
            CreateMap<GetCategorySpecsResponse, GetCategorySpecsResponseReturn>();
            CreateMap<CategoryServiceVariantResponse, CategoryServiceVariantResponseReturn>();
            CreateMap<CategorySpecsWithDetailServiceResponse, CategorySpecsWithDetailServiceResponseReturn>();
        }
        
        private void CreateCoreMaps()
        {
            CreateMap<BaseResponseService, BaseResponseReturn>();
            CreateMap<ErrorTypeService, ErrorTypeReturn>();
        }

        #endregion
    }
}