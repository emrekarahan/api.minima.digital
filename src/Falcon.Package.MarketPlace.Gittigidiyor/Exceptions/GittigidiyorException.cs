﻿using System;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Exceptions
{
    public class ElementNamespaceNotFoundException : Exception
    {
        public ElementNamespaceNotFoundException(string message) : base(message)
        {

        }
    }

    public class ResponseTypeNotFoundException : Exception
    {
        public ResponseTypeNotFoundException(string message) : base(message)
        {

        }
    }

    public class UrlFragmentNotFoundException : Exception
    {
        public UrlFragmentNotFoundException(string message) : base(message)
        {

        }
    }
}
