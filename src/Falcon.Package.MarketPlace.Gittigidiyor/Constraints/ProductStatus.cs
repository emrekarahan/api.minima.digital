﻿namespace Falcon.Package.MarketPlace.Gittigidiyor.Constraints
{
    /// <summary>
    /// A: Aktif Satışlar
    /// L: Listelemeye Hazır Ürünler
    /// S: Satılanlar
    /// U: Satılmayanlar
    /// </summary>
    /// 
    public class ProductStatus
    {
        public const string ACTIVE_PRODUCTS = "A";
        public const string READY_FOR_LISTING = "L";
        public const string SOLD = "S";
        public const string UNSOLD = "U";
    }



}
