﻿using Falcon.Package.MarketPlace.Gittigidiyor.Builder;
using Falcon.Package.MarketPlace.Gittigidiyor.Builder.Impl;
using Falcon.Package.MarketPlace.Gittigidiyor.Services;
using Falcon.Package.MarketPlace.Gittigidiyor.Services.Impl;
using AutoMapper;
using Falcon.Package.MarketPlace.Gittigidiyor.Infrastructure.Mapper;
using Microsoft.Extensions.DependencyInjection;

namespace Falcon.Package.MarketPlace.Gittigidiyor
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddGittigidiyor(this IServiceCollection services, GittigidiyorSettings settings)
        {
            services.AddAutoMapper(typeof(GittigidiyorMapperConfiguration));

            //services.AddAutoMapper(o =>
            //{
            //    o.AddProfile(typeof(GittigidiyorMapperConfiguration));
            //    o.AddProfile(typeof(CategoryMapperConfiguration));
            //    o.AddProfile(typeof(ProductMapperConfiguration));
            //    o.AddProfile(typeof(CatalogMapperConfiguration));
            //} );

            services.AddSingleton(settings);
            services.AddScoped<IGittigidiyorService, GittigidiyorService>();
            services.AddScoped<ICategoryApiService, CategoryApiService>();
            services.AddScoped<ICatalogApiService, CatalogApiService>();
            services.AddScoped<IProductApiService, ProductApiService>();
            services.AddScoped<ICityApiService, CityApiService>();
            services.AddScoped<ISalesApiService, SalesApiService>();
            return services;
        }
    }
}
