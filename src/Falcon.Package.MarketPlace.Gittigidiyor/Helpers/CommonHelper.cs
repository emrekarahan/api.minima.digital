﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Helpers
{
    public class CommonHelper
    {
        public static long GetUnixEpoch(DateTime value)
        {
            TimeSpan t = value - new DateTime(1970, 1, 1);
            int secondsSinceEpoch = (int)t.TotalSeconds;
            return secondsSinceEpoch;
        }

        public static string GetMd5Hash(string input)
        {
            //Md5CryptoServiceProvider nesnesinin yeni bir örneğini oluşturuyoruz..
            MD5 md5Hasher = MD5.Create();
            //karışık diziyi byte baz alarak convert ediyoruz..
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));
            //byte toplamak için yeni bir Stringbuilder oluşturalım..
            StringBuilder sBuilder = new StringBuilder();
            //Her byte 'ı hexedecimal olarak hash veris oluşturan döngümüz..
            foreach (var t in data)
            {
                sBuilder.Append(t.ToString("x2"));
            }
            //hexedecimal değere dönüştürüyoruz..
            return sBuilder.ToString();
        }

        public static string getSignature(string apiKey, string secretKey, long time)
        {
            string result = null;
            try
            {
                result = getMd5Hash(apiKey + secretKey + time);
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.StackTrace);
            }
            return result;
        }

        public static string getMd5Hash(string input)
        {
            byte[] array = MD5.Create().ComputeHash(Encoding.Default.GetBytes(input));
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < array.Length; i++)
            {
                stringBuilder.Append(array[i].ToString("x2"));
            }
            return stringBuilder.ToString();
        }
        public static long convertToTimestamp(DateTime value)
        {
            return (value - new DateTime(1970, 1, 1)).Ticks / 10000L;
        }
    }
}
