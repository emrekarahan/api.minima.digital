﻿using Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Sale.Requests;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Sale.Responses;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Services
{
    public interface ISalesApiService
    {
        SaleServicePagingResponseReturn GetPagedSales(GetPagedSalesRequestReturn request);
        SaleServiceResponseReturn GetSale(GetSaleRequestReturn request);
        SaleServicePagingResponseReturn GetSalesByDateRange(GetSalesByDateRangeRequestReturn request);
        void GiveRateAndComment();
        void ReplySaleComment();
        void RemindForApproval();
        void GetReasonsToCancelSales();
        void CancelSale();
        void GetAccountTransactionsV2();
        void GetSaleProcessReport();
        void RemoveSaleFromList();
        void GetItemBuyers();
        void ReceiveRemandedItem();
        void GiveApprovalForRemandedItem();
        void CancelSaleAfterEarlyCancellationRequest();
    }
}