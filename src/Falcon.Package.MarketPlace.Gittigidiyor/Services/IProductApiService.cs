﻿using Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Products.Requests;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Products.Response;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Services
{
    public interface IProductApiService
    {
        ProductServiceResponseReturn InsertAndActivateProduct(InsertAndActivateProductRequestReturn request);
        ProductServiceResponseReturn InsertProductWithNewCargoDetail(InsertProductWithNewCargoDetailRequestReturn request);
        ProductServiceResponseReturn CloneProduct(CloneProductRequestReturn request);
        ProductServiceIdResponseReturn DeleteProduct(DeleteProductRequestReturn request);
        ProductServiceIdResponseReturn GetNewlyListedProductIdList(GetNewlyListedProductIdListRequestReturn request);
        ProductServicePriceResponseReturn CalculatePriceForShoppingCart(CalculatePriceForShoppingCartRequestReturn request);
        ProductServiceIdResponseReturn FinishEarly(FinishEarlyRequestReturn request);
        ProductServiceDetailResponseReturn GetProduct(GetProductRequestReturn request);
        ProductServiceListResponseReturn GetProducts(GetProductsRequestReturn request);
        ProductServiceResponseReturn UpdatePrice(UpdatePriceRequestReturn request);
        ProductServiceResponseReturn UpdateStock(UpdateStockRequestReturn request);
        ProductServiceResponseReturn UpdateVariantStock(UpdateVariantStockRequestReturn request);
        ProductServiceIdResponseReturn RelistProducts(RelistProductsRequestReturn request);
        ProductServiceStockResponseReturn GetStockAndPrice(GetStockAndPriceRequestReturn request);
        ProductServiceDescResponseReturn GetProductDescription(GetProductDescriptionRequestReturn request);
        ProductServiceSpecResponseReturn GetProductSpecs(GetProductSpecsRequestReturn request);
        ProductServiceResponseReturn UpdatePriceByPercentage(UpdatePriceByPercentageRequestReturn request);
        ProductServiceResponseReturn UpdateProductWithNewCargoDetail(UpdateProductWithNewCargoDetailRequestReturn request);
        ProductServiceListResponseReturn GetProductsByIds(GetProductsByIdsRequestReturn request);
        ProductServiceIdResponseReturn GetProductIds(GetProductIdsRequestReturn request);
        void CheckForItemId();
        void GetProductStatuses();
        void UpdateItemId();
        void UpdateProductVariants();
        void GetProductVariants();
        void UpdateStockAndActivateProduct();
        void UpdateMarketPrice();
        void UpdateVariantStockAndActivateProduct();
        void GetItemIdDetails();
        void GpdateProductSales();
    }
}
