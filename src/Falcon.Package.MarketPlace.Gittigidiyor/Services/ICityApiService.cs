﻿using Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.City.Request;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.City.Response;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Services
{
    public interface ICityApiService
    {
        CityServiceResponseReturn GetCities(GetCitiesRequestReturn request);
    }
}
