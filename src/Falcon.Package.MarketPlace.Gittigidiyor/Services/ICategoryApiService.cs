﻿using Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Category.Requests;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Category.Response;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Services
{
    public interface ICategoryApiService
    {
        CategoryServiceResponseReturn GetCategories(GetCategoriesRequestReturn request);

        CategoryServiceAuditResponseReturn GetModifiedCategories(GetModifiedCategoriesRequestReturn request);

        CategoryServiceResponseReturn GetCategory(GetCategoryRequestReturn request);
        
        CategorySpecsServiceResponseReturn GetCategorySpecs(CategoryCodeRequestReturn request);

        CategoryServiceResponseReturn GetDeepestCategories(GetDeepestCategoriesRequestReturn request);

        CategoryServiceResponseReturn GetCategoriesByCodes(GetCategoriesByCodesRequestReturn request);

        CategoryServiceVariantResponseReturn GetCategoryVariantSpecs(CategoryCodeRequestReturn request);

        CategorySpecsWithDetailServiceResponseReturn GetCategorySpecsWithDetail(CategoryCodeRequestReturn request);

        CategoryServiceResponseReturn GetCategoriesHavingVariantSpecs(BaseRequestReturn request);

        CategoryServiceResponseReturn GetParentCategories(GetParentCategoriesRequestReturn request);

        CategoryServiceResponseReturn GetSubCategories(GetSubCategoriesRequestReturn request);
    }
}
