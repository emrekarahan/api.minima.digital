﻿using AutoMapper;
using Falcon.Package.MarketPlace.Gittigidiyor.Builder;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Products.Requests;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Products.Response;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Requests;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Product.Responses;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Services.Impl
{
    public class ProductApiService : IProductApiService
    {
        private readonly IGittigidiyorService _serviceBuilder;
        private readonly IMapper _mapper;

        public ProductApiService(IGittigidiyorService serviceBuilder,
            IMapper mapper)
        {
            _serviceBuilder = serviceBuilder;
            _mapper = mapper;
        }

        public ProductServiceResponseReturn InsertAndActivateProduct(InsertAndActivateProductRequestReturn request)
        {
            var insertRequest = _mapper.Map<InsertAndActivateProductRequestReturn, InsertAndActivateProductRequest>(request);

            var result = _serviceBuilder.Build<InsertAndActivateProductResponse, InsertAndActivateProductRequest>(
                new InsertAndActivateProductRequest(insertRequest.ItemId, insertRequest.Product,
                    insertRequest.ForceToSpecEntry, insertRequest.NextDateOption, insertRequest.Lang
                ));
            var mapped =
                _mapper.Map<ProductServiceResponse, ProductServiceResponseReturn>(result.ProductServiceResponse);
            return mapped;
        }
        public ProductServiceResponseReturn InsertProductWithNewCargoDetail(InsertProductWithNewCargoDetailRequestReturn request)
        {
            var mappedRequest =
                _mapper.Map<InsertProductWithNewCargoDetailRequestReturn, InsertProductWithNewCargoDetailRequest>(
                    request);

            var result =
                _serviceBuilder.Build<InsertProductWithNewCargoDetailResponse, InsertProductWithNewCargoDetailRequest>(
                    mappedRequest);

            var mapped =
                _mapper.Map<ProductServiceResponse, ProductServiceResponseReturn>(result.ProductServiceResponse);

            return mapped;
        }
        public ProductServiceResponseReturn CloneProduct(CloneProductRequestReturn request)
        {
            var result =
                _serviceBuilder.Build<CloneProductResponse, CloneProductRequest>(
                    new CloneProductRequest(request.ProductId, request.ItemId, request.Lang));
            var mapped = _mapper.Map<ProductServiceResponse, ProductServiceResponseReturn>(result.ProductServiceResponse);
            return mapped;
        }
        public ProductServiceIdResponseReturn DeleteProduct(DeleteProductRequestReturn request)
        {
            var mappedRequest = _mapper.Map<DeleteProductRequestReturn, DeleteProductRequest>(request);
            var result =
                _serviceBuilder.Build<DeleteProductResponse, DeleteProductRequest>(mappedRequest);
            var mapped =
                _mapper.Map<ProductServiceIdResponse, ProductServiceIdResponseReturn>(result.ProductServiceIdResponse);
            return mapped;
        }
        public ProductServiceIdResponseReturn GetNewlyListedProductIdList(GetNewlyListedProductIdListRequestReturn request)
        {
            var mappedRequest =
                _mapper
                    .Map<GetNewlyListedProductIdListRequestReturn, GetNewlyListedProductIdListRequest>(request);

            var result =
                _serviceBuilder.Build<GetNewlyListedProductIdListResponse, GetNewlyListedProductIdListRequest>(
                    mappedRequest);

            var mapped = _mapper.Map<ProductServiceIdResponse, ProductServiceIdResponseReturn>(result.ProductServiceIdResponse);
            return mapped;
        }
        public ProductServicePriceResponseReturn CalculatePriceForShoppingCart(CalculatePriceForShoppingCartRequestReturn request)
        {
            var result =
                _serviceBuilder.Build<CalculatePriceForShoppingCartResponse, CalculatePriceForShoppingCartRequest>(
                    new CalculatePriceForShoppingCartRequest(request.ProductIdList, request.ItemIdList, request.Lang));

            var mapped = _mapper.Map<ProductServicePriceResponse, ProductServicePriceResponseReturn>(result.ProductServicePriceResponse);
            return mapped;
        }
        public ProductServiceIdResponseReturn FinishEarly(FinishEarlyRequestReturn request)
        {
            var result =
                _serviceBuilder.Build<FinishEarlyResponse, FinishEarlyRequest>(
                    new FinishEarlyRequest(request.ProductIdArray, request.ItemIdArray, request.Lang));
            var mapped = _mapper.Map<ProductServiceIdResponse, ProductServiceIdResponseReturn>(result.ProductServiceIdResponse);
            return mapped;
        }
        public ProductServiceDetailResponseReturn GetProduct(GetProductRequestReturn request)
        {
            var result =
                _serviceBuilder.Build<GetProductResponse, GetProductRequest>(
                    new GetProductRequest(request.ProductId, request.ItemId, request.Lang));
            var mapped = _mapper.Map<ProductServiceDetailResponse, ProductServiceDetailResponseReturn>(result.ProductServiceDetailResponse);
            return mapped;
        }
        public ProductServiceListResponseReturn GetProducts(GetProductsRequestReturn request)
        {
            var result = _serviceBuilder.Build<GetProductsResponse, GetProductsRequest>(
                new GetProductsRequest(request.StartOffset, request.RowCount, request.Status, request.WithData,
                    request.Lang));

            var mapped =
                _mapper.Map<ProductServiceListResponse, ProductServiceListResponseReturn>(
                    result.ProductServiceListResponse);
            return mapped;
        }
        public ProductServiceResponseReturn UpdatePrice(UpdatePriceRequestReturn request)
        {
            var result =
                _serviceBuilder.Build<UpdatePriceResponse, UpdatePriceRequest>(
                    new UpdatePriceRequest(request.ProductId, request.ItemId, request.Price, request.CancelBid,
                        request.Lang));
            var mapped = _mapper.Map<ProductServiceResponse, ProductServiceResponseReturn>(result.ProductServiceResponse);
            return mapped;
        }
        public ProductServiceResponseReturn UpdateStock(UpdateStockRequestReturn request)
        {
            var result = _serviceBuilder.Build<UpdateStockResponse, UpdateStockRequest>(new UpdateStockRequest(
                request.ProductId, request.ItemId, request.Stock, request.CancelBid, request.Lang));
            var mapped = _mapper.Map<ProductServiceResponse, ProductServiceResponseReturn>(result.@return);
            return mapped;
        }
        public ProductServiceResponseReturn UpdateVariantStock(UpdateVariantStockRequestReturn request)
        {
            var result =
                _serviceBuilder.Build<UpdateVariantStockResponse, UpdateVariantStockRequest>(
                    new UpdateVariantStockRequest(request.ProductId, request.ItemId, request.VariantId, request.Stock,
                        request.Lang));
            var mapped = _mapper.Map<ProductServiceResponse, ProductServiceResponseReturn>(result.ProductServiceResponse);
            return mapped;
        }
        public ProductServiceIdResponseReturn RelistProducts(RelistProductsRequestReturn request)
        {
            var result =
                _serviceBuilder.Build<RelistProductsResponse, RelistProductsRequest>(
                    new RelistProductsRequest(request.ProductIdList, request.ItemIdList, request.Lang));
            var mapped = _mapper.Map<ProductServiceIdResponse, ProductServiceIdResponseReturn>(result.ProductServiceIdResponse);
            return mapped;
        }
        public ProductServiceStockResponseReturn GetStockAndPrice(GetStockAndPriceRequestReturn request)
        {
            var result =
                _serviceBuilder.Build<GetStockAndPriceResponse, GetStockAndPriceRequest>(
                    new GetStockAndPriceRequest(request.ProductIdList, request.ItemIdList, request.Lang));
            var mapped = _mapper.Map<ProductServiceStockResponse, ProductServiceStockResponseReturn>(result.ProductServiceStockResponse);
            return mapped;
        }
        public ProductServiceDescResponseReturn GetProductDescription(GetProductDescriptionRequestReturn request)
        {
            var result =
                _serviceBuilder.Build<GetProductDescriptionResponse, GetProductDescriptionRequest>(
                    new GetProductDescriptionRequest(request.ProductId, request.ItemId, request.Lang));
            var mapped = _mapper.Map<ProductServiceDescResponse, ProductServiceDescResponseReturn>(result.ProductServiceDescResponse);
            return mapped;
        }
        public ProductServiceSpecResponseReturn GetProductSpecs(GetProductSpecsRequestReturn request)
        {
            var result =
                _serviceBuilder.Build<GetProductSpecsResponse, GetProductSpecsRequest>(
                    new GetProductSpecsRequest(request.ProductId, request.ItemId, request.Lang));
            var mapped =
                _mapper.Map<ProductServiceSpecResponse, ProductServiceSpecResponseReturn>(
                    result.ProductServiceSpecResponse);
            return mapped;
        }
        public ProductServiceResponseReturn UpdatePriceByPercentage(UpdatePriceByPercentageRequestReturn request)
        {
            var result =
                _serviceBuilder.Build<UpdatePriceByPercentageResponse, UpdatePriceByPercentageRequest>(
                    new UpdatePriceByPercentageRequest(request.ProductId, request.ItemId, request.OperatorType,
                        request.Percentage, request.Lang));

            var mapped =
                _mapper.Map<ProductServiceResponse, ProductServiceResponseReturn>(result.ProductServiceResponse);
            return mapped;
        }
        public ProductServiceResponseReturn UpdateProductWithNewCargoDetail(
            UpdateProductWithNewCargoDetailRequestReturn request)
        {
            var mappedRequest =
                _mapper.Map<UpdateProductWithNewCargoDetailRequestReturn, UpdateProductWithNewCargoDetailRequest>(request);
            var result =
                _serviceBuilder.Build<UpdateProductWithNewCargoDetailResponse, UpdateProductWithNewCargoDetailRequest>(
                    mappedRequest);
            var mappedResult =
                _mapper.Map<ProductServiceResponse, ProductServiceResponseReturn>(result.ProductServiceResponse);
            return mappedResult;
        }
        public ProductServiceListResponseReturn GetProductsByIds(GetProductsByIdsRequestReturn request)
        {
            var result =
                _serviceBuilder.Build<GetProductsByIdsResponse, GetProductsByIdsRequest>(
                    new GetProductsByIdsRequest(request.ProductIdList, request.ItemIdList, request.WithData,
                        request.Lang));

            var mappedResult =
                _mapper.Map<ProductServiceListResponse, ProductServiceListResponseReturn>(
                    result.ProductServiceListResponse);
            return mappedResult;
        }
        public ProductServiceIdResponseReturn GetProductIds(GetProductIdsRequestReturn request)
        {
            var result =
                _serviceBuilder.Build<GetProductIdsResponse, GetProductIdsRequest>(
                    new GetProductIdsRequest(request.StartOffSet, request.RowCount, request.StatusTypes, request.Lang));

            var mappedResult =
                _mapper.Map<ProductServiceIdResponse, ProductServiceIdResponseReturn>(result.ProductServiceIdResponse);
            return mappedResult;
        }

        public void CheckForItemId()
        {
            throw new System.NotImplementedException();
        }

        public void GetProductStatuses()
        {
            throw new System.NotImplementedException();
        }

        public void UpdateItemId()
        {
            throw new System.NotImplementedException();
        }

        public void UpdateProductVariants()
        {
            throw new System.NotImplementedException();
        }

        public void GetProductVariants()
        {
            throw new System.NotImplementedException();
        }

        public void UpdateStockAndActivateProduct()
        {
            throw new System.NotImplementedException();
        }

        public void UpdateMarketPrice()
        {
            throw new System.NotImplementedException();
        }

        public void UpdateVariantStockAndActivateProduct()
        {
            throw new System.NotImplementedException();
        }

        public void GetItemIdDetails()
        {
            throw new System.NotImplementedException();
        }

        public void GpdateProductSales()
        {
            throw new System.NotImplementedException();
        }
    }
}
