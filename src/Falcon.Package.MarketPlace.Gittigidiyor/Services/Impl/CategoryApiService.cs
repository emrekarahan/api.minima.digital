﻿using System;
using AutoMapper;
using Falcon.Package.MarketPlace.Gittigidiyor.Builder;
using Falcon.Package.MarketPlace.Gittigidiyor.Helpers;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Category.Requests;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Category.Response;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Category.Requests;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Category.Responses;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Services.Impl
{
    public class CategoryApiService : ICategoryApiService
    {
        private readonly IGittigidiyorService _gittigidiyorService;
        private readonly IMapper _mapper;

        public CategoryApiService(
            IGittigidiyorService gittigidiyorService,
            IMapper mapper)
        {
            _gittigidiyorService = gittigidiyorService;
            _mapper = mapper;
        }

        public CategoryServiceResponseReturn GetCategories(GetCategoriesRequestReturn request)
        {
            var result = _gittigidiyorService.Build<GetCategoriesResponse, GetCategoriesRequest>(
                new GetCategoriesRequest(request.StartOffset, request.RowCount, request.WithSpecs, request.WithDeepest,
                    request.WithCatalog, request.Lang));

            var mapped =
                _mapper.Map<CategoryServiceResponse, CategoryServiceResponseReturn>(result.CategoryServiceResponse);
            return mapped;

            //try
            //{
            //    //long timelong = cevirTimeSpan(DateTime.Now);
            //    //string md5Hash = getMd5Hash(_gittigidiyorSettings.ApiKey + _gittigidiyorSettings.ApiSecret + timelong);

            //    //CategoryServiceClient client = new CategoryServiceClient();
            //    //OperationContextScope scope = new OperationContextScope(client.InnerChannel);
            //    //HttpRequestMessageProperty httpRequest = new HttpRequestMessageProperty();

            //    //byte[] authbytes = Encoding.ASCII.GetBytes(
            //    //    $"{_gittigidiyorSettings.RoleName}:{_gittigidiyorSettings.RolePassword}");
            //    //string base64 = Convert.ToBase64String(authbytes);
            //    //httpRequest.Headers.Add("Authorization", "Basic " + base64);
            //    //OperationContext.Current.OutgoingMessageProperties.Add(HttpRequestMessageProperty.Name, httpRequest);
            //    //ServicePointManager.Expect100Continue = false;
            //    //var result = await client.getCategoriesAsync(0, 5, true, true, true, "tr");


            //    var result = await _categoryService.getCategoriesAsync(0, 5, true, true, true, "tr");
            //    return result;

            //}
            //catch (Exception e)
            //{
            //    Console.WriteLine(e);
            //    throw;
            //}
        }

        public CategoryServiceAuditResponseReturn GetModifiedCategories(GetModifiedCategoriesRequestReturn request)
        {
            var changeTime = CommonHelper.convertToTimestamp(DateTime.UtcNow);

            var result =
                _gittigidiyorService.Build<GetModifiedCategoriesResponse, GetModifiedCategoriesRequest>(
                    new GetModifiedCategoriesRequest(changeTime, request.StartOffset, request.RowCount, request.Lang));

            var mapped =
                _mapper.Map<CategoryServiceAuditResponse, CategoryServiceAuditResponseReturn>(
                    result.CategoryServiceAuditResponse);
            return mapped;
        }

        public CategoryServiceResponseReturn GetCategory(GetCategoryRequestReturn request)
        {
            var result = _gittigidiyorService.Build<GetCategoryResponse, GetCategoryRequest>(
                new GetCategoryRequest(request.CategoryCode, request.WithSpecs, request.WithDeepest,
                    request.WithCatalog, request.Lang));
            var mapped =
                _mapper.Map<CategoryServiceResponse, CategoryServiceResponseReturn>(result.CategoryServiceResponse);
            return mapped;
        }

        public CategorySpecsServiceResponseReturn GetCategorySpecs(CategoryCodeRequestReturn request)
        {
            var result =
                _gittigidiyorService.Build<GetCategorySpecsResponse, GetCategorySpecsRequest>(
                    new GetCategorySpecsRequest(request.CategoryCode, request.Lang));
            var mapped =
                _mapper.Map<CategorySpecsServiceResponse, CategorySpecsServiceResponseReturn>(
                    result.CategorySpecsServiceResponse);
            return mapped;
        }


        public CategoryServiceResponseReturn GetDeepestCategories(GetDeepestCategoriesRequestReturn request)
        {
            var result =
                _gittigidiyorService.Build<GetDeepestCategoriesResponse, GetDeepestCategoriesRequest>(
                    new GetDeepestCategoriesRequest(request.StartOffset, request.RowCount, request.WithSpecs,
                        request.Lang));
            var mapped =
                _mapper.Map<CategoryServiceResponse, CategoryServiceResponseReturn>(result.CategoryServiceResponse);
            return mapped;
        }


        public CategoryServiceResponseReturn GetCategoriesByCodes(GetCategoriesByCodesRequestReturn request)
        {
            var result =
                _gittigidiyorService.Build<GetCategoriesByCodesResponse, GetCategoriesByCodesRequest>(
                    new GetCategoriesByCodesRequest(request.CategoryCodes, request.WithSpecs, request.WithDeepest,
                        request.WithCatalog, request.Lang));

            var mapped =
                _mapper.Map<CategoryServiceResponse, CategoryServiceResponseReturn>(result.CategoryServiceResponse);
            return mapped;
        }

        public CategoryServiceVariantResponseReturn GetCategoryVariantSpecs(CategoryCodeRequestReturn request)
        {
            var result =
                _gittigidiyorService.Build<GetCategoryVariantSpecsResponse, GetCategoryVariantSpecsRequest>(
                    new GetCategoryVariantSpecsRequest(request.CategoryCode, request.Lang));

            var mapped =
                _mapper.Map<CategoryServiceVariantResponse, CategoryServiceVariantResponseReturn>(
                    result.CategoryServiceVariantResponse);
            return mapped;
        }


        public CategorySpecsWithDetailServiceResponseReturn GetCategorySpecsWithDetail(
            CategoryCodeRequestReturn request)
        {
            var result =
                _gittigidiyorService.Build<GetCategorySpecsWithDetailResponse, GetCategorySpecsWithDetailRequest>(
                    new GetCategorySpecsWithDetailRequest(request.CategoryCode, request.Lang));

            var mapped =
                _mapper.Map<CategorySpecsWithDetailServiceResponse, CategorySpecsWithDetailServiceResponseReturn>(
                    result.CategorySpecsWithDetailServiceResponse);
            return mapped;
        }

        public CategoryServiceResponseReturn GetCategoriesHavingVariantSpecs(BaseRequestReturn request)
        {
            var result =
                _gittigidiyorService
                    .Build<GetCategoriesHavingVariantSpecsResponse, GetCategoriesHavingVariantSpecsRequest>(
                        new GetCategoriesHavingVariantSpecsRequest(request.Lang));

            var mapped =
                _mapper.Map<CategoryServiceResponse, CategoryServiceResponseReturn>(result.CategoryServiceResponse);
            return mapped;
        }


        public CategoryServiceResponseReturn GetParentCategories(GetParentCategoriesRequestReturn request)
        {
            var result =
                _gittigidiyorService.Build<GetParentCategoriesResponse, GetParentCategoriesRequest>(
                    new GetParentCategoriesRequest(request.WithSpecs, request.WithDeepest, request.WithCatalog,
                        request.Lang));

            var mapped =
                _mapper.Map<CategoryServiceResponse, CategoryServiceResponseReturn>(result.CategoryServiceResponse);
            return mapped;
        }


        public CategoryServiceResponseReturn GetSubCategories(GetSubCategoriesRequestReturn request)
        {
            var result =
                _gittigidiyorService.Build<GetSubCategoriesResponse, GetSubCategoriesRequest>(
                    new GetSubCategoriesRequest(request.CategoryCode, request.WithSpecs,
                        request.WithDeepest, request.WithCatalog, request.Lang));
            var mapped =
                _mapper.Map<CategoryServiceResponse, CategoryServiceResponseReturn>(result.CategoryServiceResponse);
            return mapped;
        }
    }
}