﻿using AutoMapper;
using Falcon.Package.MarketPlace.Gittigidiyor.Builder;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Catalog.Request;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Catalog.Response;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Catalog.Requests;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Catalog.Responses;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Services.Impl
{
    public class CatalogApiService : ICatalogApiService
    {
        private readonly IGittigidiyorService _gittigidiyorService;
        private readonly IMapper _mapper;

        public CatalogApiService(
            IGittigidiyorService gittigidiyorService,
            IMapper mapper)
        {
            _gittigidiyorService = gittigidiyorService;
            _mapper = mapper;
        }

        public CatalogSearchServiceResponseReturn SearchCatalog(SearchCatalogRequestReturn request)
        {
            var searchRequest = _mapper.Map<SearchCatalogRequestReturn, SearchCatalogRequest>(request);

            var result =
                _gittigidiyorService.Build<SearchCatalogResponse, SearchCatalogRequest>(
                    new SearchCatalogRequest(searchRequest.Page, searchRequest.RowCount, searchRequest.Criteria,
                        searchRequest.Lang));

            var mapped = _mapper.Map<CatalogSearchServiceResponse, CatalogSearchServiceResponseReturn>(result.CatalogSearchServiceResponse);
            return mapped;
        }
    }
}
