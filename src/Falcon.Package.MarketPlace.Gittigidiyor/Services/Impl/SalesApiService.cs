﻿using AutoMapper;
using Falcon.Package.MarketPlace.Gittigidiyor.Builder;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Sale.Requests;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Sale.Responses;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Sale.Requests;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.Sale.Responses;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Services.Impl
{
    public class SalesApiService : ISalesApiService
    {
        private readonly IGittigidiyorService _gittigidiyorService;
        private readonly IMapper _mapper;

        public SalesApiService(
            IGittigidiyorService gittigidiyorService, 
            IMapper mapper)
        {
            _gittigidiyorService = gittigidiyorService;
            _mapper = mapper;
        }

        public SaleServicePagingResponseReturn GetPagedSales(GetPagedSalesRequestReturn request)
        {
           var data = _gittigidiyorService.Build<GetPagedSalesResponse, GetPagedSalesRequest>(
                new GetPagedSalesRequest(request.WithData, request.ByStatus, request.ByUser, request.OrderBy,
                    request.OrderType, request.PageNumber, request.PageSize, request.Lang));

           var mappedResult =
               _mapper.Map<SaleServicePagingResponse, SaleServicePagingResponseReturn>(data.SaleServicePagingResponse);
           return mappedResult;
        }
        public SaleServiceResponseReturn GetSale(GetSaleRequestReturn request)
        {
            var data = _gittigidiyorService.Build<GetSaleResponse, GetSaleRequest>(
                new GetSaleRequest(request.SaleCode, request.Lang));

            var mappedResult =
                _mapper.Map<SaleServiceResponse, SaleServiceResponseReturn>(data.SaleServiceResponse);
            return mappedResult;
        }
        public SaleServicePagingResponseReturn GetSalesByDateRange(GetSalesByDateRangeRequestReturn request)
        {
            var data = _gittigidiyorService.Build<GetSalesByDateRangeResponse, GetSalesByDateRangeRequest>(
                new GetSalesByDateRangeRequest(request.WithData, request.ByStatus, request.ByUser, request.OrderBy,
                    request.OrderType, request.StartDate, request.EndDate, request.PageNumber, request.PageSize,
                    request.Lang));

            var mappedResult =
                _mapper.Map<SaleServicePagingResponse, SaleServicePagingResponseReturn>(data.SaleServicePagingResponse);
            return mappedResult;
        }
        public void GiveRateAndComment()
        {
            throw new System.NotImplementedException();
        }
        public void ReplySaleComment()
        {
            throw new System.NotImplementedException();
        }
        public void RemindForApproval()
        {
            throw new System.NotImplementedException();
        }
        public void GetReasonsToCancelSales()
        {
            throw new System.NotImplementedException();
        }
        public void CancelSale()
        {
            throw new System.NotImplementedException();
        }
        public void GetAccountTransactionsV2()
        {
            throw new System.NotImplementedException();
        }
        public void GetSaleProcessReport()
        {
            throw new System.NotImplementedException();
        }
        public void RemoveSaleFromList()
        {
            throw new System.NotImplementedException();
        }
        public void GetItemBuyers()
        {
            throw new System.NotImplementedException();
        }
        public void ReceiveRemandedItem()
        {
            throw new System.NotImplementedException();
        }
        public void GiveApprovalForRemandedItem()
        {
            throw new System.NotImplementedException();
        }
        public void CancelSaleAfterEarlyCancellationRequest()
        {
            throw new System.NotImplementedException();
        }
    }
}
