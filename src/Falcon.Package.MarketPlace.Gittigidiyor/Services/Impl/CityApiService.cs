﻿using System;
using AutoMapper;
using Falcon.Package.MarketPlace.Gittigidiyor.Builder;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.City.Request;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.City.Response;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.City.Requests;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.Service.City.Responses;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Services.Impl
{
    public class CityApiService : ICityApiService
    {
        private readonly IGittigidiyorService _gittigidiyorService;
        private IMapper _mapper;

        public CityApiService(
            IGittigidiyorService gittigidiyorService, 
            IMapper mapper)
        {
            _gittigidiyorService = gittigidiyorService;
            _mapper = mapper;
        }

        public CityServiceResponseReturn GetCities(GetCitiesRequestReturn request)
        {
            var result = _gittigidiyorService.Build<GetCitiesResponse, GetCitiesRequest>(new GetCitiesRequest(
                request.StartOffSet, request.RowCount, request.Lang));
            var mapped = _mapper.Map<CityServiceResponse, CityServiceResponseReturn>(result.CityServiceResponse);
            return mapped;
        }
    }
}
