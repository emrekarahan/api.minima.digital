﻿using Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Catalog.Request;
using Falcon.Package.MarketPlace.Gittigidiyor.Models.ReturnTypes.Catalog.Response;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Services
{
    public interface ICatalogApiService
    {
        CatalogSearchServiceResponseReturn SearchCatalog(SearchCatalogRequestReturn request);

    }
}
