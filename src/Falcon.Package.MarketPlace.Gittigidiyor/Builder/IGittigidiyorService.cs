﻿namespace Falcon.Package.MarketPlace.Gittigidiyor.Builder
{
    public interface IGittigidiyorService
    {
        TOut Build<TOut, T>(in T request);
    }
}
