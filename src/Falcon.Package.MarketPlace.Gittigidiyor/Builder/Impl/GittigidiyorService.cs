﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using Falcon.Package.MarketPlace.Gittigidiyor.Attributes;
using Falcon.Package.MarketPlace.Gittigidiyor.Exceptions;
using RestSharp;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Builder.Impl
{
    public class GittigidiyorService : IGittigidiyorService
    {
        private readonly GittigidiyorSettings _gittigidiyorSettings;

        public GittigidiyorService(GittigidiyorSettings gittigidiyorSettings)
        {
            _gittigidiyorSettings = gittigidiyorSettings;
        }

        public TOut Build<TOut, T>(in T request)
        {
            var elementNamespace = GetElementNamespace(request);
            if (string.IsNullOrEmpty(elementNamespace))
                throw new ElementNamespaceNotFoundException("Element Namespace not found!");

            var responseType = GetResponseType(request);
            if (responseType == null)
                throw new ResponseTypeNotFoundException("Response Type not found!");

            var urlFragment = GetUrlFragment(request);
            if (string.IsNullOrEmpty(elementNamespace))
                throw new UrlFragmentNotFoundException("Service Url Fragment not found!");

            var requestUrl = $"{_gittigidiyorSettings.ApiUrl}/{urlFragment}";


            var parameters = Serialize(request, elementNamespace);

            var sb = new StringBuilder();
            sb.Append("<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">")
                .AppendLine("<soap:Body>");

            if (!string.IsNullOrEmpty(parameters))
                sb.AppendLine(parameters);

            sb.AppendLine("</soap:Body>")
                .AppendLine("</soap:Envelope>");

            var result = sb.ToString();
            
            var data = GetData(result, requestUrl);

            XmlNamespaceManager nsmgr = new XmlNamespaceManager(data.NameTable);
            nsmgr.AddNamespace("soap", "http://schemas.xmlsoap.org/soap/envelope/");
            data.LoadXml(data.DocumentElement.SelectSingleNode("soap:Body", nsmgr)?.ChildNodes[0].OuterXml);
            var zz = data.OuterXml;

            //Type MyType = Type.GetType(FromSomewhereElse);
            //var typeOfContext = this.GetType();
            //var method = typeOfContext.GetMethod("Deserialize");
            //var genericMethod = method.MakeGenericMethod(responseType);
            //var aa = genericMethod.Invoke(this, new object[] {zz});

            var aa = Deserialize<TOut>(zz);

            return aa;
        }



        private string Serialize<T>(T obj, string elementNamespace)
        {
            XmlSerializer xsSubmit = new XmlSerializer(obj.GetType());
            XmlSerializerNamespaces xmlnsEmpty = new XmlSerializerNamespaces();
            xmlnsEmpty.Add(string.Empty, "");
            //xmlnsEmpty.Add(string.Empty, "https://product.individual.ws.listingapi.gg.com");
            xmlnsEmpty.Add(string.Empty, elementNamespace);
            var settings = new XmlWriterSettings
            {
                Indent = true,
                OmitXmlDeclaration = true
            };
            using var sww = new StringWriter();
            using XmlWriter writer = XmlWriter.Create(sww, settings);
            xsSubmit.Serialize(writer, obj, xmlnsEmpty);
            var xml = sww.ToString();

            return xml;
        }
        public T Deserialize<T>(string serialized)
        {
            try
            {
                using var reader = new StringReader(serialized);
                return (T)this.Deserialize(reader, typeof(T));
            }
            catch (XmlException e)
            {
                throw new SerializationException(e.Message, e);
            }
        }
        private object Deserialize(TextReader reader, Type t)
        {
            return new XmlSerializer(t).Deserialize(reader);
        }


        public XmlDocument GetData(string xml, string address)
        {
            byte[] authbytes = Encoding.ASCII.GetBytes(
                $"{_gittigidiyorSettings.RoleName}:{_gittigidiyorSettings.RolePassword}");
            string base64 = Convert.ToBase64String(authbytes);

            IRestClient client = new RestClient(address);
            client.AddDefaultHeader("SOAPAction", "\"\"");
            client.AddDefaultHeader("Content-Type", "text /xml; charset=utf-8");
            IRestRequest request = new RestRequest(Method.POST);
            request.AddHeader("Authorization", "Basic " + base64);
            request.AddParameter("text/xml", xml, "text/xml", ParameterType.RequestBody);
            var response = client.Execute(request);

            if (!response.IsSuccessful) return null;
            var xmlResponse = new XmlDocument();
            xmlResponse.LoadXml(response.Content);
            return xmlResponse;
        }

        #region Private
        private string GetElementName<T>(T target)
        {
            XmlRootAttribute attribute = target.GetType().GetCustomAttribute<XmlRootAttribute>();
            return attribute?.ElementName;
        }

        private string GetElementNamespace<T>(T target)
        {
            XmlRootAttribute attribute = target.GetType().GetCustomAttribute<XmlRootAttribute>();
            return attribute?.Namespace;
        }

        private Type GetResponseType<T>(T target)
        {
            ResponseTypeAttribute attribute = target.GetType().GetCustomAttribute<ResponseTypeAttribute>();
            return attribute?.ResponseType;
        }
        
        private string GetUrlFragment<T>(T target)
        {
            UrlFragmentAttribute attribute = target.GetType().GetCustomAttribute<UrlFragmentAttribute>();
            return attribute?.Fragment;
        }

        private XElement RemoveAllNamespaces(XElement xmlDocument)
        {
            if (!xmlDocument.HasElements)
            {
                XElement xElement = new XElement(xmlDocument.Name.LocalName)
                {
                    Value = xmlDocument.Value
                };

                foreach (XAttribute attribute in xmlDocument.Attributes())
                    xElement.Add(attribute);

                return xElement;
            }
            return new XElement(xmlDocument.Name.LocalName, xmlDocument.Elements().Select(el => RemoveAllNamespaces(el)));
        }
        #endregion
    }
}
