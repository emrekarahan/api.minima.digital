﻿using System;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    internal class ResponseTypeAttribute : Attribute
    {
        public Type ResponseType { get;}
        
        public ResponseTypeAttribute(Type type)
        {
            ResponseType = type;
        }
    }
}
