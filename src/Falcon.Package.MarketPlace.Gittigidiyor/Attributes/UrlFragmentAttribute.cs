﻿using System;

namespace Falcon.Package.MarketPlace.Gittigidiyor.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class UrlFragmentAttribute : Attribute
    {
        public string Fragment { get; }

        public UrlFragmentAttribute(string fragment)
        {
            Fragment = fragment;
        }
    }
}
