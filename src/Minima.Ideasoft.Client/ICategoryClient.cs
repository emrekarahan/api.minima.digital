﻿using System.Threading.Tasks;
using Minima.Core.Models.Request;
using RestSharp;

namespace Minima.Ideasoft.Client
{
    public interface ICategoryClient
    {
        Task<IRestResponse> GetCategoryList(CategoryRequestModel categoryRequestModel);
        Task<IRestResponse> GetNewCategories(CategoryRequestModel categoryRequestModel);
    }
}