﻿using Microsoft.Extensions.DependencyInjection;
using Minima.Ideasoft.Client.Impl;

namespace Minima.Ideasoft.Client
{
    public static class ServiceExtensions
    {
        public static IServiceCollection AddIdeasoftClient(this IServiceCollection services)
        {
            services.AddScoped<IProductClient, ProductClient>()
                .AddScoped<ICategoryClient, CategoryClient>()
                .AddScoped<ICustomerClient, CustomerClient>()
                .AddScoped<IProductClient, ProductClient>()
                .AddScoped<IOrderClient, OrderClient>();
            return services;
        }
    }
}