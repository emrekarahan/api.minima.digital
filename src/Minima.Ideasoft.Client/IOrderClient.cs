﻿using System;
using System.Threading.Tasks;
using RestSharp;

namespace Minima.Ideasoft.Client
{
    public interface IOrderClient
    {
        Task<IRestResponse> GetOrderList(DateTime? startDate, DateTime? endDate, string status, string s);
    }
}