﻿using System;
using Minima.Data.MongoDb.Attribute;
using Minima.Data.MongoDb.Impl;

namespace Minima.Ideasoft.Client.MongoDb
{
    [BsonCollection("DailyRawCategories")]
    public class DailyUpdatedCategories : Document
    {
        public int CategoryId { get; set; }
        public DateTimeOffset UpdatedAt { get; set; }
    }
}