﻿using System;
using Minima.Data.MongoDb.Attribute;
using Minima.Data.MongoDb.Impl;

namespace Minima.Ideasoft.Client.MongoDb
{
    [BsonCollection("DailyRawProducts")] 
    public class DailyUpdatedProducts : Document
    {
        public int ProductId { get; set; }
        public DateTimeOffset UpdatedAt { get; set; }

    }
}