﻿using System;
using System.Threading.Tasks;
using Minima.Core.Models.Request;
using RestSharp;

namespace Minima.Ideasoft.Client
{
    public interface IProductClient
    {
        Task<IRestResponse> GetProductList(ProductRequestModel productRequestModel);
        Task<IRestResponse> GetProductListStock(ProductRequestModel productRequestModel);
        Task GetNewProducts(ProductRequestModel productRequestModel);
        Task UpdateStock(int productId, int stock);
        double? GetProductDetail(int ideaProductId);

    }
}