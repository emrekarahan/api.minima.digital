﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.Extensions.Logging;
using Minima.Core;
using Minima.Core.Contants;
using Minima.Core.MessageModels;
using Minima.Core.Models.Request;
using Minima.Core.Repositories;
using Minima.Ideasoft.Client.MongoDb;
using Minima.Integration.Data;
using Minima.Integration.Data.Domain;
using Minima.Data.MongoDb.Repository;
using Newtonsoft.Json;
using Padawan.RabbitMq.Extensions;
using Padawan.RabbitMq.Provider;
using RestSharp;
using Minima.Logging.Extensions;


namespace Minima.Ideasoft.Client.Impl
{
    public class ProductClient : IProductClient
    {
        private readonly ILogger<ProductClient> _logger;
        private readonly IBusProvider _busProvider;
        private readonly IRepository<IdeasoftProductMap, IntegrationContext> _productRepository;
        private readonly IMapper _mapper;
        private readonly IMongoRepository<DailyUpdatedProducts> _dailyUpdatedProductsRepository;
        private static decimal PRODUCT_TOTAL_COUNT = 0;
        private decimal PRODUCT_PAGE_COUNT => Math.Ceiling(PRODUCT_TOTAL_COUNT / 100.00m);

        public ProductClient(
            ILogger<ProductClient> logger,
            IBusProvider busProvider,
            IRepository<IdeasoftProductMap, IntegrationContext> productRepository,
            IMapper mapper,
            IMongoRepository<DailyUpdatedProducts> dailyUpdatedProductsRepository)
        {
            _logger = logger;
            _busProvider = busProvider;
            _productRepository = productRepository;
            _mapper = mapper;
            _dailyUpdatedProductsRepository = dailyUpdatedProductsRepository;
        }


        public async Task<IRestResponse> GetProductList(ProductRequestModel productRequestModel)
        {
            await GetProductCount(productRequestModel);

            _logger.LogInformation($"{PRODUCT_TOTAL_COUNT} adet ürün güncelleniyor");

            IRestResponse response = null;
            var url = ApiUrl.PRODUCT;
            for (int i = 1; i < PRODUCT_PAGE_COUNT + 1; i++)
            {
                productRequestModel.Page = i;
                List<Parameter> parameters = ProductParameterBuilder(productRequestModel);
                response = await RestsharpHelper.GetAsync(url, parameters);

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var message = JsonConvert.DeserializeObject<List<ProductMessageModel>>(response.Content);

                    foreach (ProductMessageModel productMessageModel in message)
                    {
                        if (productMessageModel.StockAmount > 0)
                        {
                            //await InsertMongoAsync(productMessageModel);
                            await SendAsync("product_save", productMessageModel);
                        }
                    }

                }
                else
                {
                    _logger.LogInformation(response.StatusDescription);
                }

                await Task.Delay(1000);
            }
            return response;
        }

        public async Task<IRestResponse> GetProductListStock(ProductRequestModel productRequestModel)
        {
            await GetProductCount(productRequestModel);

            _logger.LogInformation($"{PRODUCT_TOTAL_COUNT} adet ürün güncelleniyor");

            IRestResponse response = null;
            var url = ApiUrl.PRODUCT;
            for (int i = 1; i < PRODUCT_PAGE_COUNT + 1; i++)
            {
                productRequestModel.Page = i;
                List<Parameter> parameters = ProductParameterBuilder(productRequestModel);
                response = await RestsharpHelper.GetAsync(url, parameters);

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var message = JsonConvert.DeserializeObject<List<ProductMessageModel>>(response.Content);

                    foreach (ProductMessageModel productMessageModel in message)
                    {

                        await SendAsync("product_save_stock", productMessageModel);
                    }

                }
                else
                {
                    _logger.LogInformation(response.StatusDescription);
                }

                await Task.Delay(1000);
            }
            return response;
        }


        public async Task GetNewProducts(ProductRequestModel productRequestModel)
        {
            await GetProductCount(productRequestModel);

            _logger.LogInformation($"{PRODUCT_TOTAL_COUNT} adet ürün güncelleniyor");

            IRestResponse response = null;
            var url = ApiUrl.PRODUCT;
            for (int i = 1; i < PRODUCT_PAGE_COUNT + 1; i++)
            {
                productRequestModel.Page = i;
                List<Parameter> parameters = ProductParameterBuilder(productRequestModel);
                response = await RestsharpHelper.GetAsync(url, parameters);

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var message = JsonConvert.DeserializeObject<List<ProductMessageModel>>(response.Content);

                    foreach (ProductMessageModel productMessageModel in message)
                    {
                        var reallyUpdated = await IsDdailyUpdated(productMessageModel);
                        if (!reallyUpdated)
                            continue;


                        //await InsertMongoAsync(productMessageModel);
                        await SendAsync("product_save", productMessageModel);

                    }

                }
                else
                {
                    _logger.LogInformation(response.StatusDescription);
                }

                await Task.Delay(1000);
            }
            //return response;
        }

        public async Task UpdateStock(int productId, int stock)
        {
            try
            {
                var client = new RestClient($"{ApiUrl.PRODUCT}/{productId}");
                client.AddDefaultHeader("Authorization", $"Bearer {CurrentTokenStatic.AccessToken}");
                var request = new RestRequest(Method.PUT);
                request.AddJsonBody(new
                {
                    stockAmount = stock
                });
                var response = await client.ExecuteAsync(request);


                if (!response.IsSuccessful)
                {
                    _logger.LogError(new EventId(666), new Exception(), "Response is not successful", new
                    {
                        IsSuccess = false,
                        Message = response.StatusDescription,
                        Task = "Product_Stock_Update"
                    }, "Product_Stock_Update");
                    throw new Exception();
                }
            }
            catch (Exception e)
            {
                _logger.LogError(new EventId(666), "Response is not successful", new
                {
                    IsSuccess = false,
                    Message = "failed",
                    Task = "Product_Stock_Update"
                }, "Product_Stock_Update");
                throw;
            }
        }

        public double? GetProductDetail(int ideaProductId)
        {
            IRestResponse response = null;

            try
            {
                var client = new RestClient($"{ApiUrl.PRODUCT}/{ideaProductId}");
                client.AddDefaultHeader("Authorization", $"Bearer {CurrentTokenStatic.AccessToken}");
                var request = new RestRequest(Method.GET);
                response = client.ExecuteAsync(request).GetAwaiter().GetResult();
                if (response.IsSuccessful)
                {
                    var model = JsonConvert.DeserializeObject<ProductMessageModel>(response.Content);
                    return model.StockAmount;
                }
                return null;
            }
            catch (Exception e)
            {
                _logger.LogError(e, new { Response = response.Content }, "GetProductDetail");
            }
            return null;
        }

        #region Private Methods
        private async Task<bool> IsDdailyUpdated(ProductMessageModel messageModel)
        {
            try
            {
                var existDailyUpdated = await _dailyUpdatedProductsRepository
                    .FindOneAsync(f => f.ProductId == messageModel.Id);

                if (existDailyUpdated == null)
                {
                    var dailyUpdatedModel = new DailyUpdatedProducts
                    {
                        ProductId = messageModel.Id,
                        UpdatedAt = messageModel.UpdatedAt
                    };
                    await _dailyUpdatedProductsRepository.InsertOneAsync(dailyUpdatedModel);
                    return true;
                }

                if (messageModel.UpdatedAt == existDailyUpdated.UpdatedAt)
                    return false;

                existDailyUpdated.UpdatedAt = messageModel.UpdatedAt.Date;
                await _dailyUpdatedProductsRepository.ReplaceOneAsync(existDailyUpdated);
                return true;
            }
            catch (Exception e)
            {
                _logger.LogError(new EventId(500), e, e.Message);
            }

            return false;
        }
        private async Task GetProductCount(ProductRequestModel productRequestModel)
        {
            if (PRODUCT_TOTAL_COUNT != 0)
                return;

            var url = ApiUrl.PRODUCT;

            List<Parameter> parameters = ProductParameterBuilder(productRequestModel);
            var response = await RestsharpHelper.GetAsync(url, parameters);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                var totalCountParamenter = response.Headers.FirstOrDefault(s => s.Name == "total_count");
                if (totalCountParamenter != null)
                {
                    PRODUCT_TOTAL_COUNT = Convert.ToInt32(totalCountParamenter.Value);
                }
            }
        }
        private List<Parameter> ProductParameterBuilder(ProductRequestModel categoryRequestModel)
        {
            List<Parameter> parameters = new List<Parameter>
            {
                new Parameter("page", categoryRequestModel.Page, ParameterType.QueryString),
                new Parameter("limit", categoryRequestModel.Limit, ParameterType.QueryString)
            };

            if (categoryRequestModel.SinceId.HasValue)
                parameters.Add(new Parameter("sinceId", categoryRequestModel.SinceId.Value, ParameterType.QueryString));
            if (categoryRequestModel.StartDate.HasValue)
                parameters.Add(new Parameter("startDate", categoryRequestModel.StartDate.Value.ToString("yyyy-MM-dd"), ParameterType.QueryString));
            if (categoryRequestModel.EndDate.HasValue)
                parameters.Add(new Parameter("endDate", categoryRequestModel.EndDate.Value.ToString("yyyy-MM-dd"), ParameterType.QueryString));
            if (categoryRequestModel.StartUpdatedAt.HasValue)
                parameters.Add(new Parameter("startUpdatedAt", categoryRequestModel.StartUpdatedAt.Value.ToString("yyyy-MM-dd"), ParameterType.QueryString));
            if (categoryRequestModel.EndUpdatedAt.HasValue)
                parameters.Add(new Parameter("endUpdatedAt", categoryRequestModel.EndUpdatedAt.Value.ToString("yyyy-MM-dd"), ParameterType.QueryString));

            return parameters;

        }
        private async Task SendAsync<T>(string type, T message) where T : class
        {
            try
            {
                var culture = new CultureInfo("en-US");

                await _busProvider.GetInstance().Send(message, $"integration_{type.ToLower(culture)}");
            }
            catch (Exception e)
            {
                _logger.LogError(new EventId(500), e, $"Error on {type} queue messaging");

                //throw new IntegrationMessageExceptions();
            }
        }


        #endregion

    }
}