﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Minima.Core;
using Minima.Core.Contants;
using Minima.Core.MessageModels;
using Minima.Core.Models.Request;
using Minima.Core.Repositories;
using Minima.Ideasoft.Client.MongoDb;
using Minima.Integration.Data;
using Minima.Integration.Data.Domain;
using Minima.Data.MongoDb.Repository;
using Newtonsoft.Json;
using Padawan.RabbitMq.Extensions;
using Padawan.RabbitMq.Provider;
using RestSharp;

namespace Minima.Ideasoft.Client.Impl
{
    public class CategoryClient : ICategoryClient
    {
        private readonly IRepository<IdeasoftCategoryMap, IntegrationContext> _categoryRepository;
        private readonly ILogger<CategoryClient> _logger;
        private readonly IBusProvider _busProvider;
        private IMongoRepository<DailyUpdatedCategories> _dailyUpdateCategoryRepository;

        private static decimal CATEGORY_TOTAL_COUNT = 0;
        private decimal CATEGORY_PAGE_COUNT => Math.Ceiling(CATEGORY_TOTAL_COUNT / CATEGORY_PAGE_LIMIT);
        private static decimal CATEGORY_PAGE_LIMIT;

        public CategoryClient(
            IRepository<IdeasoftCategoryMap,
                IntegrationContext> categoryRepository,
            IBusProvider busProvider, ILogger<CategoryClient> logger, IMongoRepository<DailyUpdatedCategories> dailyUpdateCategoryRepository)
        {
            _categoryRepository = categoryRepository;
            _busProvider = busProvider;
            _logger = logger;
            _dailyUpdateCategoryRepository = dailyUpdateCategoryRepository;
        }


        public async Task<IRestResponse> GetCategoryList(CategoryRequestModel categoryRequestModel)
        {
            await GetCategoryCount(categoryRequestModel);

            _logger.LogInformation($"{CATEGORY_TOTAL_COUNT} adet kategori güncelleniyor");

            IRestResponse response = null;
            var url = ApiUrl.CATEGORY;
            for (int i = 1; i < CATEGORY_PAGE_COUNT + 1; i++)
            {
                categoryRequestModel.Page = i;
                List<Parameter> parameters = CategoryParameterBuilder(categoryRequestModel);

                response = await RestsharpHelper.GetAsync(url, parameters);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var message = JsonConvert.DeserializeObject<List<CategoryMessageModel>>(response.Content);
                    foreach (var categoryMessageModel in message)
                        await SendAsync("category_save", categoryMessageModel, 100);
                }
                else
                {
                    _logger.LogError(response.StatusDescription);
                }
                await Task.Delay(1000);
            }
            return response;
        }
        public async Task<IRestResponse> GetNewCategories(CategoryRequestModel categoryRequestModel)
        {
            await GetCategoryCount(categoryRequestModel);
            
            _logger.LogInformation($"{CATEGORY_TOTAL_COUNT} adet kategori güncelleniyor");

            IRestResponse response = null;
            var url = ApiUrl.CATEGORY;
            for (int i = 1; i < CATEGORY_PAGE_COUNT + 1; i++)
            {
                categoryRequestModel.Page = i;
                List<Parameter> parameters = CategoryParameterBuilder(categoryRequestModel);

                response = await RestsharpHelper.GetAsync(url, parameters);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var message = JsonConvert.DeserializeObject<List<CategoryMessageModel>>(response.Content);
                    foreach (var categoryMessageModel in message)
                    {
                        var reallyUpdated = await IsDdailyUpdated(categoryMessageModel);
                        if (!reallyUpdated)
                            continue;
                        await SendAsync("category_save", categoryMessageModel, 100);
                    }
                        
                }
                else
                {
                    _logger.LogError(response.StatusDescription);
                }
                await Task.Delay(1000);
            }
            return response;
        }

        private async Task GetCategoryCount(CategoryRequestModel categoryRequestModel)
        {
            if (CATEGORY_TOTAL_COUNT != 0)
                return;


            CATEGORY_PAGE_LIMIT = categoryRequestModel.Limit;

            IRestResponse response = null;
            var url = ApiUrl.CATEGORY;

            List<Parameter> parameters = CategoryParameterBuilder(categoryRequestModel);

            response = await RestsharpHelper.GetAsync(url, parameters);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                var totalCountParamenter = response.Headers.FirstOrDefault(s => s.Name == "total_count");
                if (totalCountParamenter != null)
                {
                    CATEGORY_TOTAL_COUNT = Convert.ToInt32(totalCountParamenter.Value);
                }
            }
        }
        private List<Parameter> CategoryParameterBuilder(CategoryRequestModel categoryRequestModel)
        {
            List<Parameter> parameters = new List<Parameter>
            {
                new Parameter("page", categoryRequestModel.Page, ParameterType.QueryString),
                new Parameter("limit", categoryRequestModel.Limit, ParameterType.QueryString)
            };

            if (categoryRequestModel.SinceId.HasValue)
                parameters.Add(new Parameter("sinceId", categoryRequestModel.SinceId.Value, ParameterType.QueryString));
            if (categoryRequestModel.StartDate.HasValue)
                parameters.Add(new Parameter("startDate", categoryRequestModel.StartDate.Value.ToString("yyyy-MM-dd"), ParameterType.QueryString));
            if (categoryRequestModel.EndDate.HasValue)
                parameters.Add(new Parameter("endDate", categoryRequestModel.EndDate.Value.ToString("yyyy-MM-dd"), ParameterType.QueryString));
            if (categoryRequestModel.StartUpdatedAt.HasValue)
                parameters.Add(new Parameter("startUpdatedAt", categoryRequestModel.StartUpdatedAt.Value.ToString("yyyy-MM-dd"), ParameterType.QueryString));
            if (categoryRequestModel.EndUpdatedAt.HasValue)
                parameters.Add(new Parameter("endUpdatedAt", categoryRequestModel.EndUpdatedAt.Value.ToString("yyyy-MM-dd"), ParameterType.QueryString));

            return parameters;

        }
        private async Task<bool> IsDdailyUpdated(CategoryMessageModel messageModel)
        {
            try
            {
                var existDailyUpdated = await _dailyUpdateCategoryRepository
                    .FindOneAsync(f => f.CategoryId == messageModel.Id);

                if (existDailyUpdated == null)
                {
                    var dailyUpdatedModel = new DailyUpdatedCategories
                    {
                        CategoryId = messageModel.Id,
                        UpdatedAt = messageModel.UpdatedAt
                    };
                    await _dailyUpdateCategoryRepository.InsertOneAsync(dailyUpdatedModel);
                    return true;
                }

                if (messageModel.UpdatedAt == existDailyUpdated.UpdatedAt)
                    return false;

                existDailyUpdated.UpdatedAt = messageModel.UpdatedAt.Date;
                await _dailyUpdateCategoryRepository.ReplaceOneAsync(existDailyUpdated);
                return true;
            }
            catch (Exception e)
            {
                _logger.LogError(new EventId(500), e, e.Message);
            }

            return false;
        }
        private async Task SendAsync<T>(string type, T message, int pageSize) where T : class
        {

            //if (string.IsNullOrEmpty(message.Integrator))
            //    throw new IntegrationMissingConfigurationForIntegratoreExceptions();


            //if (message.OrderStatusCode == null || message.EventDate == null)
            //    throw new IntegrationPayloadNullExceptions();

            try
            {
                var culture = new CultureInfo("en-US");

                await _busProvider.GetInstance().Send<T>(message, $"integration_{type.ToLower(culture)}");
            }
            catch (Exception e)
            {
                _logger.LogError(new EventId(500), e, $"Error on {type} queue messaging");

                //throw new IntegrationMessageExceptions();
            }
        }
    }
}