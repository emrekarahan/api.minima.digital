﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;
using System.Threading.Tasks;
using MassTransit;
using Microsoft.Extensions.Logging;
using Minima.Core;
using Minima.Core.Contants;
using Minima.Core.MessageModels;
using RestSharp;

namespace Minima.Ideasoft.Client.Impl
{
    public class OrderClient : IOrderClient
    {
        private readonly ILogger<OrderClient> _logger;

        public OrderClient(ILogger<OrderClient> logger)
        {
            _logger = logger;
        }

        public async Task<IRestResponse> GetOrderList(DateTime? startDate, DateTime? endDate, string status, string s)
        {
            try
            {
                var client = new RestClient($"{ApiUrl.ORDER}");
                var request = new RestRequest(Method.GET);
                client.AddDefaultHeader("Authorization", $"Bearer {CurrentTokenStatic.AccessToken}");
                //request.AddParameter("status", status);
                //request.AddParameter("paymentStatus", s);
                request.AddParameter("startDate", startDate?.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture));
                request.AddParameter("endDate", endDate?.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture));

                var response = await client.ExecuteAsync(request);

                if (response.IsSuccessful && response.StatusCode == HttpStatusCode.OK)
                {
                    return response;
                }

                return null;
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
            }

            return null;
        }
    }
}