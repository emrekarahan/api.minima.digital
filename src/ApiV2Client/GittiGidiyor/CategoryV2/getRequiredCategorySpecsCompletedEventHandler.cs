﻿using System.CodeDom.Compiler;

namespace ApiV2Client.GittiGidiyor.CategoryV2
{
	// Token: 0x02000086 RID: 134
	// (Invoke) Token: 0x060004E5 RID: 1253
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	public delegate void getRequiredCategorySpecsCompletedEventHandler(object sender, getRequiredCategorySpecsCompletedEventArgs e);
}
