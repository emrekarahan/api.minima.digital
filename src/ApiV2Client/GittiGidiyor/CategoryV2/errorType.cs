﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.CategoryV2
{
	// Token: 0x02000085 RID: 133
	[XmlType(Namespace = "http://categoryv2.anonymous.ws.listingapi.gg.com")]
	[DesignerCategory("code")]
	[DebuggerStepThrough]
	[GeneratedCode("System.Xml", "4.7.3190.0")]
	[Serializable]
	public class errorType
	{
		// Token: 0x170001AF RID: 431
		// (get) Token: 0x060004DB RID: 1243 RVA: 0x00007507 File Offset: 0x00005707
		// (set) Token: 0x060004DC RID: 1244 RVA: 0x0000750F File Offset: 0x0000570F
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string errorId
		{
			get
			{
				return this.errorIdField;
			}
			set
			{
				this.errorIdField = value;
			}
		}

		// Token: 0x170001B0 RID: 432
		// (get) Token: 0x060004DD RID: 1245 RVA: 0x00007518 File Offset: 0x00005718
		// (set) Token: 0x060004DE RID: 1246 RVA: 0x00007520 File Offset: 0x00005720
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string errorCode
		{
			get
			{
				return this.errorCodeField;
			}
			set
			{
				this.errorCodeField = value;
			}
		}

		// Token: 0x170001B1 RID: 433
		// (get) Token: 0x060004DF RID: 1247 RVA: 0x00007529 File Offset: 0x00005729
		// (set) Token: 0x060004E0 RID: 1248 RVA: 0x00007531 File Offset: 0x00005731
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string message
		{
			get
			{
				return this.messageField;
			}
			set
			{
				this.messageField = value;
			}
		}

		// Token: 0x170001B2 RID: 434
		// (get) Token: 0x060004E1 RID: 1249 RVA: 0x0000753A File Offset: 0x0000573A
		// (set) Token: 0x060004E2 RID: 1250 RVA: 0x00007542 File Offset: 0x00005742
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string viewMessage
		{
			get
			{
				return this.viewMessageField;
			}
			set
			{
				this.viewMessageField = value;
			}
		}

		// Token: 0x040001DD RID: 477
		private string errorIdField;

		// Token: 0x040001DE RID: 478
		private string errorCodeField;

		// Token: 0x040001DF RID: 479
		private string messageField;

		// Token: 0x040001E0 RID: 480
		private string viewMessageField;
	}
}
