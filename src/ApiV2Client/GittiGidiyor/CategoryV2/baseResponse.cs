﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.CategoryV2
{
	// Token: 0x02000082 RID: 130
	[DebuggerStepThrough]
	[XmlType(Namespace = "http://categoryv2.anonymous.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.7.3190.0")]
	[DesignerCategory("code")]
	[XmlInclude(typeof(categorySpecServiceListResponse))]
	[Serializable]
	public class baseResponse
	{
		// Token: 0x170001A1 RID: 417
		// (get) Token: 0x060004BC RID: 1212 RVA: 0x00007401 File Offset: 0x00005601
		// (set) Token: 0x060004BD RID: 1213 RVA: 0x00007409 File Offset: 0x00005609
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string ackCode
		{
			get
			{
				return this.ackCodeField;
			}
			set
			{
				this.ackCodeField = value;
			}
		}

		// Token: 0x170001A2 RID: 418
		// (get) Token: 0x060004BE RID: 1214 RVA: 0x00007412 File Offset: 0x00005612
		// (set) Token: 0x060004BF RID: 1215 RVA: 0x0000741A File Offset: 0x0000561A
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string responseTime
		{
			get
			{
				return this.responseTimeField;
			}
			set
			{
				this.responseTimeField = value;
			}
		}

		// Token: 0x170001A3 RID: 419
		// (get) Token: 0x060004C0 RID: 1216 RVA: 0x00007423 File Offset: 0x00005623
		// (set) Token: 0x060004C1 RID: 1217 RVA: 0x0000742B File Offset: 0x0000562B
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public errorType error
		{
			get
			{
				return this.errorField;
			}
			set
			{
				this.errorField = value;
			}
		}

		// Token: 0x170001A4 RID: 420
		// (get) Token: 0x060004C2 RID: 1218 RVA: 0x00007434 File Offset: 0x00005634
		// (set) Token: 0x060004C3 RID: 1219 RVA: 0x0000743C File Offset: 0x0000563C
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string timeElapsed
		{
			get
			{
				return this.timeElapsedField;
			}
			set
			{
				this.timeElapsedField = value;
			}
		}

		// Token: 0x040001CF RID: 463
		private string ackCodeField;

		// Token: 0x040001D0 RID: 464
		private string responseTimeField;

		// Token: 0x040001D1 RID: 465
		private errorType errorField;

		// Token: 0x040001D2 RID: 466
		private string timeElapsedField;
	}
}
