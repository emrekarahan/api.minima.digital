﻿namespace ApiV2Client.GittiGidiyor.CategoryV2
{
	// Token: 0x02000006 RID: 6
	public interface ICategoryServiceV2 : IService
	{
		// Token: 0x0600000C RID: 12
		categorySpecServiceListResponse getRequiredCategorySpecs(string categoryCode, string lang);
	}
}
