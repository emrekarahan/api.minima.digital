﻿namespace ApiV2Client.GittiGidiyor.CategoryV2
{
	// Token: 0x02000007 RID: 7
	public class CategoryServiceV2 : ServiceClient<CategoryV2ServiceService>, ICategoryServiceV2, IService
	{
		// Token: 0x0600000D RID: 13 RVA: 0x0000222C File Offset: 0x0000042C
		private CategoryServiceV2()
		{
			base.configureNetworkCredentials(this.service);
		}

		// Token: 0x0600000E RID: 14 RVA: 0x0000224B File Offset: 0x0000044B
		public categorySpecServiceListResponse getRequiredCategorySpecs(string categoryCode, string lang)
		{
			return this.service.getRequiredCategorySpecs(categoryCode, lang);
		}

		// Token: 0x0600000F RID: 15 RVA: 0x0000225A File Offset: 0x0000045A
		public string getServiceName()
		{
			return this.service.getServiceName();
		}

		// Token: 0x17000002 RID: 2
		// (get) Token: 0x06000010 RID: 16 RVA: 0x00002268 File Offset: 0x00000468
		public static CategoryServiceV2 Instance
		{
			get
			{
				if (CategoryServiceV2.instance == null)
				{
					lock (CategoryServiceV2.lockObject)
					{
						if (CategoryServiceV2.instance == null)
						{
							CategoryServiceV2.instance = new CategoryServiceV2();
						}
					}
				}
				return CategoryServiceV2.instance;
			}
		}

		// Token: 0x04000005 RID: 5
		private static CategoryServiceV2 instance;

		// Token: 0x04000006 RID: 6
		private static object lockObject = new object();

		// Token: 0x04000007 RID: 7
		private CategoryV2ServiceService service = new CategoryV2ServiceService();
	}
}
