﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.CategoryV2
{
	// Token: 0x02000089 RID: 137
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	public class getServiceNameCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x060004EE RID: 1262 RVA: 0x0000757B File Offset: 0x0000577B
		internal getServiceNameCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x170001B4 RID: 436
		// (get) Token: 0x060004EF RID: 1263 RVA: 0x0000758E File Offset: 0x0000578E
		public string Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (string)this.results[0];
			}
		}

		// Token: 0x040001E2 RID: 482
		private object[] results;
	}
}
