﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.CategoryV2
{
	// Token: 0x02000084 RID: 132
	[XmlType(Namespace = "http://categoryv2.anonymous.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.7.3190.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[Serializable]
	public class specType
	{
		// Token: 0x170001AA RID: 426
		// (get) Token: 0x060004D0 RID: 1232 RVA: 0x000074AA File Offset: 0x000056AA
		// (set) Token: 0x060004D1 RID: 1233 RVA: 0x000074B2 File Offset: 0x000056B2
		[XmlAttribute]
		public string name
		{
			get
			{
				return this.nameField;
			}
			set
			{
				this.nameField = value;
			}
		}

		// Token: 0x170001AB RID: 427
		// (get) Token: 0x060004D2 RID: 1234 RVA: 0x000074BB File Offset: 0x000056BB
		// (set) Token: 0x060004D3 RID: 1235 RVA: 0x000074C3 File Offset: 0x000056C3
		[XmlAttribute]
		public string value
		{
			get
			{
				return this.valueField;
			}
			set
			{
				this.valueField = value;
			}
		}

		// Token: 0x170001AC RID: 428
		// (get) Token: 0x060004D4 RID: 1236 RVA: 0x000074CC File Offset: 0x000056CC
		// (set) Token: 0x060004D5 RID: 1237 RVA: 0x000074D4 File Offset: 0x000056D4
		[XmlAttribute]
		public string type
		{
			get
			{
				return this.typeField;
			}
			set
			{
				this.typeField = value;
			}
		}

		// Token: 0x170001AD RID: 429
		// (get) Token: 0x060004D6 RID: 1238 RVA: 0x000074DD File Offset: 0x000056DD
		// (set) Token: 0x060004D7 RID: 1239 RVA: 0x000074E5 File Offset: 0x000056E5
		[XmlAttribute]
		public bool required
		{
			get
			{
				return this.requiredField;
			}
			set
			{
				this.requiredField = value;
			}
		}

		// Token: 0x170001AE RID: 430
		// (get) Token: 0x060004D8 RID: 1240 RVA: 0x000074EE File Offset: 0x000056EE
		// (set) Token: 0x060004D9 RID: 1241 RVA: 0x000074F6 File Offset: 0x000056F6
		[XmlIgnore]
		public bool requiredSpecified
		{
			get
			{
				return this.requiredFieldSpecified;
			}
			set
			{
				this.requiredFieldSpecified = value;
			}
		}

		// Token: 0x040001D8 RID: 472
		private string nameField;

		// Token: 0x040001D9 RID: 473
		private string valueField;

		// Token: 0x040001DA RID: 474
		private string typeField;

		// Token: 0x040001DB RID: 475
		private bool requiredField;

		// Token: 0x040001DC RID: 476
		private bool requiredFieldSpecified;
	}
}
