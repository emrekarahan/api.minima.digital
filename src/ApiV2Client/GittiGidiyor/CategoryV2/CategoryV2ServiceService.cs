﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading;
using System.Web.Services;
using System.Web.Services.Description;
using System.Web.Services.Protocols;
using System.Xml.Serialization;
using ApiV2Client.Properties;

namespace ApiV2Client.GittiGidiyor.CategoryV2
{
	// Token: 0x02000081 RID: 129
	[XmlInclude(typeof(baseResponse))]
	[WebServiceBinding(Name = "CategoryV2ServiceBinding", Namespace = "http://categoryv2.anonymous.ws.listingapi.gg.com")]
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	public class CategoryV2ServiceService : SoapHttpClientProtocol
	{
		// Token: 0x060004A9 RID: 1193 RVA: 0x000070C7 File Offset: 0x000052C7
		public CategoryV2ServiceService()
		{
			this.Url = Settings.Default.ApiV2Client_GittiGidiyor_CategoryV2_CategoryV2ServiceService;
			if (this.IsLocalFileSystemWebService(this.Url))
			{
				this.UseDefaultCredentials = true;
				this.useDefaultCredentialsSetExplicitly = false;
				return;
			}
			this.useDefaultCredentialsSetExplicitly = true;
		}

		// Token: 0x1700019F RID: 415
		// (get) Token: 0x060004AA RID: 1194 RVA: 0x00007103 File Offset: 0x00005303
		// (set) Token: 0x060004AB RID: 1195 RVA: 0x0000710B File Offset: 0x0000530B
		public new string Url
		{
			get
			{
				return base.Url;
			}
			set
			{
				if (this.IsLocalFileSystemWebService(base.Url) && !this.useDefaultCredentialsSetExplicitly && !this.IsLocalFileSystemWebService(value))
				{
					base.UseDefaultCredentials = false;
				}
				base.Url = value;
			}
		}

		// Token: 0x170001A0 RID: 416
		// (get) Token: 0x060004AC RID: 1196 RVA: 0x0000713A File Offset: 0x0000533A
		// (set) Token: 0x060004AD RID: 1197 RVA: 0x00007142 File Offset: 0x00005342
		public new bool UseDefaultCredentials
		{
			get
			{
				return base.UseDefaultCredentials;
			}
			set
			{
				base.UseDefaultCredentials = value;
				this.useDefaultCredentialsSetExplicitly = true;
			}
		}

		// Token: 0x1400001E RID: 30
		// (add) Token: 0x060004AE RID: 1198 RVA: 0x00007154 File Offset: 0x00005354
		// (remove) Token: 0x060004AF RID: 1199 RVA: 0x0000718C File Offset: 0x0000538C
		public event getRequiredCategorySpecsCompletedEventHandler getRequiredCategorySpecsCompleted;

		// Token: 0x1400001F RID: 31
		// (add) Token: 0x060004B0 RID: 1200 RVA: 0x000071C4 File Offset: 0x000053C4
		// (remove) Token: 0x060004B1 RID: 1201 RVA: 0x000071FC File Offset: 0x000053FC
		public event getServiceNameCompletedEventHandler getServiceNameCompleted;

		// Token: 0x060004B2 RID: 1202 RVA: 0x00007234 File Offset: 0x00005434
		[SoapRpcMethod("", RequestNamespace = "http://categoryv2.anonymous.ws.listingapi.gg.com", ResponseNamespace = "http://categoryv2.anonymous.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public categorySpecServiceListResponse getRequiredCategorySpecs(string categoryCode, string lang)
		{
			object[] array = base.Invoke("getRequiredCategorySpecs", new object[]
			{
				categoryCode,
				lang
			});
			return (categorySpecServiceListResponse)array[0];
		}

		// Token: 0x060004B3 RID: 1203 RVA: 0x00007265 File Offset: 0x00005465
		public void getRequiredCategorySpecsAsync(string categoryCode, string lang)
		{
			this.getRequiredCategorySpecsAsync(categoryCode, lang, null);
		}

		// Token: 0x060004B4 RID: 1204 RVA: 0x00007270 File Offset: 0x00005470
		public void getRequiredCategorySpecsAsync(string categoryCode, string lang, object userState)
		{
			if (this.getRequiredCategorySpecsOperationCompleted == null)
			{
				this.getRequiredCategorySpecsOperationCompleted = new SendOrPostCallback(this.OngetRequiredCategorySpecsOperationCompleted);
			}
			base.InvokeAsync("getRequiredCategorySpecs", new object[]
			{
				categoryCode,
				lang
			}, this.getRequiredCategorySpecsOperationCompleted, userState);
		}

		// Token: 0x060004B5 RID: 1205 RVA: 0x000072BC File Offset: 0x000054BC
		private void OngetRequiredCategorySpecsOperationCompleted(object arg)
		{
			if (this.getRequiredCategorySpecsCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getRequiredCategorySpecsCompleted(this, new getRequiredCategorySpecsCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x060004B6 RID: 1206 RVA: 0x00007304 File Offset: 0x00005504
		[SoapRpcMethod("", RequestNamespace = "http://categoryv2.anonymous.ws.listingapi.gg.com", ResponseNamespace = "http://categoryv2.anonymous.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public string getServiceName()
		{
			object[] array = base.Invoke("getServiceName", new object[0]);
			return (string)array[0];
		}

		// Token: 0x060004B7 RID: 1207 RVA: 0x0000732B File Offset: 0x0000552B
		public void getServiceNameAsync()
		{
			this.getServiceNameAsync(null);
		}

		// Token: 0x060004B8 RID: 1208 RVA: 0x00007334 File Offset: 0x00005534
		public void getServiceNameAsync(object userState)
		{
			if (this.getServiceNameOperationCompleted == null)
			{
				this.getServiceNameOperationCompleted = new SendOrPostCallback(this.OngetServiceNameOperationCompleted);
			}
			base.InvokeAsync("getServiceName", new object[0], this.getServiceNameOperationCompleted, userState);
		}

		// Token: 0x060004B9 RID: 1209 RVA: 0x00007368 File Offset: 0x00005568
		private void OngetServiceNameOperationCompleted(object arg)
		{
			if (this.getServiceNameCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getServiceNameCompleted(this, new getServiceNameCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x060004BA RID: 1210 RVA: 0x000073AD File Offset: 0x000055AD
		public new void CancelAsync(object userState)
		{
			base.CancelAsync(userState);
		}

		// Token: 0x060004BB RID: 1211 RVA: 0x000073B8 File Offset: 0x000055B8
		private bool IsLocalFileSystemWebService(string url)
		{
			if (url == null || url == string.Empty)
			{
				return false;
			}
			Uri uri = new Uri(url);
			return uri.Port >= 1024 && string.Compare(uri.Host, "localHost", StringComparison.OrdinalIgnoreCase) == 0;
		}

		// Token: 0x040001CA RID: 458
		private SendOrPostCallback getRequiredCategorySpecsOperationCompleted;

		// Token: 0x040001CB RID: 459
		private SendOrPostCallback getServiceNameOperationCompleted;

		// Token: 0x040001CC RID: 460
		private bool useDefaultCredentialsSetExplicitly;
	}
}
