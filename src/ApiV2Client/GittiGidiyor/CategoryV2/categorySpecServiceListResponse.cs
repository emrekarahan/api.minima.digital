﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.CategoryV2
{
	// Token: 0x02000083 RID: 131
	[GeneratedCode("System.Xml", "4.7.3190.0")]
	[XmlType(Namespace = "http://categoryv2.anonymous.ws.listingapi.gg.com")]
	[DesignerCategory("code")]
	[DebuggerStepThrough]
	[Serializable]
	public class categorySpecServiceListResponse : baseResponse
	{
		// Token: 0x170001A5 RID: 421
		// (get) Token: 0x060004C5 RID: 1221 RVA: 0x0000744D File Offset: 0x0000564D
		// (set) Token: 0x060004C6 RID: 1222 RVA: 0x00007455 File Offset: 0x00005655
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int count
		{
			get
			{
				return this.countField;
			}
			set
			{
				this.countField = value;
			}
		}

		// Token: 0x170001A6 RID: 422
		// (get) Token: 0x060004C7 RID: 1223 RVA: 0x0000745E File Offset: 0x0000565E
		// (set) Token: 0x060004C8 RID: 1224 RVA: 0x00007466 File Offset: 0x00005666
		[XmlIgnore]
		public bool countSpecified
		{
			get
			{
				return this.countFieldSpecified;
			}
			set
			{
				this.countFieldSpecified = value;
			}
		}

		// Token: 0x170001A7 RID: 423
		// (get) Token: 0x060004C9 RID: 1225 RVA: 0x0000746F File Offset: 0x0000566F
		// (set) Token: 0x060004CA RID: 1226 RVA: 0x00007477 File Offset: 0x00005677
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public bool catalogRequired
		{
			get
			{
				return this.catalogRequiredField;
			}
			set
			{
				this.catalogRequiredField = value;
			}
		}

		// Token: 0x170001A8 RID: 424
		// (get) Token: 0x060004CB RID: 1227 RVA: 0x00007480 File Offset: 0x00005680
		// (set) Token: 0x060004CC RID: 1228 RVA: 0x00007488 File Offset: 0x00005688
		[XmlIgnore]
		public bool catalogRequiredSpecified
		{
			get
			{
				return this.catalogRequiredFieldSpecified;
			}
			set
			{
				this.catalogRequiredFieldSpecified = value;
			}
		}

		// Token: 0x170001A9 RID: 425
		// (get) Token: 0x060004CD RID: 1229 RVA: 0x00007491 File Offset: 0x00005691
		// (set) Token: 0x060004CE RID: 1230 RVA: 0x00007499 File Offset: 0x00005699
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		[XmlArrayItem("spec", Form = XmlSchemaForm.Unqualified)]
		public specType[] requiredSpecs
		{
			get
			{
				return this.requiredSpecsField;
			}
			set
			{
				this.requiredSpecsField = value;
			}
		}

		// Token: 0x040001D3 RID: 467
		private int countField;

		// Token: 0x040001D4 RID: 468
		private bool countFieldSpecified;

		// Token: 0x040001D5 RID: 469
		private bool catalogRequiredField;

		// Token: 0x040001D6 RID: 470
		private bool catalogRequiredFieldSpecified;

		// Token: 0x040001D7 RID: 471
		private specType[] requiredSpecsField;
	}
}
