﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.CategoryV2
{
	// Token: 0x02000087 RID: 135
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	public class getRequiredCategorySpecsCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x060004E8 RID: 1256 RVA: 0x00007553 File Offset: 0x00005753
		internal getRequiredCategorySpecsCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x170001B3 RID: 435
		// (get) Token: 0x060004E9 RID: 1257 RVA: 0x00007566 File Offset: 0x00005766
		public categorySpecServiceListResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (categorySpecServiceListResponse)this.results[0];
			}
		}

		// Token: 0x040001E1 RID: 481
		private object[] results;
	}
}
