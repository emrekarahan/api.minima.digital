﻿namespace ApiV2Client.GittiGidiyor
{
	// Token: 0x020001F0 RID: 496
	public class AuthConfig
	{
		// Token: 0x1700051C RID: 1308
		// (get) Token: 0x060010E8 RID: 4328 RVA: 0x00016C18 File Offset: 0x00014E18
		// (set) Token: 0x060010E9 RID: 4329 RVA: 0x00016C20 File Offset: 0x00014E20
		public string ApiKey
		{
			get => this.apiKey;
            set => this.apiKey = value;
        }

		// Token: 0x1700051D RID: 1309
		// (get) Token: 0x060010EA RID: 4330 RVA: 0x00016C29 File Offset: 0x00014E29
		// (set) Token: 0x060010EB RID: 4331 RVA: 0x00016C31 File Offset: 0x00014E31
		public string RoleName
		{
			get
			{
				return this.roleName;
			}
			set
			{
				this.roleName = value;
			}
		}

		// Token: 0x1700051E RID: 1310
		// (get) Token: 0x060010EC RID: 4332 RVA: 0x00016C3A File Offset: 0x00014E3A
		// (set) Token: 0x060010ED RID: 4333 RVA: 0x00016C42 File Offset: 0x00014E42
		public string RolePass
		{
			get
			{
				return this.rolePass;
			}
			set
			{
				this.rolePass = value;
			}
		}

		// Token: 0x1700051F RID: 1311
		// (get) Token: 0x060010EE RID: 4334 RVA: 0x00016C4B File Offset: 0x00014E4B
		// (set) Token: 0x060010EF RID: 4335 RVA: 0x00016C53 File Offset: 0x00014E53
		public string SecretKey
		{
			get
			{
				return this.secretKey;
			}
			set
			{
				this.secretKey = value;
			}
		}

		// Token: 0x04000627 RID: 1575
		private string apiKey;

		// Token: 0x04000628 RID: 1576
		private string roleName;

		// Token: 0x04000629 RID: 1577
		private string rolePass;

		// Token: 0x0400062A RID: 1578
		private string secretKey;
	}
}
