﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x0200013F RID: 319
	[DesignerCategory("code")]
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	[DebuggerStepThrough]
	public class cloneProductCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000C37 RID: 3127 RVA: 0x00011836 File Offset: 0x0000FA36
		internal cloneProductCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x170003E6 RID: 998
		// (get) Token: 0x06000C38 RID: 3128 RVA: 0x00011849 File Offset: 0x0000FA49
		public productServiceResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (productServiceResponse)this.results[0];
			}
		}

		// Token: 0x040004AC RID: 1196
		private object[] results;
	}
}
