﻿using System.CodeDom.Compiler;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x02000138 RID: 312
	// (Invoke) Token: 0x06000C22 RID: 3106
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	public delegate void calculatePriceForRevisionCompletedEventHandler(object sender, calculatePriceForRevisionCompletedEventArgs e);
}
