﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x02000128 RID: 296
	[GeneratedCode("System.Xml", "4.7.3190.0")]
	[DesignerCategory("code")]
	[DebuggerStepThrough]
	[XmlType(Namespace = "https://product.individual.ws.listingapi.gg.com")]
	[Serializable]
	public class productFinishReasonListResponse : baseResponse
	{
		// Token: 0x170003A8 RID: 936
		// (get) Token: 0x06000B9B RID: 2971 RVA: 0x00011353 File Offset: 0x0000F553
		// (set) Token: 0x06000B9C RID: 2972 RVA: 0x0001135B File Offset: 0x0000F55B
		[XmlArrayItem(Form = XmlSchemaForm.Unqualified)]
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		public productFinishReasonType[] productFinishReasonTypeList
		{
			get
			{
				return this.productFinishReasonTypeListField;
			}
			set
			{
				this.productFinishReasonTypeListField = value;
			}
		}

		// Token: 0x0400046E RID: 1134
		private productFinishReasonType[] productFinishReasonTypeListField;
	}
}
