﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x02000131 RID: 305
	[XmlType(Namespace = "https://product.individual.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.7.3190.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[Serializable]
	public class productServiceProcessResponse : baseResponse
	{
		// Token: 0x170003C0 RID: 960
		// (get) Token: 0x06000BD4 RID: 3028 RVA: 0x00011533 File Offset: 0x0000F733
		// (set) Token: 0x06000BD5 RID: 3029 RVA: 0x0001153B File Offset: 0x0000F73B
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		[XmlArrayItem("successfulProcess", Form = XmlSchemaForm.Unqualified)]
		public productSuccessProcessType[] successfulProcesses
		{
			get
			{
				return this.successfulProcessesField;
			}
			set
			{
				this.successfulProcessesField = value;
			}
		}

		// Token: 0x170003C1 RID: 961
		// (get) Token: 0x06000BD6 RID: 3030 RVA: 0x00011544 File Offset: 0x0000F744
		// (set) Token: 0x06000BD7 RID: 3031 RVA: 0x0001154C File Offset: 0x0000F74C
		[XmlArrayItem("failedProcess", Form = XmlSchemaForm.Unqualified)]
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		public productFailProcessType[] failedProcesses
		{
			get
			{
				return this.failedProcessesField;
			}
			set
			{
				this.failedProcessesField = value;
			}
		}

		// Token: 0x170003C2 RID: 962
		// (get) Token: 0x06000BD8 RID: 3032 RVA: 0x00011555 File Offset: 0x0000F755
		// (set) Token: 0x06000BD9 RID: 3033 RVA: 0x0001155D File Offset: 0x0000F75D
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string result
		{
			get
			{
				return this.resultField;
			}
			set
			{
				this.resultField = value;
			}
		}

		// Token: 0x04000486 RID: 1158
		private productSuccessProcessType[] successfulProcessesField;

		// Token: 0x04000487 RID: 1159
		private productFailProcessType[] failedProcessesField;

		// Token: 0x04000488 RID: 1160
		private string resultField;
	}
}
