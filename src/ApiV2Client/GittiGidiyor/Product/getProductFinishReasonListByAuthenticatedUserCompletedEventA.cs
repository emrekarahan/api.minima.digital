﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x02000151 RID: 337
	[DesignerCategory("code")]
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	[DebuggerStepThrough]
	public class getProductFinishReasonListByAuthenticatedUserCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000C6D RID: 3181 RVA: 0x0001199E File Offset: 0x0000FB9E
		internal getProductFinishReasonListByAuthenticatedUserCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x170003EF RID: 1007
		// (get) Token: 0x06000C6E RID: 3182 RVA: 0x000119B1 File Offset: 0x0000FBB1
		public productFinishReasonListResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (productFinishReasonListResponse)this.results[0];
			}
		}

		// Token: 0x040004B5 RID: 1205
		private object[] results;
	}
}
