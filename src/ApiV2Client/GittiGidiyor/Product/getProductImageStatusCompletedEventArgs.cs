﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x02000155 RID: 341
	[DesignerCategory("code")]
	[DebuggerStepThrough]
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	public class getProductImageStatusCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000C79 RID: 3193 RVA: 0x000119EE File Offset: 0x0000FBEE
		internal getProductImageStatusCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x170003F1 RID: 1009
		// (get) Token: 0x06000C7A RID: 3194 RVA: 0x00011A01 File Offset: 0x0000FC01
		public productServiceImageStatusResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (productServiceImageStatusResponse)this.results[0];
			}
		}

		// Token: 0x040004B7 RID: 1207
		private object[] results;
	}
}
