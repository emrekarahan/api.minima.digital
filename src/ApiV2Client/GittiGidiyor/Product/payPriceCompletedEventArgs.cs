﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x0200016D RID: 365
	[DesignerCategory("code")]
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	[DebuggerStepThrough]
	public class payPriceCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000CC1 RID: 3265 RVA: 0x00011BCE File Offset: 0x0000FDCE
		internal payPriceCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x170003FD RID: 1021
		// (get) Token: 0x06000CC2 RID: 3266 RVA: 0x00011BE1 File Offset: 0x0000FDE1
		public productServicePaymentResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (productServicePaymentResponse)this.results[0];
			}
		}

		// Token: 0x040004C3 RID: 1219
		private object[] results;
	}
}
