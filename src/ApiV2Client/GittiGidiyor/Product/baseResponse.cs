﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x02000111 RID: 273
	[XmlInclude(typeof(productServiceStockResponse))]
	[XmlInclude(typeof(productServiceDescResponse))]
	[XmlInclude(typeof(productServiceListResponse))]
	[XmlType(Namespace = "https://product.individual.ws.listingapi.gg.com")]
	[XmlInclude(typeof(productServiceImageStatusResponse))]
	[XmlInclude(typeof(productServiceStatusResponse))]
	[XmlInclude(typeof(productServicePaymentResponse))]
	[XmlInclude(typeof(itemIdDetailResponse))]
	[XmlInclude(typeof(productServicePriceResponse))]
	[XmlInclude(typeof(productServiceBooleanResponse))]
	[XmlInclude(typeof(productServiceVariantResponse))]
	[XmlInclude(typeof(productServiceSpecResponse))]
	[XmlInclude(typeof(productServiceProcessResponse))]
	[XmlInclude(typeof(productServiceResponse))]
	[XmlInclude(typeof(productServiceRetailResponse))]
	[XmlInclude(typeof(productServiceIdResponse))]
	[XmlInclude(typeof(productServiceDetailResponse))]
	[XmlInclude(typeof(productFinishReasonListResponse))]
	[GeneratedCode("System.Xml", "4.7.3190.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[Serializable]
	public class baseResponse
	{
		// Token: 0x170002FA RID: 762
		// (get) Token: 0x06000A29 RID: 2601 RVA: 0x00010715 File Offset: 0x0000E915
		// (set) Token: 0x06000A2A RID: 2602 RVA: 0x0001071D File Offset: 0x0000E91D
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string ackCode
		{
			get
			{
				return this.ackCodeField;
			}
			set
			{
				this.ackCodeField = value;
			}
		}

		// Token: 0x170002FB RID: 763
		// (get) Token: 0x06000A2B RID: 2603 RVA: 0x00010726 File Offset: 0x0000E926
		// (set) Token: 0x06000A2C RID: 2604 RVA: 0x0001072E File Offset: 0x0000E92E
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string responseTime
		{
			get
			{
				return this.responseTimeField;
			}
			set
			{
				this.responseTimeField = value;
			}
		}

		// Token: 0x170002FC RID: 764
		// (get) Token: 0x06000A2D RID: 2605 RVA: 0x00010737 File Offset: 0x0000E937
		// (set) Token: 0x06000A2E RID: 2606 RVA: 0x0001073F File Offset: 0x0000E93F
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public errorType error
		{
			get
			{
				return this.errorField;
			}
			set
			{
				this.errorField = value;
			}
		}

		// Token: 0x170002FD RID: 765
		// (get) Token: 0x06000A2F RID: 2607 RVA: 0x00010748 File Offset: 0x0000E948
		// (set) Token: 0x06000A30 RID: 2608 RVA: 0x00010750 File Offset: 0x0000E950
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string timeElapsed
		{
			get
			{
				return this.timeElapsedField;
			}
			set
			{
				this.timeElapsedField = value;
			}
		}

		// Token: 0x040003BC RID: 956
		private string ackCodeField;

		// Token: 0x040003BD RID: 957
		private string responseTimeField;

		// Token: 0x040003BE RID: 958
		private errorType errorField;

		// Token: 0x040003BF RID: 959
		private string timeElapsedField;
	}
}
