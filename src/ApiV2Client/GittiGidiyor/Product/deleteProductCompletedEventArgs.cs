﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x02000141 RID: 321
	[DesignerCategory("code")]
	[DebuggerStepThrough]
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	public class deleteProductCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000C3D RID: 3133 RVA: 0x0001185E File Offset: 0x0000FA5E
		internal deleteProductCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x170003E7 RID: 999
		// (get) Token: 0x06000C3E RID: 3134 RVA: 0x00011871 File Offset: 0x0000FA71
		public productServiceIdResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (productServiceIdResponse)this.results[0];
			}
		}

		// Token: 0x040004AD RID: 1197
		private object[] results;
	}
}
