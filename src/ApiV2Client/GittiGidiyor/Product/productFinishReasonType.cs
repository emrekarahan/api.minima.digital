﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x02000119 RID: 281
	[XmlType(Namespace = "https://product.individual.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.7.3190.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[Serializable]
	public class productFinishReasonType
	{
		// Token: 0x1700032B RID: 811
		// (get) Token: 0x06000A93 RID: 2707 RVA: 0x00010A96 File Offset: 0x0000EC96
		// (set) Token: 0x06000A94 RID: 2708 RVA: 0x00010A9E File Offset: 0x0000EC9E
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int productId
		{
			get
			{
				return this.productIdField;
			}
			set
			{
				this.productIdField = value;
			}
		}

		// Token: 0x1700032C RID: 812
		// (get) Token: 0x06000A95 RID: 2709 RVA: 0x00010AA7 File Offset: 0x0000ECA7
		// (set) Token: 0x06000A96 RID: 2710 RVA: 0x00010AAF File Offset: 0x0000ECAF
		[XmlIgnore]
		public bool productIdSpecified
		{
			get
			{
				return this.productIdFieldSpecified;
			}
			set
			{
				this.productIdFieldSpecified = value;
			}
		}

		// Token: 0x1700032D RID: 813
		// (get) Token: 0x06000A97 RID: 2711 RVA: 0x00010AB8 File Offset: 0x0000ECB8
		// (set) Token: 0x06000A98 RID: 2712 RVA: 0x00010AC0 File Offset: 0x0000ECC0
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public DateTime endDate
		{
			get
			{
				return this.endDateField;
			}
			set
			{
				this.endDateField = value;
			}
		}

		// Token: 0x1700032E RID: 814
		// (get) Token: 0x06000A99 RID: 2713 RVA: 0x00010AC9 File Offset: 0x0000ECC9
		// (set) Token: 0x06000A9A RID: 2714 RVA: 0x00010AD1 File Offset: 0x0000ECD1
		[XmlIgnore]
		public bool endDateSpecified
		{
			get
			{
				return this.endDateFieldSpecified;
			}
			set
			{
				this.endDateFieldSpecified = value;
			}
		}

		// Token: 0x1700032F RID: 815
		// (get) Token: 0x06000A9B RID: 2715 RVA: 0x00010ADA File Offset: 0x0000ECDA
		// (set) Token: 0x06000A9C RID: 2716 RVA: 0x00010AE2 File Offset: 0x0000ECE2
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string reason
		{
			get
			{
				return this.reasonField;
			}
			set
			{
				this.reasonField = value;
			}
		}

		// Token: 0x17000330 RID: 816
		// (get) Token: 0x06000A9D RID: 2717 RVA: 0x00010AEB File Offset: 0x0000ECEB
		// (set) Token: 0x06000A9E RID: 2718 RVA: 0x00010AF3 File Offset: 0x0000ECF3
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int memberId
		{
			get
			{
				return this.memberIdField;
			}
			set
			{
				this.memberIdField = value;
			}
		}

		// Token: 0x17000331 RID: 817
		// (get) Token: 0x06000A9F RID: 2719 RVA: 0x00010AFC File Offset: 0x0000ECFC
		// (set) Token: 0x06000AA0 RID: 2720 RVA: 0x00010B04 File Offset: 0x0000ED04
		[XmlIgnore]
		public bool memberIdSpecified
		{
			get
			{
				return this.memberIdFieldSpecified;
			}
			set
			{
				this.memberIdFieldSpecified = value;
			}
		}

		// Token: 0x17000332 RID: 818
		// (get) Token: 0x06000AA1 RID: 2721 RVA: 0x00010B0D File Offset: 0x0000ED0D
		// (set) Token: 0x06000AA2 RID: 2722 RVA: 0x00010B15 File Offset: 0x0000ED15
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public DateTime startDate
		{
			get
			{
				return this.startDateField;
			}
			set
			{
				this.startDateField = value;
			}
		}

		// Token: 0x17000333 RID: 819
		// (get) Token: 0x06000AA3 RID: 2723 RVA: 0x00010B1E File Offset: 0x0000ED1E
		// (set) Token: 0x06000AA4 RID: 2724 RVA: 0x00010B26 File Offset: 0x0000ED26
		[XmlIgnore]
		public bool startDateSpecified
		{
			get
			{
				return this.startDateFieldSpecified;
			}
			set
			{
				this.startDateFieldSpecified = value;
			}
		}

		// Token: 0x17000334 RID: 820
		// (get) Token: 0x06000AA5 RID: 2725 RVA: 0x00010B2F File Offset: 0x0000ED2F
		// (set) Token: 0x06000AA6 RID: 2726 RVA: 0x00010B37 File Offset: 0x0000ED37
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public bool reList
		{
			get
			{
				return this.reListField;
			}
			set
			{
				this.reListField = value;
			}
		}

		// Token: 0x040003ED RID: 1005
		private int productIdField;

		// Token: 0x040003EE RID: 1006
		private bool productIdFieldSpecified;

		// Token: 0x040003EF RID: 1007
		private DateTime endDateField;

		// Token: 0x040003F0 RID: 1008
		private bool endDateFieldSpecified;

		// Token: 0x040003F1 RID: 1009
		private string reasonField;

		// Token: 0x040003F2 RID: 1010
		private int memberIdField;

		// Token: 0x040003F3 RID: 1011
		private bool memberIdFieldSpecified;

		// Token: 0x040003F4 RID: 1012
		private DateTime startDateField;

		// Token: 0x040003F5 RID: 1013
		private bool startDateFieldSpecified;

		// Token: 0x040003F6 RID: 1014
		private bool reListField;
	}
}
