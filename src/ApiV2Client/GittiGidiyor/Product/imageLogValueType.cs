﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x0200011A RID: 282
	[DebuggerStepThrough]
	[GeneratedCode("System.Xml", "4.7.3190.0")]
	[XmlType(Namespace = "https://product.individual.ws.listingapi.gg.com")]
	[DesignerCategory("code")]
	[Serializable]
	public class imageLogValueType
	{
		// Token: 0x17000335 RID: 821
		// (get) Token: 0x06000AA8 RID: 2728 RVA: 0x00010B48 File Offset: 0x0000ED48
		// (set) Token: 0x06000AA9 RID: 2729 RVA: 0x00010B50 File Offset: 0x0000ED50
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int photoId
		{
			get
			{
				return this.photoIdField;
			}
			set
			{
				this.photoIdField = value;
			}
		}

		// Token: 0x17000336 RID: 822
		// (get) Token: 0x06000AAA RID: 2730 RVA: 0x00010B59 File Offset: 0x0000ED59
		// (set) Token: 0x06000AAB RID: 2731 RVA: 0x00010B61 File Offset: 0x0000ED61
		[XmlIgnore]
		public bool photoIdSpecified
		{
			get
			{
				return this.photoIdFieldSpecified;
			}
			set
			{
				this.photoIdFieldSpecified = value;
			}
		}

		// Token: 0x17000337 RID: 823
		// (get) Token: 0x06000AAC RID: 2732 RVA: 0x00010B6A File Offset: 0x0000ED6A
		// (set) Token: 0x06000AAD RID: 2733 RVA: 0x00010B72 File Offset: 0x0000ED72
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int variantValueId
		{
			get
			{
				return this.variantValueIdField;
			}
			set
			{
				this.variantValueIdField = value;
			}
		}

		// Token: 0x17000338 RID: 824
		// (get) Token: 0x06000AAE RID: 2734 RVA: 0x00010B7B File Offset: 0x0000ED7B
		// (set) Token: 0x06000AAF RID: 2735 RVA: 0x00010B83 File Offset: 0x0000ED83
		[XmlIgnore]
		public bool variantValueIdSpecified
		{
			get
			{
				return this.variantValueIdFieldSpecified;
			}
			set
			{
				this.variantValueIdFieldSpecified = value;
			}
		}

		// Token: 0x17000339 RID: 825
		// (get) Token: 0x06000AB0 RID: 2736 RVA: 0x00010B8C File Offset: 0x0000ED8C
		// (set) Token: 0x06000AB1 RID: 2737 RVA: 0x00010B94 File Offset: 0x0000ED94
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string photoUrl
		{
			get
			{
				return this.photoUrlField;
			}
			set
			{
				this.photoUrlField = value;
			}
		}

		// Token: 0x1700033A RID: 826
		// (get) Token: 0x06000AB2 RID: 2738 RVA: 0x00010B9D File Offset: 0x0000ED9D
		// (set) Token: 0x06000AB3 RID: 2739 RVA: 0x00010BA5 File Offset: 0x0000EDA5
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string errorType
		{
			get
			{
				return this.errorTypeField;
			}
			set
			{
				this.errorTypeField = value;
			}
		}

		// Token: 0x1700033B RID: 827
		// (get) Token: 0x06000AB4 RID: 2740 RVA: 0x00010BAE File Offset: 0x0000EDAE
		// (set) Token: 0x06000AB5 RID: 2741 RVA: 0x00010BB6 File Offset: 0x0000EDB6
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string errorMessage
		{
			get
			{
				return this.errorMessageField;
			}
			set
			{
				this.errorMessageField = value;
			}
		}

		// Token: 0x1700033C RID: 828
		// (get) Token: 0x06000AB6 RID: 2742 RVA: 0x00010BBF File Offset: 0x0000EDBF
		// (set) Token: 0x06000AB7 RID: 2743 RVA: 0x00010BC7 File Offset: 0x0000EDC7
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string processDate
		{
			get
			{
				return this.processDateField;
			}
			set
			{
				this.processDateField = value;
			}
		}

		// Token: 0x040003F7 RID: 1015
		private int photoIdField;

		// Token: 0x040003F8 RID: 1016
		private bool photoIdFieldSpecified;

		// Token: 0x040003F9 RID: 1017
		private int variantValueIdField;

		// Token: 0x040003FA RID: 1018
		private bool variantValueIdFieldSpecified;

		// Token: 0x040003FB RID: 1019
		private string photoUrlField;

		// Token: 0x040003FC RID: 1020
		private string errorTypeField;

		// Token: 0x040003FD RID: 1021
		private string errorMessageField;

		// Token: 0x040003FE RID: 1022
		private string processDateField;
	}
}
