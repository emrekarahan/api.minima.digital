﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x0200015F RID: 351
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	public class getProductsByIdsCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000C97 RID: 3223 RVA: 0x00011AB6 File Offset: 0x0000FCB6
		internal getProductsByIdsCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x170003F6 RID: 1014
		// (get) Token: 0x06000C98 RID: 3224 RVA: 0x00011AC9 File Offset: 0x0000FCC9
		public productServiceListResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (productServiceListResponse)this.results[0];
			}
		}

		// Token: 0x040004BC RID: 1212
		private object[] results;
	}
}
