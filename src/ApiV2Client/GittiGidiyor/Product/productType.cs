﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x02000126 RID: 294
	[DesignerCategory("code")]
	[DebuggerStepThrough]
	[XmlType(Namespace = "https://product.individual.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.7.3190.0")]
	[Serializable]
	public class productType
	{
		// Token: 0x17000375 RID: 885
		// (get) Token: 0x06000B33 RID: 2867 RVA: 0x00010FE0 File Offset: 0x0000F1E0
		// (set) Token: 0x06000B34 RID: 2868 RVA: 0x00010FE8 File Offset: 0x0000F1E8
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string categoryCode
		{
			get
			{
				return this.categoryCodeField;
			}
			set
			{
				this.categoryCodeField = value;
			}
		}

		// Token: 0x17000376 RID: 886
		// (get) Token: 0x06000B35 RID: 2869 RVA: 0x00010FF1 File Offset: 0x0000F1F1
		// (set) Token: 0x06000B36 RID: 2870 RVA: 0x00010FF9 File Offset: 0x0000F1F9
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int storeCategoryId
		{
			get
			{
				return this.storeCategoryIdField;
			}
			set
			{
				this.storeCategoryIdField = value;
			}
		}

		// Token: 0x17000377 RID: 887
		// (get) Token: 0x06000B37 RID: 2871 RVA: 0x00011002 File Offset: 0x0000F202
		// (set) Token: 0x06000B38 RID: 2872 RVA: 0x0001100A File Offset: 0x0000F20A
		[XmlIgnore]
		public bool storeCategoryIdSpecified
		{
			get
			{
				return this.storeCategoryIdFieldSpecified;
			}
			set
			{
				this.storeCategoryIdFieldSpecified = value;
			}
		}

		// Token: 0x17000378 RID: 888
		// (get) Token: 0x06000B39 RID: 2873 RVA: 0x00011013 File Offset: 0x0000F213
		// (set) Token: 0x06000B3A RID: 2874 RVA: 0x0001101B File Offset: 0x0000F21B
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string title
		{
			get
			{
				return this.titleField;
			}
			set
			{
				this.titleField = value;
			}
		}

		// Token: 0x17000379 RID: 889
		// (get) Token: 0x06000B3B RID: 2875 RVA: 0x00011024 File Offset: 0x0000F224
		// (set) Token: 0x06000B3C RID: 2876 RVA: 0x0001102C File Offset: 0x0000F22C
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string subtitle
		{
			get
			{
				return this.subtitleField;
			}
			set
			{
				this.subtitleField = value;
			}
		}

		// Token: 0x1700037A RID: 890
		// (get) Token: 0x06000B3D RID: 2877 RVA: 0x00011035 File Offset: 0x0000F235
		// (set) Token: 0x06000B3E RID: 2878 RVA: 0x0001103D File Offset: 0x0000F23D
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		[XmlArrayItem("spec", Form = XmlSchemaForm.Unqualified, IsNullable = false)]
		public specType[] specs
		{
			get
			{
				return this.specsField;
			}
			set
			{
				this.specsField = value;
			}
		}

		// Token: 0x1700037B RID: 891
		// (get) Token: 0x06000B3F RID: 2879 RVA: 0x00011046 File Offset: 0x0000F246
		// (set) Token: 0x06000B40 RID: 2880 RVA: 0x0001104E File Offset: 0x0000F24E
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		[XmlArrayItem("photo", Form = XmlSchemaForm.Unqualified, IsNullable = false)]
		public photoType[] photos
		{
			get
			{
				return this.photosField;
			}
			set
			{
				this.photosField = value;
			}
		}

		// Token: 0x1700037C RID: 892
		// (get) Token: 0x06000B41 RID: 2881 RVA: 0x00011057 File Offset: 0x0000F257
		// (set) Token: 0x06000B42 RID: 2882 RVA: 0x0001105F File Offset: 0x0000F25F
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int pageTemplate
		{
			get
			{
				return this.pageTemplateField;
			}
			set
			{
				this.pageTemplateField = value;
			}
		}

		// Token: 0x1700037D RID: 893
		// (get) Token: 0x06000B43 RID: 2883 RVA: 0x00011068 File Offset: 0x0000F268
		// (set) Token: 0x06000B44 RID: 2884 RVA: 0x00011070 File Offset: 0x0000F270
		[XmlIgnore]
		public bool pageTemplateSpecified
		{
			get
			{
				return this.pageTemplateFieldSpecified;
			}
			set
			{
				this.pageTemplateFieldSpecified = value;
			}
		}

		// Token: 0x1700037E RID: 894
		// (get) Token: 0x06000B45 RID: 2885 RVA: 0x00011079 File Offset: 0x0000F279
		// (set) Token: 0x06000B46 RID: 2886 RVA: 0x00011081 File Offset: 0x0000F281
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string description
		{
			get
			{
				return this.descriptionField;
			}
			set
			{
				this.descriptionField = value;
			}
		}

		// Token: 0x1700037F RID: 895
		// (get) Token: 0x06000B47 RID: 2887 RVA: 0x0001108A File Offset: 0x0000F28A
		// (set) Token: 0x06000B48 RID: 2888 RVA: 0x00011092 File Offset: 0x0000F292
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string startDate
		{
			get
			{
				return this.startDateField;
			}
			set
			{
				this.startDateField = value;
			}
		}

		// Token: 0x17000380 RID: 896
		// (get) Token: 0x06000B49 RID: 2889 RVA: 0x0001109B File Offset: 0x0000F29B
		// (set) Token: 0x06000B4A RID: 2890 RVA: 0x000110A3 File Offset: 0x0000F2A3
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int catalogId
		{
			get
			{
				return this.catalogIdField;
			}
			set
			{
				this.catalogIdField = value;
			}
		}

		// Token: 0x17000381 RID: 897
		// (get) Token: 0x06000B4B RID: 2891 RVA: 0x000110AC File Offset: 0x0000F2AC
		// (set) Token: 0x06000B4C RID: 2892 RVA: 0x000110B4 File Offset: 0x0000F2B4
		[XmlIgnore]
		public bool catalogIdSpecified
		{
			get
			{
				return this.catalogIdFieldSpecified;
			}
			set
			{
				this.catalogIdFieldSpecified = value;
			}
		}

		// Token: 0x17000382 RID: 898
		// (get) Token: 0x06000B4D RID: 2893 RVA: 0x000110BD File Offset: 0x0000F2BD
		// (set) Token: 0x06000B4E RID: 2894 RVA: 0x000110C5 File Offset: 0x0000F2C5
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int newCatalogId
		{
			get
			{
				return this.newCatalogIdField;
			}
			set
			{
				this.newCatalogIdField = value;
			}
		}

		// Token: 0x17000383 RID: 899
		// (get) Token: 0x06000B4F RID: 2895 RVA: 0x000110CE File Offset: 0x0000F2CE
		// (set) Token: 0x06000B50 RID: 2896 RVA: 0x000110D6 File Offset: 0x0000F2D6
		[XmlIgnore]
		public bool newCatalogIdSpecified
		{
			get
			{
				return this.newCatalogIdFieldSpecified;
			}
			set
			{
				this.newCatalogIdFieldSpecified = value;
			}
		}

		// Token: 0x17000384 RID: 900
		// (get) Token: 0x06000B51 RID: 2897 RVA: 0x000110DF File Offset: 0x0000F2DF
		// (set) Token: 0x06000B52 RID: 2898 RVA: 0x000110E7 File Offset: 0x0000F2E7
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int catalogDetail
		{
			get
			{
				return this.catalogDetailField;
			}
			set
			{
				this.catalogDetailField = value;
			}
		}

		// Token: 0x17000385 RID: 901
		// (get) Token: 0x06000B53 RID: 2899 RVA: 0x000110F0 File Offset: 0x0000F2F0
		// (set) Token: 0x06000B54 RID: 2900 RVA: 0x000110F8 File Offset: 0x0000F2F8
		[XmlIgnore]
		public bool catalogDetailSpecified
		{
			get
			{
				return this.catalogDetailFieldSpecified;
			}
			set
			{
				this.catalogDetailFieldSpecified = value;
			}
		}

		// Token: 0x17000386 RID: 902
		// (get) Token: 0x06000B55 RID: 2901 RVA: 0x00011101 File Offset: 0x0000F301
		// (set) Token: 0x06000B56 RID: 2902 RVA: 0x00011109 File Offset: 0x0000F309
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string catalogFilter
		{
			get
			{
				return this.catalogFilterField;
			}
			set
			{
				this.catalogFilterField = value;
			}
		}

		// Token: 0x17000387 RID: 903
		// (get) Token: 0x06000B57 RID: 2903 RVA: 0x00011112 File Offset: 0x0000F312
		// (set) Token: 0x06000B58 RID: 2904 RVA: 0x0001111A File Offset: 0x0000F31A
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string format
		{
			get
			{
				return this.formatField;
			}
			set
			{
				this.formatField = value;
			}
		}

		// Token: 0x17000388 RID: 904
		// (get) Token: 0x06000B59 RID: 2905 RVA: 0x00011123 File Offset: 0x0000F323
		// (set) Token: 0x06000B5A RID: 2906 RVA: 0x0001112B File Offset: 0x0000F32B
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public double startPrice
		{
			get
			{
				return this.startPriceField;
			}
			set
			{
				this.startPriceField = value;
			}
		}

		// Token: 0x17000389 RID: 905
		// (get) Token: 0x06000B5B RID: 2907 RVA: 0x00011134 File Offset: 0x0000F334
		// (set) Token: 0x06000B5C RID: 2908 RVA: 0x0001113C File Offset: 0x0000F33C
		[XmlIgnore]
		public bool startPriceSpecified
		{
			get
			{
				return this.startPriceFieldSpecified;
			}
			set
			{
				this.startPriceFieldSpecified = value;
			}
		}

		// Token: 0x1700038A RID: 906
		// (get) Token: 0x06000B5D RID: 2909 RVA: 0x00011145 File Offset: 0x0000F345
		// (set) Token: 0x06000B5E RID: 2910 RVA: 0x0001114D File Offset: 0x0000F34D
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public double buyNowPrice
		{
			get
			{
				return this.buyNowPriceField;
			}
			set
			{
				this.buyNowPriceField = value;
			}
		}

		// Token: 0x1700038B RID: 907
		// (get) Token: 0x06000B5F RID: 2911 RVA: 0x00011156 File Offset: 0x0000F356
		// (set) Token: 0x06000B60 RID: 2912 RVA: 0x0001115E File Offset: 0x0000F35E
		[XmlIgnore]
		public bool buyNowPriceSpecified
		{
			get
			{
				return this.buyNowPriceFieldSpecified;
			}
			set
			{
				this.buyNowPriceFieldSpecified = value;
			}
		}

		// Token: 0x1700038C RID: 908
		// (get) Token: 0x06000B61 RID: 2913 RVA: 0x00011167 File Offset: 0x0000F367
		// (set) Token: 0x06000B62 RID: 2914 RVA: 0x0001116F File Offset: 0x0000F36F
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public double netEarning
		{
			get
			{
				return this.netEarningField;
			}
			set
			{
				this.netEarningField = value;
			}
		}

		// Token: 0x1700038D RID: 909
		// (get) Token: 0x06000B63 RID: 2915 RVA: 0x00011178 File Offset: 0x0000F378
		// (set) Token: 0x06000B64 RID: 2916 RVA: 0x00011180 File Offset: 0x0000F380
		[XmlIgnore]
		public bool netEarningSpecified
		{
			get
			{
				return this.netEarningFieldSpecified;
			}
			set
			{
				this.netEarningFieldSpecified = value;
			}
		}

		// Token: 0x1700038E RID: 910
		// (get) Token: 0x06000B65 RID: 2917 RVA: 0x00011189 File Offset: 0x0000F389
		// (set) Token: 0x06000B66 RID: 2918 RVA: 0x00011191 File Offset: 0x0000F391
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int listingDays
		{
			get
			{
				return this.listingDaysField;
			}
			set
			{
				this.listingDaysField = value;
			}
		}

		// Token: 0x1700038F RID: 911
		// (get) Token: 0x06000B67 RID: 2919 RVA: 0x0001119A File Offset: 0x0000F39A
		// (set) Token: 0x06000B68 RID: 2920 RVA: 0x000111A2 File Offset: 0x0000F3A2
		[XmlIgnore]
		public bool listingDaysSpecified
		{
			get
			{
				return this.listingDaysFieldSpecified;
			}
			set
			{
				this.listingDaysFieldSpecified = value;
			}
		}

		// Token: 0x17000390 RID: 912
		// (get) Token: 0x06000B69 RID: 2921 RVA: 0x000111AB File Offset: 0x0000F3AB
		// (set) Token: 0x06000B6A RID: 2922 RVA: 0x000111B3 File Offset: 0x0000F3B3
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int productCount
		{
			get
			{
				return this.productCountField;
			}
			set
			{
				this.productCountField = value;
			}
		}

		// Token: 0x17000391 RID: 913
		// (get) Token: 0x06000B6B RID: 2923 RVA: 0x000111BC File Offset: 0x0000F3BC
		// (set) Token: 0x06000B6C RID: 2924 RVA: 0x000111C4 File Offset: 0x0000F3C4
		[XmlIgnore]
		public bool productCountSpecified
		{
			get
			{
				return this.productCountFieldSpecified;
			}
			set
			{
				this.productCountFieldSpecified = value;
			}
		}

		// Token: 0x17000392 RID: 914
		// (get) Token: 0x06000B6D RID: 2925 RVA: 0x000111CD File Offset: 0x0000F3CD
		// (set) Token: 0x06000B6E RID: 2926 RVA: 0x000111D5 File Offset: 0x0000F3D5
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public cargoDetailType cargoDetail
		{
			get
			{
				return this.cargoDetailField;
			}
			set
			{
				this.cargoDetailField = value;
			}
		}

		// Token: 0x17000393 RID: 915
		// (get) Token: 0x06000B6F RID: 2927 RVA: 0x000111DE File Offset: 0x0000F3DE
		// (set) Token: 0x06000B70 RID: 2928 RVA: 0x000111E6 File Offset: 0x0000F3E6
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public bool affiliateOption
		{
			get
			{
				return this.affiliateOptionField;
			}
			set
			{
				this.affiliateOptionField = value;
			}
		}

		// Token: 0x17000394 RID: 916
		// (get) Token: 0x06000B71 RID: 2929 RVA: 0x000111EF File Offset: 0x0000F3EF
		// (set) Token: 0x06000B72 RID: 2930 RVA: 0x000111F7 File Offset: 0x0000F3F7
		[XmlIgnore]
		public bool affiliateOptionSpecified
		{
			get
			{
				return this.affiliateOptionFieldSpecified;
			}
			set
			{
				this.affiliateOptionFieldSpecified = value;
			}
		}

		// Token: 0x17000395 RID: 917
		// (get) Token: 0x06000B73 RID: 2931 RVA: 0x00011200 File Offset: 0x0000F400
		// (set) Token: 0x06000B74 RID: 2932 RVA: 0x00011208 File Offset: 0x0000F408
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public bool boldOption
		{
			get
			{
				return this.boldOptionField;
			}
			set
			{
				this.boldOptionField = value;
			}
		}

		// Token: 0x17000396 RID: 918
		// (get) Token: 0x06000B75 RID: 2933 RVA: 0x00011211 File Offset: 0x0000F411
		// (set) Token: 0x06000B76 RID: 2934 RVA: 0x00011219 File Offset: 0x0000F419
		[XmlIgnore]
		public bool boldOptionSpecified
		{
			get
			{
				return this.boldOptionFieldSpecified;
			}
			set
			{
				this.boldOptionFieldSpecified = value;
			}
		}

		// Token: 0x17000397 RID: 919
		// (get) Token: 0x06000B77 RID: 2935 RVA: 0x00011222 File Offset: 0x0000F422
		// (set) Token: 0x06000B78 RID: 2936 RVA: 0x0001122A File Offset: 0x0000F42A
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public bool catalogOption
		{
			get
			{
				return this.catalogOptionField;
			}
			set
			{
				this.catalogOptionField = value;
			}
		}

		// Token: 0x17000398 RID: 920
		// (get) Token: 0x06000B79 RID: 2937 RVA: 0x00011233 File Offset: 0x0000F433
		// (set) Token: 0x06000B7A RID: 2938 RVA: 0x0001123B File Offset: 0x0000F43B
		[XmlIgnore]
		public bool catalogOptionSpecified
		{
			get
			{
				return this.catalogOptionFieldSpecified;
			}
			set
			{
				this.catalogOptionFieldSpecified = value;
			}
		}

		// Token: 0x17000399 RID: 921
		// (get) Token: 0x06000B7B RID: 2939 RVA: 0x00011244 File Offset: 0x0000F444
		// (set) Token: 0x06000B7C RID: 2940 RVA: 0x0001124C File Offset: 0x0000F44C
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public bool vitrineOption
		{
			get
			{
				return this.vitrineOptionField;
			}
			set
			{
				this.vitrineOptionField = value;
			}
		}

		// Token: 0x1700039A RID: 922
		// (get) Token: 0x06000B7D RID: 2941 RVA: 0x00011255 File Offset: 0x0000F455
		// (set) Token: 0x06000B7E RID: 2942 RVA: 0x0001125D File Offset: 0x0000F45D
		[XmlIgnore]
		public bool vitrineOptionSpecified
		{
			get
			{
				return this.vitrineOptionFieldSpecified;
			}
			set
			{
				this.vitrineOptionFieldSpecified = value;
			}
		}

		// Token: 0x1700039B RID: 923
		// (get) Token: 0x06000B7F RID: 2943 RVA: 0x00011266 File Offset: 0x0000F466
		// (set) Token: 0x06000B80 RID: 2944 RVA: 0x0001126E File Offset: 0x0000F46E
		[XmlArrayItem("variantGroup", Form = XmlSchemaForm.Unqualified, IsNullable = false)]
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		public variantGroupType[] variantGroups
		{
			get
			{
				return this.variantGroupsField;
			}
			set
			{
				this.variantGroupsField = value;
			}
		}

		// Token: 0x1700039C RID: 924
		// (get) Token: 0x06000B81 RID: 2945 RVA: 0x00011277 File Offset: 0x0000F477
		// (set) Token: 0x06000B82 RID: 2946 RVA: 0x0001127F File Offset: 0x0000F47F
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int auctionProfilePercentage
		{
			get
			{
				return this.auctionProfilePercentageField;
			}
			set
			{
				this.auctionProfilePercentageField = value;
			}
		}

		// Token: 0x1700039D RID: 925
		// (get) Token: 0x06000B83 RID: 2947 RVA: 0x00011288 File Offset: 0x0000F488
		// (set) Token: 0x06000B84 RID: 2948 RVA: 0x00011290 File Offset: 0x0000F490
		[XmlIgnore]
		public bool auctionProfilePercentageSpecified
		{
			get
			{
				return this.auctionProfilePercentageFieldSpecified;
			}
			set
			{
				this.auctionProfilePercentageFieldSpecified = value;
			}
		}

		// Token: 0x1700039E RID: 926
		// (get) Token: 0x06000B85 RID: 2949 RVA: 0x00011299 File Offset: 0x0000F499
		// (set) Token: 0x06000B86 RID: 2950 RVA: 0x000112A1 File Offset: 0x0000F4A1
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public double marketPrice
		{
			get
			{
				return this.marketPriceField;
			}
			set
			{
				this.marketPriceField = value;
			}
		}

		// Token: 0x1700039F RID: 927
		// (get) Token: 0x06000B87 RID: 2951 RVA: 0x000112AA File Offset: 0x0000F4AA
		// (set) Token: 0x06000B88 RID: 2952 RVA: 0x000112B2 File Offset: 0x0000F4B2
		[XmlIgnore]
		public bool marketPriceSpecified
		{
			get
			{
				return this.marketPriceFieldSpecified;
			}
			set
			{
				this.marketPriceFieldSpecified = value;
			}
		}

		// Token: 0x170003A0 RID: 928
		// (get) Token: 0x06000B89 RID: 2953 RVA: 0x000112BB File Offset: 0x0000F4BB
		// (set) Token: 0x06000B8A RID: 2954 RVA: 0x000112C3 File Offset: 0x0000F4C3
		[XmlElement(Form = XmlSchemaForm.Unqualified, DataType = "integer")]
		public string globalTradeItemNo
		{
			get
			{
				return this.globalTradeItemNoField;
			}
			set
			{
				this.globalTradeItemNoField = value;
			}
		}

		// Token: 0x170003A1 RID: 929
		// (get) Token: 0x06000B8B RID: 2955 RVA: 0x000112CC File Offset: 0x0000F4CC
		// (set) Token: 0x06000B8C RID: 2956 RVA: 0x000112D4 File Offset: 0x0000F4D4
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string manufacturerPartNo
		{
			get
			{
				return this.manufacturerPartNoField;
			}
			set
			{
				this.manufacturerPartNoField = value;
			}
		}

		// Token: 0x0400043B RID: 1083
		private string categoryCodeField;

		// Token: 0x0400043C RID: 1084
		private int storeCategoryIdField;

		// Token: 0x0400043D RID: 1085
		private bool storeCategoryIdFieldSpecified;

		// Token: 0x0400043E RID: 1086
		private string titleField;

		// Token: 0x0400043F RID: 1087
		private string subtitleField;

		// Token: 0x04000440 RID: 1088
		private specType[] specsField;

		// Token: 0x04000441 RID: 1089
		private photoType[] photosField;

		// Token: 0x04000442 RID: 1090
		private int pageTemplateField;

		// Token: 0x04000443 RID: 1091
		private bool pageTemplateFieldSpecified;

		// Token: 0x04000444 RID: 1092
		private string descriptionField;

		// Token: 0x04000445 RID: 1093
		private string startDateField;

		// Token: 0x04000446 RID: 1094
		private int catalogIdField;

		// Token: 0x04000447 RID: 1095
		private bool catalogIdFieldSpecified;

		// Token: 0x04000448 RID: 1096
		private int newCatalogIdField;

		// Token: 0x04000449 RID: 1097
		private bool newCatalogIdFieldSpecified;

		// Token: 0x0400044A RID: 1098
		private int catalogDetailField;

		// Token: 0x0400044B RID: 1099
		private bool catalogDetailFieldSpecified;

		// Token: 0x0400044C RID: 1100
		private string catalogFilterField;

		// Token: 0x0400044D RID: 1101
		private string formatField;

		// Token: 0x0400044E RID: 1102
		private double startPriceField;

		// Token: 0x0400044F RID: 1103
		private bool startPriceFieldSpecified;

		// Token: 0x04000450 RID: 1104
		private double buyNowPriceField;

		// Token: 0x04000451 RID: 1105
		private bool buyNowPriceFieldSpecified;

		// Token: 0x04000452 RID: 1106
		private double netEarningField;

		// Token: 0x04000453 RID: 1107
		private bool netEarningFieldSpecified;

		// Token: 0x04000454 RID: 1108
		private int listingDaysField;

		// Token: 0x04000455 RID: 1109
		private bool listingDaysFieldSpecified;

		// Token: 0x04000456 RID: 1110
		private int productCountField;

		// Token: 0x04000457 RID: 1111
		private bool productCountFieldSpecified;

		// Token: 0x04000458 RID: 1112
		private cargoDetailType cargoDetailField;

		// Token: 0x04000459 RID: 1113
		private bool affiliateOptionField;

		// Token: 0x0400045A RID: 1114
		private bool affiliateOptionFieldSpecified;

		// Token: 0x0400045B RID: 1115
		private bool boldOptionField;

		// Token: 0x0400045C RID: 1116
		private bool boldOptionFieldSpecified;

		// Token: 0x0400045D RID: 1117
		private bool catalogOptionField;

		// Token: 0x0400045E RID: 1118
		private bool catalogOptionFieldSpecified;

		// Token: 0x0400045F RID: 1119
		private bool vitrineOptionField;

		// Token: 0x04000460 RID: 1120
		private bool vitrineOptionFieldSpecified;

		// Token: 0x04000461 RID: 1121
		private variantGroupType[] variantGroupsField;

		// Token: 0x04000462 RID: 1122
		private int auctionProfilePercentageField;

		// Token: 0x04000463 RID: 1123
		private bool auctionProfilePercentageFieldSpecified;

		// Token: 0x04000464 RID: 1124
		private double marketPriceField;

		// Token: 0x04000465 RID: 1125
		private bool marketPriceFieldSpecified;

		// Token: 0x04000466 RID: 1126
		private string globalTradeItemNoField;

		// Token: 0x04000467 RID: 1127
		private string manufacturerPartNoField;
	}
}
