﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x0200014F RID: 335
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	public class getProductDescriptionCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000C67 RID: 3175 RVA: 0x00011976 File Offset: 0x0000FB76
		internal getProductDescriptionCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x170003EE RID: 1006
		// (get) Token: 0x06000C68 RID: 3176 RVA: 0x00011989 File Offset: 0x0000FB89
		public productServiceDescResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (productServiceDescResponse)this.results[0];
			}
		}

		// Token: 0x040004B4 RID: 1204
		private object[] results;
	}
}
