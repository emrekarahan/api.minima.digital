﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading;
using System.Web.Services;
using System.Web.Services.Description;
using System.Web.Services.Protocols;
using System.Xml.Schema;
using System.Xml.Serialization;
using ApiV2Client.Properties;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x02000110 RID: 272
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	[DesignerCategory("code")]
	[DebuggerStepThrough]
	[WebServiceBinding(Name = "IndividualProductServiceBinding", Namespace = "https://product.individual.ws.listingapi.gg.com")]
	[XmlInclude(typeof(baseResponse))]
	[XmlInclude(typeof(productFinishReasonType[]))]
	public class IndividualProductServiceService : SoapHttpClientProtocol
	{
		// Token: 0x0600093E RID: 2366 RVA: 0x0000CA8B File Offset: 0x0000AC8B
		public IndividualProductServiceService()
		{
			this.Url = Settings.Default.ApiV2Client_GittiGidiyor_Product_IndividualProductServiceService;
			if (this.IsLocalFileSystemWebService(this.Url))
			{
				this.UseDefaultCredentials = true;
				this.useDefaultCredentialsSetExplicitly = false;
				return;
			}
			this.useDefaultCredentialsSetExplicitly = true;
		}

		// Token: 0x170002F8 RID: 760
		// (get) Token: 0x0600093F RID: 2367 RVA: 0x0000CAC7 File Offset: 0x0000ACC7
		// (set) Token: 0x06000940 RID: 2368 RVA: 0x0000CACF File Offset: 0x0000ACCF
		public new string Url
		{
			get
			{
				return base.Url;
			}
			set
			{
				if (this.IsLocalFileSystemWebService(base.Url) && !this.useDefaultCredentialsSetExplicitly && !this.IsLocalFileSystemWebService(value))
				{
					base.UseDefaultCredentials = false;
				}
				base.Url = value;
			}
		}

		// Token: 0x170002F9 RID: 761
		// (get) Token: 0x06000941 RID: 2369 RVA: 0x0000CAFE File Offset: 0x0000ACFE
		// (set) Token: 0x06000942 RID: 2370 RVA: 0x0000CB06 File Offset: 0x0000AD06
		public new bool UseDefaultCredentials
		{
			get
			{
				return base.UseDefaultCredentials;
			}
			set
			{
				base.UseDefaultCredentials = value;
				this.useDefaultCredentialsSetExplicitly = true;
			}
		}

		// Token: 0x14000047 RID: 71
		// (add) Token: 0x06000943 RID: 2371 RVA: 0x0000CB18 File Offset: 0x0000AD18
		// (remove) Token: 0x06000944 RID: 2372 RVA: 0x0000CB50 File Offset: 0x0000AD50
		public event calculatePriceForRevisionCompletedEventHandler calculatePriceForRevisionCompleted;

		// Token: 0x14000048 RID: 72
		// (add) Token: 0x06000945 RID: 2373 RVA: 0x0000CB88 File Offset: 0x0000AD88
		// (remove) Token: 0x06000946 RID: 2374 RVA: 0x0000CBC0 File Offset: 0x0000ADC0
		public event calculatePriceForShoppingCartCompletedEventHandler calculatePriceForShoppingCartCompleted;

		// Token: 0x14000049 RID: 73
		// (add) Token: 0x06000947 RID: 2375 RVA: 0x0000CBF8 File Offset: 0x0000ADF8
		// (remove) Token: 0x06000948 RID: 2376 RVA: 0x0000CC30 File Offset: 0x0000AE30
		public event checkForItemIdCompletedEventHandler checkForItemIdCompleted;

		// Token: 0x1400004A RID: 74
		// (add) Token: 0x06000949 RID: 2377 RVA: 0x0000CC68 File Offset: 0x0000AE68
		// (remove) Token: 0x0600094A RID: 2378 RVA: 0x0000CCA0 File Offset: 0x0000AEA0
		public event cloneProductCompletedEventHandler cloneProductCompleted;

		// Token: 0x1400004B RID: 75
		// (add) Token: 0x0600094B RID: 2379 RVA: 0x0000CCD8 File Offset: 0x0000AED8
		// (remove) Token: 0x0600094C RID: 2380 RVA: 0x0000CD10 File Offset: 0x0000AF10
		public event deleteProductCompletedEventHandler deleteProductCompleted;

		// Token: 0x1400004C RID: 76
		// (add) Token: 0x0600094D RID: 2381 RVA: 0x0000CD48 File Offset: 0x0000AF48
		// (remove) Token: 0x0600094E RID: 2382 RVA: 0x0000CD80 File Offset: 0x0000AF80
		public event deleteProductsCompletedEventHandler deleteProductsCompleted;

		// Token: 0x1400004D RID: 77
		// (add) Token: 0x0600094F RID: 2383 RVA: 0x0000CDB8 File Offset: 0x0000AFB8
		// (remove) Token: 0x06000950 RID: 2384 RVA: 0x0000CDF0 File Offset: 0x0000AFF0
		public event finishEarlyCompletedEventHandler finishEarlyCompleted;

		// Token: 0x1400004E RID: 78
		// (add) Token: 0x06000951 RID: 2385 RVA: 0x0000CE28 File Offset: 0x0000B028
		// (remove) Token: 0x06000952 RID: 2386 RVA: 0x0000CE60 File Offset: 0x0000B060
		public event finishEarlyProductsCompletedEventHandler finishEarlyProductsCompleted;

		// Token: 0x1400004F RID: 79
		// (add) Token: 0x06000953 RID: 2387 RVA: 0x0000CE98 File Offset: 0x0000B098
		// (remove) Token: 0x06000954 RID: 2388 RVA: 0x0000CED0 File Offset: 0x0000B0D0
		public event getItemIdDetailsCompletedEventHandler getItemIdDetailsCompleted;

		// Token: 0x14000050 RID: 80
		// (add) Token: 0x06000955 RID: 2389 RVA: 0x0000CF08 File Offset: 0x0000B108
		// (remove) Token: 0x06000956 RID: 2390 RVA: 0x0000CF40 File Offset: 0x0000B140
		public event getNewlyListedProductIdListCompletedEventHandler getNewlyListedProductIdListCompleted;

		// Token: 0x14000051 RID: 81
		// (add) Token: 0x06000957 RID: 2391 RVA: 0x0000CF78 File Offset: 0x0000B178
		// (remove) Token: 0x06000958 RID: 2392 RVA: 0x0000CFB0 File Offset: 0x0000B1B0
		public event getProductCompletedEventHandler getProductCompleted;

		// Token: 0x14000052 RID: 82
		// (add) Token: 0x06000959 RID: 2393 RVA: 0x0000CFE8 File Offset: 0x0000B1E8
		// (remove) Token: 0x0600095A RID: 2394 RVA: 0x0000D020 File Offset: 0x0000B220
		public event getProductDescriptionCompletedEventHandler getProductDescriptionCompleted;

		// Token: 0x14000053 RID: 83
		// (add) Token: 0x0600095B RID: 2395 RVA: 0x0000D058 File Offset: 0x0000B258
		// (remove) Token: 0x0600095C RID: 2396 RVA: 0x0000D090 File Offset: 0x0000B290
		public event getProductFinishReasonListByAuthenticatedUserCompletedEventHandler getProductFinishReasonListByAuthenticatedUserCompleted;

		// Token: 0x14000054 RID: 84
		// (add) Token: 0x0600095D RID: 2397 RVA: 0x0000D0C8 File Offset: 0x0000B2C8
		// (remove) Token: 0x0600095E RID: 2398 RVA: 0x0000D100 File Offset: 0x0000B300
		public event getProductIdsCompletedEventHandler getProductIdsCompleted;

		// Token: 0x14000055 RID: 85
		// (add) Token: 0x0600095F RID: 2399 RVA: 0x0000D138 File Offset: 0x0000B338
		// (remove) Token: 0x06000960 RID: 2400 RVA: 0x0000D170 File Offset: 0x0000B370
		public event getProductImageStatusCompletedEventHandler getProductImageStatusCompleted;

		// Token: 0x14000056 RID: 86
		// (add) Token: 0x06000961 RID: 2401 RVA: 0x0000D1A8 File Offset: 0x0000B3A8
		// (remove) Token: 0x06000962 RID: 2402 RVA: 0x0000D1E0 File Offset: 0x0000B3E0
		public event getProductSpecsCompletedEventHandler getProductSpecsCompleted;

		// Token: 0x14000057 RID: 87
		// (add) Token: 0x06000963 RID: 2403 RVA: 0x0000D218 File Offset: 0x0000B418
		// (remove) Token: 0x06000964 RID: 2404 RVA: 0x0000D250 File Offset: 0x0000B450
		public event getProductStatusesCompletedEventHandler getProductStatusesCompleted;

		// Token: 0x14000058 RID: 88
		// (add) Token: 0x06000965 RID: 2405 RVA: 0x0000D288 File Offset: 0x0000B488
		// (remove) Token: 0x06000966 RID: 2406 RVA: 0x0000D2C0 File Offset: 0x0000B4C0
		public event getProductVariantsCompletedEventHandler getProductVariantsCompleted;

		// Token: 0x14000059 RID: 89
		// (add) Token: 0x06000967 RID: 2407 RVA: 0x0000D2F8 File Offset: 0x0000B4F8
		// (remove) Token: 0x06000968 RID: 2408 RVA: 0x0000D330 File Offset: 0x0000B530
		public event getProductsCompletedEventHandler getProductsCompleted;

		// Token: 0x1400005A RID: 90
		// (add) Token: 0x06000969 RID: 2409 RVA: 0x0000D368 File Offset: 0x0000B568
		// (remove) Token: 0x0600096A RID: 2410 RVA: 0x0000D3A0 File Offset: 0x0000B5A0
		public event getProductsByIdsCompletedEventHandler getProductsByIdsCompleted;

		// Token: 0x1400005B RID: 91
		// (add) Token: 0x0600096B RID: 2411 RVA: 0x0000D3D8 File Offset: 0x0000B5D8
		// (remove) Token: 0x0600096C RID: 2412 RVA: 0x0000D410 File Offset: 0x0000B610
		public event getServiceNameCompletedEventHandler getServiceNameCompleted;

		// Token: 0x1400005C RID: 92
		// (add) Token: 0x0600096D RID: 2413 RVA: 0x0000D448 File Offset: 0x0000B648
		// (remove) Token: 0x0600096E RID: 2414 RVA: 0x0000D480 File Offset: 0x0000B680
		public event getStockAndPriceCompletedEventHandler getStockAndPriceCompleted;

		// Token: 0x1400005D RID: 93
		// (add) Token: 0x0600096F RID: 2415 RVA: 0x0000D4B8 File Offset: 0x0000B6B8
		// (remove) Token: 0x06000970 RID: 2416 RVA: 0x0000D4F0 File Offset: 0x0000B6F0
		public event insertProductCompletedEventHandler insertProductCompleted;

		// Token: 0x1400005E RID: 94
		// (add) Token: 0x06000971 RID: 2417 RVA: 0x0000D528 File Offset: 0x0000B728
		// (remove) Token: 0x06000972 RID: 2418 RVA: 0x0000D560 File Offset: 0x0000B760
		public event insertProductWithNewCargoDetailCompletedEventHandler insertProductWithNewCargoDetailCompleted;

		// Token: 0x1400005F RID: 95
		// (add) Token: 0x06000973 RID: 2419 RVA: 0x0000D598 File Offset: 0x0000B798
		// (remove) Token: 0x06000974 RID: 2420 RVA: 0x0000D5D0 File Offset: 0x0000B7D0
		public event insertRetailProductCompletedEventHandler insertRetailProductCompleted;

		// Token: 0x14000060 RID: 96
		// (add) Token: 0x06000975 RID: 2421 RVA: 0x0000D608 File Offset: 0x0000B808
		// (remove) Token: 0x06000976 RID: 2422 RVA: 0x0000D640 File Offset: 0x0000B840
		public event insertRetailProductWithNewCargoDetailCompletedEventHandler insertRetailProductWithNewCargoDetailCompleted;

		// Token: 0x14000061 RID: 97
		// (add) Token: 0x06000977 RID: 2423 RVA: 0x0000D678 File Offset: 0x0000B878
		// (remove) Token: 0x06000978 RID: 2424 RVA: 0x0000D6B0 File Offset: 0x0000B8B0
		public event payPriceCompletedEventHandler payPriceCompleted;

		// Token: 0x14000062 RID: 98
		// (add) Token: 0x06000979 RID: 2425 RVA: 0x0000D6E8 File Offset: 0x0000B8E8
		// (remove) Token: 0x0600097A RID: 2426 RVA: 0x0000D720 File Offset: 0x0000B920
		public event relistProductsCompletedEventHandler relistProductsCompleted;

		// Token: 0x14000063 RID: 99
		// (add) Token: 0x0600097B RID: 2427 RVA: 0x0000D758 File Offset: 0x0000B958
		// (remove) Token: 0x0600097C RID: 2428 RVA: 0x0000D790 File Offset: 0x0000B990
		public event updateItemIdCompletedEventHandler updateItemIdCompleted;

		// Token: 0x14000064 RID: 100
		// (add) Token: 0x0600097D RID: 2429 RVA: 0x0000D7C8 File Offset: 0x0000B9C8
		// (remove) Token: 0x0600097E RID: 2430 RVA: 0x0000D800 File Offset: 0x0000BA00
		public event updateMarketPriceCompletedEventHandler updateMarketPriceCompleted;

		// Token: 0x14000065 RID: 101
		// (add) Token: 0x0600097F RID: 2431 RVA: 0x0000D838 File Offset: 0x0000BA38
		// (remove) Token: 0x06000980 RID: 2432 RVA: 0x0000D870 File Offset: 0x0000BA70
		public event updatePriceCompletedEventHandler updatePriceCompleted;

		// Token: 0x14000066 RID: 102
		// (add) Token: 0x06000981 RID: 2433 RVA: 0x0000D8A8 File Offset: 0x0000BAA8
		// (remove) Token: 0x06000982 RID: 2434 RVA: 0x0000D8E0 File Offset: 0x0000BAE0
		public event updatePriceByPercentageCompletedEventHandler updatePriceByPercentageCompleted;

		// Token: 0x14000067 RID: 103
		// (add) Token: 0x06000983 RID: 2435 RVA: 0x0000D918 File Offset: 0x0000BB18
		// (remove) Token: 0x06000984 RID: 2436 RVA: 0x0000D950 File Offset: 0x0000BB50
		public event updateProductCompletedEventHandler updateProductCompleted;

		// Token: 0x14000068 RID: 104
		// (add) Token: 0x06000985 RID: 2437 RVA: 0x0000D988 File Offset: 0x0000BB88
		// (remove) Token: 0x06000986 RID: 2438 RVA: 0x0000D9C0 File Offset: 0x0000BBC0
		public event updateProductVariantsCompletedEventHandler updateProductVariantsCompleted;

		// Token: 0x14000069 RID: 105
		// (add) Token: 0x06000987 RID: 2439 RVA: 0x0000D9F8 File Offset: 0x0000BBF8
		// (remove) Token: 0x06000988 RID: 2440 RVA: 0x0000DA30 File Offset: 0x0000BC30
		public event updateProductWithNewCargoDetailCompletedEventHandler updateProductWithNewCargoDetailCompleted;

		// Token: 0x1400006A RID: 106
		// (add) Token: 0x06000989 RID: 2441 RVA: 0x0000DA68 File Offset: 0x0000BC68
		// (remove) Token: 0x0600098A RID: 2442 RVA: 0x0000DAA0 File Offset: 0x0000BCA0
		public event updateProductWithVariantsCompletedEventHandler updateProductWithVariantsCompleted;

		// Token: 0x1400006B RID: 107
		// (add) Token: 0x0600098B RID: 2443 RVA: 0x0000DAD8 File Offset: 0x0000BCD8
		// (remove) Token: 0x0600098C RID: 2444 RVA: 0x0000DB10 File Offset: 0x0000BD10
		public event updateStockCompletedEventHandler updateStockCompleted;

		// Token: 0x1400006C RID: 108
		// (add) Token: 0x0600098D RID: 2445 RVA: 0x0000DB48 File Offset: 0x0000BD48
		// (remove) Token: 0x0600098E RID: 2446 RVA: 0x0000DB80 File Offset: 0x0000BD80
		public event updateVariantStockCompletedEventHandler updateVariantStockCompleted;

		// Token: 0x0600098F RID: 2447 RVA: 0x0000DBB8 File Offset: 0x0000BDB8
		[SoapRpcMethod("", RequestNamespace = "https://product.individual.ws.listingapi.gg.com", ResponseNamespace = "https://product.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public productServicePriceResponse calculatePriceForRevision(string apiKey, string sign, long time, string productId, string itemId, string lang)
		{
			object[] array = base.Invoke("calculatePriceForRevision", new object[]
			{
				apiKey,
				sign,
				time,
				productId,
				itemId,
				lang
			});
			return (productServicePriceResponse)array[0];
		}

		// Token: 0x06000990 RID: 2448 RVA: 0x0000DC01 File Offset: 0x0000BE01
		public void calculatePriceForRevisionAsync(string apiKey, string sign, long time, string productId, string itemId, string lang)
		{
			this.calculatePriceForRevisionAsync(apiKey, sign, time, productId, itemId, lang, null);
		}

		// Token: 0x06000991 RID: 2449 RVA: 0x0000DC14 File Offset: 0x0000BE14
		public void calculatePriceForRevisionAsync(string apiKey, string sign, long time, string productId, string itemId, string lang, object userState)
		{
			if (this.calculatePriceForRevisionOperationCompleted == null)
			{
				this.calculatePriceForRevisionOperationCompleted = new SendOrPostCallback(this.OncalculatePriceForRevisionOperationCompleted);
			}
			base.InvokeAsync("calculatePriceForRevision", new object[]
			{
				apiKey,
				sign,
				time,
				productId,
				itemId,
				lang
			}, this.calculatePriceForRevisionOperationCompleted, userState);
		}

		// Token: 0x06000992 RID: 2450 RVA: 0x0000DC78 File Offset: 0x0000BE78
		private void OncalculatePriceForRevisionOperationCompleted(object arg)
		{
			if (this.calculatePriceForRevisionCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.calculatePriceForRevisionCompleted(this, new calculatePriceForRevisionCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x06000993 RID: 2451 RVA: 0x0000DCC0 File Offset: 0x0000BEC0
		[SoapRpcMethod("", RequestNamespace = "https://product.individual.ws.listingapi.gg.com", ResponseNamespace = "https://product.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public productServicePriceResponse calculatePriceForShoppingCart(string apiKey, string sign, long time, [XmlArrayItem("item", Form = XmlSchemaForm.Unqualified)] int?[] productIdList, [XmlArrayItem("item", Form = XmlSchemaForm.Unqualified)] string[] itemIdList, string lang)
		{
			object[] array = base.Invoke("calculatePriceForShoppingCart", new object[]
			{
				apiKey,
				sign,
				time,
				productIdList,
				itemIdList,
				lang
			});
			return (productServicePriceResponse)array[0];
		}

		// Token: 0x06000994 RID: 2452 RVA: 0x0000DD09 File Offset: 0x0000BF09
		public void calculatePriceForShoppingCartAsync(string apiKey, string sign, long time, int?[] productIdList, string[] itemIdList, string lang)
		{
			this.calculatePriceForShoppingCartAsync(apiKey, sign, time, productIdList, itemIdList, lang, null);
		}

		// Token: 0x06000995 RID: 2453 RVA: 0x0000DD1C File Offset: 0x0000BF1C
		public void calculatePriceForShoppingCartAsync(string apiKey, string sign, long time, int?[] productIdList, string[] itemIdList, string lang, object userState)
		{
			if (this.calculatePriceForShoppingCartOperationCompleted == null)
			{
				this.calculatePriceForShoppingCartOperationCompleted = new SendOrPostCallback(this.OncalculatePriceForShoppingCartOperationCompleted);
			}
			base.InvokeAsync("calculatePriceForShoppingCart", new object[]
			{
				apiKey,
				sign,
				time,
				productIdList,
				itemIdList,
				lang
			}, this.calculatePriceForShoppingCartOperationCompleted, userState);
		}

		// Token: 0x06000996 RID: 2454 RVA: 0x0000DD80 File Offset: 0x0000BF80
		private void OncalculatePriceForShoppingCartOperationCompleted(object arg)
		{
			if (this.calculatePriceForShoppingCartCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.calculatePriceForShoppingCartCompleted(this, new calculatePriceForShoppingCartCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x06000997 RID: 2455 RVA: 0x0000DDC8 File Offset: 0x0000BFC8
		[SoapRpcMethod("", RequestNamespace = "https://product.individual.ws.listingapi.gg.com", ResponseNamespace = "https://product.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public productServiceBooleanResponse checkForItemId(string apiKey, string sign, long time, string itemId, string lang)
		{
			object[] array = base.Invoke("checkForItemId", new object[]
			{
				apiKey,
				sign,
				time,
				itemId,
				lang
			});
			return (productServiceBooleanResponse)array[0];
		}

		// Token: 0x06000998 RID: 2456 RVA: 0x0000DE0C File Offset: 0x0000C00C
		public void checkForItemIdAsync(string apiKey, string sign, long time, string itemId, string lang)
		{
			this.checkForItemIdAsync(apiKey, sign, time, itemId, lang, null);
		}

		// Token: 0x06000999 RID: 2457 RVA: 0x0000DE1C File Offset: 0x0000C01C
		public void checkForItemIdAsync(string apiKey, string sign, long time, string itemId, string lang, object userState)
		{
			if (this.checkForItemIdOperationCompleted == null)
			{
				this.checkForItemIdOperationCompleted = new SendOrPostCallback(this.OncheckForItemIdOperationCompleted);
			}
			base.InvokeAsync("checkForItemId", new object[]
			{
				apiKey,
				sign,
				time,
				itemId,
				lang
			}, this.checkForItemIdOperationCompleted, userState);
		}

		// Token: 0x0600099A RID: 2458 RVA: 0x0000DE7C File Offset: 0x0000C07C
		private void OncheckForItemIdOperationCompleted(object arg)
		{
			if (this.checkForItemIdCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.checkForItemIdCompleted(this, new checkForItemIdCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x0600099B RID: 2459 RVA: 0x0000DEC4 File Offset: 0x0000C0C4
		[SoapRpcMethod("", RequestNamespace = "https://product.individual.ws.listingapi.gg.com", ResponseNamespace = "https://product.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public productServiceResponse cloneProduct(string apiKey, string sign, long time, string productId, string itemId, string lang)
		{
			object[] array = base.Invoke("cloneProduct", new object[]
			{
				apiKey,
				sign,
				time,
				productId,
				itemId,
				lang
			});
			return (productServiceResponse)array[0];
		}

		// Token: 0x0600099C RID: 2460 RVA: 0x0000DF0D File Offset: 0x0000C10D
		public void cloneProductAsync(string apiKey, string sign, long time, string productId, string itemId, string lang)
		{
			this.cloneProductAsync(apiKey, sign, time, productId, itemId, lang, null);
		}

		// Token: 0x0600099D RID: 2461 RVA: 0x0000DF20 File Offset: 0x0000C120
		public void cloneProductAsync(string apiKey, string sign, long time, string productId, string itemId, string lang, object userState)
		{
			if (this.cloneProductOperationCompleted == null)
			{
				this.cloneProductOperationCompleted = new SendOrPostCallback(this.OncloneProductOperationCompleted);
			}
			base.InvokeAsync("cloneProduct", new object[]
			{
				apiKey,
				sign,
				time,
				productId,
				itemId,
				lang
			}, this.cloneProductOperationCompleted, userState);
		}

		// Token: 0x0600099E RID: 2462 RVA: 0x0000DF84 File Offset: 0x0000C184
		private void OncloneProductOperationCompleted(object arg)
		{
			if (this.cloneProductCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.cloneProductCompleted(this, new cloneProductCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x0600099F RID: 2463 RVA: 0x0000DFCC File Offset: 0x0000C1CC
		[SoapRpcMethod("", RequestNamespace = "https://product.individual.ws.listingapi.gg.com", ResponseNamespace = "https://product.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public productServiceIdResponse deleteProduct(string apiKey, string sign, long time, [XmlArrayItem("item", Form = XmlSchemaForm.Unqualified)] int?[] productIdList, [XmlArrayItem("item", Form = XmlSchemaForm.Unqualified)] string[] itemIdList, string lang)
		{
			object[] array = base.Invoke("deleteProduct", new object[]
			{
				apiKey,
				sign,
				time,
				productIdList,
				itemIdList,
				lang
			});
			return (productServiceIdResponse)array[0];
		}

		// Token: 0x060009A0 RID: 2464 RVA: 0x0000E015 File Offset: 0x0000C215
		public void deleteProductAsync(string apiKey, string sign, long time, int?[] productIdList, string[] itemIdList, string lang)
		{
			this.deleteProductAsync(apiKey, sign, time, productIdList, itemIdList, lang, null);
		}

		// Token: 0x060009A1 RID: 2465 RVA: 0x0000E028 File Offset: 0x0000C228
		public void deleteProductAsync(string apiKey, string sign, long time, int?[] productIdList, string[] itemIdList, string lang, object userState)
		{
			if (this.deleteProductOperationCompleted == null)
			{
				this.deleteProductOperationCompleted = new SendOrPostCallback(this.OndeleteProductOperationCompleted);
			}
			base.InvokeAsync("deleteProduct", new object[]
			{
				apiKey,
				sign,
				time,
				productIdList,
				itemIdList,
				lang
			}, this.deleteProductOperationCompleted, userState);
		}

		// Token: 0x060009A2 RID: 2466 RVA: 0x0000E08C File Offset: 0x0000C28C
		private void OndeleteProductOperationCompleted(object arg)
		{
			if (this.deleteProductCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.deleteProductCompleted(this, new deleteProductCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x060009A3 RID: 2467 RVA: 0x0000E0D4 File Offset: 0x0000C2D4
		[SoapRpcMethod("", RequestNamespace = "https://product.individual.ws.listingapi.gg.com", ResponseNamespace = "https://product.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public productServiceProcessResponse deleteProducts(string apiKey, string sign, long time, [XmlArrayItem("item", Form = XmlSchemaForm.Unqualified)] int?[] productIdList, [XmlArrayItem("item", Form = XmlSchemaForm.Unqualified)] string[] itemIdList, string lang)
		{
			object[] array = base.Invoke("deleteProducts", new object[]
			{
				apiKey,
				sign,
				time,
				productIdList,
				itemIdList,
				lang
			});
			return (productServiceProcessResponse)array[0];
		}

		// Token: 0x060009A4 RID: 2468 RVA: 0x0000E11D File Offset: 0x0000C31D
		public void deleteProductsAsync(string apiKey, string sign, long time, int?[] productIdList, string[] itemIdList, string lang)
		{
			this.deleteProductsAsync(apiKey, sign, time, productIdList, itemIdList, lang, null);
		}

		// Token: 0x060009A5 RID: 2469 RVA: 0x0000E130 File Offset: 0x0000C330
		public void deleteProductsAsync(string apiKey, string sign, long time, int?[] productIdList, string[] itemIdList, string lang, object userState)
		{
			if (this.deleteProductsOperationCompleted == null)
			{
				this.deleteProductsOperationCompleted = new SendOrPostCallback(this.OndeleteProductsOperationCompleted);
			}
			base.InvokeAsync("deleteProducts", new object[]
			{
				apiKey,
				sign,
				time,
				productIdList,
				itemIdList,
				lang
			}, this.deleteProductsOperationCompleted, userState);
		}

		// Token: 0x060009A6 RID: 2470 RVA: 0x0000E194 File Offset: 0x0000C394
		private void OndeleteProductsOperationCompleted(object arg)
		{
			if (this.deleteProductsCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.deleteProductsCompleted(this, new deleteProductsCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x060009A7 RID: 2471 RVA: 0x0000E1DC File Offset: 0x0000C3DC
		[SoapRpcMethod("", RequestNamespace = "https://product.individual.ws.listingapi.gg.com", ResponseNamespace = "https://product.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public productServiceIdResponse finishEarly(string apiKey, string sign, long time, [XmlArrayItem("item", Form = XmlSchemaForm.Unqualified)] int?[] productIdList, [XmlArrayItem("item", Form = XmlSchemaForm.Unqualified)] string[] itemIdList, string lang)
		{
			object[] array = base.Invoke("finishEarly", new object[]
			{
				apiKey,
				sign,
				time,
				productIdList,
				itemIdList,
				lang
			});
			return (productServiceIdResponse)array[0];
		}

		// Token: 0x060009A8 RID: 2472 RVA: 0x0000E225 File Offset: 0x0000C425
		public void finishEarlyAsync(string apiKey, string sign, long time, int?[] productIdList, string[] itemIdList, string lang)
		{
			this.finishEarlyAsync(apiKey, sign, time, productIdList, itemIdList, lang, null);
		}

		// Token: 0x060009A9 RID: 2473 RVA: 0x0000E238 File Offset: 0x0000C438
		public void finishEarlyAsync(string apiKey, string sign, long time, int?[] productIdList, string[] itemIdList, string lang, object userState)
		{
			if (this.finishEarlyOperationCompleted == null)
			{
				this.finishEarlyOperationCompleted = new SendOrPostCallback(this.OnfinishEarlyOperationCompleted);
			}
			base.InvokeAsync("finishEarly", new object[]
			{
				apiKey,
				sign,
				time,
				productIdList,
				itemIdList,
				lang
			}, this.finishEarlyOperationCompleted, userState);
		}

		// Token: 0x060009AA RID: 2474 RVA: 0x0000E29C File Offset: 0x0000C49C
		private void OnfinishEarlyOperationCompleted(object arg)
		{
			if (this.finishEarlyCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.finishEarlyCompleted(this, new finishEarlyCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x060009AB RID: 2475 RVA: 0x0000E2E4 File Offset: 0x0000C4E4
		[SoapRpcMethod("", RequestNamespace = "https://product.individual.ws.listingapi.gg.com", ResponseNamespace = "https://product.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public productServiceProcessResponse finishEarlyProducts(string apiKey, string sign, long time, [XmlArrayItem("item", Form = XmlSchemaForm.Unqualified)] int?[] productIdList, [XmlArrayItem("item", Form = XmlSchemaForm.Unqualified)] string[] itemIdList, string lang)
		{
			object[] array = base.Invoke("finishEarlyProducts", new object[]
			{
				apiKey,
				sign,
				time,
				productIdList,
				itemIdList,
				lang
			});
			return (productServiceProcessResponse)array[0];
		}

		// Token: 0x060009AC RID: 2476 RVA: 0x0000E32D File Offset: 0x0000C52D
		public void finishEarlyProductsAsync(string apiKey, string sign, long time, int?[] productIdList, string[] itemIdList, string lang)
		{
			this.finishEarlyProductsAsync(apiKey, sign, time, productIdList, itemIdList, lang, null);
		}

		// Token: 0x060009AD RID: 2477 RVA: 0x0000E340 File Offset: 0x0000C540
		public void finishEarlyProductsAsync(string apiKey, string sign, long time, int?[] productIdList, string[] itemIdList, string lang, object userState)
		{
			if (this.finishEarlyProductsOperationCompleted == null)
			{
				this.finishEarlyProductsOperationCompleted = new SendOrPostCallback(this.OnfinishEarlyProductsOperationCompleted);
			}
			base.InvokeAsync("finishEarlyProducts", new object[]
			{
				apiKey,
				sign,
				time,
				productIdList,
				itemIdList,
				lang
			}, this.finishEarlyProductsOperationCompleted, userState);
		}

		// Token: 0x060009AE RID: 2478 RVA: 0x0000E3A4 File Offset: 0x0000C5A4
		private void OnfinishEarlyProductsOperationCompleted(object arg)
		{
			if (this.finishEarlyProductsCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.finishEarlyProductsCompleted(this, new finishEarlyProductsCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x060009AF RID: 2479 RVA: 0x0000E3EC File Offset: 0x0000C5EC
		[SoapRpcMethod("", RequestNamespace = "https://product.individual.ws.listingapi.gg.com", ResponseNamespace = "https://product.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public itemIdDetailResponse getItemIdDetails(string apiKey, string sign, long time, [XmlArrayItem("item", Form = XmlSchemaForm.Unqualified)] int?[] productIds, string lang)
		{
			object[] array = base.Invoke("getItemIdDetails", new object[]
			{
				apiKey,
				sign,
				time,
				productIds,
				lang
			});
			return (itemIdDetailResponse)array[0];
		}

		// Token: 0x060009B0 RID: 2480 RVA: 0x0000E430 File Offset: 0x0000C630
		public void getItemIdDetailsAsync(string apiKey, string sign, long time, int?[] productIds, string lang)
		{
			this.getItemIdDetailsAsync(apiKey, sign, time, productIds, lang, null);
		}

		// Token: 0x060009B1 RID: 2481 RVA: 0x0000E440 File Offset: 0x0000C640
		public void getItemIdDetailsAsync(string apiKey, string sign, long time, int?[] productIds, string lang, object userState)
		{
			if (this.getItemIdDetailsOperationCompleted == null)
			{
				this.getItemIdDetailsOperationCompleted = new SendOrPostCallback(this.OngetItemIdDetailsOperationCompleted);
			}
			base.InvokeAsync("getItemIdDetails", new object[]
			{
				apiKey,
				sign,
				time,
				productIds,
				lang
			}, this.getItemIdDetailsOperationCompleted, userState);
		}

		// Token: 0x060009B2 RID: 2482 RVA: 0x0000E4A0 File Offset: 0x0000C6A0
		private void OngetItemIdDetailsOperationCompleted(object arg)
		{
			if (this.getItemIdDetailsCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getItemIdDetailsCompleted(this, new getItemIdDetailsCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x060009B3 RID: 2483 RVA: 0x0000E4E8 File Offset: 0x0000C6E8
		[SoapRpcMethod("", RequestNamespace = "https://product.individual.ws.listingapi.gg.com", ResponseNamespace = "https://product.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public productServiceIdResponse getNewlyListedProductIdList(string apiKey, string sign, long time, int startOffSet, int rowCount, bool viaApi, string lang)
		{
			object[] array = base.Invoke("getNewlyListedProductIdList", new object[]
			{
				apiKey,
				sign,
				time,
				startOffSet,
				rowCount,
				viaApi,
				lang
			});
			return (productServiceIdResponse)array[0];
		}

		// Token: 0x060009B4 RID: 2484 RVA: 0x0000E548 File Offset: 0x0000C748
		public void getNewlyListedProductIdListAsync(string apiKey, string sign, long time, int startOffSet, int rowCount, bool viaApi, string lang)
		{
			this.getNewlyListedProductIdListAsync(apiKey, sign, time, startOffSet, rowCount, viaApi, lang, null);
		}

		// Token: 0x060009B5 RID: 2485 RVA: 0x0000E568 File Offset: 0x0000C768
		public void getNewlyListedProductIdListAsync(string apiKey, string sign, long time, int startOffSet, int rowCount, bool viaApi, string lang, object userState)
		{
			if (this.getNewlyListedProductIdListOperationCompleted == null)
			{
				this.getNewlyListedProductIdListOperationCompleted = new SendOrPostCallback(this.OngetNewlyListedProductIdListOperationCompleted);
			}
			base.InvokeAsync("getNewlyListedProductIdList", new object[]
			{
				apiKey,
				sign,
				time,
				startOffSet,
				rowCount,
				viaApi,
				lang
			}, this.getNewlyListedProductIdListOperationCompleted, userState);
		}

		// Token: 0x060009B6 RID: 2486 RVA: 0x0000E5E0 File Offset: 0x0000C7E0
		private void OngetNewlyListedProductIdListOperationCompleted(object arg)
		{
			if (this.getNewlyListedProductIdListCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getNewlyListedProductIdListCompleted(this, new getNewlyListedProductIdListCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x060009B7 RID: 2487 RVA: 0x0000E628 File Offset: 0x0000C828
		[SoapRpcMethod("", RequestNamespace = "https://product.individual.ws.listingapi.gg.com", ResponseNamespace = "https://product.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public productServiceDetailResponse getProduct(string apiKey, string sign, long time, string productId, string itemId, string lang)
		{
			object[] array = base.Invoke("getProduct", new object[]
			{
				apiKey,
				sign,
				time,
				productId,
				itemId,
				lang
			});
			return (productServiceDetailResponse)array[0];
		}

		// Token: 0x060009B8 RID: 2488 RVA: 0x0000E671 File Offset: 0x0000C871
		public void getProductAsync(string apiKey, string sign, long time, string productId, string itemId, string lang)
		{
			this.getProductAsync(apiKey, sign, time, productId, itemId, lang, null);
		}

		// Token: 0x060009B9 RID: 2489 RVA: 0x0000E684 File Offset: 0x0000C884
		public void getProductAsync(string apiKey, string sign, long time, string productId, string itemId, string lang, object userState)
		{
			if (this.getProductOperationCompleted == null)
			{
				this.getProductOperationCompleted = new SendOrPostCallback(this.OngetProductOperationCompleted);
			}
			base.InvokeAsync("getProduct", new object[]
			{
				apiKey,
				sign,
				time,
				productId,
				itemId,
				lang
			}, this.getProductOperationCompleted, userState);
		}

		// Token: 0x060009BA RID: 2490 RVA: 0x0000E6E8 File Offset: 0x0000C8E8
		private void OngetProductOperationCompleted(object arg)
		{
			if (this.getProductCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getProductCompleted(this, new getProductCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x060009BB RID: 2491 RVA: 0x0000E730 File Offset: 0x0000C930
		[SoapRpcMethod("", RequestNamespace = "https://product.individual.ws.listingapi.gg.com", ResponseNamespace = "https://product.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public productServiceDescResponse getProductDescription(string apiKey, string sign, long time, string productId, string itemId, string lang)
		{
			object[] array = base.Invoke("getProductDescription", new object[]
			{
				apiKey,
				sign,
				time,
				productId,
				itemId,
				lang
			});
			return (productServiceDescResponse)array[0];
		}

		// Token: 0x060009BC RID: 2492 RVA: 0x0000E779 File Offset: 0x0000C979
		public void getProductDescriptionAsync(string apiKey, string sign, long time, string productId, string itemId, string lang)
		{
			this.getProductDescriptionAsync(apiKey, sign, time, productId, itemId, lang, null);
		}

		// Token: 0x060009BD RID: 2493 RVA: 0x0000E78C File Offset: 0x0000C98C
		public void getProductDescriptionAsync(string apiKey, string sign, long time, string productId, string itemId, string lang, object userState)
		{
			if (this.getProductDescriptionOperationCompleted == null)
			{
				this.getProductDescriptionOperationCompleted = new SendOrPostCallback(this.OngetProductDescriptionOperationCompleted);
			}
			base.InvokeAsync("getProductDescription", new object[]
			{
				apiKey,
				sign,
				time,
				productId,
				itemId,
				lang
			}, this.getProductDescriptionOperationCompleted, userState);
		}

		// Token: 0x060009BE RID: 2494 RVA: 0x0000E7F0 File Offset: 0x0000C9F0
		private void OngetProductDescriptionOperationCompleted(object arg)
		{
			if (this.getProductDescriptionCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getProductDescriptionCompleted(this, new getProductDescriptionCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x060009BF RID: 2495 RVA: 0x0000E838 File Offset: 0x0000CA38
		[SoapRpcMethod("", RequestNamespace = "https://product.individual.ws.listingapi.gg.com", ResponseNamespace = "https://product.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public productFinishReasonListResponse getProductFinishReasonListByAuthenticatedUser(string apiKey, string sign, long time, int startOffSet, int rowCount, string lang)
		{
			object[] array = base.Invoke("getProductFinishReasonListByAuthenticatedUser", new object[]
			{
				apiKey,
				sign,
				time,
				startOffSet,
				rowCount,
				lang
			});
			return (productFinishReasonListResponse)array[0];
		}

		// Token: 0x060009C0 RID: 2496 RVA: 0x0000E88B File Offset: 0x0000CA8B
		public void getProductFinishReasonListByAuthenticatedUserAsync(string apiKey, string sign, long time, int startOffSet, int rowCount, string lang)
		{
			this.getProductFinishReasonListByAuthenticatedUserAsync(apiKey, sign, time, startOffSet, rowCount, lang, null);
		}

		// Token: 0x060009C1 RID: 2497 RVA: 0x0000E8A0 File Offset: 0x0000CAA0
		public void getProductFinishReasonListByAuthenticatedUserAsync(string apiKey, string sign, long time, int startOffSet, int rowCount, string lang, object userState)
		{
			if (this.getProductFinishReasonListByAuthenticatedUserOperationCompleted == null)
			{
				this.getProductFinishReasonListByAuthenticatedUserOperationCompleted = new SendOrPostCallback(this.OngetProductFinishReasonListByAuthenticatedUserOperationCompleted);
			}
			base.InvokeAsync("getProductFinishReasonListByAuthenticatedUser", new object[]
			{
				apiKey,
				sign,
				time,
				startOffSet,
				rowCount,
				lang
			}, this.getProductFinishReasonListByAuthenticatedUserOperationCompleted, userState);
		}

		// Token: 0x060009C2 RID: 2498 RVA: 0x0000E90C File Offset: 0x0000CB0C
		private void OngetProductFinishReasonListByAuthenticatedUserOperationCompleted(object arg)
		{
			if (this.getProductFinishReasonListByAuthenticatedUserCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getProductFinishReasonListByAuthenticatedUserCompleted(this, new getProductFinishReasonListByAuthenticatedUserCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x060009C3 RID: 2499 RVA: 0x0000E954 File Offset: 0x0000CB54
		[SoapRpcMethod("", RequestNamespace = "https://product.individual.ws.listingapi.gg.com", ResponseNamespace = "https://product.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public productServiceIdResponse getProductIds(string apiKey, string sign, long time, int startOffSet, int rowCount, [XmlArrayItem("item", Form = XmlSchemaForm.Unqualified)] string[] statusTypes, string lang)
		{
			object[] array = base.Invoke("getProductIds", new object[]
			{
				apiKey,
				sign,
				time,
				startOffSet,
				rowCount,
				statusTypes,
				lang
			});
			return (productServiceIdResponse)array[0];
		}

		// Token: 0x060009C4 RID: 2500 RVA: 0x0000E9AC File Offset: 0x0000CBAC
		public void getProductIdsAsync(string apiKey, string sign, long time, int startOffSet, int rowCount, string[] statusTypes, string lang)
		{
			this.getProductIdsAsync(apiKey, sign, time, startOffSet, rowCount, statusTypes, lang, null);
		}

		// Token: 0x060009C5 RID: 2501 RVA: 0x0000E9CC File Offset: 0x0000CBCC
		public void getProductIdsAsync(string apiKey, string sign, long time, int startOffSet, int rowCount, string[] statusTypes, string lang, object userState)
		{
			if (this.getProductIdsOperationCompleted == null)
			{
				this.getProductIdsOperationCompleted = new SendOrPostCallback(this.OngetProductIdsOperationCompleted);
			}
			base.InvokeAsync("getProductIds", new object[]
			{
				apiKey,
				sign,
				time,
				startOffSet,
				rowCount,
				statusTypes,
				lang
			}, this.getProductIdsOperationCompleted, userState);
		}

		// Token: 0x060009C6 RID: 2502 RVA: 0x0000EA40 File Offset: 0x0000CC40
		private void OngetProductIdsOperationCompleted(object arg)
		{
			if (this.getProductIdsCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getProductIdsCompleted(this, new getProductIdsCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x060009C7 RID: 2503 RVA: 0x0000EA88 File Offset: 0x0000CC88
		[SoapRpcMethod("", RequestNamespace = "https://product.individual.ws.listingapi.gg.com", ResponseNamespace = "https://product.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public productServiceImageStatusResponse getProductImageStatus(string apiKey, string sign, long time, string productId, string lang)
		{
			object[] array = base.Invoke("getProductImageStatus", new object[]
			{
				apiKey,
				sign,
				time,
				productId,
				lang
			});
			return (productServiceImageStatusResponse)array[0];
		}

		// Token: 0x060009C8 RID: 2504 RVA: 0x0000EACC File Offset: 0x0000CCCC
		public void getProductImageStatusAsync(string apiKey, string sign, long time, string productId, string lang)
		{
			this.getProductImageStatusAsync(apiKey, sign, time, productId, lang, null);
		}

		// Token: 0x060009C9 RID: 2505 RVA: 0x0000EADC File Offset: 0x0000CCDC
		public void getProductImageStatusAsync(string apiKey, string sign, long time, string productId, string lang, object userState)
		{
			if (this.getProductImageStatusOperationCompleted == null)
			{
				this.getProductImageStatusOperationCompleted = new SendOrPostCallback(this.OngetProductImageStatusOperationCompleted);
			}
			base.InvokeAsync("getProductImageStatus", new object[]
			{
				apiKey,
				sign,
				time,
				productId,
				lang
			}, this.getProductImageStatusOperationCompleted, userState);
		}

		// Token: 0x060009CA RID: 2506 RVA: 0x0000EB3C File Offset: 0x0000CD3C
		private void OngetProductImageStatusOperationCompleted(object arg)
		{
			if (this.getProductImageStatusCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getProductImageStatusCompleted(this, new getProductImageStatusCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x060009CB RID: 2507 RVA: 0x0000EB84 File Offset: 0x0000CD84
		[SoapRpcMethod("", RequestNamespace = "https://product.individual.ws.listingapi.gg.com", ResponseNamespace = "https://product.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public productServiceSpecResponse getProductSpecs(string apiKey, string sign, long time, string productId, string itemId, string lang)
		{
			object[] array = base.Invoke("getProductSpecs", new object[]
			{
				apiKey,
				sign,
				time,
				productId,
				itemId,
				lang
			});
			return (productServiceSpecResponse)array[0];
		}

		// Token: 0x060009CC RID: 2508 RVA: 0x0000EBCD File Offset: 0x0000CDCD
		public void getProductSpecsAsync(string apiKey, string sign, long time, string productId, string itemId, string lang)
		{
			this.getProductSpecsAsync(apiKey, sign, time, productId, itemId, lang, null);
		}

		// Token: 0x060009CD RID: 2509 RVA: 0x0000EBE0 File Offset: 0x0000CDE0
		public void getProductSpecsAsync(string apiKey, string sign, long time, string productId, string itemId, string lang, object userState)
		{
			if (this.getProductSpecsOperationCompleted == null)
			{
				this.getProductSpecsOperationCompleted = new SendOrPostCallback(this.OngetProductSpecsOperationCompleted);
			}
			base.InvokeAsync("getProductSpecs", new object[]
			{
				apiKey,
				sign,
				time,
				productId,
				itemId,
				lang
			}, this.getProductSpecsOperationCompleted, userState);
		}

		// Token: 0x060009CE RID: 2510 RVA: 0x0000EC44 File Offset: 0x0000CE44
		private void OngetProductSpecsOperationCompleted(object arg)
		{
			if (this.getProductSpecsCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getProductSpecsCompleted(this, new getProductSpecsCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x060009CF RID: 2511 RVA: 0x0000EC8C File Offset: 0x0000CE8C
		[SoapRpcMethod("", RequestNamespace = "https://product.individual.ws.listingapi.gg.com", ResponseNamespace = "https://product.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public productServiceStatusResponse getProductStatuses(string apiKey, string sign, long time, [XmlArrayItem("item", Form = XmlSchemaForm.Unqualified)] int?[] productIdList, [XmlArrayItem("item", Form = XmlSchemaForm.Unqualified)] string[] itemIdList, string lang)
		{
			object[] array = base.Invoke("getProductStatuses", new object[]
			{
				apiKey,
				sign,
				time,
				productIdList,
				itemIdList,
				lang
			});
			return (productServiceStatusResponse)array[0];
		}

		// Token: 0x060009D0 RID: 2512 RVA: 0x0000ECD5 File Offset: 0x0000CED5
		public void getProductStatusesAsync(string apiKey, string sign, long time, int?[] productIdList, string[] itemIdList, string lang)
		{
			this.getProductStatusesAsync(apiKey, sign, time, productIdList, itemIdList, lang, null);
		}

		// Token: 0x060009D1 RID: 2513 RVA: 0x0000ECE8 File Offset: 0x0000CEE8
		public void getProductStatusesAsync(string apiKey, string sign, long time, int?[] productIdList, string[] itemIdList, string lang, object userState)
		{
			if (this.getProductStatusesOperationCompleted == null)
			{
				this.getProductStatusesOperationCompleted = new SendOrPostCallback(this.OngetProductStatusesOperationCompleted);
			}
			base.InvokeAsync("getProductStatuses", new object[]
			{
				apiKey,
				sign,
				time,
				productIdList,
				itemIdList,
				lang
			}, this.getProductStatusesOperationCompleted, userState);
		}

		// Token: 0x060009D2 RID: 2514 RVA: 0x0000ED4C File Offset: 0x0000CF4C
		private void OngetProductStatusesOperationCompleted(object arg)
		{
			if (this.getProductStatusesCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getProductStatusesCompleted(this, new getProductStatusesCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x060009D3 RID: 2515 RVA: 0x0000ED94 File Offset: 0x0000CF94
		[SoapRpcMethod("", RequestNamespace = "https://product.individual.ws.listingapi.gg.com", ResponseNamespace = "https://product.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public productServiceVariantResponse getProductVariants(string apiKey, string sign, long time, string productId, string itemId, long variantId, string variantStockCode, string lang)
		{
			object[] array = base.Invoke("getProductVariants", new object[]
			{
				apiKey,
				sign,
				time,
				productId,
				itemId,
				variantId,
				variantStockCode,
				lang
			});
			return (productServiceVariantResponse)array[0];
		}

		// Token: 0x060009D4 RID: 2516 RVA: 0x0000EDEC File Offset: 0x0000CFEC
		public void getProductVariantsAsync(string apiKey, string sign, long time, string productId, string itemId, long variantId, string variantStockCode, string lang)
		{
			this.getProductVariantsAsync(apiKey, sign, time, productId, itemId, variantId, variantStockCode, lang, null);
		}

		// Token: 0x060009D5 RID: 2517 RVA: 0x0000EE10 File Offset: 0x0000D010
		public void getProductVariantsAsync(string apiKey, string sign, long time, string productId, string itemId, long variantId, string variantStockCode, string lang, object userState)
		{
			if (this.getProductVariantsOperationCompleted == null)
			{
				this.getProductVariantsOperationCompleted = new SendOrPostCallback(this.OngetProductVariantsOperationCompleted);
			}
			base.InvokeAsync("getProductVariants", new object[]
			{
				apiKey,
				sign,
				time,
				productId,
				itemId,
				variantId,
				variantStockCode,
				lang
			}, this.getProductVariantsOperationCompleted, userState);
		}

		// Token: 0x060009D6 RID: 2518 RVA: 0x0000EE84 File Offset: 0x0000D084
		private void OngetProductVariantsOperationCompleted(object arg)
		{
			if (this.getProductVariantsCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getProductVariantsCompleted(this, new getProductVariantsCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x060009D7 RID: 2519 RVA: 0x0000EECC File Offset: 0x0000D0CC
		[SoapRpcMethod("", RequestNamespace = "https://product.individual.ws.listingapi.gg.com", ResponseNamespace = "https://product.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public productServiceListResponse getProducts(string apiKey, string sign, long time, int startOffSet, int rowCount, string status, bool withData, string lang)
		{
			object[] array = base.Invoke("getProducts", new object[]
			{
				apiKey,
				sign,
				time,
				startOffSet,
				rowCount,
				status,
				withData,
				lang
			});
			return (productServiceListResponse)array[0];
		}

		// Token: 0x060009D8 RID: 2520 RVA: 0x0000EF30 File Offset: 0x0000D130
		public void getProductsAsync(string apiKey, string sign, long time, int startOffSet, int rowCount, string status, bool withData, string lang)
		{
			this.getProductsAsync(apiKey, sign, time, startOffSet, rowCount, status, withData, lang, null);
		}

		// Token: 0x060009D9 RID: 2521 RVA: 0x0000EF54 File Offset: 0x0000D154
		public void getProductsAsync(string apiKey, string sign, long time, int startOffSet, int rowCount, string status, bool withData, string lang, object userState)
		{
			if (this.getProductsOperationCompleted == null)
			{
				this.getProductsOperationCompleted = new SendOrPostCallback(this.OngetProductsOperationCompleted);
			}
			base.InvokeAsync("getProducts", new object[]
			{
				apiKey,
				sign,
				time,
				startOffSet,
				rowCount,
				status,
				withData,
				lang
			}, this.getProductsOperationCompleted, userState);
		}

		// Token: 0x060009DA RID: 2522 RVA: 0x0000EFD0 File Offset: 0x0000D1D0
		private void OngetProductsOperationCompleted(object arg)
		{
			if (this.getProductsCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getProductsCompleted(this, new getProductsCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x060009DB RID: 2523 RVA: 0x0000F018 File Offset: 0x0000D218
		[SoapRpcMethod("", RequestNamespace = "https://product.individual.ws.listingapi.gg.com", ResponseNamespace = "https://product.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public productServiceListResponse getProductsByIds(string apiKey, string sign, long time, [XmlArrayItem("item", Form = XmlSchemaForm.Unqualified)] int?[] productIdList, [XmlArrayItem("item", Form = XmlSchemaForm.Unqualified)] string[] itemIdList, bool withData, string lang)
		{
			object[] array = base.Invoke("getProductsByIds", new object[]
			{
				apiKey,
				sign,
				time,
				productIdList,
				itemIdList,
				withData,
				lang
			});
			return (productServiceListResponse)array[0];
		}

		// Token: 0x060009DC RID: 2524 RVA: 0x0000F06C File Offset: 0x0000D26C
		public void getProductsByIdsAsync(string apiKey, string sign, long time, int?[] productIdList, string[] itemIdList, bool withData, string lang)
		{
			this.getProductsByIdsAsync(apiKey, sign, time, productIdList, itemIdList, withData, lang, null);
		}

		// Token: 0x060009DD RID: 2525 RVA: 0x0000F08C File Offset: 0x0000D28C
		public void getProductsByIdsAsync(string apiKey, string sign, long time, int?[] productIdList, string[] itemIdList, bool withData, string lang, object userState)
		{
			if (this.getProductsByIdsOperationCompleted == null)
			{
				this.getProductsByIdsOperationCompleted = new SendOrPostCallback(this.OngetProductsByIdsOperationCompleted);
			}
			base.InvokeAsync("getProductsByIds", new object[]
			{
				apiKey,
				sign,
				time,
				productIdList,
				itemIdList,
				withData,
				lang
			}, this.getProductsByIdsOperationCompleted, userState);
		}

		// Token: 0x060009DE RID: 2526 RVA: 0x0000F0F8 File Offset: 0x0000D2F8
		private void OngetProductsByIdsOperationCompleted(object arg)
		{
			if (this.getProductsByIdsCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getProductsByIdsCompleted(this, new getProductsByIdsCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x060009DF RID: 2527 RVA: 0x0000F140 File Offset: 0x0000D340
		[SoapRpcMethod("", RequestNamespace = "https://product.individual.ws.listingapi.gg.com", ResponseNamespace = "https://product.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public string getServiceName()
		{
			object[] array = base.Invoke("getServiceName", new object[0]);
			return (string)array[0];
		}

		// Token: 0x060009E0 RID: 2528 RVA: 0x0000F167 File Offset: 0x0000D367
		public void getServiceNameAsync()
		{
			this.getServiceNameAsync(null);
		}

		// Token: 0x060009E1 RID: 2529 RVA: 0x0000F170 File Offset: 0x0000D370
		public void getServiceNameAsync(object userState)
		{
			if (this.getServiceNameOperationCompleted == null)
			{
				this.getServiceNameOperationCompleted = new SendOrPostCallback(this.OngetServiceNameOperationCompleted);
			}
			base.InvokeAsync("getServiceName", new object[0], this.getServiceNameOperationCompleted, userState);
		}

		// Token: 0x060009E2 RID: 2530 RVA: 0x0000F1A4 File Offset: 0x0000D3A4
		private void OngetServiceNameOperationCompleted(object arg)
		{
			if (this.getServiceNameCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getServiceNameCompleted(this, new getServiceNameCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x060009E3 RID: 2531 RVA: 0x0000F1EC File Offset: 0x0000D3EC
		[SoapRpcMethod("", RequestNamespace = "https://product.individual.ws.listingapi.gg.com", ResponseNamespace = "https://product.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public productServiceStockResponse getStockAndPrice(string apiKey, string sign, long time, [XmlArrayItem("item", Form = XmlSchemaForm.Unqualified)] int?[] productIdList, [XmlArrayItem("item", Form = XmlSchemaForm.Unqualified)] string[] itemIdList, string lang)
		{
			object[] array = base.Invoke("getStockAndPrice", new object[]
			{
				apiKey,
				sign,
				time,
				productIdList,
				itemIdList,
				lang
			});
			return (productServiceStockResponse)array[0];
		}

		// Token: 0x060009E4 RID: 2532 RVA: 0x0000F235 File Offset: 0x0000D435
		public void getStockAndPriceAsync(string apiKey, string sign, long time, int?[] productIdList, string[] itemIdList, string lang)
		{
			this.getStockAndPriceAsync(apiKey, sign, time, productIdList, itemIdList, lang, null);
		}

		// Token: 0x060009E5 RID: 2533 RVA: 0x0000F248 File Offset: 0x0000D448
		public void getStockAndPriceAsync(string apiKey, string sign, long time, int?[] productIdList, string[] itemIdList, string lang, object userState)
		{
			if (this.getStockAndPriceOperationCompleted == null)
			{
				this.getStockAndPriceOperationCompleted = new SendOrPostCallback(this.OngetStockAndPriceOperationCompleted);
			}
			base.InvokeAsync("getStockAndPrice", new object[]
			{
				apiKey,
				sign,
				time,
				productIdList,
				itemIdList,
				lang
			}, this.getStockAndPriceOperationCompleted, userState);
		}

		// Token: 0x060009E6 RID: 2534 RVA: 0x0000F2AC File Offset: 0x0000D4AC
		private void OngetStockAndPriceOperationCompleted(object arg)
		{
			if (this.getStockAndPriceCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getStockAndPriceCompleted(this, new getStockAndPriceCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x060009E7 RID: 2535 RVA: 0x0000F2F4 File Offset: 0x0000D4F4
		[SoapRpcMethod("", RequestNamespace = "https://product.individual.ws.listingapi.gg.com", ResponseNamespace = "https://product.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public productServiceResponse insertProduct(string apiKey, string sign, long time, string itemId, productType product, bool forceToSpecEntry, bool nextDateOption, string lang)
		{
			object[] array = base.Invoke("insertProduct", new object[]
			{
				apiKey,
				sign,
				time,
				itemId,
				product,
				forceToSpecEntry,
				nextDateOption,
				lang
			});
			return (productServiceResponse)array[0];
		}

		// Token: 0x060009E8 RID: 2536 RVA: 0x0000F354 File Offset: 0x0000D554
		public void insertProductAsync(string apiKey, string sign, long time, string itemId, productType product, bool forceToSpecEntry, bool nextDateOption, string lang)
		{
			this.insertProductAsync(apiKey, sign, time, itemId, product, forceToSpecEntry, nextDateOption, lang, null);
		}

		// Token: 0x060009E9 RID: 2537 RVA: 0x0000F378 File Offset: 0x0000D578
		public void insertProductAsync(string apiKey, string sign, long time, string itemId, productType product, bool forceToSpecEntry, bool nextDateOption, string lang, object userState)
		{
			if (this.insertProductOperationCompleted == null)
			{
				this.insertProductOperationCompleted = new SendOrPostCallback(this.OninsertProductOperationCompleted);
			}
			base.InvokeAsync("insertProduct", new object[]
			{
				apiKey,
				sign,
				time,
				itemId,
				product,
				forceToSpecEntry,
				nextDateOption,
				lang
			}, this.insertProductOperationCompleted, userState);
		}

		// Token: 0x060009EA RID: 2538 RVA: 0x0000F3F0 File Offset: 0x0000D5F0
		private void OninsertProductOperationCompleted(object arg)
		{
			if (this.insertProductCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.insertProductCompleted(this, new insertProductCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x060009EB RID: 2539 RVA: 0x0000F438 File Offset: 0x0000D638
		[SoapRpcMethod("", RequestNamespace = "https://product.individual.ws.listingapi.gg.com", ResponseNamespace = "https://product.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public productServiceResponse insertProductWithNewCargoDetail(string apiKey, string sign, long time, string itemId, productType product, bool forceToSpecEntry, bool nextDateOption, string lang)
		{
			object[] array = base.Invoke("insertProductWithNewCargoDetail", new object[]
			{
				apiKey,
				sign,
				time,
				itemId,
				product,
				forceToSpecEntry,
				nextDateOption,
				lang
			});
			return (productServiceResponse)array[0];
		}

		// Token: 0x060009EC RID: 2540 RVA: 0x0000F498 File Offset: 0x0000D698
		public void insertProductWithNewCargoDetailAsync(string apiKey, string sign, long time, string itemId, productType product, bool forceToSpecEntry, bool nextDateOption, string lang)
		{
			this.insertProductWithNewCargoDetailAsync(apiKey, sign, time, itemId, product, forceToSpecEntry, nextDateOption, lang, null);
		}

		// Token: 0x060009ED RID: 2541 RVA: 0x0000F4BC File Offset: 0x0000D6BC
		public void insertProductWithNewCargoDetailAsync(string apiKey, string sign, long time, string itemId, productType product, bool forceToSpecEntry, bool nextDateOption, string lang, object userState)
		{
			if (this.insertProductWithNewCargoDetailOperationCompleted == null)
			{
				this.insertProductWithNewCargoDetailOperationCompleted = new SendOrPostCallback(this.OninsertProductWithNewCargoDetailOperationCompleted);
			}
			base.InvokeAsync("insertProductWithNewCargoDetail", new object[]
			{
				apiKey,
				sign,
				time,
				itemId,
				product,
				forceToSpecEntry,
				nextDateOption,
				lang
			}, this.insertProductWithNewCargoDetailOperationCompleted, userState);
		}

		// Token: 0x060009EE RID: 2542 RVA: 0x0000F534 File Offset: 0x0000D734
		private void OninsertProductWithNewCargoDetailOperationCompleted(object arg)
		{
			if (this.insertProductWithNewCargoDetailCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.insertProductWithNewCargoDetailCompleted(this, new insertProductWithNewCargoDetailCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x060009EF RID: 2543 RVA: 0x0000F57C File Offset: 0x0000D77C
		[SoapRpcMethod("", RequestNamespace = "https://product.individual.ws.listingapi.gg.com", ResponseNamespace = "https://product.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public productServiceRetailResponse insertRetailProduct(string apiKey, string sign, long time, string itemId, productType product, bool forceToSpecEntry, bool nextDateOption, string lang)
		{
			object[] array = base.Invoke("insertRetailProduct", new object[]
			{
				apiKey,
				sign,
				time,
				itemId,
				product,
				forceToSpecEntry,
				nextDateOption,
				lang
			});
			return (productServiceRetailResponse)array[0];
		}

		// Token: 0x060009F0 RID: 2544 RVA: 0x0000F5DC File Offset: 0x0000D7DC
		public void insertRetailProductAsync(string apiKey, string sign, long time, string itemId, productType product, bool forceToSpecEntry, bool nextDateOption, string lang)
		{
			this.insertRetailProductAsync(apiKey, sign, time, itemId, product, forceToSpecEntry, nextDateOption, lang, null);
		}

		// Token: 0x060009F1 RID: 2545 RVA: 0x0000F600 File Offset: 0x0000D800
		public void insertRetailProductAsync(string apiKey, string sign, long time, string itemId, productType product, bool forceToSpecEntry, bool nextDateOption, string lang, object userState)
		{
			if (this.insertRetailProductOperationCompleted == null)
			{
				this.insertRetailProductOperationCompleted = new SendOrPostCallback(this.OninsertRetailProductOperationCompleted);
			}
			base.InvokeAsync("insertRetailProduct", new object[]
			{
				apiKey,
				sign,
				time,
				itemId,
				product,
				forceToSpecEntry,
				nextDateOption,
				lang
			}, this.insertRetailProductOperationCompleted, userState);
		}

		// Token: 0x060009F2 RID: 2546 RVA: 0x0000F678 File Offset: 0x0000D878
		private void OninsertRetailProductOperationCompleted(object arg)
		{
			if (this.insertRetailProductCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.insertRetailProductCompleted(this, new insertRetailProductCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x060009F3 RID: 2547 RVA: 0x0000F6C0 File Offset: 0x0000D8C0
		[SoapRpcMethod("", RequestNamespace = "https://product.individual.ws.listingapi.gg.com", ResponseNamespace = "https://product.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public productServiceRetailResponse insertRetailProductWithNewCargoDetail(string apiKey, string sign, long time, string itemId, productType product, bool forceToSpecEntry, bool nextDateOption, string lang)
		{
			object[] array = base.Invoke("insertRetailProductWithNewCargoDetail", new object[]
			{
				apiKey,
				sign,
				time,
				itemId,
				product,
				forceToSpecEntry,
				nextDateOption,
				lang
			});
			return (productServiceRetailResponse)array[0];
		}

		// Token: 0x060009F4 RID: 2548 RVA: 0x0000F720 File Offset: 0x0000D920
		public void insertRetailProductWithNewCargoDetailAsync(string apiKey, string sign, long time, string itemId, productType product, bool forceToSpecEntry, bool nextDateOption, string lang)
		{
			this.insertRetailProductWithNewCargoDetailAsync(apiKey, sign, time, itemId, product, forceToSpecEntry, nextDateOption, lang, null);
		}

		// Token: 0x060009F5 RID: 2549 RVA: 0x0000F744 File Offset: 0x0000D944
		public void insertRetailProductWithNewCargoDetailAsync(string apiKey, string sign, long time, string itemId, productType product, bool forceToSpecEntry, bool nextDateOption, string lang, object userState)
		{
			if (this.insertRetailProductWithNewCargoDetailOperationCompleted == null)
			{
				this.insertRetailProductWithNewCargoDetailOperationCompleted = new SendOrPostCallback(this.OninsertRetailProductWithNewCargoDetailOperationCompleted);
			}
			base.InvokeAsync("insertRetailProductWithNewCargoDetail", new object[]
			{
				apiKey,
				sign,
				time,
				itemId,
				product,
				forceToSpecEntry,
				nextDateOption,
				lang
			}, this.insertRetailProductWithNewCargoDetailOperationCompleted, userState);
		}

		// Token: 0x060009F6 RID: 2550 RVA: 0x0000F7BC File Offset: 0x0000D9BC
		private void OninsertRetailProductWithNewCargoDetailOperationCompleted(object arg)
		{
			if (this.insertRetailProductWithNewCargoDetailCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.insertRetailProductWithNewCargoDetailCompleted(this, new insertRetailProductWithNewCargoDetailCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x060009F7 RID: 2551 RVA: 0x0000F804 File Offset: 0x0000DA04
		[SoapRpcMethod("", RequestNamespace = "https://product.individual.ws.listingapi.gg.com", ResponseNamespace = "https://product.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public productServicePaymentResponse payPrice(string apiKey, string sign, long time, string voucher, string ccOwnerName, string ccOwnerSurname, string ccNumber, string cvv, string expireMonth, string expireYear, string lang)
		{
			object[] array = base.Invoke("payPrice", new object[]
			{
				apiKey,
				sign,
				time,
				voucher,
				ccOwnerName,
				ccOwnerSurname,
				ccNumber,
				cvv,
				expireMonth,
				expireYear,
				lang
			});
			return (productServicePaymentResponse)array[0];
		}

		// Token: 0x060009F8 RID: 2552 RVA: 0x0000F86C File Offset: 0x0000DA6C
		public void payPriceAsync(string apiKey, string sign, long time, string voucher, string ccOwnerName, string ccOwnerSurname, string ccNumber, string cvv, string expireMonth, string expireYear, string lang)
		{
			this.payPriceAsync(apiKey, sign, time, voucher, ccOwnerName, ccOwnerSurname, ccNumber, cvv, expireMonth, expireYear, lang, null);
		}

		// Token: 0x060009F9 RID: 2553 RVA: 0x0000F894 File Offset: 0x0000DA94
		public void payPriceAsync(string apiKey, string sign, long time, string voucher, string ccOwnerName, string ccOwnerSurname, string ccNumber, string cvv, string expireMonth, string expireYear, string lang, object userState)
		{
			if (this.payPriceOperationCompleted == null)
			{
				this.payPriceOperationCompleted = new SendOrPostCallback(this.OnpayPriceOperationCompleted);
			}
			base.InvokeAsync("payPrice", new object[]
			{
				apiKey,
				sign,
				time,
				voucher,
				ccOwnerName,
				ccOwnerSurname,
				ccNumber,
				cvv,
				expireMonth,
				expireYear,
				lang
			}, this.payPriceOperationCompleted, userState);
		}

		// Token: 0x060009FA RID: 2554 RVA: 0x0000F914 File Offset: 0x0000DB14
		private void OnpayPriceOperationCompleted(object arg)
		{
			if (this.payPriceCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.payPriceCompleted(this, new payPriceCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x060009FB RID: 2555 RVA: 0x0000F95C File Offset: 0x0000DB5C
		[SoapRpcMethod("", RequestNamespace = "https://product.individual.ws.listingapi.gg.com", ResponseNamespace = "https://product.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public productServiceIdResponse relistProducts(string apiKey, string sign, long time, [XmlArrayItem("item", Form = XmlSchemaForm.Unqualified)] int?[] productIdList, [XmlArrayItem("item", Form = XmlSchemaForm.Unqualified)] string[] itemIdList, string lang)
		{
			object[] array = base.Invoke("relistProducts", new object[]
			{
				apiKey,
				sign,
				time,
				productIdList,
				itemIdList,
				lang
			});
			return (productServiceIdResponse)array[0];
		}

		// Token: 0x060009FC RID: 2556 RVA: 0x0000F9A5 File Offset: 0x0000DBA5
		public void relistProductsAsync(string apiKey, string sign, long time, int?[] productIdList, string[] itemIdList, string lang)
		{
			this.relistProductsAsync(apiKey, sign, time, productIdList, itemIdList, lang, null);
		}

		// Token: 0x060009FD RID: 2557 RVA: 0x0000F9B8 File Offset: 0x0000DBB8
		public void relistProductsAsync(string apiKey, string sign, long time, int?[] productIdList, string[] itemIdList, string lang, object userState)
		{
			if (this.relistProductsOperationCompleted == null)
			{
				this.relistProductsOperationCompleted = new SendOrPostCallback(this.OnrelistProductsOperationCompleted);
			}
			base.InvokeAsync("relistProducts", new object[]
			{
				apiKey,
				sign,
				time,
				productIdList,
				itemIdList,
				lang
			}, this.relistProductsOperationCompleted, userState);
		}

		// Token: 0x060009FE RID: 2558 RVA: 0x0000FA1C File Offset: 0x0000DC1C
		private void OnrelistProductsOperationCompleted(object arg)
		{
			if (this.relistProductsCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.relistProductsCompleted(this, new relistProductsCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x060009FF RID: 2559 RVA: 0x0000FA64 File Offset: 0x0000DC64
		[SoapRpcMethod("", RequestNamespace = "https://product.individual.ws.listingapi.gg.com", ResponseNamespace = "https://product.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public productServiceResponse updateItemId(string apiKey, string sign, long time, string productId, string itemId, string lang)
		{
			object[] array = base.Invoke("updateItemId", new object[]
			{
				apiKey,
				sign,
				time,
				productId,
				itemId,
				lang
			});
			return (productServiceResponse)array[0];
		}

		// Token: 0x06000A00 RID: 2560 RVA: 0x0000FAAD File Offset: 0x0000DCAD
		public void updateItemIdAsync(string apiKey, string sign, long time, string productId, string itemId, string lang)
		{
			this.updateItemIdAsync(apiKey, sign, time, productId, itemId, lang, null);
		}

		// Token: 0x06000A01 RID: 2561 RVA: 0x0000FAC0 File Offset: 0x0000DCC0
		public void updateItemIdAsync(string apiKey, string sign, long time, string productId, string itemId, string lang, object userState)
		{
			if (this.updateItemIdOperationCompleted == null)
			{
				this.updateItemIdOperationCompleted = new SendOrPostCallback(this.OnupdateItemIdOperationCompleted);
			}
			base.InvokeAsync("updateItemId", new object[]
			{
				apiKey,
				sign,
				time,
				productId,
				itemId,
				lang
			}, this.updateItemIdOperationCompleted, userState);
		}

		// Token: 0x06000A02 RID: 2562 RVA: 0x0000FB24 File Offset: 0x0000DD24
		private void OnupdateItemIdOperationCompleted(object arg)
		{
			if (this.updateItemIdCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.updateItemIdCompleted(this, new updateItemIdCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x06000A03 RID: 2563 RVA: 0x0000FB6C File Offset: 0x0000DD6C
		[SoapRpcMethod("", RequestNamespace = "https://product.individual.ws.listingapi.gg.com", ResponseNamespace = "https://product.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public productServiceResponse updateMarketPrice(string apiKey, string sign, long time, string productId, string marketPrice, string lang)
		{
			object[] array = base.Invoke("updateMarketPrice", new object[]
			{
				apiKey,
				sign,
				time,
				productId,
				marketPrice,
				lang
			});
			return (productServiceResponse)array[0];
		}

		// Token: 0x06000A04 RID: 2564 RVA: 0x0000FBB5 File Offset: 0x0000DDB5
		public void updateMarketPriceAsync(string apiKey, string sign, long time, string productId, string marketPrice, string lang)
		{
			this.updateMarketPriceAsync(apiKey, sign, time, productId, marketPrice, lang, null);
		}

		// Token: 0x06000A05 RID: 2565 RVA: 0x0000FBC8 File Offset: 0x0000DDC8
		public void updateMarketPriceAsync(string apiKey, string sign, long time, string productId, string marketPrice, string lang, object userState)
		{
			if (this.updateMarketPriceOperationCompleted == null)
			{
				this.updateMarketPriceOperationCompleted = new SendOrPostCallback(this.OnupdateMarketPriceOperationCompleted);
			}
			base.InvokeAsync("updateMarketPrice", new object[]
			{
				apiKey,
				sign,
				time,
				productId,
				marketPrice,
				lang
			}, this.updateMarketPriceOperationCompleted, userState);
		}

		// Token: 0x06000A06 RID: 2566 RVA: 0x0000FC2C File Offset: 0x0000DE2C
		private void OnupdateMarketPriceOperationCompleted(object arg)
		{
			if (this.updateMarketPriceCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.updateMarketPriceCompleted(this, new updateMarketPriceCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x06000A07 RID: 2567 RVA: 0x0000FC74 File Offset: 0x0000DE74
		[SoapRpcMethod("", RequestNamespace = "https://product.individual.ws.listingapi.gg.com", ResponseNamespace = "https://product.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public productServiceResponse updatePrice(string apiKey, string sign, long time, string productId, string itemId, double price, bool cancelBid, string lang)
		{
			object[] array = base.Invoke("updatePrice", new object[]
			{
				apiKey,
				sign,
				time,
				productId,
				itemId,
				price,
				cancelBid,
				lang
			});
			return (productServiceResponse)array[0];
		}

		// Token: 0x06000A08 RID: 2568 RVA: 0x0000FCD4 File Offset: 0x0000DED4
		public void updatePriceAsync(string apiKey, string sign, long time, string productId, string itemId, double price, bool cancelBid, string lang)
		{
			this.updatePriceAsync(apiKey, sign, time, productId, itemId, price, cancelBid, lang, null);
		}

		// Token: 0x06000A09 RID: 2569 RVA: 0x0000FCF8 File Offset: 0x0000DEF8
		public void updatePriceAsync(string apiKey, string sign, long time, string productId, string itemId, double price, bool cancelBid, string lang, object userState)
		{
			if (this.updatePriceOperationCompleted == null)
			{
				this.updatePriceOperationCompleted = new SendOrPostCallback(this.OnupdatePriceOperationCompleted);
			}
			base.InvokeAsync("updatePrice", new object[]
			{
				apiKey,
				sign,
				time,
				productId,
				itemId,
				price,
				cancelBid,
				lang
			}, this.updatePriceOperationCompleted, userState);
		}

		// Token: 0x06000A0A RID: 2570 RVA: 0x0000FD70 File Offset: 0x0000DF70
		private void OnupdatePriceOperationCompleted(object arg)
		{
			if (this.updatePriceCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.updatePriceCompleted(this, new updatePriceCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x06000A0B RID: 2571 RVA: 0x0000FDB8 File Offset: 0x0000DFB8
		[SoapRpcMethod("", RequestNamespace = "https://product.individual.ws.listingapi.gg.com", ResponseNamespace = "https://product.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public productServiceResponse updatePriceByPercentage(string apiKey, string sign, long time, string productId, string itemId, string operatorType, int percentage, string lang)
		{
			object[] array = base.Invoke("updatePriceByPercentage", new object[]
			{
				apiKey,
				sign,
				time,
				productId,
				itemId,
				operatorType,
				percentage,
				lang
			});
			return (productServiceResponse)array[0];
		}

		// Token: 0x06000A0C RID: 2572 RVA: 0x0000FE10 File Offset: 0x0000E010
		public void updatePriceByPercentageAsync(string apiKey, string sign, long time, string productId, string itemId, string operatorType, int percentage, string lang)
		{
			this.updatePriceByPercentageAsync(apiKey, sign, time, productId, itemId, operatorType, percentage, lang, null);
		}

		// Token: 0x06000A0D RID: 2573 RVA: 0x0000FE34 File Offset: 0x0000E034
		public void updatePriceByPercentageAsync(string apiKey, string sign, long time, string productId, string itemId, string operatorType, int percentage, string lang, object userState)
		{
			if (this.updatePriceByPercentageOperationCompleted == null)
			{
				this.updatePriceByPercentageOperationCompleted = new SendOrPostCallback(this.OnupdatePriceByPercentageOperationCompleted);
			}
			base.InvokeAsync("updatePriceByPercentage", new object[]
			{
				apiKey,
				sign,
				time,
				productId,
				itemId,
				operatorType,
				percentage,
				lang
			}, this.updatePriceByPercentageOperationCompleted, userState);
		}

		// Token: 0x06000A0E RID: 2574 RVA: 0x0000FEA8 File Offset: 0x0000E0A8
		private void OnupdatePriceByPercentageOperationCompleted(object arg)
		{
			if (this.updatePriceByPercentageCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.updatePriceByPercentageCompleted(this, new updatePriceByPercentageCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x06000A0F RID: 2575 RVA: 0x0000FEF0 File Offset: 0x0000E0F0
		[SoapRpcMethod("", RequestNamespace = "https://product.individual.ws.listingapi.gg.com", ResponseNamespace = "https://product.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public productServiceResponse updateProduct(string apiKey, string sign, long time, string itemId, string productId, productType product, bool onSale, bool forceToSpecEntry, bool nextDateOption, string lang)
		{
			object[] array = base.Invoke("updateProduct", new object[]
			{
				apiKey,
				sign,
				time,
				itemId,
				productId,
				product,
				onSale,
				forceToSpecEntry,
				nextDateOption,
				lang
			});
			return (productServiceResponse)array[0];
		}

		// Token: 0x06000A10 RID: 2576 RVA: 0x0000FF60 File Offset: 0x0000E160
		public void updateProductAsync(string apiKey, string sign, long time, string itemId, string productId, productType product, bool onSale, bool forceToSpecEntry, bool nextDateOption, string lang)
		{
			this.updateProductAsync(apiKey, sign, time, itemId, productId, product, onSale, forceToSpecEntry, nextDateOption, lang, null);
		}

		// Token: 0x06000A11 RID: 2577 RVA: 0x0000FF88 File Offset: 0x0000E188
		public void updateProductAsync(string apiKey, string sign, long time, string itemId, string productId, productType product, bool onSale, bool forceToSpecEntry, bool nextDateOption, string lang, object userState)
		{
			if (this.updateProductOperationCompleted == null)
			{
				this.updateProductOperationCompleted = new SendOrPostCallback(this.OnupdateProductOperationCompleted);
			}
			base.InvokeAsync("updateProduct", new object[]
			{
				apiKey,
				sign,
				time,
				itemId,
				productId,
				product,
				onSale,
				forceToSpecEntry,
				nextDateOption,
				lang
			}, this.updateProductOperationCompleted, userState);
		}

		// Token: 0x06000A12 RID: 2578 RVA: 0x00010010 File Offset: 0x0000E210
		private void OnupdateProductOperationCompleted(object arg)
		{
			if (this.updateProductCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.updateProductCompleted(this, new updateProductCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x06000A13 RID: 2579 RVA: 0x00010058 File Offset: 0x0000E258
		[SoapRpcMethod("", RequestNamespace = "https://product.individual.ws.listingapi.gg.com", ResponseNamespace = "https://product.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public productServiceRetailResponse updateProductVariants(string apiKey, string sign, long time, string productId, string itemId, productVariantType productVariant, string lang)
		{
			object[] array = base.Invoke("updateProductVariants", new object[]
			{
				apiKey,
				sign,
				time,
				productId,
				itemId,
				productVariant,
				lang
			});
			return (productServiceRetailResponse)array[0];
		}

		// Token: 0x06000A14 RID: 2580 RVA: 0x000100A8 File Offset: 0x0000E2A8
		public void updateProductVariantsAsync(string apiKey, string sign, long time, string productId, string itemId, productVariantType productVariant, string lang)
		{
			this.updateProductVariantsAsync(apiKey, sign, time, productId, itemId, productVariant, lang, null);
		}

		// Token: 0x06000A15 RID: 2581 RVA: 0x000100C8 File Offset: 0x0000E2C8
		public void updateProductVariantsAsync(string apiKey, string sign, long time, string productId, string itemId, productVariantType productVariant, string lang, object userState)
		{
			if (this.updateProductVariantsOperationCompleted == null)
			{
				this.updateProductVariantsOperationCompleted = new SendOrPostCallback(this.OnupdateProductVariantsOperationCompleted);
			}
			base.InvokeAsync("updateProductVariants", new object[]
			{
				apiKey,
				sign,
				time,
				productId,
				itemId,
				productVariant,
				lang
			}, this.updateProductVariantsOperationCompleted, userState);
		}

		// Token: 0x06000A16 RID: 2582 RVA: 0x00010130 File Offset: 0x0000E330
		private void OnupdateProductVariantsOperationCompleted(object arg)
		{
			if (this.updateProductVariantsCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.updateProductVariantsCompleted(this, new updateProductVariantsCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x06000A17 RID: 2583 RVA: 0x00010178 File Offset: 0x0000E378
		[SoapRpcMethod("", RequestNamespace = "https://product.individual.ws.listingapi.gg.com", ResponseNamespace = "https://product.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public productServiceResponse updateProductWithNewCargoDetail(string apiKey, string sign, long time, string itemId, string productId, productType product, bool onSale, bool forceToSpecEntry, bool nextDateOption, string lang)
		{
			object[] array = base.Invoke("updateProductWithNewCargoDetail", new object[]
			{
				apiKey,
				sign,
				time,
				itemId,
				productId,
				product,
				onSale,
				forceToSpecEntry,
				nextDateOption,
				lang
			});
			return (productServiceResponse)array[0];
		}

		// Token: 0x06000A18 RID: 2584 RVA: 0x000101E8 File Offset: 0x0000E3E8
		public void updateProductWithNewCargoDetailAsync(string apiKey, string sign, long time, string itemId, string productId, productType product, bool onSale, bool forceToSpecEntry, bool nextDateOption, string lang)
		{
			this.updateProductWithNewCargoDetailAsync(apiKey, sign, time, itemId, productId, product, onSale, forceToSpecEntry, nextDateOption, lang, null);
		}

		// Token: 0x06000A19 RID: 2585 RVA: 0x00010210 File Offset: 0x0000E410
		public void updateProductWithNewCargoDetailAsync(string apiKey, string sign, long time, string itemId, string productId, productType product, bool onSale, bool forceToSpecEntry, bool nextDateOption, string lang, object userState)
		{
			if (this.updateProductWithNewCargoDetailOperationCompleted == null)
			{
				this.updateProductWithNewCargoDetailOperationCompleted = new SendOrPostCallback(this.OnupdateProductWithNewCargoDetailOperationCompleted);
			}
			base.InvokeAsync("updateProductWithNewCargoDetail", new object[]
			{
				apiKey,
				sign,
				time,
				itemId,
				productId,
				product,
				onSale,
				forceToSpecEntry,
				nextDateOption,
				lang
			}, this.updateProductWithNewCargoDetailOperationCompleted, userState);
		}

		// Token: 0x06000A1A RID: 2586 RVA: 0x00010298 File Offset: 0x0000E498
		private void OnupdateProductWithNewCargoDetailOperationCompleted(object arg)
		{
			if (this.updateProductWithNewCargoDetailCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.updateProductWithNewCargoDetailCompleted(this, new updateProductWithNewCargoDetailCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x06000A1B RID: 2587 RVA: 0x000102E0 File Offset: 0x0000E4E0
		[SoapRpcMethod("", RequestNamespace = "https://product.individual.ws.listingapi.gg.com", ResponseNamespace = "https://product.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public productServiceResponse updateProductWithVariants(string apiKey, string sign, long time, string itemId, string productId, productType product, bool onSale, bool forceToSpecEntry, bool nextDateOption, string lang)
		{
			object[] array = base.Invoke("updateProductWithVariants", new object[]
			{
				apiKey,
				sign,
				time,
				itemId,
				productId,
				product,
				onSale,
				forceToSpecEntry,
				nextDateOption,
				lang
			});
			return (productServiceResponse)array[0];
		}

		// Token: 0x06000A1C RID: 2588 RVA: 0x00010350 File Offset: 0x0000E550
		public void updateProductWithVariantsAsync(string apiKey, string sign, long time, string itemId, string productId, productType product, bool onSale, bool forceToSpecEntry, bool nextDateOption, string lang)
		{
			this.updateProductWithVariantsAsync(apiKey, sign, time, itemId, productId, product, onSale, forceToSpecEntry, nextDateOption, lang, null);
		}

		// Token: 0x06000A1D RID: 2589 RVA: 0x00010378 File Offset: 0x0000E578
		public void updateProductWithVariantsAsync(string apiKey, string sign, long time, string itemId, string productId, productType product, bool onSale, bool forceToSpecEntry, bool nextDateOption, string lang, object userState)
		{
			if (this.updateProductWithVariantsOperationCompleted == null)
			{
				this.updateProductWithVariantsOperationCompleted = new SendOrPostCallback(this.OnupdateProductWithVariantsOperationCompleted);
			}
			base.InvokeAsync("updateProductWithVariants", new object[]
			{
				apiKey,
				sign,
				time,
				itemId,
				productId,
				product,
				onSale,
				forceToSpecEntry,
				nextDateOption,
				lang
			}, this.updateProductWithVariantsOperationCompleted, userState);
		}

		// Token: 0x06000A1E RID: 2590 RVA: 0x00010400 File Offset: 0x0000E600
		private void OnupdateProductWithVariantsOperationCompleted(object arg)
		{
			if (this.updateProductWithVariantsCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.updateProductWithVariantsCompleted(this, new updateProductWithVariantsCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x06000A1F RID: 2591 RVA: 0x00010448 File Offset: 0x0000E648
		[SoapRpcMethod("", RequestNamespace = "https://product.individual.ws.listingapi.gg.com", ResponseNamespace = "https://product.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public productServiceResponse updateStock(string apiKey, string sign, long time, string productId, string itemId, int stock, bool cancelBid, string lang)
		{
			object[] array = base.Invoke("updateStock", new object[]
			{
				apiKey,
				sign,
				time,
				productId,
				itemId,
				stock,
				cancelBid,
				lang
			});
			return (productServiceResponse)array[0];
		}

		// Token: 0x06000A20 RID: 2592 RVA: 0x000104A8 File Offset: 0x0000E6A8
		public void updateStockAsync(string apiKey, string sign, long time, string productId, string itemId, int stock, bool cancelBid, string lang)
		{
			this.updateStockAsync(apiKey, sign, time, productId, itemId, stock, cancelBid, lang, null);
		}

		// Token: 0x06000A21 RID: 2593 RVA: 0x000104CC File Offset: 0x0000E6CC
		public void updateStockAsync(string apiKey, string sign, long time, string productId, string itemId, int stock, bool cancelBid, string lang, object userState)
		{
			if (this.updateStockOperationCompleted == null)
			{
				this.updateStockOperationCompleted = new SendOrPostCallback(this.OnupdateStockOperationCompleted);
			}
			base.InvokeAsync("updateStock", new object[]
			{
				apiKey,
				sign,
				time,
				productId,
				itemId,
				stock,
				cancelBid,
				lang
			}, this.updateStockOperationCompleted, userState);
		}

		// Token: 0x06000A22 RID: 2594 RVA: 0x00010544 File Offset: 0x0000E744
		private void OnupdateStockOperationCompleted(object arg)
		{
			if (this.updateStockCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.updateStockCompleted(this, new updateStockCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x06000A23 RID: 2595 RVA: 0x0001058C File Offset: 0x0000E78C
		[SoapRpcMethod("", RequestNamespace = "https://product.individual.ws.listingapi.gg.com", ResponseNamespace = "https://product.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public productServiceResponse updateVariantStock(string apiKey, string sign, long time, string productId, string itemId, string variantId, int stock, string lang)
		{
			object[] array = base.Invoke("updateVariantStock", new object[]
			{
				apiKey,
				sign,
				time,
				productId,
				itemId,
				variantId,
				stock,
				lang
			});
			return (productServiceResponse)array[0];
		}

		// Token: 0x06000A24 RID: 2596 RVA: 0x000105E4 File Offset: 0x0000E7E4
		public void updateVariantStockAsync(string apiKey, string sign, long time, string productId, string itemId, string variantId, int stock, string lang)
		{
			this.updateVariantStockAsync(apiKey, sign, time, productId, itemId, variantId, stock, lang, null);
		}

		// Token: 0x06000A25 RID: 2597 RVA: 0x00010608 File Offset: 0x0000E808
		public void updateVariantStockAsync(string apiKey, string sign, long time, string productId, string itemId, string variantId, int stock, string lang, object userState)
		{
			if (this.updateVariantStockOperationCompleted == null)
			{
				this.updateVariantStockOperationCompleted = new SendOrPostCallback(this.OnupdateVariantStockOperationCompleted);
			}
			base.InvokeAsync("updateVariantStock", new object[]
			{
				apiKey,
				sign,
				time,
				productId,
				itemId,
				variantId,
				stock,
				lang
			}, this.updateVariantStockOperationCompleted, userState);
		}

		// Token: 0x06000A26 RID: 2598 RVA: 0x0001067C File Offset: 0x0000E87C
		private void OnupdateVariantStockOperationCompleted(object arg)
		{
			if (this.updateVariantStockCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.updateVariantStockCompleted(this, new updateVariantStockCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x06000A27 RID: 2599 RVA: 0x000106C1 File Offset: 0x0000E8C1
		public new void CancelAsync(object userState)
		{
			base.CancelAsync(userState);
		}

		// Token: 0x06000A28 RID: 2600 RVA: 0x000106CC File Offset: 0x0000E8CC
		private bool IsLocalFileSystemWebService(string url)
		{
			if (url == null || url == string.Empty)
			{
				return false;
			}
			Uri uri = new Uri(url);
			return uri.Port >= 1024 && string.Compare(uri.Host, "localHost", StringComparison.OrdinalIgnoreCase) == 0;
		}

		// Token: 0x0400036F RID: 879
		private SendOrPostCallback calculatePriceForRevisionOperationCompleted;

		// Token: 0x04000370 RID: 880
		private SendOrPostCallback calculatePriceForShoppingCartOperationCompleted;

		// Token: 0x04000371 RID: 881
		private SendOrPostCallback checkForItemIdOperationCompleted;

		// Token: 0x04000372 RID: 882
		private SendOrPostCallback cloneProductOperationCompleted;

		// Token: 0x04000373 RID: 883
		private SendOrPostCallback deleteProductOperationCompleted;

		// Token: 0x04000374 RID: 884
		private SendOrPostCallback deleteProductsOperationCompleted;

		// Token: 0x04000375 RID: 885
		private SendOrPostCallback finishEarlyOperationCompleted;

		// Token: 0x04000376 RID: 886
		private SendOrPostCallback finishEarlyProductsOperationCompleted;

		// Token: 0x04000377 RID: 887
		private SendOrPostCallback getItemIdDetailsOperationCompleted;

		// Token: 0x04000378 RID: 888
		private SendOrPostCallback getNewlyListedProductIdListOperationCompleted;

		// Token: 0x04000379 RID: 889
		private SendOrPostCallback getProductOperationCompleted;

		// Token: 0x0400037A RID: 890
		private SendOrPostCallback getProductDescriptionOperationCompleted;

		// Token: 0x0400037B RID: 891
		private SendOrPostCallback getProductFinishReasonListByAuthenticatedUserOperationCompleted;

		// Token: 0x0400037C RID: 892
		private SendOrPostCallback getProductIdsOperationCompleted;

		// Token: 0x0400037D RID: 893
		private SendOrPostCallback getProductImageStatusOperationCompleted;

		// Token: 0x0400037E RID: 894
		private SendOrPostCallback getProductSpecsOperationCompleted;

		// Token: 0x0400037F RID: 895
		private SendOrPostCallback getProductStatusesOperationCompleted;

		// Token: 0x04000380 RID: 896
		private SendOrPostCallback getProductVariantsOperationCompleted;

		// Token: 0x04000381 RID: 897
		private SendOrPostCallback getProductsOperationCompleted;

		// Token: 0x04000382 RID: 898
		private SendOrPostCallback getProductsByIdsOperationCompleted;

		// Token: 0x04000383 RID: 899
		private SendOrPostCallback getServiceNameOperationCompleted;

		// Token: 0x04000384 RID: 900
		private SendOrPostCallback getStockAndPriceOperationCompleted;

		// Token: 0x04000385 RID: 901
		private SendOrPostCallback insertProductOperationCompleted;

		// Token: 0x04000386 RID: 902
		private SendOrPostCallback insertProductWithNewCargoDetailOperationCompleted;

		// Token: 0x04000387 RID: 903
		private SendOrPostCallback insertRetailProductOperationCompleted;

		// Token: 0x04000388 RID: 904
		private SendOrPostCallback insertRetailProductWithNewCargoDetailOperationCompleted;

		// Token: 0x04000389 RID: 905
		private SendOrPostCallback payPriceOperationCompleted;

		// Token: 0x0400038A RID: 906
		private SendOrPostCallback relistProductsOperationCompleted;

		// Token: 0x0400038B RID: 907
		private SendOrPostCallback updateItemIdOperationCompleted;

		// Token: 0x0400038C RID: 908
		private SendOrPostCallback updateMarketPriceOperationCompleted;

		// Token: 0x0400038D RID: 909
		private SendOrPostCallback updatePriceOperationCompleted;

		// Token: 0x0400038E RID: 910
		private SendOrPostCallback updatePriceByPercentageOperationCompleted;

		// Token: 0x0400038F RID: 911
		private SendOrPostCallback updateProductOperationCompleted;

		// Token: 0x04000390 RID: 912
		private SendOrPostCallback updateProductVariantsOperationCompleted;

		// Token: 0x04000391 RID: 913
		private SendOrPostCallback updateProductWithNewCargoDetailOperationCompleted;

		// Token: 0x04000392 RID: 914
		private SendOrPostCallback updateProductWithVariantsOperationCompleted;

		// Token: 0x04000393 RID: 915
		private SendOrPostCallback updateStockOperationCompleted;

		// Token: 0x04000394 RID: 916
		private SendOrPostCallback updateVariantStockOperationCompleted;

		// Token: 0x04000395 RID: 917
		private bool useDefaultCredentialsSetExplicitly;
	}
}
