﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x02000165 RID: 357
	[DesignerCategory("code")]
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	[DebuggerStepThrough]
	public class insertProductCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000CA9 RID: 3241 RVA: 0x00011B2E File Offset: 0x0000FD2E
		internal insertProductCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x170003F9 RID: 1017
		// (get) Token: 0x06000CAA RID: 3242 RVA: 0x00011B41 File Offset: 0x0000FD41
		public productServiceResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (productServiceResponse)this.results[0];
			}
		}

		// Token: 0x040004BF RID: 1215
		private object[] results;
	}
}
