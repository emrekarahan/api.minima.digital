﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x02000183 RID: 387
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	public class updateVariantStockCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000D03 RID: 3331 RVA: 0x00011D86 File Offset: 0x0000FF86
		internal updateVariantStockCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x17000408 RID: 1032
		// (get) Token: 0x06000D04 RID: 3332 RVA: 0x00011D99 File Offset: 0x0000FF99
		public productServiceResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (productServiceResponse)this.results[0];
			}
		}

		// Token: 0x040004CE RID: 1230
		private object[] results;
	}
}
