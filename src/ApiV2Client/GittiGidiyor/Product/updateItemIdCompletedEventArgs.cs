﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x02000171 RID: 369
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	[DesignerCategory("code")]
	[DebuggerStepThrough]
	public class updateItemIdCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000CCD RID: 3277 RVA: 0x00011C1E File Offset: 0x0000FE1E
		internal updateItemIdCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x170003FF RID: 1023
		// (get) Token: 0x06000CCE RID: 3278 RVA: 0x00011C31 File Offset: 0x0000FE31
		public productServiceResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (productServiceResponse)this.results[0];
			}
		}

		// Token: 0x040004C5 RID: 1221
		private object[] results;
	}
}
