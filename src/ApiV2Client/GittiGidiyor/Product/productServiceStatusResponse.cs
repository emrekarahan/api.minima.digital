﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x0200012A RID: 298
	[GeneratedCode("System.Xml", "4.7.3190.0")]
	[DesignerCategory("code")]
	[XmlType(Namespace = "https://product.individual.ws.listingapi.gg.com")]
	[DebuggerStepThrough]
	[Serializable]
	public class productServiceStatusResponse : baseResponse
	{
		// Token: 0x170003AC RID: 940
		// (get) Token: 0x06000BA5 RID: 2981 RVA: 0x000113A7 File Offset: 0x0000F5A7
		// (set) Token: 0x06000BA6 RID: 2982 RVA: 0x000113AF File Offset: 0x0000F5AF
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public long productCount
		{
			get
			{
				return this.productCountField;
			}
			set
			{
				this.productCountField = value;
			}
		}

		// Token: 0x170003AD RID: 941
		// (get) Token: 0x06000BA7 RID: 2983 RVA: 0x000113B8 File Offset: 0x0000F5B8
		// (set) Token: 0x06000BA8 RID: 2984 RVA: 0x000113C0 File Offset: 0x0000F5C0
		[XmlIgnore]
		public bool productCountSpecified
		{
			get
			{
				return this.productCountFieldSpecified;
			}
			set
			{
				this.productCountFieldSpecified = value;
			}
		}

		// Token: 0x170003AE RID: 942
		// (get) Token: 0x06000BA9 RID: 2985 RVA: 0x000113C9 File Offset: 0x0000F5C9
		// (set) Token: 0x06000BAA RID: 2986 RVA: 0x000113D1 File Offset: 0x0000F5D1
		[XmlArrayItem("productStatus", Form = XmlSchemaForm.Unqualified)]
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		public productStatusType[] productStatusList
		{
			get
			{
				return this.productStatusListField;
			}
			set
			{
				this.productStatusListField = value;
			}
		}

		// Token: 0x04000472 RID: 1138
		private long productCountField;

		// Token: 0x04000473 RID: 1139
		private bool productCountFieldSpecified;

		// Token: 0x04000474 RID: 1140
		private productStatusType[] productStatusListField;
	}
}
