﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x0200017B RID: 379
	[DesignerCategory("code")]
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	[DebuggerStepThrough]
	public class updateProductVariantsCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000CEB RID: 3307 RVA: 0x00011CE6 File Offset: 0x0000FEE6
		internal updateProductVariantsCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x17000404 RID: 1028
		// (get) Token: 0x06000CEC RID: 3308 RVA: 0x00011CF9 File Offset: 0x0000FEF9
		public productServiceRetailResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (productServiceRetailResponse)this.results[0];
			}
		}

		// Token: 0x040004CA RID: 1226
		private object[] results;
	}
}
