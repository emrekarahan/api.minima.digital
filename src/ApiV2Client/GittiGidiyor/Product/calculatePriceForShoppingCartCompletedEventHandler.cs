﻿using System.CodeDom.Compiler;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x0200013A RID: 314
	// (Invoke) Token: 0x06000C28 RID: 3112
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	public delegate void calculatePriceForShoppingCartCompletedEventHandler(object sender, calculatePriceForShoppingCartCompletedEventArgs e);
}
