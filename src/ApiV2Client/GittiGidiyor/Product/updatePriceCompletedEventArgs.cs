﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x02000175 RID: 373
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	public class updatePriceCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000CD9 RID: 3289 RVA: 0x00011C6E File Offset: 0x0000FE6E
		internal updatePriceCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x17000401 RID: 1025
		// (get) Token: 0x06000CDA RID: 3290 RVA: 0x00011C81 File Offset: 0x0000FE81
		public productServiceResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (productServiceResponse)this.results[0];
			}
		}

		// Token: 0x040004C7 RID: 1223
		private object[] results;
	}
}
