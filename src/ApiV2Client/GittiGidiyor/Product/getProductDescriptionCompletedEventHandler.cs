﻿using System.CodeDom.Compiler;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x0200014E RID: 334
	// (Invoke) Token: 0x06000C64 RID: 3172
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	public delegate void getProductDescriptionCompletedEventHandler(object sender, getProductDescriptionCompletedEventArgs e);
}
