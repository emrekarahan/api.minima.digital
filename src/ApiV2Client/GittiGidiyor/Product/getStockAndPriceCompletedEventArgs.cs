﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x02000163 RID: 355
	[DesignerCategory("code")]
	[DebuggerStepThrough]
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	public class getStockAndPriceCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000CA3 RID: 3235 RVA: 0x00011B06 File Offset: 0x0000FD06
		internal getStockAndPriceCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x170003F8 RID: 1016
		// (get) Token: 0x06000CA4 RID: 3236 RVA: 0x00011B19 File Offset: 0x0000FD19
		public productServiceStockResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (productServiceStockResponse)this.results[0];
			}
		}

		// Token: 0x040004BE RID: 1214
		private object[] results;
	}
}
