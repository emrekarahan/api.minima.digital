﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x02000149 RID: 329
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	public class getItemIdDetailsCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000C55 RID: 3157 RVA: 0x000118FE File Offset: 0x0000FAFE
		internal getItemIdDetailsCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x170003EB RID: 1003
		// (get) Token: 0x06000C56 RID: 3158 RVA: 0x00011911 File Offset: 0x0000FB11
		public itemIdDetailResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (itemIdDetailResponse)this.results[0];
			}
		}

		// Token: 0x040004B1 RID: 1201
		private object[] results;
	}
}
