﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x02000181 RID: 385
	[DesignerCategory("code")]
	[DebuggerStepThrough]
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	public class updateStockCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000CFD RID: 3325 RVA: 0x00011D5E File Offset: 0x0000FF5E
		internal updateStockCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x17000407 RID: 1031
		// (get) Token: 0x06000CFE RID: 3326 RVA: 0x00011D71 File Offset: 0x0000FF71
		public productServiceResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (productServiceResponse)this.results[0];
			}
		}

		// Token: 0x040004CD RID: 1229
		private object[] results;
	}
}
