﻿using System.CodeDom.Compiler;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x02000168 RID: 360
	// (Invoke) Token: 0x06000CB2 RID: 3250
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	public delegate void insertRetailProductCompletedEventHandler(object sender, insertRetailProductCompletedEventArgs e);
}
