﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x0200011C RID: 284
	[DesignerCategory("code")]
	[DebuggerStepThrough]
	[XmlType(Namespace = "https://product.individual.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.7.3190.0")]
	[Serializable]
	public class itemIdDetailType
	{
		// Token: 0x17000341 RID: 833
		// (get) Token: 0x06000AC2 RID: 2754 RVA: 0x00010C24 File Offset: 0x0000EE24
		// (set) Token: 0x06000AC3 RID: 2755 RVA: 0x00010C2C File Offset: 0x0000EE2C
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int productId
		{
			get
			{
				return this.productIdField;
			}
			set
			{
				this.productIdField = value;
			}
		}

		// Token: 0x17000342 RID: 834
		// (get) Token: 0x06000AC4 RID: 2756 RVA: 0x00010C35 File Offset: 0x0000EE35
		// (set) Token: 0x06000AC5 RID: 2757 RVA: 0x00010C3D File Offset: 0x0000EE3D
		[XmlIgnore]
		public bool productIdSpecified
		{
			get
			{
				return this.productIdFieldSpecified;
			}
			set
			{
				this.productIdFieldSpecified = value;
			}
		}

		// Token: 0x17000343 RID: 835
		// (get) Token: 0x06000AC6 RID: 2758 RVA: 0x00010C46 File Offset: 0x0000EE46
		// (set) Token: 0x06000AC7 RID: 2759 RVA: 0x00010C4E File Offset: 0x0000EE4E
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string itemId
		{
			get
			{
				return this.itemIdField;
			}
			set
			{
				this.itemIdField = value;
			}
		}

		// Token: 0x17000344 RID: 836
		// (get) Token: 0x06000AC8 RID: 2760 RVA: 0x00010C57 File Offset: 0x0000EE57
		// (set) Token: 0x06000AC9 RID: 2761 RVA: 0x00010C5F File Offset: 0x0000EE5F
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public itemIdDetailStatus status
		{
			get
			{
				return this.statusField;
			}
			set
			{
				this.statusField = value;
			}
		}

		// Token: 0x17000345 RID: 837
		// (get) Token: 0x06000ACA RID: 2762 RVA: 0x00010C68 File Offset: 0x0000EE68
		// (set) Token: 0x06000ACB RID: 2763 RVA: 0x00010C70 File Offset: 0x0000EE70
		[XmlIgnore]
		public bool statusSpecified
		{
			get
			{
				return this.statusFieldSpecified;
			}
			set
			{
				this.statusFieldSpecified = value;
			}
		}

		// Token: 0x04000403 RID: 1027
		private int productIdField;

		// Token: 0x04000404 RID: 1028
		private bool productIdFieldSpecified;

		// Token: 0x04000405 RID: 1029
		private string itemIdField;

		// Token: 0x04000406 RID: 1030
		private itemIdDetailStatus statusField;

		// Token: 0x04000407 RID: 1031
		private bool statusFieldSpecified;
	}
}
