﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x02000115 RID: 277
	[XmlType(Namespace = "https://product.individual.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.7.3190.0")]
	[DesignerCategory("code")]
	[DebuggerStepThrough]
	[Serializable]
	public class variantGroupType
	{
		// Token: 0x1700030B RID: 779
		// (get) Token: 0x06000A4F RID: 2639 RVA: 0x00010856 File Offset: 0x0000EA56
		// (set) Token: 0x06000A50 RID: 2640 RVA: 0x0001085E File Offset: 0x0000EA5E
		[XmlArrayItem("variant", Form = XmlSchemaForm.Unqualified, IsNullable = false)]
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		public variantType[] variants
		{
			get
			{
				return this.variantsField;
			}
			set
			{
				this.variantsField = value;
			}
		}

		// Token: 0x1700030C RID: 780
		// (get) Token: 0x06000A51 RID: 2641 RVA: 0x00010867 File Offset: 0x0000EA67
		// (set) Token: 0x06000A52 RID: 2642 RVA: 0x0001086F File Offset: 0x0000EA6F
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		[XmlArrayItem("photo", Form = XmlSchemaForm.Unqualified, IsNullable = false)]
		public photoType[] photos
		{
			get
			{
				return this.photosField;
			}
			set
			{
				this.photosField = value;
			}
		}

		// Token: 0x1700030D RID: 781
		// (get) Token: 0x06000A53 RID: 2643 RVA: 0x00010878 File Offset: 0x0000EA78
		// (set) Token: 0x06000A54 RID: 2644 RVA: 0x00010880 File Offset: 0x0000EA80
		[XmlAttribute]
		public long nameId
		{
			get
			{
				return this.nameIdField;
			}
			set
			{
				this.nameIdField = value;
			}
		}

		// Token: 0x1700030E RID: 782
		// (get) Token: 0x06000A55 RID: 2645 RVA: 0x00010889 File Offset: 0x0000EA89
		// (set) Token: 0x06000A56 RID: 2646 RVA: 0x00010891 File Offset: 0x0000EA91
		[XmlIgnore]
		public bool nameIdSpecified
		{
			get
			{
				return this.nameIdFieldSpecified;
			}
			set
			{
				this.nameIdFieldSpecified = value;
			}
		}

		// Token: 0x1700030F RID: 783
		// (get) Token: 0x06000A57 RID: 2647 RVA: 0x0001089A File Offset: 0x0000EA9A
		// (set) Token: 0x06000A58 RID: 2648 RVA: 0x000108A2 File Offset: 0x0000EAA2
		[XmlAttribute]
		public long valueId
		{
			get
			{
				return this.valueIdField;
			}
			set
			{
				this.valueIdField = value;
			}
		}

		// Token: 0x17000310 RID: 784
		// (get) Token: 0x06000A59 RID: 2649 RVA: 0x000108AB File Offset: 0x0000EAAB
		// (set) Token: 0x06000A5A RID: 2650 RVA: 0x000108B3 File Offset: 0x0000EAB3
		[XmlIgnore]
		public bool valueIdSpecified
		{
			get
			{
				return this.valueIdFieldSpecified;
			}
			set
			{
				this.valueIdFieldSpecified = value;
			}
		}

		// Token: 0x17000311 RID: 785
		// (get) Token: 0x06000A5B RID: 2651 RVA: 0x000108BC File Offset: 0x0000EABC
		// (set) Token: 0x06000A5C RID: 2652 RVA: 0x000108C4 File Offset: 0x0000EAC4
		[XmlAttribute]
		public string alias
		{
			get
			{
				return this.aliasField;
			}
			set
			{
				this.aliasField = value;
			}
		}

		// Token: 0x040003CD RID: 973
		private variantType[] variantsField;

		// Token: 0x040003CE RID: 974
		private photoType[] photosField;

		// Token: 0x040003CF RID: 975
		private long nameIdField;

		// Token: 0x040003D0 RID: 976
		private bool nameIdFieldSpecified;

		// Token: 0x040003D1 RID: 977
		private long valueIdField;

		// Token: 0x040003D2 RID: 978
		private bool valueIdFieldSpecified;

		// Token: 0x040003D3 RID: 979
		private string aliasField;
	}
}
