﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x02000124 RID: 292
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[XmlType(Namespace = "https://product.individual.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.7.3190.0")]
	[Serializable]
	public class cargoDetailType
	{
		// Token: 0x17000369 RID: 873
		// (get) Token: 0x06000B19 RID: 2841 RVA: 0x00010F04 File Offset: 0x0000F104
		// (set) Token: 0x06000B1A RID: 2842 RVA: 0x00010F0C File Offset: 0x0000F10C
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string city
		{
			get
			{
				return this.cityField;
			}
			set
			{
				this.cityField = value;
			}
		}

		// Token: 0x1700036A RID: 874
		// (get) Token: 0x06000B1B RID: 2843 RVA: 0x00010F15 File Offset: 0x0000F115
		// (set) Token: 0x06000B1C RID: 2844 RVA: 0x00010F1D File Offset: 0x0000F11D
		[XmlArrayItem("cargoCompany", Form = XmlSchemaForm.Unqualified, IsNullable = false)]
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		public string[] cargoCompanies
		{
			get
			{
				return this.cargoCompaniesField;
			}
			set
			{
				this.cargoCompaniesField = value;
			}
		}

		// Token: 0x1700036B RID: 875
		// (get) Token: 0x06000B1D RID: 2845 RVA: 0x00010F26 File Offset: 0x0000F126
		// (set) Token: 0x06000B1E RID: 2846 RVA: 0x00010F2E File Offset: 0x0000F12E
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string shippingPayment
		{
			get
			{
				return this.shippingPaymentField;
			}
			set
			{
				this.shippingPaymentField = value;
			}
		}

		// Token: 0x1700036C RID: 876
		// (get) Token: 0x06000B1F RID: 2847 RVA: 0x00010F37 File Offset: 0x0000F137
		// (set) Token: 0x06000B20 RID: 2848 RVA: 0x00010F3F File Offset: 0x0000F13F
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string cargoDescription
		{
			get
			{
				return this.cargoDescriptionField;
			}
			set
			{
				this.cargoDescriptionField = value;
			}
		}

		// Token: 0x1700036D RID: 877
		// (get) Token: 0x06000B21 RID: 2849 RVA: 0x00010F48 File Offset: 0x0000F148
		// (set) Token: 0x06000B22 RID: 2850 RVA: 0x00010F50 File Offset: 0x0000F150
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string shippingWhere
		{
			get
			{
				return this.shippingWhereField;
			}
			set
			{
				this.shippingWhereField = value;
			}
		}

		// Token: 0x1700036E RID: 878
		// (get) Token: 0x06000B23 RID: 2851 RVA: 0x00010F59 File Offset: 0x0000F159
		// (set) Token: 0x06000B24 RID: 2852 RVA: 0x00010F61 File Offset: 0x0000F161
		[XmlArrayItem("cargoCompanyDetail", Form = XmlSchemaForm.Unqualified, IsNullable = false)]
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		public cargoCompanyDetailType[] cargoCompanyDetails
		{
			get
			{
				return this.cargoCompanyDetailsField;
			}
			set
			{
				this.cargoCompanyDetailsField = value;
			}
		}

		// Token: 0x1700036F RID: 879
		// (get) Token: 0x06000B25 RID: 2853 RVA: 0x00010F6A File Offset: 0x0000F16A
		// (set) Token: 0x06000B26 RID: 2854 RVA: 0x00010F72 File Offset: 0x0000F172
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public cargoTimeType shippingTime
		{
			get
			{
				return this.shippingTimeField;
			}
			set
			{
				this.shippingTimeField = value;
			}
		}

		// Token: 0x0400042F RID: 1071
		private string cityField;

		// Token: 0x04000430 RID: 1072
		private string[] cargoCompaniesField;

		// Token: 0x04000431 RID: 1073
		private string shippingPaymentField;

		// Token: 0x04000432 RID: 1074
		private string cargoDescriptionField;

		// Token: 0x04000433 RID: 1075
		private string shippingWhereField;

		// Token: 0x04000434 RID: 1076
		private cargoCompanyDetailType[] cargoCompanyDetailsField;

		// Token: 0x04000435 RID: 1077
		private cargoTimeType shippingTimeField;
	}
}
