﻿using System.CodeDom.Compiler;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x02000156 RID: 342
	// (Invoke) Token: 0x06000C7C RID: 3196
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	public delegate void getProductSpecsCompletedEventHandler(object sender, getProductSpecsCompletedEventArgs e);
}
