﻿using System.CodeDom.Compiler;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x0200016A RID: 362
	// (Invoke) Token: 0x06000CB8 RID: 3256
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	public delegate void insertRetailProductWithNewCargoDetailCompletedEventHandler(object sender, insertRetailProductWithNewCargoDetailCompletedEventArgs e);
}
