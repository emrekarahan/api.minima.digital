﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x0200013D RID: 317
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	public class checkForItemIdCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000C31 RID: 3121 RVA: 0x0001180E File Offset: 0x0000FA0E
		internal checkForItemIdCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x170003E5 RID: 997
		// (get) Token: 0x06000C32 RID: 3122 RVA: 0x00011821 File Offset: 0x0000FA21
		public productServiceBooleanResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (productServiceBooleanResponse)this.results[0];
			}
		}

		// Token: 0x040004AB RID: 1195
		private object[] results;
	}
}
