﻿using System.CodeDom.Compiler;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x02000180 RID: 384
	// (Invoke) Token: 0x06000CFA RID: 3322
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	public delegate void updateStockCompletedEventHandler(object sender, updateStockCompletedEventArgs e);
}
