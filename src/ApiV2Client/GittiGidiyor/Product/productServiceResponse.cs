﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x02000132 RID: 306
	[GeneratedCode("System.Xml", "4.7.3190.0")]
	[DesignerCategory("code")]
	[XmlType(Namespace = "https://product.individual.ws.listingapi.gg.com")]
	[DebuggerStepThrough]
	[Serializable]
	public class productServiceResponse : baseResponse
	{
		// Token: 0x170003C3 RID: 963
		// (get) Token: 0x06000BDB RID: 3035 RVA: 0x0001156E File Offset: 0x0000F76E
		// (set) Token: 0x06000BDC RID: 3036 RVA: 0x00011576 File Offset: 0x0000F776
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int productId
		{
			get
			{
				return this.productIdField;
			}
			set
			{
				this.productIdField = value;
			}
		}

		// Token: 0x170003C4 RID: 964
		// (get) Token: 0x06000BDD RID: 3037 RVA: 0x0001157F File Offset: 0x0000F77F
		// (set) Token: 0x06000BDE RID: 3038 RVA: 0x00011587 File Offset: 0x0000F787
		[XmlIgnore]
		public bool productIdSpecified
		{
			get
			{
				return this.productIdFieldSpecified;
			}
			set
			{
				this.productIdFieldSpecified = value;
			}
		}

		// Token: 0x170003C5 RID: 965
		// (get) Token: 0x06000BDF RID: 3039 RVA: 0x00011590 File Offset: 0x0000F790
		// (set) Token: 0x06000BE0 RID: 3040 RVA: 0x00011598 File Offset: 0x0000F798
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public bool payRequired
		{
			get
			{
				return this.payRequiredField;
			}
			set
			{
				this.payRequiredField = value;
			}
		}

		// Token: 0x170003C6 RID: 966
		// (get) Token: 0x06000BE1 RID: 3041 RVA: 0x000115A1 File Offset: 0x0000F7A1
		// (set) Token: 0x06000BE2 RID: 3042 RVA: 0x000115A9 File Offset: 0x0000F7A9
		[XmlIgnore]
		public bool payRequiredSpecified
		{
			get
			{
				return this.payRequiredFieldSpecified;
			}
			set
			{
				this.payRequiredFieldSpecified = value;
			}
		}

		// Token: 0x170003C7 RID: 967
		// (get) Token: 0x06000BE3 RID: 3043 RVA: 0x000115B2 File Offset: 0x0000F7B2
		// (set) Token: 0x06000BE4 RID: 3044 RVA: 0x000115BA File Offset: 0x0000F7BA
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string result
		{
			get
			{
				return this.resultField;
			}
			set
			{
				this.resultField = value;
			}
		}

		// Token: 0x170003C8 RID: 968
		// (get) Token: 0x06000BE5 RID: 3045 RVA: 0x000115C3 File Offset: 0x0000F7C3
		// (set) Token: 0x06000BE6 RID: 3046 RVA: 0x000115CB File Offset: 0x0000F7CB
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int descriptionFilterStatus
		{
			get
			{
				return this.descriptionFilterStatusField;
			}
			set
			{
				this.descriptionFilterStatusField = value;
			}
		}

		// Token: 0x170003C9 RID: 969
		// (get) Token: 0x06000BE7 RID: 3047 RVA: 0x000115D4 File Offset: 0x0000F7D4
		// (set) Token: 0x06000BE8 RID: 3048 RVA: 0x000115DC File Offset: 0x0000F7DC
		[XmlIgnore]
		public bool descriptionFilterStatusSpecified
		{
			get
			{
				return this.descriptionFilterStatusFieldSpecified;
			}
			set
			{
				this.descriptionFilterStatusFieldSpecified = value;
			}
		}

		// Token: 0x170003CA RID: 970
		// (get) Token: 0x06000BE9 RID: 3049 RVA: 0x000115E5 File Offset: 0x0000F7E5
		// (set) Token: 0x06000BEA RID: 3050 RVA: 0x000115ED File Offset: 0x0000F7ED
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		[XmlArrayItem("variantGroup", Form = XmlSchemaForm.Unqualified, IsNullable = false)]
		public variantGroupType[] variantGroups
		{
			get
			{
				return this.variantGroupsField;
			}
			set
			{
				this.variantGroupsField = value;
			}
		}

		// Token: 0x04000489 RID: 1161
		private int productIdField;

		// Token: 0x0400048A RID: 1162
		private bool productIdFieldSpecified;

		// Token: 0x0400048B RID: 1163
		private bool payRequiredField;

		// Token: 0x0400048C RID: 1164
		private bool payRequiredFieldSpecified;

		// Token: 0x0400048D RID: 1165
		private string resultField;

		// Token: 0x0400048E RID: 1166
		private int descriptionFilterStatusField;

		// Token: 0x0400048F RID: 1167
		private bool descriptionFilterStatusFieldSpecified;

		// Token: 0x04000490 RID: 1168
		private variantGroupType[] variantGroupsField;
	}
}
