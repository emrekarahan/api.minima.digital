﻿using System.CodeDom.Compiler;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x0200015C RID: 348
	// (Invoke) Token: 0x06000C8E RID: 3214
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	public delegate void getProductsCompletedEventHandler(object sender, getProductsCompletedEventArgs e);
}
