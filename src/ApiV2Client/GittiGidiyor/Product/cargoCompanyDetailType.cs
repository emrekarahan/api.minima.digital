﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x02000123 RID: 291
	[XmlType(Namespace = "https://product.individual.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.7.3190.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[Serializable]
	public class cargoCompanyDetailType
	{
		// Token: 0x17000365 RID: 869
		// (get) Token: 0x06000B10 RID: 2832 RVA: 0x00010EB8 File Offset: 0x0000F0B8
		// (set) Token: 0x06000B11 RID: 2833 RVA: 0x00010EC0 File Offset: 0x0000F0C0
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string name
		{
			get
			{
				return this.nameField;
			}
			set
			{
				this.nameField = value;
			}
		}

		// Token: 0x17000366 RID: 870
		// (get) Token: 0x06000B12 RID: 2834 RVA: 0x00010EC9 File Offset: 0x0000F0C9
		// (set) Token: 0x06000B13 RID: 2835 RVA: 0x00010ED1 File Offset: 0x0000F0D1
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string value
		{
			get
			{
				return this.valueField;
			}
			set
			{
				this.valueField = value;
			}
		}

		// Token: 0x17000367 RID: 871
		// (get) Token: 0x06000B14 RID: 2836 RVA: 0x00010EDA File Offset: 0x0000F0DA
		// (set) Token: 0x06000B15 RID: 2837 RVA: 0x00010EE2 File Offset: 0x0000F0E2
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string cityPrice
		{
			get
			{
				return this.cityPriceField;
			}
			set
			{
				this.cityPriceField = value;
			}
		}

		// Token: 0x17000368 RID: 872
		// (get) Token: 0x06000B16 RID: 2838 RVA: 0x00010EEB File Offset: 0x0000F0EB
		// (set) Token: 0x06000B17 RID: 2839 RVA: 0x00010EF3 File Offset: 0x0000F0F3
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string countryPrice
		{
			get
			{
				return this.countryPriceField;
			}
			set
			{
				this.countryPriceField = value;
			}
		}

		// Token: 0x0400042B RID: 1067
		private string nameField;

		// Token: 0x0400042C RID: 1068
		private string valueField;

		// Token: 0x0400042D RID: 1069
		private string cityPriceField;

		// Token: 0x0400042E RID: 1070
		private string countryPriceField;
	}
}
