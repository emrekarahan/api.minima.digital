﻿using System.CodeDom.Compiler;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x0200015E RID: 350
	// (Invoke) Token: 0x06000C94 RID: 3220
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	public delegate void getProductsByIdsCompletedEventHandler(object sender, getProductsByIdsCompletedEventArgs e);
}
