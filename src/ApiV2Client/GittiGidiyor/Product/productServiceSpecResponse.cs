﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x02000130 RID: 304
	[GeneratedCode("System.Xml", "4.7.3190.0")]
	[DesignerCategory("code")]
	[XmlType(Namespace = "https://product.individual.ws.listingapi.gg.com")]
	[DebuggerStepThrough]
	[Serializable]
	public class productServiceSpecResponse : baseResponse
	{
		// Token: 0x170003BC RID: 956
		// (get) Token: 0x06000BCB RID: 3019 RVA: 0x000114E7 File Offset: 0x0000F6E7
		// (set) Token: 0x06000BCC RID: 3020 RVA: 0x000114EF File Offset: 0x0000F6EF
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int productId
		{
			get
			{
				return this.productIdField;
			}
			set
			{
				this.productIdField = value;
			}
		}

		// Token: 0x170003BD RID: 957
		// (get) Token: 0x06000BCD RID: 3021 RVA: 0x000114F8 File Offset: 0x0000F6F8
		// (set) Token: 0x06000BCE RID: 3022 RVA: 0x00011500 File Offset: 0x0000F700
		[XmlIgnore]
		public bool productIdSpecified
		{
			get
			{
				return this.productIdFieldSpecified;
			}
			set
			{
				this.productIdFieldSpecified = value;
			}
		}

		// Token: 0x170003BE RID: 958
		// (get) Token: 0x06000BCF RID: 3023 RVA: 0x00011509 File Offset: 0x0000F709
		// (set) Token: 0x06000BD0 RID: 3024 RVA: 0x00011511 File Offset: 0x0000F711
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string itemId
		{
			get
			{
				return this.itemIdField;
			}
			set
			{
				this.itemIdField = value;
			}
		}

		// Token: 0x170003BF RID: 959
		// (get) Token: 0x06000BD1 RID: 3025 RVA: 0x0001151A File Offset: 0x0000F71A
		// (set) Token: 0x06000BD2 RID: 3026 RVA: 0x00011522 File Offset: 0x0000F722
		[XmlArrayItem("spec", Form = XmlSchemaForm.Unqualified, IsNullable = false)]
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		public specType[] specs
		{
			get
			{
				return this.specsField;
			}
			set
			{
				this.specsField = value;
			}
		}

		// Token: 0x04000482 RID: 1154
		private int productIdField;

		// Token: 0x04000483 RID: 1155
		private bool productIdFieldSpecified;

		// Token: 0x04000484 RID: 1156
		private string itemIdField;

		// Token: 0x04000485 RID: 1157
		private specType[] specsField;
	}
}
