﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x02000122 RID: 290
	[DesignerCategory("code")]
	[GeneratedCode("System.Xml", "4.7.3190.0")]
	[DebuggerStepThrough]
	[XmlType(Namespace = "https://product.individual.ws.listingapi.gg.com")]
	[Serializable]
	public class cargoTimeType
	{
		// Token: 0x17000363 RID: 867
		// (get) Token: 0x06000B0B RID: 2827 RVA: 0x00010E8E File Offset: 0x0000F08E
		// (set) Token: 0x06000B0C RID: 2828 RVA: 0x00010E96 File Offset: 0x0000F096
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string days
		{
			get
			{
				return this.daysField;
			}
			set
			{
				this.daysField = value;
			}
		}

		// Token: 0x17000364 RID: 868
		// (get) Token: 0x06000B0D RID: 2829 RVA: 0x00010E9F File Offset: 0x0000F09F
		// (set) Token: 0x06000B0E RID: 2830 RVA: 0x00010EA7 File Offset: 0x0000F0A7
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string beforeTime
		{
			get
			{
				return this.beforeTimeField;
			}
			set
			{
				this.beforeTimeField = value;
			}
		}

		// Token: 0x04000429 RID: 1065
		private string daysField;

		// Token: 0x0400042A RID: 1066
		private string beforeTimeField;
	}
}
