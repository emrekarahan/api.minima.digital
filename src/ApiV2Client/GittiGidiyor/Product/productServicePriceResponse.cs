﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x02000112 RID: 274
	[DebuggerStepThrough]
	[XmlType(Namespace = "https://product.individual.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.7.3190.0")]
	[DesignerCategory("code")]
	[Serializable]
	public class productServicePriceResponse : baseResponse
	{
		// Token: 0x170002FE RID: 766
		// (get) Token: 0x06000A32 RID: 2610 RVA: 0x00010761 File Offset: 0x0000E961
		// (set) Token: 0x06000A33 RID: 2611 RVA: 0x00010769 File Offset: 0x0000E969
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string paymentVoucher
		{
			get
			{
				return this.paymentVoucherField;
			}
			set
			{
				this.paymentVoucherField = value;
			}
		}

		// Token: 0x170002FF RID: 767
		// (get) Token: 0x06000A34 RID: 2612 RVA: 0x00010772 File Offset: 0x0000E972
		// (set) Token: 0x06000A35 RID: 2613 RVA: 0x0001077A File Offset: 0x0000E97A
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public double price
		{
			get
			{
				return this.priceField;
			}
			set
			{
				this.priceField = value;
			}
		}

		// Token: 0x17000300 RID: 768
		// (get) Token: 0x06000A36 RID: 2614 RVA: 0x00010783 File Offset: 0x0000E983
		// (set) Token: 0x06000A37 RID: 2615 RVA: 0x0001078B File Offset: 0x0000E98B
		[XmlIgnore]
		public bool priceSpecified
		{
			get
			{
				return this.priceFieldSpecified;
			}
			set
			{
				this.priceFieldSpecified = value;
			}
		}

		// Token: 0x17000301 RID: 769
		// (get) Token: 0x06000A38 RID: 2616 RVA: 0x00010794 File Offset: 0x0000E994
		// (set) Token: 0x06000A39 RID: 2617 RVA: 0x0001079C File Offset: 0x0000E99C
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string message
		{
			get
			{
				return this.messageField;
			}
			set
			{
				this.messageField = value;
			}
		}

		// Token: 0x17000302 RID: 770
		// (get) Token: 0x06000A3A RID: 2618 RVA: 0x000107A5 File Offset: 0x0000E9A5
		// (set) Token: 0x06000A3B RID: 2619 RVA: 0x000107AD File Offset: 0x0000E9AD
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public bool payRequired
		{
			get
			{
				return this.payRequiredField;
			}
			set
			{
				this.payRequiredField = value;
			}
		}

		// Token: 0x17000303 RID: 771
		// (get) Token: 0x06000A3C RID: 2620 RVA: 0x000107B6 File Offset: 0x0000E9B6
		// (set) Token: 0x06000A3D RID: 2621 RVA: 0x000107BE File Offset: 0x0000E9BE
		[XmlIgnore]
		public bool payRequiredSpecified
		{
			get
			{
				return this.payRequiredFieldSpecified;
			}
			set
			{
				this.payRequiredFieldSpecified = value;
			}
		}

		// Token: 0x17000304 RID: 772
		// (get) Token: 0x06000A3E RID: 2622 RVA: 0x000107C7 File Offset: 0x0000E9C7
		// (set) Token: 0x06000A3F RID: 2623 RVA: 0x000107CF File Offset: 0x0000E9CF
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int productCount
		{
			get
			{
				return this.productCountField;
			}
			set
			{
				this.productCountField = value;
			}
		}

		// Token: 0x17000305 RID: 773
		// (get) Token: 0x06000A40 RID: 2624 RVA: 0x000107D8 File Offset: 0x0000E9D8
		// (set) Token: 0x06000A41 RID: 2625 RVA: 0x000107E0 File Offset: 0x0000E9E0
		[XmlIgnore]
		public bool productCountSpecified
		{
			get
			{
				return this.productCountFieldSpecified;
			}
			set
			{
				this.productCountFieldSpecified = value;
			}
		}

		// Token: 0x040003C0 RID: 960
		private string paymentVoucherField;

		// Token: 0x040003C1 RID: 961
		private double priceField;

		// Token: 0x040003C2 RID: 962
		private bool priceFieldSpecified;

		// Token: 0x040003C3 RID: 963
		private string messageField;

		// Token: 0x040003C4 RID: 964
		private bool payRequiredField;

		// Token: 0x040003C5 RID: 965
		private bool payRequiredFieldSpecified;

		// Token: 0x040003C6 RID: 966
		private int productCountField;

		// Token: 0x040003C7 RID: 967
		private bool productCountFieldSpecified;
	}
}
