﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x02000117 RID: 279
	[XmlType(Namespace = "https://product.individual.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.7.3190.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[Serializable]
	public class variantSpecType
	{
		// Token: 0x1700031D RID: 797
		// (get) Token: 0x06000A75 RID: 2677 RVA: 0x00010998 File Offset: 0x0000EB98
		// (set) Token: 0x06000A76 RID: 2678 RVA: 0x000109A0 File Offset: 0x0000EBA0
		[XmlAttribute]
		public long nameId
		{
			get
			{
				return this.nameIdField;
			}
			set
			{
				this.nameIdField = value;
			}
		}

		// Token: 0x1700031E RID: 798
		// (get) Token: 0x06000A77 RID: 2679 RVA: 0x000109A9 File Offset: 0x0000EBA9
		// (set) Token: 0x06000A78 RID: 2680 RVA: 0x000109B1 File Offset: 0x0000EBB1
		[XmlIgnore]
		public bool nameIdSpecified
		{
			get
			{
				return this.nameIdFieldSpecified;
			}
			set
			{
				this.nameIdFieldSpecified = value;
			}
		}

		// Token: 0x1700031F RID: 799
		// (get) Token: 0x06000A79 RID: 2681 RVA: 0x000109BA File Offset: 0x0000EBBA
		// (set) Token: 0x06000A7A RID: 2682 RVA: 0x000109C2 File Offset: 0x0000EBC2
		[XmlAttribute]
		public string name
		{
			get
			{
				return this.nameField;
			}
			set
			{
				this.nameField = value;
			}
		}

		// Token: 0x17000320 RID: 800
		// (get) Token: 0x06000A7B RID: 2683 RVA: 0x000109CB File Offset: 0x0000EBCB
		// (set) Token: 0x06000A7C RID: 2684 RVA: 0x000109D3 File Offset: 0x0000EBD3
		[XmlAttribute]
		public long valueId
		{
			get
			{
				return this.valueIdField;
			}
			set
			{
				this.valueIdField = value;
			}
		}

		// Token: 0x17000321 RID: 801
		// (get) Token: 0x06000A7D RID: 2685 RVA: 0x000109DC File Offset: 0x0000EBDC
		// (set) Token: 0x06000A7E RID: 2686 RVA: 0x000109E4 File Offset: 0x0000EBE4
		[XmlIgnore]
		public bool valueIdSpecified
		{
			get
			{
				return this.valueIdFieldSpecified;
			}
			set
			{
				this.valueIdFieldSpecified = value;
			}
		}

		// Token: 0x17000322 RID: 802
		// (get) Token: 0x06000A7F RID: 2687 RVA: 0x000109ED File Offset: 0x0000EBED
		// (set) Token: 0x06000A80 RID: 2688 RVA: 0x000109F5 File Offset: 0x0000EBF5
		[XmlAttribute]
		public string value
		{
			get
			{
				return this.valueField;
			}
			set
			{
				this.valueField = value;
			}
		}

		// Token: 0x17000323 RID: 803
		// (get) Token: 0x06000A81 RID: 2689 RVA: 0x000109FE File Offset: 0x0000EBFE
		// (set) Token: 0x06000A82 RID: 2690 RVA: 0x00010A06 File Offset: 0x0000EC06
		[XmlAttribute]
		public int orderNumber
		{
			get
			{
				return this.orderNumberField;
			}
			set
			{
				this.orderNumberField = value;
			}
		}

		// Token: 0x17000324 RID: 804
		// (get) Token: 0x06000A83 RID: 2691 RVA: 0x00010A0F File Offset: 0x0000EC0F
		// (set) Token: 0x06000A84 RID: 2692 RVA: 0x00010A17 File Offset: 0x0000EC17
		[XmlIgnore]
		public bool orderNumberSpecified
		{
			get
			{
				return this.orderNumberFieldSpecified;
			}
			set
			{
				this.orderNumberFieldSpecified = value;
			}
		}

		// Token: 0x17000325 RID: 805
		// (get) Token: 0x06000A85 RID: 2693 RVA: 0x00010A20 File Offset: 0x0000EC20
		// (set) Token: 0x06000A86 RID: 2694 RVA: 0x00010A28 File Offset: 0x0000EC28
		[XmlAttribute]
		public int specDataOrderNumber
		{
			get
			{
				return this.specDataOrderNumberField;
			}
			set
			{
				this.specDataOrderNumberField = value;
			}
		}

		// Token: 0x17000326 RID: 806
		// (get) Token: 0x06000A87 RID: 2695 RVA: 0x00010A31 File Offset: 0x0000EC31
		// (set) Token: 0x06000A88 RID: 2696 RVA: 0x00010A39 File Offset: 0x0000EC39
		[XmlIgnore]
		public bool specDataOrderNumberSpecified
		{
			get
			{
				return this.specDataOrderNumberFieldSpecified;
			}
			set
			{
				this.specDataOrderNumberFieldSpecified = value;
			}
		}

		// Token: 0x040003DF RID: 991
		private long nameIdField;

		// Token: 0x040003E0 RID: 992
		private bool nameIdFieldSpecified;

		// Token: 0x040003E1 RID: 993
		private string nameField;

		// Token: 0x040003E2 RID: 994
		private long valueIdField;

		// Token: 0x040003E3 RID: 995
		private bool valueIdFieldSpecified;

		// Token: 0x040003E4 RID: 996
		private string valueField;

		// Token: 0x040003E5 RID: 997
		private int orderNumberField;

		// Token: 0x040003E6 RID: 998
		private bool orderNumberFieldSpecified;

		// Token: 0x040003E7 RID: 999
		private int specDataOrderNumberField;

		// Token: 0x040003E8 RID: 1000
		private bool specDataOrderNumberFieldSpecified;
	}
}
