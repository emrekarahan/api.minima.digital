﻿using System.CodeDom.Compiler;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x02000182 RID: 386
	// (Invoke) Token: 0x06000D00 RID: 3328
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	public delegate void updateVariantStockCompletedEventHandler(object sender, updateVariantStockCompletedEventArgs e);
}
