﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x02000139 RID: 313
	[DesignerCategory("code")]
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	[DebuggerStepThrough]
	public class calculatePriceForRevisionCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000C25 RID: 3109 RVA: 0x000117BE File Offset: 0x0000F9BE
		internal calculatePriceForRevisionCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x170003E3 RID: 995
		// (get) Token: 0x06000C26 RID: 3110 RVA: 0x000117D1 File Offset: 0x0000F9D1
		public productServicePriceResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (productServicePriceResponse)this.results[0];
			}
		}

		// Token: 0x040004A9 RID: 1193
		private object[] results;
	}
}
