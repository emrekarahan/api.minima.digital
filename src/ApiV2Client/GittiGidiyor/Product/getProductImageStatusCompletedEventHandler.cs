﻿using System.CodeDom.Compiler;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x02000154 RID: 340
	// (Invoke) Token: 0x06000C76 RID: 3190
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	public delegate void getProductImageStatusCompletedEventHandler(object sender, getProductImageStatusCompletedEventArgs e);
}
