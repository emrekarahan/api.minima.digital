﻿using System.CodeDom.Compiler;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x0200014A RID: 330
	// (Invoke) Token: 0x06000C58 RID: 3160
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	public delegate void getNewlyListedProductIdListCompletedEventHandler(object sender, getNewlyListedProductIdListCompletedEventArgs e);
}
