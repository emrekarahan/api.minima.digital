﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x0200012B RID: 299
	[XmlType(Namespace = "https://product.individual.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.7.3190.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[Serializable]
	public class productServicePaymentResponse : baseResponse
	{
		// Token: 0x170003AF RID: 943
		// (get) Token: 0x06000BAC RID: 2988 RVA: 0x000113E2 File Offset: 0x0000F5E2
		// (set) Token: 0x06000BAD RID: 2989 RVA: 0x000113EA File Offset: 0x0000F5EA
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string paidPrice
		{
			get
			{
				return this.paidPriceField;
			}
			set
			{
				this.paidPriceField = value;
			}
		}

		// Token: 0x170003B0 RID: 944
		// (get) Token: 0x06000BAE RID: 2990 RVA: 0x000113F3 File Offset: 0x0000F5F3
		// (set) Token: 0x06000BAF RID: 2991 RVA: 0x000113FB File Offset: 0x0000F5FB
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int productCount
		{
			get
			{
				return this.productCountField;
			}
			set
			{
				this.productCountField = value;
			}
		}

		// Token: 0x170003B1 RID: 945
		// (get) Token: 0x06000BB0 RID: 2992 RVA: 0x00011404 File Offset: 0x0000F604
		// (set) Token: 0x06000BB1 RID: 2993 RVA: 0x0001140C File Offset: 0x0000F60C
		[XmlIgnore]
		public bool productCountSpecified
		{
			get
			{
				return this.productCountFieldSpecified;
			}
			set
			{
				this.productCountFieldSpecified = value;
			}
		}

		// Token: 0x170003B2 RID: 946
		// (get) Token: 0x06000BB2 RID: 2994 RVA: 0x00011415 File Offset: 0x0000F615
		// (set) Token: 0x06000BB3 RID: 2995 RVA: 0x0001141D File Offset: 0x0000F61D
		[XmlArrayItem("productId", Form = XmlSchemaForm.Unqualified, IsNullable = false)]
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		public int[] products
		{
			get
			{
				return this.productsField;
			}
			set
			{
				this.productsField = value;
			}
		}

		// Token: 0x170003B3 RID: 947
		// (get) Token: 0x06000BB4 RID: 2996 RVA: 0x00011426 File Offset: 0x0000F626
		// (set) Token: 0x06000BB5 RID: 2997 RVA: 0x0001142E File Offset: 0x0000F62E
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string message
		{
			get
			{
				return this.messageField;
			}
			set
			{
				this.messageField = value;
			}
		}

		// Token: 0x04000475 RID: 1141
		private string paidPriceField;

		// Token: 0x04000476 RID: 1142
		private int productCountField;

		// Token: 0x04000477 RID: 1143
		private bool productCountFieldSpecified;

		// Token: 0x04000478 RID: 1144
		private int[] productsField;

		// Token: 0x04000479 RID: 1145
		private string messageField;
	}
}
