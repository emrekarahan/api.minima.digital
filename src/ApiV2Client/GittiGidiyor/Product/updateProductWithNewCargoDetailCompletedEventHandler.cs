﻿using System.CodeDom.Compiler;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x0200017C RID: 380
	// (Invoke) Token: 0x06000CEE RID: 3310
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	public delegate void updateProductWithNewCargoDetailCompletedEventHandler(object sender, updateProductWithNewCargoDetailCompletedEventArgs e);
}
