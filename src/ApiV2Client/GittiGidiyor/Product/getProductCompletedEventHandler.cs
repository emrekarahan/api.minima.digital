﻿using System.CodeDom.Compiler;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x0200014C RID: 332
	// (Invoke) Token: 0x06000C5E RID: 3166
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	public delegate void getProductCompletedEventHandler(object sender, getProductCompletedEventArgs e);
}
