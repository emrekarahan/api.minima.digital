﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x0200016B RID: 363
	[DebuggerStepThrough]
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	[DesignerCategory("code")]
	public class insertRetailProductWithNewCargoDetailCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000CBB RID: 3259 RVA: 0x00011BA6 File Offset: 0x0000FDA6
		internal insertRetailProductWithNewCargoDetailCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x170003FC RID: 1020
		// (get) Token: 0x06000CBC RID: 3260 RVA: 0x00011BB9 File Offset: 0x0000FDB9
		public productServiceRetailResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (productServiceRetailResponse)this.results[0];
			}
		}

		// Token: 0x040004C2 RID: 1218
		private object[] results;
	}
}
