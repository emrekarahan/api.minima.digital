﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x0200014B RID: 331
	[DebuggerStepThrough]
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	[DesignerCategory("code")]
	public class getNewlyListedProductIdListCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000C5B RID: 3163 RVA: 0x00011926 File Offset: 0x0000FB26
		internal getNewlyListedProductIdListCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x170003EC RID: 1004
		// (get) Token: 0x06000C5C RID: 3164 RVA: 0x00011939 File Offset: 0x0000FB39
		public productServiceIdResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (productServiceIdResponse)this.results[0];
			}
		}

		// Token: 0x040004B2 RID: 1202
		private object[] results;
	}
}
