﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x0200011E RID: 286
	[GeneratedCode("System.Xml", "4.7.3190.0")]
	[XmlType(Namespace = "https://product.individual.ws.listingapi.gg.com")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[Serializable]
	public class stockDetailType
	{
		// Token: 0x17000346 RID: 838
		// (get) Token: 0x06000ACD RID: 2765 RVA: 0x00010C81 File Offset: 0x0000EE81
		// (set) Token: 0x06000ACE RID: 2766 RVA: 0x00010C89 File Offset: 0x0000EE89
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string itemId
		{
			get
			{
				return this.itemIdField;
			}
			set
			{
				this.itemIdField = value;
			}
		}

		// Token: 0x17000347 RID: 839
		// (get) Token: 0x06000ACF RID: 2767 RVA: 0x00010C92 File Offset: 0x0000EE92
		// (set) Token: 0x06000AD0 RID: 2768 RVA: 0x00010C9A File Offset: 0x0000EE9A
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int productId
		{
			get
			{
				return this.productIdField;
			}
			set
			{
				this.productIdField = value;
			}
		}

		// Token: 0x17000348 RID: 840
		// (get) Token: 0x06000AD1 RID: 2769 RVA: 0x00010CA3 File Offset: 0x0000EEA3
		// (set) Token: 0x06000AD2 RID: 2770 RVA: 0x00010CAB File Offset: 0x0000EEAB
		[XmlIgnore]
		public bool productIdSpecified
		{
			get
			{
				return this.productIdFieldSpecified;
			}
			set
			{
				this.productIdFieldSpecified = value;
			}
		}

		// Token: 0x17000349 RID: 841
		// (get) Token: 0x06000AD3 RID: 2771 RVA: 0x00010CB4 File Offset: 0x0000EEB4
		// (set) Token: 0x06000AD4 RID: 2772 RVA: 0x00010CBC File Offset: 0x0000EEBC
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string format
		{
			get
			{
				return this.formatField;
			}
			set
			{
				this.formatField = value;
			}
		}

		// Token: 0x1700034A RID: 842
		// (get) Token: 0x06000AD5 RID: 2773 RVA: 0x00010CC5 File Offset: 0x0000EEC5
		// (set) Token: 0x06000AD6 RID: 2774 RVA: 0x00010CCD File Offset: 0x0000EECD
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int amount
		{
			get
			{
				return this.amountField;
			}
			set
			{
				this.amountField = value;
			}
		}

		// Token: 0x1700034B RID: 843
		// (get) Token: 0x06000AD7 RID: 2775 RVA: 0x00010CD6 File Offset: 0x0000EED6
		// (set) Token: 0x06000AD8 RID: 2776 RVA: 0x00010CDE File Offset: 0x0000EEDE
		[XmlIgnore]
		public bool amountSpecified
		{
			get
			{
				return this.amountFieldSpecified;
			}
			set
			{
				this.amountFieldSpecified = value;
			}
		}

		// Token: 0x1700034C RID: 844
		// (get) Token: 0x06000AD9 RID: 2777 RVA: 0x00010CE7 File Offset: 0x0000EEE7
		// (set) Token: 0x06000ADA RID: 2778 RVA: 0x00010CEF File Offset: 0x0000EEEF
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int soldItemCount
		{
			get
			{
				return this.soldItemCountField;
			}
			set
			{
				this.soldItemCountField = value;
			}
		}

		// Token: 0x1700034D RID: 845
		// (get) Token: 0x06000ADB RID: 2779 RVA: 0x00010CF8 File Offset: 0x0000EEF8
		// (set) Token: 0x06000ADC RID: 2780 RVA: 0x00010D00 File Offset: 0x0000EF00
		[XmlIgnore]
		public bool soldItemCountSpecified
		{
			get
			{
				return this.soldItemCountFieldSpecified;
			}
			set
			{
				this.soldItemCountFieldSpecified = value;
			}
		}

		// Token: 0x1700034E RID: 846
		// (get) Token: 0x06000ADD RID: 2781 RVA: 0x00010D09 File Offset: 0x0000EF09
		// (set) Token: 0x06000ADE RID: 2782 RVA: 0x00010D11 File Offset: 0x0000EF11
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public double startPrice
		{
			get
			{
				return this.startPriceField;
			}
			set
			{
				this.startPriceField = value;
			}
		}

		// Token: 0x1700034F RID: 847
		// (get) Token: 0x06000ADF RID: 2783 RVA: 0x00010D1A File Offset: 0x0000EF1A
		// (set) Token: 0x06000AE0 RID: 2784 RVA: 0x00010D22 File Offset: 0x0000EF22
		[XmlIgnore]
		public bool startPriceSpecified
		{
			get
			{
				return this.startPriceFieldSpecified;
			}
			set
			{
				this.startPriceFieldSpecified = value;
			}
		}

		// Token: 0x17000350 RID: 848
		// (get) Token: 0x06000AE1 RID: 2785 RVA: 0x00010D2B File Offset: 0x0000EF2B
		// (set) Token: 0x06000AE2 RID: 2786 RVA: 0x00010D33 File Offset: 0x0000EF33
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public double buyNowPrice
		{
			get
			{
				return this.buyNowPriceField;
			}
			set
			{
				this.buyNowPriceField = value;
			}
		}

		// Token: 0x17000351 RID: 849
		// (get) Token: 0x06000AE3 RID: 2787 RVA: 0x00010D3C File Offset: 0x0000EF3C
		// (set) Token: 0x06000AE4 RID: 2788 RVA: 0x00010D44 File Offset: 0x0000EF44
		[XmlIgnore]
		public bool buyNowPriceSpecified
		{
			get
			{
				return this.buyNowPriceFieldSpecified;
			}
			set
			{
				this.buyNowPriceFieldSpecified = value;
			}
		}

		// Token: 0x0400040C RID: 1036
		private string itemIdField;

		// Token: 0x0400040D RID: 1037
		private int productIdField;

		// Token: 0x0400040E RID: 1038
		private bool productIdFieldSpecified;

		// Token: 0x0400040F RID: 1039
		private string formatField;

		// Token: 0x04000410 RID: 1040
		private int amountField;

		// Token: 0x04000411 RID: 1041
		private bool amountFieldSpecified;

		// Token: 0x04000412 RID: 1042
		private int soldItemCountField;

		// Token: 0x04000413 RID: 1043
		private bool soldItemCountFieldSpecified;

		// Token: 0x04000414 RID: 1044
		private double startPriceField;

		// Token: 0x04000415 RID: 1045
		private bool startPriceFieldSpecified;

		// Token: 0x04000416 RID: 1046
		private double buyNowPriceField;

		// Token: 0x04000417 RID: 1047
		private bool buyNowPriceFieldSpecified;
	}
}
