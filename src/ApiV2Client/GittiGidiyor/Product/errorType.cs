﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x02000113 RID: 275
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[XmlType(Namespace = "https://product.individual.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.7.3190.0")]
	[Serializable]
	public class errorType
	{
		// Token: 0x17000306 RID: 774
		// (get) Token: 0x06000A43 RID: 2627 RVA: 0x000107F1 File Offset: 0x0000E9F1
		// (set) Token: 0x06000A44 RID: 2628 RVA: 0x000107F9 File Offset: 0x0000E9F9
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string errorId
		{
			get
			{
				return this.errorIdField;
			}
			set
			{
				this.errorIdField = value;
			}
		}

		// Token: 0x17000307 RID: 775
		// (get) Token: 0x06000A45 RID: 2629 RVA: 0x00010802 File Offset: 0x0000EA02
		// (set) Token: 0x06000A46 RID: 2630 RVA: 0x0001080A File Offset: 0x0000EA0A
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string errorCode
		{
			get
			{
				return this.errorCodeField;
			}
			set
			{
				this.errorCodeField = value;
			}
		}

		// Token: 0x17000308 RID: 776
		// (get) Token: 0x06000A47 RID: 2631 RVA: 0x00010813 File Offset: 0x0000EA13
		// (set) Token: 0x06000A48 RID: 2632 RVA: 0x0001081B File Offset: 0x0000EA1B
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string message
		{
			get
			{
				return this.messageField;
			}
			set
			{
				this.messageField = value;
			}
		}

		// Token: 0x17000309 RID: 777
		// (get) Token: 0x06000A49 RID: 2633 RVA: 0x00010824 File Offset: 0x0000EA24
		// (set) Token: 0x06000A4A RID: 2634 RVA: 0x0001082C File Offset: 0x0000EA2C
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string viewMessage
		{
			get
			{
				return this.viewMessageField;
			}
			set
			{
				this.viewMessageField = value;
			}
		}

		// Token: 0x040003C8 RID: 968
		private string errorIdField;

		// Token: 0x040003C9 RID: 969
		private string errorCodeField;

		// Token: 0x040003CA RID: 970
		private string messageField;

		// Token: 0x040003CB RID: 971
		private string viewMessageField;
	}
}
