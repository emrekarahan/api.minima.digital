﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x0200011F RID: 287
	[XmlType(Namespace = "https://product.individual.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.7.3190.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[Serializable]
	public class productFailProcessType
	{
		// Token: 0x17000352 RID: 850
		// (get) Token: 0x06000AE6 RID: 2790 RVA: 0x00010D55 File Offset: 0x0000EF55
		// (set) Token: 0x06000AE7 RID: 2791 RVA: 0x00010D5D File Offset: 0x0000EF5D
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int productId
		{
			get
			{
				return this.productIdField;
			}
			set
			{
				this.productIdField = value;
			}
		}

		// Token: 0x17000353 RID: 851
		// (get) Token: 0x06000AE8 RID: 2792 RVA: 0x00010D66 File Offset: 0x0000EF66
		// (set) Token: 0x06000AE9 RID: 2793 RVA: 0x00010D6E File Offset: 0x0000EF6E
		[XmlIgnore]
		public bool productIdSpecified
		{
			get
			{
				return this.productIdFieldSpecified;
			}
			set
			{
				this.productIdFieldSpecified = value;
			}
		}

		// Token: 0x17000354 RID: 852
		// (get) Token: 0x06000AEA RID: 2794 RVA: 0x00010D77 File Offset: 0x0000EF77
		// (set) Token: 0x06000AEB RID: 2795 RVA: 0x00010D7F File Offset: 0x0000EF7F
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int failCode
		{
			get
			{
				return this.failCodeField;
			}
			set
			{
				this.failCodeField = value;
			}
		}

		// Token: 0x17000355 RID: 853
		// (get) Token: 0x06000AEC RID: 2796 RVA: 0x00010D88 File Offset: 0x0000EF88
		// (set) Token: 0x06000AED RID: 2797 RVA: 0x00010D90 File Offset: 0x0000EF90
		[XmlIgnore]
		public bool failCodeSpecified
		{
			get
			{
				return this.failCodeFieldSpecified;
			}
			set
			{
				this.failCodeFieldSpecified = value;
			}
		}

		// Token: 0x17000356 RID: 854
		// (get) Token: 0x06000AEE RID: 2798 RVA: 0x00010D99 File Offset: 0x0000EF99
		// (set) Token: 0x06000AEF RID: 2799 RVA: 0x00010DA1 File Offset: 0x0000EFA1
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string failReason
		{
			get
			{
				return this.failReasonField;
			}
			set
			{
				this.failReasonField = value;
			}
		}

		// Token: 0x04000418 RID: 1048
		private int productIdField;

		// Token: 0x04000419 RID: 1049
		private bool productIdFieldSpecified;

		// Token: 0x0400041A RID: 1050
		private int failCodeField;

		// Token: 0x0400041B RID: 1051
		private bool failCodeFieldSpecified;

		// Token: 0x0400041C RID: 1052
		private string failReasonField;
	}
}
