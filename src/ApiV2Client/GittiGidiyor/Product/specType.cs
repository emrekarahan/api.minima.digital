﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x02000125 RID: 293
	[GeneratedCode("System.Xml", "4.7.3190.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[XmlType(Namespace = "https://product.individual.ws.listingapi.gg.com")]
	[Serializable]
	public class specType
	{
		// Token: 0x17000370 RID: 880
		// (get) Token: 0x06000B28 RID: 2856 RVA: 0x00010F83 File Offset: 0x0000F183
		// (set) Token: 0x06000B29 RID: 2857 RVA: 0x00010F8B File Offset: 0x0000F18B
		[XmlAttribute]
		public string name
		{
			get
			{
				return this.nameField;
			}
			set
			{
				this.nameField = value;
			}
		}

		// Token: 0x17000371 RID: 881
		// (get) Token: 0x06000B2A RID: 2858 RVA: 0x00010F94 File Offset: 0x0000F194
		// (set) Token: 0x06000B2B RID: 2859 RVA: 0x00010F9C File Offset: 0x0000F19C
		[XmlAttribute]
		public string value
		{
			get
			{
				return this.valueField;
			}
			set
			{
				this.valueField = value;
			}
		}

		// Token: 0x17000372 RID: 882
		// (get) Token: 0x06000B2C RID: 2860 RVA: 0x00010FA5 File Offset: 0x0000F1A5
		// (set) Token: 0x06000B2D RID: 2861 RVA: 0x00010FAD File Offset: 0x0000F1AD
		[XmlAttribute]
		public string type
		{
			get
			{
				return this.typeField;
			}
			set
			{
				this.typeField = value;
			}
		}

		// Token: 0x17000373 RID: 883
		// (get) Token: 0x06000B2E RID: 2862 RVA: 0x00010FB6 File Offset: 0x0000F1B6
		// (set) Token: 0x06000B2F RID: 2863 RVA: 0x00010FBE File Offset: 0x0000F1BE
		[XmlAttribute]
		public bool required
		{
			get
			{
				return this.requiredField;
			}
			set
			{
				this.requiredField = value;
			}
		}

		// Token: 0x17000374 RID: 884
		// (get) Token: 0x06000B30 RID: 2864 RVA: 0x00010FC7 File Offset: 0x0000F1C7
		// (set) Token: 0x06000B31 RID: 2865 RVA: 0x00010FCF File Offset: 0x0000F1CF
		[XmlIgnore]
		public bool requiredSpecified
		{
			get
			{
				return this.requiredFieldSpecified;
			}
			set
			{
				this.requiredFieldSpecified = value;
			}
		}

		// Token: 0x04000436 RID: 1078
		private string nameField;

		// Token: 0x04000437 RID: 1079
		private string valueField;

		// Token: 0x04000438 RID: 1080
		private string typeField;

		// Token: 0x04000439 RID: 1081
		private bool requiredField;

		// Token: 0x0400043A RID: 1082
		private bool requiredFieldSpecified;
	}
}
