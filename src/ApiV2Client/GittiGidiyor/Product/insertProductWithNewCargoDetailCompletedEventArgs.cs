﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x02000167 RID: 359
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	public class insertProductWithNewCargoDetailCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000CAF RID: 3247 RVA: 0x00011B56 File Offset: 0x0000FD56
		internal insertProductWithNewCargoDetailCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x170003FA RID: 1018
		// (get) Token: 0x06000CB0 RID: 3248 RVA: 0x00011B69 File Offset: 0x0000FD69
		public productServiceResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (productServiceResponse)this.results[0];
			}
		}

		// Token: 0x040004C0 RID: 1216
		private object[] results;
	}
}
