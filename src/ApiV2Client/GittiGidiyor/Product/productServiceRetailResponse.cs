﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x02000133 RID: 307
	[DebuggerStepThrough]
	[XmlType(Namespace = "https://product.individual.ws.listingapi.gg.com")]
	[DesignerCategory("code")]
	[GeneratedCode("System.Xml", "4.7.3190.0")]
	[Serializable]
	public class productServiceRetailResponse : baseResponse
	{
		// Token: 0x170003CB RID: 971
		// (get) Token: 0x06000BEC RID: 3052 RVA: 0x000115FE File Offset: 0x0000F7FE
		// (set) Token: 0x06000BED RID: 3053 RVA: 0x00011606 File Offset: 0x0000F806
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int productId
		{
			get
			{
				return this.productIdField;
			}
			set
			{
				this.productIdField = value;
			}
		}

		// Token: 0x170003CC RID: 972
		// (get) Token: 0x06000BEE RID: 3054 RVA: 0x0001160F File Offset: 0x0000F80F
		// (set) Token: 0x06000BEF RID: 3055 RVA: 0x00011617 File Offset: 0x0000F817
		[XmlIgnore]
		public bool productIdSpecified
		{
			get
			{
				return this.productIdFieldSpecified;
			}
			set
			{
				this.productIdFieldSpecified = value;
			}
		}

		// Token: 0x170003CD RID: 973
		// (get) Token: 0x06000BF0 RID: 3056 RVA: 0x00011620 File Offset: 0x0000F820
		// (set) Token: 0x06000BF1 RID: 3057 RVA: 0x00011628 File Offset: 0x0000F828
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string itemId
		{
			get
			{
				return this.itemIdField;
			}
			set
			{
				this.itemIdField = value;
			}
		}

		// Token: 0x170003CE RID: 974
		// (get) Token: 0x06000BF2 RID: 3058 RVA: 0x00011631 File Offset: 0x0000F831
		// (set) Token: 0x06000BF3 RID: 3059 RVA: 0x00011639 File Offset: 0x0000F839
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public bool payRequired
		{
			get
			{
				return this.payRequiredField;
			}
			set
			{
				this.payRequiredField = value;
			}
		}

		// Token: 0x170003CF RID: 975
		// (get) Token: 0x06000BF4 RID: 3060 RVA: 0x00011642 File Offset: 0x0000F842
		// (set) Token: 0x06000BF5 RID: 3061 RVA: 0x0001164A File Offset: 0x0000F84A
		[XmlIgnore]
		public bool payRequiredSpecified
		{
			get
			{
				return this.payRequiredFieldSpecified;
			}
			set
			{
				this.payRequiredFieldSpecified = value;
			}
		}

		// Token: 0x170003D0 RID: 976
		// (get) Token: 0x06000BF6 RID: 3062 RVA: 0x00011653 File Offset: 0x0000F853
		// (set) Token: 0x06000BF7 RID: 3063 RVA: 0x0001165B File Offset: 0x0000F85B
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string result
		{
			get
			{
				return this.resultField;
			}
			set
			{
				this.resultField = value;
			}
		}

		// Token: 0x170003D1 RID: 977
		// (get) Token: 0x06000BF8 RID: 3064 RVA: 0x00011664 File Offset: 0x0000F864
		// (set) Token: 0x06000BF9 RID: 3065 RVA: 0x0001166C File Offset: 0x0000F86C
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int descriptionFilterStatus
		{
			get
			{
				return this.descriptionFilterStatusField;
			}
			set
			{
				this.descriptionFilterStatusField = value;
			}
		}

		// Token: 0x170003D2 RID: 978
		// (get) Token: 0x06000BFA RID: 3066 RVA: 0x00011675 File Offset: 0x0000F875
		// (set) Token: 0x06000BFB RID: 3067 RVA: 0x0001167D File Offset: 0x0000F87D
		[XmlIgnore]
		public bool descriptionFilterStatusSpecified
		{
			get
			{
				return this.descriptionFilterStatusFieldSpecified;
			}
			set
			{
				this.descriptionFilterStatusFieldSpecified = value;
			}
		}

		// Token: 0x170003D3 RID: 979
		// (get) Token: 0x06000BFC RID: 3068 RVA: 0x00011686 File Offset: 0x0000F886
		// (set) Token: 0x06000BFD RID: 3069 RVA: 0x0001168E File Offset: 0x0000F88E
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string listingStatus
		{
			get
			{
				return this.listingStatusField;
			}
			set
			{
				this.listingStatusField = value;
			}
		}

		// Token: 0x04000491 RID: 1169
		private int productIdField;

		// Token: 0x04000492 RID: 1170
		private bool productIdFieldSpecified;

		// Token: 0x04000493 RID: 1171
		private string itemIdField;

		// Token: 0x04000494 RID: 1172
		private bool payRequiredField;

		// Token: 0x04000495 RID: 1173
		private bool payRequiredFieldSpecified;

		// Token: 0x04000496 RID: 1174
		private string resultField;

		// Token: 0x04000497 RID: 1175
		private int descriptionFilterStatusField;

		// Token: 0x04000498 RID: 1176
		private bool descriptionFilterStatusFieldSpecified;

		// Token: 0x04000499 RID: 1177
		private string listingStatusField;
	}
}
