﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x0200017F RID: 383
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	public class updateProductWithVariantsCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000CF7 RID: 3319 RVA: 0x00011D36 File Offset: 0x0000FF36
		internal updateProductWithVariantsCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x17000406 RID: 1030
		// (get) Token: 0x06000CF8 RID: 3320 RVA: 0x00011D49 File Offset: 0x0000FF49
		public productServiceResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (productServiceResponse)this.results[0];
			}
		}

		// Token: 0x040004CC RID: 1228
		private object[] results;
	}
}
