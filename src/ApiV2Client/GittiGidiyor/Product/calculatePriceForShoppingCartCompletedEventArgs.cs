﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x0200013B RID: 315
	[DesignerCategory("code")]
	[DebuggerStepThrough]
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	public class calculatePriceForShoppingCartCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000C2B RID: 3115 RVA: 0x000117E6 File Offset: 0x0000F9E6
		internal calculatePriceForShoppingCartCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x170003E4 RID: 996
		// (get) Token: 0x06000C2C RID: 3116 RVA: 0x000117F9 File Offset: 0x0000F9F9
		public productServicePriceResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (productServicePriceResponse)this.results[0];
			}
		}

		// Token: 0x040004AA RID: 1194
		private object[] results;
	}
}
