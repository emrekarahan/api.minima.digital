﻿using System;
using System.Collections.Generic;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x020001EC RID: 492
	public class ProductService : ServiceClient<IndividualProductServiceService>, IProductService, IService
	{
		// Token: 0x060010B8 RID: 4280 RVA: 0x0001641C File Offset: 0x0001461C
		private ProductService()
		{
			base.configureNetworkCredentials(this.service);
		}

		// Token: 0x060010B9 RID: 4281 RVA: 0x0001643C File Offset: 0x0001463C
		public productServicePriceResponse calculatePriceForRevision(string productId, string itemId, string lang)
		{
			long time = Util.convertToTimestamp(DateTime.Now);
			return this.service.calculatePriceForRevision(this.authConfig.ApiKey, base.getSignature(time), time, productId, itemId, lang);
		}

		// Token: 0x060010BA RID: 4282 RVA: 0x00016478 File Offset: 0x00014678
		public productServicePriceResponse calculatePriceForShoppingCart(List<int> productIdArray, List<string> itemIdArray, string lang)
		{
			long time = Util.convertToTimestamp(DateTime.Now);
			return this.service.calculatePriceForShoppingCart(this.authConfig.ApiKey, base.getSignature(time), time, Util.toIntArray(productIdArray), itemIdArray.ToArray(), lang);
		}

		// Token: 0x060010BB RID: 4283 RVA: 0x000164BC File Offset: 0x000146BC
		public productServiceResponse cloneProduct(string productId, string itemId, string lang)
		{
			long time = Util.convertToTimestamp(DateTime.Now);
			return this.service.cloneProduct(this.authConfig.ApiKey, base.getSignature(time), time, productId, itemId, lang);
		}

		// Token: 0x060010BC RID: 4284 RVA: 0x000164F8 File Offset: 0x000146F8
		public productServiceIdResponse deleteProduct(List<int> productIdArray, List<string> itemIdArray, string lang)
		{
			long time = Util.convertToTimestamp(DateTime.Now);
			return this.service.deleteProduct(this.authConfig.ApiKey, base.getSignature(time), time, Util.toIntArray(productIdArray), itemIdArray.ToArray(), lang);
		}

		// Token: 0x060010BD RID: 4285 RVA: 0x0001653C File Offset: 0x0001473C
		public productServiceIdResponse finishEarly(List<int> productIdArray, List<string> itemIdArray, string lang)
		{
			long time = Util.convertToTimestamp(DateTime.Now);
			return this.service.finishEarly(this.authConfig.ApiKey, base.getSignature(time), time, Util.toIntArray(productIdArray), itemIdArray.ToArray(), lang);
		}

		// Token: 0x060010BE RID: 4286 RVA: 0x00016580 File Offset: 0x00014780
		public productServiceIdResponse getNewlyListedProductIdList(int startOffset, int rowCount, bool viaApi, string lang)
		{
			long time = Util.convertToTimestamp(DateTime.Now);
			return this.service.getNewlyListedProductIdList(this.authConfig.ApiKey, base.getSignature(time), time, startOffset, rowCount, viaApi, lang);
		}

		// Token: 0x060010BF RID: 4287 RVA: 0x000165BC File Offset: 0x000147BC
		public productServiceDetailResponse getProduct(string productId, string itemId, string lang)
		{
			long time = Util.convertToTimestamp(DateTime.Now);
			return this.service.getProduct(this.authConfig.ApiKey, base.getSignature(time), time, productId, itemId, lang);
		}

		// Token: 0x060010C0 RID: 4288 RVA: 0x000165F8 File Offset: 0x000147F8
		public productServiceDescResponse getProductDescription(string productId, string itemId, string lang)
		{
			long time = Util.convertToTimestamp(DateTime.Now);
			return this.service.getProductDescription(this.authConfig.ApiKey, base.getSignature(time), time, productId, itemId, lang);
		}

		// Token: 0x060010C1 RID: 4289 RVA: 0x00016634 File Offset: 0x00014834
		public productServiceListResponse getProducts(int startOffset, int rowCount, string status, bool withData, string lang)
		{
			long time = Util.convertToTimestamp(DateTime.Now);
			return this.service.getProducts(this.authConfig.ApiKey, base.getSignature(time), time, startOffset, rowCount, status, withData, lang);
		}

		// Token: 0x060010C2 RID: 4290 RVA: 0x00016674 File Offset: 0x00014874
		public productServiceSpecResponse getProductSpecs(string productId, string itemId, string lang)
		{
			long time = Util.convertToTimestamp(DateTime.Now);
			return this.service.getProductSpecs(this.authConfig.ApiKey, base.getSignature(time), time, productId, itemId, lang);
		}

		// Token: 0x060010C3 RID: 4291 RVA: 0x000166AD File Offset: 0x000148AD
		public string getServiceName()
		{
			return this.service.getServiceName();
		}

		// Token: 0x060010C4 RID: 4292 RVA: 0x000166BC File Offset: 0x000148BC
		public productServiceStockResponse getStockAndPrice(List<int> productIdArray, List<string> itemIdArray, string lang)
		{
			long time = Util.convertToTimestamp(DateTime.Now);
			return this.service.getStockAndPrice(this.authConfig.ApiKey, base.getSignature(time), time, Util.toIntArray(productIdArray), itemIdArray.ToArray(), lang);
		}

		// Token: 0x060010C5 RID: 4293 RVA: 0x00016700 File Offset: 0x00014900
		public productServiceResponse insertProduct(string itemId, productType productType, bool forceToSpecEntry, bool nextDateOption, string lang)
		{
			long time = Util.convertToTimestamp(DateTime.Now);
			return this.service.insertProduct(this.authConfig.ApiKey, base.getSignature(time), time, itemId, productType, forceToSpecEntry, nextDateOption, lang);
		}

		// Token: 0x060010C6 RID: 4294 RVA: 0x00016740 File Offset: 0x00014940
		public productServiceResponse insertProductWithNewCargoDetail(string itemId, productType productType, bool forceToSpecEntry, bool nextDateOption, string lang)
		{
			long time = Util.convertToTimestamp(DateTime.Now);
			return this.service.insertProductWithNewCargoDetail(this.authConfig.ApiKey, base.getSignature(time), time, itemId, productType, forceToSpecEntry, nextDateOption, lang);
		}

		// Token: 0x060010C7 RID: 4295 RVA: 0x00016780 File Offset: 0x00014980
		public productServicePaymentResponse payPrice(string paymentVoucher, string ccOwnerName, string ccOwnerSurname, string ccNumber, string cvv, string expireMonth, string expireYear, string lang)
		{
			long time = Util.convertToTimestamp(DateTime.Now);
			return this.service.payPrice(this.authConfig.ApiKey, base.getSignature(time), time, paymentVoucher, ccOwnerName, ccOwnerSurname, ccNumber, cvv, expireMonth, expireYear, lang);
		}

		// Token: 0x060010C8 RID: 4296 RVA: 0x000167C4 File Offset: 0x000149C4
		public productServiceIdResponse relistProducts(List<int> productIdArray, List<string> itemIdArray, string lang)
		{
			long time = Util.convertToTimestamp(DateTime.Now);
			return this.service.relistProducts(this.authConfig.ApiKey, base.getSignature(time), time, Util.toIntArray(productIdArray), itemIdArray.ToArray(), lang);
		}

		// Token: 0x060010C9 RID: 4297 RVA: 0x00016808 File Offset: 0x00014A08
		public productServiceResponse updatePrice(string productId, string itemId, double newPrice, bool cancelBid, string lang)
		{
			long time = Util.convertToTimestamp(DateTime.Now);
			return this.service.updatePrice(this.authConfig.ApiKey, base.getSignature(time), time, productId, itemId, newPrice, cancelBid, lang);
		}

		// Token: 0x060010CA RID: 4298 RVA: 0x00016848 File Offset: 0x00014A48
		public productServiceResponse updateProduct(string productId, string itemId, productType productType, bool onSale, bool forceToSpecEntry, bool nextDateOption, string lang)
		{
			long time = Util.convertToTimestamp(DateTime.Now);
			return this.service.updateProduct(this.authConfig.ApiKey, base.getSignature(time), time, itemId, productId, productType, onSale, forceToSpecEntry, nextDateOption, lang);
		}

		// Token: 0x060010CB RID: 4299 RVA: 0x0001688C File Offset: 0x00014A8C
		public productServiceResponse updateStock(string productId, string itemId, int stock, bool cancelBid, string lang)
		{
			long time = Util.convertToTimestamp(DateTime.Now);
			return this.service.updateStock(this.authConfig.ApiKey, base.getSignature(time), time, productId, itemId, stock, cancelBid, lang);
		}

		// Token: 0x060010CC RID: 4300 RVA: 0x000168CC File Offset: 0x00014ACC
		public productServiceResponse updateProductWithNewCargoDetail(string itemId, string productId, productType productType, bool onSale, bool forceToSpecEntry, bool nextDateOption, string lang)
		{
			long time = Util.convertToTimestamp(DateTime.Now);
			return this.service.updateProductWithNewCargoDetail(this.authConfig.ApiKey, base.getSignature(time), time, itemId, productId, productType, onSale, forceToSpecEntry, nextDateOption, lang);
		}

		// Token: 0x060010CD RID: 4301 RVA: 0x00016910 File Offset: 0x00014B10
		public productServiceRetailResponse updateProductVariants(string productId, string itemId, productVariantType productVariant, bool onSale, bool forceToSpecEntry, bool nextDateOption, string lang)
		{
			long time = Util.convertToTimestamp(DateTime.Now);
			return this.service.updateProductVariants(this.authConfig.ApiKey, base.getSignature(time), time, productId, itemId, productVariant, lang);
		}

		// Token: 0x1700051B RID: 1307
		// (get) Token: 0x060010CE RID: 4302 RVA: 0x0001694C File Offset: 0x00014B4C
		public static ProductService Instance
		{
			get
			{
				if (ProductService.instance == null)
				{
					lock (ProductService.lockObject)
					{
						if (ProductService.instance == null)
						{
							ProductService.instance = new ProductService();
						}
					}
				}
				return ProductService.instance;
			}
		}

		// Token: 0x04000622 RID: 1570
		private static ProductService instance;

		// Token: 0x04000623 RID: 1571
		private static object lockObject = new object();

		// Token: 0x04000624 RID: 1572
		private IndividualProductServiceService service = new IndividualProductServiceService();
	}
}
