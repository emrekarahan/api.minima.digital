﻿using System.CodeDom.Compiler;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x02000172 RID: 370
	// (Invoke) Token: 0x06000CD0 RID: 3280
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	public delegate void updateMarketPriceCompletedEventHandler(object sender, updateMarketPriceCompletedEventArgs e);
}
