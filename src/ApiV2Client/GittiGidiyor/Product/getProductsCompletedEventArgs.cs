﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x0200015D RID: 349
	[DesignerCategory("code")]
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	[DebuggerStepThrough]
	public class getProductsCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000C91 RID: 3217 RVA: 0x00011A8E File Offset: 0x0000FC8E
		internal getProductsCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x170003F5 RID: 1013
		// (get) Token: 0x06000C92 RID: 3218 RVA: 0x00011AA1 File Offset: 0x0000FCA1
		public productServiceListResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (productServiceListResponse)this.results[0];
			}
		}

		// Token: 0x040004BB RID: 1211
		private object[] results;
	}
}
