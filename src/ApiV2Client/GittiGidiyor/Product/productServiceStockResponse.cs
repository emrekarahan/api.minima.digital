﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x0200012E RID: 302
	[DebuggerStepThrough]
	[XmlType(Namespace = "https://product.individual.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.7.3190.0")]
	[DesignerCategory("code")]
	[Serializable]
	public class productServiceStockResponse : baseResponse
	{
		// Token: 0x170003B7 RID: 951
		// (get) Token: 0x06000BBF RID: 3007 RVA: 0x00011482 File Offset: 0x0000F682
		// (set) Token: 0x06000BC0 RID: 3008 RVA: 0x0001148A File Offset: 0x0000F68A
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		[XmlArrayItem("product", Form = XmlSchemaForm.Unqualified)]
		public stockDetailType[] products
		{
			get
			{
				return this.productsField;
			}
			set
			{
				this.productsField = value;
			}
		}

		// Token: 0x0400047D RID: 1149
		private stockDetailType[] productsField;
	}
}
