﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x0200012D RID: 301
	[XmlType(Namespace = "https://product.individual.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.7.3190.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[Serializable]
	public class productServiceBooleanResponse : baseResponse
	{
		// Token: 0x170003B5 RID: 949
		// (get) Token: 0x06000BBA RID: 3002 RVA: 0x00011458 File Offset: 0x0000F658
		// (set) Token: 0x06000BBB RID: 3003 RVA: 0x00011460 File Offset: 0x0000F660
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public bool exists
		{
			get
			{
				return this.existsField;
			}
			set
			{
				this.existsField = value;
			}
		}

		// Token: 0x170003B6 RID: 950
		// (get) Token: 0x06000BBC RID: 3004 RVA: 0x00011469 File Offset: 0x0000F669
		// (set) Token: 0x06000BBD RID: 3005 RVA: 0x00011471 File Offset: 0x0000F671
		[XmlIgnore]
		public bool existsSpecified
		{
			get
			{
				return this.existsFieldSpecified;
			}
			set
			{
				this.existsFieldSpecified = value;
			}
		}

		// Token: 0x0400047B RID: 1147
		private bool existsField;

		// Token: 0x0400047C RID: 1148
		private bool existsFieldSpecified;
	}
}
