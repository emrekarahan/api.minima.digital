﻿using System.CodeDom.Compiler;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x02000146 RID: 326
	// (Invoke) Token: 0x06000C4C RID: 3148
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	public delegate void finishEarlyProductsCompletedEventHandler(object sender, finishEarlyProductsCompletedEventArgs e);
}
