﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x02000134 RID: 308
	[GeneratedCode("System.Xml", "4.7.3190.0")]
	[DesignerCategory("code")]
	[DebuggerStepThrough]
	[XmlType(Namespace = "https://product.individual.ws.listingapi.gg.com")]
	[Serializable]
	public class productServiceIdResponse : baseResponse
	{
		// Token: 0x170003D4 RID: 980
		// (get) Token: 0x06000BFF RID: 3071 RVA: 0x0001169F File Offset: 0x0000F89F
		// (set) Token: 0x06000C00 RID: 3072 RVA: 0x000116A7 File Offset: 0x0000F8A7
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public long productCount
		{
			get
			{
				return this.productCountField;
			}
			set
			{
				this.productCountField = value;
			}
		}

		// Token: 0x170003D5 RID: 981
		// (get) Token: 0x06000C01 RID: 3073 RVA: 0x000116B0 File Offset: 0x0000F8B0
		// (set) Token: 0x06000C02 RID: 3074 RVA: 0x000116B8 File Offset: 0x0000F8B8
		[XmlIgnore]
		public bool productCountSpecified
		{
			get
			{
				return this.productCountFieldSpecified;
			}
			set
			{
				this.productCountFieldSpecified = value;
			}
		}

		// Token: 0x170003D6 RID: 982
		// (get) Token: 0x06000C03 RID: 3075 RVA: 0x000116C1 File Offset: 0x0000F8C1
		// (set) Token: 0x06000C04 RID: 3076 RVA: 0x000116C9 File Offset: 0x0000F8C9
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		[XmlArrayItem("productId", Form = XmlSchemaForm.Unqualified)]
		public int?[] productIdList
		{
			get
			{
				return this.productIdListField;
			}
			set
			{
				this.productIdListField = value;
			}
		}

		// Token: 0x170003D7 RID: 983
		// (get) Token: 0x06000C05 RID: 3077 RVA: 0x000116D2 File Offset: 0x0000F8D2
		// (set) Token: 0x06000C06 RID: 3078 RVA: 0x000116DA File Offset: 0x0000F8DA
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string voucher
		{
			get
			{
				return this.voucherField;
			}
			set
			{
				this.voucherField = value;
			}
		}

		// Token: 0x170003D8 RID: 984
		// (get) Token: 0x06000C07 RID: 3079 RVA: 0x000116E3 File Offset: 0x0000F8E3
		// (set) Token: 0x06000C08 RID: 3080 RVA: 0x000116EB File Offset: 0x0000F8EB
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public double price
		{
			get
			{
				return this.priceField;
			}
			set
			{
				this.priceField = value;
			}
		}

		// Token: 0x170003D9 RID: 985
		// (get) Token: 0x06000C09 RID: 3081 RVA: 0x000116F4 File Offset: 0x0000F8F4
		// (set) Token: 0x06000C0A RID: 3082 RVA: 0x000116FC File Offset: 0x0000F8FC
		[XmlIgnore]
		public bool priceSpecified
		{
			get
			{
				return this.priceFieldSpecified;
			}
			set
			{
				this.priceFieldSpecified = value;
			}
		}

		// Token: 0x170003DA RID: 986
		// (get) Token: 0x06000C0B RID: 3083 RVA: 0x00011705 File Offset: 0x0000F905
		// (set) Token: 0x06000C0C RID: 3084 RVA: 0x0001170D File Offset: 0x0000F90D
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string result
		{
			get
			{
				return this.resultField;
			}
			set
			{
				this.resultField = value;
			}
		}

		// Token: 0x0400049A RID: 1178
		private long productCountField;

		// Token: 0x0400049B RID: 1179
		private bool productCountFieldSpecified;

		// Token: 0x0400049C RID: 1180
		private int?[] productIdListField;

		// Token: 0x0400049D RID: 1181
		private string voucherField;

		// Token: 0x0400049E RID: 1182
		private double priceField;

		// Token: 0x0400049F RID: 1183
		private bool priceFieldSpecified;

		// Token: 0x040004A0 RID: 1184
		private string resultField;
	}
}
