﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x02000116 RID: 278
	[GeneratedCode("System.Xml", "4.7.3190.0")]
	[XmlType(Namespace = "https://product.individual.ws.listingapi.gg.com")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[Serializable]
	public class variantType
	{
		// Token: 0x17000312 RID: 786
		// (get) Token: 0x06000A5E RID: 2654 RVA: 0x000108D5 File Offset: 0x0000EAD5
		// (set) Token: 0x06000A5F RID: 2655 RVA: 0x000108DD File Offset: 0x0000EADD
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		[XmlArrayItem("variantSpec", Form = XmlSchemaForm.Unqualified, IsNullable = false)]
		public variantSpecType[] variantSpecs
		{
			get
			{
				return this.variantSpecsField;
			}
			set
			{
				this.variantSpecsField = value;
			}
		}

		// Token: 0x17000313 RID: 787
		// (get) Token: 0x06000A60 RID: 2656 RVA: 0x000108E6 File Offset: 0x0000EAE6
		// (set) Token: 0x06000A61 RID: 2657 RVA: 0x000108EE File Offset: 0x0000EAEE
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int quantity
		{
			get
			{
				return this.quantityField;
			}
			set
			{
				this.quantityField = value;
			}
		}

		// Token: 0x17000314 RID: 788
		// (get) Token: 0x06000A62 RID: 2658 RVA: 0x000108F7 File Offset: 0x0000EAF7
		// (set) Token: 0x06000A63 RID: 2659 RVA: 0x000108FF File Offset: 0x0000EAFF
		[XmlIgnore]
		public bool quantitySpecified
		{
			get
			{
				return this.quantityFieldSpecified;
			}
			set
			{
				this.quantityFieldSpecified = value;
			}
		}

		// Token: 0x17000315 RID: 789
		// (get) Token: 0x06000A64 RID: 2660 RVA: 0x00010908 File Offset: 0x0000EB08
		// (set) Token: 0x06000A65 RID: 2661 RVA: 0x00010910 File Offset: 0x0000EB10
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string stockCode
		{
			get
			{
				return this.stockCodeField;
			}
			set
			{
				this.stockCodeField = value;
			}
		}

		// Token: 0x17000316 RID: 790
		// (get) Token: 0x06000A66 RID: 2662 RVA: 0x00010919 File Offset: 0x0000EB19
		// (set) Token: 0x06000A67 RID: 2663 RVA: 0x00010921 File Offset: 0x0000EB21
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int soldCount
		{
			get
			{
				return this.soldCountField;
			}
			set
			{
				this.soldCountField = value;
			}
		}

		// Token: 0x17000317 RID: 791
		// (get) Token: 0x06000A68 RID: 2664 RVA: 0x0001092A File Offset: 0x0000EB2A
		// (set) Token: 0x06000A69 RID: 2665 RVA: 0x00010932 File Offset: 0x0000EB32
		[XmlIgnore]
		public bool soldCountSpecified
		{
			get
			{
				return this.soldCountFieldSpecified;
			}
			set
			{
				this.soldCountFieldSpecified = value;
			}
		}

		// Token: 0x17000318 RID: 792
		// (get) Token: 0x06000A6A RID: 2666 RVA: 0x0001093B File Offset: 0x0000EB3B
		// (set) Token: 0x06000A6B RID: 2667 RVA: 0x00010943 File Offset: 0x0000EB43
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int newCatalogId
		{
			get
			{
				return this.newCatalogIdField;
			}
			set
			{
				this.newCatalogIdField = value;
			}
		}

		// Token: 0x17000319 RID: 793
		// (get) Token: 0x06000A6C RID: 2668 RVA: 0x0001094C File Offset: 0x0000EB4C
		// (set) Token: 0x06000A6D RID: 2669 RVA: 0x00010954 File Offset: 0x0000EB54
		[XmlIgnore]
		public bool newCatalogIdSpecified
		{
			get
			{
				return this.newCatalogIdFieldSpecified;
			}
			set
			{
				this.newCatalogIdFieldSpecified = value;
			}
		}

		// Token: 0x1700031A RID: 794
		// (get) Token: 0x06000A6E RID: 2670 RVA: 0x0001095D File Offset: 0x0000EB5D
		// (set) Token: 0x06000A6F RID: 2671 RVA: 0x00010965 File Offset: 0x0000EB65
		[XmlAttribute]
		public long variantId
		{
			get
			{
				return this.variantIdField;
			}
			set
			{
				this.variantIdField = value;
			}
		}

		// Token: 0x1700031B RID: 795
		// (get) Token: 0x06000A70 RID: 2672 RVA: 0x0001096E File Offset: 0x0000EB6E
		// (set) Token: 0x06000A71 RID: 2673 RVA: 0x00010976 File Offset: 0x0000EB76
		[XmlIgnore]
		public bool variantIdSpecified
		{
			get
			{
				return this.variantIdFieldSpecified;
			}
			set
			{
				this.variantIdFieldSpecified = value;
			}
		}

		// Token: 0x1700031C RID: 796
		// (get) Token: 0x06000A72 RID: 2674 RVA: 0x0001097F File Offset: 0x0000EB7F
		// (set) Token: 0x06000A73 RID: 2675 RVA: 0x00010987 File Offset: 0x0000EB87
		[XmlAttribute]
		public string operation
		{
			get
			{
				return this.operationField;
			}
			set
			{
				this.operationField = value;
			}
		}

		// Token: 0x040003D4 RID: 980
		private variantSpecType[] variantSpecsField;

		// Token: 0x040003D5 RID: 981
		private int quantityField;

		// Token: 0x040003D6 RID: 982
		private bool quantityFieldSpecified;

		// Token: 0x040003D7 RID: 983
		private string stockCodeField;

		// Token: 0x040003D8 RID: 984
		private int soldCountField;

		// Token: 0x040003D9 RID: 985
		private bool soldCountFieldSpecified;

		// Token: 0x040003DA RID: 986
		private int newCatalogIdField;

		// Token: 0x040003DB RID: 987
		private bool newCatalogIdFieldSpecified;

		// Token: 0x040003DC RID: 988
		private long variantIdField;

		// Token: 0x040003DD RID: 989
		private bool variantIdFieldSpecified;

		// Token: 0x040003DE RID: 990
		private string operationField;
	}
}
