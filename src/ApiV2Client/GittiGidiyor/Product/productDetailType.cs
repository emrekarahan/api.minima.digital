﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x02000127 RID: 295
	[DebuggerStepThrough]
	[XmlType(Namespace = "https://product.individual.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.7.3190.0")]
	[DesignerCategory("code")]
	[Serializable]
	public class productDetailType
	{
		// Token: 0x170003A2 RID: 930
		// (get) Token: 0x06000B8E RID: 2958 RVA: 0x000112E5 File Offset: 0x0000F4E5
		// (set) Token: 0x06000B8F RID: 2959 RVA: 0x000112ED File Offset: 0x0000F4ED
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string itemId
		{
			get
			{
				return this.itemIdField;
			}
			set
			{
				this.itemIdField = value;
			}
		}

		// Token: 0x170003A3 RID: 931
		// (get) Token: 0x06000B90 RID: 2960 RVA: 0x000112F6 File Offset: 0x0000F4F6
		// (set) Token: 0x06000B91 RID: 2961 RVA: 0x000112FE File Offset: 0x0000F4FE
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int productId
		{
			get
			{
				return this.productIdField;
			}
			set
			{
				this.productIdField = value;
			}
		}

		// Token: 0x170003A4 RID: 932
		// (get) Token: 0x06000B92 RID: 2962 RVA: 0x00011307 File Offset: 0x0000F507
		// (set) Token: 0x06000B93 RID: 2963 RVA: 0x0001130F File Offset: 0x0000F50F
		[XmlIgnore]
		public bool productIdSpecified
		{
			get
			{
				return this.productIdFieldSpecified;
			}
			set
			{
				this.productIdFieldSpecified = value;
			}
		}

		// Token: 0x170003A5 RID: 933
		// (get) Token: 0x06000B94 RID: 2964 RVA: 0x00011318 File Offset: 0x0000F518
		// (set) Token: 0x06000B95 RID: 2965 RVA: 0x00011320 File Offset: 0x0000F520
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public productType product
		{
			get
			{
				return this.productField;
			}
			set
			{
				this.productField = value;
			}
		}

		// Token: 0x170003A6 RID: 934
		// (get) Token: 0x06000B96 RID: 2966 RVA: 0x00011329 File Offset: 0x0000F529
		// (set) Token: 0x06000B97 RID: 2967 RVA: 0x00011331 File Offset: 0x0000F531
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public productConcerningType summary
		{
			get
			{
				return this.summaryField;
			}
			set
			{
				this.summaryField = value;
			}
		}

		// Token: 0x170003A7 RID: 935
		// (get) Token: 0x06000B98 RID: 2968 RVA: 0x0001133A File Offset: 0x0000F53A
		// (set) Token: 0x06000B99 RID: 2969 RVA: 0x00011342 File Offset: 0x0000F542
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string ees
		{
			get
			{
				return this.eesField;
			}
			set
			{
				this.eesField = value;
			}
		}

		// Token: 0x04000468 RID: 1128
		private string itemIdField;

		// Token: 0x04000469 RID: 1129
		private int productIdField;

		// Token: 0x0400046A RID: 1130
		private bool productIdFieldSpecified;

		// Token: 0x0400046B RID: 1131
		private productType productField;

		// Token: 0x0400046C RID: 1132
		private productConcerningType summaryField;

		// Token: 0x0400046D RID: 1133
		private string eesField;
	}
}
