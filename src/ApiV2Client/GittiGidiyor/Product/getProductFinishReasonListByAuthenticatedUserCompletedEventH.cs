﻿using System.CodeDom.Compiler;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x02000150 RID: 336
	// (Invoke) Token: 0x06000C6A RID: 3178
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	public delegate void getProductFinishReasonListByAuthenticatedUserCompletedEventHandler(object sender, getProductFinishReasonListByAuthenticatedUserCompletedEventArgs e);
}
