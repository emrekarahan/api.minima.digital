﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x02000118 RID: 280
	[GeneratedCode("System.Xml", "4.7.3190.0")]
	[XmlType(Namespace = "https://product.individual.ws.listingapi.gg.com")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[Serializable]
	public class photoType
	{
		// Token: 0x17000327 RID: 807
		// (get) Token: 0x06000A8A RID: 2698 RVA: 0x00010A4A File Offset: 0x0000EC4A
		// (set) Token: 0x06000A8B RID: 2699 RVA: 0x00010A52 File Offset: 0x0000EC52
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string url
		{
			get
			{
				return this.urlField;
			}
			set
			{
				this.urlField = value;
			}
		}

		// Token: 0x17000328 RID: 808
		// (get) Token: 0x06000A8C RID: 2700 RVA: 0x00010A5B File Offset: 0x0000EC5B
		// (set) Token: 0x06000A8D RID: 2701 RVA: 0x00010A63 File Offset: 0x0000EC63
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string base64
		{
			get
			{
				return this.base64Field;
			}
			set
			{
				this.base64Field = value;
			}
		}

		// Token: 0x17000329 RID: 809
		// (get) Token: 0x06000A8E RID: 2702 RVA: 0x00010A6C File Offset: 0x0000EC6C
		// (set) Token: 0x06000A8F RID: 2703 RVA: 0x00010A74 File Offset: 0x0000EC74
		[XmlAttribute]
		public int photoId
		{
			get
			{
				return this.photoIdField;
			}
			set
			{
				this.photoIdField = value;
			}
		}

		// Token: 0x1700032A RID: 810
		// (get) Token: 0x06000A90 RID: 2704 RVA: 0x00010A7D File Offset: 0x0000EC7D
		// (set) Token: 0x06000A91 RID: 2705 RVA: 0x00010A85 File Offset: 0x0000EC85
		[XmlIgnore]
		public bool photoIdSpecified
		{
			get
			{
				return this.photoIdFieldSpecified;
			}
			set
			{
				this.photoIdFieldSpecified = value;
			}
		}

		// Token: 0x040003E9 RID: 1001
		private string urlField;

		// Token: 0x040003EA RID: 1002
		private string base64Field;

		// Token: 0x040003EB RID: 1003
		private int photoIdField;

		// Token: 0x040003EC RID: 1004
		private bool photoIdFieldSpecified;
	}
}
