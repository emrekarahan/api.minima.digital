﻿using System;
using System.CodeDom.Compiler;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x0200011D RID: 285
	[XmlType(Namespace = "https://product.individual.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.7.3190.0")]
	[Serializable]
	public enum itemIdDetailStatus
	{
		// Token: 0x04000409 RID: 1033
		PASSIVE,
		// Token: 0x0400040A RID: 1034
		ACTIVE,
		// Token: 0x0400040B RID: 1035
		DUPLICATE_RECORD
	}
}
