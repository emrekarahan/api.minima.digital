﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x0200014D RID: 333
	[DesignerCategory("code")]
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	[DebuggerStepThrough]
	public class getProductCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000C61 RID: 3169 RVA: 0x0001194E File Offset: 0x0000FB4E
		internal getProductCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x170003ED RID: 1005
		// (get) Token: 0x06000C62 RID: 3170 RVA: 0x00011961 File Offset: 0x0000FB61
		public productServiceDetailResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (productServiceDetailResponse)this.results[0];
			}
		}

		// Token: 0x040004B3 RID: 1203
		private object[] results;
	}
}
