﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x0200011B RID: 283
	[GeneratedCode("System.Xml", "4.7.3190.0")]
	[XmlType(Namespace = "https://product.individual.ws.listingapi.gg.com")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[Serializable]
	public class productStatusType
	{
		// Token: 0x1700033D RID: 829
		// (get) Token: 0x06000AB9 RID: 2745 RVA: 0x00010BD8 File Offset: 0x0000EDD8
		// (set) Token: 0x06000ABA RID: 2746 RVA: 0x00010BE0 File Offset: 0x0000EDE0
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int productId
		{
			get
			{
				return this.productIdField;
			}
			set
			{
				this.productIdField = value;
			}
		}

		// Token: 0x1700033E RID: 830
		// (get) Token: 0x06000ABB RID: 2747 RVA: 0x00010BE9 File Offset: 0x0000EDE9
		// (set) Token: 0x06000ABC RID: 2748 RVA: 0x00010BF1 File Offset: 0x0000EDF1
		[XmlIgnore]
		public bool productIdSpecified
		{
			get
			{
				return this.productIdFieldSpecified;
			}
			set
			{
				this.productIdFieldSpecified = value;
			}
		}

		// Token: 0x1700033F RID: 831
		// (get) Token: 0x06000ABD RID: 2749 RVA: 0x00010BFA File Offset: 0x0000EDFA
		// (set) Token: 0x06000ABE RID: 2750 RVA: 0x00010C02 File Offset: 0x0000EE02
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string itemId
		{
			get
			{
				return this.itemIdField;
			}
			set
			{
				this.itemIdField = value;
			}
		}

		// Token: 0x17000340 RID: 832
		// (get) Token: 0x06000ABF RID: 2751 RVA: 0x00010C0B File Offset: 0x0000EE0B
		// (set) Token: 0x06000AC0 RID: 2752 RVA: 0x00010C13 File Offset: 0x0000EE13
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string currentStatus
		{
			get
			{
				return this.currentStatusField;
			}
			set
			{
				this.currentStatusField = value;
			}
		}

		// Token: 0x040003FF RID: 1023
		private int productIdField;

		// Token: 0x04000400 RID: 1024
		private bool productIdFieldSpecified;

		// Token: 0x04000401 RID: 1025
		private string itemIdField;

		// Token: 0x04000402 RID: 1026
		private string currentStatusField;
	}
}
