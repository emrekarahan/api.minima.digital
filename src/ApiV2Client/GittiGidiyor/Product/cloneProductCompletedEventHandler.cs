﻿using System.CodeDom.Compiler;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x0200013E RID: 318
	// (Invoke) Token: 0x06000C34 RID: 3124
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	public delegate void cloneProductCompletedEventHandler(object sender, cloneProductCompletedEventArgs e);
}
