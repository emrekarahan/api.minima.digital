﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x02000157 RID: 343
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	public class getProductSpecsCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000C7F RID: 3199 RVA: 0x00011A16 File Offset: 0x0000FC16
		internal getProductSpecsCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x170003F2 RID: 1010
		// (get) Token: 0x06000C80 RID: 3200 RVA: 0x00011A29 File Offset: 0x0000FC29
		public productServiceSpecResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (productServiceSpecResponse)this.results[0];
			}
		}

		// Token: 0x040004B8 RID: 1208
		private object[] results;
	}
}
