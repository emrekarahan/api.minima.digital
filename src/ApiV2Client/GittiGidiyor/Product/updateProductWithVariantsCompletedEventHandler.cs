﻿using System.CodeDom.Compiler;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x0200017E RID: 382
	// (Invoke) Token: 0x06000CF4 RID: 3316
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	public delegate void updateProductWithVariantsCompletedEventHandler(object sender, updateProductWithVariantsCompletedEventArgs e);
}
