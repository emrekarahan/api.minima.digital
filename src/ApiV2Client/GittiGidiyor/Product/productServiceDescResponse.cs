﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x02000136 RID: 310
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[XmlType(Namespace = "https://product.individual.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.7.3190.0")]
	[Serializable]
	public class productServiceDescResponse : baseResponse
	{
		// Token: 0x170003DC RID: 988
		// (get) Token: 0x06000C11 RID: 3089 RVA: 0x00011737 File Offset: 0x0000F937
		// (set) Token: 0x06000C12 RID: 3090 RVA: 0x0001173F File Offset: 0x0000F93F
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int productId
		{
			get
			{
				return this.productIdField;
			}
			set
			{
				this.productIdField = value;
			}
		}

		// Token: 0x170003DD RID: 989
		// (get) Token: 0x06000C13 RID: 3091 RVA: 0x00011748 File Offset: 0x0000F948
		// (set) Token: 0x06000C14 RID: 3092 RVA: 0x00011750 File Offset: 0x0000F950
		[XmlIgnore]
		public bool productIdSpecified
		{
			get
			{
				return this.productIdFieldSpecified;
			}
			set
			{
				this.productIdFieldSpecified = value;
			}
		}

		// Token: 0x170003DE RID: 990
		// (get) Token: 0x06000C15 RID: 3093 RVA: 0x00011759 File Offset: 0x0000F959
		// (set) Token: 0x06000C16 RID: 3094 RVA: 0x00011761 File Offset: 0x0000F961
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string itemId
		{
			get
			{
				return this.itemIdField;
			}
			set
			{
				this.itemIdField = value;
			}
		}

		// Token: 0x170003DF RID: 991
		// (get) Token: 0x06000C17 RID: 3095 RVA: 0x0001176A File Offset: 0x0000F96A
		// (set) Token: 0x06000C18 RID: 3096 RVA: 0x00011772 File Offset: 0x0000F972
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string description
		{
			get
			{
				return this.descriptionField;
			}
			set
			{
				this.descriptionField = value;
			}
		}

		// Token: 0x040004A2 RID: 1186
		private int productIdField;

		// Token: 0x040004A3 RID: 1187
		private bool productIdFieldSpecified;

		// Token: 0x040004A4 RID: 1188
		private string itemIdField;

		// Token: 0x040004A5 RID: 1189
		private string descriptionField;
	}
}
