﻿using System.CodeDom.Compiler;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x0200015A RID: 346
	// (Invoke) Token: 0x06000C88 RID: 3208
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	public delegate void getProductVariantsCompletedEventHandler(object sender, getProductVariantsCompletedEventArgs e);
}
