﻿using System.Collections.Generic;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x020001E3 RID: 483
	public interface IProductService : IService
	{
		// Token: 0x0600106B RID: 4203
		productServicePriceResponse calculatePriceForRevision(string productId, string itemId, string lang);

		// Token: 0x0600106C RID: 4204
		productServicePriceResponse calculatePriceForShoppingCart(List<int> productIdArray, List<string> itemIdArray, string lang);

		// Token: 0x0600106D RID: 4205
		productServiceResponse cloneProduct(string productId, string itemId, string lang);

		// Token: 0x0600106E RID: 4206
		productServiceIdResponse deleteProduct(List<int> productIdArray, List<string> itemIdArray, string lang);

		// Token: 0x0600106F RID: 4207
		productServiceIdResponse finishEarly(List<int> productIdArray, List<string> itemIdArray, string lang);

		// Token: 0x06001070 RID: 4208
		productServiceIdResponse getNewlyListedProductIdList(int startOffset, int rowCount, bool viaApi, string lang);

		// Token: 0x06001071 RID: 4209
		productServiceDetailResponse getProduct(string productId, string itemId, string lang);

		// Token: 0x06001072 RID: 4210
		productServiceDescResponse getProductDescription(string productId, string itemId, string lang);

		// Token: 0x06001073 RID: 4211
		productServiceListResponse getProducts(int startOffset, int rowCount, string status, bool withData, string lang);

		// Token: 0x06001074 RID: 4212
		productServiceSpecResponse getProductSpecs(string productId, string itemId, string lang);

		// Token: 0x06001075 RID: 4213
		productServiceStockResponse getStockAndPrice(List<int> productIdArray, List<string> itemIdArray, string lang);

		// Token: 0x06001076 RID: 4214
		productServiceResponse insertProduct(string itemId, productType productType, bool forceToSpecEntry, bool nextDateOption, string lang);

		// Token: 0x06001077 RID: 4215
		productServiceResponse insertProductWithNewCargoDetail(string itemId, productType productType, bool forceToSpecEntry, bool nextDateOption, string lang);

		// Token: 0x06001078 RID: 4216
		productServicePaymentResponse payPrice(string paymentVoucher, string ccOwnerName, string ccOwnerSurname, string ccNumber, string cvv, string expireMonth, string expireYear, string lang);

		// Token: 0x06001079 RID: 4217
		productServiceIdResponse relistProducts(List<int> productIdArray, List<string> itemIdArray, string lang);

		// Token: 0x0600107A RID: 4218
		productServiceResponse updatePrice(string productId, string itemId, double newPrice, bool cancelBid, string lang);

		// Token: 0x0600107B RID: 4219
		productServiceResponse updateProduct(string productId, string itemId, productType productType, bool onSale, bool forceToSpecEntry, bool nextDateOption, string lang);

		// Token: 0x0600107C RID: 4220
		productServiceResponse updateStock(string productId, string itemId, int stock, bool cancelBid, string lang);

		// Token: 0x0600107D RID: 4221
		productServiceResponse updateProductWithNewCargoDetail(string itemId, string productId, productType productType, bool onSale, bool forceToSpecEntry, bool nextDateOption, string lang);

		// Token: 0x0600107E RID: 4222
		productServiceRetailResponse updateProductVariants(string productId, string itemId, productVariantType productVariant, bool onSale, bool forceToSpecEntry, bool nextDateOption, string lang);
	}
}
