﻿using System.CodeDom.Compiler;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x02000178 RID: 376
	// (Invoke) Token: 0x06000CE2 RID: 3298
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	public delegate void updateProductCompletedEventHandler(object sender, updateProductCompletedEventArgs e);
}
