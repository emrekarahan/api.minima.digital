﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x02000143 RID: 323
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	public class deleteProductsCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000C43 RID: 3139 RVA: 0x00011886 File Offset: 0x0000FA86
		internal deleteProductsCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x170003E8 RID: 1000
		// (get) Token: 0x06000C44 RID: 3140 RVA: 0x00011899 File Offset: 0x0000FA99
		public productServiceProcessResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (productServiceProcessResponse)this.results[0];
			}
		}

		// Token: 0x040004AE RID: 1198
		private object[] results;
	}
}
