﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x02000114 RID: 276
	[GeneratedCode("System.Xml", "4.7.3190.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[XmlType(Namespace = "https://product.individual.ws.listingapi.gg.com")]
	[Serializable]
	public class productVariantType
	{
		// Token: 0x1700030A RID: 778
		// (get) Token: 0x06000A4C RID: 2636 RVA: 0x0001083D File Offset: 0x0000EA3D
		// (set) Token: 0x06000A4D RID: 2637 RVA: 0x00010845 File Offset: 0x0000EA45
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		[XmlArrayItem("variantGroup", Form = XmlSchemaForm.Unqualified, IsNullable = false)]
		public variantGroupType[] variantGroups
		{
			get
			{
				return this.variantGroupsField;
			}
			set
			{
				this.variantGroupsField = value;
			}
		}

		// Token: 0x040003CC RID: 972
		private variantGroupType[] variantGroupsField;
	}
}
