﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x02000145 RID: 325
	[DebuggerStepThrough]
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	[DesignerCategory("code")]
	public class finishEarlyCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000C49 RID: 3145 RVA: 0x000118AE File Offset: 0x0000FAAE
		internal finishEarlyCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x170003E9 RID: 1001
		// (get) Token: 0x06000C4A RID: 3146 RVA: 0x000118C1 File Offset: 0x0000FAC1
		public productServiceIdResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (productServiceIdResponse)this.results[0];
			}
		}

		// Token: 0x040004AF RID: 1199
		private object[] results;
	}
}
