﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x02000169 RID: 361
	[DesignerCategory("code")]
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	[DebuggerStepThrough]
	public class insertRetailProductCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000CB5 RID: 3253 RVA: 0x00011B7E File Offset: 0x0000FD7E
		internal insertRetailProductCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x170003FB RID: 1019
		// (get) Token: 0x06000CB6 RID: 3254 RVA: 0x00011B91 File Offset: 0x0000FD91
		public productServiceRetailResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (productServiceRetailResponse)this.results[0];
			}
		}

		// Token: 0x040004C1 RID: 1217
		private object[] results;
	}
}
