﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x02000129 RID: 297
	[DesignerCategory("code")]
	[XmlType(Namespace = "https://product.individual.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.7.3190.0")]
	[DebuggerStepThrough]
	[Serializable]
	public class productServiceImageStatusResponse : baseResponse
	{
		// Token: 0x170003A9 RID: 937
		// (get) Token: 0x06000B9E RID: 2974 RVA: 0x0001136C File Offset: 0x0000F56C
		// (set) Token: 0x06000B9F RID: 2975 RVA: 0x00011374 File Offset: 0x0000F574
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string productId
		{
			get
			{
				return this.productIdField;
			}
			set
			{
				this.productIdField = value;
			}
		}

		// Token: 0x170003AA RID: 938
		// (get) Token: 0x06000BA0 RID: 2976 RVA: 0x0001137D File Offset: 0x0000F57D
		// (set) Token: 0x06000BA1 RID: 2977 RVA: 0x00011385 File Offset: 0x0000F585
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public bool isDelayed
		{
			get
			{
				return this.isDelayedField;
			}
			set
			{
				this.isDelayedField = value;
			}
		}

		// Token: 0x170003AB RID: 939
		// (get) Token: 0x06000BA2 RID: 2978 RVA: 0x0001138E File Offset: 0x0000F58E
		// (set) Token: 0x06000BA3 RID: 2979 RVA: 0x00011396 File Offset: 0x0000F596
		[XmlArrayItem("photo", Form = XmlSchemaForm.Unqualified, IsNullable = false)]
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		public imageLogValueType[] photos
		{
			get
			{
				return this.photosField;
			}
			set
			{
				this.photosField = value;
			}
		}

		// Token: 0x0400046F RID: 1135
		private string productIdField;

		// Token: 0x04000470 RID: 1136
		private bool isDelayedField;

		// Token: 0x04000471 RID: 1137
		private imageLogValueType[] photosField;
	}
}
