﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x0200012F RID: 303
	[GeneratedCode("System.Xml", "4.7.3190.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[XmlType(Namespace = "https://product.individual.ws.listingapi.gg.com")]
	[Serializable]
	public class productServiceVariantResponse : baseResponse
	{
		// Token: 0x170003B8 RID: 952
		// (get) Token: 0x06000BC2 RID: 3010 RVA: 0x0001149B File Offset: 0x0000F69B
		// (set) Token: 0x06000BC3 RID: 3011 RVA: 0x000114A3 File Offset: 0x0000F6A3
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int productId
		{
			get
			{
				return this.productIdField;
			}
			set
			{
				this.productIdField = value;
			}
		}

		// Token: 0x170003B9 RID: 953
		// (get) Token: 0x06000BC4 RID: 3012 RVA: 0x000114AC File Offset: 0x0000F6AC
		// (set) Token: 0x06000BC5 RID: 3013 RVA: 0x000114B4 File Offset: 0x0000F6B4
		[XmlIgnore]
		public bool productIdSpecified
		{
			get
			{
				return this.productIdFieldSpecified;
			}
			set
			{
				this.productIdFieldSpecified = value;
			}
		}

		// Token: 0x170003BA RID: 954
		// (get) Token: 0x06000BC6 RID: 3014 RVA: 0x000114BD File Offset: 0x0000F6BD
		// (set) Token: 0x06000BC7 RID: 3015 RVA: 0x000114C5 File Offset: 0x0000F6C5
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string itemId
		{
			get
			{
				return this.itemIdField;
			}
			set
			{
				this.itemIdField = value;
			}
		}

		// Token: 0x170003BB RID: 955
		// (get) Token: 0x06000BC8 RID: 3016 RVA: 0x000114CE File Offset: 0x0000F6CE
		// (set) Token: 0x06000BC9 RID: 3017 RVA: 0x000114D6 File Offset: 0x0000F6D6
		[XmlArrayItem("variantGroup", Form = XmlSchemaForm.Unqualified, IsNullable = false)]
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		public variantGroupType[] variantGroups
		{
			get
			{
				return this.variantGroupsField;
			}
			set
			{
				this.variantGroupsField = value;
			}
		}

		// Token: 0x0400047E RID: 1150
		private int productIdField;

		// Token: 0x0400047F RID: 1151
		private bool productIdFieldSpecified;

		// Token: 0x04000480 RID: 1152
		private string itemIdField;

		// Token: 0x04000481 RID: 1153
		private variantGroupType[] variantGroupsField;
	}
}
