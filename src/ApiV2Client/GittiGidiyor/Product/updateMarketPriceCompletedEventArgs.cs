﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x02000173 RID: 371
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	public class updateMarketPriceCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000CD3 RID: 3283 RVA: 0x00011C46 File Offset: 0x0000FE46
		internal updateMarketPriceCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x17000400 RID: 1024
		// (get) Token: 0x06000CD4 RID: 3284 RVA: 0x00011C59 File Offset: 0x0000FE59
		public productServiceResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (productServiceResponse)this.results[0];
			}
		}

		// Token: 0x040004C6 RID: 1222
		private object[] results;
	}
}
