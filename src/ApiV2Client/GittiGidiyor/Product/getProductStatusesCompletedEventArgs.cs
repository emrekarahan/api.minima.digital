﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x02000159 RID: 345
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	public class getProductStatusesCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000C85 RID: 3205 RVA: 0x00011A3E File Offset: 0x0000FC3E
		internal getProductStatusesCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x170003F3 RID: 1011
		// (get) Token: 0x06000C86 RID: 3206 RVA: 0x00011A51 File Offset: 0x0000FC51
		public productServiceStatusResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (productServiceStatusResponse)this.results[0];
			}
		}

		// Token: 0x040004B9 RID: 1209
		private object[] results;
	}
}
