﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x02000179 RID: 377
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	public class updateProductCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000CE5 RID: 3301 RVA: 0x00011CBE File Offset: 0x0000FEBE
		internal updateProductCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x17000403 RID: 1027
		// (get) Token: 0x06000CE6 RID: 3302 RVA: 0x00011CD1 File Offset: 0x0000FED1
		public productServiceResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (productServiceResponse)this.results[0];
			}
		}

		// Token: 0x040004C9 RID: 1225
		private object[] results;
	}
}
