﻿using System.CodeDom.Compiler;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x02000158 RID: 344
	// (Invoke) Token: 0x06000C82 RID: 3202
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	public delegate void getProductStatusesCompletedEventHandler(object sender, getProductStatusesCompletedEventArgs e);
}
