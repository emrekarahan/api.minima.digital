﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x02000135 RID: 309
	[XmlType(Namespace = "https://product.individual.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.7.3190.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[Serializable]
	public class productServiceDetailResponse : baseResponse
	{
		// Token: 0x170003DB RID: 987
		// (get) Token: 0x06000C0E RID: 3086 RVA: 0x0001171E File Offset: 0x0000F91E
		// (set) Token: 0x06000C0F RID: 3087 RVA: 0x00011726 File Offset: 0x0000F926
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public productDetailType productDetail
		{
			get
			{
				return this.productDetailField;
			}
			set
			{
				this.productDetailField = value;
			}
		}

		// Token: 0x040004A1 RID: 1185
		private productDetailType productDetailField;
	}
}
