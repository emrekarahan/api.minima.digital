﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x0200015B RID: 347
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	public class getProductVariantsCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000C8B RID: 3211 RVA: 0x00011A66 File Offset: 0x0000FC66
		internal getProductVariantsCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x170003F4 RID: 1012
		// (get) Token: 0x06000C8C RID: 3212 RVA: 0x00011A79 File Offset: 0x0000FC79
		public productServiceVariantResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (productServiceVariantResponse)this.results[0];
			}
		}

		// Token: 0x040004BA RID: 1210
		private object[] results;
	}
}
