﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x0200016F RID: 367
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	public class relistProductsCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000CC7 RID: 3271 RVA: 0x00011BF6 File Offset: 0x0000FDF6
		internal relistProductsCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x170003FE RID: 1022
		// (get) Token: 0x06000CC8 RID: 3272 RVA: 0x00011C09 File Offset: 0x0000FE09
		public productServiceIdResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (productServiceIdResponse)this.results[0];
			}
		}

		// Token: 0x040004C4 RID: 1220
		private object[] results;
	}
}
