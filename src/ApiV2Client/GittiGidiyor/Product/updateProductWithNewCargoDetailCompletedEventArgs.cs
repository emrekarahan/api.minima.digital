﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x0200017D RID: 381
	[DesignerCategory("code")]
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	[DebuggerStepThrough]
	public class updateProductWithNewCargoDetailCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000CF1 RID: 3313 RVA: 0x00011D0E File Offset: 0x0000FF0E
		internal updateProductWithNewCargoDetailCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x17000405 RID: 1029
		// (get) Token: 0x06000CF2 RID: 3314 RVA: 0x00011D21 File Offset: 0x0000FF21
		public productServiceResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (productServiceResponse)this.results[0];
			}
		}

		// Token: 0x040004CB RID: 1227
		private object[] results;
	}
}
