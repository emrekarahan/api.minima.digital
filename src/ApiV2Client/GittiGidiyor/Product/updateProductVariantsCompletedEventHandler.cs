﻿using System.CodeDom.Compiler;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x0200017A RID: 378
	// (Invoke) Token: 0x06000CE8 RID: 3304
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	public delegate void updateProductVariantsCompletedEventHandler(object sender, updateProductVariantsCompletedEventArgs e);
}
