﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x02000137 RID: 311
	[GeneratedCode("System.Xml", "4.7.3190.0")]
	[DesignerCategory("code")]
	[XmlType(Namespace = "https://product.individual.ws.listingapi.gg.com")]
	[DebuggerStepThrough]
	[Serializable]
	public class productServiceListResponse : baseResponse
	{
		// Token: 0x170003E0 RID: 992
		// (get) Token: 0x06000C1A RID: 3098 RVA: 0x00011783 File Offset: 0x0000F983
		// (set) Token: 0x06000C1B RID: 3099 RVA: 0x0001178B File Offset: 0x0000F98B
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public long productCount
		{
			get
			{
				return this.productCountField;
			}
			set
			{
				this.productCountField = value;
			}
		}

		// Token: 0x170003E1 RID: 993
		// (get) Token: 0x06000C1C RID: 3100 RVA: 0x00011794 File Offset: 0x0000F994
		// (set) Token: 0x06000C1D RID: 3101 RVA: 0x0001179C File Offset: 0x0000F99C
		[XmlIgnore]
		public bool productCountSpecified
		{
			get
			{
				return this.productCountFieldSpecified;
			}
			set
			{
				this.productCountFieldSpecified = value;
			}
		}

		// Token: 0x170003E2 RID: 994
		// (get) Token: 0x06000C1E RID: 3102 RVA: 0x000117A5 File Offset: 0x0000F9A5
		// (set) Token: 0x06000C1F RID: 3103 RVA: 0x000117AD File Offset: 0x0000F9AD
		[XmlArrayItem("product", Form = XmlSchemaForm.Unqualified)]
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		public productDetailType[] products
		{
			get
			{
				return this.productsField;
			}
			set
			{
				this.productsField = value;
			}
		}

		// Token: 0x040004A6 RID: 1190
		private long productCountField;

		// Token: 0x040004A7 RID: 1191
		private bool productCountFieldSpecified;

		// Token: 0x040004A8 RID: 1192
		private productDetailType[] productsField;
	}
}
