﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x02000177 RID: 375
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	[DesignerCategory("code")]
	[DebuggerStepThrough]
	public class updatePriceByPercentageCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000CDF RID: 3295 RVA: 0x00011C96 File Offset: 0x0000FE96
		internal updatePriceByPercentageCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x17000402 RID: 1026
		// (get) Token: 0x06000CE0 RID: 3296 RVA: 0x00011CA9 File Offset: 0x0000FEA9
		public productServiceResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (productServiceResponse)this.results[0];
			}
		}

		// Token: 0x040004C8 RID: 1224
		private object[] results;
	}
}
