﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x02000147 RID: 327
	[DebuggerStepThrough]
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	[DesignerCategory("code")]
	public class finishEarlyProductsCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000C4F RID: 3151 RVA: 0x000118D6 File Offset: 0x0000FAD6
		internal finishEarlyProductsCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x170003EA RID: 1002
		// (get) Token: 0x06000C50 RID: 3152 RVA: 0x000118E9 File Offset: 0x0000FAE9
		public productServiceProcessResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (productServiceProcessResponse)this.results[0];
			}
		}

		// Token: 0x040004B0 RID: 1200
		private object[] results;
	}
}
