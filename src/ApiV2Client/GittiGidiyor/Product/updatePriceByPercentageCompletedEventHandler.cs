﻿using System.CodeDom.Compiler;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x02000176 RID: 374
	// (Invoke) Token: 0x06000CDC RID: 3292
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	public delegate void updatePriceByPercentageCompletedEventHandler(object sender, updatePriceByPercentageCompletedEventArgs e);
}
