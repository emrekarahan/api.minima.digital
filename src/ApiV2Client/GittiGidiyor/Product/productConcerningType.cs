﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x02000121 RID: 289
	[XmlType(Namespace = "https://product.individual.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.7.3190.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[Serializable]
	public class productConcerningType
	{
		// Token: 0x1700035A RID: 858
		// (get) Token: 0x06000AF8 RID: 2808 RVA: 0x00010DED File Offset: 0x0000EFED
		// (set) Token: 0x06000AF9 RID: 2809 RVA: 0x00010DF5 File Offset: 0x0000EFF5
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int soldCount
		{
			get
			{
				return this.soldCountField;
			}
			set
			{
				this.soldCountField = value;
			}
		}

		// Token: 0x1700035B RID: 859
		// (get) Token: 0x06000AFA RID: 2810 RVA: 0x00010DFE File Offset: 0x0000EFFE
		// (set) Token: 0x06000AFB RID: 2811 RVA: 0x00010E06 File Offset: 0x0000F006
		[XmlIgnore]
		public bool soldCountSpecified
		{
			get
			{
				return this.soldCountFieldSpecified;
			}
			set
			{
				this.soldCountFieldSpecified = value;
			}
		}

		// Token: 0x1700035C RID: 860
		// (get) Token: 0x06000AFC RID: 2812 RVA: 0x00010E0F File Offset: 0x0000F00F
		// (set) Token: 0x06000AFD RID: 2813 RVA: 0x00010E17 File Offset: 0x0000F017
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string endDate
		{
			get
			{
				return this.endDateField;
			}
			set
			{
				this.endDateField = value;
			}
		}

		// Token: 0x1700035D RID: 861
		// (get) Token: 0x06000AFE RID: 2814 RVA: 0x00010E20 File Offset: 0x0000F020
		// (set) Token: 0x06000AFF RID: 2815 RVA: 0x00010E28 File Offset: 0x0000F028
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string listingStatus
		{
			get
			{
				return this.listingStatusField;
			}
			set
			{
				this.listingStatusField = value;
			}
		}

		// Token: 0x1700035E RID: 862
		// (get) Token: 0x06000B00 RID: 2816 RVA: 0x00010E31 File Offset: 0x0000F031
		// (set) Token: 0x06000B01 RID: 2817 RVA: 0x00010E39 File Offset: 0x0000F039
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string currentPrice
		{
			get
			{
				return this.currentPriceField;
			}
			set
			{
				this.currentPriceField = value;
			}
		}

		// Token: 0x1700035F RID: 863
		// (get) Token: 0x06000B02 RID: 2818 RVA: 0x00010E42 File Offset: 0x0000F042
		// (set) Token: 0x06000B03 RID: 2819 RVA: 0x00010E4A File Offset: 0x0000F04A
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public bool hasRelistOption
		{
			get
			{
				return this.hasRelistOptionField;
			}
			set
			{
				this.hasRelistOptionField = value;
			}
		}

		// Token: 0x17000360 RID: 864
		// (get) Token: 0x06000B04 RID: 2820 RVA: 0x00010E53 File Offset: 0x0000F053
		// (set) Token: 0x06000B05 RID: 2821 RVA: 0x00010E5B File Offset: 0x0000F05B
		[XmlIgnore]
		public bool hasRelistOptionSpecified
		{
			get
			{
				return this.hasRelistOptionFieldSpecified;
			}
			set
			{
				this.hasRelistOptionFieldSpecified = value;
			}
		}

		// Token: 0x17000361 RID: 865
		// (get) Token: 0x06000B06 RID: 2822 RVA: 0x00010E64 File Offset: 0x0000F064
		// (set) Token: 0x06000B07 RID: 2823 RVA: 0x00010E6C File Offset: 0x0000F06C
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int bidCount
		{
			get
			{
				return this.bidCountField;
			}
			set
			{
				this.bidCountField = value;
			}
		}

		// Token: 0x17000362 RID: 866
		// (get) Token: 0x06000B08 RID: 2824 RVA: 0x00010E75 File Offset: 0x0000F075
		// (set) Token: 0x06000B09 RID: 2825 RVA: 0x00010E7D File Offset: 0x0000F07D
		[XmlIgnore]
		public bool bidCountSpecified
		{
			get
			{
				return this.bidCountFieldSpecified;
			}
			set
			{
				this.bidCountFieldSpecified = value;
			}
		}

		// Token: 0x04000420 RID: 1056
		private int soldCountField;

		// Token: 0x04000421 RID: 1057
		private bool soldCountFieldSpecified;

		// Token: 0x04000422 RID: 1058
		private string endDateField;

		// Token: 0x04000423 RID: 1059
		private string listingStatusField;

		// Token: 0x04000424 RID: 1060
		private string currentPriceField;

		// Token: 0x04000425 RID: 1061
		private bool hasRelistOptionField;

		// Token: 0x04000426 RID: 1062
		private bool hasRelistOptionFieldSpecified;

		// Token: 0x04000427 RID: 1063
		private int bidCountField;

		// Token: 0x04000428 RID: 1064
		private bool bidCountFieldSpecified;
	}
}
