﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x0200012C RID: 300
	[DesignerCategory("code")]
	[DebuggerStepThrough]
	[XmlType(Namespace = "https://product.individual.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.7.3190.0")]
	[Serializable]
	public class itemIdDetailResponse : baseResponse
	{
		// Token: 0x170003B4 RID: 948
		// (get) Token: 0x06000BB7 RID: 2999 RVA: 0x0001143F File Offset: 0x0000F63F
		// (set) Token: 0x06000BB8 RID: 3000 RVA: 0x00011447 File Offset: 0x0000F647
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		[XmlArrayItem("itemIdDetail", Form = XmlSchemaForm.Unqualified, IsNullable = false)]
		public itemIdDetailType[] itemIdDetails
		{
			get
			{
				return this.itemIdDetailsField;
			}
			set
			{
				this.itemIdDetailsField = value;
			}
		}

		// Token: 0x0400047A RID: 1146
		private itemIdDetailType[] itemIdDetailsField;
	}
}
