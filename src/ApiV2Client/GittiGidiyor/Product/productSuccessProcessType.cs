﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x02000120 RID: 288
	[XmlType(Namespace = "https://product.individual.ws.listingapi.gg.com")]
	[DebuggerStepThrough]
	[GeneratedCode("System.Xml", "4.7.3190.0")]
	[DesignerCategory("code")]
	[Serializable]
	public class productSuccessProcessType
	{
		// Token: 0x17000357 RID: 855
		// (get) Token: 0x06000AF1 RID: 2801 RVA: 0x00010DB2 File Offset: 0x0000EFB2
		// (set) Token: 0x06000AF2 RID: 2802 RVA: 0x00010DBA File Offset: 0x0000EFBA
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int productId
		{
			get
			{
				return this.productIdField;
			}
			set
			{
				this.productIdField = value;
			}
		}

		// Token: 0x17000358 RID: 856
		// (get) Token: 0x06000AF3 RID: 2803 RVA: 0x00010DC3 File Offset: 0x0000EFC3
		// (set) Token: 0x06000AF4 RID: 2804 RVA: 0x00010DCB File Offset: 0x0000EFCB
		[XmlIgnore]
		public bool productIdSpecified
		{
			get
			{
				return this.productIdFieldSpecified;
			}
			set
			{
				this.productIdFieldSpecified = value;
			}
		}

		// Token: 0x17000359 RID: 857
		// (get) Token: 0x06000AF5 RID: 2805 RVA: 0x00010DD4 File Offset: 0x0000EFD4
		// (set) Token: 0x06000AF6 RID: 2806 RVA: 0x00010DDC File Offset: 0x0000EFDC
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string changeStatus
		{
			get
			{
				return this.changeStatusField;
			}
			set
			{
				this.changeStatusField = value;
			}
		}

		// Token: 0x0400041D RID: 1053
		private int productIdField;

		// Token: 0x0400041E RID: 1054
		private bool productIdFieldSpecified;

		// Token: 0x0400041F RID: 1055
		private string changeStatusField;
	}
}
