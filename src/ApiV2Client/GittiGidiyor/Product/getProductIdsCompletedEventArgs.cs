﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Product
{
	// Token: 0x02000153 RID: 339
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	public class getProductIdsCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000C73 RID: 3187 RVA: 0x000119C6 File Offset: 0x0000FBC6
		internal getProductIdsCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x170003F0 RID: 1008
		// (get) Token: 0x06000C74 RID: 3188 RVA: 0x000119D9 File Offset: 0x0000FBD9
		public productServiceIdResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (productServiceIdResponse)this.results[0];
			}
		}

		// Token: 0x040004B6 RID: 1206
		private object[] results;
	}
}
