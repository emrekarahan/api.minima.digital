﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Catalog
{
	// Token: 0x02000080 RID: 128
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	public class searchCatalogCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x060004A7 RID: 1191 RVA: 0x0000709F File Offset: 0x0000529F
		internal searchCatalogCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x1700019E RID: 414
		// (get) Token: 0x060004A8 RID: 1192 RVA: 0x000070B2 File Offset: 0x000052B2
		public catalogServiceResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (catalogServiceResponse)this.results[0];
			}
		}

		// Token: 0x040001C9 RID: 457
		private object[] results;
	}
}
