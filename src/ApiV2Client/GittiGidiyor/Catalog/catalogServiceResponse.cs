﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Catalog
{
	// Token: 0x02000078 RID: 120
	[DesignerCategory("code")]
	[XmlType(Namespace = "http://catalog.anonymous.ws.listingapi.gg.com")]
	[DebuggerStepThrough]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[Serializable]
	public class catalogServiceResponse : baseResponse
	{
		// Token: 0x1700018E RID: 398
		// (get) Token: 0x06000478 RID: 1144 RVA: 0x00006F49 File Offset: 0x00005149
		// (set) Token: 0x06000479 RID: 1145 RVA: 0x00006F51 File Offset: 0x00005151
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int count
		{
			get
			{
				return this.countField;
			}
			set
			{
				this.countField = value;
			}
		}

		// Token: 0x1700018F RID: 399
		// (get) Token: 0x0600047A RID: 1146 RVA: 0x00006F5A File Offset: 0x0000515A
		// (set) Token: 0x0600047B RID: 1147 RVA: 0x00006F62 File Offset: 0x00005162
		[XmlIgnore]
		public bool countSpecified
		{
			get
			{
				return this.countFieldSpecified;
			}
			set
			{
				this.countFieldSpecified = value;
			}
		}

		// Token: 0x17000190 RID: 400
		// (get) Token: 0x0600047C RID: 1148 RVA: 0x00006F6B File Offset: 0x0000516B
		// (set) Token: 0x0600047D RID: 1149 RVA: 0x00006F73 File Offset: 0x00005173
		[XmlArrayItem("catalog", Form = XmlSchemaForm.Unqualified)]
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		public catalogType[] catalogs
		{
			get
			{
				return this.catalogsField;
			}
			set
			{
				this.catalogsField = value;
			}
		}

		// Token: 0x040001B9 RID: 441
		private int countField;

		// Token: 0x040001BA RID: 442
		private bool countFieldSpecified;

		// Token: 0x040001BB RID: 443
		private catalogType[] catalogsField;
	}
}
