﻿using System.CodeDom.Compiler;

namespace ApiV2Client.GittiGidiyor.Catalog
{
	// Token: 0x0200007F RID: 127
	// (Invoke) Token: 0x060004A4 RID: 1188
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	public delegate void searchCatalogCompletedEventHandler(object sender, searchCatalogCompletedEventArgs e);
}
