﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Catalog
{
	// Token: 0x0200007C RID: 124
	[DebuggerStepThrough]
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	[DesignerCategory("code")]
	public class getCatalogDetailCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x0600049B RID: 1179 RVA: 0x0000704F File Offset: 0x0000524F
		internal getCatalogDetailCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x1700019C RID: 412
		// (get) Token: 0x0600049C RID: 1180 RVA: 0x00007062 File Offset: 0x00005262
		public catalogServiceResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (catalogServiceResponse)this.results[0];
			}
		}

		// Token: 0x040001C7 RID: 455
		private object[] results;
	}
}
