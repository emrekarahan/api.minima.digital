﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Catalog
{
	// Token: 0x0200007A RID: 122
	[DebuggerStepThrough]
	[XmlType(Namespace = "http://catalog.anonymous.ws.listingapi.gg.com")]
	[DesignerCategory("code")]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[Serializable]
	public class errorType
	{
		// Token: 0x17000198 RID: 408
		// (get) Token: 0x0600048E RID: 1166 RVA: 0x00007003 File Offset: 0x00005203
		// (set) Token: 0x0600048F RID: 1167 RVA: 0x0000700B File Offset: 0x0000520B
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string errorId
		{
			get
			{
				return this.errorIdField;
			}
			set
			{
				this.errorIdField = value;
			}
		}

		// Token: 0x17000199 RID: 409
		// (get) Token: 0x06000490 RID: 1168 RVA: 0x00007014 File Offset: 0x00005214
		// (set) Token: 0x06000491 RID: 1169 RVA: 0x0000701C File Offset: 0x0000521C
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string errorCode
		{
			get
			{
				return this.errorCodeField;
			}
			set
			{
				this.errorCodeField = value;
			}
		}

		// Token: 0x1700019A RID: 410
		// (get) Token: 0x06000492 RID: 1170 RVA: 0x00007025 File Offset: 0x00005225
		// (set) Token: 0x06000493 RID: 1171 RVA: 0x0000702D File Offset: 0x0000522D
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string message
		{
			get
			{
				return this.messageField;
			}
			set
			{
				this.messageField = value;
			}
		}

		// Token: 0x1700019B RID: 411
		// (get) Token: 0x06000494 RID: 1172 RVA: 0x00007036 File Offset: 0x00005236
		// (set) Token: 0x06000495 RID: 1173 RVA: 0x0000703E File Offset: 0x0000523E
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string viewMessage
		{
			get
			{
				return this.viewMessageField;
			}
			set
			{
				this.viewMessageField = value;
			}
		}

		// Token: 0x040001C3 RID: 451
		private string errorIdField;

		// Token: 0x040001C4 RID: 452
		private string errorCodeField;

		// Token: 0x040001C5 RID: 453
		private string messageField;

		// Token: 0x040001C6 RID: 454
		private string viewMessageField;
	}
}
