﻿using System.CodeDom.Compiler;

namespace ApiV2Client.GittiGidiyor.Catalog
{
	// Token: 0x0200007B RID: 123
	// (Invoke) Token: 0x06000498 RID: 1176
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	public delegate void getCatalogDetailCompletedEventHandler(object sender, getCatalogDetailCompletedEventArgs e);
}
