﻿namespace ApiV2Client.GittiGidiyor.Catalog
{
	// Token: 0x020001E4 RID: 484
	public interface ICatalogService : IService
	{
		// Token: 0x0600107F RID: 4223
		catalogServiceResponse getCatalogDetail(int catalogId, string lang);

		// Token: 0x06001080 RID: 4224
		catalogServiceResponse searchCatalog(string categoryCode, string title, int startOffSet, int rowCount, string lang);
	}
}
