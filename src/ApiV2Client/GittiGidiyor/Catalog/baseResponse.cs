﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Catalog
{
	// Token: 0x02000077 RID: 119
	[XmlInclude(typeof(catalogServiceResponse))]
	[XmlType(Namespace = "http://catalog.anonymous.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[Serializable]
	public class baseResponse
	{
		// Token: 0x1700018A RID: 394
		// (get) Token: 0x0600046F RID: 1135 RVA: 0x00006EFD File Offset: 0x000050FD
		// (set) Token: 0x06000470 RID: 1136 RVA: 0x00006F05 File Offset: 0x00005105
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string ackCode
		{
			get
			{
				return this.ackCodeField;
			}
			set
			{
				this.ackCodeField = value;
			}
		}

		// Token: 0x1700018B RID: 395
		// (get) Token: 0x06000471 RID: 1137 RVA: 0x00006F0E File Offset: 0x0000510E
		// (set) Token: 0x06000472 RID: 1138 RVA: 0x00006F16 File Offset: 0x00005116
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string responseTime
		{
			get
			{
				return this.responseTimeField;
			}
			set
			{
				this.responseTimeField = value;
			}
		}

		// Token: 0x1700018C RID: 396
		// (get) Token: 0x06000473 RID: 1139 RVA: 0x00006F1F File Offset: 0x0000511F
		// (set) Token: 0x06000474 RID: 1140 RVA: 0x00006F27 File Offset: 0x00005127
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public errorType error
		{
			get
			{
				return this.errorField;
			}
			set
			{
				this.errorField = value;
			}
		}

		// Token: 0x1700018D RID: 397
		// (get) Token: 0x06000475 RID: 1141 RVA: 0x00006F30 File Offset: 0x00005130
		// (set) Token: 0x06000476 RID: 1142 RVA: 0x00006F38 File Offset: 0x00005138
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string timeElapsed
		{
			get
			{
				return this.timeElapsedField;
			}
			set
			{
				this.timeElapsedField = value;
			}
		}

		// Token: 0x040001B5 RID: 437
		private string ackCodeField;

		// Token: 0x040001B6 RID: 438
		private string responseTimeField;

		// Token: 0x040001B7 RID: 439
		private errorType errorField;

		// Token: 0x040001B8 RID: 440
		private string timeElapsedField;
	}
}
