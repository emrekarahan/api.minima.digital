﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Catalog
{
	// Token: 0x02000079 RID: 121
	[DebuggerStepThrough]
	[XmlType(Namespace = "http://catalog.anonymous.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[DesignerCategory("code")]
	[Serializable]
	public class catalogType
	{
		// Token: 0x17000191 RID: 401
		// (get) Token: 0x0600047F RID: 1151 RVA: 0x00006F84 File Offset: 0x00005184
		// (set) Token: 0x06000480 RID: 1152 RVA: 0x00006F8C File Offset: 0x0000518C
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int catalogId
		{
			get
			{
				return this.catalogIdField;
			}
			set
			{
				this.catalogIdField = value;
			}
		}

		// Token: 0x17000192 RID: 402
		// (get) Token: 0x06000481 RID: 1153 RVA: 0x00006F95 File Offset: 0x00005195
		// (set) Token: 0x06000482 RID: 1154 RVA: 0x00006F9D File Offset: 0x0000519D
		[XmlIgnore]
		public bool catalogIdSpecified
		{
			get
			{
				return this.catalogIdFieldSpecified;
			}
			set
			{
				this.catalogIdFieldSpecified = value;
			}
		}

		// Token: 0x17000193 RID: 403
		// (get) Token: 0x06000483 RID: 1155 RVA: 0x00006FA6 File Offset: 0x000051A6
		// (set) Token: 0x06000484 RID: 1156 RVA: 0x00006FAE File Offset: 0x000051AE
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string brandName
		{
			get
			{
				return this.brandNameField;
			}
			set
			{
				this.brandNameField = value;
			}
		}

		// Token: 0x17000194 RID: 404
		// (get) Token: 0x06000485 RID: 1157 RVA: 0x00006FB7 File Offset: 0x000051B7
		// (set) Token: 0x06000486 RID: 1158 RVA: 0x00006FBF File Offset: 0x000051BF
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string title
		{
			get
			{
				return this.titleField;
			}
			set
			{
				this.titleField = value;
			}
		}

		// Token: 0x17000195 RID: 405
		// (get) Token: 0x06000487 RID: 1159 RVA: 0x00006FC8 File Offset: 0x000051C8
		// (set) Token: 0x06000488 RID: 1160 RVA: 0x00006FD0 File Offset: 0x000051D0
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string description
		{
			get
			{
				return this.descriptionField;
			}
			set
			{
				this.descriptionField = value;
			}
		}

		// Token: 0x17000196 RID: 406
		// (get) Token: 0x06000489 RID: 1161 RVA: 0x00006FD9 File Offset: 0x000051D9
		// (set) Token: 0x0600048A RID: 1162 RVA: 0x00006FE1 File Offset: 0x000051E1
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string thumbnailImage
		{
			get
			{
				return this.thumbnailImageField;
			}
			set
			{
				this.thumbnailImageField = value;
			}
		}

		// Token: 0x17000197 RID: 407
		// (get) Token: 0x0600048B RID: 1163 RVA: 0x00006FEA File Offset: 0x000051EA
		// (set) Token: 0x0600048C RID: 1164 RVA: 0x00006FF2 File Offset: 0x000051F2
		[XmlArrayItem("image", Form = XmlSchemaForm.Unqualified, IsNullable = false)]
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		public string[] images
		{
			get
			{
				return this.imagesField;
			}
			set
			{
				this.imagesField = value;
			}
		}

		// Token: 0x040001BC RID: 444
		private int catalogIdField;

		// Token: 0x040001BD RID: 445
		private bool catalogIdFieldSpecified;

		// Token: 0x040001BE RID: 446
		private string brandNameField;

		// Token: 0x040001BF RID: 447
		private string titleField;

		// Token: 0x040001C0 RID: 448
		private string descriptionField;

		// Token: 0x040001C1 RID: 449
		private string thumbnailImageField;

		// Token: 0x040001C2 RID: 450
		private string[] imagesField;
	}
}
