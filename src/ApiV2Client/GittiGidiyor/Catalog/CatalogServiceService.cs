﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading;
using System.Web.Services;
using System.Web.Services.Description;
using System.Web.Services.Protocols;
using System.Xml.Serialization;
using ApiV2Client.Properties;

namespace ApiV2Client.GittiGidiyor.Catalog
{
	// Token: 0x02000076 RID: 118
	[DesignerCategory("code")]
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	[DebuggerStepThrough]
	[XmlInclude(typeof(baseResponse))]
	[WebServiceBinding(Name = "CatalogServiceBinding", Namespace = "http://catalog.anonymous.ws.listingapi.gg.com")]
	public class CatalogServiceService : SoapHttpClientProtocol
	{
		// Token: 0x06000456 RID: 1110 RVA: 0x00006A41 File Offset: 0x00004C41
		public CatalogServiceService()
		{
			this.Url = Settings.Default.ApiV2Client_GittiGidiyor_Catalog_CatalogServiceService;
			if (this.IsLocalFileSystemWebService(this.Url))
			{
				this.UseDefaultCredentials = true;
				this.useDefaultCredentialsSetExplicitly = false;
				return;
			}
			this.useDefaultCredentialsSetExplicitly = true;
		}

		// Token: 0x17000188 RID: 392
		// (get) Token: 0x06000457 RID: 1111 RVA: 0x00006A7D File Offset: 0x00004C7D
		// (set) Token: 0x06000458 RID: 1112 RVA: 0x00006A85 File Offset: 0x00004C85
		public new string Url
		{
			get
			{
				return base.Url;
			}
			set
			{
				if (this.IsLocalFileSystemWebService(base.Url) && !this.useDefaultCredentialsSetExplicitly && !this.IsLocalFileSystemWebService(value))
				{
					base.UseDefaultCredentials = false;
				}
				base.Url = value;
			}
		}

		// Token: 0x17000189 RID: 393
		// (get) Token: 0x06000459 RID: 1113 RVA: 0x00006AB4 File Offset: 0x00004CB4
		// (set) Token: 0x0600045A RID: 1114 RVA: 0x00006ABC File Offset: 0x00004CBC
		public new bool UseDefaultCredentials
		{
			get
			{
				return base.UseDefaultCredentials;
			}
			set
			{
				base.UseDefaultCredentials = value;
				this.useDefaultCredentialsSetExplicitly = true;
			}
		}

		// Token: 0x1400001B RID: 27
		// (add) Token: 0x0600045B RID: 1115 RVA: 0x00006ACC File Offset: 0x00004CCC
		// (remove) Token: 0x0600045C RID: 1116 RVA: 0x00006B04 File Offset: 0x00004D04
		public event getCatalogDetailCompletedEventHandler getCatalogDetailCompleted;

		// Token: 0x1400001C RID: 28
		// (add) Token: 0x0600045D RID: 1117 RVA: 0x00006B3C File Offset: 0x00004D3C
		// (remove) Token: 0x0600045E RID: 1118 RVA: 0x00006B74 File Offset: 0x00004D74
		public event getServiceNameCompletedEventHandler getServiceNameCompleted;

		// Token: 0x1400001D RID: 29
		// (add) Token: 0x0600045F RID: 1119 RVA: 0x00006BAC File Offset: 0x00004DAC
		// (remove) Token: 0x06000460 RID: 1120 RVA: 0x00006BE4 File Offset: 0x00004DE4
		public event searchCatalogCompletedEventHandler searchCatalogCompleted;

		// Token: 0x06000461 RID: 1121 RVA: 0x00006C1C File Offset: 0x00004E1C
		[SoapRpcMethod("", RequestNamespace = "http://catalog.anonymous.ws.listingapi.gg.com", ResponseNamespace = "http://catalog.anonymous.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public catalogServiceResponse getCatalogDetail(int catalogId, string lang)
		{
			object[] array = base.Invoke("getCatalogDetail", new object[]
			{
				catalogId,
				lang
			});
			return (catalogServiceResponse)array[0];
		}

		// Token: 0x06000462 RID: 1122 RVA: 0x00006C52 File Offset: 0x00004E52
		public void getCatalogDetailAsync(int catalogId, string lang)
		{
			this.getCatalogDetailAsync(catalogId, lang, null);
		}

		// Token: 0x06000463 RID: 1123 RVA: 0x00006C60 File Offset: 0x00004E60
		public void getCatalogDetailAsync(int catalogId, string lang, object userState)
		{
			if (this.getCatalogDetailOperationCompleted == null)
			{
				this.getCatalogDetailOperationCompleted = new SendOrPostCallback(this.OngetCatalogDetailOperationCompleted);
			}
			base.InvokeAsync("getCatalogDetail", new object[]
			{
				catalogId,
				lang
			}, this.getCatalogDetailOperationCompleted, userState);
		}

		// Token: 0x06000464 RID: 1124 RVA: 0x00006CB0 File Offset: 0x00004EB0
		private void OngetCatalogDetailOperationCompleted(object arg)
		{
			if (this.getCatalogDetailCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getCatalogDetailCompleted(this, new getCatalogDetailCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x06000465 RID: 1125 RVA: 0x00006CF8 File Offset: 0x00004EF8
		[SoapRpcMethod("", RequestNamespace = "http://catalog.anonymous.ws.listingapi.gg.com", ResponseNamespace = "http://catalog.anonymous.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public string getServiceName()
		{
			object[] array = base.Invoke("getServiceName", new object[0]);
			return (string)array[0];
		}

		// Token: 0x06000466 RID: 1126 RVA: 0x00006D1F File Offset: 0x00004F1F
		public void getServiceNameAsync()
		{
			this.getServiceNameAsync(null);
		}

		// Token: 0x06000467 RID: 1127 RVA: 0x00006D28 File Offset: 0x00004F28
		public void getServiceNameAsync(object userState)
		{
			if (this.getServiceNameOperationCompleted == null)
			{
				this.getServiceNameOperationCompleted = new SendOrPostCallback(this.OngetServiceNameOperationCompleted);
			}
			base.InvokeAsync("getServiceName", new object[0], this.getServiceNameOperationCompleted, userState);
		}

		// Token: 0x06000468 RID: 1128 RVA: 0x00006D5C File Offset: 0x00004F5C
		private void OngetServiceNameOperationCompleted(object arg)
		{
			if (this.getServiceNameCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getServiceNameCompleted(this, new getServiceNameCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x06000469 RID: 1129 RVA: 0x00006DA4 File Offset: 0x00004FA4
		[SoapRpcMethod("", RequestNamespace = "http://catalog.anonymous.ws.listingapi.gg.com", ResponseNamespace = "http://catalog.anonymous.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public catalogServiceResponse searchCatalog(string categoryCode, string title, int startOffSet, int rowCount, string lang)
		{
			object[] array = base.Invoke("searchCatalog", new object[]
			{
				categoryCode,
				title,
				startOffSet,
				rowCount,
				lang
			});
			return (catalogServiceResponse)array[0];
		}

		// Token: 0x0600046A RID: 1130 RVA: 0x00006DED File Offset: 0x00004FED
		public void searchCatalogAsync(string categoryCode, string title, int startOffSet, int rowCount, string lang)
		{
			this.searchCatalogAsync(categoryCode, title, startOffSet, rowCount, lang, null);
		}

		// Token: 0x0600046B RID: 1131 RVA: 0x00006E00 File Offset: 0x00005000
		public void searchCatalogAsync(string categoryCode, string title, int startOffSet, int rowCount, string lang, object userState)
		{
			if (this.searchCatalogOperationCompleted == null)
			{
				this.searchCatalogOperationCompleted = new SendOrPostCallback(this.OnsearchCatalogOperationCompleted);
			}
			base.InvokeAsync("searchCatalog", new object[]
			{
				categoryCode,
				title,
				startOffSet,
				rowCount,
				lang
			}, this.searchCatalogOperationCompleted, userState);
		}

		// Token: 0x0600046C RID: 1132 RVA: 0x00006E64 File Offset: 0x00005064
		private void OnsearchCatalogOperationCompleted(object arg)
		{
			if (this.searchCatalogCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.searchCatalogCompleted(this, new searchCatalogCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x0600046D RID: 1133 RVA: 0x00006EA9 File Offset: 0x000050A9
		public new void CancelAsync(object userState)
		{
			base.CancelAsync(userState);
		}

		// Token: 0x0600046E RID: 1134 RVA: 0x00006EB4 File Offset: 0x000050B4
		private bool IsLocalFileSystemWebService(string url)
		{
			if (url == null || url == string.Empty)
			{
				return false;
			}
			Uri uri = new Uri(url);
			return uri.Port >= 1024 && string.Compare(uri.Host, "localHost", StringComparison.OrdinalIgnoreCase) == 0;
		}

		// Token: 0x040001AE RID: 430
		private SendOrPostCallback getCatalogDetailOperationCompleted;

		// Token: 0x040001AF RID: 431
		private SendOrPostCallback getServiceNameOperationCompleted;

		// Token: 0x040001B0 RID: 432
		private SendOrPostCallback searchCatalogOperationCompleted;

		// Token: 0x040001B1 RID: 433
		private bool useDefaultCredentialsSetExplicitly;
	}
}
