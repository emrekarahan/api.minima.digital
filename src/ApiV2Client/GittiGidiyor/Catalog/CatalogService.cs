﻿namespace ApiV2Client.GittiGidiyor.Catalog
{
	// Token: 0x020001E5 RID: 485
	public class CatalogService : ServiceClient<CatalogServiceService>, ICatalogService, IService
	{
		// Token: 0x06001081 RID: 4225 RVA: 0x00015F0C File Offset: 0x0001410C
		private CatalogService()
		{
			base.configureNetworkCredentials(this.service);
		}

		// Token: 0x06001082 RID: 4226 RVA: 0x00015F2B File Offset: 0x0001412B
		public catalogServiceResponse getCatalogDetail(int catalogId, string lang)
		{
			return this.service.getCatalogDetail(catalogId, lang);
		}

		// Token: 0x06001083 RID: 4227 RVA: 0x00015F3A File Offset: 0x0001413A
		public string getServiceName()
		{
			return this.service.getServiceName();
		}

		// Token: 0x06001084 RID: 4228 RVA: 0x00015F47 File Offset: 0x00014147
		public catalogServiceResponse searchCatalog(string categoryCode, string title, int startOffSet, int rowCount, string lang)
		{
			return this.service.searchCatalog(categoryCode, title, startOffSet, rowCount, lang);
		}

		// Token: 0x17000516 RID: 1302
		// (get) Token: 0x06001085 RID: 4229 RVA: 0x00015F5C File Offset: 0x0001415C
		public static CatalogService Instance
		{
			get
			{
				if (CatalogService.instance == null)
				{
					lock (CatalogService.lockObject)
					{
						if (CatalogService.instance == null)
						{
							CatalogService.instance = new CatalogService();
						}
					}
				}
				return CatalogService.instance;
			}
		}

		// Token: 0x04000613 RID: 1555
		private static CatalogService instance;

		// Token: 0x04000614 RID: 1556
		private static object lockObject = new object();

		// Token: 0x04000615 RID: 1557
		private CatalogServiceService service = new CatalogServiceService();
	}
}
