﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Catalog
{
	// Token: 0x0200007E RID: 126
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	public class getServiceNameCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x060004A1 RID: 1185 RVA: 0x00007077 File Offset: 0x00005277
		internal getServiceNameCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x1700019D RID: 413
		// (get) Token: 0x060004A2 RID: 1186 RVA: 0x0000708A File Offset: 0x0000528A
		public string Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (string)this.results[0];
			}
		}

		// Token: 0x040001C8 RID: 456
		private object[] results;
	}
}
