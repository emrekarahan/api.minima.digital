﻿using System;
using System.Net;
using System.Web.Services.Protocols;

namespace ApiV2Client.GittiGidiyor
{
	// Token: 0x02000002 RID: 2
	public class ServiceClient<P>
	{
		// Token: 0x06000001 RID: 1 RVA: 0x00002050 File Offset: 0x00000250
		public ServiceClient()
		{
			try
			{
				this.authConfig = ConfigurationManager.getAuthParameters();
			}
			catch (Exception ex)
			{
				Console.Error.WriteLine(ex.StackTrace);
			}
		}

		// Token: 0x06000002 RID: 2 RVA: 0x00002094 File Offset: 0x00000294
		protected void configureNetworkCredentials(SoapHttpClientProtocol client)
		{
			client.Credentials = new NetworkCredential(this.authConfig.RoleName, this.authConfig.RolePass);
		}

		// Token: 0x06000003 RID: 3 RVA: 0x000020B8 File Offset: 0x000002B8
		protected string getSignature(long time)
		{
			string result = null;
			try
			{
				result = Util.getMd5Hash(this.authConfig.ApiKey + this.authConfig.SecretKey + time);
			}
			catch (Exception ex)
			{
				Console.Error.WriteLine(ex.StackTrace);
			}
			return result;
		}

		// Token: 0x06000004 RID: 4 RVA: 0x00002114 File Offset: 0x00000314
		private void validateAuthConfig()
		{
			if (this.authConfig == null)
			{
				throw new Exception("Please enter authentication parameters");
			}
			if (this.authConfig.ApiKey == null)
			{
				throw new Exception("Please enter apiKey as a authentication parameter");
			}
			if (this.authConfig.SecretKey == null)
			{
				throw new Exception("Please enter secretKey as a authentication parameter");
			}
			if (this.authConfig.RoleName == null)
			{
				throw new Exception("Please enter roleName as a authentication parameter");
			}
			if (this.authConfig.RolePass == null)
			{
				throw new Exception("Please enter rolePass as a authentication parameter");
			}
		}

		// Token: 0x04000001 RID: 1
		protected AuthConfig authConfig;
	}
}
