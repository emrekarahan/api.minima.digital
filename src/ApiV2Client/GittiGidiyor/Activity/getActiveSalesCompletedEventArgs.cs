﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Activity
{
	// Token: 0x02000023 RID: 35
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	public class getActiveSalesCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000222 RID: 546 RVA: 0x00004148 File Offset: 0x00002348
		internal getActiveSalesCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x170000EA RID: 234
		// (get) Token: 0x06000223 RID: 547 RVA: 0x0000415B File Offset: 0x0000235B
		public activitySaleResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (activitySaleResponse)this.results[0];
			}
		}

		// Token: 0x040000F0 RID: 240
		private object[] results;
	}
}
