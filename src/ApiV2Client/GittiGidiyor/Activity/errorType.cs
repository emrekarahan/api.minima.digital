﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Activity
{
	// Token: 0x02000015 RID: 21
	[DesignerCategory("code")]
	[XmlType(Namespace = "http://activity.individual.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[DebuggerStepThrough]
	[Serializable]
	public class errorType
	{
		// Token: 0x17000056 RID: 86
		// (get) Token: 0x060000E9 RID: 233 RVA: 0x0000370C File Offset: 0x0000190C
		// (set) Token: 0x060000EA RID: 234 RVA: 0x00003714 File Offset: 0x00001914
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string errorId
		{
			get
			{
				return this.errorIdField;
			}
			set
			{
				this.errorIdField = value;
			}
		}

		// Token: 0x17000057 RID: 87
		// (get) Token: 0x060000EB RID: 235 RVA: 0x0000371D File Offset: 0x0000191D
		// (set) Token: 0x060000EC RID: 236 RVA: 0x00003725 File Offset: 0x00001925
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string errorCode
		{
			get
			{
				return this.errorCodeField;
			}
			set
			{
				this.errorCodeField = value;
			}
		}

		// Token: 0x17000058 RID: 88
		// (get) Token: 0x060000ED RID: 237 RVA: 0x0000372E File Offset: 0x0000192E
		// (set) Token: 0x060000EE RID: 238 RVA: 0x00003736 File Offset: 0x00001936
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string message
		{
			get
			{
				return this.messageField;
			}
			set
			{
				this.messageField = value;
			}
		}

		// Token: 0x17000059 RID: 89
		// (get) Token: 0x060000EF RID: 239 RVA: 0x0000373F File Offset: 0x0000193F
		// (set) Token: 0x060000F0 RID: 240 RVA: 0x00003747 File Offset: 0x00001947
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string viewMessage
		{
			get
			{
				return this.viewMessageField;
			}
			set
			{
				this.viewMessageField = value;
			}
		}

		// Token: 0x0400005C RID: 92
		private string errorIdField;

		// Token: 0x0400005D RID: 93
		private string errorCodeField;

		// Token: 0x0400005E RID: 94
		private string messageField;

		// Token: 0x0400005F RID: 95
		private string viewMessageField;
	}
}
