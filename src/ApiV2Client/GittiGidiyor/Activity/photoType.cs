﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Activity
{
	// Token: 0x02000014 RID: 20
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[XmlType(Namespace = "http://activity.individual.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[Serializable]
	public class photoType
	{
		// Token: 0x17000052 RID: 82
		// (get) Token: 0x060000E0 RID: 224 RVA: 0x000036C0 File Offset: 0x000018C0
		// (set) Token: 0x060000E1 RID: 225 RVA: 0x000036C8 File Offset: 0x000018C8
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string url
		{
			get
			{
				return this.urlField;
			}
			set
			{
				this.urlField = value;
			}
		}

		// Token: 0x17000053 RID: 83
		// (get) Token: 0x060000E2 RID: 226 RVA: 0x000036D1 File Offset: 0x000018D1
		// (set) Token: 0x060000E3 RID: 227 RVA: 0x000036D9 File Offset: 0x000018D9
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string base64
		{
			get
			{
				return this.base64Field;
			}
			set
			{
				this.base64Field = value;
			}
		}

		// Token: 0x17000054 RID: 84
		// (get) Token: 0x060000E4 RID: 228 RVA: 0x000036E2 File Offset: 0x000018E2
		// (set) Token: 0x060000E5 RID: 229 RVA: 0x000036EA File Offset: 0x000018EA
		[XmlAttribute]
		public int photoId
		{
			get
			{
				return this.photoIdField;
			}
			set
			{
				this.photoIdField = value;
			}
		}

		// Token: 0x17000055 RID: 85
		// (get) Token: 0x060000E6 RID: 230 RVA: 0x000036F3 File Offset: 0x000018F3
		// (set) Token: 0x060000E7 RID: 231 RVA: 0x000036FB File Offset: 0x000018FB
		[XmlIgnore]
		public bool photoIdSpecified
		{
			get
			{
				return this.photoIdFieldSpecified;
			}
			set
			{
				this.photoIdFieldSpecified = value;
			}
		}

		// Token: 0x04000058 RID: 88
		private string urlField;

		// Token: 0x04000059 RID: 89
		private string base64Field;

		// Token: 0x0400005A RID: 90
		private int photoIdField;

		// Token: 0x0400005B RID: 91
		private bool photoIdFieldSpecified;
	}
}
