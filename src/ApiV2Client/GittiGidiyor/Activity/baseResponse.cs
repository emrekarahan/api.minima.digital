﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Activity
{
	// Token: 0x0200000C RID: 12
	[DebuggerStepThrough]
	[XmlType(Namespace = "http://activity.individual.ws.listingapi.gg.com")]
	[DesignerCategory("code")]
	[XmlInclude(typeof(activityWatchResponse))]
	[XmlInclude(typeof(activityUnsoldResponse))]
	[XmlInclude(typeof(activityDidntWinResponse))]
	[XmlInclude(typeof(activityBidResponse))]
	[XmlInclude(typeof(activitySaleResponse))]
	[XmlInclude(typeof(activityWonResponse))]
	[XmlInclude(typeof(activitySoldResponse))]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[Serializable]
	public class baseResponse
	{
		// Token: 0x17000017 RID: 23
		// (get) Token: 0x06000062 RID: 98 RVA: 0x00003295 File Offset: 0x00001495
		// (set) Token: 0x06000063 RID: 99 RVA: 0x0000329D File Offset: 0x0000149D
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string ackCode
		{
			get
			{
				return this.ackCodeField;
			}
			set
			{
				this.ackCodeField = value;
			}
		}

		// Token: 0x17000018 RID: 24
		// (get) Token: 0x06000064 RID: 100 RVA: 0x000032A6 File Offset: 0x000014A6
		// (set) Token: 0x06000065 RID: 101 RVA: 0x000032AE File Offset: 0x000014AE
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string responseTime
		{
			get
			{
				return this.responseTimeField;
			}
			set
			{
				this.responseTimeField = value;
			}
		}

		// Token: 0x17000019 RID: 25
		// (get) Token: 0x06000066 RID: 102 RVA: 0x000032B7 File Offset: 0x000014B7
		// (set) Token: 0x06000067 RID: 103 RVA: 0x000032BF File Offset: 0x000014BF
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public errorType error
		{
			get
			{
				return this.errorField;
			}
			set
			{
				this.errorField = value;
			}
		}

		// Token: 0x1700001A RID: 26
		// (get) Token: 0x06000068 RID: 104 RVA: 0x000032C8 File Offset: 0x000014C8
		// (set) Token: 0x06000069 RID: 105 RVA: 0x000032D0 File Offset: 0x000014D0
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string timeElapsed
		{
			get
			{
				return this.timeElapsedField;
			}
			set
			{
				this.timeElapsedField = value;
			}
		}

		// Token: 0x0400001D RID: 29
		private string ackCodeField;

		// Token: 0x0400001E RID: 30
		private string responseTimeField;

		// Token: 0x0400001F RID: 31
		private errorType errorField;

		// Token: 0x04000020 RID: 32
		private string timeElapsedField;
	}
}
