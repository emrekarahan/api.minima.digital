﻿namespace ApiV2Client.GittiGidiyor.Activity
{
	// Token: 0x020001DF RID: 479
	public interface IActivityService : IService
	{
		// Token: 0x0600104F RID: 4175
		activitySaleResponse getActiveSales(int startOffSet, int rowCount, bool withData, string lang);

		// Token: 0x06001050 RID: 4176
		activityBidResponse getBidItems(int startOffSet, int rowCount, bool withData, string lang);

		// Token: 0x06001051 RID: 4177
		activityDidntWinResponse getDidntWinItems(int startOffSet, int rowCount, bool withData, int dayInterval, string lang);

		// Token: 0x06001052 RID: 4178
		activitySoldResponse getSoldItems(int startOffSet, int rowCount, bool withData, string lang);

		// Token: 0x06001053 RID: 4179
		activityUnsoldResponse getUnsoldItems(int startOffSet, int rowCount, bool withData, string lang);

		// Token: 0x06001054 RID: 4180
		activityWatchResponse getWatchItems(int startOffSet, int rowCount, bool withData, string lang);

		// Token: 0x06001055 RID: 4181
		activityWonResponse getWonItems(int startOffSet, int rowCount, bool withData, string lang);
	}
}
