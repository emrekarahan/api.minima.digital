﻿using System.CodeDom.Compiler;

namespace ApiV2Client.GittiGidiyor.Activity
{
	// Token: 0x02000022 RID: 34
	// (Invoke) Token: 0x0600021F RID: 543
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	public delegate void getActiveSalesCompletedEventHandler(object sender, getActiveSalesCompletedEventArgs e);
}
