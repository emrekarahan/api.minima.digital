﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Activity
{
	// Token: 0x02000020 RID: 32
	[XmlType(Namespace = "http://activity.individual.ws.listingapi.gg.com")]
	[DesignerCategory("code")]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[DebuggerStepThrough]
	[Serializable]
	public class activitySoldResponse : baseResponse
	{
		// Token: 0x170000C6 RID: 198
		// (get) Token: 0x060001D4 RID: 468 RVA: 0x00003ED4 File Offset: 0x000020D4
		// (set) Token: 0x060001D5 RID: 469 RVA: 0x00003EDC File Offset: 0x000020DC
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int count
		{
			get
			{
				return this.countField;
			}
			set
			{
				this.countField = value;
			}
		}

		// Token: 0x170000C7 RID: 199
		// (get) Token: 0x060001D6 RID: 470 RVA: 0x00003EE5 File Offset: 0x000020E5
		// (set) Token: 0x060001D7 RID: 471 RVA: 0x00003EED File Offset: 0x000020ED
		[XmlIgnore]
		public bool countSpecified
		{
			get
			{
				return this.countFieldSpecified;
			}
			set
			{
				this.countFieldSpecified = value;
			}
		}

		// Token: 0x170000C8 RID: 200
		// (get) Token: 0x060001D8 RID: 472 RVA: 0x00003EF6 File Offset: 0x000020F6
		// (set) Token: 0x060001D9 RID: 473 RVA: 0x00003EFE File Offset: 0x000020FE
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int itemCount
		{
			get
			{
				return this.itemCountField;
			}
			set
			{
				this.itemCountField = value;
			}
		}

		// Token: 0x170000C9 RID: 201
		// (get) Token: 0x060001DA RID: 474 RVA: 0x00003F07 File Offset: 0x00002107
		// (set) Token: 0x060001DB RID: 475 RVA: 0x00003F0F File Offset: 0x0000210F
		[XmlIgnore]
		public bool itemCountSpecified
		{
			get
			{
				return this.itemCountFieldSpecified;
			}
			set
			{
				this.itemCountFieldSpecified = value;
			}
		}

		// Token: 0x170000CA RID: 202
		// (get) Token: 0x060001DC RID: 476 RVA: 0x00003F18 File Offset: 0x00002118
		// (set) Token: 0x060001DD RID: 477 RVA: 0x00003F20 File Offset: 0x00002120
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		[XmlArrayItem("item", Form = XmlSchemaForm.Unqualified)]
		public activitySoldType[] itemList
		{
			get
			{
				return this.itemListField;
			}
			set
			{
				this.itemListField = value;
			}
		}

		// Token: 0x040000CC RID: 204
		private int countField;

		// Token: 0x040000CD RID: 205
		private bool countFieldSpecified;

		// Token: 0x040000CE RID: 206
		private int itemCountField;

		// Token: 0x040000CF RID: 207
		private bool itemCountFieldSpecified;

		// Token: 0x040000D0 RID: 208
		private activitySoldType[] itemListField;
	}
}
