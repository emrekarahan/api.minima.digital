﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Activity
{
	// Token: 0x02000010 RID: 16
	[XmlType(Namespace = "http://activity.individual.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[Serializable]
	public class bidderType
	{
		// Token: 0x17000035 RID: 53
		// (get) Token: 0x060000A2 RID: 162 RVA: 0x000034B3 File Offset: 0x000016B3
		// (set) Token: 0x060000A3 RID: 163 RVA: 0x000034BB File Offset: 0x000016BB
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string bidder
		{
			get
			{
				return this.bidderField;
			}
			set
			{
				this.bidderField = value;
			}
		}

		// Token: 0x17000036 RID: 54
		// (get) Token: 0x060000A4 RID: 164 RVA: 0x000034C4 File Offset: 0x000016C4
		// (set) Token: 0x060000A5 RID: 165 RVA: 0x000034CC File Offset: 0x000016CC
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string bidderMasked
		{
			get
			{
				return this.bidderMaskedField;
			}
			set
			{
				this.bidderMaskedField = value;
			}
		}

		// Token: 0x17000037 RID: 55
		// (get) Token: 0x060000A6 RID: 166 RVA: 0x000034D5 File Offset: 0x000016D5
		// (set) Token: 0x060000A7 RID: 167 RVA: 0x000034DD File Offset: 0x000016DD
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string ribbonType
		{
			get
			{
				return this.ribbonTypeField;
			}
			set
			{
				this.ribbonTypeField = value;
			}
		}

		// Token: 0x17000038 RID: 56
		// (get) Token: 0x060000A8 RID: 168 RVA: 0x000034E6 File Offset: 0x000016E6
		// (set) Token: 0x060000A9 RID: 169 RVA: 0x000034EE File Offset: 0x000016EE
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int processCount
		{
			get
			{
				return this.processCountField;
			}
			set
			{
				this.processCountField = value;
			}
		}

		// Token: 0x17000039 RID: 57
		// (get) Token: 0x060000AA RID: 170 RVA: 0x000034F7 File Offset: 0x000016F7
		// (set) Token: 0x060000AB RID: 171 RVA: 0x000034FF File Offset: 0x000016FF
		[XmlIgnore]
		public bool processCountSpecified
		{
			get
			{
				return this.processCountFieldSpecified;
			}
			set
			{
				this.processCountFieldSpecified = value;
			}
		}

		// Token: 0x0400003B RID: 59
		private string bidderField;

		// Token: 0x0400003C RID: 60
		private string bidderMaskedField;

		// Token: 0x0400003D RID: 61
		private string ribbonTypeField;

		// Token: 0x0400003E RID: 62
		private int processCountField;

		// Token: 0x0400003F RID: 63
		private bool processCountFieldSpecified;
	}
}
