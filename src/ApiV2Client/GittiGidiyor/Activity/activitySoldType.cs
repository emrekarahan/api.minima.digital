﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Activity
{
	// Token: 0x02000021 RID: 33
	[XmlType(Namespace = "http://activity.individual.ws.listingapi.gg.com")]
	[DesignerCategory("code")]
	[DebuggerStepThrough]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[Serializable]
	public class activitySoldType : activityBaseType
	{
		// Token: 0x170000CB RID: 203
		// (get) Token: 0x060001DF RID: 479 RVA: 0x00003F31 File Offset: 0x00002131
		// (set) Token: 0x060001E0 RID: 480 RVA: 0x00003F39 File Offset: 0x00002139
		[XmlArrayItem("photo", Form = XmlSchemaForm.Unqualified, IsNullable = false)]
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		public photoType[] photos
		{
			get
			{
				return this.photosField;
			}
			set
			{
				this.photosField = value;
			}
		}

		// Token: 0x170000CC RID: 204
		// (get) Token: 0x060001E1 RID: 481 RVA: 0x00003F42 File Offset: 0x00002142
		// (set) Token: 0x060001E2 RID: 482 RVA: 0x00003F4A File Offset: 0x0000214A
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string format
		{
			get
			{
				return this.formatField;
			}
			set
			{
				this.formatField = value;
			}
		}

		// Token: 0x170000CD RID: 205
		// (get) Token: 0x060001E3 RID: 483 RVA: 0x00003F53 File Offset: 0x00002153
		// (set) Token: 0x060001E4 RID: 484 RVA: 0x00003F5B File Offset: 0x0000215B
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public double price
		{
			get
			{
				return this.priceField;
			}
			set
			{
				this.priceField = value;
			}
		}

		// Token: 0x170000CE RID: 206
		// (get) Token: 0x060001E5 RID: 485 RVA: 0x00003F64 File Offset: 0x00002164
		// (set) Token: 0x060001E6 RID: 486 RVA: 0x00003F6C File Offset: 0x0000216C
		[XmlIgnore]
		public bool priceSpecified
		{
			get
			{
				return this.priceFieldSpecified;
			}
			set
			{
				this.priceFieldSpecified = value;
			}
		}

		// Token: 0x170000CF RID: 207
		// (get) Token: 0x060001E7 RID: 487 RVA: 0x00003F75 File Offset: 0x00002175
		// (set) Token: 0x060001E8 RID: 488 RVA: 0x00003F7D File Offset: 0x0000217D
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public double paidPrice
		{
			get
			{
				return this.paidPriceField;
			}
			set
			{
				this.paidPriceField = value;
			}
		}

		// Token: 0x170000D0 RID: 208
		// (get) Token: 0x060001E9 RID: 489 RVA: 0x00003F86 File Offset: 0x00002186
		// (set) Token: 0x060001EA RID: 490 RVA: 0x00003F8E File Offset: 0x0000218E
		[XmlIgnore]
		public bool paidPriceSpecified
		{
			get
			{
				return this.paidPriceFieldSpecified;
			}
			set
			{
				this.paidPriceFieldSpecified = value;
			}
		}

		// Token: 0x170000D1 RID: 209
		// (get) Token: 0x060001EB RID: 491 RVA: 0x00003F97 File Offset: 0x00002197
		// (set) Token: 0x060001EC RID: 492 RVA: 0x00003F9F File Offset: 0x0000219F
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public double discountedPrice
		{
			get
			{
				return this.discountedPriceField;
			}
			set
			{
				this.discountedPriceField = value;
			}
		}

		// Token: 0x170000D2 RID: 210
		// (get) Token: 0x060001ED RID: 493 RVA: 0x00003FA8 File Offset: 0x000021A8
		// (set) Token: 0x060001EE RID: 494 RVA: 0x00003FB0 File Offset: 0x000021B0
		[XmlIgnore]
		public bool discountedPriceSpecified
		{
			get
			{
				return this.discountedPriceFieldSpecified;
			}
			set
			{
				this.discountedPriceFieldSpecified = value;
			}
		}

		// Token: 0x170000D3 RID: 211
		// (get) Token: 0x060001EF RID: 495 RVA: 0x00003FB9 File Offset: 0x000021B9
		// (set) Token: 0x060001F0 RID: 496 RVA: 0x00003FC1 File Offset: 0x000021C1
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public double installmentPrice
		{
			get
			{
				return this.installmentPriceField;
			}
			set
			{
				this.installmentPriceField = value;
			}
		}

		// Token: 0x170000D4 RID: 212
		// (get) Token: 0x060001F1 RID: 497 RVA: 0x00003FCA File Offset: 0x000021CA
		// (set) Token: 0x060001F2 RID: 498 RVA: 0x00003FD2 File Offset: 0x000021D2
		[XmlIgnore]
		public bool installmentPriceSpecified
		{
			get
			{
				return this.installmentPriceFieldSpecified;
			}
			set
			{
				this.installmentPriceFieldSpecified = value;
			}
		}

		// Token: 0x170000D5 RID: 213
		// (get) Token: 0x060001F3 RID: 499 RVA: 0x00003FDB File Offset: 0x000021DB
		// (set) Token: 0x060001F4 RID: 500 RVA: 0x00003FE3 File Offset: 0x000021E3
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int productCount
		{
			get
			{
				return this.productCountField;
			}
			set
			{
				this.productCountField = value;
			}
		}

		// Token: 0x170000D6 RID: 214
		// (get) Token: 0x060001F5 RID: 501 RVA: 0x00003FEC File Offset: 0x000021EC
		// (set) Token: 0x060001F6 RID: 502 RVA: 0x00003FF4 File Offset: 0x000021F4
		[XmlIgnore]
		public bool productCountSpecified
		{
			get
			{
				return this.productCountFieldSpecified;
			}
			set
			{
				this.productCountFieldSpecified = value;
			}
		}

		// Token: 0x170000D7 RID: 215
		// (get) Token: 0x060001F7 RID: 503 RVA: 0x00003FFD File Offset: 0x000021FD
		// (set) Token: 0x060001F8 RID: 504 RVA: 0x00004005 File Offset: 0x00002205
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string endDate
		{
			get
			{
				return this.endDateField;
			}
			set
			{
				this.endDateField = value;
			}
		}

		// Token: 0x170000D8 RID: 216
		// (get) Token: 0x060001F9 RID: 505 RVA: 0x0000400E File Offset: 0x0000220E
		// (set) Token: 0x060001FA RID: 506 RVA: 0x00004016 File Offset: 0x00002216
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string lastProcessDate
		{
			get
			{
				return this.lastProcessDateField;
			}
			set
			{
				this.lastProcessDateField = value;
			}
		}

		// Token: 0x170000D9 RID: 217
		// (get) Token: 0x060001FB RID: 507 RVA: 0x0000401F File Offset: 0x0000221F
		// (set) Token: 0x060001FC RID: 508 RVA: 0x00004027 File Offset: 0x00002227
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int status
		{
			get
			{
				return this.statusField;
			}
			set
			{
				this.statusField = value;
			}
		}

		// Token: 0x170000DA RID: 218
		// (get) Token: 0x060001FD RID: 509 RVA: 0x00004030 File Offset: 0x00002230
		// (set) Token: 0x060001FE RID: 510 RVA: 0x00004038 File Offset: 0x00002238
		[XmlIgnore]
		public bool statusSpecified
		{
			get
			{
				return this.statusFieldSpecified;
			}
			set
			{
				this.statusFieldSpecified = value;
			}
		}

		// Token: 0x170000DB RID: 219
		// (get) Token: 0x060001FF RID: 511 RVA: 0x00004041 File Offset: 0x00002241
		// (set) Token: 0x06000200 RID: 512 RVA: 0x00004049 File Offset: 0x00002249
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string statusDescription
		{
			get
			{
				return this.statusDescriptionField;
			}
			set
			{
				this.statusDescriptionField = value;
			}
		}

		// Token: 0x170000DC RID: 220
		// (get) Token: 0x06000201 RID: 513 RVA: 0x00004052 File Offset: 0x00002252
		// (set) Token: 0x06000202 RID: 514 RVA: 0x0000405A File Offset: 0x0000225A
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public cargoInfoType cargoDetail
		{
			get
			{
				return this.cargoDetailField;
			}
			set
			{
				this.cargoDetailField = value;
			}
		}

		// Token: 0x170000DD RID: 221
		// (get) Token: 0x06000203 RID: 515 RVA: 0x00004063 File Offset: 0x00002263
		// (set) Token: 0x06000204 RID: 516 RVA: 0x0000406B File Offset: 0x0000226B
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string saleCode
		{
			get
			{
				return this.saleCodeField;
			}
			set
			{
				this.saleCodeField = value;
			}
		}

		// Token: 0x170000DE RID: 222
		// (get) Token: 0x06000205 RID: 517 RVA: 0x00004074 File Offset: 0x00002274
		// (set) Token: 0x06000206 RID: 518 RVA: 0x0000407C File Offset: 0x0000227C
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string note
		{
			get
			{
				return this.noteField;
			}
			set
			{
				this.noteField = value;
			}
		}

		// Token: 0x170000DF RID: 223
		// (get) Token: 0x06000207 RID: 519 RVA: 0x00004085 File Offset: 0x00002285
		// (set) Token: 0x06000208 RID: 520 RVA: 0x0000408D File Offset: 0x0000228D
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public orderBuyerInfoType buyerInfo
		{
			get
			{
				return this.buyerInfoField;
			}
			set
			{
				this.buyerInfoField = value;
			}
		}

		// Token: 0x170000E0 RID: 224
		// (get) Token: 0x06000209 RID: 521 RVA: 0x00004096 File Offset: 0x00002296
		// (set) Token: 0x0600020A RID: 522 RVA: 0x0000409E File Offset: 0x0000229E
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int buyerProcessCount
		{
			get
			{
				return this.buyerProcessCountField;
			}
			set
			{
				this.buyerProcessCountField = value;
			}
		}

		// Token: 0x170000E1 RID: 225
		// (get) Token: 0x0600020B RID: 523 RVA: 0x000040A7 File Offset: 0x000022A7
		// (set) Token: 0x0600020C RID: 524 RVA: 0x000040AF File Offset: 0x000022AF
		[XmlIgnore]
		public bool buyerProcessCountSpecified
		{
			get
			{
				return this.buyerProcessCountFieldSpecified;
			}
			set
			{
				this.buyerProcessCountFieldSpecified = value;
			}
		}

		// Token: 0x170000E2 RID: 226
		// (get) Token: 0x0600020D RID: 525 RVA: 0x000040B8 File Offset: 0x000022B8
		// (set) Token: 0x0600020E RID: 526 RVA: 0x000040C0 File Offset: 0x000022C0
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string thumbImageLink
		{
			get
			{
				return this.thumbImageLinkField;
			}
			set
			{
				this.thumbImageLinkField = value;
			}
		}

		// Token: 0x170000E3 RID: 227
		// (get) Token: 0x0600020F RID: 527 RVA: 0x000040C9 File Offset: 0x000022C9
		// (set) Token: 0x06000210 RID: 528 RVA: 0x000040D1 File Offset: 0x000022D1
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		[XmlArrayItem("saleTxCode", Form = XmlSchemaForm.Unqualified)]
		public int?[] saleTxCodeList
		{
			get
			{
				return this.saleTxCodeListField;
			}
			set
			{
				this.saleTxCodeListField = value;
			}
		}

		// Token: 0x170000E4 RID: 228
		// (get) Token: 0x06000211 RID: 529 RVA: 0x000040DA File Offset: 0x000022DA
		// (set) Token: 0x06000212 RID: 530 RVA: 0x000040E2 File Offset: 0x000022E2
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		[XmlArrayItem("variantSpec", Form = XmlSchemaForm.Unqualified)]
		public itemVariantSpecType[] variantSpecs
		{
			get
			{
				return this.variantSpecsField;
			}
			set
			{
				this.variantSpecsField = value;
			}
		}

		// Token: 0x170000E5 RID: 229
		// (get) Token: 0x06000213 RID: 531 RVA: 0x000040EB File Offset: 0x000022EB
		// (set) Token: 0x06000214 RID: 532 RVA: 0x000040F3 File Offset: 0x000022F3
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int saleId
		{
			get
			{
				return this.saleIdField;
			}
			set
			{
				this.saleIdField = value;
			}
		}

		// Token: 0x170000E6 RID: 230
		// (get) Token: 0x06000215 RID: 533 RVA: 0x000040FC File Offset: 0x000022FC
		// (set) Token: 0x06000216 RID: 534 RVA: 0x00004104 File Offset: 0x00002304
		[XmlIgnore]
		public bool saleIdSpecified
		{
			get
			{
				return this.saleIdFieldSpecified;
			}
			set
			{
				this.saleIdFieldSpecified = value;
			}
		}

		// Token: 0x170000E7 RID: 231
		// (get) Token: 0x06000217 RID: 535 RVA: 0x0000410D File Offset: 0x0000230D
		// (set) Token: 0x06000218 RID: 536 RVA: 0x00004115 File Offset: 0x00002315
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string shortStatusDescription
		{
			get
			{
				return this.shortStatusDescriptionField;
			}
			set
			{
				this.shortStatusDescriptionField = value;
			}
		}

		// Token: 0x170000E8 RID: 232
		// (get) Token: 0x06000219 RID: 537 RVA: 0x0000411E File Offset: 0x0000231E
		// (set) Token: 0x0600021A RID: 538 RVA: 0x00004126 File Offset: 0x00002326
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public bool hasInvoiceInfo
		{
			get
			{
				return this.hasInvoiceInfoField;
			}
			set
			{
				this.hasInvoiceInfoField = value;
			}
		}

		// Token: 0x170000E9 RID: 233
		// (get) Token: 0x0600021B RID: 539 RVA: 0x0000412F File Offset: 0x0000232F
		// (set) Token: 0x0600021C RID: 540 RVA: 0x00004137 File Offset: 0x00002337
		[XmlIgnore]
		public bool hasInvoiceInfoSpecified
		{
			get
			{
				return this.hasInvoiceInfoFieldSpecified;
			}
			set
			{
				this.hasInvoiceInfoFieldSpecified = value;
			}
		}

		// Token: 0x040000D1 RID: 209
		private photoType[] photosField;

		// Token: 0x040000D2 RID: 210
		private string formatField;

		// Token: 0x040000D3 RID: 211
		private double priceField;

		// Token: 0x040000D4 RID: 212
		private bool priceFieldSpecified;

		// Token: 0x040000D5 RID: 213
		private double paidPriceField;

		// Token: 0x040000D6 RID: 214
		private bool paidPriceFieldSpecified;

		// Token: 0x040000D7 RID: 215
		private double discountedPriceField;

		// Token: 0x040000D8 RID: 216
		private bool discountedPriceFieldSpecified;

		// Token: 0x040000D9 RID: 217
		private double installmentPriceField;

		// Token: 0x040000DA RID: 218
		private bool installmentPriceFieldSpecified;

		// Token: 0x040000DB RID: 219
		private int productCountField;

		// Token: 0x040000DC RID: 220
		private bool productCountFieldSpecified;

		// Token: 0x040000DD RID: 221
		private string endDateField;

		// Token: 0x040000DE RID: 222
		private string lastProcessDateField;

		// Token: 0x040000DF RID: 223
		private int statusField;

		// Token: 0x040000E0 RID: 224
		private bool statusFieldSpecified;

		// Token: 0x040000E1 RID: 225
		private string statusDescriptionField;

		// Token: 0x040000E2 RID: 226
		private cargoInfoType cargoDetailField;

		// Token: 0x040000E3 RID: 227
		private string saleCodeField;

		// Token: 0x040000E4 RID: 228
		private string noteField;

		// Token: 0x040000E5 RID: 229
		private orderBuyerInfoType buyerInfoField;

		// Token: 0x040000E6 RID: 230
		private int buyerProcessCountField;

		// Token: 0x040000E7 RID: 231
		private bool buyerProcessCountFieldSpecified;

		// Token: 0x040000E8 RID: 232
		private string thumbImageLinkField;

		// Token: 0x040000E9 RID: 233
		private int?[] saleTxCodeListField;

		// Token: 0x040000EA RID: 234
		private itemVariantSpecType[] variantSpecsField;

		// Token: 0x040000EB RID: 235
		private int saleIdField;

		// Token: 0x040000EC RID: 236
		private bool saleIdFieldSpecified;

		// Token: 0x040000ED RID: 237
		private string shortStatusDescriptionField;

		// Token: 0x040000EE RID: 238
		private bool hasInvoiceInfoField;

		// Token: 0x040000EF RID: 239
		private bool hasInvoiceInfoFieldSpecified;
	}
}
