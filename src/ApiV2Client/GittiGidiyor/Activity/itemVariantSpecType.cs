﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Activity
{
	// Token: 0x02000011 RID: 17
	[XmlType(Namespace = "http://activity.individual.ws.listingapi.gg.com")]
	[DesignerCategory("code")]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[DebuggerStepThrough]
	[Serializable]
	public class itemVariantSpecType
	{
		// Token: 0x1700003A RID: 58
		// (get) Token: 0x060000AD RID: 173 RVA: 0x00003510 File Offset: 0x00001710
		// (set) Token: 0x060000AE RID: 174 RVA: 0x00003518 File Offset: 0x00001718
		[XmlAttribute]
		public string name
		{
			get
			{
				return this.nameField;
			}
			set
			{
				this.nameField = value;
			}
		}

		// Token: 0x1700003B RID: 59
		// (get) Token: 0x060000AF RID: 175 RVA: 0x00003521 File Offset: 0x00001721
		// (set) Token: 0x060000B0 RID: 176 RVA: 0x00003529 File Offset: 0x00001729
		[XmlAttribute]
		public string value
		{
			get
			{
				return this.valueField;
			}
			set
			{
				this.valueField = value;
			}
		}

		// Token: 0x1700003C RID: 60
		// (get) Token: 0x060000B1 RID: 177 RVA: 0x00003532 File Offset: 0x00001732
		// (set) Token: 0x060000B2 RID: 178 RVA: 0x0000353A File Offset: 0x0000173A
		[XmlAttribute]
		public int quantity
		{
			get
			{
				return this.quantityField;
			}
			set
			{
				this.quantityField = value;
			}
		}

		// Token: 0x1700003D RID: 61
		// (get) Token: 0x060000B3 RID: 179 RVA: 0x00003543 File Offset: 0x00001743
		// (set) Token: 0x060000B4 RID: 180 RVA: 0x0000354B File Offset: 0x0000174B
		[XmlIgnore]
		public bool quantitySpecified
		{
			get
			{
				return this.quantityFieldSpecified;
			}
			set
			{
				this.quantityFieldSpecified = value;
			}
		}

		// Token: 0x1700003E RID: 62
		// (get) Token: 0x060000B5 RID: 181 RVA: 0x00003554 File Offset: 0x00001754
		// (set) Token: 0x060000B6 RID: 182 RVA: 0x0000355C File Offset: 0x0000175C
		[XmlAttribute]
		public int soldQuantity
		{
			get
			{
				return this.soldQuantityField;
			}
			set
			{
				this.soldQuantityField = value;
			}
		}

		// Token: 0x1700003F RID: 63
		// (get) Token: 0x060000B7 RID: 183 RVA: 0x00003565 File Offset: 0x00001765
		// (set) Token: 0x060000B8 RID: 184 RVA: 0x0000356D File Offset: 0x0000176D
		[XmlIgnore]
		public bool soldQuantitySpecified
		{
			get
			{
				return this.soldQuantityFieldSpecified;
			}
			set
			{
				this.soldQuantityFieldSpecified = value;
			}
		}

		// Token: 0x17000040 RID: 64
		// (get) Token: 0x060000B9 RID: 185 RVA: 0x00003576 File Offset: 0x00001776
		// (set) Token: 0x060000BA RID: 186 RVA: 0x0000357E File Offset: 0x0000177E
		[XmlAttribute]
		public string stockCode
		{
			get
			{
				return this.stockCodeField;
			}
			set
			{
				this.stockCodeField = value;
			}
		}

		// Token: 0x04000040 RID: 64
		private string nameField;

		// Token: 0x04000041 RID: 65
		private string valueField;

		// Token: 0x04000042 RID: 66
		private int quantityField;

		// Token: 0x04000043 RID: 67
		private bool quantityFieldSpecified;

		// Token: 0x04000044 RID: 68
		private int soldQuantityField;

		// Token: 0x04000045 RID: 69
		private bool soldQuantityFieldSpecified;

		// Token: 0x04000046 RID: 70
		private string stockCodeField;
	}
}
