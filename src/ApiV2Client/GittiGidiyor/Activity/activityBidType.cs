﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Activity
{
	// Token: 0x0200001D RID: 29
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[XmlType(Namespace = "http://activity.individual.ws.listingapi.gg.com")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[Serializable]
	public class activityBidType : activityBaseType
	{
		// Token: 0x17000098 RID: 152
		// (get) Token: 0x06000175 RID: 373 RVA: 0x00003BAE File Offset: 0x00001DAE
		// (set) Token: 0x06000176 RID: 374 RVA: 0x00003BB6 File Offset: 0x00001DB6
		[XmlArrayItem("photo", Form = XmlSchemaForm.Unqualified, IsNullable = false)]
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		public photoType[] photos
		{
			get
			{
				return this.photosField;
			}
			set
			{
				this.photosField = value;
			}
		}

		// Token: 0x17000099 RID: 153
		// (get) Token: 0x06000177 RID: 375 RVA: 0x00003BBF File Offset: 0x00001DBF
		// (set) Token: 0x06000178 RID: 376 RVA: 0x00003BC7 File Offset: 0x00001DC7
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public double proxy
		{
			get
			{
				return this.proxyField;
			}
			set
			{
				this.proxyField = value;
			}
		}

		// Token: 0x1700009A RID: 154
		// (get) Token: 0x06000179 RID: 377 RVA: 0x00003BD0 File Offset: 0x00001DD0
		// (set) Token: 0x0600017A RID: 378 RVA: 0x00003BD8 File Offset: 0x00001DD8
		[XmlIgnore]
		public bool proxySpecified
		{
			get
			{
				return this.proxyFieldSpecified;
			}
			set
			{
				this.proxyFieldSpecified = value;
			}
		}

		// Token: 0x1700009B RID: 155
		// (get) Token: 0x0600017B RID: 379 RVA: 0x00003BE1 File Offset: 0x00001DE1
		// (set) Token: 0x0600017C RID: 380 RVA: 0x00003BE9 File Offset: 0x00001DE9
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string seller
		{
			get
			{
				return this.sellerField;
			}
			set
			{
				this.sellerField = value;
			}
		}

		// Token: 0x1700009C RID: 156
		// (get) Token: 0x0600017D RID: 381 RVA: 0x00003BF2 File Offset: 0x00001DF2
		// (set) Token: 0x0600017E RID: 382 RVA: 0x00003BFA File Offset: 0x00001DFA
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string shippingWhere
		{
			get
			{
				return this.shippingWhereField;
			}
			set
			{
				this.shippingWhereField = value;
			}
		}

		// Token: 0x1700009D RID: 157
		// (get) Token: 0x0600017F RID: 383 RVA: 0x00003C03 File Offset: 0x00001E03
		// (set) Token: 0x06000180 RID: 384 RVA: 0x00003C0B File Offset: 0x00001E0B
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string sellerStoreName
		{
			get
			{
				return this.sellerStoreNameField;
			}
			set
			{
				this.sellerStoreNameField = value;
			}
		}

		// Token: 0x1700009E RID: 158
		// (get) Token: 0x06000181 RID: 385 RVA: 0x00003C14 File Offset: 0x00001E14
		// (set) Token: 0x06000182 RID: 386 RVA: 0x00003C1C File Offset: 0x00001E1C
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string category
		{
			get
			{
				return this.categoryField;
			}
			set
			{
				this.categoryField = value;
			}
		}

		// Token: 0x1700009F RID: 159
		// (get) Token: 0x06000183 RID: 387 RVA: 0x00003C25 File Offset: 0x00001E25
		// (set) Token: 0x06000184 RID: 388 RVA: 0x00003C2D File Offset: 0x00001E2D
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string remainingTime
		{
			get
			{
				return this.remainingTimeField;
			}
			set
			{
				this.remainingTimeField = value;
			}
		}

		// Token: 0x170000A0 RID: 160
		// (get) Token: 0x06000185 RID: 389 RVA: 0x00003C36 File Offset: 0x00001E36
		// (set) Token: 0x06000186 RID: 390 RVA: 0x00003C3E File Offset: 0x00001E3E
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public double bidPrice
		{
			get
			{
				return this.bidPriceField;
			}
			set
			{
				this.bidPriceField = value;
			}
		}

		// Token: 0x170000A1 RID: 161
		// (get) Token: 0x06000187 RID: 391 RVA: 0x00003C47 File Offset: 0x00001E47
		// (set) Token: 0x06000188 RID: 392 RVA: 0x00003C4F File Offset: 0x00001E4F
		[XmlIgnore]
		public bool bidPriceSpecified
		{
			get
			{
				return this.bidPriceFieldSpecified;
			}
			set
			{
				this.bidPriceFieldSpecified = value;
			}
		}

		// Token: 0x170000A2 RID: 162
		// (get) Token: 0x06000189 RID: 393 RVA: 0x00003C58 File Offset: 0x00001E58
		// (set) Token: 0x0600018A RID: 394 RVA: 0x00003C60 File Offset: 0x00001E60
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public double highPrice
		{
			get
			{
				return this.highPriceField;
			}
			set
			{
				this.highPriceField = value;
			}
		}

		// Token: 0x170000A3 RID: 163
		// (get) Token: 0x0600018B RID: 395 RVA: 0x00003C69 File Offset: 0x00001E69
		// (set) Token: 0x0600018C RID: 396 RVA: 0x00003C71 File Offset: 0x00001E71
		[XmlIgnore]
		public bool highPriceSpecified
		{
			get
			{
				return this.highPriceFieldSpecified;
			}
			set
			{
				this.highPriceFieldSpecified = value;
			}
		}

		// Token: 0x170000A4 RID: 164
		// (get) Token: 0x0600018D RID: 397 RVA: 0x00003C7A File Offset: 0x00001E7A
		// (set) Token: 0x0600018E RID: 398 RVA: 0x00003C82 File Offset: 0x00001E82
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int bidCount
		{
			get
			{
				return this.bidCountField;
			}
			set
			{
				this.bidCountField = value;
			}
		}

		// Token: 0x170000A5 RID: 165
		// (get) Token: 0x0600018F RID: 399 RVA: 0x00003C8B File Offset: 0x00001E8B
		// (set) Token: 0x06000190 RID: 400 RVA: 0x00003C93 File Offset: 0x00001E93
		[XmlIgnore]
		public bool bidCountSpecified
		{
			get
			{
				return this.bidCountFieldSpecified;
			}
			set
			{
				this.bidCountFieldSpecified = value;
			}
		}

		// Token: 0x170000A6 RID: 166
		// (get) Token: 0x06000191 RID: 401 RVA: 0x00003C9C File Offset: 0x00001E9C
		// (set) Token: 0x06000192 RID: 402 RVA: 0x00003CA4 File Offset: 0x00001EA4
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int productCount
		{
			get
			{
				return this.productCountField;
			}
			set
			{
				this.productCountField = value;
			}
		}

		// Token: 0x170000A7 RID: 167
		// (get) Token: 0x06000193 RID: 403 RVA: 0x00003CAD File Offset: 0x00001EAD
		// (set) Token: 0x06000194 RID: 404 RVA: 0x00003CB5 File Offset: 0x00001EB5
		[XmlIgnore]
		public bool productCountSpecified
		{
			get
			{
				return this.productCountFieldSpecified;
			}
			set
			{
				this.productCountFieldSpecified = value;
			}
		}

		// Token: 0x170000A8 RID: 168
		// (get) Token: 0x06000195 RID: 405 RVA: 0x00003CBE File Offset: 0x00001EBE
		// (set) Token: 0x06000196 RID: 406 RVA: 0x00003CC6 File Offset: 0x00001EC6
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int soldItemCount
		{
			get
			{
				return this.soldItemCountField;
			}
			set
			{
				this.soldItemCountField = value;
			}
		}

		// Token: 0x170000A9 RID: 169
		// (get) Token: 0x06000197 RID: 407 RVA: 0x00003CCF File Offset: 0x00001ECF
		// (set) Token: 0x06000198 RID: 408 RVA: 0x00003CD7 File Offset: 0x00001ED7
		[XmlIgnore]
		public bool soldItemCountSpecified
		{
			get
			{
				return this.soldItemCountFieldSpecified;
			}
			set
			{
				this.soldItemCountFieldSpecified = value;
			}
		}

		// Token: 0x170000AA RID: 170
		// (get) Token: 0x06000199 RID: 409 RVA: 0x00003CE0 File Offset: 0x00001EE0
		// (set) Token: 0x0600019A RID: 410 RVA: 0x00003CE8 File Offset: 0x00001EE8
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string thumbImageLink
		{
			get
			{
				return this.thumbImageLinkField;
			}
			set
			{
				this.thumbImageLinkField = value;
			}
		}

		// Token: 0x0400009E RID: 158
		private photoType[] photosField;

		// Token: 0x0400009F RID: 159
		private double proxyField;

		// Token: 0x040000A0 RID: 160
		private bool proxyFieldSpecified;

		// Token: 0x040000A1 RID: 161
		private string sellerField;

		// Token: 0x040000A2 RID: 162
		private string shippingWhereField;

		// Token: 0x040000A3 RID: 163
		private string sellerStoreNameField;

		// Token: 0x040000A4 RID: 164
		private string categoryField;

		// Token: 0x040000A5 RID: 165
		private string remainingTimeField;

		// Token: 0x040000A6 RID: 166
		private double bidPriceField;

		// Token: 0x040000A7 RID: 167
		private bool bidPriceFieldSpecified;

		// Token: 0x040000A8 RID: 168
		private double highPriceField;

		// Token: 0x040000A9 RID: 169
		private bool highPriceFieldSpecified;

		// Token: 0x040000AA RID: 170
		private int bidCountField;

		// Token: 0x040000AB RID: 171
		private bool bidCountFieldSpecified;

		// Token: 0x040000AC RID: 172
		private int productCountField;

		// Token: 0x040000AD RID: 173
		private bool productCountFieldSpecified;

		// Token: 0x040000AE RID: 174
		private int soldItemCountField;

		// Token: 0x040000AF RID: 175
		private bool soldItemCountFieldSpecified;

		// Token: 0x040000B0 RID: 176
		private string thumbImageLinkField;
	}
}
