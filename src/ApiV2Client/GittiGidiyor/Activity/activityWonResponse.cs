﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Activity
{
	// Token: 0x0200001E RID: 30
	[DebuggerStepThrough]
	[XmlType(Namespace = "http://activity.individual.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[DesignerCategory("code")]
	[Serializable]
	public class activityWonResponse : baseResponse
	{
		// Token: 0x170000AB RID: 171
		// (get) Token: 0x0600019C RID: 412 RVA: 0x00003CF9 File Offset: 0x00001EF9
		// (set) Token: 0x0600019D RID: 413 RVA: 0x00003D01 File Offset: 0x00001F01
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int count
		{
			get
			{
				return this.countField;
			}
			set
			{
				this.countField = value;
			}
		}

		// Token: 0x170000AC RID: 172
		// (get) Token: 0x0600019E RID: 414 RVA: 0x00003D0A File Offset: 0x00001F0A
		// (set) Token: 0x0600019F RID: 415 RVA: 0x00003D12 File Offset: 0x00001F12
		[XmlIgnore]
		public bool countSpecified
		{
			get
			{
				return this.countFieldSpecified;
			}
			set
			{
				this.countFieldSpecified = value;
			}
		}

		// Token: 0x170000AD RID: 173
		// (get) Token: 0x060001A0 RID: 416 RVA: 0x00003D1B File Offset: 0x00001F1B
		// (set) Token: 0x060001A1 RID: 417 RVA: 0x00003D23 File Offset: 0x00001F23
		[XmlArrayItem("item", Form = XmlSchemaForm.Unqualified)]
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		public activityWonType[] itemList
		{
			get
			{
				return this.itemListField;
			}
			set
			{
				this.itemListField = value;
			}
		}

		// Token: 0x040000B1 RID: 177
		private int countField;

		// Token: 0x040000B2 RID: 178
		private bool countFieldSpecified;

		// Token: 0x040000B3 RID: 179
		private activityWonType[] itemListField;
	}
}
