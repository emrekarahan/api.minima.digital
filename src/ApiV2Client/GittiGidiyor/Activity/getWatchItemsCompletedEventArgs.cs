﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Activity
{
	// Token: 0x0200002F RID: 47
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	public class getWatchItemsCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000246 RID: 582 RVA: 0x00004238 File Offset: 0x00002438
		internal getWatchItemsCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x170000F0 RID: 240
		// (get) Token: 0x06000247 RID: 583 RVA: 0x0000424B File Offset: 0x0000244B
		public activityWatchResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (activityWatchResponse)this.results[0];
			}
		}

		// Token: 0x040000F6 RID: 246
		private object[] results;
	}
}
