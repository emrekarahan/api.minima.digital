﻿using System.CodeDom.Compiler;

namespace ApiV2Client.GittiGidiyor.Activity
{
	// Token: 0x02000024 RID: 36
	// (Invoke) Token: 0x06000225 RID: 549
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	public delegate void getBidItemsCompletedEventHandler(object sender, getBidItemsCompletedEventArgs e);
}
