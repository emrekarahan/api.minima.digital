﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Activity
{
	// Token: 0x0200001C RID: 28
	[XmlType(Namespace = "http://activity.individual.ws.listingapi.gg.com")]
	[DesignerCategory("code")]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[DebuggerStepThrough]
	[Serializable]
	public class activityBidResponse : baseResponse
	{
		// Token: 0x17000095 RID: 149
		// (get) Token: 0x0600016E RID: 366 RVA: 0x00003B73 File Offset: 0x00001D73
		// (set) Token: 0x0600016F RID: 367 RVA: 0x00003B7B File Offset: 0x00001D7B
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int count
		{
			get
			{
				return this.countField;
			}
			set
			{
				this.countField = value;
			}
		}

		// Token: 0x17000096 RID: 150
		// (get) Token: 0x06000170 RID: 368 RVA: 0x00003B84 File Offset: 0x00001D84
		// (set) Token: 0x06000171 RID: 369 RVA: 0x00003B8C File Offset: 0x00001D8C
		[XmlIgnore]
		public bool countSpecified
		{
			get
			{
				return this.countFieldSpecified;
			}
			set
			{
				this.countFieldSpecified = value;
			}
		}

		// Token: 0x17000097 RID: 151
		// (get) Token: 0x06000172 RID: 370 RVA: 0x00003B95 File Offset: 0x00001D95
		// (set) Token: 0x06000173 RID: 371 RVA: 0x00003B9D File Offset: 0x00001D9D
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		[XmlArrayItem("item", Form = XmlSchemaForm.Unqualified)]
		public activityBidType[] itemList
		{
			get
			{
				return this.itemListField;
			}
			set
			{
				this.itemListField = value;
			}
		}

		// Token: 0x0400009B RID: 155
		private int countField;

		// Token: 0x0400009C RID: 156
		private bool countFieldSpecified;

		// Token: 0x0400009D RID: 157
		private activityBidType[] itemListField;
	}
}
