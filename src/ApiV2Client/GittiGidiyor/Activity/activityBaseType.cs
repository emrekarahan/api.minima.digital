﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Activity
{
	// Token: 0x0200000E RID: 14
	[XmlInclude(typeof(activityWatchType))]
	[XmlInclude(typeof(activitySaleType))]
	[XmlInclude(typeof(activityWonType))]
	[XmlType(Namespace = "http://activity.individual.ws.listingapi.gg.com")]
	[XmlInclude(typeof(activityUnsoldType))]
	[XmlInclude(typeof(activityDidntWinType))]
	[XmlInclude(typeof(activityBidType))]
	[XmlInclude(typeof(activitySoldType))]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[Serializable]
	public abstract class activityBaseType
	{
		// Token: 0x1700001E RID: 30
		// (get) Token: 0x06000072 RID: 114 RVA: 0x0000331C File Offset: 0x0000151C
		// (set) Token: 0x06000073 RID: 115 RVA: 0x00003324 File Offset: 0x00001524
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string itemId
		{
			get
			{
				return this.itemIdField;
			}
			set
			{
				this.itemIdField = value;
			}
		}

		// Token: 0x1700001F RID: 31
		// (get) Token: 0x06000074 RID: 116 RVA: 0x0000332D File Offset: 0x0000152D
		// (set) Token: 0x06000075 RID: 117 RVA: 0x00003335 File Offset: 0x00001535
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int productId
		{
			get
			{
				return this.productIdField;
			}
			set
			{
				this.productIdField = value;
			}
		}

		// Token: 0x17000020 RID: 32
		// (get) Token: 0x06000076 RID: 118 RVA: 0x0000333E File Offset: 0x0000153E
		// (set) Token: 0x06000077 RID: 119 RVA: 0x00003346 File Offset: 0x00001546
		[XmlIgnore]
		public bool productIdSpecified
		{
			get
			{
				return this.productIdFieldSpecified;
			}
			set
			{
				this.productIdFieldSpecified = value;
			}
		}

		// Token: 0x17000021 RID: 33
		// (get) Token: 0x06000078 RID: 120 RVA: 0x0000334F File Offset: 0x0000154F
		// (set) Token: 0x06000079 RID: 121 RVA: 0x00003357 File Offset: 0x00001557
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string title
		{
			get
			{
				return this.titleField;
			}
			set
			{
				this.titleField = value;
			}
		}

		// Token: 0x17000022 RID: 34
		// (get) Token: 0x0600007A RID: 122 RVA: 0x00003360 File Offset: 0x00001560
		// (set) Token: 0x0600007B RID: 123 RVA: 0x00003368 File Offset: 0x00001568
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string productLink
		{
			get
			{
				return this.productLinkField;
			}
			set
			{
				this.productLinkField = value;
			}
		}

		// Token: 0x17000023 RID: 35
		// (get) Token: 0x0600007C RID: 124 RVA: 0x00003371 File Offset: 0x00001571
		// (set) Token: 0x0600007D RID: 125 RVA: 0x00003379 File Offset: 0x00001579
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public long variantId
		{
			get
			{
				return this.variantIdField;
			}
			set
			{
				this.variantIdField = value;
			}
		}

		// Token: 0x17000024 RID: 36
		// (get) Token: 0x0600007E RID: 126 RVA: 0x00003382 File Offset: 0x00001582
		// (set) Token: 0x0600007F RID: 127 RVA: 0x0000338A File Offset: 0x0000158A
		[XmlIgnore]
		public bool variantIdSpecified
		{
			get
			{
				return this.variantIdFieldSpecified;
			}
			set
			{
				this.variantIdFieldSpecified = value;
			}
		}

		// Token: 0x04000024 RID: 36
		private string itemIdField;

		// Token: 0x04000025 RID: 37
		private int productIdField;

		// Token: 0x04000026 RID: 38
		private bool productIdFieldSpecified;

		// Token: 0x04000027 RID: 39
		private string titleField;

		// Token: 0x04000028 RID: 40
		private string productLinkField;

		// Token: 0x04000029 RID: 41
		private long variantIdField;

		// Token: 0x0400002A RID: 42
		private bool variantIdFieldSpecified;
	}
}
