﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Activity
{
	// Token: 0x0200000D RID: 13
	[XmlType(Namespace = "http://activity.individual.ws.listingapi.gg.com")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[Serializable]
	public class activitySaleResponse : baseResponse
	{
		// Token: 0x1700001B RID: 27
		// (get) Token: 0x0600006B RID: 107 RVA: 0x000032E1 File Offset: 0x000014E1
		// (set) Token: 0x0600006C RID: 108 RVA: 0x000032E9 File Offset: 0x000014E9
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int count
		{
			get
			{
				return this.countField;
			}
			set
			{
				this.countField = value;
			}
		}

		// Token: 0x1700001C RID: 28
		// (get) Token: 0x0600006D RID: 109 RVA: 0x000032F2 File Offset: 0x000014F2
		// (set) Token: 0x0600006E RID: 110 RVA: 0x000032FA File Offset: 0x000014FA
		[XmlIgnore]
		public bool countSpecified
		{
			get
			{
				return this.countFieldSpecified;
			}
			set
			{
				this.countFieldSpecified = value;
			}
		}

		// Token: 0x1700001D RID: 29
		// (get) Token: 0x0600006F RID: 111 RVA: 0x00003303 File Offset: 0x00001503
		// (set) Token: 0x06000070 RID: 112 RVA: 0x0000330B File Offset: 0x0000150B
		[XmlArrayItem("item", Form = XmlSchemaForm.Unqualified)]
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		public activitySaleType[] itemList
		{
			get
			{
				return this.itemListField;
			}
			set
			{
				this.itemListField = value;
			}
		}

		// Token: 0x04000021 RID: 33
		private int countField;

		// Token: 0x04000022 RID: 34
		private bool countFieldSpecified;

		// Token: 0x04000023 RID: 35
		private activitySaleType[] itemListField;
	}
}
