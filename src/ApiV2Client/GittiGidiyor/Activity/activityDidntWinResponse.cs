﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Activity
{
	// Token: 0x0200001A RID: 26
	[DesignerCategory("code")]
	[XmlType(Namespace = "http://activity.individual.ws.listingapi.gg.com")]
	[DebuggerStepThrough]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[Serializable]
	public class activityDidntWinResponse : baseResponse
	{
		// Token: 0x17000085 RID: 133
		// (get) Token: 0x0600014C RID: 332 RVA: 0x00003A53 File Offset: 0x00001C53
		// (set) Token: 0x0600014D RID: 333 RVA: 0x00003A5B File Offset: 0x00001C5B
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int count
		{
			get
			{
				return this.countField;
			}
			set
			{
				this.countField = value;
			}
		}

		// Token: 0x17000086 RID: 134
		// (get) Token: 0x0600014E RID: 334 RVA: 0x00003A64 File Offset: 0x00001C64
		// (set) Token: 0x0600014F RID: 335 RVA: 0x00003A6C File Offset: 0x00001C6C
		[XmlIgnore]
		public bool countSpecified
		{
			get
			{
				return this.countFieldSpecified;
			}
			set
			{
				this.countFieldSpecified = value;
			}
		}

		// Token: 0x17000087 RID: 135
		// (get) Token: 0x06000150 RID: 336 RVA: 0x00003A75 File Offset: 0x00001C75
		// (set) Token: 0x06000151 RID: 337 RVA: 0x00003A7D File Offset: 0x00001C7D
		[XmlArrayItem("item", Form = XmlSchemaForm.Unqualified)]
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		public activityDidntWinType[] itemList
		{
			get
			{
				return this.itemListField;
			}
			set
			{
				this.itemListField = value;
			}
		}

		// Token: 0x0400008B RID: 139
		private int countField;

		// Token: 0x0400008C RID: 140
		private bool countFieldSpecified;

		// Token: 0x0400008D RID: 141
		private activityDidntWinType[] itemListField;
	}
}
