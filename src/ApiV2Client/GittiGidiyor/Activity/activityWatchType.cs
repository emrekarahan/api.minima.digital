﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Activity
{
	// Token: 0x02000017 RID: 23
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[XmlType(Namespace = "http://activity.individual.ws.listingapi.gg.com")]
	[Serializable]
	public class activityWatchType : activityBaseType
	{
		// Token: 0x1700005D RID: 93
		// (get) Token: 0x060000F9 RID: 249 RVA: 0x00003793 File Offset: 0x00001993
		// (set) Token: 0x060000FA RID: 250 RVA: 0x0000379B File Offset: 0x0000199B
		[XmlArrayItem("photo", Form = XmlSchemaForm.Unqualified, IsNullable = false)]
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		public photoType[] photos
		{
			get
			{
				return this.photosField;
			}
			set
			{
				this.photosField = value;
			}
		}

		// Token: 0x1700005E RID: 94
		// (get) Token: 0x060000FB RID: 251 RVA: 0x000037A4 File Offset: 0x000019A4
		// (set) Token: 0x060000FC RID: 252 RVA: 0x000037AC File Offset: 0x000019AC
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public bool inBasket
		{
			get
			{
				return this.inBasketField;
			}
			set
			{
				this.inBasketField = value;
			}
		}

		// Token: 0x1700005F RID: 95
		// (get) Token: 0x060000FD RID: 253 RVA: 0x000037B5 File Offset: 0x000019B5
		// (set) Token: 0x060000FE RID: 254 RVA: 0x000037BD File Offset: 0x000019BD
		[XmlIgnore]
		public bool inBasketSpecified
		{
			get
			{
				return this.inBasketFieldSpecified;
			}
			set
			{
				this.inBasketFieldSpecified = value;
			}
		}

		// Token: 0x17000060 RID: 96
		// (get) Token: 0x060000FF RID: 255 RVA: 0x000037C6 File Offset: 0x000019C6
		// (set) Token: 0x06000100 RID: 256 RVA: 0x000037CE File Offset: 0x000019CE
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string seller
		{
			get
			{
				return this.sellerField;
			}
			set
			{
				this.sellerField = value;
			}
		}

		// Token: 0x17000061 RID: 97
		// (get) Token: 0x06000101 RID: 257 RVA: 0x000037D7 File Offset: 0x000019D7
		// (set) Token: 0x06000102 RID: 258 RVA: 0x000037DF File Offset: 0x000019DF
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string shippingWhere
		{
			get
			{
				return this.shippingWhereField;
			}
			set
			{
				this.shippingWhereField = value;
			}
		}

		// Token: 0x17000062 RID: 98
		// (get) Token: 0x06000103 RID: 259 RVA: 0x000037E8 File Offset: 0x000019E8
		// (set) Token: 0x06000104 RID: 260 RVA: 0x000037F0 File Offset: 0x000019F0
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string sellerStoreName
		{
			get
			{
				return this.sellerStoreNameField;
			}
			set
			{
				this.sellerStoreNameField = value;
			}
		}

		// Token: 0x17000063 RID: 99
		// (get) Token: 0x06000105 RID: 261 RVA: 0x000037F9 File Offset: 0x000019F9
		// (set) Token: 0x06000106 RID: 262 RVA: 0x00003801 File Offset: 0x00001A01
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string category
		{
			get
			{
				return this.categoryField;
			}
			set
			{
				this.categoryField = value;
			}
		}

		// Token: 0x17000064 RID: 100
		// (get) Token: 0x06000107 RID: 263 RVA: 0x0000380A File Offset: 0x00001A0A
		// (set) Token: 0x06000108 RID: 264 RVA: 0x00003812 File Offset: 0x00001A12
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string format
		{
			get
			{
				return this.formatField;
			}
			set
			{
				this.formatField = value;
			}
		}

		// Token: 0x17000065 RID: 101
		// (get) Token: 0x06000109 RID: 265 RVA: 0x0000381B File Offset: 0x00001A1B
		// (set) Token: 0x0600010A RID: 266 RVA: 0x00003823 File Offset: 0x00001A23
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public double buyNowPrice
		{
			get
			{
				return this.buyNowPriceField;
			}
			set
			{
				this.buyNowPriceField = value;
			}
		}

		// Token: 0x17000066 RID: 102
		// (get) Token: 0x0600010B RID: 267 RVA: 0x0000382C File Offset: 0x00001A2C
		// (set) Token: 0x0600010C RID: 268 RVA: 0x00003834 File Offset: 0x00001A34
		[XmlIgnore]
		public bool buyNowPriceSpecified
		{
			get
			{
				return this.buyNowPriceFieldSpecified;
			}
			set
			{
				this.buyNowPriceFieldSpecified = value;
			}
		}

		// Token: 0x17000067 RID: 103
		// (get) Token: 0x0600010D RID: 269 RVA: 0x0000383D File Offset: 0x00001A3D
		// (set) Token: 0x0600010E RID: 270 RVA: 0x00003845 File Offset: 0x00001A45
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public double highPrice
		{
			get
			{
				return this.highPriceField;
			}
			set
			{
				this.highPriceField = value;
			}
		}

		// Token: 0x17000068 RID: 104
		// (get) Token: 0x0600010F RID: 271 RVA: 0x0000384E File Offset: 0x00001A4E
		// (set) Token: 0x06000110 RID: 272 RVA: 0x00003856 File Offset: 0x00001A56
		[XmlIgnore]
		public bool highPriceSpecified
		{
			get
			{
				return this.highPriceFieldSpecified;
			}
			set
			{
				this.highPriceFieldSpecified = value;
			}
		}

		// Token: 0x17000069 RID: 105
		// (get) Token: 0x06000111 RID: 273 RVA: 0x0000385F File Offset: 0x00001A5F
		// (set) Token: 0x06000112 RID: 274 RVA: 0x00003867 File Offset: 0x00001A67
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string remainingTime
		{
			get
			{
				return this.remainingTimeField;
			}
			set
			{
				this.remainingTimeField = value;
			}
		}

		// Token: 0x1700006A RID: 106
		// (get) Token: 0x06000113 RID: 275 RVA: 0x00003870 File Offset: 0x00001A70
		// (set) Token: 0x06000114 RID: 276 RVA: 0x00003878 File Offset: 0x00001A78
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int bidCount
		{
			get
			{
				return this.bidCountField;
			}
			set
			{
				this.bidCountField = value;
			}
		}

		// Token: 0x1700006B RID: 107
		// (get) Token: 0x06000115 RID: 277 RVA: 0x00003881 File Offset: 0x00001A81
		// (set) Token: 0x06000116 RID: 278 RVA: 0x00003889 File Offset: 0x00001A89
		[XmlIgnore]
		public bool bidCountSpecified
		{
			get
			{
				return this.bidCountFieldSpecified;
			}
			set
			{
				this.bidCountFieldSpecified = value;
			}
		}

		// Token: 0x1700006C RID: 108
		// (get) Token: 0x06000117 RID: 279 RVA: 0x00003892 File Offset: 0x00001A92
		// (set) Token: 0x06000118 RID: 280 RVA: 0x0000389A File Offset: 0x00001A9A
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int productCount
		{
			get
			{
				return this.productCountField;
			}
			set
			{
				this.productCountField = value;
			}
		}

		// Token: 0x1700006D RID: 109
		// (get) Token: 0x06000119 RID: 281 RVA: 0x000038A3 File Offset: 0x00001AA3
		// (set) Token: 0x0600011A RID: 282 RVA: 0x000038AB File Offset: 0x00001AAB
		[XmlIgnore]
		public bool productCountSpecified
		{
			get
			{
				return this.productCountFieldSpecified;
			}
			set
			{
				this.productCountFieldSpecified = value;
			}
		}

		// Token: 0x1700006E RID: 110
		// (get) Token: 0x0600011B RID: 283 RVA: 0x000038B4 File Offset: 0x00001AB4
		// (set) Token: 0x0600011C RID: 284 RVA: 0x000038BC File Offset: 0x00001ABC
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int soldItemCount
		{
			get
			{
				return this.soldItemCountField;
			}
			set
			{
				this.soldItemCountField = value;
			}
		}

		// Token: 0x1700006F RID: 111
		// (get) Token: 0x0600011D RID: 285 RVA: 0x000038C5 File Offset: 0x00001AC5
		// (set) Token: 0x0600011E RID: 286 RVA: 0x000038CD File Offset: 0x00001ACD
		[XmlIgnore]
		public bool soldItemCountSpecified
		{
			get
			{
				return this.soldItemCountFieldSpecified;
			}
			set
			{
				this.soldItemCountFieldSpecified = value;
			}
		}

		// Token: 0x17000070 RID: 112
		// (get) Token: 0x0600011F RID: 287 RVA: 0x000038D6 File Offset: 0x00001AD6
		// (set) Token: 0x06000120 RID: 288 RVA: 0x000038DE File Offset: 0x00001ADE
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string thumbImageLink
		{
			get
			{
				return this.thumbImageLinkField;
			}
			set
			{
				this.thumbImageLinkField = value;
			}
		}

		// Token: 0x17000071 RID: 113
		// (get) Token: 0x06000121 RID: 289 RVA: 0x000038E7 File Offset: 0x00001AE7
		// (set) Token: 0x06000122 RID: 290 RVA: 0x000038EF File Offset: 0x00001AEF
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public bool isFinished
		{
			get
			{
				return this.isFinishedField;
			}
			set
			{
				this.isFinishedField = value;
			}
		}

		// Token: 0x17000072 RID: 114
		// (get) Token: 0x06000123 RID: 291 RVA: 0x000038F8 File Offset: 0x00001AF8
		// (set) Token: 0x06000124 RID: 292 RVA: 0x00003900 File Offset: 0x00001B00
		[XmlIgnore]
		public bool isFinishedSpecified
		{
			get
			{
				return this.isFinishedFieldSpecified;
			}
			set
			{
				this.isFinishedFieldSpecified = value;
			}
		}

		// Token: 0x17000073 RID: 115
		// (get) Token: 0x06000125 RID: 293 RVA: 0x00003909 File Offset: 0x00001B09
		// (set) Token: 0x06000126 RID: 294 RVA: 0x00003911 File Offset: 0x00001B11
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int positiveRatePercentage
		{
			get
			{
				return this.positiveRatePercentageField;
			}
			set
			{
				this.positiveRatePercentageField = value;
			}
		}

		// Token: 0x17000074 RID: 116
		// (get) Token: 0x06000127 RID: 295 RVA: 0x0000391A File Offset: 0x00001B1A
		// (set) Token: 0x06000128 RID: 296 RVA: 0x00003922 File Offset: 0x00001B22
		[XmlIgnore]
		public bool positiveRatePercentageSpecified
		{
			get
			{
				return this.positiveRatePercentageFieldSpecified;
			}
			set
			{
				this.positiveRatePercentageFieldSpecified = value;
			}
		}

		// Token: 0x17000075 RID: 117
		// (get) Token: 0x06000129 RID: 297 RVA: 0x0000392B File Offset: 0x00001B2B
		// (set) Token: 0x0600012A RID: 298 RVA: 0x00003933 File Offset: 0x00001B33
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public bidderType highestBidder
		{
			get
			{
				return this.highestBidderField;
			}
			set
			{
				this.highestBidderField = value;
			}
		}

		// Token: 0x17000076 RID: 118
		// (get) Token: 0x0600012B RID: 299 RVA: 0x0000393C File Offset: 0x00001B3C
		// (set) Token: 0x0600012C RID: 300 RVA: 0x00003944 File Offset: 0x00001B44
		[XmlArrayItem("variantSpec", Form = XmlSchemaForm.Unqualified)]
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		public itemVariantSpecType[] variantSpecs
		{
			get
			{
				return this.variantSpecsField;
			}
			set
			{
				this.variantSpecsField = value;
			}
		}

		// Token: 0x17000077 RID: 119
		// (get) Token: 0x0600012D RID: 301 RVA: 0x0000394D File Offset: 0x00001B4D
		// (set) Token: 0x0600012E RID: 302 RVA: 0x00003955 File Offset: 0x00001B55
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string startDate
		{
			get
			{
				return this.startDateField;
			}
			set
			{
				this.startDateField = value;
			}
		}

		// Token: 0x17000078 RID: 120
		// (get) Token: 0x0600012F RID: 303 RVA: 0x0000395E File Offset: 0x00001B5E
		// (set) Token: 0x06000130 RID: 304 RVA: 0x00003966 File Offset: 0x00001B66
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string featureType
		{
			get
			{
				return this.featureTypeField;
			}
			set
			{
				this.featureTypeField = value;
			}
		}

		// Token: 0x04000063 RID: 99
		private photoType[] photosField;

		// Token: 0x04000064 RID: 100
		private bool inBasketField;

		// Token: 0x04000065 RID: 101
		private bool inBasketFieldSpecified;

		// Token: 0x04000066 RID: 102
		private string sellerField;

		// Token: 0x04000067 RID: 103
		private string shippingWhereField;

		// Token: 0x04000068 RID: 104
		private string sellerStoreNameField;

		// Token: 0x04000069 RID: 105
		private string categoryField;

		// Token: 0x0400006A RID: 106
		private string formatField;

		// Token: 0x0400006B RID: 107
		private double buyNowPriceField;

		// Token: 0x0400006C RID: 108
		private bool buyNowPriceFieldSpecified;

		// Token: 0x0400006D RID: 109
		private double highPriceField;

		// Token: 0x0400006E RID: 110
		private bool highPriceFieldSpecified;

		// Token: 0x0400006F RID: 111
		private string remainingTimeField;

		// Token: 0x04000070 RID: 112
		private int bidCountField;

		// Token: 0x04000071 RID: 113
		private bool bidCountFieldSpecified;

		// Token: 0x04000072 RID: 114
		private int productCountField;

		// Token: 0x04000073 RID: 115
		private bool productCountFieldSpecified;

		// Token: 0x04000074 RID: 116
		private int soldItemCountField;

		// Token: 0x04000075 RID: 117
		private bool soldItemCountFieldSpecified;

		// Token: 0x04000076 RID: 118
		private string thumbImageLinkField;

		// Token: 0x04000077 RID: 119
		private bool isFinishedField;

		// Token: 0x04000078 RID: 120
		private bool isFinishedFieldSpecified;

		// Token: 0x04000079 RID: 121
		private int positiveRatePercentageField;

		// Token: 0x0400007A RID: 122
		private bool positiveRatePercentageFieldSpecified;

		// Token: 0x0400007B RID: 123
		private bidderType highestBidderField;

		// Token: 0x0400007C RID: 124
		private itemVariantSpecType[] variantSpecsField;

		// Token: 0x0400007D RID: 125
		private string startDateField;

		// Token: 0x0400007E RID: 126
		private string featureTypeField;
	}
}
