﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Activity
{
	// Token: 0x02000013 RID: 19
	[XmlType(Namespace = "http://activity.individual.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[Serializable]
	public class cargoInfoType
	{
		// Token: 0x1700004F RID: 79
		// (get) Token: 0x060000D9 RID: 217 RVA: 0x00003685 File Offset: 0x00001885
		// (set) Token: 0x060000DA RID: 218 RVA: 0x0000368D File Offset: 0x0000188D
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string shippingPayment
		{
			get
			{
				return this.shippingPaymentField;
			}
			set
			{
				this.shippingPaymentField = value;
			}
		}

		// Token: 0x17000050 RID: 80
		// (get) Token: 0x060000DB RID: 219 RVA: 0x00003696 File Offset: 0x00001896
		// (set) Token: 0x060000DC RID: 220 RVA: 0x0000369E File Offset: 0x0000189E
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string cargoCompany
		{
			get
			{
				return this.cargoCompanyField;
			}
			set
			{
				this.cargoCompanyField = value;
			}
		}

		// Token: 0x17000051 RID: 81
		// (get) Token: 0x060000DD RID: 221 RVA: 0x000036A7 File Offset: 0x000018A7
		// (set) Token: 0x060000DE RID: 222 RVA: 0x000036AF File Offset: 0x000018AF
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string cargoCode
		{
			get
			{
				return this.cargoCodeField;
			}
			set
			{
				this.cargoCodeField = value;
			}
		}

		// Token: 0x04000055 RID: 85
		private string shippingPaymentField;

		// Token: 0x04000056 RID: 86
		private string cargoCompanyField;

		// Token: 0x04000057 RID: 87
		private string cargoCodeField;
	}
}
