﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Activity
{
	// Token: 0x02000019 RID: 25
	[XmlType(Namespace = "http://activity.individual.ws.listingapi.gg.com")]
	[DesignerCategory("code")]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[DebuggerStepThrough]
	[Serializable]
	public class activityUnsoldType : activityBaseType
	{
		// Token: 0x1700007C RID: 124
		// (get) Token: 0x06000139 RID: 313 RVA: 0x000039B2 File Offset: 0x00001BB2
		// (set) Token: 0x0600013A RID: 314 RVA: 0x000039BA File Offset: 0x00001BBA
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string endDate
		{
			get
			{
				return this.endDateField;
			}
			set
			{
				this.endDateField = value;
			}
		}

		// Token: 0x1700007D RID: 125
		// (get) Token: 0x0600013B RID: 315 RVA: 0x000039C3 File Offset: 0x00001BC3
		// (set) Token: 0x0600013C RID: 316 RVA: 0x000039CB File Offset: 0x00001BCB
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string thumbImageLink
		{
			get
			{
				return this.thumbImageLinkField;
			}
			set
			{
				this.thumbImageLinkField = value;
			}
		}

		// Token: 0x1700007E RID: 126
		// (get) Token: 0x0600013D RID: 317 RVA: 0x000039D4 File Offset: 0x00001BD4
		// (set) Token: 0x0600013E RID: 318 RVA: 0x000039DC File Offset: 0x00001BDC
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string format
		{
			get
			{
				return this.formatField;
			}
			set
			{
				this.formatField = value;
			}
		}

		// Token: 0x1700007F RID: 127
		// (get) Token: 0x0600013F RID: 319 RVA: 0x000039E5 File Offset: 0x00001BE5
		// (set) Token: 0x06000140 RID: 320 RVA: 0x000039ED File Offset: 0x00001BED
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public double buyNowPrice
		{
			get
			{
				return this.buyNowPriceField;
			}
			set
			{
				this.buyNowPriceField = value;
			}
		}

		// Token: 0x17000080 RID: 128
		// (get) Token: 0x06000141 RID: 321 RVA: 0x000039F6 File Offset: 0x00001BF6
		// (set) Token: 0x06000142 RID: 322 RVA: 0x000039FE File Offset: 0x00001BFE
		[XmlIgnore]
		public bool buyNowPriceSpecified
		{
			get
			{
				return this.buyNowPriceFieldSpecified;
			}
			set
			{
				this.buyNowPriceFieldSpecified = value;
			}
		}

		// Token: 0x17000081 RID: 129
		// (get) Token: 0x06000143 RID: 323 RVA: 0x00003A07 File Offset: 0x00001C07
		// (set) Token: 0x06000144 RID: 324 RVA: 0x00003A0F File Offset: 0x00001C0F
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public double highPrice
		{
			get
			{
				return this.highPriceField;
			}
			set
			{
				this.highPriceField = value;
			}
		}

		// Token: 0x17000082 RID: 130
		// (get) Token: 0x06000145 RID: 325 RVA: 0x00003A18 File Offset: 0x00001C18
		// (set) Token: 0x06000146 RID: 326 RVA: 0x00003A20 File Offset: 0x00001C20
		[XmlIgnore]
		public bool highPriceSpecified
		{
			get
			{
				return this.highPriceFieldSpecified;
			}
			set
			{
				this.highPriceFieldSpecified = value;
			}
		}

		// Token: 0x17000083 RID: 131
		// (get) Token: 0x06000147 RID: 327 RVA: 0x00003A29 File Offset: 0x00001C29
		// (set) Token: 0x06000148 RID: 328 RVA: 0x00003A31 File Offset: 0x00001C31
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public double startPrice
		{
			get
			{
				return this.startPriceField;
			}
			set
			{
				this.startPriceField = value;
			}
		}

		// Token: 0x17000084 RID: 132
		// (get) Token: 0x06000149 RID: 329 RVA: 0x00003A3A File Offset: 0x00001C3A
		// (set) Token: 0x0600014A RID: 330 RVA: 0x00003A42 File Offset: 0x00001C42
		[XmlIgnore]
		public bool startPriceSpecified
		{
			get
			{
				return this.startPriceFieldSpecified;
			}
			set
			{
				this.startPriceFieldSpecified = value;
			}
		}

		// Token: 0x04000082 RID: 130
		private string endDateField;

		// Token: 0x04000083 RID: 131
		private string thumbImageLinkField;

		// Token: 0x04000084 RID: 132
		private string formatField;

		// Token: 0x04000085 RID: 133
		private double buyNowPriceField;

		// Token: 0x04000086 RID: 134
		private bool buyNowPriceFieldSpecified;

		// Token: 0x04000087 RID: 135
		private double highPriceField;

		// Token: 0x04000088 RID: 136
		private bool highPriceFieldSpecified;

		// Token: 0x04000089 RID: 137
		private double startPriceField;

		// Token: 0x0400008A RID: 138
		private bool startPriceFieldSpecified;
	}
}
