﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Activity
{
	// Token: 0x0200001F RID: 31
	[DebuggerStepThrough]
	[XmlType(Namespace = "http://activity.individual.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[DesignerCategory("code")]
	[Serializable]
	public class activityWonType : activityBaseType
	{
		// Token: 0x170000AE RID: 174
		// (get) Token: 0x060001A3 RID: 419 RVA: 0x00003D34 File Offset: 0x00001F34
		// (set) Token: 0x060001A4 RID: 420 RVA: 0x00003D3C File Offset: 0x00001F3C
		[XmlArrayItem("photo", Form = XmlSchemaForm.Unqualified, IsNullable = false)]
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		public photoType[] photos
		{
			get
			{
				return this.photosField;
			}
			set
			{
				this.photosField = value;
			}
		}

		// Token: 0x170000AF RID: 175
		// (get) Token: 0x060001A5 RID: 421 RVA: 0x00003D45 File Offset: 0x00001F45
		// (set) Token: 0x060001A6 RID: 422 RVA: 0x00003D4D File Offset: 0x00001F4D
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string format
		{
			get
			{
				return this.formatField;
			}
			set
			{
				this.formatField = value;
			}
		}

		// Token: 0x170000B0 RID: 176
		// (get) Token: 0x060001A7 RID: 423 RVA: 0x00003D56 File Offset: 0x00001F56
		// (set) Token: 0x060001A8 RID: 424 RVA: 0x00003D5E File Offset: 0x00001F5E
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public double price
		{
			get
			{
				return this.priceField;
			}
			set
			{
				this.priceField = value;
			}
		}

		// Token: 0x170000B1 RID: 177
		// (get) Token: 0x060001A9 RID: 425 RVA: 0x00003D67 File Offset: 0x00001F67
		// (set) Token: 0x060001AA RID: 426 RVA: 0x00003D6F File Offset: 0x00001F6F
		[XmlIgnore]
		public bool priceSpecified
		{
			get
			{
				return this.priceFieldSpecified;
			}
			set
			{
				this.priceFieldSpecified = value;
			}
		}

		// Token: 0x170000B2 RID: 178
		// (get) Token: 0x060001AB RID: 427 RVA: 0x00003D78 File Offset: 0x00001F78
		// (set) Token: 0x060001AC RID: 428 RVA: 0x00003D80 File Offset: 0x00001F80
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public double paidPrice
		{
			get
			{
				return this.paidPriceField;
			}
			set
			{
				this.paidPriceField = value;
			}
		}

		// Token: 0x170000B3 RID: 179
		// (get) Token: 0x060001AD RID: 429 RVA: 0x00003D89 File Offset: 0x00001F89
		// (set) Token: 0x060001AE RID: 430 RVA: 0x00003D91 File Offset: 0x00001F91
		[XmlIgnore]
		public bool paidPriceSpecified
		{
			get
			{
				return this.paidPriceFieldSpecified;
			}
			set
			{
				this.paidPriceFieldSpecified = value;
			}
		}

		// Token: 0x170000B4 RID: 180
		// (get) Token: 0x060001AF RID: 431 RVA: 0x00003D9A File Offset: 0x00001F9A
		// (set) Token: 0x060001B0 RID: 432 RVA: 0x00003DA2 File Offset: 0x00001FA2
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public double discountedPrice
		{
			get
			{
				return this.discountedPriceField;
			}
			set
			{
				this.discountedPriceField = value;
			}
		}

		// Token: 0x170000B5 RID: 181
		// (get) Token: 0x060001B1 RID: 433 RVA: 0x00003DAB File Offset: 0x00001FAB
		// (set) Token: 0x060001B2 RID: 434 RVA: 0x00003DB3 File Offset: 0x00001FB3
		[XmlIgnore]
		public bool discountedPriceSpecified
		{
			get
			{
				return this.discountedPriceFieldSpecified;
			}
			set
			{
				this.discountedPriceFieldSpecified = value;
			}
		}

		// Token: 0x170000B6 RID: 182
		// (get) Token: 0x060001B3 RID: 435 RVA: 0x00003DBC File Offset: 0x00001FBC
		// (set) Token: 0x060001B4 RID: 436 RVA: 0x00003DC4 File Offset: 0x00001FC4
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public double installmentPrice
		{
			get
			{
				return this.installmentPriceField;
			}
			set
			{
				this.installmentPriceField = value;
			}
		}

		// Token: 0x170000B7 RID: 183
		// (get) Token: 0x060001B5 RID: 437 RVA: 0x00003DCD File Offset: 0x00001FCD
		// (set) Token: 0x060001B6 RID: 438 RVA: 0x00003DD5 File Offset: 0x00001FD5
		[XmlIgnore]
		public bool installmentPriceSpecified
		{
			get
			{
				return this.installmentPriceFieldSpecified;
			}
			set
			{
				this.installmentPriceFieldSpecified = value;
			}
		}

		// Token: 0x170000B8 RID: 184
		// (get) Token: 0x060001B7 RID: 439 RVA: 0x00003DDE File Offset: 0x00001FDE
		// (set) Token: 0x060001B8 RID: 440 RVA: 0x00003DE6 File Offset: 0x00001FE6
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int productCount
		{
			get
			{
				return this.productCountField;
			}
			set
			{
				this.productCountField = value;
			}
		}

		// Token: 0x170000B9 RID: 185
		// (get) Token: 0x060001B9 RID: 441 RVA: 0x00003DEF File Offset: 0x00001FEF
		// (set) Token: 0x060001BA RID: 442 RVA: 0x00003DF7 File Offset: 0x00001FF7
		[XmlIgnore]
		public bool productCountSpecified
		{
			get
			{
				return this.productCountFieldSpecified;
			}
			set
			{
				this.productCountFieldSpecified = value;
			}
		}

		// Token: 0x170000BA RID: 186
		// (get) Token: 0x060001BB RID: 443 RVA: 0x00003E00 File Offset: 0x00002000
		// (set) Token: 0x060001BC RID: 444 RVA: 0x00003E08 File Offset: 0x00002008
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string endDate
		{
			get
			{
				return this.endDateField;
			}
			set
			{
				this.endDateField = value;
			}
		}

		// Token: 0x170000BB RID: 187
		// (get) Token: 0x060001BD RID: 445 RVA: 0x00003E11 File Offset: 0x00002011
		// (set) Token: 0x060001BE RID: 446 RVA: 0x00003E19 File Offset: 0x00002019
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string lastProcessDate
		{
			get
			{
				return this.lastProcessDateField;
			}
			set
			{
				this.lastProcessDateField = value;
			}
		}

		// Token: 0x170000BC RID: 188
		// (get) Token: 0x060001BF RID: 447 RVA: 0x00003E22 File Offset: 0x00002022
		// (set) Token: 0x060001C0 RID: 448 RVA: 0x00003E2A File Offset: 0x0000202A
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int status
		{
			get
			{
				return this.statusField;
			}
			set
			{
				this.statusField = value;
			}
		}

		// Token: 0x170000BD RID: 189
		// (get) Token: 0x060001C1 RID: 449 RVA: 0x00003E33 File Offset: 0x00002033
		// (set) Token: 0x060001C2 RID: 450 RVA: 0x00003E3B File Offset: 0x0000203B
		[XmlIgnore]
		public bool statusSpecified
		{
			get
			{
				return this.statusFieldSpecified;
			}
			set
			{
				this.statusFieldSpecified = value;
			}
		}

		// Token: 0x170000BE RID: 190
		// (get) Token: 0x060001C3 RID: 451 RVA: 0x00003E44 File Offset: 0x00002044
		// (set) Token: 0x060001C4 RID: 452 RVA: 0x00003E4C File Offset: 0x0000204C
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string statusDescription
		{
			get
			{
				return this.statusDescriptionField;
			}
			set
			{
				this.statusDescriptionField = value;
			}
		}

		// Token: 0x170000BF RID: 191
		// (get) Token: 0x060001C5 RID: 453 RVA: 0x00003E55 File Offset: 0x00002055
		// (set) Token: 0x060001C6 RID: 454 RVA: 0x00003E5D File Offset: 0x0000205D
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public cargoInfoType cargoDetail
		{
			get
			{
				return this.cargoDetailField;
			}
			set
			{
				this.cargoDetailField = value;
			}
		}

		// Token: 0x170000C0 RID: 192
		// (get) Token: 0x060001C7 RID: 455 RVA: 0x00003E66 File Offset: 0x00002066
		// (set) Token: 0x060001C8 RID: 456 RVA: 0x00003E6E File Offset: 0x0000206E
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string saleCode
		{
			get
			{
				return this.saleCodeField;
			}
			set
			{
				this.saleCodeField = value;
			}
		}

		// Token: 0x170000C1 RID: 193
		// (get) Token: 0x060001C9 RID: 457 RVA: 0x00003E77 File Offset: 0x00002077
		// (set) Token: 0x060001CA RID: 458 RVA: 0x00003E7F File Offset: 0x0000207F
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string seller
		{
			get
			{
				return this.sellerField;
			}
			set
			{
				this.sellerField = value;
			}
		}

		// Token: 0x170000C2 RID: 194
		// (get) Token: 0x060001CB RID: 459 RVA: 0x00003E88 File Offset: 0x00002088
		// (set) Token: 0x060001CC RID: 460 RVA: 0x00003E90 File Offset: 0x00002090
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int sellerProcessCount
		{
			get
			{
				return this.sellerProcessCountField;
			}
			set
			{
				this.sellerProcessCountField = value;
			}
		}

		// Token: 0x170000C3 RID: 195
		// (get) Token: 0x060001CD RID: 461 RVA: 0x00003E99 File Offset: 0x00002099
		// (set) Token: 0x060001CE RID: 462 RVA: 0x00003EA1 File Offset: 0x000020A1
		[XmlIgnore]
		public bool sellerProcessCountSpecified
		{
			get
			{
				return this.sellerProcessCountFieldSpecified;
			}
			set
			{
				this.sellerProcessCountFieldSpecified = value;
			}
		}

		// Token: 0x170000C4 RID: 196
		// (get) Token: 0x060001CF RID: 463 RVA: 0x00003EAA File Offset: 0x000020AA
		// (set) Token: 0x060001D0 RID: 464 RVA: 0x00003EB2 File Offset: 0x000020B2
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string note
		{
			get
			{
				return this.noteField;
			}
			set
			{
				this.noteField = value;
			}
		}

		// Token: 0x170000C5 RID: 197
		// (get) Token: 0x060001D1 RID: 465 RVA: 0x00003EBB File Offset: 0x000020BB
		// (set) Token: 0x060001D2 RID: 466 RVA: 0x00003EC3 File Offset: 0x000020C3
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string thumbImageLink
		{
			get
			{
				return this.thumbImageLinkField;
			}
			set
			{
				this.thumbImageLinkField = value;
			}
		}

		// Token: 0x040000B4 RID: 180
		private photoType[] photosField;

		// Token: 0x040000B5 RID: 181
		private string formatField;

		// Token: 0x040000B6 RID: 182
		private double priceField;

		// Token: 0x040000B7 RID: 183
		private bool priceFieldSpecified;

		// Token: 0x040000B8 RID: 184
		private double paidPriceField;

		// Token: 0x040000B9 RID: 185
		private bool paidPriceFieldSpecified;

		// Token: 0x040000BA RID: 186
		private double discountedPriceField;

		// Token: 0x040000BB RID: 187
		private bool discountedPriceFieldSpecified;

		// Token: 0x040000BC RID: 188
		private double installmentPriceField;

		// Token: 0x040000BD RID: 189
		private bool installmentPriceFieldSpecified;

		// Token: 0x040000BE RID: 190
		private int productCountField;

		// Token: 0x040000BF RID: 191
		private bool productCountFieldSpecified;

		// Token: 0x040000C0 RID: 192
		private string endDateField;

		// Token: 0x040000C1 RID: 193
		private string lastProcessDateField;

		// Token: 0x040000C2 RID: 194
		private int statusField;

		// Token: 0x040000C3 RID: 195
		private bool statusFieldSpecified;

		// Token: 0x040000C4 RID: 196
		private string statusDescriptionField;

		// Token: 0x040000C5 RID: 197
		private cargoInfoType cargoDetailField;

		// Token: 0x040000C6 RID: 198
		private string saleCodeField;

		// Token: 0x040000C7 RID: 199
		private string sellerField;

		// Token: 0x040000C8 RID: 200
		private int sellerProcessCountField;

		// Token: 0x040000C9 RID: 201
		private bool sellerProcessCountFieldSpecified;

		// Token: 0x040000CA RID: 202
		private string noteField;

		// Token: 0x040000CB RID: 203
		private string thumbImageLinkField;
	}
}
