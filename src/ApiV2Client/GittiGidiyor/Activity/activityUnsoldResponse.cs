﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Activity
{
	// Token: 0x02000018 RID: 24
	[DesignerCategory("code")]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[DebuggerStepThrough]
	[XmlType(Namespace = "http://activity.individual.ws.listingapi.gg.com")]
	[Serializable]
	public class activityUnsoldResponse : baseResponse
	{
		// Token: 0x17000079 RID: 121
		// (get) Token: 0x06000132 RID: 306 RVA: 0x00003977 File Offset: 0x00001B77
		// (set) Token: 0x06000133 RID: 307 RVA: 0x0000397F File Offset: 0x00001B7F
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int count
		{
			get
			{
				return this.countField;
			}
			set
			{
				this.countField = value;
			}
		}

		// Token: 0x1700007A RID: 122
		// (get) Token: 0x06000134 RID: 308 RVA: 0x00003988 File Offset: 0x00001B88
		// (set) Token: 0x06000135 RID: 309 RVA: 0x00003990 File Offset: 0x00001B90
		[XmlIgnore]
		public bool countSpecified
		{
			get
			{
				return this.countFieldSpecified;
			}
			set
			{
				this.countFieldSpecified = value;
			}
		}

		// Token: 0x1700007B RID: 123
		// (get) Token: 0x06000136 RID: 310 RVA: 0x00003999 File Offset: 0x00001B99
		// (set) Token: 0x06000137 RID: 311 RVA: 0x000039A1 File Offset: 0x00001BA1
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		[XmlArrayItem("item", Form = XmlSchemaForm.Unqualified)]
		public activityUnsoldType[] itemList
		{
			get
			{
				return this.itemListField;
			}
			set
			{
				this.itemListField = value;
			}
		}

		// Token: 0x0400007F RID: 127
		private int countField;

		// Token: 0x04000080 RID: 128
		private bool countFieldSpecified;

		// Token: 0x04000081 RID: 129
		private activityUnsoldType[] itemListField;
	}
}
