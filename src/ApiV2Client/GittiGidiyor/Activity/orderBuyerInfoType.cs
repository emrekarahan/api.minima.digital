﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Activity
{
	// Token: 0x02000012 RID: 18
	[DesignerCategory("code")]
	[XmlType(Namespace = "http://activity.individual.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[DebuggerStepThrough]
	[Serializable]
	public class orderBuyerInfoType
	{
		// Token: 0x17000041 RID: 65
		// (get) Token: 0x060000BC RID: 188 RVA: 0x0000358F File Offset: 0x0000178F
		// (set) Token: 0x060000BD RID: 189 RVA: 0x00003597 File Offset: 0x00001797
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string username
		{
			get
			{
				return this.usernameField;
			}
			set
			{
				this.usernameField = value;
			}
		}

		// Token: 0x17000042 RID: 66
		// (get) Token: 0x060000BE RID: 190 RVA: 0x000035A0 File Offset: 0x000017A0
		// (set) Token: 0x060000BF RID: 191 RVA: 0x000035A8 File Offset: 0x000017A8
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string name
		{
			get
			{
				return this.nameField;
			}
			set
			{
				this.nameField = value;
			}
		}

		// Token: 0x17000043 RID: 67
		// (get) Token: 0x060000C0 RID: 192 RVA: 0x000035B1 File Offset: 0x000017B1
		// (set) Token: 0x060000C1 RID: 193 RVA: 0x000035B9 File Offset: 0x000017B9
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string surname
		{
			get
			{
				return this.surnameField;
			}
			set
			{
				this.surnameField = value;
			}
		}

		// Token: 0x17000044 RID: 68
		// (get) Token: 0x060000C2 RID: 194 RVA: 0x000035C2 File Offset: 0x000017C2
		// (set) Token: 0x060000C3 RID: 195 RVA: 0x000035CA File Offset: 0x000017CA
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string phone
		{
			get
			{
				return this.phoneField;
			}
			set
			{
				this.phoneField = value;
			}
		}

		// Token: 0x17000045 RID: 69
		// (get) Token: 0x060000C4 RID: 196 RVA: 0x000035D3 File Offset: 0x000017D3
		// (set) Token: 0x060000C5 RID: 197 RVA: 0x000035DB File Offset: 0x000017DB
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string mobilePhone
		{
			get
			{
				return this.mobilePhoneField;
			}
			set
			{
				this.mobilePhoneField = value;
			}
		}

		// Token: 0x17000046 RID: 70
		// (get) Token: 0x060000C6 RID: 198 RVA: 0x000035E4 File Offset: 0x000017E4
		// (set) Token: 0x060000C7 RID: 199 RVA: 0x000035EC File Offset: 0x000017EC
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string email
		{
			get
			{
				return this.emailField;
			}
			set
			{
				this.emailField = value;
			}
		}

		// Token: 0x17000047 RID: 71
		// (get) Token: 0x060000C8 RID: 200 RVA: 0x000035F5 File Offset: 0x000017F5
		// (set) Token: 0x060000C9 RID: 201 RVA: 0x000035FD File Offset: 0x000017FD
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string address
		{
			get
			{
				return this.addressField;
			}
			set
			{
				this.addressField = value;
			}
		}

		// Token: 0x17000048 RID: 72
		// (get) Token: 0x060000CA RID: 202 RVA: 0x00003606 File Offset: 0x00001806
		// (set) Token: 0x060000CB RID: 203 RVA: 0x0000360E File Offset: 0x0000180E
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string district
		{
			get
			{
				return this.districtField;
			}
			set
			{
				this.districtField = value;
			}
		}

		// Token: 0x17000049 RID: 73
		// (get) Token: 0x060000CC RID: 204 RVA: 0x00003617 File Offset: 0x00001817
		// (set) Token: 0x060000CD RID: 205 RVA: 0x0000361F File Offset: 0x0000181F
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string city
		{
			get
			{
				return this.cityField;
			}
			set
			{
				this.cityField = value;
			}
		}

		// Token: 0x1700004A RID: 74
		// (get) Token: 0x060000CE RID: 206 RVA: 0x00003628 File Offset: 0x00001828
		// (set) Token: 0x060000CF RID: 207 RVA: 0x00003630 File Offset: 0x00001830
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string zipCode
		{
			get
			{
				return this.zipCodeField;
			}
			set
			{
				this.zipCodeField = value;
			}
		}

		// Token: 0x1700004B RID: 75
		// (get) Token: 0x060000D0 RID: 208 RVA: 0x00003639 File Offset: 0x00001839
		// (set) Token: 0x060000D1 RID: 209 RVA: 0x00003641 File Offset: 0x00001841
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int processCount
		{
			get
			{
				return this.processCountField;
			}
			set
			{
				this.processCountField = value;
			}
		}

		// Token: 0x1700004C RID: 76
		// (get) Token: 0x060000D2 RID: 210 RVA: 0x0000364A File Offset: 0x0000184A
		// (set) Token: 0x060000D3 RID: 211 RVA: 0x00003652 File Offset: 0x00001852
		[XmlIgnore]
		public bool processCountSpecified
		{
			get
			{
				return this.processCountFieldSpecified;
			}
			set
			{
				this.processCountFieldSpecified = value;
			}
		}

		// Token: 0x1700004D RID: 77
		// (get) Token: 0x060000D4 RID: 212 RVA: 0x0000365B File Offset: 0x0000185B
		// (set) Token: 0x060000D5 RID: 213 RVA: 0x00003663 File Offset: 0x00001863
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int ratePercentage
		{
			get
			{
				return this.ratePercentageField;
			}
			set
			{
				this.ratePercentageField = value;
			}
		}

		// Token: 0x1700004E RID: 78
		// (get) Token: 0x060000D6 RID: 214 RVA: 0x0000366C File Offset: 0x0000186C
		// (set) Token: 0x060000D7 RID: 215 RVA: 0x00003674 File Offset: 0x00001874
		[XmlIgnore]
		public bool ratePercentageSpecified
		{
			get
			{
				return this.ratePercentageFieldSpecified;
			}
			set
			{
				this.ratePercentageFieldSpecified = value;
			}
		}

		// Token: 0x04000047 RID: 71
		private string usernameField;

		// Token: 0x04000048 RID: 72
		private string nameField;

		// Token: 0x04000049 RID: 73
		private string surnameField;

		// Token: 0x0400004A RID: 74
		private string phoneField;

		// Token: 0x0400004B RID: 75
		private string mobilePhoneField;

		// Token: 0x0400004C RID: 76
		private string emailField;

		// Token: 0x0400004D RID: 77
		private string addressField;

		// Token: 0x0400004E RID: 78
		private string districtField;

		// Token: 0x0400004F RID: 79
		private string cityField;

		// Token: 0x04000050 RID: 80
		private string zipCodeField;

		// Token: 0x04000051 RID: 81
		private int processCountField;

		// Token: 0x04000052 RID: 82
		private bool processCountFieldSpecified;

		// Token: 0x04000053 RID: 83
		private int ratePercentageField;

		// Token: 0x04000054 RID: 84
		private bool ratePercentageFieldSpecified;
	}
}
