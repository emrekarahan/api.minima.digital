﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Activity
{
	// Token: 0x02000016 RID: 22
	[XmlType(Namespace = "http://activity.individual.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[Serializable]
	public class activityWatchResponse : baseResponse
	{
		// Token: 0x1700005A RID: 90
		// (get) Token: 0x060000F2 RID: 242 RVA: 0x00003758 File Offset: 0x00001958
		// (set) Token: 0x060000F3 RID: 243 RVA: 0x00003760 File Offset: 0x00001960
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int count
		{
			get
			{
				return this.countField;
			}
			set
			{
				this.countField = value;
			}
		}

		// Token: 0x1700005B RID: 91
		// (get) Token: 0x060000F4 RID: 244 RVA: 0x00003769 File Offset: 0x00001969
		// (set) Token: 0x060000F5 RID: 245 RVA: 0x00003771 File Offset: 0x00001971
		[XmlIgnore]
		public bool countSpecified
		{
			get
			{
				return this.countFieldSpecified;
			}
			set
			{
				this.countFieldSpecified = value;
			}
		}

		// Token: 0x1700005C RID: 92
		// (get) Token: 0x060000F6 RID: 246 RVA: 0x0000377A File Offset: 0x0000197A
		// (set) Token: 0x060000F7 RID: 247 RVA: 0x00003782 File Offset: 0x00001982
		[XmlArrayItem("item", Form = XmlSchemaForm.Unqualified)]
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		public activityWatchType[] itemList
		{
			get
			{
				return this.itemListField;
			}
			set
			{
				this.itemListField = value;
			}
		}

		// Token: 0x04000060 RID: 96
		private int countField;

		// Token: 0x04000061 RID: 97
		private bool countFieldSpecified;

		// Token: 0x04000062 RID: 98
		private activityWatchType[] itemListField;
	}
}
