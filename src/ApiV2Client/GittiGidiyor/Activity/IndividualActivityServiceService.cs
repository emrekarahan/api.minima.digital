﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading;
using System.Web.Services;
using System.Web.Services.Description;
using System.Web.Services.Protocols;
using System.Xml.Serialization;
using ApiV2Client.Properties;

namespace ApiV2Client.GittiGidiyor.Activity
{
	// Token: 0x0200000B RID: 11
	[DebuggerStepThrough]
	[WebServiceBinding(Name = "IndividualActivityServiceBinding", Namespace = "http://activity.individual.ws.listingapi.gg.com")]
	[XmlInclude(typeof(baseResponse))]
	[DesignerCategory("code")]
	[XmlInclude(typeof(activityBaseType))]
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	public class IndividualActivityServiceService : SoapHttpClientProtocol
	{
		// Token: 0x0600002B RID: 43 RVA: 0x000024B9 File Offset: 0x000006B9
		public IndividualActivityServiceService()
		{
			this.Url = Settings.Default.ApiV2Client_GittiGidiyor_Activity_IndividualActivityServiceService;
			if (this.IsLocalFileSystemWebService(this.Url))
			{
				this.UseDefaultCredentials = true;
				this.useDefaultCredentialsSetExplicitly = false;
				return;
			}
			this.useDefaultCredentialsSetExplicitly = true;
		}

		// Token: 0x17000015 RID: 21
		// (get) Token: 0x0600002C RID: 44 RVA: 0x000024F5 File Offset: 0x000006F5
		// (set) Token: 0x0600002D RID: 45 RVA: 0x000024FD File Offset: 0x000006FD
		public new string Url
		{
			get
			{
				return base.Url;
			}
			set
			{
				if (this.IsLocalFileSystemWebService(base.Url) && !this.useDefaultCredentialsSetExplicitly && !this.IsLocalFileSystemWebService(value))
				{
					base.UseDefaultCredentials = false;
				}
				base.Url = value;
			}
		}

		// Token: 0x17000016 RID: 22
		// (get) Token: 0x0600002E RID: 46 RVA: 0x0000252C File Offset: 0x0000072C
		// (set) Token: 0x0600002F RID: 47 RVA: 0x00002534 File Offset: 0x00000734
		public new bool UseDefaultCredentials
		{
			get
			{
				return base.UseDefaultCredentials;
			}
			set
			{
				base.UseDefaultCredentials = value;
				this.useDefaultCredentialsSetExplicitly = true;
			}
		}

		// Token: 0x14000001 RID: 1
		// (add) Token: 0x06000030 RID: 48 RVA: 0x00002544 File Offset: 0x00000744
		// (remove) Token: 0x06000031 RID: 49 RVA: 0x0000257C File Offset: 0x0000077C
		public event getActiveSalesCompletedEventHandler getActiveSalesCompleted;

		// Token: 0x14000002 RID: 2
		// (add) Token: 0x06000032 RID: 50 RVA: 0x000025B4 File Offset: 0x000007B4
		// (remove) Token: 0x06000033 RID: 51 RVA: 0x000025EC File Offset: 0x000007EC
		public event getBidItemsCompletedEventHandler getBidItemsCompleted;

		// Token: 0x14000003 RID: 3
		// (add) Token: 0x06000034 RID: 52 RVA: 0x00002624 File Offset: 0x00000824
		// (remove) Token: 0x06000035 RID: 53 RVA: 0x0000265C File Offset: 0x0000085C
		public event getDidntWinItemsCompletedEventHandler getDidntWinItemsCompleted;

		// Token: 0x14000004 RID: 4
		// (add) Token: 0x06000036 RID: 54 RVA: 0x00002694 File Offset: 0x00000894
		// (remove) Token: 0x06000037 RID: 55 RVA: 0x000026CC File Offset: 0x000008CC
		public event getServiceNameCompletedEventHandler getServiceNameCompleted;

		// Token: 0x14000005 RID: 5
		// (add) Token: 0x06000038 RID: 56 RVA: 0x00002704 File Offset: 0x00000904
		// (remove) Token: 0x06000039 RID: 57 RVA: 0x0000273C File Offset: 0x0000093C
		public event getSoldItemsCompletedEventHandler getSoldItemsCompleted;

		// Token: 0x14000006 RID: 6
		// (add) Token: 0x0600003A RID: 58 RVA: 0x00002774 File Offset: 0x00000974
		// (remove) Token: 0x0600003B RID: 59 RVA: 0x000027AC File Offset: 0x000009AC
		public event getUnsoldItemsCompletedEventHandler getUnsoldItemsCompleted;

		// Token: 0x14000007 RID: 7
		// (add) Token: 0x0600003C RID: 60 RVA: 0x000027E4 File Offset: 0x000009E4
		// (remove) Token: 0x0600003D RID: 61 RVA: 0x0000281C File Offset: 0x00000A1C
		public event getWatchItemsCompletedEventHandler getWatchItemsCompleted;

		// Token: 0x14000008 RID: 8
		// (add) Token: 0x0600003E RID: 62 RVA: 0x00002854 File Offset: 0x00000A54
		// (remove) Token: 0x0600003F RID: 63 RVA: 0x0000288C File Offset: 0x00000A8C
		public event getWonItemsCompletedEventHandler getWonItemsCompleted;

		// Token: 0x06000040 RID: 64 RVA: 0x000028C4 File Offset: 0x00000AC4
		[SoapRpcMethod("", RequestNamespace = "http://activity.individual.ws.listingapi.gg.com", ResponseNamespace = "http://activity.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public activitySaleResponse getActiveSales(string apiKey, string sign, long time, int startOffSet, int rowCount, bool withData, string lang)
		{
			object[] array = base.Invoke("getActiveSales", new object[]
			{
				apiKey,
				sign,
				time,
				startOffSet,
				rowCount,
				withData,
				lang
			});
			return (activitySaleResponse)array[0];
		}

		// Token: 0x06000041 RID: 65 RVA: 0x00002924 File Offset: 0x00000B24
		public void getActiveSalesAsync(string apiKey, string sign, long time, int startOffSet, int rowCount, bool withData, string lang)
		{
			this.getActiveSalesAsync(apiKey, sign, time, startOffSet, rowCount, withData, lang, null);
		}

		// Token: 0x06000042 RID: 66 RVA: 0x00002944 File Offset: 0x00000B44
		public void getActiveSalesAsync(string apiKey, string sign, long time, int startOffSet, int rowCount, bool withData, string lang, object userState)
		{
			if (this.getActiveSalesOperationCompleted == null)
			{
				this.getActiveSalesOperationCompleted = new SendOrPostCallback(this.OngetActiveSalesOperationCompleted);
			}
			base.InvokeAsync("getActiveSales", new object[]
			{
				apiKey,
				sign,
				time,
				startOffSet,
				rowCount,
				withData,
				lang
			}, this.getActiveSalesOperationCompleted, userState);
		}

		// Token: 0x06000043 RID: 67 RVA: 0x000029BC File Offset: 0x00000BBC
		private void OngetActiveSalesOperationCompleted(object arg)
		{
			if (this.getActiveSalesCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getActiveSalesCompleted(this, new getActiveSalesCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x06000044 RID: 68 RVA: 0x00002A04 File Offset: 0x00000C04
		[SoapRpcMethod("", RequestNamespace = "http://activity.individual.ws.listingapi.gg.com", ResponseNamespace = "http://activity.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public activityBidResponse getBidItems(string apiKey, string sign, long time, int startOffSet, int rowCount, bool withData, string lang)
		{
			object[] array = base.Invoke("getBidItems", new object[]
			{
				apiKey,
				sign,
				time,
				startOffSet,
				rowCount,
				withData,
				lang
			});
			return (activityBidResponse)array[0];
		}

		// Token: 0x06000045 RID: 69 RVA: 0x00002A64 File Offset: 0x00000C64
		public void getBidItemsAsync(string apiKey, string sign, long time, int startOffSet, int rowCount, bool withData, string lang)
		{
			this.getBidItemsAsync(apiKey, sign, time, startOffSet, rowCount, withData, lang, null);
		}

		// Token: 0x06000046 RID: 70 RVA: 0x00002A84 File Offset: 0x00000C84
		public void getBidItemsAsync(string apiKey, string sign, long time, int startOffSet, int rowCount, bool withData, string lang, object userState)
		{
			if (this.getBidItemsOperationCompleted == null)
			{
				this.getBidItemsOperationCompleted = new SendOrPostCallback(this.OngetBidItemsOperationCompleted);
			}
			base.InvokeAsync("getBidItems", new object[]
			{
				apiKey,
				sign,
				time,
				startOffSet,
				rowCount,
				withData,
				lang
			}, this.getBidItemsOperationCompleted, userState);
		}

		// Token: 0x06000047 RID: 71 RVA: 0x00002AFC File Offset: 0x00000CFC
		private void OngetBidItemsOperationCompleted(object arg)
		{
			if (this.getBidItemsCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getBidItemsCompleted(this, new getBidItemsCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x06000048 RID: 72 RVA: 0x00002B44 File Offset: 0x00000D44
		[SoapRpcMethod("", RequestNamespace = "http://activity.individual.ws.listingapi.gg.com", ResponseNamespace = "http://activity.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public activityDidntWinResponse getDidntWinItems(string apiKey, string sign, long time, int startOffSet, int rowCount, bool withData, int dayInterval, string lang)
		{
			object[] array = base.Invoke("getDidntWinItems", new object[]
			{
				apiKey,
				sign,
				time,
				startOffSet,
				rowCount,
				withData,
				dayInterval,
				lang
			});
			return (activityDidntWinResponse)array[0];
		}

		// Token: 0x06000049 RID: 73 RVA: 0x00002BAC File Offset: 0x00000DAC
		public void getDidntWinItemsAsync(string apiKey, string sign, long time, int startOffSet, int rowCount, bool withData, int dayInterval, string lang)
		{
			this.getDidntWinItemsAsync(apiKey, sign, time, startOffSet, rowCount, withData, dayInterval, lang, null);
		}

		// Token: 0x0600004A RID: 74 RVA: 0x00002BD0 File Offset: 0x00000DD0
		public void getDidntWinItemsAsync(string apiKey, string sign, long time, int startOffSet, int rowCount, bool withData, int dayInterval, string lang, object userState)
		{
			if (this.getDidntWinItemsOperationCompleted == null)
			{
				this.getDidntWinItemsOperationCompleted = new SendOrPostCallback(this.OngetDidntWinItemsOperationCompleted);
			}
			base.InvokeAsync("getDidntWinItems", new object[]
			{
				apiKey,
				sign,
				time,
				startOffSet,
				rowCount,
				withData,
				dayInterval,
				lang
			}, this.getDidntWinItemsOperationCompleted, userState);
		}

		// Token: 0x0600004B RID: 75 RVA: 0x00002C50 File Offset: 0x00000E50
		private void OngetDidntWinItemsOperationCompleted(object arg)
		{
			if (this.getDidntWinItemsCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getDidntWinItemsCompleted(this, new getDidntWinItemsCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x0600004C RID: 76 RVA: 0x00002C98 File Offset: 0x00000E98
		[SoapRpcMethod("", RequestNamespace = "http://activity.individual.ws.listingapi.gg.com", ResponseNamespace = "http://activity.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public string getServiceName()
		{
			object[] array = base.Invoke("getServiceName", new object[0]);
			return (string)array[0];
		}

		// Token: 0x0600004D RID: 77 RVA: 0x00002CBF File Offset: 0x00000EBF
		public void getServiceNameAsync()
		{
			this.getServiceNameAsync(null);
		}

		// Token: 0x0600004E RID: 78 RVA: 0x00002CC8 File Offset: 0x00000EC8
		public void getServiceNameAsync(object userState)
		{
			if (this.getServiceNameOperationCompleted == null)
			{
				this.getServiceNameOperationCompleted = new SendOrPostCallback(this.OngetServiceNameOperationCompleted);
			}
			base.InvokeAsync("getServiceName", new object[0], this.getServiceNameOperationCompleted, userState);
		}

		// Token: 0x0600004F RID: 79 RVA: 0x00002CFC File Offset: 0x00000EFC
		private void OngetServiceNameOperationCompleted(object arg)
		{
			if (this.getServiceNameCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getServiceNameCompleted(this, new getServiceNameCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x06000050 RID: 80 RVA: 0x00002D44 File Offset: 0x00000F44
		[SoapRpcMethod("", RequestNamespace = "http://activity.individual.ws.listingapi.gg.com", ResponseNamespace = "http://activity.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public activitySoldResponse getSoldItems(string apiKey, string sign, long time, int startOffSet, int rowCount, bool withData, string lang)
		{
			object[] array = base.Invoke("getSoldItems", new object[]
			{
				apiKey,
				sign,
				time,
				startOffSet,
				rowCount,
				withData,
				lang
			});
			return (activitySoldResponse)array[0];
		}

		// Token: 0x06000051 RID: 81 RVA: 0x00002DA4 File Offset: 0x00000FA4
		public void getSoldItemsAsync(string apiKey, string sign, long time, int startOffSet, int rowCount, bool withData, string lang)
		{
			this.getSoldItemsAsync(apiKey, sign, time, startOffSet, rowCount, withData, lang, null);
		}

		// Token: 0x06000052 RID: 82 RVA: 0x00002DC4 File Offset: 0x00000FC4
		public void getSoldItemsAsync(string apiKey, string sign, long time, int startOffSet, int rowCount, bool withData, string lang, object userState)
		{
			if (this.getSoldItemsOperationCompleted == null)
			{
				this.getSoldItemsOperationCompleted = new SendOrPostCallback(this.OngetSoldItemsOperationCompleted);
			}
			base.InvokeAsync("getSoldItems", new object[]
			{
				apiKey,
				sign,
				time,
				startOffSet,
				rowCount,
				withData,
				lang
			}, this.getSoldItemsOperationCompleted, userState);
		}

		// Token: 0x06000053 RID: 83 RVA: 0x00002E3C File Offset: 0x0000103C
		private void OngetSoldItemsOperationCompleted(object arg)
		{
			if (this.getSoldItemsCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getSoldItemsCompleted(this, new getSoldItemsCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x06000054 RID: 84 RVA: 0x00002E84 File Offset: 0x00001084
		[SoapRpcMethod("", RequestNamespace = "http://activity.individual.ws.listingapi.gg.com", ResponseNamespace = "http://activity.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public activityUnsoldResponse getUnsoldItems(string apiKey, string sign, long time, int startOffSet, int rowCount, bool withData, string lang)
		{
			object[] array = base.Invoke("getUnsoldItems", new object[]
			{
				apiKey,
				sign,
				time,
				startOffSet,
				rowCount,
				withData,
				lang
			});
			return (activityUnsoldResponse)array[0];
		}

		// Token: 0x06000055 RID: 85 RVA: 0x00002EE4 File Offset: 0x000010E4
		public void getUnsoldItemsAsync(string apiKey, string sign, long time, int startOffSet, int rowCount, bool withData, string lang)
		{
			this.getUnsoldItemsAsync(apiKey, sign, time, startOffSet, rowCount, withData, lang, null);
		}

		// Token: 0x06000056 RID: 86 RVA: 0x00002F04 File Offset: 0x00001104
		public void getUnsoldItemsAsync(string apiKey, string sign, long time, int startOffSet, int rowCount, bool withData, string lang, object userState)
		{
			if (this.getUnsoldItemsOperationCompleted == null)
			{
				this.getUnsoldItemsOperationCompleted = new SendOrPostCallback(this.OngetUnsoldItemsOperationCompleted);
			}
			base.InvokeAsync("getUnsoldItems", new object[]
			{
				apiKey,
				sign,
				time,
				startOffSet,
				rowCount,
				withData,
				lang
			}, this.getUnsoldItemsOperationCompleted, userState);
		}

		// Token: 0x06000057 RID: 87 RVA: 0x00002F7C File Offset: 0x0000117C
		private void OngetUnsoldItemsOperationCompleted(object arg)
		{
			if (this.getUnsoldItemsCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getUnsoldItemsCompleted(this, new getUnsoldItemsCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x06000058 RID: 88 RVA: 0x00002FC4 File Offset: 0x000011C4
		[SoapRpcMethod("", RequestNamespace = "http://activity.individual.ws.listingapi.gg.com", ResponseNamespace = "http://activity.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public activityWatchResponse getWatchItems(string apiKey, string sign, long time, int startOffSet, int rowCount, bool withData, string lang)
		{
			object[] array = base.Invoke("getWatchItems", new object[]
			{
				apiKey,
				sign,
				time,
				startOffSet,
				rowCount,
				withData,
				lang
			});
			return (activityWatchResponse)array[0];
		}

		// Token: 0x06000059 RID: 89 RVA: 0x00003024 File Offset: 0x00001224
		public void getWatchItemsAsync(string apiKey, string sign, long time, int startOffSet, int rowCount, bool withData, string lang)
		{
			this.getWatchItemsAsync(apiKey, sign, time, startOffSet, rowCount, withData, lang, null);
		}

		// Token: 0x0600005A RID: 90 RVA: 0x00003044 File Offset: 0x00001244
		public void getWatchItemsAsync(string apiKey, string sign, long time, int startOffSet, int rowCount, bool withData, string lang, object userState)
		{
			if (this.getWatchItemsOperationCompleted == null)
			{
				this.getWatchItemsOperationCompleted = new SendOrPostCallback(this.OngetWatchItemsOperationCompleted);
			}
			base.InvokeAsync("getWatchItems", new object[]
			{
				apiKey,
				sign,
				time,
				startOffSet,
				rowCount,
				withData,
				lang
			}, this.getWatchItemsOperationCompleted, userState);
		}

		// Token: 0x0600005B RID: 91 RVA: 0x000030BC File Offset: 0x000012BC
		private void OngetWatchItemsOperationCompleted(object arg)
		{
			if (this.getWatchItemsCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getWatchItemsCompleted(this, new getWatchItemsCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x0600005C RID: 92 RVA: 0x00003104 File Offset: 0x00001304
		[SoapRpcMethod("", RequestNamespace = "http://activity.individual.ws.listingapi.gg.com", ResponseNamespace = "http://activity.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public activityWonResponse getWonItems(string apiKey, string sign, long time, int startOffSet, int rowCount, bool withData, string lang)
		{
			object[] array = base.Invoke("getWonItems", new object[]
			{
				apiKey,
				sign,
				time,
				startOffSet,
				rowCount,
				withData,
				lang
			});
			return (activityWonResponse)array[0];
		}

		// Token: 0x0600005D RID: 93 RVA: 0x00003164 File Offset: 0x00001364
		public void getWonItemsAsync(string apiKey, string sign, long time, int startOffSet, int rowCount, bool withData, string lang)
		{
			this.getWonItemsAsync(apiKey, sign, time, startOffSet, rowCount, withData, lang, null);
		}

		// Token: 0x0600005E RID: 94 RVA: 0x00003184 File Offset: 0x00001384
		public void getWonItemsAsync(string apiKey, string sign, long time, int startOffSet, int rowCount, bool withData, string lang, object userState)
		{
			if (this.getWonItemsOperationCompleted == null)
			{
				this.getWonItemsOperationCompleted = new SendOrPostCallback(this.OngetWonItemsOperationCompleted);
			}
			base.InvokeAsync("getWonItems", new object[]
			{
				apiKey,
				sign,
				time,
				startOffSet,
				rowCount,
				withData,
				lang
			}, this.getWonItemsOperationCompleted, userState);
		}

		// Token: 0x0600005F RID: 95 RVA: 0x000031FC File Offset: 0x000013FC
		private void OngetWonItemsOperationCompleted(object arg)
		{
			if (this.getWonItemsCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getWonItemsCompleted(this, new getWonItemsCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x06000060 RID: 96 RVA: 0x00003241 File Offset: 0x00001441
		public new void CancelAsync(object userState)
		{
			base.CancelAsync(userState);
		}

		// Token: 0x06000061 RID: 97 RVA: 0x0000324C File Offset: 0x0000144C
		private bool IsLocalFileSystemWebService(string url)
		{
			if (url == null || url == string.Empty)
			{
				return false;
			}
			Uri uri = new Uri(url);
			return uri.Port >= 1024 && string.Compare(uri.Host, "localHost", StringComparison.OrdinalIgnoreCase) == 0;
		}

		// Token: 0x0400000C RID: 12
		private SendOrPostCallback getActiveSalesOperationCompleted;

		// Token: 0x0400000D RID: 13
		private SendOrPostCallback getBidItemsOperationCompleted;

		// Token: 0x0400000E RID: 14
		private SendOrPostCallback getDidntWinItemsOperationCompleted;

		// Token: 0x0400000F RID: 15
		private SendOrPostCallback getServiceNameOperationCompleted;

		// Token: 0x04000010 RID: 16
		private SendOrPostCallback getSoldItemsOperationCompleted;

		// Token: 0x04000011 RID: 17
		private SendOrPostCallback getUnsoldItemsOperationCompleted;

		// Token: 0x04000012 RID: 18
		private SendOrPostCallback getWatchItemsOperationCompleted;

		// Token: 0x04000013 RID: 19
		private SendOrPostCallback getWonItemsOperationCompleted;

		// Token: 0x04000014 RID: 20
		private bool useDefaultCredentialsSetExplicitly;
	}
}
