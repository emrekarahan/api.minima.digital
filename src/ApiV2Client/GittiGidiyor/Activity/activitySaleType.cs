﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Activity
{
	// Token: 0x0200000F RID: 15
	[XmlType(Namespace = "http://activity.individual.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[Serializable]
	public class activitySaleType : activityBaseType
	{
		// Token: 0x17000025 RID: 37
		// (get) Token: 0x06000081 RID: 129 RVA: 0x0000339B File Offset: 0x0000159B
		// (set) Token: 0x06000082 RID: 130 RVA: 0x000033A3 File Offset: 0x000015A3
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string format
		{
			get
			{
				return this.formatField;
			}
			set
			{
				this.formatField = value;
			}
		}

		// Token: 0x17000026 RID: 38
		// (get) Token: 0x06000083 RID: 131 RVA: 0x000033AC File Offset: 0x000015AC
		// (set) Token: 0x06000084 RID: 132 RVA: 0x000033B4 File Offset: 0x000015B4
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public double buyNowPrice
		{
			get
			{
				return this.buyNowPriceField;
			}
			set
			{
				this.buyNowPriceField = value;
			}
		}

		// Token: 0x17000027 RID: 39
		// (get) Token: 0x06000085 RID: 133 RVA: 0x000033BD File Offset: 0x000015BD
		// (set) Token: 0x06000086 RID: 134 RVA: 0x000033C5 File Offset: 0x000015C5
		[XmlIgnore]
		public bool buyNowPriceSpecified
		{
			get
			{
				return this.buyNowPriceFieldSpecified;
			}
			set
			{
				this.buyNowPriceFieldSpecified = value;
			}
		}

		// Token: 0x17000028 RID: 40
		// (get) Token: 0x06000087 RID: 135 RVA: 0x000033CE File Offset: 0x000015CE
		// (set) Token: 0x06000088 RID: 136 RVA: 0x000033D6 File Offset: 0x000015D6
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public double highPrice
		{
			get
			{
				return this.highPriceField;
			}
			set
			{
				this.highPriceField = value;
			}
		}

		// Token: 0x17000029 RID: 41
		// (get) Token: 0x06000089 RID: 137 RVA: 0x000033DF File Offset: 0x000015DF
		// (set) Token: 0x0600008A RID: 138 RVA: 0x000033E7 File Offset: 0x000015E7
		[XmlIgnore]
		public bool highPriceSpecified
		{
			get
			{
				return this.highPriceFieldSpecified;
			}
			set
			{
				this.highPriceFieldSpecified = value;
			}
		}

		// Token: 0x1700002A RID: 42
		// (get) Token: 0x0600008B RID: 139 RVA: 0x000033F0 File Offset: 0x000015F0
		// (set) Token: 0x0600008C RID: 140 RVA: 0x000033F8 File Offset: 0x000015F8
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int productCount
		{
			get
			{
				return this.productCountField;
			}
			set
			{
				this.productCountField = value;
			}
		}

		// Token: 0x1700002B RID: 43
		// (get) Token: 0x0600008D RID: 141 RVA: 0x00003401 File Offset: 0x00001601
		// (set) Token: 0x0600008E RID: 142 RVA: 0x00003409 File Offset: 0x00001609
		[XmlIgnore]
		public bool productCountSpecified
		{
			get
			{
				return this.productCountFieldSpecified;
			}
			set
			{
				this.productCountFieldSpecified = value;
			}
		}

		// Token: 0x1700002C RID: 44
		// (get) Token: 0x0600008F RID: 143 RVA: 0x00003412 File Offset: 0x00001612
		// (set) Token: 0x06000090 RID: 144 RVA: 0x0000341A File Offset: 0x0000161A
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int soldProductCount
		{
			get
			{
				return this.soldProductCountField;
			}
			set
			{
				this.soldProductCountField = value;
			}
		}

		// Token: 0x1700002D RID: 45
		// (get) Token: 0x06000091 RID: 145 RVA: 0x00003423 File Offset: 0x00001623
		// (set) Token: 0x06000092 RID: 146 RVA: 0x0000342B File Offset: 0x0000162B
		[XmlIgnore]
		public bool soldProductCountSpecified
		{
			get
			{
				return this.soldProductCountFieldSpecified;
			}
			set
			{
				this.soldProductCountFieldSpecified = value;
			}
		}

		// Token: 0x1700002E RID: 46
		// (get) Token: 0x06000093 RID: 147 RVA: 0x00003434 File Offset: 0x00001634
		// (set) Token: 0x06000094 RID: 148 RVA: 0x0000343C File Offset: 0x0000163C
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string remainingTime
		{
			get
			{
				return this.remainingTimeField;
			}
			set
			{
				this.remainingTimeField = value;
			}
		}

		// Token: 0x1700002F RID: 47
		// (get) Token: 0x06000095 RID: 149 RVA: 0x00003445 File Offset: 0x00001645
		// (set) Token: 0x06000096 RID: 150 RVA: 0x0000344D File Offset: 0x0000164D
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int bidCount
		{
			get
			{
				return this.bidCountField;
			}
			set
			{
				this.bidCountField = value;
			}
		}

		// Token: 0x17000030 RID: 48
		// (get) Token: 0x06000097 RID: 151 RVA: 0x00003456 File Offset: 0x00001656
		// (set) Token: 0x06000098 RID: 152 RVA: 0x0000345E File Offset: 0x0000165E
		[XmlIgnore]
		public bool bidCountSpecified
		{
			get
			{
				return this.bidCountFieldSpecified;
			}
			set
			{
				this.bidCountFieldSpecified = value;
			}
		}

		// Token: 0x17000031 RID: 49
		// (get) Token: 0x06000099 RID: 153 RVA: 0x00003467 File Offset: 0x00001667
		// (set) Token: 0x0600009A RID: 154 RVA: 0x0000346F File Offset: 0x0000166F
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string highestBidder
		{
			get
			{
				return this.highestBidderField;
			}
			set
			{
				this.highestBidderField = value;
			}
		}

		// Token: 0x17000032 RID: 50
		// (get) Token: 0x0600009B RID: 155 RVA: 0x00003478 File Offset: 0x00001678
		// (set) Token: 0x0600009C RID: 156 RVA: 0x00003480 File Offset: 0x00001680
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int watcherNumber
		{
			get
			{
				return this.watcherNumberField;
			}
			set
			{
				this.watcherNumberField = value;
			}
		}

		// Token: 0x17000033 RID: 51
		// (get) Token: 0x0600009D RID: 157 RVA: 0x00003489 File Offset: 0x00001689
		// (set) Token: 0x0600009E RID: 158 RVA: 0x00003491 File Offset: 0x00001691
		[XmlIgnore]
		public bool watcherNumberSpecified
		{
			get
			{
				return this.watcherNumberFieldSpecified;
			}
			set
			{
				this.watcherNumberFieldSpecified = value;
			}
		}

		// Token: 0x17000034 RID: 52
		// (get) Token: 0x0600009F RID: 159 RVA: 0x0000349A File Offset: 0x0000169A
		// (set) Token: 0x060000A0 RID: 160 RVA: 0x000034A2 File Offset: 0x000016A2
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string thumbImageLink
		{
			get
			{
				return this.thumbImageLinkField;
			}
			set
			{
				this.thumbImageLinkField = value;
			}
		}

		// Token: 0x0400002B RID: 43
		private string formatField;

		// Token: 0x0400002C RID: 44
		private double buyNowPriceField;

		// Token: 0x0400002D RID: 45
		private bool buyNowPriceFieldSpecified;

		// Token: 0x0400002E RID: 46
		private double highPriceField;

		// Token: 0x0400002F RID: 47
		private bool highPriceFieldSpecified;

		// Token: 0x04000030 RID: 48
		private int productCountField;

		// Token: 0x04000031 RID: 49
		private bool productCountFieldSpecified;

		// Token: 0x04000032 RID: 50
		private int soldProductCountField;

		// Token: 0x04000033 RID: 51
		private bool soldProductCountFieldSpecified;

		// Token: 0x04000034 RID: 52
		private string remainingTimeField;

		// Token: 0x04000035 RID: 53
		private int bidCountField;

		// Token: 0x04000036 RID: 54
		private bool bidCountFieldSpecified;

		// Token: 0x04000037 RID: 55
		private string highestBidderField;

		// Token: 0x04000038 RID: 56
		private int watcherNumberField;

		// Token: 0x04000039 RID: 57
		private bool watcherNumberFieldSpecified;

		// Token: 0x0400003A RID: 58
		private string thumbImageLinkField;
	}
}
