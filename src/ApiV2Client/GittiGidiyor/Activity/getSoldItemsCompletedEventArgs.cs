﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Activity
{
	// Token: 0x0200002B RID: 43
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	public class getSoldItemsCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x0600023A RID: 570 RVA: 0x000041E8 File Offset: 0x000023E8
		internal getSoldItemsCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x170000EE RID: 238
		// (get) Token: 0x0600023B RID: 571 RVA: 0x000041FB File Offset: 0x000023FB
		public activitySoldResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (activitySoldResponse)this.results[0];
			}
		}

		// Token: 0x040000F4 RID: 244
		private object[] results;
	}
}
