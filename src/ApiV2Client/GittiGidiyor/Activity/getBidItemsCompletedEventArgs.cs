﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Activity
{
	// Token: 0x02000025 RID: 37
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	public class getBidItemsCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000228 RID: 552 RVA: 0x00004170 File Offset: 0x00002370
		internal getBidItemsCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x170000EB RID: 235
		// (get) Token: 0x06000229 RID: 553 RVA: 0x00004183 File Offset: 0x00002383
		public activityBidResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (activityBidResponse)this.results[0];
			}
		}

		// Token: 0x040000F1 RID: 241
		private object[] results;
	}
}
