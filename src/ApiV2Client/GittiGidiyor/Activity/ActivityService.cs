﻿using System;

namespace ApiV2Client.GittiGidiyor.Activity
{
	// Token: 0x020001E0 RID: 480
	public class ActivityService : ServiceClient<IndividualActivityServiceService>, IActivityService, IService
	{
		// Token: 0x06001056 RID: 4182 RVA: 0x00015BA8 File Offset: 0x00013DA8
		private ActivityService()
		{
			base.configureNetworkCredentials(this.service);
		}

		// Token: 0x06001057 RID: 4183 RVA: 0x00015BC8 File Offset: 0x00013DC8
		public activitySaleResponse getActiveSales(int startOffSet, int rowCount, bool withData, string lang)
		{
			long time = Util.convertToTimestamp(DateTime.Now);
			return this.service.getActiveSales(this.authConfig.ApiKey, base.getSignature(time), time, startOffSet, rowCount, withData, lang);
		}

		// Token: 0x06001058 RID: 4184 RVA: 0x00015C04 File Offset: 0x00013E04
		public activityBidResponse getBidItems(int startOffSet, int rowCount, bool withData, string lang)
		{
			long time = Util.convertToTimestamp(DateTime.Now);
			return this.service.getBidItems(this.authConfig.ApiKey, base.getSignature(time), time, startOffSet, rowCount, withData, lang);
		}

		// Token: 0x06001059 RID: 4185 RVA: 0x00015C40 File Offset: 0x00013E40
		public activityDidntWinResponse getDidntWinItems(int startOffSet, int rowCount, bool withData, int dayInterval, string lang)
		{
			long time = Util.convertToTimestamp(DateTime.Now);
			return this.service.getDidntWinItems(this.authConfig.ApiKey, base.getSignature(time), time, startOffSet, rowCount, withData, dayInterval, lang);
		}

		// Token: 0x0600105A RID: 4186 RVA: 0x00015C7D File Offset: 0x00013E7D
		public string getServiceName()
		{
			return this.service.getServiceName();
		}

		// Token: 0x0600105B RID: 4187 RVA: 0x00015C8C File Offset: 0x00013E8C
		public activitySoldResponse getSoldItems(int startOffSet, int rowCount, bool withData, string lang)
		{
			long time = Util.convertToTimestamp(DateTime.Now);
			return this.service.getSoldItems(this.authConfig.ApiKey, base.getSignature(time), time, startOffSet, rowCount, withData, lang);
		}

		// Token: 0x0600105C RID: 4188 RVA: 0x00015CC8 File Offset: 0x00013EC8
		public activityUnsoldResponse getUnsoldItems(int startOffSet, int rowCount, bool withData, string lang)
		{
			long time = Util.convertToTimestamp(DateTime.Now);
			return this.service.getUnsoldItems(this.authConfig.ApiKey, base.getSignature(time), time, startOffSet, rowCount, withData, lang);
		}

		// Token: 0x0600105D RID: 4189 RVA: 0x00015D04 File Offset: 0x00013F04
		public activityWatchResponse getWatchItems(int startOffSet, int rowCount, bool withData, string lang)
		{
			long time = Util.convertToTimestamp(DateTime.Now);
			return this.service.getWatchItems(this.authConfig.ApiKey, base.getSignature(time), time, startOffSet, rowCount, withData, lang);
		}

		// Token: 0x0600105E RID: 4190 RVA: 0x00015D40 File Offset: 0x00013F40
		public activityWonResponse getWonItems(int startOffSet, int rowCount, bool withData, string lang)
		{
			long time = Util.convertToTimestamp(DateTime.Now);
			return this.service.getWonItems(this.authConfig.ApiKey, base.getSignature(time), time, startOffSet, rowCount, withData, lang);
		}

		// Token: 0x17000514 RID: 1300
		// (get) Token: 0x0600105F RID: 4191 RVA: 0x00015D7C File Offset: 0x00013F7C
		public static ActivityService Instance
		{
			get
			{
				if (ActivityService.instance == null)
				{
					lock (ActivityService.lockObject)
					{
						if (ActivityService.instance == null)
						{
							ActivityService.instance = new ActivityService();
						}
					}
				}
				return ActivityService.instance;
			}
		}

		// Token: 0x0400060D RID: 1549
		private static ActivityService instance;

		// Token: 0x0400060E RID: 1550
		private static object lockObject = new object();

		// Token: 0x0400060F RID: 1551
		private IndividualActivityServiceService service = new IndividualActivityServiceService();
	}
}
