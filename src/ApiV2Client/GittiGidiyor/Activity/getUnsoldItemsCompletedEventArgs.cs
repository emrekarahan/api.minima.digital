﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Activity
{
	// Token: 0x0200002D RID: 45
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	public class getUnsoldItemsCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000240 RID: 576 RVA: 0x00004210 File Offset: 0x00002410
		internal getUnsoldItemsCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x170000EF RID: 239
		// (get) Token: 0x06000241 RID: 577 RVA: 0x00004223 File Offset: 0x00002423
		public activityUnsoldResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (activityUnsoldResponse)this.results[0];
			}
		}

		// Token: 0x040000F5 RID: 245
		private object[] results;
	}
}
