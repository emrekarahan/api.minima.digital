﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Activity
{
	// Token: 0x02000027 RID: 39
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	public class getDidntWinItemsCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x0600022E RID: 558 RVA: 0x00004198 File Offset: 0x00002398
		internal getDidntWinItemsCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x170000EC RID: 236
		// (get) Token: 0x0600022F RID: 559 RVA: 0x000041AB File Offset: 0x000023AB
		public activityDidntWinResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (activityDidntWinResponse)this.results[0];
			}
		}

		// Token: 0x040000F2 RID: 242
		private object[] results;
	}
}
