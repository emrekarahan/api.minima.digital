﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Activity
{
	// Token: 0x0200001B RID: 27
	[XmlType(Namespace = "http://activity.individual.ws.listingapi.gg.com")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[Serializable]
	public class activityDidntWinType : activityBaseType
	{
		// Token: 0x17000088 RID: 136
		// (get) Token: 0x06000153 RID: 339 RVA: 0x00003A8E File Offset: 0x00001C8E
		// (set) Token: 0x06000154 RID: 340 RVA: 0x00003A96 File Offset: 0x00001C96
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string seller
		{
			get
			{
				return this.sellerField;
			}
			set
			{
				this.sellerField = value;
			}
		}

		// Token: 0x17000089 RID: 137
		// (get) Token: 0x06000155 RID: 341 RVA: 0x00003A9F File Offset: 0x00001C9F
		// (set) Token: 0x06000156 RID: 342 RVA: 0x00003AA7 File Offset: 0x00001CA7
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public double startPrice
		{
			get
			{
				return this.startPriceField;
			}
			set
			{
				this.startPriceField = value;
			}
		}

		// Token: 0x1700008A RID: 138
		// (get) Token: 0x06000157 RID: 343 RVA: 0x00003AB0 File Offset: 0x00001CB0
		// (set) Token: 0x06000158 RID: 344 RVA: 0x00003AB8 File Offset: 0x00001CB8
		[XmlIgnore]
		public bool startPriceSpecified
		{
			get
			{
				return this.startPriceFieldSpecified;
			}
			set
			{
				this.startPriceFieldSpecified = value;
			}
		}

		// Token: 0x1700008B RID: 139
		// (get) Token: 0x06000159 RID: 345 RVA: 0x00003AC1 File Offset: 0x00001CC1
		// (set) Token: 0x0600015A RID: 346 RVA: 0x00003AC9 File Offset: 0x00001CC9
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public double highPrice
		{
			get
			{
				return this.highPriceField;
			}
			set
			{
				this.highPriceField = value;
			}
		}

		// Token: 0x1700008C RID: 140
		// (get) Token: 0x0600015B RID: 347 RVA: 0x00003AD2 File Offset: 0x00001CD2
		// (set) Token: 0x0600015C RID: 348 RVA: 0x00003ADA File Offset: 0x00001CDA
		[XmlIgnore]
		public bool highPriceSpecified
		{
			get
			{
				return this.highPriceFieldSpecified;
			}
			set
			{
				this.highPriceFieldSpecified = value;
			}
		}

		// Token: 0x1700008D RID: 141
		// (get) Token: 0x0600015D RID: 349 RVA: 0x00003AE3 File Offset: 0x00001CE3
		// (set) Token: 0x0600015E RID: 350 RVA: 0x00003AEB File Offset: 0x00001CEB
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int bidCount
		{
			get
			{
				return this.bidCountField;
			}
			set
			{
				this.bidCountField = value;
			}
		}

		// Token: 0x1700008E RID: 142
		// (get) Token: 0x0600015F RID: 351 RVA: 0x00003AF4 File Offset: 0x00001CF4
		// (set) Token: 0x06000160 RID: 352 RVA: 0x00003AFC File Offset: 0x00001CFC
		[XmlIgnore]
		public bool bidCountSpecified
		{
			get
			{
				return this.bidCountFieldSpecified;
			}
			set
			{
				this.bidCountFieldSpecified = value;
			}
		}

		// Token: 0x1700008F RID: 143
		// (get) Token: 0x06000161 RID: 353 RVA: 0x00003B05 File Offset: 0x00001D05
		// (set) Token: 0x06000162 RID: 354 RVA: 0x00003B0D File Offset: 0x00001D0D
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int productCount
		{
			get
			{
				return this.productCountField;
			}
			set
			{
				this.productCountField = value;
			}
		}

		// Token: 0x17000090 RID: 144
		// (get) Token: 0x06000163 RID: 355 RVA: 0x00003B16 File Offset: 0x00001D16
		// (set) Token: 0x06000164 RID: 356 RVA: 0x00003B1E File Offset: 0x00001D1E
		[XmlIgnore]
		public bool productCountSpecified
		{
			get
			{
				return this.productCountFieldSpecified;
			}
			set
			{
				this.productCountFieldSpecified = value;
			}
		}

		// Token: 0x17000091 RID: 145
		// (get) Token: 0x06000165 RID: 357 RVA: 0x00003B27 File Offset: 0x00001D27
		// (set) Token: 0x06000166 RID: 358 RVA: 0x00003B2F File Offset: 0x00001D2F
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int soldItemCount
		{
			get
			{
				return this.soldItemCountField;
			}
			set
			{
				this.soldItemCountField = value;
			}
		}

		// Token: 0x17000092 RID: 146
		// (get) Token: 0x06000167 RID: 359 RVA: 0x00003B38 File Offset: 0x00001D38
		// (set) Token: 0x06000168 RID: 360 RVA: 0x00003B40 File Offset: 0x00001D40
		[XmlIgnore]
		public bool soldItemCountSpecified
		{
			get
			{
				return this.soldItemCountFieldSpecified;
			}
			set
			{
				this.soldItemCountFieldSpecified = value;
			}
		}

		// Token: 0x17000093 RID: 147
		// (get) Token: 0x06000169 RID: 361 RVA: 0x00003B49 File Offset: 0x00001D49
		// (set) Token: 0x0600016A RID: 362 RVA: 0x00003B51 File Offset: 0x00001D51
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string endDate
		{
			get
			{
				return this.endDateField;
			}
			set
			{
				this.endDateField = value;
			}
		}

		// Token: 0x17000094 RID: 148
		// (get) Token: 0x0600016B RID: 363 RVA: 0x00003B5A File Offset: 0x00001D5A
		// (set) Token: 0x0600016C RID: 364 RVA: 0x00003B62 File Offset: 0x00001D62
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string thumbImageLink
		{
			get
			{
				return this.thumbImageLinkField;
			}
			set
			{
				this.thumbImageLinkField = value;
			}
		}

		// Token: 0x0400008E RID: 142
		private string sellerField;

		// Token: 0x0400008F RID: 143
		private double startPriceField;

		// Token: 0x04000090 RID: 144
		private bool startPriceFieldSpecified;

		// Token: 0x04000091 RID: 145
		private double highPriceField;

		// Token: 0x04000092 RID: 146
		private bool highPriceFieldSpecified;

		// Token: 0x04000093 RID: 147
		private int bidCountField;

		// Token: 0x04000094 RID: 148
		private bool bidCountFieldSpecified;

		// Token: 0x04000095 RID: 149
		private int productCountField;

		// Token: 0x04000096 RID: 150
		private bool productCountFieldSpecified;

		// Token: 0x04000097 RID: 151
		private int soldItemCountField;

		// Token: 0x04000098 RID: 152
		private bool soldItemCountFieldSpecified;

		// Token: 0x04000099 RID: 153
		private string endDateField;

		// Token: 0x0400009A RID: 154
		private string thumbImageLinkField;
	}
}
