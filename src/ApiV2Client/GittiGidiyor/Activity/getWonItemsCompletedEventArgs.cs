﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Activity
{
	// Token: 0x02000031 RID: 49
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	public class getWonItemsCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x0600024C RID: 588 RVA: 0x00004260 File Offset: 0x00002460
		internal getWonItemsCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x170000F1 RID: 241
		// (get) Token: 0x0600024D RID: 589 RVA: 0x00004273 File Offset: 0x00002473
		public activityWonResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (activityWonResponse)this.results[0];
			}
		}

		// Token: 0x040000F7 RID: 247
		private object[] results;
	}
}
