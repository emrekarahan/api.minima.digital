﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Address
{
	// Token: 0x02000047 RID: 71
	[DesignerCategory("code")]
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	[DebuggerStepThrough]
	public class makeAddressAsDefaultCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x060002F8 RID: 760 RVA: 0x0000522B File Offset: 0x0000342B
		internal makeAddressAsDefaultCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x1700011C RID: 284
		// (get) Token: 0x060002F9 RID: 761 RVA: 0x0000523E File Offset: 0x0000343E
		public addressServiceResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (addressServiceResponse)this.results[0];
			}
		}

		// Token: 0x04000131 RID: 305
		private object[] results;
	}
}
