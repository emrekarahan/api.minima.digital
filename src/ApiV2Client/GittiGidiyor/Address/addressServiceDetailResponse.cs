﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Address
{
	// Token: 0x02000038 RID: 56
	[XmlType(Namespace = "http://application.individual.ws.listingapi.gg.com")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[Serializable]
	public class addressServiceDetailResponse : baseResponse
	{
		// Token: 0x17000110 RID: 272
		// (get) Token: 0x060002C2 RID: 706 RVA: 0x000050C5 File Offset: 0x000032C5
		// (set) Token: 0x060002C3 RID: 707 RVA: 0x000050CD File Offset: 0x000032CD
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public addressType address
		{
			get
			{
				return this.addressField;
			}
			set
			{
				this.addressField = value;
			}
		}

		// Token: 0x04000125 RID: 293
		private addressType addressField;
	}
}
