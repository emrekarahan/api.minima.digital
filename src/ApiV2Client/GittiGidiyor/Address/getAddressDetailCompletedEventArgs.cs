﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Address
{
	// Token: 0x0200003F RID: 63
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	[DesignerCategory("code")]
	[DebuggerStepThrough]
	public class getAddressDetailCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x060002E0 RID: 736 RVA: 0x0000518B File Offset: 0x0000338B
		internal getAddressDetailCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x17000118 RID: 280
		// (get) Token: 0x060002E1 RID: 737 RVA: 0x0000519E File Offset: 0x0000339E
		public addressServiceDetailResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (addressServiceDetailResponse)this.results[0];
			}
		}

		// Token: 0x0400012D RID: 301
		private object[] results;
	}
}
