﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Address
{
	// Token: 0x02000043 RID: 67
	[DesignerCategory("code")]
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	[DebuggerStepThrough]
	public class getDefaultAddressDetailCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x060002EC RID: 748 RVA: 0x000051DB File Offset: 0x000033DB
		internal getDefaultAddressDetailCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x1700011A RID: 282
		// (get) Token: 0x060002ED RID: 749 RVA: 0x000051EE File Offset: 0x000033EE
		public addressServiceDetailResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (addressServiceDetailResponse)this.results[0];
			}
		}

		// Token: 0x0400012F RID: 303
		private object[] results;
	}
}
