﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Address
{
	// Token: 0x02000037 RID: 55
	[XmlType(Namespace = "http://application.individual.ws.listingapi.gg.com")]
	[DesignerCategory("code")]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[DebuggerStepThrough]
	[Serializable]
	public class addressServiceResponse : baseResponse
	{
		// Token: 0x1700010D RID: 269
		// (get) Token: 0x060002BB RID: 699 RVA: 0x0000508A File Offset: 0x0000328A
		// (set) Token: 0x060002BC RID: 700 RVA: 0x00005092 File Offset: 0x00003292
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int addressId
		{
			get
			{
				return this.addressIdField;
			}
			set
			{
				this.addressIdField = value;
			}
		}

		// Token: 0x1700010E RID: 270
		// (get) Token: 0x060002BD RID: 701 RVA: 0x0000509B File Offset: 0x0000329B
		// (set) Token: 0x060002BE RID: 702 RVA: 0x000050A3 File Offset: 0x000032A3
		[XmlIgnore]
		public bool addressIdSpecified
		{
			get
			{
				return this.addressIdFieldSpecified;
			}
			set
			{
				this.addressIdFieldSpecified = value;
			}
		}

		// Token: 0x1700010F RID: 271
		// (get) Token: 0x060002BF RID: 703 RVA: 0x000050AC File Offset: 0x000032AC
		// (set) Token: 0x060002C0 RID: 704 RVA: 0x000050B4 File Offset: 0x000032B4
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string result
		{
			get
			{
				return this.resultField;
			}
			set
			{
				this.resultField = value;
			}
		}

		// Token: 0x04000122 RID: 290
		private int addressIdField;

		// Token: 0x04000123 RID: 291
		private bool addressIdFieldSpecified;

		// Token: 0x04000124 RID: 292
		private string resultField;
	}
}
