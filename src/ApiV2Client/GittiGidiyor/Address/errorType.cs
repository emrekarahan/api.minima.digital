﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Address
{
	// Token: 0x02000035 RID: 53
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[DesignerCategory("code")]
	[XmlType(Namespace = "http://application.individual.ws.listingapi.gg.com")]
	[DebuggerStepThrough]
	[Serializable]
	public class errorType
	{
		// Token: 0x17000105 RID: 261
		// (get) Token: 0x060002A9 RID: 681 RVA: 0x00004FF2 File Offset: 0x000031F2
		// (set) Token: 0x060002AA RID: 682 RVA: 0x00004FFA File Offset: 0x000031FA
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string errorId
		{
			get
			{
				return this.errorIdField;
			}
			set
			{
				this.errorIdField = value;
			}
		}

		// Token: 0x17000106 RID: 262
		// (get) Token: 0x060002AB RID: 683 RVA: 0x00005003 File Offset: 0x00003203
		// (set) Token: 0x060002AC RID: 684 RVA: 0x0000500B File Offset: 0x0000320B
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string errorCode
		{
			get
			{
				return this.errorCodeField;
			}
			set
			{
				this.errorCodeField = value;
			}
		}

		// Token: 0x17000107 RID: 263
		// (get) Token: 0x060002AD RID: 685 RVA: 0x00005014 File Offset: 0x00003214
		// (set) Token: 0x060002AE RID: 686 RVA: 0x0000501C File Offset: 0x0000321C
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string message
		{
			get
			{
				return this.messageField;
			}
			set
			{
				this.messageField = value;
			}
		}

		// Token: 0x17000108 RID: 264
		// (get) Token: 0x060002AF RID: 687 RVA: 0x00005025 File Offset: 0x00003225
		// (set) Token: 0x060002B0 RID: 688 RVA: 0x0000502D File Offset: 0x0000322D
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string viewMessage
		{
			get
			{
				return this.viewMessageField;
			}
			set
			{
				this.viewMessageField = value;
			}
		}

		// Token: 0x0400011A RID: 282
		private string errorIdField;

		// Token: 0x0400011B RID: 283
		private string errorCodeField;

		// Token: 0x0400011C RID: 284
		private string messageField;

		// Token: 0x0400011D RID: 285
		private string viewMessageField;
	}
}
