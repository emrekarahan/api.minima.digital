﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Address
{
	// Token: 0x02000049 RID: 73
	[DebuggerStepThrough]
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	[DesignerCategory("code")]
	public class updateAddressCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x060002FE RID: 766 RVA: 0x00005253 File Offset: 0x00003453
		internal updateAddressCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x1700011D RID: 285
		// (get) Token: 0x060002FF RID: 767 RVA: 0x00005266 File Offset: 0x00003466
		public addressServiceResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (addressServiceResponse)this.results[0];
			}
		}

		// Token: 0x04000132 RID: 306
		private object[] results;
	}
}
