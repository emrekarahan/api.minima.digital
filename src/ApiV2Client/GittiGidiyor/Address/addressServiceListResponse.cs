﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Address
{
	// Token: 0x02000039 RID: 57
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[XmlType(Namespace = "http://application.individual.ws.listingapi.gg.com")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[Serializable]
	public class addressServiceListResponse : baseResponse
	{
		// Token: 0x17000111 RID: 273
		// (get) Token: 0x060002C5 RID: 709 RVA: 0x000050DE File Offset: 0x000032DE
		// (set) Token: 0x060002C6 RID: 710 RVA: 0x000050E6 File Offset: 0x000032E6
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int totalCount
		{
			get
			{
				return this.totalCountField;
			}
			set
			{
				this.totalCountField = value;
			}
		}

		// Token: 0x17000112 RID: 274
		// (get) Token: 0x060002C7 RID: 711 RVA: 0x000050EF File Offset: 0x000032EF
		// (set) Token: 0x060002C8 RID: 712 RVA: 0x000050F7 File Offset: 0x000032F7
		[XmlIgnore]
		public bool totalCountSpecified
		{
			get
			{
				return this.totalCountFieldSpecified;
			}
			set
			{
				this.totalCountFieldSpecified = value;
			}
		}

		// Token: 0x17000113 RID: 275
		// (get) Token: 0x060002C9 RID: 713 RVA: 0x00005100 File Offset: 0x00003300
		// (set) Token: 0x060002CA RID: 714 RVA: 0x00005108 File Offset: 0x00003308
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int count
		{
			get
			{
				return this.countField;
			}
			set
			{
				this.countField = value;
			}
		}

		// Token: 0x17000114 RID: 276
		// (get) Token: 0x060002CB RID: 715 RVA: 0x00005111 File Offset: 0x00003311
		// (set) Token: 0x060002CC RID: 716 RVA: 0x00005119 File Offset: 0x00003319
		[XmlIgnore]
		public bool countSpecified
		{
			get
			{
				return this.countFieldSpecified;
			}
			set
			{
				this.countFieldSpecified = value;
			}
		}

		// Token: 0x17000115 RID: 277
		// (get) Token: 0x060002CD RID: 717 RVA: 0x00005122 File Offset: 0x00003322
		// (set) Token: 0x060002CE RID: 718 RVA: 0x0000512A File Offset: 0x0000332A
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		[XmlArrayItem("address", Form = XmlSchemaForm.Unqualified)]
		public addressType[] addresses
		{
			get
			{
				return this.addressesField;
			}
			set
			{
				this.addressesField = value;
			}
		}

		// Token: 0x04000126 RID: 294
		private int totalCountField;

		// Token: 0x04000127 RID: 295
		private bool totalCountFieldSpecified;

		// Token: 0x04000128 RID: 296
		private int countField;

		// Token: 0x04000129 RID: 297
		private bool countFieldSpecified;

		// Token: 0x0400012A RID: 298
		private addressType[] addressesField;
	}
}
