﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Address
{
	// Token: 0x02000041 RID: 65
	[DesignerCategory("code")]
	[DebuggerStepThrough]
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	public class getAddressListCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x060002E6 RID: 742 RVA: 0x000051B3 File Offset: 0x000033B3
		internal getAddressListCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x17000119 RID: 281
		// (get) Token: 0x060002E7 RID: 743 RVA: 0x000051C6 File Offset: 0x000033C6
		public addressServiceListResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (addressServiceListResponse)this.results[0];
			}
		}

		// Token: 0x0400012E RID: 302
		private object[] results;
	}
}
