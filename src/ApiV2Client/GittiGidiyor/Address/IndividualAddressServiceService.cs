﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading;
using System.Web.Services;
using System.Web.Services.Description;
using System.Web.Services.Protocols;
using System.Xml.Serialization;
using ApiV2Client.Properties;

namespace ApiV2Client.GittiGidiyor.Address
{
	// Token: 0x02000032 RID: 50
	[XmlInclude(typeof(baseResponse))]
	[DebuggerStepThrough]
	[WebServiceBinding(Name = "IndividualAddressServiceBinding", Namespace = "http://application.individual.ws.listingapi.gg.com")]
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	[DesignerCategory("code")]
	public class IndividualAddressServiceService : SoapHttpClientProtocol
	{
		// Token: 0x0600024E RID: 590 RVA: 0x00004288 File Offset: 0x00002488
		public IndividualAddressServiceService()
		{
			this.Url = Settings.Default.ApiV2Client_GittiGidiyor_Address_IndividualAddressServiceService;
			if (this.IsLocalFileSystemWebService(this.Url))
			{
				this.UseDefaultCredentials = true;
				this.useDefaultCredentialsSetExplicitly = false;
				return;
			}
			this.useDefaultCredentialsSetExplicitly = true;
		}

		// Token: 0x170000F2 RID: 242
		// (get) Token: 0x0600024F RID: 591 RVA: 0x000042C4 File Offset: 0x000024C4
		// (set) Token: 0x06000250 RID: 592 RVA: 0x000042CC File Offset: 0x000024CC
		public new string Url
		{
			get
			{
				return base.Url;
			}
			set
			{
				if (this.IsLocalFileSystemWebService(base.Url) && !this.useDefaultCredentialsSetExplicitly && !this.IsLocalFileSystemWebService(value))
				{
					base.UseDefaultCredentials = false;
				}
				base.Url = value;
			}
		}

		// Token: 0x170000F3 RID: 243
		// (get) Token: 0x06000251 RID: 593 RVA: 0x000042FB File Offset: 0x000024FB
		// (set) Token: 0x06000252 RID: 594 RVA: 0x00004303 File Offset: 0x00002503
		public new bool UseDefaultCredentials
		{
			get
			{
				return base.UseDefaultCredentials;
			}
			set
			{
				base.UseDefaultCredentials = value;
				this.useDefaultCredentialsSetExplicitly = true;
			}
		}

		// Token: 0x14000009 RID: 9
		// (add) Token: 0x06000253 RID: 595 RVA: 0x00004314 File Offset: 0x00002514
		// (remove) Token: 0x06000254 RID: 596 RVA: 0x0000434C File Offset: 0x0000254C
		public event createNewAddressCompletedEventHandler createNewAddressCompleted;

		// Token: 0x1400000A RID: 10
		// (add) Token: 0x06000255 RID: 597 RVA: 0x00004384 File Offset: 0x00002584
		// (remove) Token: 0x06000256 RID: 598 RVA: 0x000043BC File Offset: 0x000025BC
		public event deleteAddressCompletedEventHandler deleteAddressCompleted;

		// Token: 0x1400000B RID: 11
		// (add) Token: 0x06000257 RID: 599 RVA: 0x000043F4 File Offset: 0x000025F4
		// (remove) Token: 0x06000258 RID: 600 RVA: 0x0000442C File Offset: 0x0000262C
		public event getAddressDetailCompletedEventHandler getAddressDetailCompleted;

		// Token: 0x1400000C RID: 12
		// (add) Token: 0x06000259 RID: 601 RVA: 0x00004464 File Offset: 0x00002664
		// (remove) Token: 0x0600025A RID: 602 RVA: 0x0000449C File Offset: 0x0000269C
		public event getAddressListCompletedEventHandler getAddressListCompleted;

		// Token: 0x1400000D RID: 13
		// (add) Token: 0x0600025B RID: 603 RVA: 0x000044D4 File Offset: 0x000026D4
		// (remove) Token: 0x0600025C RID: 604 RVA: 0x0000450C File Offset: 0x0000270C
		public event getDefaultAddressDetailCompletedEventHandler getDefaultAddressDetailCompleted;

		// Token: 0x1400000E RID: 14
		// (add) Token: 0x0600025D RID: 605 RVA: 0x00004544 File Offset: 0x00002744
		// (remove) Token: 0x0600025E RID: 606 RVA: 0x0000457C File Offset: 0x0000277C
		public event getServiceNameCompletedEventHandler getServiceNameCompleted;

		// Token: 0x1400000F RID: 15
		// (add) Token: 0x0600025F RID: 607 RVA: 0x000045B4 File Offset: 0x000027B4
		// (remove) Token: 0x06000260 RID: 608 RVA: 0x000045EC File Offset: 0x000027EC
		public event makeAddressAsDefaultCompletedEventHandler makeAddressAsDefaultCompleted;

		// Token: 0x14000010 RID: 16
		// (add) Token: 0x06000261 RID: 609 RVA: 0x00004624 File Offset: 0x00002824
		// (remove) Token: 0x06000262 RID: 610 RVA: 0x0000465C File Offset: 0x0000285C
		public event updateAddressCompletedEventHandler updateAddressCompleted;

		// Token: 0x06000263 RID: 611 RVA: 0x00004694 File Offset: 0x00002894
		[SoapRpcMethod("", RequestNamespace = "http://application.individual.ws.listingapi.gg.com", ResponseNamespace = "http://application.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public addressServiceResponse createNewAddress(string apiKey, string sign, long time, addressType address, string lang)
		{
			object[] array = base.Invoke("createNewAddress", new object[]
			{
				apiKey,
				sign,
				time,
				address,
				lang
			});
			return (addressServiceResponse)array[0];
		}

		// Token: 0x06000264 RID: 612 RVA: 0x000046D8 File Offset: 0x000028D8
		public void createNewAddressAsync(string apiKey, string sign, long time, addressType address, string lang)
		{
			this.createNewAddressAsync(apiKey, sign, time, address, lang, null);
		}

		// Token: 0x06000265 RID: 613 RVA: 0x000046E8 File Offset: 0x000028E8
		public void createNewAddressAsync(string apiKey, string sign, long time, addressType address, string lang, object userState)
		{
			if (this.createNewAddressOperationCompleted == null)
			{
				this.createNewAddressOperationCompleted = new SendOrPostCallback(this.OncreateNewAddressOperationCompleted);
			}
			base.InvokeAsync("createNewAddress", new object[]
			{
				apiKey,
				sign,
				time,
				address,
				lang
			}, this.createNewAddressOperationCompleted, userState);
		}

		// Token: 0x06000266 RID: 614 RVA: 0x00004748 File Offset: 0x00002948
		private void OncreateNewAddressOperationCompleted(object arg)
		{
			if (this.createNewAddressCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.createNewAddressCompleted(this, new createNewAddressCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x06000267 RID: 615 RVA: 0x00004790 File Offset: 0x00002990
		[SoapRpcMethod("", RequestNamespace = "http://application.individual.ws.listingapi.gg.com", ResponseNamespace = "http://application.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public addressServiceResponse deleteAddress(string apiKey, string sign, long time, int addressId, string lang)
		{
			object[] array = base.Invoke("deleteAddress", new object[]
			{
				apiKey,
				sign,
				time,
				addressId,
				lang
			});
			return (addressServiceResponse)array[0];
		}

		// Token: 0x06000268 RID: 616 RVA: 0x000047D9 File Offset: 0x000029D9
		public void deleteAddressAsync(string apiKey, string sign, long time, int addressId, string lang)
		{
			this.deleteAddressAsync(apiKey, sign, time, addressId, lang, null);
		}

		// Token: 0x06000269 RID: 617 RVA: 0x000047EC File Offset: 0x000029EC
		public void deleteAddressAsync(string apiKey, string sign, long time, int addressId, string lang, object userState)
		{
			if (this.deleteAddressOperationCompleted == null)
			{
				this.deleteAddressOperationCompleted = new SendOrPostCallback(this.OndeleteAddressOperationCompleted);
			}
			base.InvokeAsync("deleteAddress", new object[]
			{
				apiKey,
				sign,
				time,
				addressId,
				lang
			}, this.deleteAddressOperationCompleted, userState);
		}

		// Token: 0x0600026A RID: 618 RVA: 0x00004850 File Offset: 0x00002A50
		private void OndeleteAddressOperationCompleted(object arg)
		{
			if (this.deleteAddressCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.deleteAddressCompleted(this, new deleteAddressCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x0600026B RID: 619 RVA: 0x00004898 File Offset: 0x00002A98
		[SoapRpcMethod("", RequestNamespace = "http://application.individual.ws.listingapi.gg.com", ResponseNamespace = "http://application.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public addressServiceDetailResponse getAddressDetail(string apiKey, string sign, long time, int addressId, string lang)
		{
			object[] array = base.Invoke("getAddressDetail", new object[]
			{
				apiKey,
				sign,
				time,
				addressId,
				lang
			});
			return (addressServiceDetailResponse)array[0];
		}

		// Token: 0x0600026C RID: 620 RVA: 0x000048E1 File Offset: 0x00002AE1
		public void getAddressDetailAsync(string apiKey, string sign, long time, int addressId, string lang)
		{
			this.getAddressDetailAsync(apiKey, sign, time, addressId, lang, null);
		}

		// Token: 0x0600026D RID: 621 RVA: 0x000048F4 File Offset: 0x00002AF4
		public void getAddressDetailAsync(string apiKey, string sign, long time, int addressId, string lang, object userState)
		{
			if (this.getAddressDetailOperationCompleted == null)
			{
				this.getAddressDetailOperationCompleted = new SendOrPostCallback(this.OngetAddressDetailOperationCompleted);
			}
			base.InvokeAsync("getAddressDetail", new object[]
			{
				apiKey,
				sign,
				time,
				addressId,
				lang
			}, this.getAddressDetailOperationCompleted, userState);
		}

		// Token: 0x0600026E RID: 622 RVA: 0x00004958 File Offset: 0x00002B58
		private void OngetAddressDetailOperationCompleted(object arg)
		{
			if (this.getAddressDetailCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getAddressDetailCompleted(this, new getAddressDetailCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x0600026F RID: 623 RVA: 0x000049A0 File Offset: 0x00002BA0
		[SoapRpcMethod("", RequestNamespace = "http://application.individual.ws.listingapi.gg.com", ResponseNamespace = "http://application.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public addressServiceListResponse getAddressList(string apiKey, string sign, long time, int startOffSet, int rowCount, string lang)
		{
			object[] array = base.Invoke("getAddressList", new object[]
			{
				apiKey,
				sign,
				time,
				startOffSet,
				rowCount,
				lang
			});
			return (addressServiceListResponse)array[0];
		}

		// Token: 0x06000270 RID: 624 RVA: 0x000049F3 File Offset: 0x00002BF3
		public void getAddressListAsync(string apiKey, string sign, long time, int startOffSet, int rowCount, string lang)
		{
			this.getAddressListAsync(apiKey, sign, time, startOffSet, rowCount, lang, null);
		}

		// Token: 0x06000271 RID: 625 RVA: 0x00004A08 File Offset: 0x00002C08
		public void getAddressListAsync(string apiKey, string sign, long time, int startOffSet, int rowCount, string lang, object userState)
		{
			if (this.getAddressListOperationCompleted == null)
			{
				this.getAddressListOperationCompleted = new SendOrPostCallback(this.OngetAddressListOperationCompleted);
			}
			base.InvokeAsync("getAddressList", new object[]
			{
				apiKey,
				sign,
				time,
				startOffSet,
				rowCount,
				lang
			}, this.getAddressListOperationCompleted, userState);
		}

		// Token: 0x06000272 RID: 626 RVA: 0x00004A74 File Offset: 0x00002C74
		private void OngetAddressListOperationCompleted(object arg)
		{
			if (this.getAddressListCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getAddressListCompleted(this, new getAddressListCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x06000273 RID: 627 RVA: 0x00004ABC File Offset: 0x00002CBC
		[SoapRpcMethod("", RequestNamespace = "http://application.individual.ws.listingapi.gg.com", ResponseNamespace = "http://application.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public addressServiceDetailResponse getDefaultAddressDetail(string apiKey, string sign, long time, string lang)
		{
			object[] array = base.Invoke("getDefaultAddressDetail", new object[]
			{
				apiKey,
				sign,
				time,
				lang
			});
			return (addressServiceDetailResponse)array[0];
		}

		// Token: 0x06000274 RID: 628 RVA: 0x00004AFB File Offset: 0x00002CFB
		public void getDefaultAddressDetailAsync(string apiKey, string sign, long time, string lang)
		{
			this.getDefaultAddressDetailAsync(apiKey, sign, time, lang, null);
		}

		// Token: 0x06000275 RID: 629 RVA: 0x00004B0C File Offset: 0x00002D0C
		public void getDefaultAddressDetailAsync(string apiKey, string sign, long time, string lang, object userState)
		{
			if (this.getDefaultAddressDetailOperationCompleted == null)
			{
				this.getDefaultAddressDetailOperationCompleted = new SendOrPostCallback(this.OngetDefaultAddressDetailOperationCompleted);
			}
			base.InvokeAsync("getDefaultAddressDetail", new object[]
			{
				apiKey,
				sign,
				time,
				lang
			}, this.getDefaultAddressDetailOperationCompleted, userState);
		}

		// Token: 0x06000276 RID: 630 RVA: 0x00004B64 File Offset: 0x00002D64
		private void OngetDefaultAddressDetailOperationCompleted(object arg)
		{
			if (this.getDefaultAddressDetailCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getDefaultAddressDetailCompleted(this, new getDefaultAddressDetailCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x06000277 RID: 631 RVA: 0x00004BAC File Offset: 0x00002DAC
		[SoapRpcMethod("", RequestNamespace = "http://application.individual.ws.listingapi.gg.com", ResponseNamespace = "http://application.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public string getServiceName()
		{
			object[] array = base.Invoke("getServiceName", new object[0]);
			return (string)array[0];
		}

		// Token: 0x06000278 RID: 632 RVA: 0x00004BD3 File Offset: 0x00002DD3
		public void getServiceNameAsync()
		{
			this.getServiceNameAsync(null);
		}

		// Token: 0x06000279 RID: 633 RVA: 0x00004BDC File Offset: 0x00002DDC
		public void getServiceNameAsync(object userState)
		{
			if (this.getServiceNameOperationCompleted == null)
			{
				this.getServiceNameOperationCompleted = new SendOrPostCallback(this.OngetServiceNameOperationCompleted);
			}
			base.InvokeAsync("getServiceName", new object[0], this.getServiceNameOperationCompleted, userState);
		}

		// Token: 0x0600027A RID: 634 RVA: 0x00004C10 File Offset: 0x00002E10
		private void OngetServiceNameOperationCompleted(object arg)
		{
			if (this.getServiceNameCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getServiceNameCompleted(this, new getServiceNameCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x0600027B RID: 635 RVA: 0x00004C58 File Offset: 0x00002E58
		[SoapRpcMethod("", RequestNamespace = "http://application.individual.ws.listingapi.gg.com", ResponseNamespace = "http://application.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public addressServiceResponse makeAddressAsDefault(string apiKey, string sign, long time, int addressId, string lang)
		{
			object[] array = base.Invoke("makeAddressAsDefault", new object[]
			{
				apiKey,
				sign,
				time,
				addressId,
				lang
			});
			return (addressServiceResponse)array[0];
		}

		// Token: 0x0600027C RID: 636 RVA: 0x00004CA1 File Offset: 0x00002EA1
		public void makeAddressAsDefaultAsync(string apiKey, string sign, long time, int addressId, string lang)
		{
			this.makeAddressAsDefaultAsync(apiKey, sign, time, addressId, lang, null);
		}

		// Token: 0x0600027D RID: 637 RVA: 0x00004CB4 File Offset: 0x00002EB4
		public void makeAddressAsDefaultAsync(string apiKey, string sign, long time, int addressId, string lang, object userState)
		{
			if (this.makeAddressAsDefaultOperationCompleted == null)
			{
				this.makeAddressAsDefaultOperationCompleted = new SendOrPostCallback(this.OnmakeAddressAsDefaultOperationCompleted);
			}
			base.InvokeAsync("makeAddressAsDefault", new object[]
			{
				apiKey,
				sign,
				time,
				addressId,
				lang
			}, this.makeAddressAsDefaultOperationCompleted, userState);
		}

		// Token: 0x0600027E RID: 638 RVA: 0x00004D18 File Offset: 0x00002F18
		private void OnmakeAddressAsDefaultOperationCompleted(object arg)
		{
			if (this.makeAddressAsDefaultCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.makeAddressAsDefaultCompleted(this, new makeAddressAsDefaultCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x0600027F RID: 639 RVA: 0x00004D60 File Offset: 0x00002F60
		[SoapRpcMethod("", RequestNamespace = "http://application.individual.ws.listingapi.gg.com", ResponseNamespace = "http://application.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public addressServiceResponse updateAddress(string apiKey, string sign, long time, int addressId, addressType address, string lang)
		{
			object[] array = base.Invoke("updateAddress", new object[]
			{
				apiKey,
				sign,
				time,
				addressId,
				address,
				lang
			});
			return (addressServiceResponse)array[0];
		}

		// Token: 0x06000280 RID: 640 RVA: 0x00004DAE File Offset: 0x00002FAE
		public void updateAddressAsync(string apiKey, string sign, long time, int addressId, addressType address, string lang)
		{
			this.updateAddressAsync(apiKey, sign, time, addressId, address, lang, null);
		}

		// Token: 0x06000281 RID: 641 RVA: 0x00004DC0 File Offset: 0x00002FC0
		public void updateAddressAsync(string apiKey, string sign, long time, int addressId, addressType address, string lang, object userState)
		{
			if (this.updateAddressOperationCompleted == null)
			{
				this.updateAddressOperationCompleted = new SendOrPostCallback(this.OnupdateAddressOperationCompleted);
			}
			base.InvokeAsync("updateAddress", new object[]
			{
				apiKey,
				sign,
				time,
				addressId,
				address,
				lang
			}, this.updateAddressOperationCompleted, userState);
		}

		// Token: 0x06000282 RID: 642 RVA: 0x00004E28 File Offset: 0x00003028
		private void OnupdateAddressOperationCompleted(object arg)
		{
			if (this.updateAddressCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.updateAddressCompleted(this, new updateAddressCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x06000283 RID: 643 RVA: 0x00004E6D File Offset: 0x0000306D
		public new void CancelAsync(object userState)
		{
			base.CancelAsync(userState);
		}

		// Token: 0x06000284 RID: 644 RVA: 0x00004E78 File Offset: 0x00003078
		private bool IsLocalFileSystemWebService(string url)
		{
			if (url == null || url == string.Empty)
			{
				return false;
			}
			Uri uri = new Uri(url);
			return uri.Port >= 1024 && string.Compare(uri.Host, "localHost", StringComparison.OrdinalIgnoreCase) == 0;
		}

		// Token: 0x040000F8 RID: 248
		private SendOrPostCallback createNewAddressOperationCompleted;

		// Token: 0x040000F9 RID: 249
		private SendOrPostCallback deleteAddressOperationCompleted;

		// Token: 0x040000FA RID: 250
		private SendOrPostCallback getAddressDetailOperationCompleted;

		// Token: 0x040000FB RID: 251
		private SendOrPostCallback getAddressListOperationCompleted;

		// Token: 0x040000FC RID: 252
		private SendOrPostCallback getDefaultAddressDetailOperationCompleted;

		// Token: 0x040000FD RID: 253
		private SendOrPostCallback getServiceNameOperationCompleted;

		// Token: 0x040000FE RID: 254
		private SendOrPostCallback makeAddressAsDefaultOperationCompleted;

		// Token: 0x040000FF RID: 255
		private SendOrPostCallback updateAddressOperationCompleted;

		// Token: 0x04000100 RID: 256
		private bool useDefaultCredentialsSetExplicitly;
	}
}
