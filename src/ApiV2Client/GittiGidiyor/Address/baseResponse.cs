﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Address
{
	// Token: 0x02000036 RID: 54
	[XmlInclude(typeof(addressServiceDetailResponse))]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[DebuggerStepThrough]
	[XmlInclude(typeof(addressServiceResponse))]
	[XmlType(Namespace = "http://application.individual.ws.listingapi.gg.com")]
	[DesignerCategory("code")]
	[XmlInclude(typeof(addressServiceListResponse))]
	[Serializable]
	public class baseResponse
	{
		// Token: 0x17000109 RID: 265
		// (get) Token: 0x060002B2 RID: 690 RVA: 0x0000503E File Offset: 0x0000323E
		// (set) Token: 0x060002B3 RID: 691 RVA: 0x00005046 File Offset: 0x00003246
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string ackCode
		{
			get
			{
				return this.ackCodeField;
			}
			set
			{
				this.ackCodeField = value;
			}
		}

		// Token: 0x1700010A RID: 266
		// (get) Token: 0x060002B4 RID: 692 RVA: 0x0000504F File Offset: 0x0000324F
		// (set) Token: 0x060002B5 RID: 693 RVA: 0x00005057 File Offset: 0x00003257
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string responseTime
		{
			get
			{
				return this.responseTimeField;
			}
			set
			{
				this.responseTimeField = value;
			}
		}

		// Token: 0x1700010B RID: 267
		// (get) Token: 0x060002B6 RID: 694 RVA: 0x00005060 File Offset: 0x00003260
		// (set) Token: 0x060002B7 RID: 695 RVA: 0x00005068 File Offset: 0x00003268
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public errorType error
		{
			get
			{
				return this.errorField;
			}
			set
			{
				this.errorField = value;
			}
		}

		// Token: 0x1700010C RID: 268
		// (get) Token: 0x060002B8 RID: 696 RVA: 0x00005071 File Offset: 0x00003271
		// (set) Token: 0x060002B9 RID: 697 RVA: 0x00005079 File Offset: 0x00003279
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string timeElapsed
		{
			get
			{
				return this.timeElapsedField;
			}
			set
			{
				this.timeElapsedField = value;
			}
		}

		// Token: 0x0400011E RID: 286
		private string ackCodeField;

		// Token: 0x0400011F RID: 287
		private string responseTimeField;

		// Token: 0x04000120 RID: 288
		private errorType errorField;

		// Token: 0x04000121 RID: 289
		private string timeElapsedField;
	}
}
