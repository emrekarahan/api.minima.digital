﻿using System;

namespace ApiV2Client.GittiGidiyor.Address
{
	// Token: 0x020001E2 RID: 482
	public class AddressService : ServiceClient<IndividualAddressServiceService>, IAddressService, IService
	{
		// Token: 0x06001064 RID: 4196 RVA: 0x00015DD8 File Offset: 0x00013FD8
		private AddressService()
		{
			base.configureNetworkCredentials(this.service);
		}

		// Token: 0x06001065 RID: 4197 RVA: 0x00015DF8 File Offset: 0x00013FF8
		public addressServiceDetailResponse getAddressDetail(int addressId, string lang)
		{
			long time = Util.convertToTimestamp(DateTime.Now);
			return this.service.getAddressDetail(this.authConfig.ApiKey, base.getSignature(time), time, addressId, lang);
		}

		// Token: 0x06001066 RID: 4198 RVA: 0x00015E30 File Offset: 0x00014030
		public addressServiceListResponse getAddressList(int startOffSet, int rowCount, string lang)
		{
			long time = Util.convertToTimestamp(DateTime.Now);
			return this.service.getAddressList(this.authConfig.ApiKey, base.getSignature(time), time, startOffSet, rowCount, lang);
		}

		// Token: 0x06001067 RID: 4199 RVA: 0x00015E6C File Offset: 0x0001406C
		public addressServiceDetailResponse getDefaultAddressDetail(string lang)
		{
			long time = Util.convertToTimestamp(DateTime.Now);
			return this.service.getDefaultAddressDetail(this.authConfig.ApiKey, base.getSignature(time), time, lang);
		}

		// Token: 0x06001068 RID: 4200 RVA: 0x00015EA3 File Offset: 0x000140A3
		public string getServiceName()
		{
			return this.service.getServiceName();
		}

		// Token: 0x17000515 RID: 1301
		// (get) Token: 0x06001069 RID: 4201 RVA: 0x00015EB0 File Offset: 0x000140B0
		public static AddressService Instance
		{
			get
			{
				if (AddressService.instance == null)
				{
					lock (AddressService.lockObject)
					{
						if (AddressService.instance == null)
						{
							AddressService.instance = new AddressService();
						}
					}
				}
				return AddressService.instance;
			}
		}

		// Token: 0x04000610 RID: 1552
		private static AddressService instance;

		// Token: 0x04000611 RID: 1553
		private static object lockObject = new object();

		// Token: 0x04000612 RID: 1554
		private IndividualAddressServiceService service = new IndividualAddressServiceService();
	}
}
