﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Address
{
	// Token: 0x0200003D RID: 61
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	[DesignerCategory("code")]
	[DebuggerStepThrough]
	public class deleteAddressCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x060002DA RID: 730 RVA: 0x00005163 File Offset: 0x00003363
		internal deleteAddressCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x17000117 RID: 279
		// (get) Token: 0x060002DB RID: 731 RVA: 0x00005176 File Offset: 0x00003376
		public addressServiceResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (addressServiceResponse)this.results[0];
			}
		}

		// Token: 0x0400012C RID: 300
		private object[] results;
	}
}
