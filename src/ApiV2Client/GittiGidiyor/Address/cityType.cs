﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Address
{
	// Token: 0x02000034 RID: 52
	[XmlType(Namespace = "http://application.individual.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[Serializable]
	public class cityType
	{
		// Token: 0x17000102 RID: 258
		// (get) Token: 0x060002A2 RID: 674 RVA: 0x00004FB7 File Offset: 0x000031B7
		// (set) Token: 0x060002A3 RID: 675 RVA: 0x00004FBF File Offset: 0x000031BF
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int trCode
		{
			get
			{
				return this.trCodeField;
			}
			set
			{
				this.trCodeField = value;
			}
		}

		// Token: 0x17000103 RID: 259
		// (get) Token: 0x060002A4 RID: 676 RVA: 0x00004FC8 File Offset: 0x000031C8
		// (set) Token: 0x060002A5 RID: 677 RVA: 0x00004FD0 File Offset: 0x000031D0
		[XmlIgnore]
		public bool trCodeSpecified
		{
			get
			{
				return this.trCodeFieldSpecified;
			}
			set
			{
				this.trCodeFieldSpecified = value;
			}
		}

		// Token: 0x17000104 RID: 260
		// (get) Token: 0x060002A6 RID: 678 RVA: 0x00004FD9 File Offset: 0x000031D9
		// (set) Token: 0x060002A7 RID: 679 RVA: 0x00004FE1 File Offset: 0x000031E1
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string cityName
		{
			get
			{
				return this.cityNameField;
			}
			set
			{
				this.cityNameField = value;
			}
		}

		// Token: 0x04000117 RID: 279
		private int trCodeField;

		// Token: 0x04000118 RID: 280
		private bool trCodeFieldSpecified;

		// Token: 0x04000119 RID: 281
		private string cityNameField;
	}
}
