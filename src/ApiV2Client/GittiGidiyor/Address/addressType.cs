﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Address
{
	// Token: 0x02000033 RID: 51
	[DesignerCategory("code")]
	[DebuggerStepThrough]
	[XmlType(Namespace = "http://application.individual.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[Serializable]
	public class addressType
	{
		// Token: 0x170000F4 RID: 244
		// (get) Token: 0x06000285 RID: 645 RVA: 0x00004EC1 File Offset: 0x000030C1
		// (set) Token: 0x06000286 RID: 646 RVA: 0x00004EC9 File Offset: 0x000030C9
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int addressId
		{
			get
			{
				return this.addressIdField;
			}
			set
			{
				this.addressIdField = value;
			}
		}

		// Token: 0x170000F5 RID: 245
		// (get) Token: 0x06000287 RID: 647 RVA: 0x00004ED2 File Offset: 0x000030D2
		// (set) Token: 0x06000288 RID: 648 RVA: 0x00004EDA File Offset: 0x000030DA
		[XmlIgnore]
		public bool addressIdSpecified
		{
			get
			{
				return this.addressIdFieldSpecified;
			}
			set
			{
				this.addressIdFieldSpecified = value;
			}
		}

		// Token: 0x170000F6 RID: 246
		// (get) Token: 0x06000289 RID: 649 RVA: 0x00004EE3 File Offset: 0x000030E3
		// (set) Token: 0x0600028A RID: 650 RVA: 0x00004EEB File Offset: 0x000030EB
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public bool isDefault
		{
			get
			{
				return this.isDefaultField;
			}
			set
			{
				this.isDefaultField = value;
			}
		}

		// Token: 0x170000F7 RID: 247
		// (get) Token: 0x0600028B RID: 651 RVA: 0x00004EF4 File Offset: 0x000030F4
		// (set) Token: 0x0600028C RID: 652 RVA: 0x00004EFC File Offset: 0x000030FC
		[XmlIgnore]
		public bool isDefaultSpecified
		{
			get
			{
				return this.isDefaultFieldSpecified;
			}
			set
			{
				this.isDefaultFieldSpecified = value;
			}
		}

		// Token: 0x170000F8 RID: 248
		// (get) Token: 0x0600028D RID: 653 RVA: 0x00004F05 File Offset: 0x00003105
		// (set) Token: 0x0600028E RID: 654 RVA: 0x00004F0D File Offset: 0x0000310D
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string name
		{
			get
			{
				return this.nameField;
			}
			set
			{
				this.nameField = value;
			}
		}

		// Token: 0x170000F9 RID: 249
		// (get) Token: 0x0600028F RID: 655 RVA: 0x00004F16 File Offset: 0x00003116
		// (set) Token: 0x06000290 RID: 656 RVA: 0x00004F1E File Offset: 0x0000311E
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string surname
		{
			get
			{
				return this.surnameField;
			}
			set
			{
				this.surnameField = value;
			}
		}

		// Token: 0x170000FA RID: 250
		// (get) Token: 0x06000291 RID: 657 RVA: 0x00004F27 File Offset: 0x00003127
		// (set) Token: 0x06000292 RID: 658 RVA: 0x00004F2F File Offset: 0x0000312F
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string alias
		{
			get
			{
				return this.aliasField;
			}
			set
			{
				this.aliasField = value;
			}
		}

		// Token: 0x170000FB RID: 251
		// (get) Token: 0x06000293 RID: 659 RVA: 0x00004F38 File Offset: 0x00003138
		// (set) Token: 0x06000294 RID: 660 RVA: 0x00004F40 File Offset: 0x00003140
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string address
		{
			get
			{
				return this.addressField;
			}
			set
			{
				this.addressField = value;
			}
		}

		// Token: 0x170000FC RID: 252
		// (get) Token: 0x06000295 RID: 661 RVA: 0x00004F49 File Offset: 0x00003149
		// (set) Token: 0x06000296 RID: 662 RVA: 0x00004F51 File Offset: 0x00003151
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string district
		{
			get
			{
				return this.districtField;
			}
			set
			{
				this.districtField = value;
			}
		}

		// Token: 0x170000FD RID: 253
		// (get) Token: 0x06000297 RID: 663 RVA: 0x00004F5A File Offset: 0x0000315A
		// (set) Token: 0x06000298 RID: 664 RVA: 0x00004F62 File Offset: 0x00003162
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string zipCode
		{
			get
			{
				return this.zipCodeField;
			}
			set
			{
				this.zipCodeField = value;
			}
		}

		// Token: 0x170000FE RID: 254
		// (get) Token: 0x06000299 RID: 665 RVA: 0x00004F6B File Offset: 0x0000316B
		// (set) Token: 0x0600029A RID: 666 RVA: 0x00004F73 File Offset: 0x00003173
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public cityType city
		{
			get
			{
				return this.cityField;
			}
			set
			{
				this.cityField = value;
			}
		}

		// Token: 0x170000FF RID: 255
		// (get) Token: 0x0600029B RID: 667 RVA: 0x00004F7C File Offset: 0x0000317C
		// (set) Token: 0x0600029C RID: 668 RVA: 0x00004F84 File Offset: 0x00003184
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string phoneNumber
		{
			get
			{
				return this.phoneNumberField;
			}
			set
			{
				this.phoneNumberField = value;
			}
		}

		// Token: 0x17000100 RID: 256
		// (get) Token: 0x0600029D RID: 669 RVA: 0x00004F8D File Offset: 0x0000318D
		// (set) Token: 0x0600029E RID: 670 RVA: 0x00004F95 File Offset: 0x00003195
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string gsmNumber
		{
			get
			{
				return this.gsmNumberField;
			}
			set
			{
				this.gsmNumberField = value;
			}
		}

		// Token: 0x17000101 RID: 257
		// (get) Token: 0x0600029F RID: 671 RVA: 0x00004F9E File Offset: 0x0000319E
		// (set) Token: 0x060002A0 RID: 672 RVA: 0x00004FA6 File Offset: 0x000031A6
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string faxNumber
		{
			get
			{
				return this.faxNumberField;
			}
			set
			{
				this.faxNumberField = value;
			}
		}

		// Token: 0x04000109 RID: 265
		private int addressIdField;

		// Token: 0x0400010A RID: 266
		private bool addressIdFieldSpecified;

		// Token: 0x0400010B RID: 267
		private bool isDefaultField;

		// Token: 0x0400010C RID: 268
		private bool isDefaultFieldSpecified;

		// Token: 0x0400010D RID: 269
		private string nameField;

		// Token: 0x0400010E RID: 270
		private string surnameField;

		// Token: 0x0400010F RID: 271
		private string aliasField;

		// Token: 0x04000110 RID: 272
		private string addressField;

		// Token: 0x04000111 RID: 273
		private string districtField;

		// Token: 0x04000112 RID: 274
		private string zipCodeField;

		// Token: 0x04000113 RID: 275
		private cityType cityField;

		// Token: 0x04000114 RID: 276
		private string phoneNumberField;

		// Token: 0x04000115 RID: 277
		private string gsmNumberField;

		// Token: 0x04000116 RID: 278
		private string faxNumberField;
	}
}
