﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Address
{
	// Token: 0x0200003B RID: 59
	[DesignerCategory("code")]
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	[DebuggerStepThrough]
	public class createNewAddressCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x060002D4 RID: 724 RVA: 0x0000513B File Offset: 0x0000333B
		internal createNewAddressCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x17000116 RID: 278
		// (get) Token: 0x060002D5 RID: 725 RVA: 0x0000514E File Offset: 0x0000334E
		public addressServiceResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (addressServiceResponse)this.results[0];
			}
		}

		// Token: 0x0400012B RID: 299
		private object[] results;
	}
}
