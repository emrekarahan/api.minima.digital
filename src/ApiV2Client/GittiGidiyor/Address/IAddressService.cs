﻿namespace ApiV2Client.GittiGidiyor.Address
{
	// Token: 0x020001E1 RID: 481
	public interface IAddressService : IService
	{
		// Token: 0x06001061 RID: 4193
		addressServiceDetailResponse getAddressDetail(int addressId, string lang);

		// Token: 0x06001062 RID: 4194
		addressServiceListResponse getAddressList(int startOffSet, int rowCount, string lang);

		// Token: 0x06001063 RID: 4195
		addressServiceDetailResponse getDefaultAddressDetail(string lang);
	}
}
