﻿using System.CodeDom.Compiler;

namespace ApiV2Client.GittiGidiyor.Search
{
	// Token: 0x020001C9 RID: 457
	// (Invoke) Token: 0x06000FD1 RID: 4049
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	public delegate void searchForCategoriesCompletedEventHandler(object sender, searchForCategoriesCompletedEventArgs e);
}
