﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Search
{
	// Token: 0x020001B9 RID: 441
	[DesignerCategory("code")]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[DebuggerStepThrough]
	[XmlType(Namespace = "http://search.anonymous.ws.listingapi.gg.com")]
	[Serializable]
	public class categorySpecCriteriaType
	{
		// Token: 0x170004B3 RID: 1203
		// (get) Token: 0x06000F32 RID: 3890 RVA: 0x00014D5C File Offset: 0x00012F5C
		// (set) Token: 0x06000F33 RID: 3891 RVA: 0x00014D64 File Offset: 0x00012F64
		[XmlAttribute]
		public string name
		{
			get
			{
				return this.nameField;
			}
			set
			{
				this.nameField = value;
			}
		}

		// Token: 0x170004B4 RID: 1204
		// (get) Token: 0x06000F34 RID: 3892 RVA: 0x00014D6D File Offset: 0x00012F6D
		// (set) Token: 0x06000F35 RID: 3893 RVA: 0x00014D75 File Offset: 0x00012F75
		[XmlAttribute]
		public string value
		{
			get
			{
				return this.valueField;
			}
			set
			{
				this.valueField = value;
			}
		}

		// Token: 0x040005A1 RID: 1441
		private string nameField;

		// Token: 0x040005A2 RID: 1442
		private string valueField;
	}
}
