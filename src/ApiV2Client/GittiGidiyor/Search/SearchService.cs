﻿namespace ApiV2Client.GittiGidiyor.Search
{
	// Token: 0x02000009 RID: 9
	public class SearchService : ServiceClient<SearchServiceService>, ISearchService, IService
	{
		// Token: 0x06000013 RID: 19 RVA: 0x000022C4 File Offset: 0x000004C4
		private SearchService()
		{
			base.configureNetworkCredentials(this.service);
		}

		// Token: 0x06000014 RID: 20 RVA: 0x000022E3 File Offset: 0x000004E3
		public string getServiceName()
		{
			return this.service.getServiceName();
		}

		// Token: 0x06000015 RID: 21 RVA: 0x000022F0 File Offset: 0x000004F0
		public searchServiceResponse search(string keyword, searchCriteriaType criteria, int startOffset, int rowCount, bool includeDescription, bool withData, string orderBy, string lang)
		{
			return this.service.search(keyword, criteria, startOffset, rowCount, includeDescription, withData, orderBy, lang);
		}

		// Token: 0x17000003 RID: 3
		// (get) Token: 0x06000016 RID: 22 RVA: 0x00002318 File Offset: 0x00000518
		public static SearchService Instance
		{
			get
			{
				if (SearchService.instance == null)
				{
					lock (SearchService.lockObject)
					{
						if (SearchService.instance == null)
						{
							SearchService.instance = new SearchService();
						}
					}
				}
				return SearchService.instance;
			}
		}

		// Token: 0x04000008 RID: 8
		private static SearchService instance;

		// Token: 0x04000009 RID: 9
		private static object lockObject = new object();

		// Token: 0x0400000A RID: 10
		private SearchServiceService service = new SearchServiceService();
	}
}
