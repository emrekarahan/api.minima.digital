﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Search
{
	// Token: 0x020001BB RID: 443
	[XmlType(Namespace = "http://search.anonymous.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[Serializable]
	public class specSearchResultType
	{
		// Token: 0x170004B8 RID: 1208
		// (get) Token: 0x06000F3E RID: 3902 RVA: 0x00014DC1 File Offset: 0x00012FC1
		// (set) Token: 0x06000F3F RID: 3903 RVA: 0x00014DC9 File Offset: 0x00012FC9
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		[XmlArrayItem("facet", Form = XmlSchemaForm.Unqualified)]
		public specValueFacetType[] facets
		{
			get
			{
				return this.facetsField;
			}
			set
			{
				this.facetsField = value;
			}
		}

		// Token: 0x170004B9 RID: 1209
		// (get) Token: 0x06000F40 RID: 3904 RVA: 0x00014DD2 File Offset: 0x00012FD2
		// (set) Token: 0x06000F41 RID: 3905 RVA: 0x00014DDA File Offset: 0x00012FDA
		[XmlAttribute]
		public string name
		{
			get
			{
				return this.nameField;
			}
			set
			{
				this.nameField = value;
			}
		}

		// Token: 0x040005A6 RID: 1446
		private specValueFacetType[] facetsField;

		// Token: 0x040005A7 RID: 1447
		private string nameField;
	}
}
