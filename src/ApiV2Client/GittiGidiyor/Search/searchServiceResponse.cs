﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Search
{
	// Token: 0x020001C1 RID: 449
	[XmlType(Namespace = "http://search.anonymous.ws.listingapi.gg.com")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[Serializable]
	public class searchServiceResponse : baseResponse
	{
		// Token: 0x170004E9 RID: 1257
		// (get) Token: 0x06000FA6 RID: 4006 RVA: 0x00015132 File Offset: 0x00013332
		// (set) Token: 0x06000FA7 RID: 4007 RVA: 0x0001513A File Offset: 0x0001333A
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public long count
		{
			get
			{
				return this.countField;
			}
			set
			{
				this.countField = value;
			}
		}

		// Token: 0x170004EA RID: 1258
		// (get) Token: 0x06000FA8 RID: 4008 RVA: 0x00015143 File Offset: 0x00013343
		// (set) Token: 0x06000FA9 RID: 4009 RVA: 0x0001514B File Offset: 0x0001334B
		[XmlIgnore]
		public bool countSpecified
		{
			get
			{
				return this.countFieldSpecified;
			}
			set
			{
				this.countFieldSpecified = value;
			}
		}

		// Token: 0x170004EB RID: 1259
		// (get) Token: 0x06000FAA RID: 4010 RVA: 0x00015154 File Offset: 0x00013354
		// (set) Token: 0x06000FAB RID: 4011 RVA: 0x0001515C File Offset: 0x0001335C
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public bool isFuzzy
		{
			get
			{
				return this.isFuzzyField;
			}
			set
			{
				this.isFuzzyField = value;
			}
		}

		// Token: 0x170004EC RID: 1260
		// (get) Token: 0x06000FAC RID: 4012 RVA: 0x00015165 File Offset: 0x00013365
		// (set) Token: 0x06000FAD RID: 4013 RVA: 0x0001516D File Offset: 0x0001336D
		[XmlIgnore]
		public bool isFuzzySpecified
		{
			get
			{
				return this.isFuzzyFieldSpecified;
			}
			set
			{
				this.isFuzzyFieldSpecified = value;
			}
		}

		// Token: 0x170004ED RID: 1261
		// (get) Token: 0x06000FAE RID: 4014 RVA: 0x00015176 File Offset: 0x00013376
		// (set) Token: 0x06000FAF RID: 4015 RVA: 0x0001517E File Offset: 0x0001337E
		[XmlArrayItem("product", Form = XmlSchemaForm.Unqualified)]
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		public searchResultType[] products
		{
			get
			{
				return this.productsField;
			}
			set
			{
				this.productsField = value;
			}
		}

		// Token: 0x170004EE RID: 1262
		// (get) Token: 0x06000FB0 RID: 4016 RVA: 0x00015187 File Offset: 0x00013387
		// (set) Token: 0x06000FB1 RID: 4017 RVA: 0x0001518F File Offset: 0x0001338F
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		[XmlArrayItem("category", Form = XmlSchemaForm.Unqualified)]
		public categorySearchResultType[] categories
		{
			get
			{
				return this.categoriesField;
			}
			set
			{
				this.categoriesField = value;
			}
		}

		// Token: 0x170004EF RID: 1263
		// (get) Token: 0x06000FB2 RID: 4018 RVA: 0x00015198 File Offset: 0x00013398
		// (set) Token: 0x06000FB3 RID: 4019 RVA: 0x000151A0 File Offset: 0x000133A0
		[XmlArrayItem("spec", Form = XmlSchemaForm.Unqualified)]
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		public specSearchResultType[] specs
		{
			get
			{
				return this.specsField;
			}
			set
			{
				this.specsField = value;
			}
		}

		// Token: 0x170004F0 RID: 1264
		// (get) Token: 0x06000FB4 RID: 4020 RVA: 0x000151A9 File Offset: 0x000133A9
		// (set) Token: 0x06000FB5 RID: 4021 RVA: 0x000151B1 File Offset: 0x000133B1
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		[XmlArrayItem("facet", Form = XmlSchemaForm.Unqualified)]
		public specSearchResultType[] facets
		{
			get
			{
				return this.facetsField;
			}
			set
			{
				this.facetsField = value;
			}
		}

		// Token: 0x040005D7 RID: 1495
		private long countField;

		// Token: 0x040005D8 RID: 1496
		private bool countFieldSpecified;

		// Token: 0x040005D9 RID: 1497
		private bool isFuzzyField;

		// Token: 0x040005DA RID: 1498
		private bool isFuzzyFieldSpecified;

		// Token: 0x040005DB RID: 1499
		private searchResultType[] productsField;

		// Token: 0x040005DC RID: 1500
		private categorySearchResultType[] categoriesField;

		// Token: 0x040005DD RID: 1501
		private specSearchResultType[] specsField;

		// Token: 0x040005DE RID: 1502
		private specSearchResultType[] facetsField;
	}
}
