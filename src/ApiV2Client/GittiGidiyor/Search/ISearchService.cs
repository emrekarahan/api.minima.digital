﻿namespace ApiV2Client.GittiGidiyor.Search
{
	// Token: 0x02000008 RID: 8
	public interface ISearchService : IService
	{
		// Token: 0x06000012 RID: 18
		searchServiceResponse search(string keyword, searchCriteriaType criteria, int startOffset, int rowCount, bool includeDescription, bool withData, string orderBy, string lang);
	}
}
