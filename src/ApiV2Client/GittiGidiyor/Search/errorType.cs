﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Search
{
	// Token: 0x020001BE RID: 446
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[XmlType(Namespace = "http://search.anonymous.ws.listingapi.gg.com")]
	[Serializable]
	public class errorType
	{
		// Token: 0x170004DC RID: 1244
		// (get) Token: 0x06000F89 RID: 3977 RVA: 0x0001503D File Offset: 0x0001323D
		// (set) Token: 0x06000F8A RID: 3978 RVA: 0x00015045 File Offset: 0x00013245
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string errorId
		{
			get
			{
				return this.errorIdField;
			}
			set
			{
				this.errorIdField = value;
			}
		}

		// Token: 0x170004DD RID: 1245
		// (get) Token: 0x06000F8B RID: 3979 RVA: 0x0001504E File Offset: 0x0001324E
		// (set) Token: 0x06000F8C RID: 3980 RVA: 0x00015056 File Offset: 0x00013256
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string errorCode
		{
			get
			{
				return this.errorCodeField;
			}
			set
			{
				this.errorCodeField = value;
			}
		}

		// Token: 0x170004DE RID: 1246
		// (get) Token: 0x06000F8D RID: 3981 RVA: 0x0001505F File Offset: 0x0001325F
		// (set) Token: 0x06000F8E RID: 3982 RVA: 0x00015067 File Offset: 0x00013267
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string message
		{
			get
			{
				return this.messageField;
			}
			set
			{
				this.messageField = value;
			}
		}

		// Token: 0x170004DF RID: 1247
		// (get) Token: 0x06000F8F RID: 3983 RVA: 0x00015070 File Offset: 0x00013270
		// (set) Token: 0x06000F90 RID: 3984 RVA: 0x00015078 File Offset: 0x00013278
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string viewMessage
		{
			get
			{
				return this.viewMessageField;
			}
			set
			{
				this.viewMessageField = value;
			}
		}

		// Token: 0x040005CA RID: 1482
		private string errorIdField;

		// Token: 0x040005CB RID: 1483
		private string errorCodeField;

		// Token: 0x040005CC RID: 1484
		private string messageField;

		// Token: 0x040005CD RID: 1485
		private string viewMessageField;
	}
}
