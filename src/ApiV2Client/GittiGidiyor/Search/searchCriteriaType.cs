﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Search
{
	// Token: 0x020001B8 RID: 440
	[XmlType(Namespace = "http://search.anonymous.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[Serializable]
	public class searchCriteriaType
	{
		// Token: 0x17000484 RID: 1156
		// (get) Token: 0x06000ED3 RID: 3795 RVA: 0x00014A35 File Offset: 0x00012C35
		// (set) Token: 0x06000ED4 RID: 3796 RVA: 0x00014A3D File Offset: 0x00012C3D
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string format
		{
			get
			{
				return this.formatField;
			}
			set
			{
				this.formatField = value;
			}
		}

		// Token: 0x17000485 RID: 1157
		// (get) Token: 0x06000ED5 RID: 3797 RVA: 0x00014A46 File Offset: 0x00012C46
		// (set) Token: 0x06000ED6 RID: 3798 RVA: 0x00014A4E File Offset: 0x00012C4E
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public bool freeShipping
		{
			get
			{
				return this.freeShippingField;
			}
			set
			{
				this.freeShippingField = value;
			}
		}

		// Token: 0x17000486 RID: 1158
		// (get) Token: 0x06000ED7 RID: 3799 RVA: 0x00014A57 File Offset: 0x00012C57
		// (set) Token: 0x06000ED8 RID: 3800 RVA: 0x00014A5F File Offset: 0x00012C5F
		[XmlIgnore]
		public bool freeShippingSpecified
		{
			get
			{
				return this.freeShippingFieldSpecified;
			}
			set
			{
				this.freeShippingFieldSpecified = value;
			}
		}

		// Token: 0x17000487 RID: 1159
		// (get) Token: 0x06000ED9 RID: 3801 RVA: 0x00014A68 File Offset: 0x00012C68
		// (set) Token: 0x06000EDA RID: 3802 RVA: 0x00014A70 File Offset: 0x00012C70
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public bool startFromOneTL
		{
			get
			{
				return this.startFromOneTLField;
			}
			set
			{
				this.startFromOneTLField = value;
			}
		}

		// Token: 0x17000488 RID: 1160
		// (get) Token: 0x06000EDB RID: 3803 RVA: 0x00014A79 File Offset: 0x00012C79
		// (set) Token: 0x06000EDC RID: 3804 RVA: 0x00014A81 File Offset: 0x00012C81
		[XmlIgnore]
		public bool startFromOneTLSpecified
		{
			get
			{
				return this.startFromOneTLFieldSpecified;
			}
			set
			{
				this.startFromOneTLFieldSpecified = value;
			}
		}

		// Token: 0x17000489 RID: 1161
		// (get) Token: 0x06000EDD RID: 3805 RVA: 0x00014A8A File Offset: 0x00012C8A
		// (set) Token: 0x06000EDE RID: 3806 RVA: 0x00014A92 File Offset: 0x00012C92
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public bool catalogOption
		{
			get
			{
				return this.catalogOptionField;
			}
			set
			{
				this.catalogOptionField = value;
			}
		}

		// Token: 0x1700048A RID: 1162
		// (get) Token: 0x06000EDF RID: 3807 RVA: 0x00014A9B File Offset: 0x00012C9B
		// (set) Token: 0x06000EE0 RID: 3808 RVA: 0x00014AA3 File Offset: 0x00012CA3
		[XmlIgnore]
		public bool catalogOptionSpecified
		{
			get
			{
				return this.catalogOptionFieldSpecified;
			}
			set
			{
				this.catalogOptionFieldSpecified = value;
			}
		}

		// Token: 0x1700048B RID: 1163
		// (get) Token: 0x06000EE1 RID: 3809 RVA: 0x00014AAC File Offset: 0x00012CAC
		// (set) Token: 0x06000EE2 RID: 3810 RVA: 0x00014AB4 File Offset: 0x00012CB4
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public bool newProduct
		{
			get
			{
				return this.newProductField;
			}
			set
			{
				this.newProductField = value;
			}
		}

		// Token: 0x1700048C RID: 1164
		// (get) Token: 0x06000EE3 RID: 3811 RVA: 0x00014ABD File Offset: 0x00012CBD
		// (set) Token: 0x06000EE4 RID: 3812 RVA: 0x00014AC5 File Offset: 0x00012CC5
		[XmlIgnore]
		public bool newProductSpecified
		{
			get
			{
				return this.newProductFieldSpecified;
			}
			set
			{
				this.newProductFieldSpecified = value;
			}
		}

		// Token: 0x1700048D RID: 1165
		// (get) Token: 0x06000EE5 RID: 3813 RVA: 0x00014ACE File Offset: 0x00012CCE
		// (set) Token: 0x06000EE6 RID: 3814 RVA: 0x00014AD6 File Offset: 0x00012CD6
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public double minPrice
		{
			get
			{
				return this.minPriceField;
			}
			set
			{
				this.minPriceField = value;
			}
		}

		// Token: 0x1700048E RID: 1166
		// (get) Token: 0x06000EE7 RID: 3815 RVA: 0x00014ADF File Offset: 0x00012CDF
		// (set) Token: 0x06000EE8 RID: 3816 RVA: 0x00014AE7 File Offset: 0x00012CE7
		[XmlIgnore]
		public bool minPriceSpecified
		{
			get
			{
				return this.minPriceFieldSpecified;
			}
			set
			{
				this.minPriceFieldSpecified = value;
			}
		}

		// Token: 0x1700048F RID: 1167
		// (get) Token: 0x06000EE9 RID: 3817 RVA: 0x00014AF0 File Offset: 0x00012CF0
		// (set) Token: 0x06000EEA RID: 3818 RVA: 0x00014AF8 File Offset: 0x00012CF8
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public double maxPrice
		{
			get
			{
				return this.maxPriceField;
			}
			set
			{
				this.maxPriceField = value;
			}
		}

		// Token: 0x17000490 RID: 1168
		// (get) Token: 0x06000EEB RID: 3819 RVA: 0x00014B01 File Offset: 0x00012D01
		// (set) Token: 0x06000EEC RID: 3820 RVA: 0x00014B09 File Offset: 0x00012D09
		[XmlIgnore]
		public bool maxPriceSpecified
		{
			get
			{
				return this.maxPriceFieldSpecified;
			}
			set
			{
				this.maxPriceFieldSpecified = value;
			}
		}

		// Token: 0x17000491 RID: 1169
		// (get) Token: 0x06000EED RID: 3821 RVA: 0x00014B12 File Offset: 0x00012D12
		// (set) Token: 0x06000EEE RID: 3822 RVA: 0x00014B1A File Offset: 0x00012D1A
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int city
		{
			get
			{
				return this.cityField;
			}
			set
			{
				this.cityField = value;
			}
		}

		// Token: 0x17000492 RID: 1170
		// (get) Token: 0x06000EEF RID: 3823 RVA: 0x00014B23 File Offset: 0x00012D23
		// (set) Token: 0x06000EF0 RID: 3824 RVA: 0x00014B2B File Offset: 0x00012D2B
		[XmlIgnore]
		public bool citySpecified
		{
			get
			{
				return this.cityFieldSpecified;
			}
			set
			{
				this.cityFieldSpecified = value;
			}
		}

		// Token: 0x17000493 RID: 1171
		// (get) Token: 0x06000EF1 RID: 3825 RVA: 0x00014B34 File Offset: 0x00012D34
		// (set) Token: 0x06000EF2 RID: 3826 RVA: 0x00014B3C File Offset: 0x00012D3C
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int newItems
		{
			get
			{
				return this.newItemsField;
			}
			set
			{
				this.newItemsField = value;
			}
		}

		// Token: 0x17000494 RID: 1172
		// (get) Token: 0x06000EF3 RID: 3827 RVA: 0x00014B45 File Offset: 0x00012D45
		// (set) Token: 0x06000EF4 RID: 3828 RVA: 0x00014B4D File Offset: 0x00012D4D
		[XmlIgnore]
		public bool newItemsSpecified
		{
			get
			{
				return this.newItemsFieldSpecified;
			}
			set
			{
				this.newItemsFieldSpecified = value;
			}
		}

		// Token: 0x17000495 RID: 1173
		// (get) Token: 0x06000EF5 RID: 3829 RVA: 0x00014B56 File Offset: 0x00012D56
		// (set) Token: 0x06000EF6 RID: 3830 RVA: 0x00014B5E File Offset: 0x00012D5E
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int runOutItems
		{
			get
			{
				return this.runOutItemsField;
			}
			set
			{
				this.runOutItemsField = value;
			}
		}

		// Token: 0x17000496 RID: 1174
		// (get) Token: 0x06000EF7 RID: 3831 RVA: 0x00014B67 File Offset: 0x00012D67
		// (set) Token: 0x06000EF8 RID: 3832 RVA: 0x00014B6F File Offset: 0x00012D6F
		[XmlIgnore]
		public bool runOutItemsSpecified
		{
			get
			{
				return this.runOutItemsFieldSpecified;
			}
			set
			{
				this.runOutItemsFieldSpecified = value;
			}
		}

		// Token: 0x17000497 RID: 1175
		// (get) Token: 0x06000EF9 RID: 3833 RVA: 0x00014B78 File Offset: 0x00012D78
		// (set) Token: 0x06000EFA RID: 3834 RVA: 0x00014B80 File Offset: 0x00012D80
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int finishedItems
		{
			get
			{
				return this.finishedItemsField;
			}
			set
			{
				this.finishedItemsField = value;
			}
		}

		// Token: 0x17000498 RID: 1176
		// (get) Token: 0x06000EFB RID: 3835 RVA: 0x00014B89 File Offset: 0x00012D89
		// (set) Token: 0x06000EFC RID: 3836 RVA: 0x00014B91 File Offset: 0x00012D91
		[XmlIgnore]
		public bool finishedItemsSpecified
		{
			get
			{
				return this.finishedItemsFieldSpecified;
			}
			set
			{
				this.finishedItemsFieldSpecified = value;
			}
		}

		// Token: 0x17000499 RID: 1177
		// (get) Token: 0x06000EFD RID: 3837 RVA: 0x00014B9A File Offset: 0x00012D9A
		// (set) Token: 0x06000EFE RID: 3838 RVA: 0x00014BA2 File Offset: 0x00012DA2
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string seller
		{
			get
			{
				return this.sellerField;
			}
			set
			{
				this.sellerField = value;
			}
		}

		// Token: 0x1700049A RID: 1178
		// (get) Token: 0x06000EFF RID: 3839 RVA: 0x00014BAB File Offset: 0x00012DAB
		// (set) Token: 0x06000F00 RID: 3840 RVA: 0x00014BB3 File Offset: 0x00012DB3
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public bool sellerExcluded
		{
			get
			{
				return this.sellerExcludedField;
			}
			set
			{
				this.sellerExcludedField = value;
			}
		}

		// Token: 0x1700049B RID: 1179
		// (get) Token: 0x06000F01 RID: 3841 RVA: 0x00014BBC File Offset: 0x00012DBC
		// (set) Token: 0x06000F02 RID: 3842 RVA: 0x00014BC4 File Offset: 0x00012DC4
		[XmlIgnore]
		public bool sellerExcludedSpecified
		{
			get
			{
				return this.sellerExcludedFieldSpecified;
			}
			set
			{
				this.sellerExcludedFieldSpecified = value;
			}
		}

		// Token: 0x1700049C RID: 1180
		// (get) Token: 0x06000F03 RID: 3843 RVA: 0x00014BCD File Offset: 0x00012DCD
		// (set) Token: 0x06000F04 RID: 3844 RVA: 0x00014BD5 File Offset: 0x00012DD5
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string categoryCode
		{
			get
			{
				return this.categoryCodeField;
			}
			set
			{
				this.categoryCodeField = value;
			}
		}

		// Token: 0x1700049D RID: 1181
		// (get) Token: 0x06000F05 RID: 3845 RVA: 0x00014BDE File Offset: 0x00012DDE
		// (set) Token: 0x06000F06 RID: 3846 RVA: 0x00014BE6 File Offset: 0x00012DE6
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int catalogId
		{
			get
			{
				return this.catalogIdField;
			}
			set
			{
				this.catalogIdField = value;
			}
		}

		// Token: 0x1700049E RID: 1182
		// (get) Token: 0x06000F07 RID: 3847 RVA: 0x00014BEF File Offset: 0x00012DEF
		// (set) Token: 0x06000F08 RID: 3848 RVA: 0x00014BF7 File Offset: 0x00012DF7
		[XmlIgnore]
		public bool catalogIdSpecified
		{
			get
			{
				return this.catalogIdFieldSpecified;
			}
			set
			{
				this.catalogIdFieldSpecified = value;
			}
		}

		// Token: 0x1700049F RID: 1183
		// (get) Token: 0x06000F09 RID: 3849 RVA: 0x00014C00 File Offset: 0x00012E00
		// (set) Token: 0x06000F0A RID: 3850 RVA: 0x00014C08 File Offset: 0x00012E08
		[XmlArrayItem("categorySpec", Form = XmlSchemaForm.Unqualified)]
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		public categorySpecCriteriaType[] categorySpecs
		{
			get
			{
				return this.categorySpecsField;
			}
			set
			{
				this.categorySpecsField = value;
			}
		}

		// Token: 0x170004A0 RID: 1184
		// (get) Token: 0x06000F0B RID: 3851 RVA: 0x00014C11 File Offset: 0x00012E11
		// (set) Token: 0x06000F0C RID: 3852 RVA: 0x00014C19 File Offset: 0x00012E19
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public bool returnSpecs
		{
			get
			{
				return this.returnSpecsField;
			}
			set
			{
				this.returnSpecsField = value;
			}
		}

		// Token: 0x170004A1 RID: 1185
		// (get) Token: 0x06000F0D RID: 3853 RVA: 0x00014C22 File Offset: 0x00012E22
		// (set) Token: 0x06000F0E RID: 3854 RVA: 0x00014C2A File Offset: 0x00012E2A
		[XmlIgnore]
		public bool returnSpecsSpecified
		{
			get
			{
				return this.returnSpecsFieldSpecified;
			}
			set
			{
				this.returnSpecsFieldSpecified = value;
			}
		}

		// Token: 0x170004A2 RID: 1186
		// (get) Token: 0x06000F0F RID: 3855 RVA: 0x00014C33 File Offset: 0x00012E33
		// (set) Token: 0x06000F10 RID: 3856 RVA: 0x00014C3B File Offset: 0x00012E3B
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public bool newProductWithNoSpec
		{
			get
			{
				return this.newProductWithNoSpecField;
			}
			set
			{
				this.newProductWithNoSpecField = value;
			}
		}

		// Token: 0x170004A3 RID: 1187
		// (get) Token: 0x06000F11 RID: 3857 RVA: 0x00014C44 File Offset: 0x00012E44
		// (set) Token: 0x06000F12 RID: 3858 RVA: 0x00014C4C File Offset: 0x00012E4C
		[XmlIgnore]
		public bool newProductWithNoSpecSpecified
		{
			get
			{
				return this.newProductWithNoSpecFieldSpecified;
			}
			set
			{
				this.newProductWithNoSpecFieldSpecified = value;
			}
		}

		// Token: 0x170004A4 RID: 1188
		// (get) Token: 0x06000F13 RID: 3859 RVA: 0x00014C55 File Offset: 0x00012E55
		// (set) Token: 0x06000F14 RID: 3860 RVA: 0x00014C5D File Offset: 0x00012E5D
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public bool storeVisibility
		{
			get
			{
				return this.storeVisibilityField;
			}
			set
			{
				this.storeVisibilityField = value;
			}
		}

		// Token: 0x170004A5 RID: 1189
		// (get) Token: 0x06000F15 RID: 3861 RVA: 0x00014C66 File Offset: 0x00012E66
		// (set) Token: 0x06000F16 RID: 3862 RVA: 0x00014C6E File Offset: 0x00012E6E
		[XmlIgnore]
		public bool storeVisibilitySpecified
		{
			get
			{
				return this.storeVisibilityFieldSpecified;
			}
			set
			{
				this.storeVisibilityFieldSpecified = value;
			}
		}

		// Token: 0x170004A6 RID: 1190
		// (get) Token: 0x06000F17 RID: 3863 RVA: 0x00014C77 File Offset: 0x00012E77
		// (set) Token: 0x06000F18 RID: 3864 RVA: 0x00014C7F File Offset: 0x00012E7F
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int storeType
		{
			get
			{
				return this.storeTypeField;
			}
			set
			{
				this.storeTypeField = value;
			}
		}

		// Token: 0x170004A7 RID: 1191
		// (get) Token: 0x06000F19 RID: 3865 RVA: 0x00014C88 File Offset: 0x00012E88
		// (set) Token: 0x06000F1A RID: 3866 RVA: 0x00014C90 File Offset: 0x00012E90
		[XmlIgnore]
		public bool storeTypeSpecified
		{
			get
			{
				return this.storeTypeFieldSpecified;
			}
			set
			{
				this.storeTypeFieldSpecified = value;
			}
		}

		// Token: 0x170004A8 RID: 1192
		// (get) Token: 0x06000F1B RID: 3867 RVA: 0x00014C99 File Offset: 0x00012E99
		// (set) Token: 0x06000F1C RID: 3868 RVA: 0x00014CA1 File Offset: 0x00012EA1
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int storeCategoryId
		{
			get
			{
				return this.storeCategoryIdField;
			}
			set
			{
				this.storeCategoryIdField = value;
			}
		}

		// Token: 0x170004A9 RID: 1193
		// (get) Token: 0x06000F1D RID: 3869 RVA: 0x00014CAA File Offset: 0x00012EAA
		// (set) Token: 0x06000F1E RID: 3870 RVA: 0x00014CB2 File Offset: 0x00012EB2
		[XmlIgnore]
		public bool storeCategoryIdSpecified
		{
			get
			{
				return this.storeCategoryIdFieldSpecified;
			}
			set
			{
				this.storeCategoryIdFieldSpecified = value;
			}
		}

		// Token: 0x170004AA RID: 1194
		// (get) Token: 0x06000F1F RID: 3871 RVA: 0x00014CBB File Offset: 0x00012EBB
		// (set) Token: 0x06000F20 RID: 3872 RVA: 0x00014CC3 File Offset: 0x00012EC3
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int categoryLevel
		{
			get
			{
				return this.categoryLevelField;
			}
			set
			{
				this.categoryLevelField = value;
			}
		}

		// Token: 0x170004AB RID: 1195
		// (get) Token: 0x06000F21 RID: 3873 RVA: 0x00014CCC File Offset: 0x00012ECC
		// (set) Token: 0x06000F22 RID: 3874 RVA: 0x00014CD4 File Offset: 0x00012ED4
		[XmlIgnore]
		public bool categoryLevelSpecified
		{
			get
			{
				return this.categoryLevelFieldSpecified;
			}
			set
			{
				this.categoryLevelFieldSpecified = value;
			}
		}

		// Token: 0x170004AC RID: 1196
		// (get) Token: 0x06000F23 RID: 3875 RVA: 0x00014CDD File Offset: 0x00012EDD
		// (set) Token: 0x06000F24 RID: 3876 RVA: 0x00014CE5 File Offset: 0x00012EE5
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public facetFields facetFields
		{
			get
			{
				return this.facetFieldsField;
			}
			set
			{
				this.facetFieldsField = value;
			}
		}

		// Token: 0x170004AD RID: 1197
		// (get) Token: 0x06000F25 RID: 3877 RVA: 0x00014CEE File Offset: 0x00012EEE
		// (set) Token: 0x06000F26 RID: 3878 RVA: 0x00014CF6 File Offset: 0x00012EF6
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int parkCategoryId
		{
			get
			{
				return this.parkCategoryIdField;
			}
			set
			{
				this.parkCategoryIdField = value;
			}
		}

		// Token: 0x170004AE RID: 1198
		// (get) Token: 0x06000F27 RID: 3879 RVA: 0x00014CFF File Offset: 0x00012EFF
		// (set) Token: 0x06000F28 RID: 3880 RVA: 0x00014D07 File Offset: 0x00012F07
		[XmlIgnore]
		public bool parkCategoryIdSpecified
		{
			get
			{
				return this.parkCategoryIdFieldSpecified;
			}
			set
			{
				this.parkCategoryIdFieldSpecified = value;
			}
		}

		// Token: 0x170004AF RID: 1199
		// (get) Token: 0x06000F29 RID: 3881 RVA: 0x00014D10 File Offset: 0x00012F10
		// (set) Token: 0x06000F2A RID: 3882 RVA: 0x00014D18 File Offset: 0x00012F18
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public bool parkItems
		{
			get
			{
				return this.parkItemsField;
			}
			set
			{
				this.parkItemsField = value;
			}
		}

		// Token: 0x170004B0 RID: 1200
		// (get) Token: 0x06000F2B RID: 3883 RVA: 0x00014D21 File Offset: 0x00012F21
		// (set) Token: 0x06000F2C RID: 3884 RVA: 0x00014D29 File Offset: 0x00012F29
		[XmlIgnore]
		public bool parkItemsSpecified
		{
			get
			{
				return this.parkItemsFieldSpecified;
			}
			set
			{
				this.parkItemsFieldSpecified = value;
			}
		}

		// Token: 0x170004B1 RID: 1201
		// (get) Token: 0x06000F2D RID: 3885 RVA: 0x00014D32 File Offset: 0x00012F32
		// (set) Token: 0x06000F2E RID: 3886 RVA: 0x00014D3A File Offset: 0x00012F3A
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public bool returnImages
		{
			get
			{
				return this.returnImagesField;
			}
			set
			{
				this.returnImagesField = value;
			}
		}

		// Token: 0x170004B2 RID: 1202
		// (get) Token: 0x06000F2F RID: 3887 RVA: 0x00014D43 File Offset: 0x00012F43
		// (set) Token: 0x06000F30 RID: 3888 RVA: 0x00014D4B File Offset: 0x00012F4B
		[XmlIgnore]
		public bool returnImagesSpecified
		{
			get
			{
				return this.returnImagesFieldSpecified;
			}
			set
			{
				this.returnImagesFieldSpecified = value;
			}
		}

		// Token: 0x04000572 RID: 1394
		private string formatField;

		// Token: 0x04000573 RID: 1395
		private bool freeShippingField;

		// Token: 0x04000574 RID: 1396
		private bool freeShippingFieldSpecified;

		// Token: 0x04000575 RID: 1397
		private bool startFromOneTLField;

		// Token: 0x04000576 RID: 1398
		private bool startFromOneTLFieldSpecified;

		// Token: 0x04000577 RID: 1399
		private bool catalogOptionField;

		// Token: 0x04000578 RID: 1400
		private bool catalogOptionFieldSpecified;

		// Token: 0x04000579 RID: 1401
		private bool newProductField;

		// Token: 0x0400057A RID: 1402
		private bool newProductFieldSpecified;

		// Token: 0x0400057B RID: 1403
		private double minPriceField;

		// Token: 0x0400057C RID: 1404
		private bool minPriceFieldSpecified;

		// Token: 0x0400057D RID: 1405
		private double maxPriceField;

		// Token: 0x0400057E RID: 1406
		private bool maxPriceFieldSpecified;

		// Token: 0x0400057F RID: 1407
		private int cityField;

		// Token: 0x04000580 RID: 1408
		private bool cityFieldSpecified;

		// Token: 0x04000581 RID: 1409
		private int newItemsField;

		// Token: 0x04000582 RID: 1410
		private bool newItemsFieldSpecified;

		// Token: 0x04000583 RID: 1411
		private int runOutItemsField;

		// Token: 0x04000584 RID: 1412
		private bool runOutItemsFieldSpecified;

		// Token: 0x04000585 RID: 1413
		private int finishedItemsField;

		// Token: 0x04000586 RID: 1414
		private bool finishedItemsFieldSpecified;

		// Token: 0x04000587 RID: 1415
		private string sellerField;

		// Token: 0x04000588 RID: 1416
		private bool sellerExcludedField;

		// Token: 0x04000589 RID: 1417
		private bool sellerExcludedFieldSpecified;

		// Token: 0x0400058A RID: 1418
		private string categoryCodeField;

		// Token: 0x0400058B RID: 1419
		private int catalogIdField;

		// Token: 0x0400058C RID: 1420
		private bool catalogIdFieldSpecified;

		// Token: 0x0400058D RID: 1421
		private categorySpecCriteriaType[] categorySpecsField;

		// Token: 0x0400058E RID: 1422
		private bool returnSpecsField;

		// Token: 0x0400058F RID: 1423
		private bool returnSpecsFieldSpecified;

		// Token: 0x04000590 RID: 1424
		private bool newProductWithNoSpecField;

		// Token: 0x04000591 RID: 1425
		private bool newProductWithNoSpecFieldSpecified;

		// Token: 0x04000592 RID: 1426
		private bool storeVisibilityField;

		// Token: 0x04000593 RID: 1427
		private bool storeVisibilityFieldSpecified;

		// Token: 0x04000594 RID: 1428
		private int storeTypeField;

		// Token: 0x04000595 RID: 1429
		private bool storeTypeFieldSpecified;

		// Token: 0x04000596 RID: 1430
		private int storeCategoryIdField;

		// Token: 0x04000597 RID: 1431
		private bool storeCategoryIdFieldSpecified;

		// Token: 0x04000598 RID: 1432
		private int categoryLevelField;

		// Token: 0x04000599 RID: 1433
		private bool categoryLevelFieldSpecified;

		// Token: 0x0400059A RID: 1434
		private facetFields facetFieldsField;

		// Token: 0x0400059B RID: 1435
		private int parkCategoryIdField;

		// Token: 0x0400059C RID: 1436
		private bool parkCategoryIdFieldSpecified;

		// Token: 0x0400059D RID: 1437
		private bool parkItemsField;

		// Token: 0x0400059E RID: 1438
		private bool parkItemsFieldSpecified;

		// Token: 0x0400059F RID: 1439
		private bool returnImagesField;

		// Token: 0x040005A0 RID: 1440
		private bool returnImagesFieldSpecified;
	}
}
