﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Search
{
	// Token: 0x020001C2 RID: 450
	[XmlType(Namespace = "http://search.anonymous.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[Serializable]
	public class facetFields
	{
		// Token: 0x170004F1 RID: 1265
		// (get) Token: 0x06000FB7 RID: 4023 RVA: 0x000151C2 File Offset: 0x000133C2
		// (set) Token: 0x06000FB8 RID: 4024 RVA: 0x000151CA File Offset: 0x000133CA
		[XmlArrayItem("facet", Form = XmlSchemaForm.Unqualified)]
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		public string[] facets
		{
			get
			{
				return this.facetsField;
			}
			set
			{
				this.facetsField = value;
			}
		}

		// Token: 0x170004F2 RID: 1266
		// (get) Token: 0x06000FB9 RID: 4025 RVA: 0x000151D3 File Offset: 0x000133D3
		// (set) Token: 0x06000FBA RID: 4026 RVA: 0x000151DB File Offset: 0x000133DB
		[XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true)]
		public int? limit
		{
			get
			{
				return this.limitField;
			}
			set
			{
				this.limitField = value;
			}
		}

		// Token: 0x170004F3 RID: 1267
		// (get) Token: 0x06000FBB RID: 4027 RVA: 0x000151E4 File Offset: 0x000133E4
		// (set) Token: 0x06000FBC RID: 4028 RVA: 0x000151EC File Offset: 0x000133EC
		[XmlIgnore]
		public bool limitSpecified
		{
			get
			{
				return this.limitFieldSpecified;
			}
			set
			{
				this.limitFieldSpecified = value;
			}
		}

		// Token: 0x040005DF RID: 1503
		private string[] facetsField;

		// Token: 0x040005E0 RID: 1504
		private int? limitField;

		// Token: 0x040005E1 RID: 1505
		private bool limitFieldSpecified;
	}
}
