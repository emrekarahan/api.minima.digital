﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Search
{
	// Token: 0x020001BD RID: 445
	[DebuggerStepThrough]
	[XmlType(Namespace = "http://search.anonymous.ws.listingapi.gg.com")]
	[DesignerCategory("code")]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[Serializable]
	public class searchResultType
	{
		// Token: 0x170004C0 RID: 1216
		// (get) Token: 0x06000F50 RID: 3920 RVA: 0x00014E59 File Offset: 0x00013059
		// (set) Token: 0x06000F51 RID: 3921 RVA: 0x00014E61 File Offset: 0x00013061
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int productId
		{
			get
			{
				return this.productIdField;
			}
			set
			{
				this.productIdField = value;
			}
		}

		// Token: 0x170004C1 RID: 1217
		// (get) Token: 0x06000F52 RID: 3922 RVA: 0x00014E6A File Offset: 0x0001306A
		// (set) Token: 0x06000F53 RID: 3923 RVA: 0x00014E72 File Offset: 0x00013072
		[XmlIgnore]
		public bool productIdSpecified
		{
			get
			{
				return this.productIdFieldSpecified;
			}
			set
			{
				this.productIdFieldSpecified = value;
			}
		}

		// Token: 0x170004C2 RID: 1218
		// (get) Token: 0x06000F54 RID: 3924 RVA: 0x00014E7B File Offset: 0x0001307B
		// (set) Token: 0x06000F55 RID: 3925 RVA: 0x00014E83 File Offset: 0x00013083
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string imageLink
		{
			get
			{
				return this.imageLinkField;
			}
			set
			{
				this.imageLinkField = value;
			}
		}

		// Token: 0x170004C3 RID: 1219
		// (get) Token: 0x06000F56 RID: 3926 RVA: 0x00014E8C File Offset: 0x0001308C
		// (set) Token: 0x06000F57 RID: 3927 RVA: 0x00014E94 File Offset: 0x00013094
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string url
		{
			get
			{
				return this.urlField;
			}
			set
			{
				this.urlField = value;
			}
		}

		// Token: 0x170004C4 RID: 1220
		// (get) Token: 0x06000F58 RID: 3928 RVA: 0x00014E9D File Offset: 0x0001309D
		// (set) Token: 0x06000F59 RID: 3929 RVA: 0x00014EA5 File Offset: 0x000130A5
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string title
		{
			get
			{
				return this.titleField;
			}
			set
			{
				this.titleField = value;
			}
		}

		// Token: 0x170004C5 RID: 1221
		// (get) Token: 0x06000F5A RID: 3930 RVA: 0x00014EAE File Offset: 0x000130AE
		// (set) Token: 0x06000F5B RID: 3931 RVA: 0x00014EB6 File Offset: 0x000130B6
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string subtitle
		{
			get
			{
				return this.subtitleField;
			}
			set
			{
				this.subtitleField = value;
			}
		}

		// Token: 0x170004C6 RID: 1222
		// (get) Token: 0x06000F5C RID: 3932 RVA: 0x00014EBF File Offset: 0x000130BF
		// (set) Token: 0x06000F5D RID: 3933 RVA: 0x00014EC7 File Offset: 0x000130C7
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string seller
		{
			get
			{
				return this.sellerField;
			}
			set
			{
				this.sellerField = value;
			}
		}

		// Token: 0x170004C7 RID: 1223
		// (get) Token: 0x06000F5E RID: 3934 RVA: 0x00014ED0 File Offset: 0x000130D0
		// (set) Token: 0x06000F5F RID: 3935 RVA: 0x00014ED8 File Offset: 0x000130D8
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string format
		{
			get
			{
				return this.formatField;
			}
			set
			{
				this.formatField = value;
			}
		}

		// Token: 0x170004C8 RID: 1224
		// (get) Token: 0x06000F60 RID: 3936 RVA: 0x00014EE1 File Offset: 0x000130E1
		// (set) Token: 0x06000F61 RID: 3937 RVA: 0x00014EE9 File Offset: 0x000130E9
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public bool hasBuyNowPrice
		{
			get
			{
				return this.hasBuyNowPriceField;
			}
			set
			{
				this.hasBuyNowPriceField = value;
			}
		}

		// Token: 0x170004C9 RID: 1225
		// (get) Token: 0x06000F62 RID: 3938 RVA: 0x00014EF2 File Offset: 0x000130F2
		// (set) Token: 0x06000F63 RID: 3939 RVA: 0x00014EFA File Offset: 0x000130FA
		[XmlIgnore]
		public bool hasBuyNowPriceSpecified
		{
			get
			{
				return this.hasBuyNowPriceFieldSpecified;
			}
			set
			{
				this.hasBuyNowPriceFieldSpecified = value;
			}
		}

		// Token: 0x170004CA RID: 1226
		// (get) Token: 0x06000F64 RID: 3940 RVA: 0x00014F03 File Offset: 0x00013103
		// (set) Token: 0x06000F65 RID: 3941 RVA: 0x00014F0B File Offset: 0x0001310B
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int bidCount
		{
			get
			{
				return this.bidCountField;
			}
			set
			{
				this.bidCountField = value;
			}
		}

		// Token: 0x170004CB RID: 1227
		// (get) Token: 0x06000F66 RID: 3942 RVA: 0x00014F14 File Offset: 0x00013114
		// (set) Token: 0x06000F67 RID: 3943 RVA: 0x00014F1C File Offset: 0x0001311C
		[XmlIgnore]
		public bool bidCountSpecified
		{
			get
			{
				return this.bidCountFieldSpecified;
			}
			set
			{
				this.bidCountFieldSpecified = value;
			}
		}

		// Token: 0x170004CC RID: 1228
		// (get) Token: 0x06000F68 RID: 3944 RVA: 0x00014F25 File Offset: 0x00013125
		// (set) Token: 0x06000F69 RID: 3945 RVA: 0x00014F2D File Offset: 0x0001312D
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string currentPrice
		{
			get
			{
				return this.currentPriceField;
			}
			set
			{
				this.currentPriceField = value;
			}
		}

		// Token: 0x170004CD RID: 1229
		// (get) Token: 0x06000F6A RID: 3946 RVA: 0x00014F36 File Offset: 0x00013136
		// (set) Token: 0x06000F6B RID: 3947 RVA: 0x00014F3E File Offset: 0x0001313E
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string buyNowPrice
		{
			get
			{
				return this.buyNowPriceField;
			}
			set
			{
				this.buyNowPriceField = value;
			}
		}

		// Token: 0x170004CE RID: 1230
		// (get) Token: 0x06000F6C RID: 3948 RVA: 0x00014F47 File Offset: 0x00013147
		// (set) Token: 0x06000F6D RID: 3949 RVA: 0x00014F4F File Offset: 0x0001314F
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string remainingTime
		{
			get
			{
				return this.remainingTimeField;
			}
			set
			{
				this.remainingTimeField = value;
			}
		}

		// Token: 0x170004CF RID: 1231
		// (get) Token: 0x06000F6E RID: 3950 RVA: 0x00014F58 File Offset: 0x00013158
		// (set) Token: 0x06000F6F RID: 3951 RVA: 0x00014F60 File Offset: 0x00013160
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string shippingPayment
		{
			get
			{
				return this.shippingPaymentField;
			}
			set
			{
				this.shippingPaymentField = value;
			}
		}

		// Token: 0x170004D0 RID: 1232
		// (get) Token: 0x06000F70 RID: 3952 RVA: 0x00014F69 File Offset: 0x00013169
		// (set) Token: 0x06000F71 RID: 3953 RVA: 0x00014F71 File Offset: 0x00013171
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string featureType
		{
			get
			{
				return this.featureTypeField;
			}
			set
			{
				this.featureTypeField = value;
			}
		}

		// Token: 0x170004D1 RID: 1233
		// (get) Token: 0x06000F72 RID: 3954 RVA: 0x00014F7A File Offset: 0x0001317A
		// (set) Token: 0x06000F73 RID: 3955 RVA: 0x00014F82 File Offset: 0x00013182
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public bool newItem
		{
			get
			{
				return this.newItemField;
			}
			set
			{
				this.newItemField = value;
			}
		}

		// Token: 0x170004D2 RID: 1234
		// (get) Token: 0x06000F74 RID: 3956 RVA: 0x00014F8B File Offset: 0x0001318B
		// (set) Token: 0x06000F75 RID: 3957 RVA: 0x00014F93 File Offset: 0x00013193
		[XmlIgnore]
		public bool newItemSpecified
		{
			get
			{
				return this.newItemFieldSpecified;
			}
			set
			{
				this.newItemFieldSpecified = value;
			}
		}

		// Token: 0x170004D3 RID: 1235
		// (get) Token: 0x06000F76 RID: 3958 RVA: 0x00014F9C File Offset: 0x0001319C
		// (set) Token: 0x06000F77 RID: 3959 RVA: 0x00014FA4 File Offset: 0x000131A4
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public bool bold
		{
			get
			{
				return this.boldField;
			}
			set
			{
				this.boldField = value;
			}
		}

		// Token: 0x170004D4 RID: 1236
		// (get) Token: 0x06000F78 RID: 3960 RVA: 0x00014FAD File Offset: 0x000131AD
		// (set) Token: 0x06000F79 RID: 3961 RVA: 0x00014FB5 File Offset: 0x000131B5
		[XmlIgnore]
		public bool boldSpecified
		{
			get
			{
				return this.boldFieldSpecified;
			}
			set
			{
				this.boldFieldSpecified = value;
			}
		}

		// Token: 0x170004D5 RID: 1237
		// (get) Token: 0x06000F7A RID: 3962 RVA: 0x00014FBE File Offset: 0x000131BE
		// (set) Token: 0x06000F7B RID: 3963 RVA: 0x00014FC6 File Offset: 0x000131C6
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string marketPrice
		{
			get
			{
				return this.marketPriceField;
			}
			set
			{
				this.marketPriceField = value;
			}
		}

		// Token: 0x170004D6 RID: 1238
		// (get) Token: 0x06000F7C RID: 3964 RVA: 0x00014FCF File Offset: 0x000131CF
		// (set) Token: 0x06000F7D RID: 3965 RVA: 0x00014FD7 File Offset: 0x000131D7
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int productCount
		{
			get
			{
				return this.productCountField;
			}
			set
			{
				this.productCountField = value;
			}
		}

		// Token: 0x170004D7 RID: 1239
		// (get) Token: 0x06000F7E RID: 3966 RVA: 0x00014FE0 File Offset: 0x000131E0
		// (set) Token: 0x06000F7F RID: 3967 RVA: 0x00014FE8 File Offset: 0x000131E8
		[XmlIgnore]
		public bool productCountSpecified
		{
			get
			{
				return this.productCountFieldSpecified;
			}
			set
			{
				this.productCountFieldSpecified = value;
			}
		}

		// Token: 0x170004D8 RID: 1240
		// (get) Token: 0x06000F80 RID: 3968 RVA: 0x00014FF1 File Offset: 0x000131F1
		// (set) Token: 0x06000F81 RID: 3969 RVA: 0x00014FF9 File Offset: 0x000131F9
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int soldNum
		{
			get
			{
				return this.soldNumField;
			}
			set
			{
				this.soldNumField = value;
			}
		}

		// Token: 0x170004D9 RID: 1241
		// (get) Token: 0x06000F82 RID: 3970 RVA: 0x00015002 File Offset: 0x00013202
		// (set) Token: 0x06000F83 RID: 3971 RVA: 0x0001500A File Offset: 0x0001320A
		[XmlIgnore]
		public bool soldNumSpecified
		{
			get
			{
				return this.soldNumFieldSpecified;
			}
			set
			{
				this.soldNumFieldSpecified = value;
			}
		}

		// Token: 0x170004DA RID: 1242
		// (get) Token: 0x06000F84 RID: 3972 RVA: 0x00015013 File Offset: 0x00013213
		// (set) Token: 0x06000F85 RID: 3973 RVA: 0x0001501B File Offset: 0x0001321B
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int storeSoldNum
		{
			get
			{
				return this.storeSoldNumField;
			}
			set
			{
				this.storeSoldNumField = value;
			}
		}

		// Token: 0x170004DB RID: 1243
		// (get) Token: 0x06000F86 RID: 3974 RVA: 0x00015024 File Offset: 0x00013224
		// (set) Token: 0x06000F87 RID: 3975 RVA: 0x0001502C File Offset: 0x0001322C
		[XmlIgnore]
		public bool storeSoldNumSpecified
		{
			get
			{
				return this.storeSoldNumFieldSpecified;
			}
			set
			{
				this.storeSoldNumFieldSpecified = value;
			}
		}

		// Token: 0x040005AE RID: 1454
		private int productIdField;

		// Token: 0x040005AF RID: 1455
		private bool productIdFieldSpecified;

		// Token: 0x040005B0 RID: 1456
		private string imageLinkField;

		// Token: 0x040005B1 RID: 1457
		private string urlField;

		// Token: 0x040005B2 RID: 1458
		private string titleField;

		// Token: 0x040005B3 RID: 1459
		private string subtitleField;

		// Token: 0x040005B4 RID: 1460
		private string sellerField;

		// Token: 0x040005B5 RID: 1461
		private string formatField;

		// Token: 0x040005B6 RID: 1462
		private bool hasBuyNowPriceField;

		// Token: 0x040005B7 RID: 1463
		private bool hasBuyNowPriceFieldSpecified;

		// Token: 0x040005B8 RID: 1464
		private int bidCountField;

		// Token: 0x040005B9 RID: 1465
		private bool bidCountFieldSpecified;

		// Token: 0x040005BA RID: 1466
		private string currentPriceField;

		// Token: 0x040005BB RID: 1467
		private string buyNowPriceField;

		// Token: 0x040005BC RID: 1468
		private string remainingTimeField;

		// Token: 0x040005BD RID: 1469
		private string shippingPaymentField;

		// Token: 0x040005BE RID: 1470
		private string featureTypeField;

		// Token: 0x040005BF RID: 1471
		private bool newItemField;

		// Token: 0x040005C0 RID: 1472
		private bool newItemFieldSpecified;

		// Token: 0x040005C1 RID: 1473
		private bool boldField;

		// Token: 0x040005C2 RID: 1474
		private bool boldFieldSpecified;

		// Token: 0x040005C3 RID: 1475
		private string marketPriceField;

		// Token: 0x040005C4 RID: 1476
		private int productCountField;

		// Token: 0x040005C5 RID: 1477
		private bool productCountFieldSpecified;

		// Token: 0x040005C6 RID: 1478
		private int soldNumField;

		// Token: 0x040005C7 RID: 1479
		private bool soldNumFieldSpecified;

		// Token: 0x040005C8 RID: 1480
		private int storeSoldNumField;

		// Token: 0x040005C9 RID: 1481
		private bool storeSoldNumFieldSpecified;
	}
}
