﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Search
{
	// Token: 0x020001C6 RID: 454
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	public class searchCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000FC8 RID: 4040 RVA: 0x00015225 File Offset: 0x00013425
		internal searchCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x170004F5 RID: 1269
		// (get) Token: 0x06000FC9 RID: 4041 RVA: 0x00015238 File Offset: 0x00013438
		public searchServiceResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (searchServiceResponse)this.results[0];
			}
		}

		// Token: 0x040005E3 RID: 1507
		private object[] results;
	}
}
