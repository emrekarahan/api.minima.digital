﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Search
{
	// Token: 0x020001C8 RID: 456
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	public class searchByVersionCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000FCE RID: 4046 RVA: 0x0001524D File Offset: 0x0001344D
		internal searchByVersionCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x170004F6 RID: 1270
		// (get) Token: 0x06000FCF RID: 4047 RVA: 0x00015260 File Offset: 0x00013460
		public searchServiceResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (searchServiceResponse)this.results[0];
			}
		}

		// Token: 0x040005E4 RID: 1508
		private object[] results;
	}
}
