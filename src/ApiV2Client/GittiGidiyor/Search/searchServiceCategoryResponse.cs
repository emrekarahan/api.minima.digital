﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Search
{
	// Token: 0x020001C0 RID: 448
	[DesignerCategory("code")]
	[XmlType(Namespace = "http://search.anonymous.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[DebuggerStepThrough]
	[Serializable]
	public class searchServiceCategoryResponse : baseResponse
	{
		// Token: 0x170004E4 RID: 1252
		// (get) Token: 0x06000F9B RID: 3995 RVA: 0x000150D5 File Offset: 0x000132D5
		// (set) Token: 0x06000F9C RID: 3996 RVA: 0x000150DD File Offset: 0x000132DD
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public long count
		{
			get
			{
				return this.countField;
			}
			set
			{
				this.countField = value;
			}
		}

		// Token: 0x170004E5 RID: 1253
		// (get) Token: 0x06000F9D RID: 3997 RVA: 0x000150E6 File Offset: 0x000132E6
		// (set) Token: 0x06000F9E RID: 3998 RVA: 0x000150EE File Offset: 0x000132EE
		[XmlIgnore]
		public bool countSpecified
		{
			get
			{
				return this.countFieldSpecified;
			}
			set
			{
				this.countFieldSpecified = value;
			}
		}

		// Token: 0x170004E6 RID: 1254
		// (get) Token: 0x06000F9F RID: 3999 RVA: 0x000150F7 File Offset: 0x000132F7
		// (set) Token: 0x06000FA0 RID: 4000 RVA: 0x000150FF File Offset: 0x000132FF
		[XmlArrayItem("category", Form = XmlSchemaForm.Unqualified)]
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		public categorySearchResultType[] categories
		{
			get
			{
				return this.categoriesField;
			}
			set
			{
				this.categoriesField = value;
			}
		}

		// Token: 0x170004E7 RID: 1255
		// (get) Token: 0x06000FA1 RID: 4001 RVA: 0x00015108 File Offset: 0x00013308
		// (set) Token: 0x06000FA2 RID: 4002 RVA: 0x00015110 File Offset: 0x00013310
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public long totalProductCount
		{
			get
			{
				return this.totalProductCountField;
			}
			set
			{
				this.totalProductCountField = value;
			}
		}

		// Token: 0x170004E8 RID: 1256
		// (get) Token: 0x06000FA3 RID: 4003 RVA: 0x00015119 File Offset: 0x00013319
		// (set) Token: 0x06000FA4 RID: 4004 RVA: 0x00015121 File Offset: 0x00013321
		[XmlIgnore]
		public bool totalProductCountSpecified
		{
			get
			{
				return this.totalProductCountFieldSpecified;
			}
			set
			{
				this.totalProductCountFieldSpecified = value;
			}
		}

		// Token: 0x040005D2 RID: 1490
		private long countField;

		// Token: 0x040005D3 RID: 1491
		private bool countFieldSpecified;

		// Token: 0x040005D4 RID: 1492
		private categorySearchResultType[] categoriesField;

		// Token: 0x040005D5 RID: 1493
		private long totalProductCountField;

		// Token: 0x040005D6 RID: 1494
		private bool totalProductCountFieldSpecified;
	}
}
