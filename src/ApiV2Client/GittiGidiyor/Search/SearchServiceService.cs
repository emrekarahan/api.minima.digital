﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading;
using System.Web.Services;
using System.Web.Services.Description;
using System.Web.Services.Protocols;
using System.Xml.Serialization;
using ApiV2Client.Properties;

namespace ApiV2Client.GittiGidiyor.Search
{
	// Token: 0x020001B7 RID: 439
	[WebServiceBinding(Name = "SearchServiceBinding", Namespace = "http://search.anonymous.ws.listingapi.gg.com")]
	[XmlInclude(typeof(baseResponse))]
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	public class SearchServiceService : SoapHttpClientProtocol
	{
		// Token: 0x06000EB4 RID: 3764 RVA: 0x00014377 File Offset: 0x00012577
		public SearchServiceService()
		{
			this.Url = Settings.Default.ApiV2Client_GittiGidiyor_Search_SearchServiceService;
			if (this.IsLocalFileSystemWebService(this.Url))
			{
				this.UseDefaultCredentials = true;
				this.useDefaultCredentialsSetExplicitly = false;
				return;
			}
			this.useDefaultCredentialsSetExplicitly = true;
		}

		// Token: 0x17000482 RID: 1154
		// (get) Token: 0x06000EB5 RID: 3765 RVA: 0x000143B3 File Offset: 0x000125B3
		// (set) Token: 0x06000EB6 RID: 3766 RVA: 0x000143BB File Offset: 0x000125BB
		public new string Url
		{
			get
			{
				return base.Url;
			}
			set
			{
				if (this.IsLocalFileSystemWebService(base.Url) && !this.useDefaultCredentialsSetExplicitly && !this.IsLocalFileSystemWebService(value))
				{
					base.UseDefaultCredentials = false;
				}
				base.Url = value;
			}
		}

		// Token: 0x17000483 RID: 1155
		// (get) Token: 0x06000EB7 RID: 3767 RVA: 0x000143EA File Offset: 0x000125EA
		// (set) Token: 0x06000EB8 RID: 3768 RVA: 0x000143F2 File Offset: 0x000125F2
		public new bool UseDefaultCredentials
		{
			get
			{
				return base.UseDefaultCredentials;
			}
			set
			{
				base.UseDefaultCredentials = value;
				this.useDefaultCredentialsSetExplicitly = true;
			}
		}

		// Token: 0x1400007E RID: 126
		// (add) Token: 0x06000EB9 RID: 3769 RVA: 0x00014404 File Offset: 0x00012604
		// (remove) Token: 0x06000EBA RID: 3770 RVA: 0x0001443C File Offset: 0x0001263C
		public event getServiceNameCompletedEventHandler getServiceNameCompleted;

		// Token: 0x1400007F RID: 127
		// (add) Token: 0x06000EBB RID: 3771 RVA: 0x00014474 File Offset: 0x00012674
		// (remove) Token: 0x06000EBC RID: 3772 RVA: 0x000144AC File Offset: 0x000126AC
		public event searchCompletedEventHandler searchCompleted;

		// Token: 0x14000080 RID: 128
		// (add) Token: 0x06000EBD RID: 3773 RVA: 0x000144E4 File Offset: 0x000126E4
		// (remove) Token: 0x06000EBE RID: 3774 RVA: 0x0001451C File Offset: 0x0001271C
		public event searchByVersionCompletedEventHandler searchByVersionCompleted;

		// Token: 0x14000081 RID: 129
		// (add) Token: 0x06000EBF RID: 3775 RVA: 0x00014554 File Offset: 0x00012754
		// (remove) Token: 0x06000EC0 RID: 3776 RVA: 0x0001458C File Offset: 0x0001278C
		public event searchForCategoriesCompletedEventHandler searchForCategoriesCompleted;

		// Token: 0x06000EC1 RID: 3777 RVA: 0x000145C4 File Offset: 0x000127C4
		[SoapRpcMethod("", RequestNamespace = "http://search.anonymous.ws.listingapi.gg.com", ResponseNamespace = "http://search.anonymous.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public string getServiceName()
		{
			object[] array = base.Invoke("getServiceName", new object[0]);
			return (string)array[0];
		}

		// Token: 0x06000EC2 RID: 3778 RVA: 0x000145EB File Offset: 0x000127EB
		public void getServiceNameAsync()
		{
			this.getServiceNameAsync(null);
		}

		// Token: 0x06000EC3 RID: 3779 RVA: 0x000145F4 File Offset: 0x000127F4
		public void getServiceNameAsync(object userState)
		{
			if (this.getServiceNameOperationCompleted == null)
			{
				this.getServiceNameOperationCompleted = new SendOrPostCallback(this.OngetServiceNameOperationCompleted);
			}
			base.InvokeAsync("getServiceName", new object[0], this.getServiceNameOperationCompleted, userState);
		}

		// Token: 0x06000EC4 RID: 3780 RVA: 0x00014628 File Offset: 0x00012828
		private void OngetServiceNameOperationCompleted(object arg)
		{
			if (this.getServiceNameCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getServiceNameCompleted(this, new getServiceNameCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x06000EC5 RID: 3781 RVA: 0x00014670 File Offset: 0x00012870
		[SoapRpcMethod("", RequestNamespace = "http://search.anonymous.ws.listingapi.gg.com", ResponseNamespace = "http://search.anonymous.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public searchServiceResponse search(string keyword, searchCriteriaType criteria, int startOffSet, int rowCount, bool includeDescription, bool withData, string orderBy, string lang)
		{
			object[] array = base.Invoke("search", new object[]
			{
				keyword,
				criteria,
				startOffSet,
				rowCount,
				includeDescription,
				withData,
				orderBy,
				lang
			});
			return (searchServiceResponse)array[0];
		}

		// Token: 0x06000EC6 RID: 3782 RVA: 0x000146D4 File Offset: 0x000128D4
		public void searchAsync(string keyword, searchCriteriaType criteria, int startOffSet, int rowCount, bool includeDescription, bool withData, string orderBy, string lang)
		{
			this.searchAsync(keyword, criteria, startOffSet, rowCount, includeDescription, withData, orderBy, lang, null);
		}

		// Token: 0x06000EC7 RID: 3783 RVA: 0x000146F8 File Offset: 0x000128F8
		public void searchAsync(string keyword, searchCriteriaType criteria, int startOffSet, int rowCount, bool includeDescription, bool withData, string orderBy, string lang, object userState)
		{
			if (this.searchOperationCompleted == null)
			{
				this.searchOperationCompleted = new SendOrPostCallback(this.OnsearchOperationCompleted);
			}
			base.InvokeAsync("search", new object[]
			{
				keyword,
				criteria,
				startOffSet,
				rowCount,
				includeDescription,
				withData,
				orderBy,
				lang
			}, this.searchOperationCompleted, userState);
		}

		// Token: 0x06000EC8 RID: 3784 RVA: 0x00014774 File Offset: 0x00012974
		private void OnsearchOperationCompleted(object arg)
		{
			if (this.searchCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.searchCompleted(this, new searchCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x06000EC9 RID: 3785 RVA: 0x000147BC File Offset: 0x000129BC
		[SoapRpcMethod("", RequestNamespace = "http://search.anonymous.ws.listingapi.gg.com", ResponseNamespace = "http://search.anonymous.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public searchServiceResponse searchByVersion(string keyword, searchCriteriaType criteria, int startOffSet, int rowCount, bool includeDescription, bool withData, string orderBy, string version, string lang)
		{
			object[] array = base.Invoke("searchByVersion", new object[]
			{
				keyword,
				criteria,
				startOffSet,
				rowCount,
				includeDescription,
				withData,
				orderBy,
				version,
				lang
			});
			return (searchServiceResponse)array[0];
		}

		// Token: 0x06000ECA RID: 3786 RVA: 0x00014824 File Offset: 0x00012A24
		public void searchByVersionAsync(string keyword, searchCriteriaType criteria, int startOffSet, int rowCount, bool includeDescription, bool withData, string orderBy, string version, string lang)
		{
			this.searchByVersionAsync(keyword, criteria, startOffSet, rowCount, includeDescription, withData, orderBy, version, lang, null);
		}

		// Token: 0x06000ECB RID: 3787 RVA: 0x00014848 File Offset: 0x00012A48
		public void searchByVersionAsync(string keyword, searchCriteriaType criteria, int startOffSet, int rowCount, bool includeDescription, bool withData, string orderBy, string version, string lang, object userState)
		{
			if (this.searchByVersionOperationCompleted == null)
			{
				this.searchByVersionOperationCompleted = new SendOrPostCallback(this.OnsearchByVersionOperationCompleted);
			}
			base.InvokeAsync("searchByVersion", new object[]
			{
				keyword,
				criteria,
				startOffSet,
				rowCount,
				includeDescription,
				withData,
				orderBy,
				version,
				lang
			}, this.searchByVersionOperationCompleted, userState);
		}

		// Token: 0x06000ECC RID: 3788 RVA: 0x000148CC File Offset: 0x00012ACC
		private void OnsearchByVersionOperationCompleted(object arg)
		{
			if (this.searchByVersionCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.searchByVersionCompleted(this, new searchByVersionCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x06000ECD RID: 3789 RVA: 0x00014914 File Offset: 0x00012B14
		[SoapRpcMethod("", RequestNamespace = "http://search.anonymous.ws.listingapi.gg.com", ResponseNamespace = "http://search.anonymous.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public searchServiceCategoryResponse searchForCategories(string categoryCode, string lang)
		{
			object[] array = base.Invoke("searchForCategories", new object[]
			{
				categoryCode,
				lang
			});
			return (searchServiceCategoryResponse)array[0];
		}

		// Token: 0x06000ECE RID: 3790 RVA: 0x00014945 File Offset: 0x00012B45
		public void searchForCategoriesAsync(string categoryCode, string lang)
		{
			this.searchForCategoriesAsync(categoryCode, lang, null);
		}

		// Token: 0x06000ECF RID: 3791 RVA: 0x00014950 File Offset: 0x00012B50
		public void searchForCategoriesAsync(string categoryCode, string lang, object userState)
		{
			if (this.searchForCategoriesOperationCompleted == null)
			{
				this.searchForCategoriesOperationCompleted = new SendOrPostCallback(this.OnsearchForCategoriesOperationCompleted);
			}
			base.InvokeAsync("searchForCategories", new object[]
			{
				categoryCode,
				lang
			}, this.searchForCategoriesOperationCompleted, userState);
		}

		// Token: 0x06000ED0 RID: 3792 RVA: 0x0001499C File Offset: 0x00012B9C
		private void OnsearchForCategoriesOperationCompleted(object arg)
		{
			if (this.searchForCategoriesCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.searchForCategoriesCompleted(this, new searchForCategoriesCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x06000ED1 RID: 3793 RVA: 0x000149E1 File Offset: 0x00012BE1
		public new void CancelAsync(object userState)
		{
			base.CancelAsync(userState);
		}

		// Token: 0x06000ED2 RID: 3794 RVA: 0x000149EC File Offset: 0x00012BEC
		private bool IsLocalFileSystemWebService(string url)
		{
			if (url == null || url == string.Empty)
			{
				return false;
			}
			Uri uri = new Uri(url);
			return uri.Port >= 1024 && string.Compare(uri.Host, "localHost", StringComparison.OrdinalIgnoreCase) == 0;
		}

		// Token: 0x04000569 RID: 1385
		private SendOrPostCallback getServiceNameOperationCompleted;

		// Token: 0x0400056A RID: 1386
		private SendOrPostCallback searchOperationCompleted;

		// Token: 0x0400056B RID: 1387
		private SendOrPostCallback searchByVersionOperationCompleted;

		// Token: 0x0400056C RID: 1388
		private SendOrPostCallback searchForCategoriesOperationCompleted;

		// Token: 0x0400056D RID: 1389
		private bool useDefaultCredentialsSetExplicitly;
	}
}
