﻿using System.CodeDom.Compiler;

namespace ApiV2Client.GittiGidiyor.Search
{
	// Token: 0x020001C5 RID: 453
	// (Invoke) Token: 0x06000FC5 RID: 4037
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	public delegate void searchCompletedEventHandler(object sender, searchCompletedEventArgs e);
}
