﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Search
{
	// Token: 0x020001BF RID: 447
	[XmlType(Namespace = "http://search.anonymous.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[XmlInclude(typeof(searchServiceCategoryResponse))]
	[XmlInclude(typeof(searchServiceResponse))]
	[Serializable]
	public class baseResponse
	{
		// Token: 0x170004E0 RID: 1248
		// (get) Token: 0x06000F92 RID: 3986 RVA: 0x00015089 File Offset: 0x00013289
		// (set) Token: 0x06000F93 RID: 3987 RVA: 0x00015091 File Offset: 0x00013291
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string ackCode
		{
			get
			{
				return this.ackCodeField;
			}
			set
			{
				this.ackCodeField = value;
			}
		}

		// Token: 0x170004E1 RID: 1249
		// (get) Token: 0x06000F94 RID: 3988 RVA: 0x0001509A File Offset: 0x0001329A
		// (set) Token: 0x06000F95 RID: 3989 RVA: 0x000150A2 File Offset: 0x000132A2
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string responseTime
		{
			get
			{
				return this.responseTimeField;
			}
			set
			{
				this.responseTimeField = value;
			}
		}

		// Token: 0x170004E2 RID: 1250
		// (get) Token: 0x06000F96 RID: 3990 RVA: 0x000150AB File Offset: 0x000132AB
		// (set) Token: 0x06000F97 RID: 3991 RVA: 0x000150B3 File Offset: 0x000132B3
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public errorType error
		{
			get
			{
				return this.errorField;
			}
			set
			{
				this.errorField = value;
			}
		}

		// Token: 0x170004E3 RID: 1251
		// (get) Token: 0x06000F98 RID: 3992 RVA: 0x000150BC File Offset: 0x000132BC
		// (set) Token: 0x06000F99 RID: 3993 RVA: 0x000150C4 File Offset: 0x000132C4
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string timeElapsed
		{
			get
			{
				return this.timeElapsedField;
			}
			set
			{
				this.timeElapsedField = value;
			}
		}

		// Token: 0x040005CE RID: 1486
		private string ackCodeField;

		// Token: 0x040005CF RID: 1487
		private string responseTimeField;

		// Token: 0x040005D0 RID: 1488
		private errorType errorField;

		// Token: 0x040005D1 RID: 1489
		private string timeElapsedField;
	}
}
