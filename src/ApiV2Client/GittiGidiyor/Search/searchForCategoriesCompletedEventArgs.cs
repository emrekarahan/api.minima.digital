﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Search
{
	// Token: 0x020001CA RID: 458
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	public class searchForCategoriesCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000FD4 RID: 4052 RVA: 0x00015275 File Offset: 0x00013475
		internal searchForCategoriesCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x170004F7 RID: 1271
		// (get) Token: 0x06000FD5 RID: 4053 RVA: 0x00015288 File Offset: 0x00013488
		public searchServiceCategoryResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (searchServiceCategoryResponse)this.results[0];
			}
		}

		// Token: 0x040005E5 RID: 1509
		private object[] results;
	}
}
