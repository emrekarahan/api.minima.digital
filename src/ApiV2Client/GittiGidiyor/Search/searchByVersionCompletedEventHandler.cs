﻿using System.CodeDom.Compiler;

namespace ApiV2Client.GittiGidiyor.Search
{
	// Token: 0x020001C7 RID: 455
	// (Invoke) Token: 0x06000FCB RID: 4043
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	public delegate void searchByVersionCompletedEventHandler(object sender, searchByVersionCompletedEventArgs e);
}
