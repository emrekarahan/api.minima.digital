﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Search
{
	// Token: 0x020001C4 RID: 452
	[DebuggerStepThrough]
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	[DesignerCategory("code")]
	public class getServiceNameCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000FC2 RID: 4034 RVA: 0x000151FD File Offset: 0x000133FD
		internal getServiceNameCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x170004F4 RID: 1268
		// (get) Token: 0x06000FC3 RID: 4035 RVA: 0x00015210 File Offset: 0x00013410
		public string Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (string)this.results[0];
			}
		}

		// Token: 0x040005E2 RID: 1506
		private object[] results;
	}
}
