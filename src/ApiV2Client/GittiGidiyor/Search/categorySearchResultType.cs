﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Search
{
	// Token: 0x020001BC RID: 444
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[XmlType(Namespace = "http://search.anonymous.ws.listingapi.gg.com")]
	[Serializable]
	public class categorySearchResultType
	{
		// Token: 0x170004BA RID: 1210
		// (get) Token: 0x06000F43 RID: 3907 RVA: 0x00014DEB File Offset: 0x00012FEB
		// (set) Token: 0x06000F44 RID: 3908 RVA: 0x00014DF3 File Offset: 0x00012FF3
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string code
		{
			get
			{
				return this.codeField;
			}
			set
			{
				this.codeField = value;
			}
		}

		// Token: 0x170004BB RID: 1211
		// (get) Token: 0x06000F45 RID: 3909 RVA: 0x00014DFC File Offset: 0x00012FFC
		// (set) Token: 0x06000F46 RID: 3910 RVA: 0x00014E04 File Offset: 0x00013004
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string name
		{
			get
			{
				return this.nameField;
			}
			set
			{
				this.nameField = value;
			}
		}

		// Token: 0x170004BC RID: 1212
		// (get) Token: 0x06000F47 RID: 3911 RVA: 0x00014E0D File Offset: 0x0001300D
		// (set) Token: 0x06000F48 RID: 3912 RVA: 0x00014E15 File Offset: 0x00013015
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public long count
		{
			get
			{
				return this.countField;
			}
			set
			{
				this.countField = value;
			}
		}

		// Token: 0x170004BD RID: 1213
		// (get) Token: 0x06000F49 RID: 3913 RVA: 0x00014E1E File Offset: 0x0001301E
		// (set) Token: 0x06000F4A RID: 3914 RVA: 0x00014E26 File Offset: 0x00013026
		[XmlIgnore]
		public bool countSpecified
		{
			get
			{
				return this.countFieldSpecified;
			}
			set
			{
				this.countFieldSpecified = value;
			}
		}

		// Token: 0x170004BE RID: 1214
		// (get) Token: 0x06000F4B RID: 3915 RVA: 0x00014E2F File Offset: 0x0001302F
		// (set) Token: 0x06000F4C RID: 3916 RVA: 0x00014E37 File Offset: 0x00013037
		[XmlAttribute]
		public bool deepest
		{
			get
			{
				return this.deepestField;
			}
			set
			{
				this.deepestField = value;
			}
		}

		// Token: 0x170004BF RID: 1215
		// (get) Token: 0x06000F4D RID: 3917 RVA: 0x00014E40 File Offset: 0x00013040
		// (set) Token: 0x06000F4E RID: 3918 RVA: 0x00014E48 File Offset: 0x00013048
		[XmlIgnore]
		public bool deepestSpecified
		{
			get
			{
				return this.deepestFieldSpecified;
			}
			set
			{
				this.deepestFieldSpecified = value;
			}
		}

		// Token: 0x040005A8 RID: 1448
		private string codeField;

		// Token: 0x040005A9 RID: 1449
		private string nameField;

		// Token: 0x040005AA RID: 1450
		private long countField;

		// Token: 0x040005AB RID: 1451
		private bool countFieldSpecified;

		// Token: 0x040005AC RID: 1452
		private bool deepestField;

		// Token: 0x040005AD RID: 1453
		private bool deepestFieldSpecified;
	}
}
