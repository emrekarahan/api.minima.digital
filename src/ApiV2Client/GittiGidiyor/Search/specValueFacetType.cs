﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Search
{
	// Token: 0x020001BA RID: 442
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[XmlType(Namespace = "http://search.anonymous.ws.listingapi.gg.com")]
	[DesignerCategory("code")]
	[DebuggerStepThrough]
	[Serializable]
	public class specValueFacetType
	{
		// Token: 0x170004B5 RID: 1205
		// (get) Token: 0x06000F37 RID: 3895 RVA: 0x00014D86 File Offset: 0x00012F86
		// (set) Token: 0x06000F38 RID: 3896 RVA: 0x00014D8E File Offset: 0x00012F8E
		[XmlAttribute]
		public string value
		{
			get
			{
				return this.valueField;
			}
			set
			{
				this.valueField = value;
			}
		}

		// Token: 0x170004B6 RID: 1206
		// (get) Token: 0x06000F39 RID: 3897 RVA: 0x00014D97 File Offset: 0x00012F97
		// (set) Token: 0x06000F3A RID: 3898 RVA: 0x00014D9F File Offset: 0x00012F9F
		[XmlAttribute]
		public long count
		{
			get
			{
				return this.countField;
			}
			set
			{
				this.countField = value;
			}
		}

		// Token: 0x170004B7 RID: 1207
		// (get) Token: 0x06000F3B RID: 3899 RVA: 0x00014DA8 File Offset: 0x00012FA8
		// (set) Token: 0x06000F3C RID: 3900 RVA: 0x00014DB0 File Offset: 0x00012FB0
		[XmlIgnore]
		public bool countSpecified
		{
			get
			{
				return this.countFieldSpecified;
			}
			set
			{
				this.countFieldSpecified = value;
			}
		}

		// Token: 0x040005A3 RID: 1443
		private string valueField;

		// Token: 0x040005A4 RID: 1444
		private long countField;

		// Token: 0x040005A5 RID: 1445
		private bool countFieldSpecified;
	}
}
