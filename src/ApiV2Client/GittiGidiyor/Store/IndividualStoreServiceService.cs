﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading;
using System.Web.Services;
using System.Web.Services.Description;
using System.Web.Services.Protocols;
using System.Xml.Serialization;
using ApiV2Client.Properties;

namespace ApiV2Client.GittiGidiyor.Store
{
	// Token: 0x020001CB RID: 459
	[DebuggerStepThrough]
	[WebServiceBinding(Name = "IndividualStoreServiceBinding", Namespace = "http://store.individual.ws.listingapi.gg.com")]
	[XmlInclude(typeof(baseResponse))]
	[DesignerCategory("code")]
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	public class IndividualStoreServiceService : SoapHttpClientProtocol
	{
		// Token: 0x06000FD6 RID: 4054 RVA: 0x0001529D File Offset: 0x0001349D
		public IndividualStoreServiceService()
		{
			this.Url = Settings.Default.ApiV2Client_GittiGidiyor_Store_IndividualStoreServiceService;
			if (this.IsLocalFileSystemWebService(this.Url))
			{
				this.UseDefaultCredentials = true;
				this.useDefaultCredentialsSetExplicitly = false;
				return;
			}
			this.useDefaultCredentialsSetExplicitly = true;
		}

		// Token: 0x170004F8 RID: 1272
		// (get) Token: 0x06000FD7 RID: 4055 RVA: 0x000152D9 File Offset: 0x000134D9
		// (set) Token: 0x06000FD8 RID: 4056 RVA: 0x000152E1 File Offset: 0x000134E1
		public new string Url
		{
			get
			{
				return base.Url;
			}
			set
			{
				if (this.IsLocalFileSystemWebService(base.Url) && !this.useDefaultCredentialsSetExplicitly && !this.IsLocalFileSystemWebService(value))
				{
					base.UseDefaultCredentials = false;
				}
				base.Url = value;
			}
		}

		// Token: 0x170004F9 RID: 1273
		// (get) Token: 0x06000FD9 RID: 4057 RVA: 0x00015310 File Offset: 0x00013510
		// (set) Token: 0x06000FDA RID: 4058 RVA: 0x00015318 File Offset: 0x00013518
		public new bool UseDefaultCredentials
		{
			get
			{
				return base.UseDefaultCredentials;
			}
			set
			{
				base.UseDefaultCredentials = value;
				this.useDefaultCredentialsSetExplicitly = true;
			}
		}

		// Token: 0x14000082 RID: 130
		// (add) Token: 0x06000FDB RID: 4059 RVA: 0x00015328 File Offset: 0x00013528
		// (remove) Token: 0x06000FDC RID: 4060 RVA: 0x00015360 File Offset: 0x00013560
		public event getServiceNameCompletedEventHandler getServiceNameCompleted;

		// Token: 0x14000083 RID: 131
		// (add) Token: 0x06000FDD RID: 4061 RVA: 0x00015398 File Offset: 0x00013598
		// (remove) Token: 0x06000FDE RID: 4062 RVA: 0x000153D0 File Offset: 0x000135D0
		public event getStoreCompletedEventHandler getStoreCompleted;

		// Token: 0x06000FDF RID: 4063 RVA: 0x00015408 File Offset: 0x00013608
		[SoapRpcMethod("", RequestNamespace = "http://store.individual.ws.listingapi.gg.com", ResponseNamespace = "http://store.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public string getServiceName()
		{
			object[] array = base.Invoke("getServiceName", new object[0]);
			return (string)array[0];
		}

		// Token: 0x06000FE0 RID: 4064 RVA: 0x0001542F File Offset: 0x0001362F
		public void getServiceNameAsync()
		{
			this.getServiceNameAsync(null);
		}

		// Token: 0x06000FE1 RID: 4065 RVA: 0x00015438 File Offset: 0x00013638
		public void getServiceNameAsync(object userState)
		{
			if (this.getServiceNameOperationCompleted == null)
			{
				this.getServiceNameOperationCompleted = new SendOrPostCallback(this.OngetServiceNameOperationCompleted);
			}
			base.InvokeAsync("getServiceName", new object[0], this.getServiceNameOperationCompleted, userState);
		}

		// Token: 0x06000FE2 RID: 4066 RVA: 0x0001546C File Offset: 0x0001366C
		private void OngetServiceNameOperationCompleted(object arg)
		{
			if (this.getServiceNameCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getServiceNameCompleted(this, new getServiceNameCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x06000FE3 RID: 4067 RVA: 0x000154B4 File Offset: 0x000136B4
		[SoapRpcMethod("", RequestNamespace = "http://store.individual.ws.listingapi.gg.com", ResponseNamespace = "http://store.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public storeServiceResponse getStore(string apiKey, string sign, long time, string lang)
		{
			object[] array = base.Invoke("getStore", new object[]
			{
				apiKey,
				sign,
				time,
				lang
			});
			return (storeServiceResponse)array[0];
		}

		// Token: 0x06000FE4 RID: 4068 RVA: 0x000154F3 File Offset: 0x000136F3
		public void getStoreAsync(string apiKey, string sign, long time, string lang)
		{
			this.getStoreAsync(apiKey, sign, time, lang, null);
		}

		// Token: 0x06000FE5 RID: 4069 RVA: 0x00015504 File Offset: 0x00013704
		public void getStoreAsync(string apiKey, string sign, long time, string lang, object userState)
		{
			if (this.getStoreOperationCompleted == null)
			{
				this.getStoreOperationCompleted = new SendOrPostCallback(this.OngetStoreOperationCompleted);
			}
			base.InvokeAsync("getStore", new object[]
			{
				apiKey,
				sign,
				time,
				lang
			}, this.getStoreOperationCompleted, userState);
		}

		// Token: 0x06000FE6 RID: 4070 RVA: 0x0001555C File Offset: 0x0001375C
		private void OngetStoreOperationCompleted(object arg)
		{
			if (this.getStoreCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getStoreCompleted(this, new getStoreCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x06000FE7 RID: 4071 RVA: 0x000155A1 File Offset: 0x000137A1
		public new void CancelAsync(object userState)
		{
			base.CancelAsync(userState);
		}

		// Token: 0x06000FE8 RID: 4072 RVA: 0x000155AC File Offset: 0x000137AC
		private bool IsLocalFileSystemWebService(string url)
		{
			if (url == null || url == string.Empty)
			{
				return false;
			}
			Uri uri = new Uri(url);
			return uri.Port >= 1024 && string.Compare(uri.Host, "localHost", StringComparison.OrdinalIgnoreCase) == 0;
		}

		// Token: 0x040005E6 RID: 1510
		private SendOrPostCallback getServiceNameOperationCompleted;

		// Token: 0x040005E7 RID: 1511
		private SendOrPostCallback getStoreOperationCompleted;

		// Token: 0x040005E8 RID: 1512
		private bool useDefaultCredentialsSetExplicitly;
	}
}
