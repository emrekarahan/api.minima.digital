﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Store
{
	// Token: 0x020001CD RID: 461
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[XmlType(Namespace = "http://store.individual.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[Serializable]
	public class storeServiceResponse : baseResponse
	{
		// Token: 0x170004FE RID: 1278
		// (get) Token: 0x06000FF2 RID: 4082 RVA: 0x00015641 File Offset: 0x00013841
		// (set) Token: 0x06000FF3 RID: 4083 RVA: 0x00015649 File Offset: 0x00013849
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public storeType store
		{
			get
			{
				return this.storeField;
			}
			set
			{
				this.storeField = value;
			}
		}

		// Token: 0x040005EF RID: 1519
		private storeType storeField;
	}
}
