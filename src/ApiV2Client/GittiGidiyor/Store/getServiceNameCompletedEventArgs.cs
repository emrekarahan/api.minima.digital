﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Store
{
	// Token: 0x020001D2 RID: 466
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	public class getServiceNameCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x0600101A RID: 4122 RVA: 0x00015771 File Offset: 0x00013971
		internal getServiceNameCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x1700050E RID: 1294
		// (get) Token: 0x0600101B RID: 4123 RVA: 0x00015784 File Offset: 0x00013984
		public string Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (string)this.results[0];
			}
		}

		// Token: 0x040005FF RID: 1535
		private object[] results;
	}
}
