﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Store
{
	// Token: 0x020001D0 RID: 464
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[DesignerCategory("code")]
	[XmlType(Namespace = "http://store.individual.ws.listingapi.gg.com")]
	[DebuggerStepThrough]
	[Serializable]
	public class errorType
	{
		// Token: 0x1700050A RID: 1290
		// (get) Token: 0x0600100D RID: 4109 RVA: 0x00015725 File Offset: 0x00013925
		// (set) Token: 0x0600100E RID: 4110 RVA: 0x0001572D File Offset: 0x0001392D
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string errorId
		{
			get
			{
				return this.errorIdField;
			}
			set
			{
				this.errorIdField = value;
			}
		}

		// Token: 0x1700050B RID: 1291
		// (get) Token: 0x0600100F RID: 4111 RVA: 0x00015736 File Offset: 0x00013936
		// (set) Token: 0x06001010 RID: 4112 RVA: 0x0001573E File Offset: 0x0001393E
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string errorCode
		{
			get
			{
				return this.errorCodeField;
			}
			set
			{
				this.errorCodeField = value;
			}
		}

		// Token: 0x1700050C RID: 1292
		// (get) Token: 0x06001011 RID: 4113 RVA: 0x00015747 File Offset: 0x00013947
		// (set) Token: 0x06001012 RID: 4114 RVA: 0x0001574F File Offset: 0x0001394F
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string message
		{
			get
			{
				return this.messageField;
			}
			set
			{
				this.messageField = value;
			}
		}

		// Token: 0x1700050D RID: 1293
		// (get) Token: 0x06001013 RID: 4115 RVA: 0x00015758 File Offset: 0x00013958
		// (set) Token: 0x06001014 RID: 4116 RVA: 0x00015760 File Offset: 0x00013960
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string viewMessage
		{
			get
			{
				return this.viewMessageField;
			}
			set
			{
				this.viewMessageField = value;
			}
		}

		// Token: 0x040005FB RID: 1531
		private string errorIdField;

		// Token: 0x040005FC RID: 1532
		private string errorCodeField;

		// Token: 0x040005FD RID: 1533
		private string messageField;

		// Token: 0x040005FE RID: 1534
		private string viewMessageField;
	}
}
