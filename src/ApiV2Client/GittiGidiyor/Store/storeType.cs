﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Store
{
	// Token: 0x020001CE RID: 462
	[DesignerCategory("code")]
	[DebuggerStepThrough]
	[XmlType(Namespace = "http://store.individual.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[Serializable]
	public class storeType
	{
		// Token: 0x170004FF RID: 1279
		// (get) Token: 0x06000FF5 RID: 4085 RVA: 0x0001565A File Offset: 0x0001385A
		// (set) Token: 0x06000FF6 RID: 4086 RVA: 0x00015662 File Offset: 0x00013862
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string name
		{
			get
			{
				return this.nameField;
			}
			set
			{
				this.nameField = value;
			}
		}

		// Token: 0x17000500 RID: 1280
		// (get) Token: 0x06000FF7 RID: 4087 RVA: 0x0001566B File Offset: 0x0001386B
		// (set) Token: 0x06000FF8 RID: 4088 RVA: 0x00015673 File Offset: 0x00013873
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string endDate
		{
			get
			{
				return this.endDateField;
			}
			set
			{
				this.endDateField = value;
			}
		}

		// Token: 0x17000501 RID: 1281
		// (get) Token: 0x06000FF9 RID: 4089 RVA: 0x0001567C File Offset: 0x0001387C
		// (set) Token: 0x06000FFA RID: 4090 RVA: 0x00015684 File Offset: 0x00013884
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string lastUpdate
		{
			get
			{
				return this.lastUpdateField;
			}
			set
			{
				this.lastUpdateField = value;
			}
		}

		// Token: 0x17000502 RID: 1282
		// (get) Token: 0x06000FFB RID: 4091 RVA: 0x0001568D File Offset: 0x0001388D
		// (set) Token: 0x06000FFC RID: 4092 RVA: 0x00015695 File Offset: 0x00013895
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string registerDate
		{
			get
			{
				return this.registerDateField;
			}
			set
			{
				this.registerDateField = value;
			}
		}

		// Token: 0x17000503 RID: 1283
		// (get) Token: 0x06000FFD RID: 4093 RVA: 0x0001569E File Offset: 0x0001389E
		// (set) Token: 0x06000FFE RID: 4094 RVA: 0x000156A6 File Offset: 0x000138A6
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public bool isSubscribed
		{
			get
			{
				return this.isSubscribedField;
			}
			set
			{
				this.isSubscribedField = value;
			}
		}

		// Token: 0x17000504 RID: 1284
		// (get) Token: 0x06000FFF RID: 4095 RVA: 0x000156AF File Offset: 0x000138AF
		// (set) Token: 0x06001000 RID: 4096 RVA: 0x000156B7 File Offset: 0x000138B7
		[XmlElement("storeType", Form = XmlSchemaForm.Unqualified)]
		public int storeType1
		{
			get
			{
				return this.storeType1Field;
			}
			set
			{
				this.storeType1Field = value;
			}
		}

		// Token: 0x17000505 RID: 1285
		// (get) Token: 0x06001001 RID: 4097 RVA: 0x000156C0 File Offset: 0x000138C0
		// (set) Token: 0x06001002 RID: 4098 RVA: 0x000156C8 File Offset: 0x000138C8
		[XmlIgnore]
		public bool storeType1Specified
		{
			get
			{
				return this.storeType1FieldSpecified;
			}
			set
			{
				this.storeType1FieldSpecified = value;
			}
		}

		// Token: 0x17000506 RID: 1286
		// (get) Token: 0x06001003 RID: 4099 RVA: 0x000156D1 File Offset: 0x000138D1
		// (set) Token: 0x06001004 RID: 4100 RVA: 0x000156D9 File Offset: 0x000138D9
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		[XmlArrayItem("category", Form = XmlSchemaForm.Unqualified)]
		public storeCategoryType[] categories
		{
			get
			{
				return this.categoriesField;
			}
			set
			{
				this.categoriesField = value;
			}
		}

		// Token: 0x040005F0 RID: 1520
		private string nameField;

		// Token: 0x040005F1 RID: 1521
		private string endDateField;

		// Token: 0x040005F2 RID: 1522
		private string lastUpdateField;

		// Token: 0x040005F3 RID: 1523
		private string registerDateField;

		// Token: 0x040005F4 RID: 1524
		private bool isSubscribedField;

		// Token: 0x040005F5 RID: 1525
		private int storeType1Field;

		// Token: 0x040005F6 RID: 1526
		private bool storeType1FieldSpecified;

		// Token: 0x040005F7 RID: 1527
		private storeCategoryType[] categoriesField;
	}
}
