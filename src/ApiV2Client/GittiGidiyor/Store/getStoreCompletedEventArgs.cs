﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Store
{
	// Token: 0x020001D4 RID: 468
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	public class getStoreCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06001020 RID: 4128 RVA: 0x00015799 File Offset: 0x00013999
		internal getStoreCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x1700050F RID: 1295
		// (get) Token: 0x06001021 RID: 4129 RVA: 0x000157AC File Offset: 0x000139AC
		public storeServiceResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (storeServiceResponse)this.results[0];
			}
		}

		// Token: 0x04000600 RID: 1536
		private object[] results;
	}
}
