﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Store
{
	// Token: 0x020001CC RID: 460
	[XmlType(Namespace = "http://store.individual.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[DesignerCategory("code")]
	[XmlInclude(typeof(storeServiceResponse))]
	[DebuggerStepThrough]
	[Serializable]
	public class baseResponse
	{
		// Token: 0x170004FA RID: 1274
		// (get) Token: 0x06000FE9 RID: 4073 RVA: 0x000155F5 File Offset: 0x000137F5
		// (set) Token: 0x06000FEA RID: 4074 RVA: 0x000155FD File Offset: 0x000137FD
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string ackCode
		{
			get
			{
				return this.ackCodeField;
			}
			set
			{
				this.ackCodeField = value;
			}
		}

		// Token: 0x170004FB RID: 1275
		// (get) Token: 0x06000FEB RID: 4075 RVA: 0x00015606 File Offset: 0x00013806
		// (set) Token: 0x06000FEC RID: 4076 RVA: 0x0001560E File Offset: 0x0001380E
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string responseTime
		{
			get
			{
				return this.responseTimeField;
			}
			set
			{
				this.responseTimeField = value;
			}
		}

		// Token: 0x170004FC RID: 1276
		// (get) Token: 0x06000FED RID: 4077 RVA: 0x00015617 File Offset: 0x00013817
		// (set) Token: 0x06000FEE RID: 4078 RVA: 0x0001561F File Offset: 0x0001381F
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public errorType error
		{
			get
			{
				return this.errorField;
			}
			set
			{
				this.errorField = value;
			}
		}

		// Token: 0x170004FD RID: 1277
		// (get) Token: 0x06000FEF RID: 4079 RVA: 0x00015628 File Offset: 0x00013828
		// (set) Token: 0x06000FF0 RID: 4080 RVA: 0x00015630 File Offset: 0x00013830
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string timeElapsed
		{
			get
			{
				return this.timeElapsedField;
			}
			set
			{
				this.timeElapsedField = value;
			}
		}

		// Token: 0x040005EB RID: 1515
		private string ackCodeField;

		// Token: 0x040005EC RID: 1516
		private string responseTimeField;

		// Token: 0x040005ED RID: 1517
		private errorType errorField;

		// Token: 0x040005EE RID: 1518
		private string timeElapsedField;
	}
}
