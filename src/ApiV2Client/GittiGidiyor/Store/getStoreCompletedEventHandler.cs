﻿using System.CodeDom.Compiler;

namespace ApiV2Client.GittiGidiyor.Store
{
	// Token: 0x020001D3 RID: 467
	// (Invoke) Token: 0x0600101D RID: 4125
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	public delegate void getStoreCompletedEventHandler(object sender, getStoreCompletedEventArgs e);
}
