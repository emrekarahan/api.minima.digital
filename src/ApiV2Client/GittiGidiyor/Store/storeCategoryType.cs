﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Store
{
	// Token: 0x020001CF RID: 463
	[DebuggerStepThrough]
	[XmlType(Namespace = "http://store.individual.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[DesignerCategory("code")]
	[Serializable]
	public class storeCategoryType
	{
		// Token: 0x17000507 RID: 1287
		// (get) Token: 0x06001006 RID: 4102 RVA: 0x000156EA File Offset: 0x000138EA
		// (set) Token: 0x06001007 RID: 4103 RVA: 0x000156F2 File Offset: 0x000138F2
		[XmlAttribute]
		public int id
		{
			get
			{
				return this.idField;
			}
			set
			{
				this.idField = value;
			}
		}

		// Token: 0x17000508 RID: 1288
		// (get) Token: 0x06001008 RID: 4104 RVA: 0x000156FB File Offset: 0x000138FB
		// (set) Token: 0x06001009 RID: 4105 RVA: 0x00015703 File Offset: 0x00013903
		[XmlIgnore]
		public bool idSpecified
		{
			get
			{
				return this.idFieldSpecified;
			}
			set
			{
				this.idFieldSpecified = value;
			}
		}

		// Token: 0x17000509 RID: 1289
		// (get) Token: 0x0600100A RID: 4106 RVA: 0x0001570C File Offset: 0x0001390C
		// (set) Token: 0x0600100B RID: 4107 RVA: 0x00015714 File Offset: 0x00013914
		[XmlAttribute]
		public string name
		{
			get
			{
				return this.nameField;
			}
			set
			{
				this.nameField = value;
			}
		}

		// Token: 0x040005F8 RID: 1528
		private int idField;

		// Token: 0x040005F9 RID: 1529
		private bool idFieldSpecified;

		// Token: 0x040005FA RID: 1530
		private string nameField;
	}
}
