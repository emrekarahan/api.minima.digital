﻿namespace ApiV2Client.GittiGidiyor.Store
{
	// Token: 0x020001D9 RID: 473
	public interface IStoreService : IService
	{
		// Token: 0x06001034 RID: 4148
		storeServiceResponse getStore(string lang);
	}
}
