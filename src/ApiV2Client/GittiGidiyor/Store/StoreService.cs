﻿using System;

namespace ApiV2Client.GittiGidiyor.Store
{
	// Token: 0x020001E6 RID: 486
	public class StoreService : ServiceClient<IndividualStoreServiceService>, IStoreService, IService
	{
		// Token: 0x06001087 RID: 4231 RVA: 0x00015FB8 File Offset: 0x000141B8
		private StoreService()
		{
			base.configureNetworkCredentials(this.service);
		}

		// Token: 0x06001088 RID: 4232 RVA: 0x00015FD7 File Offset: 0x000141D7
		public string getServiceName()
		{
			return this.service.getServiceName();
		}

		// Token: 0x06001089 RID: 4233 RVA: 0x00015FE4 File Offset: 0x000141E4
		public storeServiceResponse getStore(string lang)
		{
			long time = Util.convertToTimestamp(DateTime.Now);
			return this.service.getStore(this.authConfig.ApiKey, base.getSignature(time), time, lang);
		}

		// Token: 0x17000517 RID: 1303
		// (get) Token: 0x0600108A RID: 4234 RVA: 0x0001601C File Offset: 0x0001421C
		public static StoreService Instance
		{
			get
			{
				if (StoreService.instance == null)
				{
					lock (StoreService.lockObject)
					{
						if (StoreService.instance == null)
						{
							StoreService.instance = new StoreService();
						}
					}
				}
				return StoreService.instance;
			}
		}

		// Token: 0x04000616 RID: 1558
		private static StoreService instance;

		// Token: 0x04000617 RID: 1559
		private static object lockObject = new object();

		// Token: 0x04000618 RID: 1560
		private IndividualStoreServiceService service = new IndividualStoreServiceService();
	}
}
