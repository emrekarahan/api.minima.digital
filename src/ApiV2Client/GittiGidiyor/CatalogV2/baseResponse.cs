﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.CatalogV2
{
	// Token: 0x0200006D RID: 109
	[XmlInclude(typeof(simpleCatalogServiceListResponse))]
	[XmlInclude(typeof(catalogSearchServiceResponse))]
	[DebuggerStepThrough]
	[XmlType(Namespace = "http://catalogv2.anonymous.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.7.3190.0")]
	[DesignerCategory("code")]
	[Serializable]
	public class baseResponse
	{
		// Token: 0x1700017A RID: 378
		// (get) Token: 0x0600042B RID: 1067 RVA: 0x000068F6 File Offset: 0x00004AF6
		// (set) Token: 0x0600042C RID: 1068 RVA: 0x000068FE File Offset: 0x00004AFE
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string ackCode
		{
			get
			{
				return this.ackCodeField;
			}
			set
			{
				this.ackCodeField = value;
			}
		}

		// Token: 0x1700017B RID: 379
		// (get) Token: 0x0600042D RID: 1069 RVA: 0x00006907 File Offset: 0x00004B07
		// (set) Token: 0x0600042E RID: 1070 RVA: 0x0000690F File Offset: 0x00004B0F
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string responseTime
		{
			get
			{
				return this.responseTimeField;
			}
			set
			{
				this.responseTimeField = value;
			}
		}

		// Token: 0x1700017C RID: 380
		// (get) Token: 0x0600042F RID: 1071 RVA: 0x00006918 File Offset: 0x00004B18
		// (set) Token: 0x06000430 RID: 1072 RVA: 0x00006920 File Offset: 0x00004B20
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public errorType error
		{
			get
			{
				return this.errorField;
			}
			set
			{
				this.errorField = value;
			}
		}

		// Token: 0x1700017D RID: 381
		// (get) Token: 0x06000431 RID: 1073 RVA: 0x00006929 File Offset: 0x00004B29
		// (set) Token: 0x06000432 RID: 1074 RVA: 0x00006931 File Offset: 0x00004B31
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string timeElapsed
		{
			get
			{
				return this.timeElapsedField;
			}
			set
			{
				this.timeElapsedField = value;
			}
		}

		// Token: 0x040001A0 RID: 416
		private string ackCodeField;

		// Token: 0x040001A1 RID: 417
		private string responseTimeField;

		// Token: 0x040001A2 RID: 418
		private errorType errorField;

		// Token: 0x040001A3 RID: 419
		private string timeElapsedField;
	}
}
