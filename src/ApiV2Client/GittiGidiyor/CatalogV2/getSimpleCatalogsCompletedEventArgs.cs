﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.CatalogV2
{
	// Token: 0x02000073 RID: 115
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	public class getSimpleCatalogsCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x0600044E RID: 1102 RVA: 0x000069F1 File Offset: 0x00004BF1
		internal getSimpleCatalogsCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x17000186 RID: 390
		// (get) Token: 0x0600044F RID: 1103 RVA: 0x00006A04 File Offset: 0x00004C04
		public simpleCatalogServiceListResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (simpleCatalogServiceListResponse)this.results[0];
			}
		}

		// Token: 0x040001AC RID: 428
		private object[] results;
	}
}
