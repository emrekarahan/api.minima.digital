﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.CatalogV2
{
	// Token: 0x0200006F RID: 111
	[XmlType(Namespace = "http://catalogv2.anonymous.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.7.3190.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[Serializable]
	public class simpleCatalogServiceListResponse : baseResponse
	{
		// Token: 0x17000182 RID: 386
		// (get) Token: 0x0600043D RID: 1085 RVA: 0x0000698E File Offset: 0x00004B8E
		// (set) Token: 0x0600043E RID: 1086 RVA: 0x00006996 File Offset: 0x00004B96
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int count
		{
			get
			{
				return this.countField;
			}
			set
			{
				this.countField = value;
			}
		}

		// Token: 0x17000183 RID: 387
		// (get) Token: 0x0600043F RID: 1087 RVA: 0x0000699F File Offset: 0x00004B9F
		// (set) Token: 0x06000440 RID: 1088 RVA: 0x000069A7 File Offset: 0x00004BA7
		[XmlIgnore]
		public bool countSpecified
		{
			get
			{
				return this.countFieldSpecified;
			}
			set
			{
				this.countFieldSpecified = value;
			}
		}

		// Token: 0x17000184 RID: 388
		// (get) Token: 0x06000441 RID: 1089 RVA: 0x000069B0 File Offset: 0x00004BB0
		// (set) Token: 0x06000442 RID: 1090 RVA: 0x000069B8 File Offset: 0x00004BB8
		[XmlArrayItem("catalog", Form = XmlSchemaForm.Unqualified)]
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		public simpleCatalogType[] catalogs
		{
			get
			{
				return this.catalogsField;
			}
			set
			{
				this.catalogsField = value;
			}
		}

		// Token: 0x040001A8 RID: 424
		private int countField;

		// Token: 0x040001A9 RID: 425
		private bool countFieldSpecified;

		// Token: 0x040001AA RID: 426
		private simpleCatalogType[] catalogsField;
	}
}
