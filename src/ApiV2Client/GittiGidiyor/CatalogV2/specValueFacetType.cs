﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.CatalogV2
{
	// Token: 0x02000068 RID: 104
	[XmlType(Namespace = "http://catalogv2.anonymous.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.7.3190.0")]
	[DesignerCategory("code")]
	[DebuggerStepThrough]
	[Serializable]
	public class specValueFacetType
	{
		// Token: 0x1700015E RID: 350
		// (get) Token: 0x060003EE RID: 1006 RVA: 0x000066F2 File Offset: 0x000048F2
		// (set) Token: 0x060003EF RID: 1007 RVA: 0x000066FA File Offset: 0x000048FA
		[XmlAttribute]
		public string value
		{
			get
			{
				return this.valueField;
			}
			set
			{
				this.valueField = value;
			}
		}

		// Token: 0x1700015F RID: 351
		// (get) Token: 0x060003F0 RID: 1008 RVA: 0x00006703 File Offset: 0x00004903
		// (set) Token: 0x060003F1 RID: 1009 RVA: 0x0000670B File Offset: 0x0000490B
		[XmlAttribute]
		public long count
		{
			get
			{
				return this.countField;
			}
			set
			{
				this.countField = value;
			}
		}

		// Token: 0x17000160 RID: 352
		// (get) Token: 0x060003F2 RID: 1010 RVA: 0x00006714 File Offset: 0x00004914
		// (set) Token: 0x060003F3 RID: 1011 RVA: 0x0000671C File Offset: 0x0000491C
		[XmlIgnore]
		public bool countSpecified
		{
			get
			{
				return this.countFieldSpecified;
			}
			set
			{
				this.countFieldSpecified = value;
			}
		}

		// Token: 0x04000184 RID: 388
		private string valueField;

		// Token: 0x04000185 RID: 389
		private long countField;

		// Token: 0x04000186 RID: 390
		private bool countFieldSpecified;
	}
}
