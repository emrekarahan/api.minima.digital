﻿namespace ApiV2Client.GittiGidiyor.CatalogV2
{
	// Token: 0x02000004 RID: 4
	public interface ICatalogServiceV2 : IService
	{
		// Token: 0x06000006 RID: 6
		simpleCatalogServiceListResponse getSimpleCatalogs(string categoryCode, specType[] specs, string lang);
	}
}
