﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.CatalogV2
{
	// Token: 0x0200006A RID: 106
	[XmlType(Namespace = "http://catalogv2.anonymous.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.7.3190.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[Serializable]
	public class categoryType
	{
		// Token: 0x17000163 RID: 355
		// (get) Token: 0x060003FA RID: 1018 RVA: 0x00006757 File Offset: 0x00004957
		// (set) Token: 0x060003FB RID: 1019 RVA: 0x0000675F File Offset: 0x0000495F
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int categoryAttributeId
		{
			get
			{
				return this.categoryAttributeIdField;
			}
			set
			{
				this.categoryAttributeIdField = value;
			}
		}

		// Token: 0x17000164 RID: 356
		// (get) Token: 0x060003FC RID: 1020 RVA: 0x00006768 File Offset: 0x00004968
		// (set) Token: 0x060003FD RID: 1021 RVA: 0x00006770 File Offset: 0x00004970
		[XmlIgnore]
		public bool categoryAttributeIdSpecified
		{
			get
			{
				return this.categoryAttributeIdFieldSpecified;
			}
			set
			{
				this.categoryAttributeIdFieldSpecified = value;
			}
		}

		// Token: 0x17000165 RID: 357
		// (get) Token: 0x060003FE RID: 1022 RVA: 0x00006779 File Offset: 0x00004979
		// (set) Token: 0x060003FF RID: 1023 RVA: 0x00006781 File Offset: 0x00004981
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string categoryCode
		{
			get
			{
				return this.categoryCodeField;
			}
			set
			{
				this.categoryCodeField = value;
			}
		}

		// Token: 0x17000166 RID: 358
		// (get) Token: 0x06000400 RID: 1024 RVA: 0x0000678A File Offset: 0x0000498A
		// (set) Token: 0x06000401 RID: 1025 RVA: 0x00006792 File Offset: 0x00004992
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string categoryName
		{
			get
			{
				return this.categoryNameField;
			}
			set
			{
				this.categoryNameField = value;
			}
		}

		// Token: 0x17000167 RID: 359
		// (get) Token: 0x06000402 RID: 1026 RVA: 0x0000679B File Offset: 0x0000499B
		// (set) Token: 0x06000403 RID: 1027 RVA: 0x000067A3 File Offset: 0x000049A3
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string categoryPerma
		{
			get
			{
				return this.categoryPermaField;
			}
			set
			{
				this.categoryPermaField = value;
			}
		}

		// Token: 0x17000168 RID: 360
		// (get) Token: 0x06000404 RID: 1028 RVA: 0x000067AC File Offset: 0x000049AC
		// (set) Token: 0x06000405 RID: 1029 RVA: 0x000067B4 File Offset: 0x000049B4
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public bool active
		{
			get
			{
				return this.activeField;
			}
			set
			{
				this.activeField = value;
			}
		}

		// Token: 0x17000169 RID: 361
		// (get) Token: 0x06000406 RID: 1030 RVA: 0x000067BD File Offset: 0x000049BD
		// (set) Token: 0x06000407 RID: 1031 RVA: 0x000067C5 File Offset: 0x000049C5
		[XmlIgnore]
		public bool activeSpecified
		{
			get
			{
				return this.activeFieldSpecified;
			}
			set
			{
				this.activeFieldSpecified = value;
			}
		}

		// Token: 0x1700016A RID: 362
		// (get) Token: 0x06000408 RID: 1032 RVA: 0x000067CE File Offset: 0x000049CE
		// (set) Token: 0x06000409 RID: 1033 RVA: 0x000067D6 File Offset: 0x000049D6
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public bool hasCatalog
		{
			get
			{
				return this.hasCatalogField;
			}
			set
			{
				this.hasCatalogField = value;
			}
		}

		// Token: 0x1700016B RID: 363
		// (get) Token: 0x0600040A RID: 1034 RVA: 0x000067DF File Offset: 0x000049DF
		// (set) Token: 0x0600040B RID: 1035 RVA: 0x000067E7 File Offset: 0x000049E7
		[XmlIgnore]
		public bool hasCatalogSpecified
		{
			get
			{
				return this.hasCatalogFieldSpecified;
			}
			set
			{
				this.hasCatalogFieldSpecified = value;
			}
		}

		// Token: 0x1700016C RID: 364
		// (get) Token: 0x0600040C RID: 1036 RVA: 0x000067F0 File Offset: 0x000049F0
		// (set) Token: 0x0600040D RID: 1037 RVA: 0x000067F8 File Offset: 0x000049F8
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public bool hasSpec
		{
			get
			{
				return this.hasSpecField;
			}
			set
			{
				this.hasSpecField = value;
			}
		}

		// Token: 0x1700016D RID: 365
		// (get) Token: 0x0600040E RID: 1038 RVA: 0x00006801 File Offset: 0x00004A01
		// (set) Token: 0x0600040F RID: 1039 RVA: 0x00006809 File Offset: 0x00004A09
		[XmlIgnore]
		public bool hasSpecSpecified
		{
			get
			{
				return this.hasSpecFieldSpecified;
			}
			set
			{
				this.hasSpecFieldSpecified = value;
			}
		}

		// Token: 0x1700016E RID: 366
		// (get) Token: 0x06000410 RID: 1040 RVA: 0x00006812 File Offset: 0x00004A12
		// (set) Token: 0x06000411 RID: 1041 RVA: 0x0000681A File Offset: 0x00004A1A
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int orderNumber
		{
			get
			{
				return this.orderNumberField;
			}
			set
			{
				this.orderNumberField = value;
			}
		}

		// Token: 0x1700016F RID: 367
		// (get) Token: 0x06000412 RID: 1042 RVA: 0x00006823 File Offset: 0x00004A23
		// (set) Token: 0x06000413 RID: 1043 RVA: 0x0000682B File Offset: 0x00004A2B
		[XmlIgnore]
		public bool orderNumberSpecified
		{
			get
			{
				return this.orderNumberFieldSpecified;
			}
			set
			{
				this.orderNumberFieldSpecified = value;
			}
		}

		// Token: 0x17000170 RID: 368
		// (get) Token: 0x06000414 RID: 1044 RVA: 0x00006834 File Offset: 0x00004A34
		// (set) Token: 0x06000415 RID: 1045 RVA: 0x0000683C File Offset: 0x00004A3C
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int installmentNumber
		{
			get
			{
				return this.installmentNumberField;
			}
			set
			{
				this.installmentNumberField = value;
			}
		}

		// Token: 0x17000171 RID: 369
		// (get) Token: 0x06000416 RID: 1046 RVA: 0x00006845 File Offset: 0x00004A45
		// (set) Token: 0x06000417 RID: 1047 RVA: 0x0000684D File Offset: 0x00004A4D
		[XmlIgnore]
		public bool installmentNumberSpecified
		{
			get
			{
				return this.installmentNumberFieldSpecified;
			}
			set
			{
				this.installmentNumberFieldSpecified = value;
			}
		}

		// Token: 0x04000189 RID: 393
		private int categoryAttributeIdField;

		// Token: 0x0400018A RID: 394
		private bool categoryAttributeIdFieldSpecified;

		// Token: 0x0400018B RID: 395
		private string categoryCodeField;

		// Token: 0x0400018C RID: 396
		private string categoryNameField;

		// Token: 0x0400018D RID: 397
		private string categoryPermaField;

		// Token: 0x0400018E RID: 398
		private bool activeField;

		// Token: 0x0400018F RID: 399
		private bool activeFieldSpecified;

		// Token: 0x04000190 RID: 400
		private bool hasCatalogField;

		// Token: 0x04000191 RID: 401
		private bool hasCatalogFieldSpecified;

		// Token: 0x04000192 RID: 402
		private bool hasSpecField;

		// Token: 0x04000193 RID: 403
		private bool hasSpecFieldSpecified;

		// Token: 0x04000194 RID: 404
		private int orderNumberField;

		// Token: 0x04000195 RID: 405
		private bool orderNumberFieldSpecified;

		// Token: 0x04000196 RID: 406
		private int installmentNumberField;

		// Token: 0x04000197 RID: 407
		private bool installmentNumberFieldSpecified;
	}
}
