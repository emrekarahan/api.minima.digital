﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.CatalogV2
{
	// Token: 0x02000071 RID: 113
	[DesignerCategory("code")]
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	[DebuggerStepThrough]
	public class getServiceNameCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000448 RID: 1096 RVA: 0x000069C9 File Offset: 0x00004BC9
		internal getServiceNameCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x17000185 RID: 389
		// (get) Token: 0x06000449 RID: 1097 RVA: 0x000069DC File Offset: 0x00004BDC
		public string Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (string)this.results[0];
			}
		}

		// Token: 0x040001AB RID: 427
		private object[] results;
	}
}
