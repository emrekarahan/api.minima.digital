﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.CatalogV2
{
	// Token: 0x0200006C RID: 108
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[XmlType(Namespace = "http://catalogv2.anonymous.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.7.3190.0")]
	[Serializable]
	public class errorType
	{
		// Token: 0x17000176 RID: 374
		// (get) Token: 0x06000422 RID: 1058 RVA: 0x000068AA File Offset: 0x00004AAA
		// (set) Token: 0x06000423 RID: 1059 RVA: 0x000068B2 File Offset: 0x00004AB2
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string errorId
		{
			get
			{
				return this.errorIdField;
			}
			set
			{
				this.errorIdField = value;
			}
		}

		// Token: 0x17000177 RID: 375
		// (get) Token: 0x06000424 RID: 1060 RVA: 0x000068BB File Offset: 0x00004ABB
		// (set) Token: 0x06000425 RID: 1061 RVA: 0x000068C3 File Offset: 0x00004AC3
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string errorCode
		{
			get
			{
				return this.errorCodeField;
			}
			set
			{
				this.errorCodeField = value;
			}
		}

		// Token: 0x17000178 RID: 376
		// (get) Token: 0x06000426 RID: 1062 RVA: 0x000068CC File Offset: 0x00004ACC
		// (set) Token: 0x06000427 RID: 1063 RVA: 0x000068D4 File Offset: 0x00004AD4
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string message
		{
			get
			{
				return this.messageField;
			}
			set
			{
				this.messageField = value;
			}
		}

		// Token: 0x17000179 RID: 377
		// (get) Token: 0x06000428 RID: 1064 RVA: 0x000068DD File Offset: 0x00004ADD
		// (set) Token: 0x06000429 RID: 1065 RVA: 0x000068E5 File Offset: 0x00004AE5
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string viewMessage
		{
			get
			{
				return this.viewMessageField;
			}
			set
			{
				this.viewMessageField = value;
			}
		}

		// Token: 0x0400019C RID: 412
		private string errorIdField;

		// Token: 0x0400019D RID: 413
		private string errorCodeField;

		// Token: 0x0400019E RID: 414
		private string messageField;

		// Token: 0x0400019F RID: 415
		private string viewMessageField;
	}
}
