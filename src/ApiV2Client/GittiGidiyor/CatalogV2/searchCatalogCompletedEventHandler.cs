﻿using System.CodeDom.Compiler;

namespace ApiV2Client.GittiGidiyor.CatalogV2
{
	// Token: 0x02000074 RID: 116
	// (Invoke) Token: 0x06000451 RID: 1105
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	public delegate void searchCatalogCompletedEventHandler(object sender, searchCatalogCompletedEventArgs e);
}
