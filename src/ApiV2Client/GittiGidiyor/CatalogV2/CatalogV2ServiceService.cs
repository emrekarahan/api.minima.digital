﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading;
using System.Web.Services;
using System.Web.Services.Description;
using System.Web.Services.Protocols;
using System.Xml.Schema;
using System.Xml.Serialization;
using ApiV2Client.Properties;

namespace ApiV2Client.GittiGidiyor.CatalogV2
{
	// Token: 0x02000065 RID: 101
	[DebuggerStepThrough]
	[WebServiceBinding(Name = "CatalogV2ServiceBinding", Namespace = "http://catalogv2.anonymous.ws.listingapi.gg.com")]
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	[XmlInclude(typeof(baseResponse))]
	[DesignerCategory("code")]
	public class CatalogV2ServiceService : SoapHttpClientProtocol
	{
		// Token: 0x060003C1 RID: 961 RVA: 0x00006197 File Offset: 0x00004397
		public CatalogV2ServiceService()
		{
			this.Url = Settings.Default.ApiV2Client_GittiGidiyor_CatalogV2_CatalogV2ServiceService;
			if (this.IsLocalFileSystemWebService(this.Url))
			{
				this.UseDefaultCredentials = true;
				this.useDefaultCredentialsSetExplicitly = false;
				return;
			}
			this.useDefaultCredentialsSetExplicitly = true;
		}

		// Token: 0x17000153 RID: 339
		// (get) Token: 0x060003C2 RID: 962 RVA: 0x000061D3 File Offset: 0x000043D3
		// (set) Token: 0x060003C3 RID: 963 RVA: 0x000061DB File Offset: 0x000043DB
		public new string Url
		{
			get
			{
				return base.Url;
			}
			set
			{
				if (this.IsLocalFileSystemWebService(base.Url) && !this.useDefaultCredentialsSetExplicitly && !this.IsLocalFileSystemWebService(value))
				{
					base.UseDefaultCredentials = false;
				}
				base.Url = value;
			}
		}

		// Token: 0x17000154 RID: 340
		// (get) Token: 0x060003C4 RID: 964 RVA: 0x0000620A File Offset: 0x0000440A
		// (set) Token: 0x060003C5 RID: 965 RVA: 0x00006212 File Offset: 0x00004412
		public new bool UseDefaultCredentials
		{
			get
			{
				return base.UseDefaultCredentials;
			}
			set
			{
				base.UseDefaultCredentials = value;
				this.useDefaultCredentialsSetExplicitly = true;
			}
		}

		// Token: 0x14000018 RID: 24
		// (add) Token: 0x060003C6 RID: 966 RVA: 0x00006224 File Offset: 0x00004424
		// (remove) Token: 0x060003C7 RID: 967 RVA: 0x0000625C File Offset: 0x0000445C
		public event getServiceNameCompletedEventHandler getServiceNameCompleted;

		// Token: 0x14000019 RID: 25
		// (add) Token: 0x060003C8 RID: 968 RVA: 0x00006294 File Offset: 0x00004494
		// (remove) Token: 0x060003C9 RID: 969 RVA: 0x000062CC File Offset: 0x000044CC
		public event getSimpleCatalogsCompletedEventHandler getSimpleCatalogsCompleted;

		// Token: 0x1400001A RID: 26
		// (add) Token: 0x060003CA RID: 970 RVA: 0x00006304 File Offset: 0x00004504
		// (remove) Token: 0x060003CB RID: 971 RVA: 0x0000633C File Offset: 0x0000453C
		public event searchCatalogCompletedEventHandler searchCatalogCompleted;

		// Token: 0x060003CC RID: 972 RVA: 0x00006374 File Offset: 0x00004574
		[SoapRpcMethod("", RequestNamespace = "http://catalogv2.anonymous.ws.listingapi.gg.com", ResponseNamespace = "http://catalogv2.anonymous.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public string getServiceName()
		{
			object[] array = base.Invoke("getServiceName", new object[0]);
			return (string)array[0];
		}

		// Token: 0x060003CD RID: 973 RVA: 0x0000639B File Offset: 0x0000459B
		public void getServiceNameAsync()
		{
			this.getServiceNameAsync(null);
		}

		// Token: 0x060003CE RID: 974 RVA: 0x000063A4 File Offset: 0x000045A4
		public void getServiceNameAsync(object userState)
		{
			if (this.getServiceNameOperationCompleted == null)
			{
				this.getServiceNameOperationCompleted = new SendOrPostCallback(this.OngetServiceNameOperationCompleted);
			}
			base.InvokeAsync("getServiceName", new object[0], this.getServiceNameOperationCompleted, userState);
		}

		// Token: 0x060003CF RID: 975 RVA: 0x000063D8 File Offset: 0x000045D8
		private void OngetServiceNameOperationCompleted(object arg)
		{
			if (this.getServiceNameCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getServiceNameCompleted(this, new getServiceNameCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x060003D0 RID: 976 RVA: 0x00006420 File Offset: 0x00004620
		[SoapRpcMethod("", RequestNamespace = "http://catalogv2.anonymous.ws.listingapi.gg.com", ResponseNamespace = "http://catalogv2.anonymous.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public simpleCatalogServiceListResponse getSimpleCatalogs(string categoryCode, [XmlArrayItem("item", Form = XmlSchemaForm.Unqualified)] specType[] specs, string lang)
		{
			object[] array = base.Invoke("getSimpleCatalogs", new object[]
			{
				categoryCode,
				specs,
				lang
			});
			return (simpleCatalogServiceListResponse)array[0];
		}

		// Token: 0x060003D1 RID: 977 RVA: 0x00006455 File Offset: 0x00004655
		public void getSimpleCatalogsAsync(string categoryCode, specType[] specs, string lang)
		{
			this.getSimpleCatalogsAsync(categoryCode, specs, lang, null);
		}

		// Token: 0x060003D2 RID: 978 RVA: 0x00006464 File Offset: 0x00004664
		public void getSimpleCatalogsAsync(string categoryCode, specType[] specs, string lang, object userState)
		{
			if (this.getSimpleCatalogsOperationCompleted == null)
			{
				this.getSimpleCatalogsOperationCompleted = new SendOrPostCallback(this.OngetSimpleCatalogsOperationCompleted);
			}
			base.InvokeAsync("getSimpleCatalogs", new object[]
			{
				categoryCode,
				specs,
				lang
			}, this.getSimpleCatalogsOperationCompleted, userState);
		}

		// Token: 0x060003D3 RID: 979 RVA: 0x000064B4 File Offset: 0x000046B4
		private void OngetSimpleCatalogsOperationCompleted(object arg)
		{
			if (this.getSimpleCatalogsCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getSimpleCatalogsCompleted(this, new getSimpleCatalogsCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x060003D4 RID: 980 RVA: 0x000064FC File Offset: 0x000046FC
		[SoapRpcMethod("", RequestNamespace = "http://catalogv2.anonymous.ws.listingapi.gg.com", ResponseNamespace = "http://catalogv2.anonymous.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public catalogSearchServiceResponse searchCatalog(int page, int rowCount, catalogSearchCriteriaType criteria, string lang)
		{
			object[] array = base.Invoke("searchCatalog", new object[]
			{
				page,
				rowCount,
				criteria,
				lang
			});
			return (catalogSearchServiceResponse)array[0];
		}

		// Token: 0x060003D5 RID: 981 RVA: 0x00006540 File Offset: 0x00004740
		public void searchCatalogAsync(int page, int rowCount, catalogSearchCriteriaType criteria, string lang)
		{
			this.searchCatalogAsync(page, rowCount, criteria, lang, null);
		}

		// Token: 0x060003D6 RID: 982 RVA: 0x00006550 File Offset: 0x00004750
		public void searchCatalogAsync(int page, int rowCount, catalogSearchCriteriaType criteria, string lang, object userState)
		{
			if (this.searchCatalogOperationCompleted == null)
			{
				this.searchCatalogOperationCompleted = new SendOrPostCallback(this.OnsearchCatalogOperationCompleted);
			}
			base.InvokeAsync("searchCatalog", new object[]
			{
				page,
				rowCount,
				criteria,
				lang
			}, this.searchCatalogOperationCompleted, userState);
		}

		// Token: 0x060003D7 RID: 983 RVA: 0x000065B0 File Offset: 0x000047B0
		private void OnsearchCatalogOperationCompleted(object arg)
		{
			if (this.searchCatalogCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.searchCatalogCompleted(this, new searchCatalogCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x060003D8 RID: 984 RVA: 0x000065F5 File Offset: 0x000047F5
		public new void CancelAsync(object userState)
		{
			base.CancelAsync(userState);
		}

		// Token: 0x060003D9 RID: 985 RVA: 0x00006600 File Offset: 0x00004800
		private bool IsLocalFileSystemWebService(string url)
		{
			if (url == null || url == string.Empty)
			{
				return false;
			}
			Uri uri = new Uri(url);
			return uri.Port >= 1024 && string.Compare(uri.Host, "localHost", StringComparison.OrdinalIgnoreCase) == 0;
		}

		// Token: 0x04000174 RID: 372
		private SendOrPostCallback getServiceNameOperationCompleted;

		// Token: 0x04000175 RID: 373
		private SendOrPostCallback getSimpleCatalogsOperationCompleted;

		// Token: 0x04000176 RID: 374
		private SendOrPostCallback searchCatalogOperationCompleted;

		// Token: 0x04000177 RID: 375
		private bool useDefaultCredentialsSetExplicitly;
	}
}
