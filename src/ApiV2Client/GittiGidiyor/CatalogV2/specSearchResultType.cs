﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.CatalogV2
{
	// Token: 0x02000069 RID: 105
	[DesignerCategory("code")]
	[GeneratedCode("System.Xml", "4.7.3190.0")]
	[XmlType(Namespace = "http://catalogv2.anonymous.ws.listingapi.gg.com")]
	[DebuggerStepThrough]
	[Serializable]
	public class specSearchResultType
	{
		// Token: 0x17000161 RID: 353
		// (get) Token: 0x060003F5 RID: 1013 RVA: 0x0000672D File Offset: 0x0000492D
		// (set) Token: 0x060003F6 RID: 1014 RVA: 0x00006735 File Offset: 0x00004935
		[XmlArrayItem("facet", Form = XmlSchemaForm.Unqualified)]
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		public specValueFacetType[] facets
		{
			get
			{
				return this.facetsField;
			}
			set
			{
				this.facetsField = value;
			}
		}

		// Token: 0x17000162 RID: 354
		// (get) Token: 0x060003F7 RID: 1015 RVA: 0x0000673E File Offset: 0x0000493E
		// (set) Token: 0x060003F8 RID: 1016 RVA: 0x00006746 File Offset: 0x00004946
		[XmlAttribute]
		public string name
		{
			get
			{
				return this.nameField;
			}
			set
			{
				this.nameField = value;
			}
		}

		// Token: 0x04000187 RID: 391
		private specValueFacetType[] facetsField;

		// Token: 0x04000188 RID: 392
		private string nameField;
	}
}
