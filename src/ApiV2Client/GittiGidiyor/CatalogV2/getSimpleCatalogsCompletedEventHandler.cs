﻿using System.CodeDom.Compiler;

namespace ApiV2Client.GittiGidiyor.CatalogV2
{
	// Token: 0x02000072 RID: 114
	// (Invoke) Token: 0x0600044B RID: 1099
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	public delegate void getSimpleCatalogsCompletedEventHandler(object sender, getSimpleCatalogsCompletedEventArgs e);
}
