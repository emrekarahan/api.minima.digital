﻿namespace ApiV2Client.GittiGidiyor.CatalogV2
{
	// Token: 0x02000005 RID: 5
	public class CatalogServiceV2 : ServiceClient<CatalogV2ServiceService>, ICatalogServiceV2, IService
	{
		// Token: 0x06000007 RID: 7 RVA: 0x00002194 File Offset: 0x00000394
		private CatalogServiceV2()
		{
			base.configureNetworkCredentials(this.service);
		}

		// Token: 0x06000008 RID: 8 RVA: 0x000021B3 File Offset: 0x000003B3
		public simpleCatalogServiceListResponse getSimpleCatalogs(string categoryCode, specType[] specs, string lang)
		{
			return this.service.getSimpleCatalogs(categoryCode, specs, lang);
		}

		// Token: 0x06000009 RID: 9 RVA: 0x000021C3 File Offset: 0x000003C3
		public string getServiceName()
		{
			return this.service.getServiceName();
		}

		// Token: 0x17000001 RID: 1
		// (get) Token: 0x0600000A RID: 10 RVA: 0x000021D0 File Offset: 0x000003D0
		public static CatalogServiceV2 Instance
		{
			get
			{
				if (CatalogServiceV2.instance == null)
				{
					lock (CatalogServiceV2.lockObject)
					{
						if (CatalogServiceV2.instance == null)
						{
							CatalogServiceV2.instance = new CatalogServiceV2();
						}
					}
				}
				return CatalogServiceV2.instance;
			}
		}

		// Token: 0x04000002 RID: 2
		private static CatalogServiceV2 instance;

		// Token: 0x04000003 RID: 3
		private static object lockObject = new object();

		// Token: 0x04000004 RID: 4
		private CatalogV2ServiceService service = new CatalogV2ServiceService();
	}
}
