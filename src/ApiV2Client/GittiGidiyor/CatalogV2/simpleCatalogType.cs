﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.CatalogV2
{
	// Token: 0x0200006B RID: 107
	[XmlType(Namespace = "http://catalogv2.anonymous.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.7.3190.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[Serializable]
	public class simpleCatalogType
	{
		// Token: 0x17000172 RID: 370
		// (get) Token: 0x06000419 RID: 1049 RVA: 0x0000685E File Offset: 0x00004A5E
		// (set) Token: 0x0600041A RID: 1050 RVA: 0x00006866 File Offset: 0x00004A66
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int catalogAttributeId
		{
			get
			{
				return this.catalogAttributeIdField;
			}
			set
			{
				this.catalogAttributeIdField = value;
			}
		}

		// Token: 0x17000173 RID: 371
		// (get) Token: 0x0600041B RID: 1051 RVA: 0x0000686F File Offset: 0x00004A6F
		// (set) Token: 0x0600041C RID: 1052 RVA: 0x00006877 File Offset: 0x00004A77
		[XmlIgnore]
		public bool catalogAttributeIdSpecified
		{
			get
			{
				return this.catalogAttributeIdFieldSpecified;
			}
			set
			{
				this.catalogAttributeIdFieldSpecified = value;
			}
		}

		// Token: 0x17000174 RID: 372
		// (get) Token: 0x0600041D RID: 1053 RVA: 0x00006880 File Offset: 0x00004A80
		// (set) Token: 0x0600041E RID: 1054 RVA: 0x00006888 File Offset: 0x00004A88
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string catalogName
		{
			get
			{
				return this.catalogNameField;
			}
			set
			{
				this.catalogNameField = value;
			}
		}

		// Token: 0x17000175 RID: 373
		// (get) Token: 0x0600041F RID: 1055 RVA: 0x00006891 File Offset: 0x00004A91
		// (set) Token: 0x06000420 RID: 1056 RVA: 0x00006899 File Offset: 0x00004A99
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public categoryType category
		{
			get
			{
				return this.categoryField;
			}
			set
			{
				this.categoryField = value;
			}
		}

		// Token: 0x04000198 RID: 408
		private int catalogAttributeIdField;

		// Token: 0x04000199 RID: 409
		private bool catalogAttributeIdFieldSpecified;

		// Token: 0x0400019A RID: 410
		private string catalogNameField;

		// Token: 0x0400019B RID: 411
		private categoryType categoryField;
	}
}
