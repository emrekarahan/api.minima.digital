﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.CatalogV2
{
	// Token: 0x02000075 RID: 117
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	public class searchCatalogCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000454 RID: 1108 RVA: 0x00006A19 File Offset: 0x00004C19
		internal searchCatalogCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x17000187 RID: 391
		// (get) Token: 0x06000455 RID: 1109 RVA: 0x00006A2C File Offset: 0x00004C2C
		public catalogSearchServiceResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (catalogSearchServiceResponse)this.results[0];
			}
		}

		// Token: 0x040001AD RID: 429
		private object[] results;
	}
}
