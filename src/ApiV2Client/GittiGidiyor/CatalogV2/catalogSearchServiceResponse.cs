﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.CatalogV2
{
	// Token: 0x0200006E RID: 110
	[GeneratedCode("System.Xml", "4.7.3190.0")]
	[XmlType(Namespace = "http://catalogv2.anonymous.ws.listingapi.gg.com")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[Serializable]
	public class catalogSearchServiceResponse : baseResponse
	{
		// Token: 0x1700017E RID: 382
		// (get) Token: 0x06000434 RID: 1076 RVA: 0x00006942 File Offset: 0x00004B42
		// (set) Token: 0x06000435 RID: 1077 RVA: 0x0000694A File Offset: 0x00004B4A
		[XmlElement(Form = XmlSchemaForm.Unqualified, IsNullable = true)]
		public int? totalCount
		{
			get
			{
				return this.totalCountField;
			}
			set
			{
				this.totalCountField = value;
			}
		}

		// Token: 0x1700017F RID: 383
		// (get) Token: 0x06000436 RID: 1078 RVA: 0x00006953 File Offset: 0x00004B53
		// (set) Token: 0x06000437 RID: 1079 RVA: 0x0000695B File Offset: 0x00004B5B
		[XmlIgnore]
		public bool totalCountSpecified
		{
			get
			{
				return this.totalCountFieldSpecified;
			}
			set
			{
				this.totalCountFieldSpecified = value;
			}
		}

		// Token: 0x17000180 RID: 384
		// (get) Token: 0x06000438 RID: 1080 RVA: 0x00006964 File Offset: 0x00004B64
		// (set) Token: 0x06000439 RID: 1081 RVA: 0x0000696C File Offset: 0x00004B6C
		[XmlArrayItem("catalog", Form = XmlSchemaForm.Unqualified)]
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		public simpleCatalogType[] catalogs
		{
			get
			{
				return this.catalogsField;
			}
			set
			{
				this.catalogsField = value;
			}
		}

		// Token: 0x17000181 RID: 385
		// (get) Token: 0x0600043A RID: 1082 RVA: 0x00006975 File Offset: 0x00004B75
		// (set) Token: 0x0600043B RID: 1083 RVA: 0x0000697D File Offset: 0x00004B7D
		[XmlArrayItem("specFacet", Form = XmlSchemaForm.Unqualified)]
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		public specSearchResultType[] specFacets
		{
			get
			{
				return this.specFacetsField;
			}
			set
			{
				this.specFacetsField = value;
			}
		}

		// Token: 0x040001A4 RID: 420
		private int? totalCountField;

		// Token: 0x040001A5 RID: 421
		private bool totalCountFieldSpecified;

		// Token: 0x040001A6 RID: 422
		private simpleCatalogType[] catalogsField;

		// Token: 0x040001A7 RID: 423
		private specSearchResultType[] specFacetsField;
	}
}
