﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.CatalogV2
{
	// Token: 0x02000067 RID: 103
	[GeneratedCode("System.Xml", "4.7.3190.0")]
	[DesignerCategory("code")]
	[XmlType(Namespace = "http://catalogv2.anonymous.ws.listingapi.gg.com")]
	[DebuggerStepThrough]
	[Serializable]
	public class catalogSearchCriteriaType
	{
		// Token: 0x1700015A RID: 346
		// (get) Token: 0x060003E5 RID: 997 RVA: 0x000066A6 File Offset: 0x000048A6
		// (set) Token: 0x060003E6 RID: 998 RVA: 0x000066AE File Offset: 0x000048AE
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string keyword
		{
			get
			{
				return this.keywordField;
			}
			set
			{
				this.keywordField = value;
			}
		}

		// Token: 0x1700015B RID: 347
		// (get) Token: 0x060003E7 RID: 999 RVA: 0x000066B7 File Offset: 0x000048B7
		// (set) Token: 0x060003E8 RID: 1000 RVA: 0x000066BF File Offset: 0x000048BF
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string categoryCode
		{
			get
			{
				return this.categoryCodeField;
			}
			set
			{
				this.categoryCodeField = value;
			}
		}

		// Token: 0x1700015C RID: 348
		// (get) Token: 0x060003E9 RID: 1001 RVA: 0x000066C8 File Offset: 0x000048C8
		// (set) Token: 0x060003EA RID: 1002 RVA: 0x000066D0 File Offset: 0x000048D0
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		[XmlArrayItem("specCriteria", Form = XmlSchemaForm.Unqualified, IsNullable = false)]
		public specType[] specCriterias
		{
			get
			{
				return this.specCriteriasField;
			}
			set
			{
				this.specCriteriasField = value;
			}
		}

		// Token: 0x1700015D RID: 349
		// (get) Token: 0x060003EB RID: 1003 RVA: 0x000066D9 File Offset: 0x000048D9
		// (set) Token: 0x060003EC RID: 1004 RVA: 0x000066E1 File Offset: 0x000048E1
		[XmlArrayItem("specFacetName", Form = XmlSchemaForm.Unqualified, IsNullable = false)]
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		public string[] specFacetNames
		{
			get
			{
				return this.specFacetNamesField;
			}
			set
			{
				this.specFacetNamesField = value;
			}
		}

		// Token: 0x04000180 RID: 384
		private string keywordField;

		// Token: 0x04000181 RID: 385
		private string categoryCodeField;

		// Token: 0x04000182 RID: 386
		private specType[] specCriteriasField;

		// Token: 0x04000183 RID: 387
		private string[] specFacetNamesField;
	}
}
