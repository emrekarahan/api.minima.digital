﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.CatalogV2
{
	// Token: 0x02000066 RID: 102
	[GeneratedCode("System.Xml", "4.7.3190.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[XmlType(Namespace = "http://catalogv2.anonymous.ws.listingapi.gg.com")]
	[Serializable]
	public class specType
	{
		// Token: 0x17000155 RID: 341
		// (get) Token: 0x060003DA RID: 986 RVA: 0x00006649 File Offset: 0x00004849
		// (set) Token: 0x060003DB RID: 987 RVA: 0x00006651 File Offset: 0x00004851
		[XmlAttribute]
		public string name
		{
			get
			{
				return this.nameField;
			}
			set
			{
				this.nameField = value;
			}
		}

		// Token: 0x17000156 RID: 342
		// (get) Token: 0x060003DC RID: 988 RVA: 0x0000665A File Offset: 0x0000485A
		// (set) Token: 0x060003DD RID: 989 RVA: 0x00006662 File Offset: 0x00004862
		[XmlAttribute]
		public string value
		{
			get
			{
				return this.valueField;
			}
			set
			{
				this.valueField = value;
			}
		}

		// Token: 0x17000157 RID: 343
		// (get) Token: 0x060003DE RID: 990 RVA: 0x0000666B File Offset: 0x0000486B
		// (set) Token: 0x060003DF RID: 991 RVA: 0x00006673 File Offset: 0x00004873
		[XmlAttribute]
		public string type
		{
			get
			{
				return this.typeField;
			}
			set
			{
				this.typeField = value;
			}
		}

		// Token: 0x17000158 RID: 344
		// (get) Token: 0x060003E0 RID: 992 RVA: 0x0000667C File Offset: 0x0000487C
		// (set) Token: 0x060003E1 RID: 993 RVA: 0x00006684 File Offset: 0x00004884
		[XmlAttribute]
		public bool required
		{
			get
			{
				return this.requiredField;
			}
			set
			{
				this.requiredField = value;
			}
		}

		// Token: 0x17000159 RID: 345
		// (get) Token: 0x060003E2 RID: 994 RVA: 0x0000668D File Offset: 0x0000488D
		// (set) Token: 0x060003E3 RID: 995 RVA: 0x00006695 File Offset: 0x00004895
		[XmlIgnore]
		public bool requiredSpecified
		{
			get
			{
				return this.requiredFieldSpecified;
			}
			set
			{
				this.requiredFieldSpecified = value;
			}
		}

		// Token: 0x0400017B RID: 379
		private string nameField;

		// Token: 0x0400017C RID: 380
		private string valueField;

		// Token: 0x0400017D RID: 381
		private string typeField;

		// Token: 0x0400017E RID: 382
		private bool requiredField;

		// Token: 0x0400017F RID: 383
		private bool requiredFieldSpecified;
	}
}
