﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading;
using System.Web.Services;
using System.Web.Services.Description;
using System.Web.Services.Protocols;
using System.Xml.Serialization;
using ApiV2Client.Properties;

namespace ApiV2Client.GittiGidiyor.Application
{
	// Token: 0x0200004A RID: 74
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	[DesignerCategory("code")]
	[XmlInclude(typeof(baseResponse))]
	[DebuggerStepThrough]
	[WebServiceBinding(Name = "ApplicationServiceBinding", Namespace = "http://application.anonymous.ws.listingapi.gg.com")]
	public class ApplicationServiceService : SoapHttpClientProtocol
	{
		// Token: 0x06000300 RID: 768 RVA: 0x0000527B File Offset: 0x0000347B
		public ApplicationServiceService()
		{
			this.Url = Settings.Default.ApiV2Client_GittiGidiyor_Application_ApplicationServiceService;
			if (this.IsLocalFileSystemWebService(this.Url))
			{
				this.UseDefaultCredentials = true;
				this.useDefaultCredentialsSetExplicitly = false;
				return;
			}
			this.useDefaultCredentialsSetExplicitly = true;
		}

		// Token: 0x1700011E RID: 286
		// (get) Token: 0x06000301 RID: 769 RVA: 0x000052B7 File Offset: 0x000034B7
		// (set) Token: 0x06000302 RID: 770 RVA: 0x000052BF File Offset: 0x000034BF
		public new string Url
		{
			get
			{
				return base.Url;
			}
			set
			{
				if (this.IsLocalFileSystemWebService(base.Url) && !this.useDefaultCredentialsSetExplicitly && !this.IsLocalFileSystemWebService(value))
				{
					base.UseDefaultCredentials = false;
				}
				base.Url = value;
			}
		}

		// Token: 0x1700011F RID: 287
		// (get) Token: 0x06000303 RID: 771 RVA: 0x000052EE File Offset: 0x000034EE
		// (set) Token: 0x06000304 RID: 772 RVA: 0x000052F6 File Offset: 0x000034F6
		public new bool UseDefaultCredentials
		{
			get
			{
				return base.UseDefaultCredentials;
			}
			set
			{
				base.UseDefaultCredentials = value;
				this.useDefaultCredentialsSetExplicitly = true;
			}
		}

		// Token: 0x14000011 RID: 17
		// (add) Token: 0x06000305 RID: 773 RVA: 0x00005308 File Offset: 0x00003508
		// (remove) Token: 0x06000306 RID: 774 RVA: 0x00005340 File Offset: 0x00003540
		public event createApplicationCompletedEventHandler createApplicationCompleted;

		// Token: 0x14000012 RID: 18
		// (add) Token: 0x06000307 RID: 775 RVA: 0x00005378 File Offset: 0x00003578
		// (remove) Token: 0x06000308 RID: 776 RVA: 0x000053B0 File Offset: 0x000035B0
		public event deleteApplicationCompletedEventHandler deleteApplicationCompleted;

		// Token: 0x14000013 RID: 19
		// (add) Token: 0x06000309 RID: 777 RVA: 0x000053E8 File Offset: 0x000035E8
		// (remove) Token: 0x0600030A RID: 778 RVA: 0x00005420 File Offset: 0x00003620
		public event getApplicationListCompletedEventHandler getApplicationListCompleted;

		// Token: 0x14000014 RID: 20
		// (add) Token: 0x0600030B RID: 779 RVA: 0x00005458 File Offset: 0x00003658
		// (remove) Token: 0x0600030C RID: 780 RVA: 0x00005490 File Offset: 0x00003690
		public event getServiceNameCompletedEventHandler getServiceNameCompleted;

		// Token: 0x0600030D RID: 781 RVA: 0x000054C8 File Offset: 0x000036C8
		[SoapRpcMethod("", RequestNamespace = "http://application.anonymous.ws.listingapi.gg.com", ResponseNamespace = "http://application.anonymous.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public applicationServiceResponse createApplication(applicationInfoType applicationInfo, string lang)
		{
			object[] array = base.Invoke("createApplication", new object[]
			{
				applicationInfo,
				lang
			});
			return (applicationServiceResponse)array[0];
		}

		// Token: 0x0600030E RID: 782 RVA: 0x000054F9 File Offset: 0x000036F9
		public void createApplicationAsync(applicationInfoType applicationInfo, string lang)
		{
			this.createApplicationAsync(applicationInfo, lang, null);
		}

		// Token: 0x0600030F RID: 783 RVA: 0x00005504 File Offset: 0x00003704
		public void createApplicationAsync(applicationInfoType applicationInfo, string lang, object userState)
		{
			if (this.createApplicationOperationCompleted == null)
			{
				this.createApplicationOperationCompleted = new SendOrPostCallback(this.OncreateApplicationOperationCompleted);
			}
			base.InvokeAsync("createApplication", new object[]
			{
				applicationInfo,
				lang
			}, this.createApplicationOperationCompleted, userState);
		}

		// Token: 0x06000310 RID: 784 RVA: 0x00005550 File Offset: 0x00003750
		private void OncreateApplicationOperationCompleted(object arg)
		{
			if (this.createApplicationCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.createApplicationCompleted(this, new createApplicationCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x06000311 RID: 785 RVA: 0x00005598 File Offset: 0x00003798
		[SoapRpcMethod("", RequestNamespace = "http://application.anonymous.ws.listingapi.gg.com", ResponseNamespace = "http://application.anonymous.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public messageServiceResponse deleteApplication(string developerId, string apiKey, string lang)
		{
			object[] array = base.Invoke("deleteApplication", new object[]
			{
				developerId,
				apiKey,
				lang
			});
			return (messageServiceResponse)array[0];
		}

		// Token: 0x06000312 RID: 786 RVA: 0x000055CD File Offset: 0x000037CD
		public void deleteApplicationAsync(string developerId, string apiKey, string lang)
		{
			this.deleteApplicationAsync(developerId, apiKey, lang, null);
		}

		// Token: 0x06000313 RID: 787 RVA: 0x000055DC File Offset: 0x000037DC
		public void deleteApplicationAsync(string developerId, string apiKey, string lang, object userState)
		{
			if (this.deleteApplicationOperationCompleted == null)
			{
				this.deleteApplicationOperationCompleted = new SendOrPostCallback(this.OndeleteApplicationOperationCompleted);
			}
			base.InvokeAsync("deleteApplication", new object[]
			{
				developerId,
				apiKey,
				lang
			}, this.deleteApplicationOperationCompleted, userState);
		}

		// Token: 0x06000314 RID: 788 RVA: 0x0000562C File Offset: 0x0000382C
		private void OndeleteApplicationOperationCompleted(object arg)
		{
			if (this.deleteApplicationCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.deleteApplicationCompleted(this, new deleteApplicationCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x06000315 RID: 789 RVA: 0x00005674 File Offset: 0x00003874
		[SoapRpcMethod("", RequestNamespace = "http://application.anonymous.ws.listingapi.gg.com", ResponseNamespace = "http://application.anonymous.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public applicationServiceListResponse getApplicationList(string developerId, string lang)
		{
			object[] array = base.Invoke("getApplicationList", new object[]
			{
				developerId,
				lang
			});
			return (applicationServiceListResponse)array[0];
		}

		// Token: 0x06000316 RID: 790 RVA: 0x000056A5 File Offset: 0x000038A5
		public void getApplicationListAsync(string developerId, string lang)
		{
			this.getApplicationListAsync(developerId, lang, null);
		}

		// Token: 0x06000317 RID: 791 RVA: 0x000056B0 File Offset: 0x000038B0
		public void getApplicationListAsync(string developerId, string lang, object userState)
		{
			if (this.getApplicationListOperationCompleted == null)
			{
				this.getApplicationListOperationCompleted = new SendOrPostCallback(this.OngetApplicationListOperationCompleted);
			}
			base.InvokeAsync("getApplicationList", new object[]
			{
				developerId,
				lang
			}, this.getApplicationListOperationCompleted, userState);
		}

		// Token: 0x06000318 RID: 792 RVA: 0x000056FC File Offset: 0x000038FC
		private void OngetApplicationListOperationCompleted(object arg)
		{
			if (this.getApplicationListCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getApplicationListCompleted(this, new getApplicationListCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x06000319 RID: 793 RVA: 0x00005744 File Offset: 0x00003944
		[SoapRpcMethod("", RequestNamespace = "http://application.anonymous.ws.listingapi.gg.com", ResponseNamespace = "http://application.anonymous.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public string getServiceName()
		{
			object[] array = base.Invoke("getServiceName", new object[0]);
			return (string)array[0];
		}

		// Token: 0x0600031A RID: 794 RVA: 0x0000576B File Offset: 0x0000396B
		public void getServiceNameAsync()
		{
			this.getServiceNameAsync(null);
		}

		// Token: 0x0600031B RID: 795 RVA: 0x00005774 File Offset: 0x00003974
		public void getServiceNameAsync(object userState)
		{
			if (this.getServiceNameOperationCompleted == null)
			{
				this.getServiceNameOperationCompleted = new SendOrPostCallback(this.OngetServiceNameOperationCompleted);
			}
			base.InvokeAsync("getServiceName", new object[0], this.getServiceNameOperationCompleted, userState);
		}

		// Token: 0x0600031C RID: 796 RVA: 0x000057A8 File Offset: 0x000039A8
		private void OngetServiceNameOperationCompleted(object arg)
		{
			if (this.getServiceNameCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getServiceNameCompleted(this, new getServiceNameCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x0600031D RID: 797 RVA: 0x000057ED File Offset: 0x000039ED
		public new void CancelAsync(object userState)
		{
			base.CancelAsync(userState);
		}

		// Token: 0x0600031E RID: 798 RVA: 0x000057F8 File Offset: 0x000039F8
		private bool IsLocalFileSystemWebService(string url)
		{
			if (url == null || url == string.Empty)
			{
				return false;
			}
			Uri uri = new Uri(url);
			return uri.Port >= 1024 && string.Compare(uri.Host, "localHost", StringComparison.OrdinalIgnoreCase) == 0;
		}

		// Token: 0x04000133 RID: 307
		private SendOrPostCallback createApplicationOperationCompleted;

		// Token: 0x04000134 RID: 308
		private SendOrPostCallback deleteApplicationOperationCompleted;

		// Token: 0x04000135 RID: 309
		private SendOrPostCallback getApplicationListOperationCompleted;

		// Token: 0x04000136 RID: 310
		private SendOrPostCallback getServiceNameOperationCompleted;

		// Token: 0x04000137 RID: 311
		private bool useDefaultCredentialsSetExplicitly;
	}
}
