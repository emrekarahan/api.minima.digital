﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Application
{
	// Token: 0x02000059 RID: 89
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	[DesignerCategory("code")]
	[DebuggerStepThrough]
	public class getServiceNameCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x0600037C RID: 892 RVA: 0x00005B11 File Offset: 0x00003D11
		internal getServiceNameCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x17000143 RID: 323
		// (get) Token: 0x0600037D RID: 893 RVA: 0x00005B24 File Offset: 0x00003D24
		public string Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (string)this.results[0];
			}
		}

		// Token: 0x0400015F RID: 351
		private object[] results;
	}
}
