﻿namespace ApiV2Client.GittiGidiyor.Application
{
	// Token: 0x020001DD RID: 477
	public interface IApplicationService : IService
	{
		// Token: 0x06001045 RID: 4165
		applicationServiceResponse createApplication(applicationInfoType applicationInfoType, string lang);

		// Token: 0x06001046 RID: 4166
		messageServiceResponse deleteApplication(string developerId, string apiKey, string lang);

		// Token: 0x06001047 RID: 4167
		applicationServiceListResponse getApplicationList(string developerId, string lang);
	}
}
