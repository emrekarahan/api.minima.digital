﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Application
{
	// Token: 0x02000057 RID: 87
	[DesignerCategory("code")]
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	[DebuggerStepThrough]
	public class getApplicationListCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000376 RID: 886 RVA: 0x00005AE9 File Offset: 0x00003CE9
		internal getApplicationListCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x17000142 RID: 322
		// (get) Token: 0x06000377 RID: 887 RVA: 0x00005AFC File Offset: 0x00003CFC
		public applicationServiceListResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (applicationServiceListResponse)this.results[0];
			}
		}

		// Token: 0x0400015E RID: 350
		private object[] results;
	}
}
