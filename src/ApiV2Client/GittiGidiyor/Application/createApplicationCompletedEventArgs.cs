﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Application
{
	// Token: 0x02000053 RID: 83
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	public class createApplicationCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x0600036A RID: 874 RVA: 0x00005A99 File Offset: 0x00003C99
		internal createApplicationCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x17000140 RID: 320
		// (get) Token: 0x0600036B RID: 875 RVA: 0x00005AAC File Offset: 0x00003CAC
		public applicationServiceResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (applicationServiceResponse)this.results[0];
			}
		}

		// Token: 0x0400015C RID: 348
		private object[] results;
	}
}
