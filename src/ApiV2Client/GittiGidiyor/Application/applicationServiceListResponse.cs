﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Application
{
	// Token: 0x02000050 RID: 80
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[XmlType(Namespace = "http://application.anonymous.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[Serializable]
	public class applicationServiceListResponse : baseResponse
	{
		// Token: 0x1700013B RID: 315
		// (get) Token: 0x0600035A RID: 858 RVA: 0x00005A34 File Offset: 0x00003C34
		// (set) Token: 0x0600035B RID: 859 RVA: 0x00005A3C File Offset: 0x00003C3C
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int applicationCount
		{
			get
			{
				return this.applicationCountField;
			}
			set
			{
				this.applicationCountField = value;
			}
		}

		// Token: 0x1700013C RID: 316
		// (get) Token: 0x0600035C RID: 860 RVA: 0x00005A45 File Offset: 0x00003C45
		// (set) Token: 0x0600035D RID: 861 RVA: 0x00005A4D File Offset: 0x00003C4D
		[XmlIgnore]
		public bool applicationCountSpecified
		{
			get
			{
				return this.applicationCountFieldSpecified;
			}
			set
			{
				this.applicationCountFieldSpecified = value;
			}
		}

		// Token: 0x1700013D RID: 317
		// (get) Token: 0x0600035E RID: 862 RVA: 0x00005A56 File Offset: 0x00003C56
		// (set) Token: 0x0600035F RID: 863 RVA: 0x00005A5E File Offset: 0x00003C5E
		[XmlArrayItem("application", Form = XmlSchemaForm.Unqualified)]
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		public applicationType[] applications
		{
			get
			{
				return this.applicationsField;
			}
			set
			{
				this.applicationsField = value;
			}
		}

		// Token: 0x04000157 RID: 343
		private int applicationCountField;

		// Token: 0x04000158 RID: 344
		private bool applicationCountFieldSpecified;

		// Token: 0x04000159 RID: 345
		private applicationType[] applicationsField;
	}
}
