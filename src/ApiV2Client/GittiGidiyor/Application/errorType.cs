﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Application
{
	// Token: 0x0200004D RID: 77
	[XmlType(Namespace = "http://application.anonymous.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[Serializable]
	public class errorType
	{
		// Token: 0x17000132 RID: 306
		// (get) Token: 0x06000345 RID: 837 RVA: 0x00005983 File Offset: 0x00003B83
		// (set) Token: 0x06000346 RID: 838 RVA: 0x0000598B File Offset: 0x00003B8B
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string errorId
		{
			get
			{
				return this.errorIdField;
			}
			set
			{
				this.errorIdField = value;
			}
		}

		// Token: 0x17000133 RID: 307
		// (get) Token: 0x06000347 RID: 839 RVA: 0x00005994 File Offset: 0x00003B94
		// (set) Token: 0x06000348 RID: 840 RVA: 0x0000599C File Offset: 0x00003B9C
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string errorCode
		{
			get
			{
				return this.errorCodeField;
			}
			set
			{
				this.errorCodeField = value;
			}
		}

		// Token: 0x17000134 RID: 308
		// (get) Token: 0x06000349 RID: 841 RVA: 0x000059A5 File Offset: 0x00003BA5
		// (set) Token: 0x0600034A RID: 842 RVA: 0x000059AD File Offset: 0x00003BAD
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string message
		{
			get
			{
				return this.messageField;
			}
			set
			{
				this.messageField = value;
			}
		}

		// Token: 0x17000135 RID: 309
		// (get) Token: 0x0600034B RID: 843 RVA: 0x000059B6 File Offset: 0x00003BB6
		// (set) Token: 0x0600034C RID: 844 RVA: 0x000059BE File Offset: 0x00003BBE
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string viewMessage
		{
			get
			{
				return this.viewMessageField;
			}
			set
			{
				this.viewMessageField = value;
			}
		}

		// Token: 0x0400014E RID: 334
		private string errorIdField;

		// Token: 0x0400014F RID: 335
		private string errorCodeField;

		// Token: 0x04000150 RID: 336
		private string messageField;

		// Token: 0x04000151 RID: 337
		private string viewMessageField;
	}
}
