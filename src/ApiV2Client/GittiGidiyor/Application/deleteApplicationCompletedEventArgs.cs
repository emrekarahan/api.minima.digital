﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Application
{
	// Token: 0x02000055 RID: 85
	[DesignerCategory("code")]
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	[DebuggerStepThrough]
	public class deleteApplicationCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000370 RID: 880 RVA: 0x00005AC1 File Offset: 0x00003CC1
		internal deleteApplicationCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x17000141 RID: 321
		// (get) Token: 0x06000371 RID: 881 RVA: 0x00005AD4 File Offset: 0x00003CD4
		public messageServiceResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (messageServiceResponse)this.results[0];
			}
		}

		// Token: 0x0400015D RID: 349
		private object[] results;
	}
}
