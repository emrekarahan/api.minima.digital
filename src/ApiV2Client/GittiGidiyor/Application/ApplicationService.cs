﻿namespace ApiV2Client.GittiGidiyor.Application
{
	// Token: 0x020001DE RID: 478
	public class ApplicationService : ServiceClient<ApplicationServiceService>, IApplicationService, IService
	{
		// Token: 0x06001048 RID: 4168 RVA: 0x00015AF0 File Offset: 0x00013CF0
		private ApplicationService()
		{
			base.configureNetworkCredentials(this.service);
		}

		// Token: 0x06001049 RID: 4169 RVA: 0x00015B0F File Offset: 0x00013D0F
		public applicationServiceResponse createApplication(applicationInfoType applicationInfo, string lang)
		{
			return this.service.createApplication(applicationInfo, lang);
		}

		// Token: 0x0600104A RID: 4170 RVA: 0x00015B1E File Offset: 0x00013D1E
		public messageServiceResponse deleteApplication(string developerId, string apiKey, string lang)
		{
			return this.service.deleteApplication(developerId, apiKey, lang);
		}

		// Token: 0x0600104B RID: 4171 RVA: 0x00015B2E File Offset: 0x00013D2E
		public applicationServiceListResponse getApplicationList(string developerId, string lang)
		{
			return this.service.getApplicationList(developerId, lang);
		}

		// Token: 0x0600104C RID: 4172 RVA: 0x00015B3D File Offset: 0x00013D3D
		public string getServiceName()
		{
			return this.service.getServiceName();
		}

		// Token: 0x17000513 RID: 1299
		// (get) Token: 0x0600104D RID: 4173 RVA: 0x00015B4C File Offset: 0x00013D4C
		public static ApplicationService Instance
		{
			get
			{
				if (ApplicationService.instance == null)
				{
					lock (ApplicationService.lockObject)
					{
						if (ApplicationService.instance == null)
						{
							ApplicationService.instance = new ApplicationService();
						}
					}
				}
				return ApplicationService.instance;
			}
		}

		// Token: 0x0400060A RID: 1546
		private static ApplicationService instance;

		// Token: 0x0400060B RID: 1547
		private static object lockObject = new object();

		// Token: 0x0400060C RID: 1548
		private ApplicationServiceService service = new ApplicationServiceService();
	}
}
