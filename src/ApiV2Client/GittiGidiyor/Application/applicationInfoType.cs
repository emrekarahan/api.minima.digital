﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Application
{
	// Token: 0x0200004B RID: 75
	[DebuggerStepThrough]
	[XmlType(Namespace = "http://application.anonymous.ws.listingapi.gg.com")]
	[DesignerCategory("code")]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[Serializable]
	public class applicationInfoType
	{
		// Token: 0x17000120 RID: 288
		// (get) Token: 0x0600031F RID: 799 RVA: 0x00005841 File Offset: 0x00003A41
		// (set) Token: 0x06000320 RID: 800 RVA: 0x00005849 File Offset: 0x00003A49
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string developerId
		{
			get
			{
				return this.developerIdField;
			}
			set
			{
				this.developerIdField = value;
			}
		}

		// Token: 0x17000121 RID: 289
		// (get) Token: 0x06000321 RID: 801 RVA: 0x00005852 File Offset: 0x00003A52
		// (set) Token: 0x06000322 RID: 802 RVA: 0x0000585A File Offset: 0x00003A5A
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string name
		{
			get
			{
				return this.nameField;
			}
			set
			{
				this.nameField = value;
			}
		}

		// Token: 0x17000122 RID: 290
		// (get) Token: 0x06000323 RID: 803 RVA: 0x00005863 File Offset: 0x00003A63
		// (set) Token: 0x06000324 RID: 804 RVA: 0x0000586B File Offset: 0x00003A6B
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string description
		{
			get
			{
				return this.descriptionField;
			}
			set
			{
				this.descriptionField = value;
			}
		}

		// Token: 0x17000123 RID: 291
		// (get) Token: 0x06000325 RID: 805 RVA: 0x00005874 File Offset: 0x00003A74
		// (set) Token: 0x06000326 RID: 806 RVA: 0x0000587C File Offset: 0x00003A7C
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string accessType
		{
			get
			{
				return this.accessTypeField;
			}
			set
			{
				this.accessTypeField = value;
			}
		}

		// Token: 0x17000124 RID: 292
		// (get) Token: 0x06000327 RID: 807 RVA: 0x00005885 File Offset: 0x00003A85
		// (set) Token: 0x06000328 RID: 808 RVA: 0x0000588D File Offset: 0x00003A8D
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string appType
		{
			get
			{
				return this.appTypeField;
			}
			set
			{
				this.appTypeField = value;
			}
		}

		// Token: 0x17000125 RID: 293
		// (get) Token: 0x06000329 RID: 809 RVA: 0x00005896 File Offset: 0x00003A96
		// (set) Token: 0x0600032A RID: 810 RVA: 0x0000589E File Offset: 0x00003A9E
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string descDetail
		{
			get
			{
				return this.descDetailField;
			}
			set
			{
				this.descDetailField = value;
			}
		}

		// Token: 0x17000126 RID: 294
		// (get) Token: 0x0600032B RID: 811 RVA: 0x000058A7 File Offset: 0x00003AA7
		// (set) Token: 0x0600032C RID: 812 RVA: 0x000058AF File Offset: 0x00003AAF
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string successReturnUrl
		{
			get
			{
				return this.successReturnUrlField;
			}
			set
			{
				this.successReturnUrlField = value;
			}
		}

		// Token: 0x17000127 RID: 295
		// (get) Token: 0x0600032D RID: 813 RVA: 0x000058B8 File Offset: 0x00003AB8
		// (set) Token: 0x0600032E RID: 814 RVA: 0x000058C0 File Offset: 0x00003AC0
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string failReturnUrl
		{
			get
			{
				return this.failReturnUrlField;
			}
			set
			{
				this.failReturnUrlField = value;
			}
		}

		// Token: 0x0400013C RID: 316
		private string developerIdField;

		// Token: 0x0400013D RID: 317
		private string nameField;

		// Token: 0x0400013E RID: 318
		private string descriptionField;

		// Token: 0x0400013F RID: 319
		private string accessTypeField;

		// Token: 0x04000140 RID: 320
		private string appTypeField;

		// Token: 0x04000141 RID: 321
		private string descDetailField;

		// Token: 0x04000142 RID: 322
		private string successReturnUrlField;

		// Token: 0x04000143 RID: 323
		private string failReturnUrlField;
	}
}
