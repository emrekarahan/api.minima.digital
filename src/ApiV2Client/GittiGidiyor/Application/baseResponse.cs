﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Application
{
	// Token: 0x0200004E RID: 78
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[XmlType(Namespace = "http://application.anonymous.ws.listingapi.gg.com")]
	[XmlInclude(typeof(applicationServiceListResponse))]
	[XmlInclude(typeof(messageServiceResponse))]
	[XmlInclude(typeof(applicationServiceResponse))]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[Serializable]
	public class baseResponse
	{
		// Token: 0x17000136 RID: 310
		// (get) Token: 0x0600034E RID: 846 RVA: 0x000059CF File Offset: 0x00003BCF
		// (set) Token: 0x0600034F RID: 847 RVA: 0x000059D7 File Offset: 0x00003BD7
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string ackCode
		{
			get
			{
				return this.ackCodeField;
			}
			set
			{
				this.ackCodeField = value;
			}
		}

		// Token: 0x17000137 RID: 311
		// (get) Token: 0x06000350 RID: 848 RVA: 0x000059E0 File Offset: 0x00003BE0
		// (set) Token: 0x06000351 RID: 849 RVA: 0x000059E8 File Offset: 0x00003BE8
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string responseTime
		{
			get
			{
				return this.responseTimeField;
			}
			set
			{
				this.responseTimeField = value;
			}
		}

		// Token: 0x17000138 RID: 312
		// (get) Token: 0x06000352 RID: 850 RVA: 0x000059F1 File Offset: 0x00003BF1
		// (set) Token: 0x06000353 RID: 851 RVA: 0x000059F9 File Offset: 0x00003BF9
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public errorType error
		{
			get
			{
				return this.errorField;
			}
			set
			{
				this.errorField = value;
			}
		}

		// Token: 0x17000139 RID: 313
		// (get) Token: 0x06000354 RID: 852 RVA: 0x00005A02 File Offset: 0x00003C02
		// (set) Token: 0x06000355 RID: 853 RVA: 0x00005A0A File Offset: 0x00003C0A
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string timeElapsed
		{
			get
			{
				return this.timeElapsedField;
			}
			set
			{
				this.timeElapsedField = value;
			}
		}

		// Token: 0x04000152 RID: 338
		private string ackCodeField;

		// Token: 0x04000153 RID: 339
		private string responseTimeField;

		// Token: 0x04000154 RID: 340
		private errorType errorField;

		// Token: 0x04000155 RID: 341
		private string timeElapsedField;
	}
}
