﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Application
{
	// Token: 0x0200004C RID: 76
	[XmlType(Namespace = "http://application.anonymous.ws.listingapi.gg.com")]
	[DesignerCategory("code")]
	[DebuggerStepThrough]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[Serializable]
	public class applicationType
	{
		// Token: 0x17000128 RID: 296
		// (get) Token: 0x06000330 RID: 816 RVA: 0x000058D1 File Offset: 0x00003AD1
		// (set) Token: 0x06000331 RID: 817 RVA: 0x000058D9 File Offset: 0x00003AD9
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string apiKey
		{
			get
			{
				return this.apiKeyField;
			}
			set
			{
				this.apiKeyField = value;
			}
		}

		// Token: 0x17000129 RID: 297
		// (get) Token: 0x06000332 RID: 818 RVA: 0x000058E2 File Offset: 0x00003AE2
		// (set) Token: 0x06000333 RID: 819 RVA: 0x000058EA File Offset: 0x00003AEA
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string secretKey
		{
			get
			{
				return this.secretKeyField;
			}
			set
			{
				this.secretKeyField = value;
			}
		}

		// Token: 0x1700012A RID: 298
		// (get) Token: 0x06000334 RID: 820 RVA: 0x000058F3 File Offset: 0x00003AF3
		// (set) Token: 0x06000335 RID: 821 RVA: 0x000058FB File Offset: 0x00003AFB
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string name
		{
			get
			{
				return this.nameField;
			}
			set
			{
				this.nameField = value;
			}
		}

		// Token: 0x1700012B RID: 299
		// (get) Token: 0x06000336 RID: 822 RVA: 0x00005904 File Offset: 0x00003B04
		// (set) Token: 0x06000337 RID: 823 RVA: 0x0000590C File Offset: 0x00003B0C
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string description
		{
			get
			{
				return this.descriptionField;
			}
			set
			{
				this.descriptionField = value;
			}
		}

		// Token: 0x1700012C RID: 300
		// (get) Token: 0x06000338 RID: 824 RVA: 0x00005915 File Offset: 0x00003B15
		// (set) Token: 0x06000339 RID: 825 RVA: 0x0000591D File Offset: 0x00003B1D
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string accessType
		{
			get
			{
				return this.accessTypeField;
			}
			set
			{
				this.accessTypeField = value;
			}
		}

		// Token: 0x1700012D RID: 301
		// (get) Token: 0x0600033A RID: 826 RVA: 0x00005926 File Offset: 0x00003B26
		// (set) Token: 0x0600033B RID: 827 RVA: 0x0000592E File Offset: 0x00003B2E
		[XmlElement("applicationType", Form = XmlSchemaForm.Unqualified)]
		public string applicationType1
		{
			get
			{
				return this.applicationType1Field;
			}
			set
			{
				this.applicationType1Field = value;
			}
		}

		// Token: 0x1700012E RID: 302
		// (get) Token: 0x0600033C RID: 828 RVA: 0x00005937 File Offset: 0x00003B37
		// (set) Token: 0x0600033D RID: 829 RVA: 0x0000593F File Offset: 0x00003B3F
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string descDetail
		{
			get
			{
				return this.descDetailField;
			}
			set
			{
				this.descDetailField = value;
			}
		}

		// Token: 0x1700012F RID: 303
		// (get) Token: 0x0600033E RID: 830 RVA: 0x00005948 File Offset: 0x00003B48
		// (set) Token: 0x0600033F RID: 831 RVA: 0x00005950 File Offset: 0x00003B50
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string registerDate
		{
			get
			{
				return this.registerDateField;
			}
			set
			{
				this.registerDateField = value;
			}
		}

		// Token: 0x17000130 RID: 304
		// (get) Token: 0x06000340 RID: 832 RVA: 0x00005959 File Offset: 0x00003B59
		// (set) Token: 0x06000341 RID: 833 RVA: 0x00005961 File Offset: 0x00003B61
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string successReturnUrl
		{
			get
			{
				return this.successReturnUrlField;
			}
			set
			{
				this.successReturnUrlField = value;
			}
		}

		// Token: 0x17000131 RID: 305
		// (get) Token: 0x06000342 RID: 834 RVA: 0x0000596A File Offset: 0x00003B6A
		// (set) Token: 0x06000343 RID: 835 RVA: 0x00005972 File Offset: 0x00003B72
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string failReturnUrl
		{
			get
			{
				return this.failReturnUrlField;
			}
			set
			{
				this.failReturnUrlField = value;
			}
		}

		// Token: 0x04000144 RID: 324
		private string apiKeyField;

		// Token: 0x04000145 RID: 325
		private string secretKeyField;

		// Token: 0x04000146 RID: 326
		private string nameField;

		// Token: 0x04000147 RID: 327
		private string descriptionField;

		// Token: 0x04000148 RID: 328
		private string accessTypeField;

		// Token: 0x04000149 RID: 329
		private string applicationType1Field;

		// Token: 0x0400014A RID: 330
		private string descDetailField;

		// Token: 0x0400014B RID: 331
		private string registerDateField;

		// Token: 0x0400014C RID: 332
		private string successReturnUrlField;

		// Token: 0x0400014D RID: 333
		private string failReturnUrlField;
	}
}
