﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Application
{
	// Token: 0x0200004F RID: 79
	[DebuggerStepThrough]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[DesignerCategory("code")]
	[XmlType(Namespace = "http://application.anonymous.ws.listingapi.gg.com")]
	[Serializable]
	public class messageServiceResponse : baseResponse
	{
		// Token: 0x1700013A RID: 314
		// (get) Token: 0x06000357 RID: 855 RVA: 0x00005A1B File Offset: 0x00003C1B
		// (set) Token: 0x06000358 RID: 856 RVA: 0x00005A23 File Offset: 0x00003C23
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string result
		{
			get
			{
				return this.resultField;
			}
			set
			{
				this.resultField = value;
			}
		}

		// Token: 0x04000156 RID: 342
		private string resultField;
	}
}
