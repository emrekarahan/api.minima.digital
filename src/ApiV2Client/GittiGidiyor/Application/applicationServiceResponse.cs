﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Application
{
	// Token: 0x02000051 RID: 81
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[DesignerCategory("code")]
	[XmlType(Namespace = "http://application.anonymous.ws.listingapi.gg.com")]
	[DebuggerStepThrough]
	[Serializable]
	public class applicationServiceResponse : baseResponse
	{
		// Token: 0x1700013E RID: 318
		// (get) Token: 0x06000361 RID: 865 RVA: 0x00005A6F File Offset: 0x00003C6F
		// (set) Token: 0x06000362 RID: 866 RVA: 0x00005A77 File Offset: 0x00003C77
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public applicationType application
		{
			get
			{
				return this.applicationField;
			}
			set
			{
				this.applicationField = value;
			}
		}

		// Token: 0x1700013F RID: 319
		// (get) Token: 0x06000363 RID: 867 RVA: 0x00005A80 File Offset: 0x00003C80
		// (set) Token: 0x06000364 RID: 868 RVA: 0x00005A88 File Offset: 0x00003C88
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string developerId
		{
			get
			{
				return this.developerIdField;
			}
			set
			{
				this.developerIdField = value;
			}
		}

		// Token: 0x0400015A RID: 346
		private applicationType applicationField;

		// Token: 0x0400015B RID: 347
		private string developerIdField;
	}
}
