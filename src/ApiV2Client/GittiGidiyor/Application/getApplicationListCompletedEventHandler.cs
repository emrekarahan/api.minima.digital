﻿using System.CodeDom.Compiler;

namespace ApiV2Client.GittiGidiyor.Application
{
	// Token: 0x02000056 RID: 86
	// (Invoke) Token: 0x06000373 RID: 883
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	public delegate void getApplicationListCompletedEventHandler(object sender, getApplicationListCompletedEventArgs e);
}
