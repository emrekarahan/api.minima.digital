﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Developer
{
	// Token: 0x020000CE RID: 206
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[DesignerCategory("code")]
	[XmlType(Namespace = "https://developer.anonymous.ws.listingapi.gg.com")]
	[DebuggerStepThrough]
	[Serializable]
	public class developerServiceResponse : baseResponse
	{
		// Token: 0x1700022E RID: 558
		// (get) Token: 0x060006DE RID: 1758 RVA: 0x0000A0D9 File Offset: 0x000082D9
		// (set) Token: 0x060006DF RID: 1759 RVA: 0x0000A0E1 File Offset: 0x000082E1
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string developerId
		{
			get
			{
				return this.developerIdField;
			}
			set
			{
				this.developerIdField = value;
			}
		}

		// Token: 0x1700022F RID: 559
		// (get) Token: 0x060006E0 RID: 1760 RVA: 0x0000A0EA File Offset: 0x000082EA
		// (set) Token: 0x060006E1 RID: 1761 RVA: 0x0000A0F2 File Offset: 0x000082F2
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string nick
		{
			get
			{
				return this.nickField;
			}
			set
			{
				this.nickField = value;
			}
		}

		// Token: 0x17000230 RID: 560
		// (get) Token: 0x060006E2 RID: 1762 RVA: 0x0000A0FB File Offset: 0x000082FB
		// (set) Token: 0x060006E3 RID: 1763 RVA: 0x0000A103 File Offset: 0x00008303
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string registerDate
		{
			get
			{
				return this.registerDateField;
			}
			set
			{
				this.registerDateField = value;
			}
		}

		// Token: 0x04000287 RID: 647
		private string developerIdField;

		// Token: 0x04000288 RID: 648
		private string nickField;

		// Token: 0x04000289 RID: 649
		private string registerDateField;
	}
}
