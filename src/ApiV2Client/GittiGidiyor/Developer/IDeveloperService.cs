﻿namespace ApiV2Client.GittiGidiyor.Developer
{
	// Token: 0x020001D5 RID: 469
	public interface IDeveloperService : IService
	{
		// Token: 0x06001022 RID: 4130
		developerServiceResponse createDeveloper(string nick, string password, string lang);

		// Token: 0x06001023 RID: 4131
		developerServiceResponse isDeveloper(string nick, string password, string lang);
	}
}
