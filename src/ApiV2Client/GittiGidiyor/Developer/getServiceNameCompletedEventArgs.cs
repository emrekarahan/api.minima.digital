﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Developer
{
	// Token: 0x020000D1 RID: 209
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	public class getServiceNameCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x060006F2 RID: 1778 RVA: 0x0000A160 File Offset: 0x00008360
		internal getServiceNameCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x17000235 RID: 565
		// (get) Token: 0x060006F3 RID: 1779 RVA: 0x0000A173 File Offset: 0x00008373
		public string Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (string)this.results[0];
			}
		}

		// Token: 0x0400028E RID: 654
		private object[] results;
	}
}
