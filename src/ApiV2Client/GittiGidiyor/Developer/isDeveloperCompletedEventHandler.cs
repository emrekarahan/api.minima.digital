﻿using System.CodeDom.Compiler;

namespace ApiV2Client.GittiGidiyor.Developer
{
	// Token: 0x020000D2 RID: 210
	// (Invoke) Token: 0x060006F5 RID: 1781
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	public delegate void isDeveloperCompletedEventHandler(object sender, isDeveloperCompletedEventArgs e);
}
