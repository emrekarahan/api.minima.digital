﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Developer
{
	// Token: 0x020000D5 RID: 213
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	public class registerDeveloperCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x060006FE RID: 1790 RVA: 0x0000A1B0 File Offset: 0x000083B0
		internal registerDeveloperCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x17000237 RID: 567
		// (get) Token: 0x060006FF RID: 1791 RVA: 0x0000A1C3 File Offset: 0x000083C3
		public developerServiceResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (developerServiceResponse)this.results[0];
			}
		}

		// Token: 0x04000290 RID: 656
		private object[] results;
	}
}
