﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Developer
{
	// Token: 0x020000CF RID: 207
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[XmlType(Namespace = "https://developer.anonymous.ws.listingapi.gg.com")]
	[Serializable]
	public class errorType
	{
		// Token: 0x17000231 RID: 561
		// (get) Token: 0x060006E5 RID: 1765 RVA: 0x0000A114 File Offset: 0x00008314
		// (set) Token: 0x060006E6 RID: 1766 RVA: 0x0000A11C File Offset: 0x0000831C
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string errorId
		{
			get
			{
				return this.errorIdField;
			}
			set
			{
				this.errorIdField = value;
			}
		}

		// Token: 0x17000232 RID: 562
		// (get) Token: 0x060006E7 RID: 1767 RVA: 0x0000A125 File Offset: 0x00008325
		// (set) Token: 0x060006E8 RID: 1768 RVA: 0x0000A12D File Offset: 0x0000832D
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string errorCode
		{
			get
			{
				return this.errorCodeField;
			}
			set
			{
				this.errorCodeField = value;
			}
		}

		// Token: 0x17000233 RID: 563
		// (get) Token: 0x060006E9 RID: 1769 RVA: 0x0000A136 File Offset: 0x00008336
		// (set) Token: 0x060006EA RID: 1770 RVA: 0x0000A13E File Offset: 0x0000833E
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string message
		{
			get
			{
				return this.messageField;
			}
			set
			{
				this.messageField = value;
			}
		}

		// Token: 0x17000234 RID: 564
		// (get) Token: 0x060006EB RID: 1771 RVA: 0x0000A147 File Offset: 0x00008347
		// (set) Token: 0x060006EC RID: 1772 RVA: 0x0000A14F File Offset: 0x0000834F
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string viewMessage
		{
			get
			{
				return this.viewMessageField;
			}
			set
			{
				this.viewMessageField = value;
			}
		}

		// Token: 0x0400028A RID: 650
		private string errorIdField;

		// Token: 0x0400028B RID: 651
		private string errorCodeField;

		// Token: 0x0400028C RID: 652
		private string messageField;

		// Token: 0x0400028D RID: 653
		private string viewMessageField;
	}
}
