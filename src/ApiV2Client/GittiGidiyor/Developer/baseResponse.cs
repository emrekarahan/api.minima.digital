﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Developer
{
	// Token: 0x020000CD RID: 205
	[DebuggerStepThrough]
	[XmlInclude(typeof(developerServiceResponse))]
	[DesignerCategory("code")]
	[XmlType(Namespace = "https://developer.anonymous.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[Serializable]
	public class baseResponse
	{
		// Token: 0x1700022A RID: 554
		// (get) Token: 0x060006D5 RID: 1749 RVA: 0x0000A08D File Offset: 0x0000828D
		// (set) Token: 0x060006D6 RID: 1750 RVA: 0x0000A095 File Offset: 0x00008295
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string ackCode
		{
			get
			{
				return this.ackCodeField;
			}
			set
			{
				this.ackCodeField = value;
			}
		}

		// Token: 0x1700022B RID: 555
		// (get) Token: 0x060006D7 RID: 1751 RVA: 0x0000A09E File Offset: 0x0000829E
		// (set) Token: 0x060006D8 RID: 1752 RVA: 0x0000A0A6 File Offset: 0x000082A6
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string responseTime
		{
			get
			{
				return this.responseTimeField;
			}
			set
			{
				this.responseTimeField = value;
			}
		}

		// Token: 0x1700022C RID: 556
		// (get) Token: 0x060006D9 RID: 1753 RVA: 0x0000A0AF File Offset: 0x000082AF
		// (set) Token: 0x060006DA RID: 1754 RVA: 0x0000A0B7 File Offset: 0x000082B7
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public errorType error
		{
			get
			{
				return this.errorField;
			}
			set
			{
				this.errorField = value;
			}
		}

		// Token: 0x1700022D RID: 557
		// (get) Token: 0x060006DB RID: 1755 RVA: 0x0000A0C0 File Offset: 0x000082C0
		// (set) Token: 0x060006DC RID: 1756 RVA: 0x0000A0C8 File Offset: 0x000082C8
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string timeElapsed
		{
			get
			{
				return this.timeElapsedField;
			}
			set
			{
				this.timeElapsedField = value;
			}
		}

		// Token: 0x04000283 RID: 643
		private string ackCodeField;

		// Token: 0x04000284 RID: 644
		private string responseTimeField;

		// Token: 0x04000285 RID: 645
		private errorType errorField;

		// Token: 0x04000286 RID: 646
		private string timeElapsedField;
	}
}
