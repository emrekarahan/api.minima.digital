﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading;
using System.Web.Services;
using System.Web.Services.Description;
using System.Web.Services.Protocols;
using System.Xml.Serialization;
using ApiV2Client.Properties;

namespace ApiV2Client.GittiGidiyor.Developer
{
	// Token: 0x020000CC RID: 204
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	[DesignerCategory("code")]
	[XmlInclude(typeof(baseResponse))]
	[DebuggerStepThrough]
	[WebServiceBinding(Name = "DeveloperServiceBinding", Namespace = "https://developer.anonymous.ws.listingapi.gg.com")]
	public class DeveloperServiceService : SoapHttpClientProtocol
	{
		// Token: 0x060006BC RID: 1724 RVA: 0x00009BFB File Offset: 0x00007DFB
		public DeveloperServiceService()
		{
			this.Url = Settings.Default.ApiV2Client_GittiGidiyor_Developer_DeveloperServiceService;
			if (this.IsLocalFileSystemWebService(this.Url))
			{
				this.UseDefaultCredentials = true;
				this.useDefaultCredentialsSetExplicitly = false;
				return;
			}
			this.useDefaultCredentialsSetExplicitly = true;
		}

		// Token: 0x17000228 RID: 552
		// (get) Token: 0x060006BD RID: 1725 RVA: 0x00009C37 File Offset: 0x00007E37
		// (set) Token: 0x060006BE RID: 1726 RVA: 0x00009C3F File Offset: 0x00007E3F
		public new string Url
		{
			get
			{
				return base.Url;
			}
			set
			{
				if (this.IsLocalFileSystemWebService(base.Url) && !this.useDefaultCredentialsSetExplicitly && !this.IsLocalFileSystemWebService(value))
				{
					base.UseDefaultCredentials = false;
				}
				base.Url = value;
			}
		}

		// Token: 0x17000229 RID: 553
		// (get) Token: 0x060006BF RID: 1727 RVA: 0x00009C6E File Offset: 0x00007E6E
		// (set) Token: 0x060006C0 RID: 1728 RVA: 0x00009C76 File Offset: 0x00007E76
		public new bool UseDefaultCredentials
		{
			get
			{
				return base.UseDefaultCredentials;
			}
			set
			{
				base.UseDefaultCredentials = value;
				this.useDefaultCredentialsSetExplicitly = true;
			}
		}

		// Token: 0x14000034 RID: 52
		// (add) Token: 0x060006C1 RID: 1729 RVA: 0x00009C88 File Offset: 0x00007E88
		// (remove) Token: 0x060006C2 RID: 1730 RVA: 0x00009CC0 File Offset: 0x00007EC0
		public event getServiceNameCompletedEventHandler getServiceNameCompleted;

		// Token: 0x14000035 RID: 53
		// (add) Token: 0x060006C3 RID: 1731 RVA: 0x00009CF8 File Offset: 0x00007EF8
		// (remove) Token: 0x060006C4 RID: 1732 RVA: 0x00009D30 File Offset: 0x00007F30
		public event isDeveloperCompletedEventHandler isDeveloperCompleted;

		// Token: 0x14000036 RID: 54
		// (add) Token: 0x060006C5 RID: 1733 RVA: 0x00009D68 File Offset: 0x00007F68
		// (remove) Token: 0x060006C6 RID: 1734 RVA: 0x00009DA0 File Offset: 0x00007FA0
		public event registerDeveloperCompletedEventHandler registerDeveloperCompleted;

		// Token: 0x060006C7 RID: 1735 RVA: 0x00009DD8 File Offset: 0x00007FD8
		[SoapRpcMethod("", RequestNamespace = "https://developer.anonymous.ws.listingapi.gg.com", ResponseNamespace = "https://developer.anonymous.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public string getServiceName()
		{
			object[] array = base.Invoke("getServiceName", new object[0]);
			return (string)array[0];
		}

		// Token: 0x060006C8 RID: 1736 RVA: 0x00009DFF File Offset: 0x00007FFF
		public void getServiceNameAsync()
		{
			this.getServiceNameAsync(null);
		}

		// Token: 0x060006C9 RID: 1737 RVA: 0x00009E08 File Offset: 0x00008008
		public void getServiceNameAsync(object userState)
		{
			if (this.getServiceNameOperationCompleted == null)
			{
				this.getServiceNameOperationCompleted = new SendOrPostCallback(this.OngetServiceNameOperationCompleted);
			}
			base.InvokeAsync("getServiceName", new object[0], this.getServiceNameOperationCompleted, userState);
		}

		// Token: 0x060006CA RID: 1738 RVA: 0x00009E3C File Offset: 0x0000803C
		private void OngetServiceNameOperationCompleted(object arg)
		{
			if (this.getServiceNameCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getServiceNameCompleted(this, new getServiceNameCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x060006CB RID: 1739 RVA: 0x00009E84 File Offset: 0x00008084
		[SoapRpcMethod("", RequestNamespace = "https://developer.anonymous.ws.listingapi.gg.com", ResponseNamespace = "https://developer.anonymous.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public developerServiceResponse isDeveloper(string nick, string password, string lang)
		{
			object[] array = base.Invoke("isDeveloper", new object[]
			{
				nick,
				password,
				lang
			});
			return (developerServiceResponse)array[0];
		}

		// Token: 0x060006CC RID: 1740 RVA: 0x00009EB9 File Offset: 0x000080B9
		public void isDeveloperAsync(string nick, string password, string lang)
		{
			this.isDeveloperAsync(nick, password, lang, null);
		}

		// Token: 0x060006CD RID: 1741 RVA: 0x00009EC8 File Offset: 0x000080C8
		public void isDeveloperAsync(string nick, string password, string lang, object userState)
		{
			if (this.isDeveloperOperationCompleted == null)
			{
				this.isDeveloperOperationCompleted = new SendOrPostCallback(this.OnisDeveloperOperationCompleted);
			}
			base.InvokeAsync("isDeveloper", new object[]
			{
				nick,
				password,
				lang
			}, this.isDeveloperOperationCompleted, userState);
		}

		// Token: 0x060006CE RID: 1742 RVA: 0x00009F18 File Offset: 0x00008118
		private void OnisDeveloperOperationCompleted(object arg)
		{
			if (this.isDeveloperCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.isDeveloperCompleted(this, new isDeveloperCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x060006CF RID: 1743 RVA: 0x00009F60 File Offset: 0x00008160
		[SoapRpcMethod("", RequestNamespace = "https://developer.anonymous.ws.listingapi.gg.com", ResponseNamespace = "https://developer.anonymous.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public developerServiceResponse registerDeveloper(string nick, string password, string lang)
		{
			object[] array = base.Invoke("registerDeveloper", new object[]
			{
				nick,
				password,
				lang
			});
			return (developerServiceResponse)array[0];
		}

		// Token: 0x060006D0 RID: 1744 RVA: 0x00009F95 File Offset: 0x00008195
		public void registerDeveloperAsync(string nick, string password, string lang)
		{
			this.registerDeveloperAsync(nick, password, lang, null);
		}

		// Token: 0x060006D1 RID: 1745 RVA: 0x00009FA4 File Offset: 0x000081A4
		public void registerDeveloperAsync(string nick, string password, string lang, object userState)
		{
			if (this.registerDeveloperOperationCompleted == null)
			{
				this.registerDeveloperOperationCompleted = new SendOrPostCallback(this.OnregisterDeveloperOperationCompleted);
			}
			base.InvokeAsync("registerDeveloper", new object[]
			{
				nick,
				password,
				lang
			}, this.registerDeveloperOperationCompleted, userState);
		}

		// Token: 0x060006D2 RID: 1746 RVA: 0x00009FF4 File Offset: 0x000081F4
		private void OnregisterDeveloperOperationCompleted(object arg)
		{
			if (this.registerDeveloperCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.registerDeveloperCompleted(this, new registerDeveloperCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x060006D3 RID: 1747 RVA: 0x0000A039 File Offset: 0x00008239
		public new void CancelAsync(object userState)
		{
			base.CancelAsync(userState);
		}

		// Token: 0x060006D4 RID: 1748 RVA: 0x0000A044 File Offset: 0x00008244
		private bool IsLocalFileSystemWebService(string url)
		{
			if (url == null || url == string.Empty)
			{
				return false;
			}
			Uri uri = new Uri(url);
			return uri.Port >= 1024 && string.Compare(uri.Host, "localHost", StringComparison.OrdinalIgnoreCase) == 0;
		}

		// Token: 0x0400027C RID: 636
		private SendOrPostCallback getServiceNameOperationCompleted;

		// Token: 0x0400027D RID: 637
		private SendOrPostCallback isDeveloperOperationCompleted;

		// Token: 0x0400027E RID: 638
		private SendOrPostCallback registerDeveloperOperationCompleted;

		// Token: 0x0400027F RID: 639
		private bool useDefaultCredentialsSetExplicitly;
	}
}
