﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Developer
{
	// Token: 0x020000D3 RID: 211
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	public class isDeveloperCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x060006F8 RID: 1784 RVA: 0x0000A188 File Offset: 0x00008388
		internal isDeveloperCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x17000236 RID: 566
		// (get) Token: 0x060006F9 RID: 1785 RVA: 0x0000A19B File Offset: 0x0000839B
		public developerServiceResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (developerServiceResponse)this.results[0];
			}
		}

		// Token: 0x0400028F RID: 655
		private object[] results;
	}
}
