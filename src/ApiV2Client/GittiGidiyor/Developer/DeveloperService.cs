﻿namespace ApiV2Client.GittiGidiyor.Developer
{
	// Token: 0x020001D6 RID: 470
	public class DeveloperService : ServiceClient<DeveloperServiceService>, IDeveloperService, IService
	{
		// Token: 0x06001024 RID: 4132 RVA: 0x000157C1 File Offset: 0x000139C1
		private DeveloperService()
		{
			base.configureNetworkCredentials(this.service);
		}

		// Token: 0x06001025 RID: 4133 RVA: 0x000157E0 File Offset: 0x000139E0
		public developerServiceResponse createDeveloper(string nick, string password, string lang)
		{
			return this.service.registerDeveloper(nick, password, lang);
		}

		// Token: 0x06001026 RID: 4134 RVA: 0x000157F0 File Offset: 0x000139F0
		public string getServiceName()
		{
			return this.service.getServiceName();
		}

		// Token: 0x06001027 RID: 4135 RVA: 0x000157FD File Offset: 0x000139FD
		public developerServiceResponse isDeveloper(string nick, string password, string lang)
		{
			return this.service.isDeveloper(nick, password, lang);
		}

		// Token: 0x17000510 RID: 1296
		// (get) Token: 0x06001028 RID: 4136 RVA: 0x00015810 File Offset: 0x00013A10
		public static DeveloperService Instance
		{
			get
			{
				if (DeveloperService.instance == null)
				{
					lock (DeveloperService.lockObject)
					{
						if (DeveloperService.instance == null)
						{
							DeveloperService.instance = new DeveloperService();
						}
					}
				}
				return DeveloperService.instance;
			}
		}

		// Token: 0x04000601 RID: 1537
		private static DeveloperService instance;

		// Token: 0x04000602 RID: 1538
		private static object lockObject = new object();

		// Token: 0x04000603 RID: 1539
		private DeveloperServiceService service = new DeveloperServiceService();
	}
}
