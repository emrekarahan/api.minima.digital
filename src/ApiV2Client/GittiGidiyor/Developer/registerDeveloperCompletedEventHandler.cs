﻿using System.CodeDom.Compiler;

namespace ApiV2Client.GittiGidiyor.Developer
{
	// Token: 0x020000D4 RID: 212
	// (Invoke) Token: 0x060006FB RID: 1787
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	public delegate void registerDeveloperCompletedEventHandler(object sender, registerDeveloperCompletedEventArgs e);
}
