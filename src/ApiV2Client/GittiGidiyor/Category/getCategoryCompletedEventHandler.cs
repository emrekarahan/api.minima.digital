﻿using System.CodeDom.Compiler;

namespace ApiV2Client.GittiGidiyor.Category
{
	// Token: 0x020000A2 RID: 162
	// (Invoke) Token: 0x060005F9 RID: 1529
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	public delegate void getCategoryCompletedEventHandler(object sender, getCategoryCompletedEventArgs e);
}
