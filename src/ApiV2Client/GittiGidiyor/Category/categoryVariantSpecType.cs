﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Category
{
	// Token: 0x02000090 RID: 144
	[XmlType(Namespace = "http://category.anonymous.ws.listingapi.gg.com")]
	[DesignerCategory("code")]
	[DebuggerStepThrough]
	[GeneratedCode("System.Xml", "4.7.3190.0")]
	[Serializable]
	public class categoryVariantSpecType
	{
		// Token: 0x170001CF RID: 463
		// (get) Token: 0x06000580 RID: 1408 RVA: 0x00008B41 File Offset: 0x00006D41
		// (set) Token: 0x06000581 RID: 1409 RVA: 0x00008B49 File Offset: 0x00006D49
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		[XmlArrayItem("specValue", Form = XmlSchemaForm.Unqualified)]
		public categoryVariantSpecValueType[] specValues
		{
			get
			{
				return this.specValuesField;
			}
			set
			{
				this.specValuesField = value;
			}
		}

		// Token: 0x170001D0 RID: 464
		// (get) Token: 0x06000582 RID: 1410 RVA: 0x00008B52 File Offset: 0x00006D52
		// (set) Token: 0x06000583 RID: 1411 RVA: 0x00008B5A File Offset: 0x00006D5A
		[XmlAttribute]
		public long nameId
		{
			get
			{
				return this.nameIdField;
			}
			set
			{
				this.nameIdField = value;
			}
		}

		// Token: 0x170001D1 RID: 465
		// (get) Token: 0x06000584 RID: 1412 RVA: 0x00008B63 File Offset: 0x00006D63
		// (set) Token: 0x06000585 RID: 1413 RVA: 0x00008B6B File Offset: 0x00006D6B
		[XmlIgnore]
		public bool nameIdSpecified
		{
			get
			{
				return this.nameIdFieldSpecified;
			}
			set
			{
				this.nameIdFieldSpecified = value;
			}
		}

		// Token: 0x170001D2 RID: 466
		// (get) Token: 0x06000586 RID: 1414 RVA: 0x00008B74 File Offset: 0x00006D74
		// (set) Token: 0x06000587 RID: 1415 RVA: 0x00008B7C File Offset: 0x00006D7C
		[XmlAttribute]
		public string name
		{
			get
			{
				return this.nameField;
			}
			set
			{
				this.nameField = value;
			}
		}

		// Token: 0x170001D3 RID: 467
		// (get) Token: 0x06000588 RID: 1416 RVA: 0x00008B85 File Offset: 0x00006D85
		// (set) Token: 0x06000589 RID: 1417 RVA: 0x00008B8D File Offset: 0x00006D8D
		[XmlAttribute]
		public string type
		{
			get
			{
				return this.typeField;
			}
			set
			{
				this.typeField = value;
			}
		}

		// Token: 0x170001D4 RID: 468
		// (get) Token: 0x0600058A RID: 1418 RVA: 0x00008B96 File Offset: 0x00006D96
		// (set) Token: 0x0600058B RID: 1419 RVA: 0x00008B9E File Offset: 0x00006D9E
		[XmlAttribute]
		public int orderNumber
		{
			get
			{
				return this.orderNumberField;
			}
			set
			{
				this.orderNumberField = value;
			}
		}

		// Token: 0x170001D5 RID: 469
		// (get) Token: 0x0600058C RID: 1420 RVA: 0x00008BA7 File Offset: 0x00006DA7
		// (set) Token: 0x0600058D RID: 1421 RVA: 0x00008BAF File Offset: 0x00006DAF
		[XmlIgnore]
		public bool orderNumberSpecified
		{
			get
			{
				return this.orderNumberFieldSpecified;
			}
			set
			{
				this.orderNumberFieldSpecified = value;
			}
		}

		// Token: 0x170001D6 RID: 470
		// (get) Token: 0x0600058E RID: 1422 RVA: 0x00008BB8 File Offset: 0x00006DB8
		// (set) Token: 0x0600058F RID: 1423 RVA: 0x00008BC0 File Offset: 0x00006DC0
		[XmlAttribute]
		public bool @base
		{
			get
			{
				return this.baseField;
			}
			set
			{
				this.baseField = value;
			}
		}

		// Token: 0x170001D7 RID: 471
		// (get) Token: 0x06000590 RID: 1424 RVA: 0x00008BC9 File Offset: 0x00006DC9
		// (set) Token: 0x06000591 RID: 1425 RVA: 0x00008BD1 File Offset: 0x00006DD1
		[XmlIgnore]
		public bool baseSpecified
		{
			get
			{
				return this.baseFieldSpecified;
			}
			set
			{
				this.baseFieldSpecified = value;
			}
		}

		// Token: 0x04000218 RID: 536
		private categoryVariantSpecValueType[] specValuesField;

		// Token: 0x04000219 RID: 537
		private long nameIdField;

		// Token: 0x0400021A RID: 538
		private bool nameIdFieldSpecified;

		// Token: 0x0400021B RID: 539
		private string nameField;

		// Token: 0x0400021C RID: 540
		private string typeField;

		// Token: 0x0400021D RID: 541
		private int orderNumberField;

		// Token: 0x0400021E RID: 542
		private bool orderNumberFieldSpecified;

		// Token: 0x0400021F RID: 543
		private bool baseField;

		// Token: 0x04000220 RID: 544
		private bool baseFieldSpecified;
	}
}
