﻿using System.CodeDom.Compiler;

namespace ApiV2Client.GittiGidiyor.Category
{
	// Token: 0x020000A0 RID: 160
	// (Invoke) Token: 0x060005F3 RID: 1523
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	public delegate void getCategoriesHavingVariantSpecsCompletedEventHandler(object sender, getCategoriesHavingVariantSpecsCompletedEventArgs e);
}
