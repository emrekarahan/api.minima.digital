﻿using System.CodeDom.Compiler;

namespace ApiV2Client.GittiGidiyor.Category
{
	// Token: 0x0200009C RID: 156
	// (Invoke) Token: 0x060005E7 RID: 1511
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	public delegate void getCategoriesCompletedEventHandler(object sender, getCategoriesCompletedEventArgs e);
}
