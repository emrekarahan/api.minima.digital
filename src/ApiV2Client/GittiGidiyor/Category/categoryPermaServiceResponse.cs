﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Category
{
	// Token: 0x02000099 RID: 153
	[XmlType(Namespace = "http://category.anonymous.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.7.3190.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[Serializable]
	public class categoryPermaServiceResponse : baseResponse
	{
		// Token: 0x170001F3 RID: 499
		// (get) Token: 0x060005D1 RID: 1489 RVA: 0x00008DED File Offset: 0x00006FED
		// (set) Token: 0x060005D2 RID: 1490 RVA: 0x00008DF5 File Offset: 0x00006FF5
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int categoryCount
		{
			get
			{
				return this.categoryCountField;
			}
			set
			{
				this.categoryCountField = value;
			}
		}

		// Token: 0x170001F4 RID: 500
		// (get) Token: 0x060005D3 RID: 1491 RVA: 0x00008DFE File Offset: 0x00006FFE
		// (set) Token: 0x060005D4 RID: 1492 RVA: 0x00008E06 File Offset: 0x00007006
		[XmlIgnore]
		public bool categoryCountSpecified
		{
			get
			{
				return this.categoryCountFieldSpecified;
			}
			set
			{
				this.categoryCountFieldSpecified = value;
			}
		}

		// Token: 0x170001F5 RID: 501
		// (get) Token: 0x060005D5 RID: 1493 RVA: 0x00008E0F File Offset: 0x0000700F
		// (set) Token: 0x060005D6 RID: 1494 RVA: 0x00008E17 File Offset: 0x00007017
		[XmlArrayItem("category", Form = XmlSchemaForm.Unqualified)]
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		public categoryPermaType[] categories
		{
			get
			{
				return this.categoriesField;
			}
			set
			{
				this.categoriesField = value;
			}
		}

		// Token: 0x0400023C RID: 572
		private int categoryCountField;

		// Token: 0x0400023D RID: 573
		private bool categoryCountFieldSpecified;

		// Token: 0x0400023E RID: 574
		private categoryPermaType[] categoriesField;
	}
}
