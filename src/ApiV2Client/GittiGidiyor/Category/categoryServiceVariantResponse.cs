﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Category
{
	// Token: 0x02000098 RID: 152
	[GeneratedCode("System.Xml", "4.7.3190.0")]
	[XmlType(Namespace = "http://category.anonymous.ws.listingapi.gg.com")]
	[DesignerCategory("code")]
	[DebuggerStepThrough]
	[Serializable]
	public class categoryServiceVariantResponse : baseResponse
	{
		// Token: 0x170001F0 RID: 496
		// (get) Token: 0x060005CA RID: 1482 RVA: 0x00008DB2 File Offset: 0x00006FB2
		// (set) Token: 0x060005CB RID: 1483 RVA: 0x00008DBA File Offset: 0x00006FBA
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public long specCount
		{
			get
			{
				return this.specCountField;
			}
			set
			{
				this.specCountField = value;
			}
		}

		// Token: 0x170001F1 RID: 497
		// (get) Token: 0x060005CC RID: 1484 RVA: 0x00008DC3 File Offset: 0x00006FC3
		// (set) Token: 0x060005CD RID: 1485 RVA: 0x00008DCB File Offset: 0x00006FCB
		[XmlIgnore]
		public bool specCountSpecified
		{
			get
			{
				return this.specCountFieldSpecified;
			}
			set
			{
				this.specCountFieldSpecified = value;
			}
		}

		// Token: 0x170001F2 RID: 498
		// (get) Token: 0x060005CE RID: 1486 RVA: 0x00008DD4 File Offset: 0x00006FD4
		// (set) Token: 0x060005CF RID: 1487 RVA: 0x00008DDC File Offset: 0x00006FDC
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		[XmlArrayItem("spec", Form = XmlSchemaForm.Unqualified)]
		public categoryVariantSpecType[] specs
		{
			get
			{
				return this.specsField;
			}
			set
			{
				this.specsField = value;
			}
		}

		// Token: 0x04000239 RID: 569
		private long specCountField;

		// Token: 0x0400023A RID: 570
		private bool specCountFieldSpecified;

		// Token: 0x0400023B RID: 571
		private categoryVariantSpecType[] specsField;
	}
}
