﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Category
{
	// Token: 0x02000091 RID: 145
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[XmlType(Namespace = "http://category.anonymous.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.7.3190.0")]
	[Serializable]
	public class categoryPermaType
	{
		// Token: 0x170001D8 RID: 472
		// (get) Token: 0x06000593 RID: 1427 RVA: 0x00008BE2 File Offset: 0x00006DE2
		// (set) Token: 0x06000594 RID: 1428 RVA: 0x00008BEA File Offset: 0x00006DEA
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string categoryCode
		{
			get
			{
				return this.categoryCodeField;
			}
			set
			{
				this.categoryCodeField = value;
			}
		}

		// Token: 0x170001D9 RID: 473
		// (get) Token: 0x06000595 RID: 1429 RVA: 0x00008BF3 File Offset: 0x00006DF3
		// (set) Token: 0x06000596 RID: 1430 RVA: 0x00008BFB File Offset: 0x00006DFB
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string categoryPerma
		{
			get
			{
				return this.categoryPermaField;
			}
			set
			{
				this.categoryPermaField = value;
			}
		}

		// Token: 0x170001DA RID: 474
		// (get) Token: 0x06000597 RID: 1431 RVA: 0x00008C04 File Offset: 0x00006E04
		// (set) Token: 0x06000598 RID: 1432 RVA: 0x00008C0C File Offset: 0x00006E0C
		[XmlArrayItem("breadCrumb", Form = XmlSchemaForm.Unqualified)]
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		public string[] breadCrumbs
		{
			get
			{
				return this.breadCrumbsField;
			}
			set
			{
				this.breadCrumbsField = value;
			}
		}

		// Token: 0x04000221 RID: 545
		private string categoryCodeField;

		// Token: 0x04000222 RID: 546
		private string categoryPermaField;

		// Token: 0x04000223 RID: 547
		private string[] breadCrumbsField;
	}
}
