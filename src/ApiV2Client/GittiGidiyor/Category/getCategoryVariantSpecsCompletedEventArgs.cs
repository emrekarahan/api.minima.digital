﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Category
{
	// Token: 0x020000AB RID: 171
	[DesignerCategory("code")]
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	[DebuggerStepThrough]
	public class getCategoryVariantSpecsCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000614 RID: 1556 RVA: 0x00008FB6 File Offset: 0x000071B6
		internal getCategoryVariantSpecsCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x17000203 RID: 515
		// (get) Token: 0x06000615 RID: 1557 RVA: 0x00008FC9 File Offset: 0x000071C9
		public categoryServiceVariantResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (categoryServiceVariantResponse)this.results[0];
			}
		}

		// Token: 0x0400024C RID: 588
		private object[] results;
	}
}
