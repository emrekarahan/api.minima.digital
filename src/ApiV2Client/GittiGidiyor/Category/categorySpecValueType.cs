﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Category
{
	// Token: 0x02000092 RID: 146
	[GeneratedCode("System.Xml", "4.7.3190.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[XmlType(Namespace = "http://category.anonymous.ws.listingapi.gg.com")]
	[Serializable]
	public class categorySpecValueType
	{
		// Token: 0x170001DB RID: 475
		// (get) Token: 0x0600059A RID: 1434 RVA: 0x00008C1D File Offset: 0x00006E1D
		// (set) Token: 0x0600059B RID: 1435 RVA: 0x00008C25 File Offset: 0x00006E25
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string name
		{
			get
			{
				return this.nameField;
			}
			set
			{
				this.nameField = value;
			}
		}

		// Token: 0x170001DC RID: 476
		// (get) Token: 0x0600059C RID: 1436 RVA: 0x00008C2E File Offset: 0x00006E2E
		// (set) Token: 0x0600059D RID: 1437 RVA: 0x00008C36 File Offset: 0x00006E36
		[XmlAttribute]
		public int specId
		{
			get
			{
				return this.specIdField;
			}
			set
			{
				this.specIdField = value;
			}
		}

		// Token: 0x170001DD RID: 477
		// (get) Token: 0x0600059E RID: 1438 RVA: 0x00008C3F File Offset: 0x00006E3F
		// (set) Token: 0x0600059F RID: 1439 RVA: 0x00008C47 File Offset: 0x00006E47
		[XmlIgnore]
		public bool specIdSpecified
		{
			get
			{
				return this.specIdFieldSpecified;
			}
			set
			{
				this.specIdFieldSpecified = value;
			}
		}

		// Token: 0x170001DE RID: 478
		// (get) Token: 0x060005A0 RID: 1440 RVA: 0x00008C50 File Offset: 0x00006E50
		// (set) Token: 0x060005A1 RID: 1441 RVA: 0x00008C58 File Offset: 0x00006E58
		[XmlAttribute]
		public int parentSpecId
		{
			get
			{
				return this.parentSpecIdField;
			}
			set
			{
				this.parentSpecIdField = value;
			}
		}

		// Token: 0x170001DF RID: 479
		// (get) Token: 0x060005A2 RID: 1442 RVA: 0x00008C61 File Offset: 0x00006E61
		// (set) Token: 0x060005A3 RID: 1443 RVA: 0x00008C69 File Offset: 0x00006E69
		[XmlIgnore]
		public bool parentSpecIdSpecified
		{
			get
			{
				return this.parentSpecIdFieldSpecified;
			}
			set
			{
				this.parentSpecIdFieldSpecified = value;
			}
		}

		// Token: 0x04000224 RID: 548
		private string nameField;

		// Token: 0x04000225 RID: 549
		private int specIdField;

		// Token: 0x04000226 RID: 550
		private bool specIdFieldSpecified;

		// Token: 0x04000227 RID: 551
		private int parentSpecIdField;

		// Token: 0x04000228 RID: 552
		private bool parentSpecIdFieldSpecified;
	}
}
