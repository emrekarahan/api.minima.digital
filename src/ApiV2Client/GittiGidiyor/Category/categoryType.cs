﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Category
{
	// Token: 0x0200008D RID: 141
	[GeneratedCode("System.Xml", "4.7.3190.0")]
	[DesignerCategory("code")]
	[XmlType(Namespace = "http://category.anonymous.ws.listingapi.gg.com")]
	[DebuggerStepThrough]
	[Serializable]
	public class categoryType
	{
		// Token: 0x170001BE RID: 446
		// (get) Token: 0x0600055B RID: 1371 RVA: 0x00008A08 File Offset: 0x00006C08
		// (set) Token: 0x0600055C RID: 1372 RVA: 0x00008A10 File Offset: 0x00006C10
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string categoryCode
		{
			get
			{
				return this.categoryCodeField;
			}
			set
			{
				this.categoryCodeField = value;
			}
		}

		// Token: 0x170001BF RID: 447
		// (get) Token: 0x0600055D RID: 1373 RVA: 0x00008A19 File Offset: 0x00006C19
		// (set) Token: 0x0600055E RID: 1374 RVA: 0x00008A21 File Offset: 0x00006C21
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string categoryName
		{
			get
			{
				return this.categoryNameField;
			}
			set
			{
				this.categoryNameField = value;
			}
		}

		// Token: 0x170001C0 RID: 448
		// (get) Token: 0x0600055F RID: 1375 RVA: 0x00008A2A File Offset: 0x00006C2A
		// (set) Token: 0x06000560 RID: 1376 RVA: 0x00008A32 File Offset: 0x00006C32
		[XmlArrayItem("spec", Form = XmlSchemaForm.Unqualified)]
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		public categorySpecType[] specs
		{
			get
			{
				return this.specsField;
			}
			set
			{
				this.specsField = value;
			}
		}

		// Token: 0x170001C1 RID: 449
		// (get) Token: 0x06000561 RID: 1377 RVA: 0x00008A3B File Offset: 0x00006C3B
		// (set) Token: 0x06000562 RID: 1378 RVA: 0x00008A43 File Offset: 0x00006C43
		[XmlAttribute]
		public bool deepest
		{
			get
			{
				return this.deepestField;
			}
			set
			{
				this.deepestField = value;
			}
		}

		// Token: 0x170001C2 RID: 450
		// (get) Token: 0x06000563 RID: 1379 RVA: 0x00008A4C File Offset: 0x00006C4C
		// (set) Token: 0x06000564 RID: 1380 RVA: 0x00008A54 File Offset: 0x00006C54
		[XmlIgnore]
		public bool deepestSpecified
		{
			get
			{
				return this.deepestFieldSpecified;
			}
			set
			{
				this.deepestFieldSpecified = value;
			}
		}

		// Token: 0x170001C3 RID: 451
		// (get) Token: 0x06000565 RID: 1381 RVA: 0x00008A5D File Offset: 0x00006C5D
		// (set) Token: 0x06000566 RID: 1382 RVA: 0x00008A65 File Offset: 0x00006C65
		[XmlAttribute]
		public bool hasCatalog
		{
			get
			{
				return this.hasCatalogField;
			}
			set
			{
				this.hasCatalogField = value;
			}
		}

		// Token: 0x170001C4 RID: 452
		// (get) Token: 0x06000567 RID: 1383 RVA: 0x00008A6E File Offset: 0x00006C6E
		// (set) Token: 0x06000568 RID: 1384 RVA: 0x00008A76 File Offset: 0x00006C76
		[XmlIgnore]
		public bool hasCatalogSpecified
		{
			get
			{
				return this.hasCatalogFieldSpecified;
			}
			set
			{
				this.hasCatalogFieldSpecified = value;
			}
		}

		// Token: 0x04000207 RID: 519
		private string categoryCodeField;

		// Token: 0x04000208 RID: 520
		private string categoryNameField;

		// Token: 0x04000209 RID: 521
		private categorySpecType[] specsField;

		// Token: 0x0400020A RID: 522
		private bool deepestField;

		// Token: 0x0400020B RID: 523
		private bool deepestFieldSpecified;

		// Token: 0x0400020C RID: 524
		private bool hasCatalogField;

		// Token: 0x0400020D RID: 525
		private bool hasCatalogFieldSpecified;
	}
}
