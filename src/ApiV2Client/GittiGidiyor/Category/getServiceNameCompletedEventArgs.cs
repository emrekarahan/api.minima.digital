﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Category
{
	// Token: 0x020000B5 RID: 181
	[DebuggerStepThrough]
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	[DesignerCategory("code")]
	public class getServiceNameCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000632 RID: 1586 RVA: 0x0000907E File Offset: 0x0000727E
		internal getServiceNameCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x17000208 RID: 520
		// (get) Token: 0x06000633 RID: 1587 RVA: 0x00009091 File Offset: 0x00007291
		public string Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (string)this.results[0];
			}
		}

		// Token: 0x04000251 RID: 593
		private object[] results;
	}
}
