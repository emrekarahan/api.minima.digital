﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Category
{
	// Token: 0x0200008E RID: 142
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[XmlType(Namespace = "http://category.anonymous.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.7.3190.0")]
	[Serializable]
	public class categorySpecType
	{
		// Token: 0x170001C5 RID: 453
		// (get) Token: 0x0600056A RID: 1386 RVA: 0x00008A87 File Offset: 0x00006C87
		// (set) Token: 0x0600056B RID: 1387 RVA: 0x00008A8F File Offset: 0x00006C8F
		[XmlArrayItem("value", Form = XmlSchemaForm.Unqualified)]
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		public string[] values
		{
			get
			{
				return this.valuesField;
			}
			set
			{
				this.valuesField = value;
			}
		}

		// Token: 0x170001C6 RID: 454
		// (get) Token: 0x0600056C RID: 1388 RVA: 0x00008A98 File Offset: 0x00006C98
		// (set) Token: 0x0600056D RID: 1389 RVA: 0x00008AA0 File Offset: 0x00006CA0
		[XmlAttribute]
		public string name
		{
			get
			{
				return this.nameField;
			}
			set
			{
				this.nameField = value;
			}
		}

		// Token: 0x170001C7 RID: 455
		// (get) Token: 0x0600056E RID: 1390 RVA: 0x00008AA9 File Offset: 0x00006CA9
		// (set) Token: 0x0600056F RID: 1391 RVA: 0x00008AB1 File Offset: 0x00006CB1
		[XmlAttribute]
		public bool required
		{
			get
			{
				return this.requiredField;
			}
			set
			{
				this.requiredField = value;
			}
		}

		// Token: 0x170001C8 RID: 456
		// (get) Token: 0x06000570 RID: 1392 RVA: 0x00008ABA File Offset: 0x00006CBA
		// (set) Token: 0x06000571 RID: 1393 RVA: 0x00008AC2 File Offset: 0x00006CC2
		[XmlIgnore]
		public bool requiredSpecified
		{
			get
			{
				return this.requiredFieldSpecified;
			}
			set
			{
				this.requiredFieldSpecified = value;
			}
		}

		// Token: 0x170001C9 RID: 457
		// (get) Token: 0x06000572 RID: 1394 RVA: 0x00008ACB File Offset: 0x00006CCB
		// (set) Token: 0x06000573 RID: 1395 RVA: 0x00008AD3 File Offset: 0x00006CD3
		[XmlAttribute]
		public string type
		{
			get
			{
				return this.typeField;
			}
			set
			{
				this.typeField = value;
			}
		}

		// Token: 0x0400020E RID: 526
		private string[] valuesField;

		// Token: 0x0400020F RID: 527
		private string nameField;

		// Token: 0x04000210 RID: 528
		private bool requiredField;

		// Token: 0x04000211 RID: 529
		private bool requiredFieldSpecified;

		// Token: 0x04000212 RID: 530
		private string typeField;
	}
}
