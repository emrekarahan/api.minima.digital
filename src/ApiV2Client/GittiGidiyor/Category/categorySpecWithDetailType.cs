﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Category
{
	// Token: 0x02000093 RID: 147
	[DebuggerStepThrough]
	[GeneratedCode("System.Xml", "4.7.3190.0")]
	[XmlType(Namespace = "http://category.anonymous.ws.listingapi.gg.com")]
	[DesignerCategory("code")]
	[Serializable]
	public class categorySpecWithDetailType
	{
		// Token: 0x170001E0 RID: 480
		// (get) Token: 0x060005A5 RID: 1445 RVA: 0x00008C7A File Offset: 0x00006E7A
		// (set) Token: 0x060005A6 RID: 1446 RVA: 0x00008C82 File Offset: 0x00006E82
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		[XmlArrayItem("value", Form = XmlSchemaForm.Unqualified)]
		public categorySpecValueType[] values
		{
			get
			{
				return this.valuesField;
			}
			set
			{
				this.valuesField = value;
			}
		}

		// Token: 0x170001E1 RID: 481
		// (get) Token: 0x060005A7 RID: 1447 RVA: 0x00008C8B File Offset: 0x00006E8B
		// (set) Token: 0x060005A8 RID: 1448 RVA: 0x00008C93 File Offset: 0x00006E93
		[XmlAttribute]
		public string name
		{
			get
			{
				return this.nameField;
			}
			set
			{
				this.nameField = value;
			}
		}

		// Token: 0x170001E2 RID: 482
		// (get) Token: 0x060005A9 RID: 1449 RVA: 0x00008C9C File Offset: 0x00006E9C
		// (set) Token: 0x060005AA RID: 1450 RVA: 0x00008CA4 File Offset: 0x00006EA4
		[XmlAttribute]
		public bool required
		{
			get
			{
				return this.requiredField;
			}
			set
			{
				this.requiredField = value;
			}
		}

		// Token: 0x170001E3 RID: 483
		// (get) Token: 0x060005AB RID: 1451 RVA: 0x00008CAD File Offset: 0x00006EAD
		// (set) Token: 0x060005AC RID: 1452 RVA: 0x00008CB5 File Offset: 0x00006EB5
		[XmlIgnore]
		public bool requiredSpecified
		{
			get
			{
				return this.requiredFieldSpecified;
			}
			set
			{
				this.requiredFieldSpecified = value;
			}
		}

		// Token: 0x170001E4 RID: 484
		// (get) Token: 0x060005AD RID: 1453 RVA: 0x00008CBE File Offset: 0x00006EBE
		// (set) Token: 0x060005AE RID: 1454 RVA: 0x00008CC6 File Offset: 0x00006EC6
		[XmlAttribute]
		public string type
		{
			get
			{
				return this.typeField;
			}
			set
			{
				this.typeField = value;
			}
		}

		// Token: 0x170001E5 RID: 485
		// (get) Token: 0x060005AF RID: 1455 RVA: 0x00008CCF File Offset: 0x00006ECF
		// (set) Token: 0x060005B0 RID: 1456 RVA: 0x00008CD7 File Offset: 0x00006ED7
		[XmlAttribute]
		public int specId
		{
			get
			{
				return this.specIdField;
			}
			set
			{
				this.specIdField = value;
			}
		}

		// Token: 0x170001E6 RID: 486
		// (get) Token: 0x060005B1 RID: 1457 RVA: 0x00008CE0 File Offset: 0x00006EE0
		// (set) Token: 0x060005B2 RID: 1458 RVA: 0x00008CE8 File Offset: 0x00006EE8
		[XmlIgnore]
		public bool specIdSpecified
		{
			get
			{
				return this.specIdFieldSpecified;
			}
			set
			{
				this.specIdFieldSpecified = value;
			}
		}

		// Token: 0x170001E7 RID: 487
		// (get) Token: 0x060005B3 RID: 1459 RVA: 0x00008CF1 File Offset: 0x00006EF1
		// (set) Token: 0x060005B4 RID: 1460 RVA: 0x00008CF9 File Offset: 0x00006EF9
		[XmlAttribute]
		public int childSpecId
		{
			get
			{
				return this.childSpecIdField;
			}
			set
			{
				this.childSpecIdField = value;
			}
		}

		// Token: 0x170001E8 RID: 488
		// (get) Token: 0x060005B5 RID: 1461 RVA: 0x00008D02 File Offset: 0x00006F02
		// (set) Token: 0x060005B6 RID: 1462 RVA: 0x00008D0A File Offset: 0x00006F0A
		[XmlIgnore]
		public bool childSpecIdSpecified
		{
			get
			{
				return this.childSpecIdFieldSpecified;
			}
			set
			{
				this.childSpecIdFieldSpecified = value;
			}
		}

		// Token: 0x04000229 RID: 553
		private categorySpecValueType[] valuesField;

		// Token: 0x0400022A RID: 554
		private string nameField;

		// Token: 0x0400022B RID: 555
		private bool requiredField;

		// Token: 0x0400022C RID: 556
		private bool requiredFieldSpecified;

		// Token: 0x0400022D RID: 557
		private string typeField;

		// Token: 0x0400022E RID: 558
		private int specIdField;

		// Token: 0x0400022F RID: 559
		private bool specIdFieldSpecified;

		// Token: 0x04000230 RID: 560
		private int childSpecIdField;

		// Token: 0x04000231 RID: 561
		private bool childSpecIdFieldSpecified;
	}
}
