﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Category
{
	// Token: 0x020000A7 RID: 167
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	public class getCategorySpecsCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000608 RID: 1544 RVA: 0x00008F66 File Offset: 0x00007166
		internal getCategorySpecsCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x17000201 RID: 513
		// (get) Token: 0x06000609 RID: 1545 RVA: 0x00008F79 File Offset: 0x00007179
		public categorySpecsServiceResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (categorySpecsServiceResponse)this.results[0];
			}
		}

		// Token: 0x0400024A RID: 586
		private object[] results;
	}
}
