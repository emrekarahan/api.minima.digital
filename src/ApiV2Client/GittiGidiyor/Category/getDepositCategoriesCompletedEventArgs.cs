﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Category
{
	// Token: 0x020000AF RID: 175
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	public class getDepositCategoriesCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000620 RID: 1568 RVA: 0x00009006 File Offset: 0x00007206
		internal getDepositCategoriesCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x17000205 RID: 517
		// (get) Token: 0x06000621 RID: 1569 RVA: 0x00009019 File Offset: 0x00007219
		public categoryServiceResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (categoryServiceResponse)this.results[0];
			}
		}

		// Token: 0x0400024E RID: 590
		private object[] results;
	}
}
