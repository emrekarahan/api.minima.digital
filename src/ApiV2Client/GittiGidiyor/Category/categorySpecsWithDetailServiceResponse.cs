﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Category
{
	// Token: 0x0200009A RID: 154
	[GeneratedCode("System.Xml", "4.7.3190.0")]
	[DesignerCategory("code")]
	[XmlType(Namespace = "http://category.anonymous.ws.listingapi.gg.com")]
	[DebuggerStepThrough]
	[Serializable]
	public class categorySpecsWithDetailServiceResponse : baseResponse
	{
		// Token: 0x170001F6 RID: 502
		// (get) Token: 0x060005D8 RID: 1496 RVA: 0x00008E28 File Offset: 0x00007028
		// (set) Token: 0x060005D9 RID: 1497 RVA: 0x00008E30 File Offset: 0x00007030
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		[XmlArrayItem("spec", Form = XmlSchemaForm.Unqualified)]
		public categorySpecWithDetailType[] specs
		{
			get
			{
				return this.specsField;
			}
			set
			{
				this.specsField = value;
			}
		}

		// Token: 0x0400023F RID: 575
		private categorySpecWithDetailType[] specsField;
	}
}
