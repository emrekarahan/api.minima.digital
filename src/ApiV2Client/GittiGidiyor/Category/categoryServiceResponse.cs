﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Category
{
	// Token: 0x0200008C RID: 140
	[DesignerCategory("code")]
	[XmlType(Namespace = "http://category.anonymous.ws.listingapi.gg.com")]
	[DebuggerStepThrough]
	[GeneratedCode("System.Xml", "4.7.3190.0")]
	[Serializable]
	public class categoryServiceResponse : baseResponse
	{
		// Token: 0x170001BB RID: 443
		// (get) Token: 0x06000554 RID: 1364 RVA: 0x000089CD File Offset: 0x00006BCD
		// (set) Token: 0x06000555 RID: 1365 RVA: 0x000089D5 File Offset: 0x00006BD5
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int categoryCount
		{
			get
			{
				return this.categoryCountField;
			}
			set
			{
				this.categoryCountField = value;
			}
		}

		// Token: 0x170001BC RID: 444
		// (get) Token: 0x06000556 RID: 1366 RVA: 0x000089DE File Offset: 0x00006BDE
		// (set) Token: 0x06000557 RID: 1367 RVA: 0x000089E6 File Offset: 0x00006BE6
		[XmlIgnore]
		public bool categoryCountSpecified
		{
			get
			{
				return this.categoryCountFieldSpecified;
			}
			set
			{
				this.categoryCountFieldSpecified = value;
			}
		}

		// Token: 0x170001BD RID: 445
		// (get) Token: 0x06000558 RID: 1368 RVA: 0x000089EF File Offset: 0x00006BEF
		// (set) Token: 0x06000559 RID: 1369 RVA: 0x000089F7 File Offset: 0x00006BF7
		[XmlArrayItem("category", Form = XmlSchemaForm.Unqualified)]
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		public categoryType[] categories
		{
			get
			{
				return this.categoriesField;
			}
			set
			{
				this.categoriesField = value;
			}
		}

		// Token: 0x04000204 RID: 516
		private int categoryCountField;

		// Token: 0x04000205 RID: 517
		private bool categoryCountFieldSpecified;

		// Token: 0x04000206 RID: 518
		private categoryType[] categoriesField;
	}
}
