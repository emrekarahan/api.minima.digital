﻿using System.CodeDom.Compiler;

namespace ApiV2Client.GittiGidiyor.Category
{
	// Token: 0x020000A4 RID: 164
	// (Invoke) Token: 0x060005FF RID: 1535
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	public delegate void getCategoryMetaDataCompletedEventHandler(object sender, getCategoryMetaDataCompletedEventArgs e);
}
