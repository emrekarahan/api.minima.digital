﻿using System.CodeDom.Compiler;

namespace ApiV2Client.GittiGidiyor.Category
{
	// Token: 0x020000B2 RID: 178
	// (Invoke) Token: 0x06000629 RID: 1577
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	public delegate void getParentCategoriesCompletedEventHandler(object sender, getParentCategoriesCompletedEventArgs e);
}
