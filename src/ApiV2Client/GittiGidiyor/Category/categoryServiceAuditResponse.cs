﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Category
{
	// Token: 0x0200009B RID: 155
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[XmlType(Namespace = "http://category.anonymous.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.7.3190.0")]
	[Serializable]
	public class categoryServiceAuditResponse : baseResponse
	{
		// Token: 0x170001F7 RID: 503
		// (get) Token: 0x060005DB RID: 1499 RVA: 0x00008E41 File Offset: 0x00007041
		// (set) Token: 0x060005DC RID: 1500 RVA: 0x00008E49 File Offset: 0x00007049
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int totalCount
		{
			get
			{
				return this.totalCountField;
			}
			set
			{
				this.totalCountField = value;
			}
		}

		// Token: 0x170001F8 RID: 504
		// (get) Token: 0x060005DD RID: 1501 RVA: 0x00008E52 File Offset: 0x00007052
		// (set) Token: 0x060005DE RID: 1502 RVA: 0x00008E5A File Offset: 0x0000705A
		[XmlIgnore]
		public bool totalCountSpecified
		{
			get
			{
				return this.totalCountFieldSpecified;
			}
			set
			{
				this.totalCountFieldSpecified = value;
			}
		}

		// Token: 0x170001F9 RID: 505
		// (get) Token: 0x060005DF RID: 1503 RVA: 0x00008E63 File Offset: 0x00007063
		// (set) Token: 0x060005E0 RID: 1504 RVA: 0x00008E6B File Offset: 0x0000706B
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int count
		{
			get
			{
				return this.countField;
			}
			set
			{
				this.countField = value;
			}
		}

		// Token: 0x170001FA RID: 506
		// (get) Token: 0x060005E1 RID: 1505 RVA: 0x00008E74 File Offset: 0x00007074
		// (set) Token: 0x060005E2 RID: 1506 RVA: 0x00008E7C File Offset: 0x0000707C
		[XmlIgnore]
		public bool countSpecified
		{
			get
			{
				return this.countFieldSpecified;
			}
			set
			{
				this.countFieldSpecified = value;
			}
		}

		// Token: 0x170001FB RID: 507
		// (get) Token: 0x060005E3 RID: 1507 RVA: 0x00008E85 File Offset: 0x00007085
		// (set) Token: 0x060005E4 RID: 1508 RVA: 0x00008E8D File Offset: 0x0000708D
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		[XmlArrayItem("categoryAudit", Form = XmlSchemaForm.Unqualified)]
		public categoryAuditType[] categoryAudits
		{
			get
			{
				return this.categoryAuditsField;
			}
			set
			{
				this.categoryAuditsField = value;
			}
		}

		// Token: 0x04000240 RID: 576
		private int totalCountField;

		// Token: 0x04000241 RID: 577
		private bool totalCountFieldSpecified;

		// Token: 0x04000242 RID: 578
		private int countField;

		// Token: 0x04000243 RID: 579
		private bool countFieldSpecified;

		// Token: 0x04000244 RID: 580
		private categoryAuditType[] categoryAuditsField;
	}
}
