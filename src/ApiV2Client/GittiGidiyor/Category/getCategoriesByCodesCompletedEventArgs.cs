﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Category
{
	// Token: 0x0200009F RID: 159
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	public class getCategoriesByCodesCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x060005F0 RID: 1520 RVA: 0x00008EC6 File Offset: 0x000070C6
		internal getCategoriesByCodesCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x170001FD RID: 509
		// (get) Token: 0x060005F1 RID: 1521 RVA: 0x00008ED9 File Offset: 0x000070D9
		public categoryServiceResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (categoryServiceResponse)this.results[0];
			}
		}

		// Token: 0x04000246 RID: 582
		private object[] results;
	}
}
