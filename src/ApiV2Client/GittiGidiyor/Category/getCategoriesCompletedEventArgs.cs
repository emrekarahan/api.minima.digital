﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Category
{
	// Token: 0x0200009D RID: 157
	[DesignerCategory("code")]
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	[DebuggerStepThrough]
	public class getCategoriesCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x060005EA RID: 1514 RVA: 0x00008E9E File Offset: 0x0000709E
		internal getCategoriesCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x170001FC RID: 508
		// (get) Token: 0x060005EB RID: 1515 RVA: 0x00008EB1 File Offset: 0x000070B1
		public categoryServiceResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (categoryServiceResponse)this.results[0];
			}
		}

		// Token: 0x04000245 RID: 581
		private object[] results;
	}
}
