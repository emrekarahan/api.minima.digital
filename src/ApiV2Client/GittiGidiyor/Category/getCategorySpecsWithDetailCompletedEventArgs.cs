﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Category
{
	// Token: 0x020000A9 RID: 169
	[DebuggerStepThrough]
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	[DesignerCategory("code")]
	public class getCategorySpecsWithDetailCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x0600060E RID: 1550 RVA: 0x00008F8E File Offset: 0x0000718E
		internal getCategorySpecsWithDetailCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x17000202 RID: 514
		// (get) Token: 0x0600060F RID: 1551 RVA: 0x00008FA1 File Offset: 0x000071A1
		public categorySpecsWithDetailServiceResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (categorySpecsWithDetailServiceResponse)this.results[0];
			}
		}

		// Token: 0x0400024B RID: 587
		private object[] results;
	}
}
