﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading;
using System.Web.Services;
using System.Web.Services.Description;
using System.Web.Services.Protocols;
using System.Xml.Schema;
using System.Xml.Serialization;
using ApiV2Client.Properties;

namespace ApiV2Client.GittiGidiyor.Category
{
	// Token: 0x0200008A RID: 138
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	[WebServiceBinding(Name = "CategoryServiceBinding", Namespace = "http://category.anonymous.ws.listingapi.gg.com")]
	[DesignerCategory("code")]
	[XmlInclude(typeof(baseAuditType))]
	[XmlInclude(typeof(baseResponse))]
	[DebuggerStepThrough]
	public class CategoryServiceService : SoapHttpClientProtocol
	{
		// Token: 0x060004F0 RID: 1264 RVA: 0x000075A3 File Offset: 0x000057A3
		public CategoryServiceService()
		{
			this.Url = Settings.Default.ApiV2Client_GittiGidiyor_Catalog_CategoryServiceService;
			if (this.IsLocalFileSystemWebService(this.Url))
			{
				this.UseDefaultCredentials = true;
				this.useDefaultCredentialsSetExplicitly = false;
				return;
			}
			this.useDefaultCredentialsSetExplicitly = true;
		}

		// Token: 0x170001B5 RID: 437
		// (get) Token: 0x060004F1 RID: 1265 RVA: 0x000075DF File Offset: 0x000057DF
		// (set) Token: 0x060004F2 RID: 1266 RVA: 0x000075E7 File Offset: 0x000057E7
		public new string Url
		{
			get
			{
				return base.Url;
			}
			set
			{
				if (this.IsLocalFileSystemWebService(base.Url) && !this.useDefaultCredentialsSetExplicitly && !this.IsLocalFileSystemWebService(value))
				{
					base.UseDefaultCredentials = false;
				}
				base.Url = value;
			}
		}

		// Token: 0x170001B6 RID: 438
		// (get) Token: 0x060004F3 RID: 1267 RVA: 0x00007616 File Offset: 0x00005816
		// (set) Token: 0x060004F4 RID: 1268 RVA: 0x0000761E File Offset: 0x0000581E
		public new bool UseDefaultCredentials
		{
			get
			{
				return base.UseDefaultCredentials;
			}
			set
			{
				base.UseDefaultCredentials = value;
				this.useDefaultCredentialsSetExplicitly = true;
			}
		}

		// Token: 0x14000020 RID: 32
		// (add) Token: 0x060004F5 RID: 1269 RVA: 0x00007630 File Offset: 0x00005830
		// (remove) Token: 0x060004F6 RID: 1270 RVA: 0x00007668 File Offset: 0x00005868
		public event getCategoriesCompletedEventHandler getCategoriesCompleted;

		// Token: 0x14000021 RID: 33
		// (add) Token: 0x060004F7 RID: 1271 RVA: 0x000076A0 File Offset: 0x000058A0
		// (remove) Token: 0x060004F8 RID: 1272 RVA: 0x000076D8 File Offset: 0x000058D8
		public event getCategoriesByCodesCompletedEventHandler getCategoriesByCodesCompleted;

		// Token: 0x14000022 RID: 34
		// (add) Token: 0x060004F9 RID: 1273 RVA: 0x00007710 File Offset: 0x00005910
		// (remove) Token: 0x060004FA RID: 1274 RVA: 0x00007748 File Offset: 0x00005948
		public event getCategoriesHavingVariantSpecsCompletedEventHandler getCategoriesHavingVariantSpecsCompleted;

		// Token: 0x14000023 RID: 35
		// (add) Token: 0x060004FB RID: 1275 RVA: 0x00007780 File Offset: 0x00005980
		// (remove) Token: 0x060004FC RID: 1276 RVA: 0x000077B8 File Offset: 0x000059B8
		public event getCategoryCompletedEventHandler getCategoryCompleted;

		// Token: 0x14000024 RID: 36
		// (add) Token: 0x060004FD RID: 1277 RVA: 0x000077F0 File Offset: 0x000059F0
		// (remove) Token: 0x060004FE RID: 1278 RVA: 0x00007828 File Offset: 0x00005A28
		public event getCategoryMetaDataCompletedEventHandler getCategoryMetaDataCompleted;

		// Token: 0x14000025 RID: 37
		// (add) Token: 0x060004FF RID: 1279 RVA: 0x00007860 File Offset: 0x00005A60
		// (remove) Token: 0x06000500 RID: 1280 RVA: 0x00007898 File Offset: 0x00005A98
		public event getCategorySpecsCompletedEventHandler getCategorySpecsCompleted;

		// Token: 0x14000026 RID: 38
		// (add) Token: 0x06000501 RID: 1281 RVA: 0x000078D0 File Offset: 0x00005AD0
		// (remove) Token: 0x06000502 RID: 1282 RVA: 0x00007908 File Offset: 0x00005B08
		public event getCategorySpecsWithDetailCompletedEventHandler getCategorySpecsWithDetailCompleted;

		// Token: 0x14000027 RID: 39
		// (add) Token: 0x06000503 RID: 1283 RVA: 0x00007940 File Offset: 0x00005B40
		// (remove) Token: 0x06000504 RID: 1284 RVA: 0x00007978 File Offset: 0x00005B78
		public event getCategoryVariantSpecsCompletedEventHandler getCategoryVariantSpecsCompleted;

		// Token: 0x14000028 RID: 40
		// (add) Token: 0x06000505 RID: 1285 RVA: 0x000079B0 File Offset: 0x00005BB0
		// (remove) Token: 0x06000506 RID: 1286 RVA: 0x000079E8 File Offset: 0x00005BE8
		public event getDeepestCategoriesCompletedEventHandler getDeepestCategoriesCompleted;

		// Token: 0x14000029 RID: 41
		// (add) Token: 0x06000507 RID: 1287 RVA: 0x00007A20 File Offset: 0x00005C20
		// (remove) Token: 0x06000508 RID: 1288 RVA: 0x00007A58 File Offset: 0x00005C58
		public event getDepositCategoriesCompletedEventHandler getDepositCategoriesCompleted;

		// Token: 0x1400002A RID: 42
		// (add) Token: 0x06000509 RID: 1289 RVA: 0x00007A90 File Offset: 0x00005C90
		// (remove) Token: 0x0600050A RID: 1290 RVA: 0x00007AC8 File Offset: 0x00005CC8
		public event getModifiedCategoriesCompletedEventHandler getModifiedCategoriesCompleted;

		// Token: 0x1400002B RID: 43
		// (add) Token: 0x0600050B RID: 1291 RVA: 0x00007B00 File Offset: 0x00005D00
		// (remove) Token: 0x0600050C RID: 1292 RVA: 0x00007B38 File Offset: 0x00005D38
		public event getParentCategoriesCompletedEventHandler getParentCategoriesCompleted;

		// Token: 0x1400002C RID: 44
		// (add) Token: 0x0600050D RID: 1293 RVA: 0x00007B70 File Offset: 0x00005D70
		// (remove) Token: 0x0600050E RID: 1294 RVA: 0x00007BA8 File Offset: 0x00005DA8
		public event getServiceNameCompletedEventHandler getServiceNameCompleted;

		// Token: 0x1400002D RID: 45
		// (add) Token: 0x0600050F RID: 1295 RVA: 0x00007BE0 File Offset: 0x00005DE0
		// (remove) Token: 0x06000510 RID: 1296 RVA: 0x00007C18 File Offset: 0x00005E18
		public event getSubCategoriesCompletedEventHandler getSubCategoriesCompleted;

		// Token: 0x06000511 RID: 1297 RVA: 0x00007C50 File Offset: 0x00005E50
		[SoapRpcMethod("", RequestNamespace = "http://category.anonymous.ws.listingapi.gg.com", ResponseNamespace = "http://category.anonymous.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public categoryServiceResponse getCategories(int startOffSet, int rowCount, bool withSpecs, bool withDeepest, bool withCatalog, string lang)
		{
			object[] array = base.Invoke("getCategories", new object[]
			{
				startOffSet,
				rowCount,
				withSpecs,
				withDeepest,
				withCatalog,
				lang
			});
			return (categoryServiceResponse)array[0];
		}

		// Token: 0x06000512 RID: 1298 RVA: 0x00007CAD File Offset: 0x00005EAD
		public void getCategoriesAsync(int startOffSet, int rowCount, bool withSpecs, bool withDeepest, bool withCatalog, string lang)
		{
			this.getCategoriesAsync(startOffSet, rowCount, withSpecs, withDeepest, withCatalog, lang, null);
		}

		// Token: 0x06000513 RID: 1299 RVA: 0x00007CC0 File Offset: 0x00005EC0
		public void getCategoriesAsync(int startOffSet, int rowCount, bool withSpecs, bool withDeepest, bool withCatalog, string lang, object userState)
		{
			if (this.getCategoriesOperationCompleted == null)
			{
				this.getCategoriesOperationCompleted = new SendOrPostCallback(this.OngetCategoriesOperationCompleted);
			}
			base.InvokeAsync("getCategories", new object[]
			{
				startOffSet,
				rowCount,
				withSpecs,
				withDeepest,
				withCatalog,
				lang
			}, this.getCategoriesOperationCompleted, userState);
		}

		// Token: 0x06000514 RID: 1300 RVA: 0x00007D38 File Offset: 0x00005F38
		private void OngetCategoriesOperationCompleted(object arg)
		{
			if (this.getCategoriesCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getCategoriesCompleted(this, new getCategoriesCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x06000515 RID: 1301 RVA: 0x00007D80 File Offset: 0x00005F80
		[SoapRpcMethod("", RequestNamespace = "http://category.anonymous.ws.listingapi.gg.com", ResponseNamespace = "http://category.anonymous.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public categoryServiceResponse getCategoriesByCodes([XmlArrayItem("item", Form = XmlSchemaForm.Unqualified)] string[] categoryCodes, bool withSpecs, bool withDeepest, bool withCatalog, string lang)
		{
			object[] array = base.Invoke("getCategoriesByCodes", new object[]
			{
				categoryCodes,
				withSpecs,
				withDeepest,
				withCatalog,
				lang
			});
			return (categoryServiceResponse)array[0];
		}

		// Token: 0x06000516 RID: 1302 RVA: 0x00007DCE File Offset: 0x00005FCE
		public void getCategoriesByCodesAsync(string[] categoryCodes, bool withSpecs, bool withDeepest, bool withCatalog, string lang)
		{
			this.getCategoriesByCodesAsync(categoryCodes, withSpecs, withDeepest, withCatalog, lang, null);
		}

		// Token: 0x06000517 RID: 1303 RVA: 0x00007DE0 File Offset: 0x00005FE0
		public void getCategoriesByCodesAsync(string[] categoryCodes, bool withSpecs, bool withDeepest, bool withCatalog, string lang, object userState)
		{
			if (this.getCategoriesByCodesOperationCompleted == null)
			{
				this.getCategoriesByCodesOperationCompleted = new SendOrPostCallback(this.OngetCategoriesByCodesOperationCompleted);
			}
			base.InvokeAsync("getCategoriesByCodes", new object[]
			{
				categoryCodes,
				withSpecs,
				withDeepest,
				withCatalog,
				lang
			}, this.getCategoriesByCodesOperationCompleted, userState);
		}

		// Token: 0x06000518 RID: 1304 RVA: 0x00007E48 File Offset: 0x00006048
		private void OngetCategoriesByCodesOperationCompleted(object arg)
		{
			if (this.getCategoriesByCodesCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getCategoriesByCodesCompleted(this, new getCategoriesByCodesCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x06000519 RID: 1305 RVA: 0x00007E90 File Offset: 0x00006090
		[SoapRpcMethod("", RequestNamespace = "http://category.anonymous.ws.listingapi.gg.com", ResponseNamespace = "http://category.anonymous.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public categoryServiceResponse getCategoriesHavingVariantSpecs(string lang)
		{
			object[] array = base.Invoke("getCategoriesHavingVariantSpecs", new object[]
			{
				lang
			});
			return (categoryServiceResponse)array[0];
		}

		// Token: 0x0600051A RID: 1306 RVA: 0x00007EBD File Offset: 0x000060BD
		public void getCategoriesHavingVariantSpecsAsync(string lang)
		{
			this.getCategoriesHavingVariantSpecsAsync(lang, null);
		}

		// Token: 0x0600051B RID: 1307 RVA: 0x00007EC8 File Offset: 0x000060C8
		public void getCategoriesHavingVariantSpecsAsync(string lang, object userState)
		{
			if (this.getCategoriesHavingVariantSpecsOperationCompleted == null)
			{
				this.getCategoriesHavingVariantSpecsOperationCompleted = new SendOrPostCallback(this.OngetCategoriesHavingVariantSpecsOperationCompleted);
			}
			base.InvokeAsync("getCategoriesHavingVariantSpecs", new object[]
			{
				lang
			}, this.getCategoriesHavingVariantSpecsOperationCompleted, userState);
		}

		// Token: 0x0600051C RID: 1308 RVA: 0x00007F10 File Offset: 0x00006110
		private void OngetCategoriesHavingVariantSpecsOperationCompleted(object arg)
		{
			if (this.getCategoriesHavingVariantSpecsCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getCategoriesHavingVariantSpecsCompleted(this, new getCategoriesHavingVariantSpecsCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x0600051D RID: 1309 RVA: 0x00007F58 File Offset: 0x00006158
		[SoapRpcMethod("", RequestNamespace = "http://category.anonymous.ws.listingapi.gg.com", ResponseNamespace = "http://category.anonymous.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public categoryServiceResponse getCategory(string categoryCode, bool withSpecs, bool withDeepest, bool withCatalog, string lang)
		{
			object[] array = base.Invoke("getCategory", new object[]
			{
				categoryCode,
				withSpecs,
				withDeepest,
				withCatalog,
				lang
			});
			return (categoryServiceResponse)array[0];
		}

		// Token: 0x0600051E RID: 1310 RVA: 0x00007FA6 File Offset: 0x000061A6
		public void getCategoryAsync(string categoryCode, bool withSpecs, bool withDeepest, bool withCatalog, string lang)
		{
			this.getCategoryAsync(categoryCode, withSpecs, withDeepest, withCatalog, lang, null);
		}

		// Token: 0x0600051F RID: 1311 RVA: 0x00007FB8 File Offset: 0x000061B8
		public void getCategoryAsync(string categoryCode, bool withSpecs, bool withDeepest, bool withCatalog, string lang, object userState)
		{
			if (this.getCategoryOperationCompleted == null)
			{
				this.getCategoryOperationCompleted = new SendOrPostCallback(this.OngetCategoryOperationCompleted);
			}
			base.InvokeAsync("getCategory", new object[]
			{
				categoryCode,
				withSpecs,
				withDeepest,
				withCatalog,
				lang
			}, this.getCategoryOperationCompleted, userState);
		}

		// Token: 0x06000520 RID: 1312 RVA: 0x00008020 File Offset: 0x00006220
		private void OngetCategoryOperationCompleted(object arg)
		{
			if (this.getCategoryCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getCategoryCompleted(this, new getCategoryCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x06000521 RID: 1313 RVA: 0x00008068 File Offset: 0x00006268
		[SoapRpcMethod("", RequestNamespace = "http://category.anonymous.ws.listingapi.gg.com", ResponseNamespace = "http://category.anonymous.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public categoryPermaServiceResponse getCategoryMetaData(string lang)
		{
			object[] array = base.Invoke("getCategoryMetaData", new object[]
			{
				lang
			});
			return (categoryPermaServiceResponse)array[0];
		}

		// Token: 0x06000522 RID: 1314 RVA: 0x00008095 File Offset: 0x00006295
		public void getCategoryMetaDataAsync(string lang)
		{
			this.getCategoryMetaDataAsync(lang, null);
		}

		// Token: 0x06000523 RID: 1315 RVA: 0x000080A0 File Offset: 0x000062A0
		public void getCategoryMetaDataAsync(string lang, object userState)
		{
			if (this.getCategoryMetaDataOperationCompleted == null)
			{
				this.getCategoryMetaDataOperationCompleted = new SendOrPostCallback(this.OngetCategoryMetaDataOperationCompleted);
			}
			base.InvokeAsync("getCategoryMetaData", new object[]
			{
				lang
			}, this.getCategoryMetaDataOperationCompleted, userState);
		}

		// Token: 0x06000524 RID: 1316 RVA: 0x000080E8 File Offset: 0x000062E8
		private void OngetCategoryMetaDataOperationCompleted(object arg)
		{
			if (this.getCategoryMetaDataCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getCategoryMetaDataCompleted(this, new getCategoryMetaDataCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x06000525 RID: 1317 RVA: 0x00008130 File Offset: 0x00006330
		[SoapRpcMethod("", RequestNamespace = "http://category.anonymous.ws.listingapi.gg.com", ResponseNamespace = "http://category.anonymous.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public categorySpecsServiceResponse getCategorySpecs(string categoryCode, string lang)
		{
			object[] array = base.Invoke("getCategorySpecs", new object[]
			{
				categoryCode,
				lang
			});
			return (categorySpecsServiceResponse)array[0];
		}

		// Token: 0x06000526 RID: 1318 RVA: 0x00008161 File Offset: 0x00006361
		public void getCategorySpecsAsync(string categoryCode, string lang)
		{
			this.getCategorySpecsAsync(categoryCode, lang, null);
		}

		// Token: 0x06000527 RID: 1319 RVA: 0x0000816C File Offset: 0x0000636C
		public void getCategorySpecsAsync(string categoryCode, string lang, object userState)
		{
			if (this.getCategorySpecsOperationCompleted == null)
			{
				this.getCategorySpecsOperationCompleted = new SendOrPostCallback(this.OngetCategorySpecsOperationCompleted);
			}
			base.InvokeAsync("getCategorySpecs", new object[]
			{
				categoryCode,
				lang
			}, this.getCategorySpecsOperationCompleted, userState);
		}

		// Token: 0x06000528 RID: 1320 RVA: 0x000081B8 File Offset: 0x000063B8
		private void OngetCategorySpecsOperationCompleted(object arg)
		{
			if (this.getCategorySpecsCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getCategorySpecsCompleted(this, new getCategorySpecsCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x06000529 RID: 1321 RVA: 0x00008200 File Offset: 0x00006400
		[SoapRpcMethod("", RequestNamespace = "http://category.anonymous.ws.listingapi.gg.com", ResponseNamespace = "http://category.anonymous.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public categorySpecsWithDetailServiceResponse getCategorySpecsWithDetail(string categoryCode, string lang)
		{
			object[] array = base.Invoke("getCategorySpecsWithDetail", new object[]
			{
				categoryCode,
				lang
			});
			return (categorySpecsWithDetailServiceResponse)array[0];
		}

		// Token: 0x0600052A RID: 1322 RVA: 0x00008231 File Offset: 0x00006431
		public void getCategorySpecsWithDetailAsync(string categoryCode, string lang)
		{
			this.getCategorySpecsWithDetailAsync(categoryCode, lang, null);
		}

		// Token: 0x0600052B RID: 1323 RVA: 0x0000823C File Offset: 0x0000643C
		public void getCategorySpecsWithDetailAsync(string categoryCode, string lang, object userState)
		{
			if (this.getCategorySpecsWithDetailOperationCompleted == null)
			{
				this.getCategorySpecsWithDetailOperationCompleted = new SendOrPostCallback(this.OngetCategorySpecsWithDetailOperationCompleted);
			}
			base.InvokeAsync("getCategorySpecsWithDetail", new object[]
			{
				categoryCode,
				lang
			}, this.getCategorySpecsWithDetailOperationCompleted, userState);
		}

		// Token: 0x0600052C RID: 1324 RVA: 0x00008288 File Offset: 0x00006488
		private void OngetCategorySpecsWithDetailOperationCompleted(object arg)
		{
			if (this.getCategorySpecsWithDetailCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getCategorySpecsWithDetailCompleted(this, new getCategorySpecsWithDetailCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x0600052D RID: 1325 RVA: 0x000082D0 File Offset: 0x000064D0
		[SoapRpcMethod("", RequestNamespace = "http://category.anonymous.ws.listingapi.gg.com", ResponseNamespace = "http://category.anonymous.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public categoryServiceVariantResponse getCategoryVariantSpecs(string categoryCode, string lang)
		{
			object[] array = base.Invoke("getCategoryVariantSpecs", new object[]
			{
				categoryCode,
				lang
			});
			return (categoryServiceVariantResponse)array[0];
		}

		// Token: 0x0600052E RID: 1326 RVA: 0x00008301 File Offset: 0x00006501
		public void getCategoryVariantSpecsAsync(string categoryCode, string lang)
		{
			this.getCategoryVariantSpecsAsync(categoryCode, lang, null);
		}

		// Token: 0x0600052F RID: 1327 RVA: 0x0000830C File Offset: 0x0000650C
		public void getCategoryVariantSpecsAsync(string categoryCode, string lang, object userState)
		{
			if (this.getCategoryVariantSpecsOperationCompleted == null)
			{
				this.getCategoryVariantSpecsOperationCompleted = new SendOrPostCallback(this.OngetCategoryVariantSpecsOperationCompleted);
			}
			base.InvokeAsync("getCategoryVariantSpecs", new object[]
			{
				categoryCode,
				lang
			}, this.getCategoryVariantSpecsOperationCompleted, userState);
		}

		// Token: 0x06000530 RID: 1328 RVA: 0x00008358 File Offset: 0x00006558
		private void OngetCategoryVariantSpecsOperationCompleted(object arg)
		{
			if (this.getCategoryVariantSpecsCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getCategoryVariantSpecsCompleted(this, new getCategoryVariantSpecsCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x06000531 RID: 1329 RVA: 0x000083A0 File Offset: 0x000065A0
		[SoapRpcMethod("", RequestNamespace = "http://category.anonymous.ws.listingapi.gg.com", ResponseNamespace = "http://category.anonymous.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public categoryServiceResponse getDeepestCategories(int startOffSet, int rowCount, bool withSpecs, string lang)
		{
			object[] array = base.Invoke("getDeepestCategories", new object[]
			{
				startOffSet,
				rowCount,
				withSpecs,
				lang
			});
			return (categoryServiceResponse)array[0];
		}

		// Token: 0x06000532 RID: 1330 RVA: 0x000083E9 File Offset: 0x000065E9
		public void getDeepestCategoriesAsync(int startOffSet, int rowCount, bool withSpecs, string lang)
		{
			this.getDeepestCategoriesAsync(startOffSet, rowCount, withSpecs, lang, null);
		}

		// Token: 0x06000533 RID: 1331 RVA: 0x000083F8 File Offset: 0x000065F8
		public void getDeepestCategoriesAsync(int startOffSet, int rowCount, bool withSpecs, string lang, object userState)
		{
			if (this.getDeepestCategoriesOperationCompleted == null)
			{
				this.getDeepestCategoriesOperationCompleted = new SendOrPostCallback(this.OngetDeepestCategoriesOperationCompleted);
			}
			base.InvokeAsync("getDeepestCategories", new object[]
			{
				startOffSet,
				rowCount,
				withSpecs,
				lang
			}, this.getDeepestCategoriesOperationCompleted, userState);
		}

		// Token: 0x06000534 RID: 1332 RVA: 0x0000845C File Offset: 0x0000665C
		private void OngetDeepestCategoriesOperationCompleted(object arg)
		{
			if (this.getDeepestCategoriesCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getDeepestCategoriesCompleted(this, new getDeepestCategoriesCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x06000535 RID: 1333 RVA: 0x000084A4 File Offset: 0x000066A4
		[SoapRpcMethod("", RequestNamespace = "http://category.anonymous.ws.listingapi.gg.com", ResponseNamespace = "http://category.anonymous.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public categoryServiceResponse getDepositCategories(string lang)
		{
			object[] array = base.Invoke("getDepositCategories", new object[]
			{
				lang
			});
			return (categoryServiceResponse)array[0];
		}

		// Token: 0x06000536 RID: 1334 RVA: 0x000084D1 File Offset: 0x000066D1
		public void getDepositCategoriesAsync(string lang)
		{
			this.getDepositCategoriesAsync(lang, null);
		}

		// Token: 0x06000537 RID: 1335 RVA: 0x000084DC File Offset: 0x000066DC
		public void getDepositCategoriesAsync(string lang, object userState)
		{
			if (this.getDepositCategoriesOperationCompleted == null)
			{
				this.getDepositCategoriesOperationCompleted = new SendOrPostCallback(this.OngetDepositCategoriesOperationCompleted);
			}
			base.InvokeAsync("getDepositCategories", new object[]
			{
				lang
			}, this.getDepositCategoriesOperationCompleted, userState);
		}

		// Token: 0x06000538 RID: 1336 RVA: 0x00008524 File Offset: 0x00006724
		private void OngetDepositCategoriesOperationCompleted(object arg)
		{
			if (this.getDepositCategoriesCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getDepositCategoriesCompleted(this, new getDepositCategoriesCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x06000539 RID: 1337 RVA: 0x0000856C File Offset: 0x0000676C
		[SoapRpcMethod("", RequestNamespace = "http://category.anonymous.ws.listingapi.gg.com", ResponseNamespace = "http://category.anonymous.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public categoryServiceAuditResponse getModifiedCategories(long changeTime, int startOffSet, int rowCount, string lang)
		{
			object[] array = base.Invoke("getModifiedCategories", new object[]
			{
				changeTime,
				startOffSet,
				rowCount,
				lang
			});
			return (categoryServiceAuditResponse)array[0];
		}

		// Token: 0x0600053A RID: 1338 RVA: 0x000085B5 File Offset: 0x000067B5
		public void getModifiedCategoriesAsync(long changeTime, int startOffSet, int rowCount, string lang)
		{
			this.getModifiedCategoriesAsync(changeTime, startOffSet, rowCount, lang, null);
		}

		// Token: 0x0600053B RID: 1339 RVA: 0x000085C4 File Offset: 0x000067C4
		public void getModifiedCategoriesAsync(long changeTime, int startOffSet, int rowCount, string lang, object userState)
		{
			if (this.getModifiedCategoriesOperationCompleted == null)
			{
				this.getModifiedCategoriesOperationCompleted = new SendOrPostCallback(this.OngetModifiedCategoriesOperationCompleted);
			}
			base.InvokeAsync("getModifiedCategories", new object[]
			{
				changeTime,
				startOffSet,
				rowCount,
				lang
			}, this.getModifiedCategoriesOperationCompleted, userState);
		}

		// Token: 0x0600053C RID: 1340 RVA: 0x00008628 File Offset: 0x00006828
		private void OngetModifiedCategoriesOperationCompleted(object arg)
		{
			if (this.getModifiedCategoriesCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getModifiedCategoriesCompleted(this, new getModifiedCategoriesCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x0600053D RID: 1341 RVA: 0x00008670 File Offset: 0x00006870
		[SoapRpcMethod("", RequestNamespace = "http://category.anonymous.ws.listingapi.gg.com", ResponseNamespace = "http://category.anonymous.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public categoryServiceResponse getParentCategories(bool withSpecs, bool withDeepest, bool withCatalog, string lang)
		{
			object[] array = base.Invoke("getParentCategories", new object[]
			{
				withSpecs,
				withDeepest,
				withCatalog,
				lang
			});
			return (categoryServiceResponse)array[0];
		}

		// Token: 0x0600053E RID: 1342 RVA: 0x000086B9 File Offset: 0x000068B9
		public void getParentCategoriesAsync(bool withSpecs, bool withDeepest, bool withCatalog, string lang)
		{
			this.getParentCategoriesAsync(withSpecs, withDeepest, withCatalog, lang, null);
		}

		// Token: 0x0600053F RID: 1343 RVA: 0x000086C8 File Offset: 0x000068C8
		public void getParentCategoriesAsync(bool withSpecs, bool withDeepest, bool withCatalog, string lang, object userState)
		{
			if (this.getParentCategoriesOperationCompleted == null)
			{
				this.getParentCategoriesOperationCompleted = new SendOrPostCallback(this.OngetParentCategoriesOperationCompleted);
			}
			base.InvokeAsync("getParentCategories", new object[]
			{
				withSpecs,
				withDeepest,
				withCatalog,
				lang
			}, this.getParentCategoriesOperationCompleted, userState);
		}

		// Token: 0x06000540 RID: 1344 RVA: 0x0000872C File Offset: 0x0000692C
		private void OngetParentCategoriesOperationCompleted(object arg)
		{
			if (this.getParentCategoriesCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getParentCategoriesCompleted(this, new getParentCategoriesCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x06000541 RID: 1345 RVA: 0x00008774 File Offset: 0x00006974
		[SoapRpcMethod("", RequestNamespace = "http://category.anonymous.ws.listingapi.gg.com", ResponseNamespace = "http://category.anonymous.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public string getServiceName()
		{
			object[] array = base.Invoke("getServiceName", new object[0]);
			return (string)array[0];
		}

		// Token: 0x06000542 RID: 1346 RVA: 0x0000879B File Offset: 0x0000699B
		public void getServiceNameAsync()
		{
			this.getServiceNameAsync(null);
		}

		// Token: 0x06000543 RID: 1347 RVA: 0x000087A4 File Offset: 0x000069A4
		public void getServiceNameAsync(object userState)
		{
			if (this.getServiceNameOperationCompleted == null)
			{
				this.getServiceNameOperationCompleted = new SendOrPostCallback(this.OngetServiceNameOperationCompleted);
			}
			base.InvokeAsync("getServiceName", new object[0], this.getServiceNameOperationCompleted, userState);
		}

		// Token: 0x06000544 RID: 1348 RVA: 0x000087D8 File Offset: 0x000069D8
		private void OngetServiceNameOperationCompleted(object arg)
		{
			if (this.getServiceNameCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getServiceNameCompleted(this, new getServiceNameCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x06000545 RID: 1349 RVA: 0x00008820 File Offset: 0x00006A20
		[SoapRpcMethod("", RequestNamespace = "http://category.anonymous.ws.listingapi.gg.com", ResponseNamespace = "http://category.anonymous.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public categoryServiceResponse getSubCategories(string categoryCode, bool withSpecs, bool withDeepest, bool withCatalog, string lang)
		{
			object[] array = base.Invoke("getSubCategories", new object[]
			{
				categoryCode,
				withSpecs,
				withDeepest,
				withCatalog,
				lang
			});
			return (categoryServiceResponse)array[0];
		}

		// Token: 0x06000546 RID: 1350 RVA: 0x0000886E File Offset: 0x00006A6E
		public void getSubCategoriesAsync(string categoryCode, bool withSpecs, bool withDeepest, bool withCatalog, string lang)
		{
			this.getSubCategoriesAsync(categoryCode, withSpecs, withDeepest, withCatalog, lang, null);
		}

		// Token: 0x06000547 RID: 1351 RVA: 0x00008880 File Offset: 0x00006A80
		public void getSubCategoriesAsync(string categoryCode, bool withSpecs, bool withDeepest, bool withCatalog, string lang, object userState)
		{
			if (this.getSubCategoriesOperationCompleted == null)
			{
				this.getSubCategoriesOperationCompleted = new SendOrPostCallback(this.OngetSubCategoriesOperationCompleted);
			}
			base.InvokeAsync("getSubCategories", new object[]
			{
				categoryCode,
				withSpecs,
				withDeepest,
				withCatalog,
				lang
			}, this.getSubCategoriesOperationCompleted, userState);
		}

		// Token: 0x06000548 RID: 1352 RVA: 0x000088E8 File Offset: 0x00006AE8
		private void OngetSubCategoriesOperationCompleted(object arg)
		{
			if (this.getSubCategoriesCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getSubCategoriesCompleted(this, new getSubCategoriesCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x06000549 RID: 1353 RVA: 0x0000892D File Offset: 0x00006B2D
		public new void CancelAsync(object userState)
		{
			base.CancelAsync(userState);
		}

		// Token: 0x0600054A RID: 1354 RVA: 0x00008938 File Offset: 0x00006B38
		private bool IsLocalFileSystemWebService(string url)
		{
			if (url == null || url == string.Empty)
			{
				return false;
			}
			Uri uri = new Uri(url);
			return uri.Port >= 1024 && string.Compare(uri.Host, "localHost", StringComparison.OrdinalIgnoreCase) == 0;
		}

		// Token: 0x040001E3 RID: 483
		private SendOrPostCallback getCategoriesOperationCompleted;

		// Token: 0x040001E4 RID: 484
		private SendOrPostCallback getCategoriesByCodesOperationCompleted;

		// Token: 0x040001E5 RID: 485
		private SendOrPostCallback getCategoriesHavingVariantSpecsOperationCompleted;

		// Token: 0x040001E6 RID: 486
		private SendOrPostCallback getCategoryOperationCompleted;

		// Token: 0x040001E7 RID: 487
		private SendOrPostCallback getCategoryMetaDataOperationCompleted;

		// Token: 0x040001E8 RID: 488
		private SendOrPostCallback getCategorySpecsOperationCompleted;

		// Token: 0x040001E9 RID: 489
		private SendOrPostCallback getCategorySpecsWithDetailOperationCompleted;

		// Token: 0x040001EA RID: 490
		private SendOrPostCallback getCategoryVariantSpecsOperationCompleted;

		// Token: 0x040001EB RID: 491
		private SendOrPostCallback getDeepestCategoriesOperationCompleted;

		// Token: 0x040001EC RID: 492
		private SendOrPostCallback getDepositCategoriesOperationCompleted;

		// Token: 0x040001ED RID: 493
		private SendOrPostCallback getModifiedCategoriesOperationCompleted;

		// Token: 0x040001EE RID: 494
		private SendOrPostCallback getParentCategoriesOperationCompleted;

		// Token: 0x040001EF RID: 495
		private SendOrPostCallback getServiceNameOperationCompleted;

		// Token: 0x040001F0 RID: 496
		private SendOrPostCallback getSubCategoriesOperationCompleted;

		// Token: 0x040001F1 RID: 497
		private bool useDefaultCredentialsSetExplicitly;
	}
}
