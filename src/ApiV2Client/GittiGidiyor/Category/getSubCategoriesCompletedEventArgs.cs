﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Category
{
	// Token: 0x020000B7 RID: 183
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	public class getSubCategoriesCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000638 RID: 1592 RVA: 0x000090A6 File Offset: 0x000072A6
		internal getSubCategoriesCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x17000209 RID: 521
		// (get) Token: 0x06000639 RID: 1593 RVA: 0x000090B9 File Offset: 0x000072B9
		public categoryServiceResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (categoryServiceResponse)this.results[0];
			}
		}

		// Token: 0x04000252 RID: 594
		private object[] results;
	}
}
