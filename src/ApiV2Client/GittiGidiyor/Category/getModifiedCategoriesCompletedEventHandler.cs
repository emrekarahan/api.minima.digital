﻿using System.CodeDom.Compiler;

namespace ApiV2Client.GittiGidiyor.Category
{
	// Token: 0x020000B0 RID: 176
	// (Invoke) Token: 0x06000623 RID: 1571
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	public delegate void getModifiedCategoriesCompletedEventHandler(object sender, getModifiedCategoriesCompletedEventArgs e);
}
