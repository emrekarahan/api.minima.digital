﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Category
{
	// Token: 0x0200008F RID: 143
	[XmlType(Namespace = "http://category.anonymous.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.7.3190.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[Serializable]
	public class categoryVariantSpecValueType
	{
		// Token: 0x170001CA RID: 458
		// (get) Token: 0x06000575 RID: 1397 RVA: 0x00008AE4 File Offset: 0x00006CE4
		// (set) Token: 0x06000576 RID: 1398 RVA: 0x00008AEC File Offset: 0x00006CEC
		[XmlAttribute]
		public long valueId
		{
			get
			{
				return this.valueIdField;
			}
			set
			{
				this.valueIdField = value;
			}
		}

		// Token: 0x170001CB RID: 459
		// (get) Token: 0x06000577 RID: 1399 RVA: 0x00008AF5 File Offset: 0x00006CF5
		// (set) Token: 0x06000578 RID: 1400 RVA: 0x00008AFD File Offset: 0x00006CFD
		[XmlIgnore]
		public bool valueIdSpecified
		{
			get
			{
				return this.valueIdFieldSpecified;
			}
			set
			{
				this.valueIdFieldSpecified = value;
			}
		}

		// Token: 0x170001CC RID: 460
		// (get) Token: 0x06000579 RID: 1401 RVA: 0x00008B06 File Offset: 0x00006D06
		// (set) Token: 0x0600057A RID: 1402 RVA: 0x00008B0E File Offset: 0x00006D0E
		[XmlAttribute]
		public string value
		{
			get
			{
				return this.valueField;
			}
			set
			{
				this.valueField = value;
			}
		}

		// Token: 0x170001CD RID: 461
		// (get) Token: 0x0600057B RID: 1403 RVA: 0x00008B17 File Offset: 0x00006D17
		// (set) Token: 0x0600057C RID: 1404 RVA: 0x00008B1F File Offset: 0x00006D1F
		[XmlAttribute]
		public int orderNumber
		{
			get
			{
				return this.orderNumberField;
			}
			set
			{
				this.orderNumberField = value;
			}
		}

		// Token: 0x170001CE RID: 462
		// (get) Token: 0x0600057D RID: 1405 RVA: 0x00008B28 File Offset: 0x00006D28
		// (set) Token: 0x0600057E RID: 1406 RVA: 0x00008B30 File Offset: 0x00006D30
		[XmlIgnore]
		public bool orderNumberSpecified
		{
			get
			{
				return this.orderNumberFieldSpecified;
			}
			set
			{
				this.orderNumberFieldSpecified = value;
			}
		}

		// Token: 0x04000213 RID: 531
		private long valueIdField;

		// Token: 0x04000214 RID: 532
		private bool valueIdFieldSpecified;

		// Token: 0x04000215 RID: 533
		private string valueField;

		// Token: 0x04000216 RID: 534
		private int orderNumberField;

		// Token: 0x04000217 RID: 535
		private bool orderNumberFieldSpecified;
	}
}
