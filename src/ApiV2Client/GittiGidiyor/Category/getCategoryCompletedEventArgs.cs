﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Category
{
	// Token: 0x020000A3 RID: 163
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	[DesignerCategory("code")]
	[DebuggerStepThrough]
	public class getCategoryCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x060005FC RID: 1532 RVA: 0x00008F16 File Offset: 0x00007116
		internal getCategoryCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x170001FF RID: 511
		// (get) Token: 0x060005FD RID: 1533 RVA: 0x00008F29 File Offset: 0x00007129
		public categoryServiceResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (categoryServiceResponse)this.results[0];
			}
		}

		// Token: 0x04000248 RID: 584
		private object[] results;
	}
}
