﻿using System.CodeDom.Compiler;

namespace ApiV2Client.GittiGidiyor.Category
{
	// Token: 0x020000AA RID: 170
	// (Invoke) Token: 0x06000611 RID: 1553
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	public delegate void getCategoryVariantSpecsCompletedEventHandler(object sender, getCategoryVariantSpecsCompletedEventArgs e);
}
