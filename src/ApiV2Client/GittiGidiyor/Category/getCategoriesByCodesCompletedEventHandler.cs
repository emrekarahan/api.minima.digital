﻿using System.CodeDom.Compiler;

namespace ApiV2Client.GittiGidiyor.Category
{
	// Token: 0x0200009E RID: 158
	// (Invoke) Token: 0x060005ED RID: 1517
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	public delegate void getCategoriesByCodesCompletedEventHandler(object sender, getCategoriesByCodesCompletedEventArgs e);
}
