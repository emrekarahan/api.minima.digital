﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Category
{
	// Token: 0x02000094 RID: 148
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[XmlType(Namespace = "http://category.anonymous.ws.listingapi.gg.com")]
	[XmlInclude(typeof(categoryAuditType))]
	[GeneratedCode("System.Xml", "4.7.3190.0")]
	[Serializable]
	public abstract class baseAuditType
	{
		// Token: 0x170001E9 RID: 489
		// (get) Token: 0x060005B8 RID: 1464 RVA: 0x00008D1B File Offset: 0x00006F1B
		// (set) Token: 0x060005B9 RID: 1465 RVA: 0x00008D23 File Offset: 0x00006F23
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string changeType
		{
			get
			{
				return this.changeTypeField;
			}
			set
			{
				this.changeTypeField = value;
			}
		}

		// Token: 0x04000232 RID: 562
		private string changeTypeField;
	}
}
