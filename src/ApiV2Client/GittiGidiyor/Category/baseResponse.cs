﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Category
{
	// Token: 0x0200008B RID: 139
	[DesignerCategory("code")]
	[XmlInclude(typeof(categoryServiceAuditResponse))]
	[GeneratedCode("System.Xml", "4.7.3190.0")]
	[DebuggerStepThrough]
	[XmlInclude(typeof(categorySpecsServiceResponse))]
	[XmlType(Namespace = "http://category.anonymous.ws.listingapi.gg.com")]
	[XmlInclude(typeof(categorySpecsWithDetailServiceResponse))]
	[XmlInclude(typeof(categoryServiceVariantResponse))]
	[XmlInclude(typeof(categoryServiceResponse))]
	[XmlInclude(typeof(categoryPermaServiceResponse))]
	[Serializable]
	public class baseResponse
	{
		// Token: 0x170001B7 RID: 439
		// (get) Token: 0x0600054B RID: 1355 RVA: 0x00008981 File Offset: 0x00006B81
		// (set) Token: 0x0600054C RID: 1356 RVA: 0x00008989 File Offset: 0x00006B89
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string ackCode
		{
			get
			{
				return this.ackCodeField;
			}
			set
			{
				this.ackCodeField = value;
			}
		}

		// Token: 0x170001B8 RID: 440
		// (get) Token: 0x0600054D RID: 1357 RVA: 0x00008992 File Offset: 0x00006B92
		// (set) Token: 0x0600054E RID: 1358 RVA: 0x0000899A File Offset: 0x00006B9A
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string responseTime
		{
			get
			{
				return this.responseTimeField;
			}
			set
			{
				this.responseTimeField = value;
			}
		}

		// Token: 0x170001B9 RID: 441
		// (get) Token: 0x0600054F RID: 1359 RVA: 0x000089A3 File Offset: 0x00006BA3
		// (set) Token: 0x06000550 RID: 1360 RVA: 0x000089AB File Offset: 0x00006BAB
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public errorType error
		{
			get
			{
				return this.errorField;
			}
			set
			{
				this.errorField = value;
			}
		}

		// Token: 0x170001BA RID: 442
		// (get) Token: 0x06000551 RID: 1361 RVA: 0x000089B4 File Offset: 0x00006BB4
		// (set) Token: 0x06000552 RID: 1362 RVA: 0x000089BC File Offset: 0x00006BBC
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string timeElapsed
		{
			get
			{
				return this.timeElapsedField;
			}
			set
			{
				this.timeElapsedField = value;
			}
		}

		// Token: 0x04000200 RID: 512
		private string ackCodeField;

		// Token: 0x04000201 RID: 513
		private string responseTimeField;

		// Token: 0x04000202 RID: 514
		private errorType errorField;

		// Token: 0x04000203 RID: 515
		private string timeElapsedField;
	}
}
