﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Category
{
	// Token: 0x020000A1 RID: 161
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	public class getCategoriesHavingVariantSpecsCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x060005F6 RID: 1526 RVA: 0x00008EEE File Offset: 0x000070EE
		internal getCategoriesHavingVariantSpecsCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x170001FE RID: 510
		// (get) Token: 0x060005F7 RID: 1527 RVA: 0x00008F01 File Offset: 0x00007101
		public categoryServiceResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (categoryServiceResponse)this.results[0];
			}
		}

		// Token: 0x04000247 RID: 583
		private object[] results;
	}
}
