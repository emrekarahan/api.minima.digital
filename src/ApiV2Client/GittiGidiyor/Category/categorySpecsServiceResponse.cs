﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Category
{
	// Token: 0x02000097 RID: 151
	[DebuggerStepThrough]
	[GeneratedCode("System.Xml", "4.7.3190.0")]
	[XmlType(Namespace = "http://category.anonymous.ws.listingapi.gg.com")]
	[DesignerCategory("code")]
	[Serializable]
	public class categorySpecsServiceResponse : baseResponse
	{
		// Token: 0x170001EF RID: 495
		// (get) Token: 0x060005C7 RID: 1479 RVA: 0x00008D99 File Offset: 0x00006F99
		// (set) Token: 0x060005C8 RID: 1480 RVA: 0x00008DA1 File Offset: 0x00006FA1
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		[XmlArrayItem("spec", Form = XmlSchemaForm.Unqualified)]
		public categorySpecType[] specs
		{
			get
			{
				return this.specsField;
			}
			set
			{
				this.specsField = value;
			}
		}

		// Token: 0x04000238 RID: 568
		private categorySpecType[] specsField;
	}
}
