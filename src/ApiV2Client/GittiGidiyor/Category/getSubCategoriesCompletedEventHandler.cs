﻿using System.CodeDom.Compiler;

namespace ApiV2Client.GittiGidiyor.Category
{
	// Token: 0x020000B6 RID: 182
	// (Invoke) Token: 0x06000635 RID: 1589
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	public delegate void getSubCategoriesCompletedEventHandler(object sender, getSubCategoriesCompletedEventArgs e);
}
