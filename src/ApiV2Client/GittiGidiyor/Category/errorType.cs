﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Category
{
	// Token: 0x02000096 RID: 150
	[DesignerCategory("code")]
	[DebuggerStepThrough]
	[XmlType(Namespace = "http://category.anonymous.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.7.3190.0")]
	[Serializable]
	public class errorType
	{
		// Token: 0x170001EB RID: 491
		// (get) Token: 0x060005BE RID: 1470 RVA: 0x00008D4D File Offset: 0x00006F4D
		// (set) Token: 0x060005BF RID: 1471 RVA: 0x00008D55 File Offset: 0x00006F55
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string errorId
		{
			get
			{
				return this.errorIdField;
			}
			set
			{
				this.errorIdField = value;
			}
		}

		// Token: 0x170001EC RID: 492
		// (get) Token: 0x060005C0 RID: 1472 RVA: 0x00008D5E File Offset: 0x00006F5E
		// (set) Token: 0x060005C1 RID: 1473 RVA: 0x00008D66 File Offset: 0x00006F66
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string errorCode
		{
			get
			{
				return this.errorCodeField;
			}
			set
			{
				this.errorCodeField = value;
			}
		}

		// Token: 0x170001ED RID: 493
		// (get) Token: 0x060005C2 RID: 1474 RVA: 0x00008D6F File Offset: 0x00006F6F
		// (set) Token: 0x060005C3 RID: 1475 RVA: 0x00008D77 File Offset: 0x00006F77
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string message
		{
			get
			{
				return this.messageField;
			}
			set
			{
				this.messageField = value;
			}
		}

		// Token: 0x170001EE RID: 494
		// (get) Token: 0x060005C4 RID: 1476 RVA: 0x00008D80 File Offset: 0x00006F80
		// (set) Token: 0x060005C5 RID: 1477 RVA: 0x00008D88 File Offset: 0x00006F88
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string viewMessage
		{
			get
			{
				return this.viewMessageField;
			}
			set
			{
				this.viewMessageField = value;
			}
		}

		// Token: 0x04000234 RID: 564
		private string errorIdField;

		// Token: 0x04000235 RID: 565
		private string errorCodeField;

		// Token: 0x04000236 RID: 566
		private string messageField;

		// Token: 0x04000237 RID: 567
		private string viewMessageField;
	}
}
