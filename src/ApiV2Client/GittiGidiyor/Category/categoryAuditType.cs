﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Category
{
	// Token: 0x02000095 RID: 149
	[XmlType(Namespace = "http://category.anonymous.ws.listingapi.gg.com")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[GeneratedCode("System.Xml", "4.7.3190.0")]
	[Serializable]
	public class categoryAuditType : baseAuditType
	{
		// Token: 0x170001EA RID: 490
		// (get) Token: 0x060005BB RID: 1467 RVA: 0x00008D34 File Offset: 0x00006F34
		// (set) Token: 0x060005BC RID: 1468 RVA: 0x00008D3C File Offset: 0x00006F3C
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string categoryCode
		{
			get
			{
				return this.categoryCodeField;
			}
			set
			{
				this.categoryCodeField = value;
			}
		}

		// Token: 0x04000233 RID: 563
		private string categoryCodeField;
	}
}
