﻿namespace ApiV2Client.GittiGidiyor.Category
{
	// Token: 0x020001EB RID: 491
	public class CategoryService : ServiceClient<CategoryServiceService>, ICategoryService, IService
	{
		// Token: 0x060010A8 RID: 4264 RVA: 0x000162C0 File Offset: 0x000144C0
		private CategoryService()
		{
			base.configureNetworkCredentials(this.service);
		}

		// Token: 0x060010A9 RID: 4265 RVA: 0x000162DF File Offset: 0x000144DF
		public categoryServiceResponse getCategories(int startOffSet, int rowCount, bool withSpecs, bool withDeepest, bool withCatalog, string lang)
		{
			return this.service.getCategories(startOffSet, rowCount, withSpecs, withDeepest, withCatalog, lang);
		}

		// Token: 0x060010AA RID: 4266 RVA: 0x000162F5 File Offset: 0x000144F5
		public categoryServiceResponse getCategoriesByCodes(string[] categoryCodes, bool withSpecs, bool withDeepest, bool withCatalog, string lang)
		{
			return this.service.getCategoriesByCodes(categoryCodes, withSpecs, withDeepest, withCatalog, lang);
		}

		// Token: 0x060010AB RID: 4267 RVA: 0x00016309 File Offset: 0x00014509
		public categoryServiceResponse getCategory(string categoryCode, bool withSpecs, bool withDeepest, bool withCatalog, string lang)
		{
			return this.service.getCategory(categoryCode, withSpecs, withDeepest, withCatalog, lang);
		}

		// Token: 0x060010AC RID: 4268 RVA: 0x0001631D File Offset: 0x0001451D
		public categorySpecsServiceResponse getCategorySpecs(string categoryCode, string lang)
		{
			return this.service.getCategorySpecs(categoryCode, lang);
		}

		// Token: 0x060010AD RID: 4269 RVA: 0x0001632C File Offset: 0x0001452C
		public categoryServiceResponse getDeepestCategories(int startOffSet, int rowCount, bool withSpecs, string lang)
		{
			return this.service.getDeepestCategories(startOffSet, rowCount, withSpecs, lang);
		}

		// Token: 0x060010AE RID: 4270 RVA: 0x0001633E File Offset: 0x0001453E
		public categoryServiceAuditResponse getModifiedCategories(long changeTime, int startOffset, int rowCount, string lang)
		{
			return this.service.getModifiedCategories(changeTime, startOffset, rowCount, lang);
		}

		// Token: 0x060010AF RID: 4271 RVA: 0x00016350 File Offset: 0x00014550
		public categoryServiceVariantResponse getCategoryVariantSpecs(string categoryCode, string lang)
		{
			return this.service.getCategoryVariantSpecs(categoryCode, lang);
		}

		// Token: 0x060010B0 RID: 4272 RVA: 0x0001635F File Offset: 0x0001455F
		public categoryServiceResponse getCategoriesHavingVariantSpecs(string lang)
		{
			return this.service.getCategoriesHavingVariantSpecs(lang);
		}

		// Token: 0x060010B1 RID: 4273 RVA: 0x0001636D File Offset: 0x0001456D
		public categoryServiceResponse getSubCategories(string categoryCode, bool withSpecs, bool withDeepest, bool withCatalog, string lang)
		{
			return this.service.getSubCategories(categoryCode, withSpecs, withDeepest, withCatalog, lang);
		}

		// Token: 0x060010B2 RID: 4274 RVA: 0x00016381 File Offset: 0x00014581
		public categorySpecsWithDetailServiceResponse getCategorySpecsWithDetail(string categoryCode, string lang)
		{
			return this.service.getCategorySpecsWithDetail(categoryCode, lang);
		}

		// Token: 0x060010B3 RID: 4275 RVA: 0x00016390 File Offset: 0x00014590
		public categoryServiceResponse getParentCategories(bool withSpecs, bool withDeepest, bool withCatalog, string lang)
		{
			return this.service.getParentCategories(withSpecs, withDeepest, withCatalog, lang);
		}

		// Token: 0x060010B4 RID: 4276 RVA: 0x000163A2 File Offset: 0x000145A2
		public categoryServiceResponse getDepositCategories(string lang)
		{
			return this.service.getDepositCategories(lang);
		}

		// Token: 0x060010B5 RID: 4277 RVA: 0x000163B0 File Offset: 0x000145B0
		public string getServiceName()
		{
			return this.service.getServiceName();
		}

		// Token: 0x1700051A RID: 1306
		// (get) Token: 0x060010B6 RID: 4278 RVA: 0x000163C0 File Offset: 0x000145C0
		public static CategoryService Instance
		{
			get
			{
				if (CategoryService.instance == null)
				{
					lock (CategoryService.lockObject)
					{
						if (CategoryService.instance == null)
						{
							CategoryService.instance = new CategoryService();
						}
					}
				}
				return CategoryService.instance;
			}
		}

		// Token: 0x0400061F RID: 1567
		private static CategoryService instance;

		// Token: 0x04000620 RID: 1568
		private static object lockObject = new object();

		// Token: 0x04000621 RID: 1569
		private CategoryServiceService service = new CategoryServiceService();
	}
}
