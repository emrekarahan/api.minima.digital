﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Category
{
	// Token: 0x020000A5 RID: 165
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	public class getCategoryMetaDataCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000602 RID: 1538 RVA: 0x00008F3E File Offset: 0x0000713E
		internal getCategoryMetaDataCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x17000200 RID: 512
		// (get) Token: 0x06000603 RID: 1539 RVA: 0x00008F51 File Offset: 0x00007151
		public categoryPermaServiceResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (categoryPermaServiceResponse)this.results[0];
			}
		}

		// Token: 0x04000249 RID: 585
		private object[] results;
	}
}
