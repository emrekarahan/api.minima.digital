﻿namespace ApiV2Client.GittiGidiyor.Category
{
	// Token: 0x020001E9 RID: 489
	public interface ICategoryService : IService
	{
		// Token: 0x06001096 RID: 4246
		categoryServiceResponse getCategories(int startOffSet, int rowCount, bool withSpecs, bool withDeepest, bool withCatalog, string lang);

		// Token: 0x06001097 RID: 4247
		categoryServiceResponse getCategoriesByCodes(string[] categoryCodes, bool withSpecs, bool withDeepest, bool withCatalog, string lang);

		// Token: 0x06001098 RID: 4248
		categoryServiceResponse getCategory(string categoryCode, bool withSpecs, bool withDeepest, bool withCatalog, string lang);

		// Token: 0x06001099 RID: 4249
		categorySpecsServiceResponse getCategorySpecs(string categoryCode, string lang);

		// Token: 0x0600109A RID: 4250
		categoryServiceResponse getDeepestCategories(int startOffSet, int rowCount, bool withSpecs, string lang);

		// Token: 0x0600109B RID: 4251
		categoryServiceAuditResponse getModifiedCategories(long changeTime, int startOffset, int rowCount, string lang);

		// Token: 0x0600109C RID: 4252
		categoryServiceVariantResponse getCategoryVariantSpecs(string categoryCode, string lang);

		// Token: 0x0600109D RID: 4253
		categoryServiceResponse getCategoriesHavingVariantSpecs(string lang);

		// Token: 0x0600109E RID: 4254
		categoryServiceResponse getSubCategories(string categoryCode, bool withSpecs, bool withDeepest, bool withCatalog, string lang);

		// Token: 0x0600109F RID: 4255
		categorySpecsWithDetailServiceResponse getCategorySpecsWithDetail(string categoryCode, string lang);

		// Token: 0x060010A0 RID: 4256
		categoryServiceResponse getParentCategories(bool withSpecs, bool withDeepest, bool withCatalog, string lang);

		// Token: 0x060010A1 RID: 4257
		categoryServiceResponse getDepositCategories(string lang);
	}
}
