﻿using System.CodeDom.Compiler;

namespace ApiV2Client.GittiGidiyor.Category
{
	// Token: 0x020000A8 RID: 168
	// (Invoke) Token: 0x0600060B RID: 1547
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	public delegate void getCategorySpecsWithDetailCompletedEventHandler(object sender, getCategorySpecsWithDetailCompletedEventArgs e);
}
