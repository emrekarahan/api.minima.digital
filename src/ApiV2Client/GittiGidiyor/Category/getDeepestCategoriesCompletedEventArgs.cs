﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Category
{
	// Token: 0x020000AD RID: 173
	[DesignerCategory("code")]
	[DebuggerStepThrough]
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	public class getDeepestCategoriesCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x0600061A RID: 1562 RVA: 0x00008FDE File Offset: 0x000071DE
		internal getDeepestCategoriesCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x17000204 RID: 516
		// (get) Token: 0x0600061B RID: 1563 RVA: 0x00008FF1 File Offset: 0x000071F1
		public categoryServiceResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (categoryServiceResponse)this.results[0];
			}
		}

		// Token: 0x0400024D RID: 589
		private object[] results;
	}
}
