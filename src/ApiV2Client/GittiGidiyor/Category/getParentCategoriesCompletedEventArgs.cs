﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Category
{
	// Token: 0x020000B3 RID: 179
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	public class getParentCategoriesCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x0600062C RID: 1580 RVA: 0x00009056 File Offset: 0x00007256
		internal getParentCategoriesCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x17000207 RID: 519
		// (get) Token: 0x0600062D RID: 1581 RVA: 0x00009069 File Offset: 0x00007269
		public categoryServiceResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (categoryServiceResponse)this.results[0];
			}
		}

		// Token: 0x04000250 RID: 592
		private object[] results;
	}
}
