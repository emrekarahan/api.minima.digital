﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Category
{
	// Token: 0x020000B1 RID: 177
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	[DesignerCategory("code")]
	[DebuggerStepThrough]
	public class getModifiedCategoriesCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000626 RID: 1574 RVA: 0x0000902E File Offset: 0x0000722E
		internal getModifiedCategoriesCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x17000206 RID: 518
		// (get) Token: 0x06000627 RID: 1575 RVA: 0x00009041 File Offset: 0x00007241
		public categoryServiceAuditResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (categoryServiceAuditResponse)this.results[0];
			}
		}

		// Token: 0x0400024F RID: 591
		private object[] results;
	}
}
