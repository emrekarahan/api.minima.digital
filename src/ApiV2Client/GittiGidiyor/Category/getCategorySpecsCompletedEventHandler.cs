﻿using System.CodeDom.Compiler;

namespace ApiV2Client.GittiGidiyor.Category
{
	// Token: 0x020000A6 RID: 166
	// (Invoke) Token: 0x06000605 RID: 1541
	[GeneratedCode("System.Web.Services", "4.7.3190.0")]
	public delegate void getCategorySpecsCompletedEventHandler(object sender, getCategorySpecsCompletedEventArgs e);
}
