﻿using System.CodeDom.Compiler;

namespace ApiV2Client.GittiGidiyor.Message
{
	// Token: 0x020000E4 RID: 228
	// (Invoke) Token: 0x06000789 RID: 1929
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	public delegate void getSendedMessagesCompletedEventHandler(object sender, getSendedMessagesCompletedEventArgs e);
}
