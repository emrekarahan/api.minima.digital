﻿using System.CodeDom.Compiler;

namespace ApiV2Client.GittiGidiyor.Message
{
	// Token: 0x020000E0 RID: 224
	// (Invoke) Token: 0x0600077D RID: 1917
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	public delegate void getInboxMessagesCompletedEventHandler(object sender, getInboxMessagesCompletedEventArgs e);
}
