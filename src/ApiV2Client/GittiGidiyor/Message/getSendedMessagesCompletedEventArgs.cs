﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Message
{
	// Token: 0x020000E5 RID: 229
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	public class getSendedMessagesCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x0600078C RID: 1932 RVA: 0x0000B13B File Offset: 0x0000933B
		internal getSendedMessagesCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x17000258 RID: 600
		// (get) Token: 0x0600078D RID: 1933 RVA: 0x0000B14E File Offset: 0x0000934E
		public userMessageServiceResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (userMessageServiceResponse)this.results[0];
			}
		}

		// Token: 0x040002C0 RID: 704
		private object[] results;
	}
}
