﻿namespace ApiV2Client.GittiGidiyor.Message
{
	// Token: 0x020001E7 RID: 487
	public interface IMessageService : IService
	{
		// Token: 0x0600108C RID: 4236
		userMessageServiceResponse getInboxMessages(bool unread, int startOffset, int rowCount, string lang);

		// Token: 0x0600108D RID: 4237
		userMessageServiceResponse getSendedMessages(int startOffset, int rowCount, string lang);

		// Token: 0x0600108E RID: 4238
		userMessageServiceIdResponse sendNewMessage(string to, string title, string message, bool sendCopy, string lang);
	}
}
