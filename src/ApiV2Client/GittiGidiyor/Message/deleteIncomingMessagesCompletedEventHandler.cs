﻿using System.CodeDom.Compiler;

namespace ApiV2Client.GittiGidiyor.Message
{
	// Token: 0x020000DC RID: 220
	// (Invoke) Token: 0x06000771 RID: 1905
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	public delegate void deleteIncomingMessagesCompletedEventHandler(object sender, deleteIncomingMessagesCompletedEventArgs e);
}
