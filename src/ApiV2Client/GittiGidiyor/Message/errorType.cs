﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Message
{
	// Token: 0x020000D9 RID: 217
	[DebuggerStepThrough]
	[XmlType(Namespace = "http://message.individual.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[DesignerCategory("code")]
	[Serializable]
	public class errorType
	{
		// Token: 0x17000242 RID: 578
		// (get) Token: 0x06000749 RID: 1865 RVA: 0x0000AF51 File Offset: 0x00009151
		// (set) Token: 0x0600074A RID: 1866 RVA: 0x0000AF59 File Offset: 0x00009159
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string errorId
		{
			get
			{
				return this.errorIdField;
			}
			set
			{
				this.errorIdField = value;
			}
		}

		// Token: 0x17000243 RID: 579
		// (get) Token: 0x0600074B RID: 1867 RVA: 0x0000AF62 File Offset: 0x00009162
		// (set) Token: 0x0600074C RID: 1868 RVA: 0x0000AF6A File Offset: 0x0000916A
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string errorCode
		{
			get
			{
				return this.errorCodeField;
			}
			set
			{
				this.errorCodeField = value;
			}
		}

		// Token: 0x17000244 RID: 580
		// (get) Token: 0x0600074D RID: 1869 RVA: 0x0000AF73 File Offset: 0x00009173
		// (set) Token: 0x0600074E RID: 1870 RVA: 0x0000AF7B File Offset: 0x0000917B
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string message
		{
			get
			{
				return this.messageField;
			}
			set
			{
				this.messageField = value;
			}
		}

		// Token: 0x17000245 RID: 581
		// (get) Token: 0x0600074F RID: 1871 RVA: 0x0000AF84 File Offset: 0x00009184
		// (set) Token: 0x06000750 RID: 1872 RVA: 0x0000AF8C File Offset: 0x0000918C
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string viewMessage
		{
			get
			{
				return this.viewMessageField;
			}
			set
			{
				this.viewMessageField = value;
			}
		}

		// Token: 0x040002AA RID: 682
		private string errorIdField;

		// Token: 0x040002AB RID: 683
		private string errorCodeField;

		// Token: 0x040002AC RID: 684
		private string messageField;

		// Token: 0x040002AD RID: 685
		private string viewMessageField;
	}
}
