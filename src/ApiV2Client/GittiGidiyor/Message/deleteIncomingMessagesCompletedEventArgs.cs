﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Message
{
	// Token: 0x020000DD RID: 221
	[DesignerCategory("code")]
	[DebuggerStepThrough]
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	public class deleteIncomingMessagesCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000774 RID: 1908 RVA: 0x0000B09B File Offset: 0x0000929B
		internal deleteIncomingMessagesCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x17000254 RID: 596
		// (get) Token: 0x06000775 RID: 1909 RVA: 0x0000B0AE File Offset: 0x000092AE
		public userMessageServiceIdResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (userMessageServiceIdResponse)this.results[0];
			}
		}

		// Token: 0x040002BC RID: 700
		private object[] results;
	}
}
