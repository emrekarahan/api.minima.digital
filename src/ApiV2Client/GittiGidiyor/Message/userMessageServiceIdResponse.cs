﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Message
{
	// Token: 0x020000D8 RID: 216
	[DebuggerStepThrough]
	[XmlType(Namespace = "http://message.individual.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[DesignerCategory("code")]
	[Serializable]
	public class userMessageServiceIdResponse : baseResponse
	{
		// Token: 0x1700023E RID: 574
		// (get) Token: 0x06000740 RID: 1856 RVA: 0x0000AF05 File Offset: 0x00009105
		// (set) Token: 0x06000741 RID: 1857 RVA: 0x0000AF0D File Offset: 0x0000910D
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int messageCount
		{
			get
			{
				return this.messageCountField;
			}
			set
			{
				this.messageCountField = value;
			}
		}

		// Token: 0x1700023F RID: 575
		// (get) Token: 0x06000742 RID: 1858 RVA: 0x0000AF16 File Offset: 0x00009116
		// (set) Token: 0x06000743 RID: 1859 RVA: 0x0000AF1E File Offset: 0x0000911E
		[XmlIgnore]
		public bool messageCountSpecified
		{
			get
			{
				return this.messageCountFieldSpecified;
			}
			set
			{
				this.messageCountFieldSpecified = value;
			}
		}

		// Token: 0x17000240 RID: 576
		// (get) Token: 0x06000744 RID: 1860 RVA: 0x0000AF27 File Offset: 0x00009127
		// (set) Token: 0x06000745 RID: 1861 RVA: 0x0000AF2F File Offset: 0x0000912F
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		[XmlArrayItem("messageId", Form = XmlSchemaForm.Unqualified)]
		public int?[] messageIdList
		{
			get
			{
				return this.messageIdListField;
			}
			set
			{
				this.messageIdListField = value;
			}
		}

		// Token: 0x17000241 RID: 577
		// (get) Token: 0x06000746 RID: 1862 RVA: 0x0000AF38 File Offset: 0x00009138
		// (set) Token: 0x06000747 RID: 1863 RVA: 0x0000AF40 File Offset: 0x00009140
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string result
		{
			get
			{
				return this.resultField;
			}
			set
			{
				this.resultField = value;
			}
		}

		// Token: 0x040002A6 RID: 678
		private int messageCountField;

		// Token: 0x040002A7 RID: 679
		private bool messageCountFieldSpecified;

		// Token: 0x040002A8 RID: 680
		private int?[] messageIdListField;

		// Token: 0x040002A9 RID: 681
		private string resultField;
	}
}
