﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Message
{
	// Token: 0x020000D7 RID: 215
	[XmlInclude(typeof(userMessageServiceIdResponse))]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[XmlInclude(typeof(userMessageServiceResponse))]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[XmlType(Namespace = "http://message.individual.ws.listingapi.gg.com")]
	[Serializable]
	public class baseResponse
	{
		// Token: 0x1700023A RID: 570
		// (get) Token: 0x06000737 RID: 1847 RVA: 0x0000AEB9 File Offset: 0x000090B9
		// (set) Token: 0x06000738 RID: 1848 RVA: 0x0000AEC1 File Offset: 0x000090C1
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string ackCode
		{
			get
			{
				return this.ackCodeField;
			}
			set
			{
				this.ackCodeField = value;
			}
		}

		// Token: 0x1700023B RID: 571
		// (get) Token: 0x06000739 RID: 1849 RVA: 0x0000AECA File Offset: 0x000090CA
		// (set) Token: 0x0600073A RID: 1850 RVA: 0x0000AED2 File Offset: 0x000090D2
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string responseTime
		{
			get
			{
				return this.responseTimeField;
			}
			set
			{
				this.responseTimeField = value;
			}
		}

		// Token: 0x1700023C RID: 572
		// (get) Token: 0x0600073B RID: 1851 RVA: 0x0000AEDB File Offset: 0x000090DB
		// (set) Token: 0x0600073C RID: 1852 RVA: 0x0000AEE3 File Offset: 0x000090E3
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public errorType error
		{
			get
			{
				return this.errorField;
			}
			set
			{
				this.errorField = value;
			}
		}

		// Token: 0x1700023D RID: 573
		// (get) Token: 0x0600073D RID: 1853 RVA: 0x0000AEEC File Offset: 0x000090EC
		// (set) Token: 0x0600073E RID: 1854 RVA: 0x0000AEF4 File Offset: 0x000090F4
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string timeElapsed
		{
			get
			{
				return this.timeElapsedField;
			}
			set
			{
				this.timeElapsedField = value;
			}
		}

		// Token: 0x040002A2 RID: 674
		private string ackCodeField;

		// Token: 0x040002A3 RID: 675
		private string responseTimeField;

		// Token: 0x040002A4 RID: 676
		private errorType errorField;

		// Token: 0x040002A5 RID: 677
		private string timeElapsedField;
	}
}
