﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Message
{
	// Token: 0x020000E3 RID: 227
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	[DesignerCategory("code")]
	[DebuggerStepThrough]
	public class getMessagesByUserCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000786 RID: 1926 RVA: 0x0000B113 File Offset: 0x00009313
		internal getMessagesByUserCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x17000257 RID: 599
		// (get) Token: 0x06000787 RID: 1927 RVA: 0x0000B126 File Offset: 0x00009326
		public userMessageServiceResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (userMessageServiceResponse)this.results[0];
			}
		}

		// Token: 0x040002BF RID: 703
		private object[] results;
	}
}
