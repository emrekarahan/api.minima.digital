﻿using System.CodeDom.Compiler;

namespace ApiV2Client.GittiGidiyor.Message
{
	// Token: 0x020000DE RID: 222
	// (Invoke) Token: 0x06000777 RID: 1911
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	public delegate void deleteOutgoingMessagesCompletedEventHandler(object sender, deleteOutgoingMessagesCompletedEventArgs e);
}
