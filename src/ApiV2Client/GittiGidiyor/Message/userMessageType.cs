﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Message
{
	// Token: 0x020000DA RID: 218
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[XmlType(Namespace = "http://message.individual.ws.listingapi.gg.com")]
	[Serializable]
	public class userMessageType
	{
		// Token: 0x17000246 RID: 582
		// (get) Token: 0x06000752 RID: 1874 RVA: 0x0000AF9D File Offset: 0x0000919D
		// (set) Token: 0x06000753 RID: 1875 RVA: 0x0000AFA5 File Offset: 0x000091A5
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int messageId
		{
			get
			{
				return this.messageIdField;
			}
			set
			{
				this.messageIdField = value;
			}
		}

		// Token: 0x17000247 RID: 583
		// (get) Token: 0x06000754 RID: 1876 RVA: 0x0000AFAE File Offset: 0x000091AE
		// (set) Token: 0x06000755 RID: 1877 RVA: 0x0000AFB6 File Offset: 0x000091B6
		[XmlIgnore]
		public bool messageIdSpecified
		{
			get
			{
				return this.messageIdFieldSpecified;
			}
			set
			{
				this.messageIdFieldSpecified = value;
			}
		}

		// Token: 0x17000248 RID: 584
		// (get) Token: 0x06000756 RID: 1878 RVA: 0x0000AFBF File Offset: 0x000091BF
		// (set) Token: 0x06000757 RID: 1879 RVA: 0x0000AFC7 File Offset: 0x000091C7
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string from
		{
			get
			{
				return this.fromField;
			}
			set
			{
				this.fromField = value;
			}
		}

		// Token: 0x17000249 RID: 585
		// (get) Token: 0x06000758 RID: 1880 RVA: 0x0000AFD0 File Offset: 0x000091D0
		// (set) Token: 0x06000759 RID: 1881 RVA: 0x0000AFD8 File Offset: 0x000091D8
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string to
		{
			get
			{
				return this.toField;
			}
			set
			{
				this.toField = value;
			}
		}

		// Token: 0x1700024A RID: 586
		// (get) Token: 0x0600075A RID: 1882 RVA: 0x0000AFE1 File Offset: 0x000091E1
		// (set) Token: 0x0600075B RID: 1883 RVA: 0x0000AFE9 File Offset: 0x000091E9
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string title
		{
			get
			{
				return this.titleField;
			}
			set
			{
				this.titleField = value;
			}
		}

		// Token: 0x1700024B RID: 587
		// (get) Token: 0x0600075C RID: 1884 RVA: 0x0000AFF2 File Offset: 0x000091F2
		// (set) Token: 0x0600075D RID: 1885 RVA: 0x0000AFFA File Offset: 0x000091FA
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string messageContent
		{
			get
			{
				return this.messageContentField;
			}
			set
			{
				this.messageContentField = value;
			}
		}

		// Token: 0x1700024C RID: 588
		// (get) Token: 0x0600075E RID: 1886 RVA: 0x0000B003 File Offset: 0x00009203
		// (set) Token: 0x0600075F RID: 1887 RVA: 0x0000B00B File Offset: 0x0000920B
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string time
		{
			get
			{
				return this.timeField;
			}
			set
			{
				this.timeField = value;
			}
		}

		// Token: 0x1700024D RID: 589
		// (get) Token: 0x06000760 RID: 1888 RVA: 0x0000B014 File Offset: 0x00009214
		// (set) Token: 0x06000761 RID: 1889 RVA: 0x0000B01C File Offset: 0x0000921C
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string deleteTime
		{
			get
			{
				return this.deleteTimeField;
			}
			set
			{
				this.deleteTimeField = value;
			}
		}

		// Token: 0x1700024E RID: 590
		// (get) Token: 0x06000762 RID: 1890 RVA: 0x0000B025 File Offset: 0x00009225
		// (set) Token: 0x06000763 RID: 1891 RVA: 0x0000B02D File Offset: 0x0000922D
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public bool isRead
		{
			get
			{
				return this.isReadField;
			}
			set
			{
				this.isReadField = value;
			}
		}

		// Token: 0x1700024F RID: 591
		// (get) Token: 0x06000764 RID: 1892 RVA: 0x0000B036 File Offset: 0x00009236
		// (set) Token: 0x06000765 RID: 1893 RVA: 0x0000B03E File Offset: 0x0000923E
		[XmlIgnore]
		public bool isReadSpecified
		{
			get
			{
				return this.isReadFieldSpecified;
			}
			set
			{
				this.isReadFieldSpecified = value;
			}
		}

		// Token: 0x17000250 RID: 592
		// (get) Token: 0x06000766 RID: 1894 RVA: 0x0000B047 File Offset: 0x00009247
		// (set) Token: 0x06000767 RID: 1895 RVA: 0x0000B04F File Offset: 0x0000924F
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public bool deleted
		{
			get
			{
				return this.deletedField;
			}
			set
			{
				this.deletedField = value;
			}
		}

		// Token: 0x040002AE RID: 686
		private int messageIdField;

		// Token: 0x040002AF RID: 687
		private bool messageIdFieldSpecified;

		// Token: 0x040002B0 RID: 688
		private string fromField;

		// Token: 0x040002B1 RID: 689
		private string toField;

		// Token: 0x040002B2 RID: 690
		private string titleField;

		// Token: 0x040002B3 RID: 691
		private string messageContentField;

		// Token: 0x040002B4 RID: 692
		private string timeField;

		// Token: 0x040002B5 RID: 693
		private string deleteTimeField;

		// Token: 0x040002B6 RID: 694
		private bool isReadField;

		// Token: 0x040002B7 RID: 695
		private bool isReadFieldSpecified;

		// Token: 0x040002B8 RID: 696
		private bool deletedField;
	}
}
