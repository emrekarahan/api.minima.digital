﻿using System.CodeDom.Compiler;

namespace ApiV2Client.GittiGidiyor.Message
{
	// Token: 0x020000E8 RID: 232
	// (Invoke) Token: 0x06000795 RID: 1941
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	public delegate void readMessageCompletedEventHandler(object sender, readMessageCompletedEventArgs e);
}
