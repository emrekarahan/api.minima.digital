﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Message
{
	// Token: 0x020000EB RID: 235
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	public class sendNewMessageCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x0600079E RID: 1950 RVA: 0x0000B1B3 File Offset: 0x000093B3
		internal sendNewMessageCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x1700025B RID: 603
		// (get) Token: 0x0600079F RID: 1951 RVA: 0x0000B1C6 File Offset: 0x000093C6
		public userMessageServiceIdResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (userMessageServiceIdResponse)this.results[0];
			}
		}

		// Token: 0x040002C3 RID: 707
		private object[] results;
	}
}
