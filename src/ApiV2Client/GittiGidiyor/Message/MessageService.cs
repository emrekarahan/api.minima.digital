﻿using System;

namespace ApiV2Client.GittiGidiyor.Message
{
	// Token: 0x020001E8 RID: 488
	public class MessageService : ServiceClient<IndividualUserMessageServiceService>, IMessageService, IService
	{
		// Token: 0x0600108F RID: 4239 RVA: 0x00016078 File Offset: 0x00014278
		private MessageService()
		{
			base.configureNetworkCredentials(this.service);
		}

		// Token: 0x06001090 RID: 4240 RVA: 0x00016098 File Offset: 0x00014298
		public userMessageServiceResponse getInboxMessages(bool unread, int startOffset, int rowCount, string lang)
		{
			long time = Util.convertToTimestamp(DateTime.Now);
			return this.service.getInboxMessages(this.authConfig.ApiKey, base.getSignature(time), time, startOffset, rowCount, unread, lang);
		}

		// Token: 0x06001091 RID: 4241 RVA: 0x000160D4 File Offset: 0x000142D4
		public userMessageServiceResponse getSendedMessages(int startOffset, int rowCount, string lang)
		{
			long time = Util.convertToTimestamp(DateTime.Now);
			return this.service.getSendedMessages(this.authConfig.ApiKey, base.getSignature(time), time, startOffset, rowCount, lang);
		}

		// Token: 0x06001092 RID: 4242 RVA: 0x0001610D File Offset: 0x0001430D
		public string getServiceName()
		{
			return this.service.getServiceName();
		}

		// Token: 0x06001093 RID: 4243 RVA: 0x0001611C File Offset: 0x0001431C
		public userMessageServiceIdResponse sendNewMessage(string to, string title, string message, bool sendCopy, string lang)
		{
			long time = Util.convertToTimestamp(DateTime.Now);
			return this.service.sendNewMessage(this.authConfig.ApiKey, base.getSignature(time), time, to, title, message, sendCopy, lang);
		}

		// Token: 0x17000518 RID: 1304
		// (get) Token: 0x06001094 RID: 4244 RVA: 0x0001615C File Offset: 0x0001435C
		public static MessageService Instance
		{
			get
			{
				if (MessageService.instance == null)
				{
					lock (MessageService.lockObject)
					{
						if (MessageService.instance == null)
						{
							MessageService.instance = new MessageService();
						}
					}
				}
				return MessageService.instance;
			}
		}

		// Token: 0x04000619 RID: 1561
		private static MessageService instance;

		// Token: 0x0400061A RID: 1562
		private static object lockObject = new object();

		// Token: 0x0400061B RID: 1563
		private IndividualUserMessageServiceService service = new IndividualUserMessageServiceService();
	}
}
