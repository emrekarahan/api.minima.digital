﻿using System.CodeDom.Compiler;

namespace ApiV2Client.GittiGidiyor.Message
{
	// Token: 0x020000EA RID: 234
	// (Invoke) Token: 0x0600079B RID: 1947
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	public delegate void sendNewMessageCompletedEventHandler(object sender, sendNewMessageCompletedEventArgs e);
}
