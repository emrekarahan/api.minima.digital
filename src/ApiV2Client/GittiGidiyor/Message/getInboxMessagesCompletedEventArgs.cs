﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Message
{
	// Token: 0x020000E1 RID: 225
	[DebuggerStepThrough]
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	[DesignerCategory("code")]
	public class getInboxMessagesCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000780 RID: 1920 RVA: 0x0000B0EB File Offset: 0x000092EB
		internal getInboxMessagesCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x17000256 RID: 598
		// (get) Token: 0x06000781 RID: 1921 RVA: 0x0000B0FE File Offset: 0x000092FE
		public userMessageServiceResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (userMessageServiceResponse)this.results[0];
			}
		}

		// Token: 0x040002BE RID: 702
		private object[] results;
	}
}
