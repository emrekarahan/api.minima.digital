﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Message
{
	// Token: 0x020000DB RID: 219
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[DesignerCategory("code")]
	[DebuggerStepThrough]
	[XmlType(Namespace = "http://message.individual.ws.listingapi.gg.com")]
	[Serializable]
	public class userMessageServiceResponse : baseResponse
	{
		// Token: 0x17000251 RID: 593
		// (get) Token: 0x06000769 RID: 1897 RVA: 0x0000B060 File Offset: 0x00009260
		// (set) Token: 0x0600076A RID: 1898 RVA: 0x0000B068 File Offset: 0x00009268
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int messageCount
		{
			get
			{
				return this.messageCountField;
			}
			set
			{
				this.messageCountField = value;
			}
		}

		// Token: 0x17000252 RID: 594
		// (get) Token: 0x0600076B RID: 1899 RVA: 0x0000B071 File Offset: 0x00009271
		// (set) Token: 0x0600076C RID: 1900 RVA: 0x0000B079 File Offset: 0x00009279
		[XmlIgnore]
		public bool messageCountSpecified
		{
			get
			{
				return this.messageCountFieldSpecified;
			}
			set
			{
				this.messageCountFieldSpecified = value;
			}
		}

		// Token: 0x17000253 RID: 595
		// (get) Token: 0x0600076D RID: 1901 RVA: 0x0000B082 File Offset: 0x00009282
		// (set) Token: 0x0600076E RID: 1902 RVA: 0x0000B08A File Offset: 0x0000928A
		[XmlArrayItem("message", Form = XmlSchemaForm.Unqualified)]
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		public userMessageType[] messages
		{
			get
			{
				return this.messagesField;
			}
			set
			{
				this.messagesField = value;
			}
		}

		// Token: 0x040002B9 RID: 697
		private int messageCountField;

		// Token: 0x040002BA RID: 698
		private bool messageCountFieldSpecified;

		// Token: 0x040002BB RID: 699
		private userMessageType[] messagesField;
	}
}
