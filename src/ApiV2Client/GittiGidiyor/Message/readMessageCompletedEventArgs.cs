﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Message
{
	// Token: 0x020000E9 RID: 233
	[DesignerCategory("code")]
	[DebuggerStepThrough]
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	public class readMessageCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000798 RID: 1944 RVA: 0x0000B18B File Offset: 0x0000938B
		internal readMessageCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x1700025A RID: 602
		// (get) Token: 0x06000799 RID: 1945 RVA: 0x0000B19E File Offset: 0x0000939E
		public userMessageServiceResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (userMessageServiceResponse)this.results[0];
			}
		}

		// Token: 0x040002C2 RID: 706
		private object[] results;
	}
}
