﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Message
{
	// Token: 0x020000DF RID: 223
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	public class deleteOutgoingMessagesCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x0600077A RID: 1914 RVA: 0x0000B0C3 File Offset: 0x000092C3
		internal deleteOutgoingMessagesCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x17000255 RID: 597
		// (get) Token: 0x0600077B RID: 1915 RVA: 0x0000B0D6 File Offset: 0x000092D6
		public userMessageServiceIdResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (userMessageServiceIdResponse)this.results[0];
			}
		}

		// Token: 0x040002BD RID: 701
		private object[] results;
	}
}
