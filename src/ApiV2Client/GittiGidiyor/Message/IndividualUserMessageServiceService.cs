﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading;
using System.Web.Services;
using System.Web.Services.Description;
using System.Web.Services.Protocols;
using System.Xml.Schema;
using System.Xml.Serialization;
using ApiV2Client.Properties;

namespace ApiV2Client.GittiGidiyor.Message
{
	// Token: 0x020000D6 RID: 214
	[DebuggerStepThrough]
	[XmlInclude(typeof(baseResponse))]
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	[DesignerCategory("code")]
	[WebServiceBinding(Name = "IndividualMessageServiceBinding", Namespace = "http://message.individual.ws.listingapi.gg.com")]
	public class IndividualUserMessageServiceService : SoapHttpClientProtocol
	{
		// Token: 0x06000700 RID: 1792 RVA: 0x0000A1D8 File Offset: 0x000083D8
		public IndividualUserMessageServiceService()
		{
			this.Url = Settings.Default.ApiV2Client_GittiGidiyor_Message_IndividualUserMessageServiceService;
			if (this.IsLocalFileSystemWebService(this.Url))
			{
				this.UseDefaultCredentials = true;
				this.useDefaultCredentialsSetExplicitly = false;
				return;
			}
			this.useDefaultCredentialsSetExplicitly = true;
		}

		// Token: 0x17000238 RID: 568
		// (get) Token: 0x06000701 RID: 1793 RVA: 0x0000A214 File Offset: 0x00008414
		// (set) Token: 0x06000702 RID: 1794 RVA: 0x0000A21C File Offset: 0x0000841C
		public new string Url
		{
			get
			{
				return base.Url;
			}
			set
			{
				if (this.IsLocalFileSystemWebService(base.Url) && !this.useDefaultCredentialsSetExplicitly && !this.IsLocalFileSystemWebService(value))
				{
					base.UseDefaultCredentials = false;
				}
				base.Url = value;
			}
		}

		// Token: 0x17000239 RID: 569
		// (get) Token: 0x06000703 RID: 1795 RVA: 0x0000A24B File Offset: 0x0000844B
		// (set) Token: 0x06000704 RID: 1796 RVA: 0x0000A253 File Offset: 0x00008453
		public new bool UseDefaultCredentials
		{
			get
			{
				return base.UseDefaultCredentials;
			}
			set
			{
				base.UseDefaultCredentials = value;
				this.useDefaultCredentialsSetExplicitly = true;
			}
		}

		// Token: 0x14000037 RID: 55
		// (add) Token: 0x06000705 RID: 1797 RVA: 0x0000A264 File Offset: 0x00008464
		// (remove) Token: 0x06000706 RID: 1798 RVA: 0x0000A29C File Offset: 0x0000849C
		public event deleteIncomingMessagesCompletedEventHandler deleteIncomingMessagesCompleted;

		// Token: 0x14000038 RID: 56
		// (add) Token: 0x06000707 RID: 1799 RVA: 0x0000A2D4 File Offset: 0x000084D4
		// (remove) Token: 0x06000708 RID: 1800 RVA: 0x0000A30C File Offset: 0x0000850C
		public event deleteOutgoingMessagesCompletedEventHandler deleteOutgoingMessagesCompleted;

		// Token: 0x14000039 RID: 57
		// (add) Token: 0x06000709 RID: 1801 RVA: 0x0000A344 File Offset: 0x00008544
		// (remove) Token: 0x0600070A RID: 1802 RVA: 0x0000A37C File Offset: 0x0000857C
		public event getInboxMessagesCompletedEventHandler getInboxMessagesCompleted;

		// Token: 0x1400003A RID: 58
		// (add) Token: 0x0600070B RID: 1803 RVA: 0x0000A3B4 File Offset: 0x000085B4
		// (remove) Token: 0x0600070C RID: 1804 RVA: 0x0000A3EC File Offset: 0x000085EC
		public event getMessagesByUserCompletedEventHandler getMessagesByUserCompleted;

		// Token: 0x1400003B RID: 59
		// (add) Token: 0x0600070D RID: 1805 RVA: 0x0000A424 File Offset: 0x00008624
		// (remove) Token: 0x0600070E RID: 1806 RVA: 0x0000A45C File Offset: 0x0000865C
		public event getSendedMessagesCompletedEventHandler getSendedMessagesCompleted;

		// Token: 0x1400003C RID: 60
		// (add) Token: 0x0600070F RID: 1807 RVA: 0x0000A494 File Offset: 0x00008694
		// (remove) Token: 0x06000710 RID: 1808 RVA: 0x0000A4CC File Offset: 0x000086CC
		public event getServiceNameCompletedEventHandler getServiceNameCompleted;

		// Token: 0x1400003D RID: 61
		// (add) Token: 0x06000711 RID: 1809 RVA: 0x0000A504 File Offset: 0x00008704
		// (remove) Token: 0x06000712 RID: 1810 RVA: 0x0000A53C File Offset: 0x0000873C
		public event readMessageCompletedEventHandler readMessageCompleted;

		// Token: 0x1400003E RID: 62
		// (add) Token: 0x06000713 RID: 1811 RVA: 0x0000A574 File Offset: 0x00008774
		// (remove) Token: 0x06000714 RID: 1812 RVA: 0x0000A5AC File Offset: 0x000087AC
		public event sendNewMessageCompletedEventHandler sendNewMessageCompleted;

		// Token: 0x06000715 RID: 1813 RVA: 0x0000A5E4 File Offset: 0x000087E4
		[SoapRpcMethod("", RequestNamespace = "http://message.individual.ws.listingapi.gg.com", ResponseNamespace = "http://message.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public userMessageServiceIdResponse deleteIncomingMessages(string apiKey, string sign, long time, [XmlArrayItem("item", Form = XmlSchemaForm.Unqualified)] int?[] messageId, string lang)
		{
			object[] array = base.Invoke("deleteIncomingMessages", new object[]
			{
				apiKey,
				sign,
				time,
				messageId,
				lang
			});
			return (userMessageServiceIdResponse)array[0];
		}

		// Token: 0x06000716 RID: 1814 RVA: 0x0000A628 File Offset: 0x00008828
		public void deleteIncomingMessagesAsync(string apiKey, string sign, long time, int?[] messageId, string lang)
		{
			this.deleteIncomingMessagesAsync(apiKey, sign, time, messageId, lang, null);
		}

		// Token: 0x06000717 RID: 1815 RVA: 0x0000A638 File Offset: 0x00008838
		public void deleteIncomingMessagesAsync(string apiKey, string sign, long time, int?[] messageId, string lang, object userState)
		{
			if (this.deleteIncomingMessagesOperationCompleted == null)
			{
				this.deleteIncomingMessagesOperationCompleted = new SendOrPostCallback(this.OndeleteIncomingMessagesOperationCompleted);
			}
			base.InvokeAsync("deleteIncomingMessages", new object[]
			{
				apiKey,
				sign,
				time,
				messageId,
				lang
			}, this.deleteIncomingMessagesOperationCompleted, userState);
		}

		// Token: 0x06000718 RID: 1816 RVA: 0x0000A698 File Offset: 0x00008898
		private void OndeleteIncomingMessagesOperationCompleted(object arg)
		{
			if (this.deleteIncomingMessagesCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.deleteIncomingMessagesCompleted(this, new deleteIncomingMessagesCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x06000719 RID: 1817 RVA: 0x0000A6E0 File Offset: 0x000088E0
		[SoapRpcMethod("", RequestNamespace = "http://message.individual.ws.listingapi.gg.com", ResponseNamespace = "http://message.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public userMessageServiceIdResponse deleteOutgoingMessages(string apiKey, string sign, long time, [XmlArrayItem("item", Form = XmlSchemaForm.Unqualified)] int?[] messageId, string lang)
		{
			object[] array = base.Invoke("deleteOutgoingMessages", new object[]
			{
				apiKey,
				sign,
				time,
				messageId,
				lang
			});
			return (userMessageServiceIdResponse)array[0];
		}

		// Token: 0x0600071A RID: 1818 RVA: 0x0000A724 File Offset: 0x00008924
		public void deleteOutgoingMessagesAsync(string apiKey, string sign, long time, int?[] messageId, string lang)
		{
			this.deleteOutgoingMessagesAsync(apiKey, sign, time, messageId, lang, null);
		}

		// Token: 0x0600071B RID: 1819 RVA: 0x0000A734 File Offset: 0x00008934
		public void deleteOutgoingMessagesAsync(string apiKey, string sign, long time, int?[] messageId, string lang, object userState)
		{
			if (this.deleteOutgoingMessagesOperationCompleted == null)
			{
				this.deleteOutgoingMessagesOperationCompleted = new SendOrPostCallback(this.OndeleteOutgoingMessagesOperationCompleted);
			}
			base.InvokeAsync("deleteOutgoingMessages", new object[]
			{
				apiKey,
				sign,
				time,
				messageId,
				lang
			}, this.deleteOutgoingMessagesOperationCompleted, userState);
		}

		// Token: 0x0600071C RID: 1820 RVA: 0x0000A794 File Offset: 0x00008994
		private void OndeleteOutgoingMessagesOperationCompleted(object arg)
		{
			if (this.deleteOutgoingMessagesCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.deleteOutgoingMessagesCompleted(this, new deleteOutgoingMessagesCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x0600071D RID: 1821 RVA: 0x0000A7DC File Offset: 0x000089DC
		[SoapRpcMethod("", RequestNamespace = "http://message.individual.ws.listingapi.gg.com", ResponseNamespace = "http://message.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public userMessageServiceResponse getInboxMessages(string apiKey, string sign, long time, int startOffSet, int rowCount, bool unread, string lang)
		{
			object[] array = base.Invoke("getInboxMessages", new object[]
			{
				apiKey,
				sign,
				time,
				startOffSet,
				rowCount,
				unread,
				lang
			});
			return (userMessageServiceResponse)array[0];
		}

		// Token: 0x0600071E RID: 1822 RVA: 0x0000A83C File Offset: 0x00008A3C
		public void getInboxMessagesAsync(string apiKey, string sign, long time, int startOffSet, int rowCount, bool unread, string lang)
		{
			this.getInboxMessagesAsync(apiKey, sign, time, startOffSet, rowCount, unread, lang, null);
		}

		// Token: 0x0600071F RID: 1823 RVA: 0x0000A85C File Offset: 0x00008A5C
		public void getInboxMessagesAsync(string apiKey, string sign, long time, int startOffSet, int rowCount, bool unread, string lang, object userState)
		{
			if (this.getInboxMessagesOperationCompleted == null)
			{
				this.getInboxMessagesOperationCompleted = new SendOrPostCallback(this.OngetInboxMessagesOperationCompleted);
			}
			base.InvokeAsync("getInboxMessages", new object[]
			{
				apiKey,
				sign,
				time,
				startOffSet,
				rowCount,
				unread,
				lang
			}, this.getInboxMessagesOperationCompleted, userState);
		}

		// Token: 0x06000720 RID: 1824 RVA: 0x0000A8D4 File Offset: 0x00008AD4
		private void OngetInboxMessagesOperationCompleted(object arg)
		{
			if (this.getInboxMessagesCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getInboxMessagesCompleted(this, new getInboxMessagesCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x06000721 RID: 1825 RVA: 0x0000A91C File Offset: 0x00008B1C
		[SoapRpcMethod("", RequestNamespace = "http://message.individual.ws.listingapi.gg.com", ResponseNamespace = "http://message.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public userMessageServiceResponse getMessagesByUser(string apiKey, string sign, long time, string byUser, string messageType, int startOffSet, int rowCount, string lang)
		{
			object[] array = base.Invoke("getMessagesByUser", new object[]
			{
				apiKey,
				sign,
				time,
				byUser,
				messageType,
				startOffSet,
				rowCount,
				lang
			});
			return (userMessageServiceResponse)array[0];
		}

		// Token: 0x06000722 RID: 1826 RVA: 0x0000A97C File Offset: 0x00008B7C
		public void getMessagesByUserAsync(string apiKey, string sign, long time, string byUser, string messageType, int startOffSet, int rowCount, string lang)
		{
			this.getMessagesByUserAsync(apiKey, sign, time, byUser, messageType, startOffSet, rowCount, lang, null);
		}

		// Token: 0x06000723 RID: 1827 RVA: 0x0000A9A0 File Offset: 0x00008BA0
		public void getMessagesByUserAsync(string apiKey, string sign, long time, string byUser, string messageType, int startOffSet, int rowCount, string lang, object userState)
		{
			if (this.getMessagesByUserOperationCompleted == null)
			{
				this.getMessagesByUserOperationCompleted = new SendOrPostCallback(this.OngetMessagesByUserOperationCompleted);
			}
			base.InvokeAsync("getMessagesByUser", new object[]
			{
				apiKey,
				sign,
				time,
				byUser,
				messageType,
				startOffSet,
				rowCount,
				lang
			}, this.getMessagesByUserOperationCompleted, userState);
		}

		// Token: 0x06000724 RID: 1828 RVA: 0x0000AA18 File Offset: 0x00008C18
		private void OngetMessagesByUserOperationCompleted(object arg)
		{
			if (this.getMessagesByUserCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getMessagesByUserCompleted(this, new getMessagesByUserCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x06000725 RID: 1829 RVA: 0x0000AA60 File Offset: 0x00008C60
		[SoapRpcMethod("", RequestNamespace = "http://message.individual.ws.listingapi.gg.com", ResponseNamespace = "http://message.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public userMessageServiceResponse getSendedMessages(string apiKey, string sign, long time, int startOffSet, int rowCount, string lang)
		{
			object[] array = base.Invoke("getSendedMessages", new object[]
			{
				apiKey,
				sign,
				time,
				startOffSet,
				rowCount,
				lang
			});
			return (userMessageServiceResponse)array[0];
		}

		// Token: 0x06000726 RID: 1830 RVA: 0x0000AAB3 File Offset: 0x00008CB3
		public void getSendedMessagesAsync(string apiKey, string sign, long time, int startOffSet, int rowCount, string lang)
		{
			this.getSendedMessagesAsync(apiKey, sign, time, startOffSet, rowCount, lang, null);
		}

		// Token: 0x06000727 RID: 1831 RVA: 0x0000AAC8 File Offset: 0x00008CC8
		public void getSendedMessagesAsync(string apiKey, string sign, long time, int startOffSet, int rowCount, string lang, object userState)
		{
			if (this.getSendedMessagesOperationCompleted == null)
			{
				this.getSendedMessagesOperationCompleted = new SendOrPostCallback(this.OngetSendedMessagesOperationCompleted);
			}
			base.InvokeAsync("getSendedMessages", new object[]
			{
				apiKey,
				sign,
				time,
				startOffSet,
				rowCount,
				lang
			}, this.getSendedMessagesOperationCompleted, userState);
		}

		// Token: 0x06000728 RID: 1832 RVA: 0x0000AB34 File Offset: 0x00008D34
		private void OngetSendedMessagesOperationCompleted(object arg)
		{
			if (this.getSendedMessagesCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getSendedMessagesCompleted(this, new getSendedMessagesCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x06000729 RID: 1833 RVA: 0x0000AB7C File Offset: 0x00008D7C
		[SoapRpcMethod("", RequestNamespace = "http://message.individual.ws.listingapi.gg.com", ResponseNamespace = "http://message.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public string getServiceName()
		{
			object[] array = base.Invoke("getServiceName", new object[0]);
			return (string)array[0];
		}

		// Token: 0x0600072A RID: 1834 RVA: 0x0000ABA3 File Offset: 0x00008DA3
		public void getServiceNameAsync()
		{
			this.getServiceNameAsync(null);
		}

		// Token: 0x0600072B RID: 1835 RVA: 0x0000ABAC File Offset: 0x00008DAC
		public void getServiceNameAsync(object userState)
		{
			if (this.getServiceNameOperationCompleted == null)
			{
				this.getServiceNameOperationCompleted = new SendOrPostCallback(this.OngetServiceNameOperationCompleted);
			}
			base.InvokeAsync("getServiceName", new object[0], this.getServiceNameOperationCompleted, userState);
		}

		// Token: 0x0600072C RID: 1836 RVA: 0x0000ABE0 File Offset: 0x00008DE0
		private void OngetServiceNameOperationCompleted(object arg)
		{
			if (this.getServiceNameCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getServiceNameCompleted(this, new getServiceNameCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x0600072D RID: 1837 RVA: 0x0000AC28 File Offset: 0x00008E28
		[SoapRpcMethod("", RequestNamespace = "http://message.individual.ws.listingapi.gg.com", ResponseNamespace = "http://message.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public userMessageServiceResponse readMessage(string apiKey, string sign, long time, int messageId, string lang)
		{
			object[] array = base.Invoke("readMessage", new object[]
			{
				apiKey,
				sign,
				time,
				messageId,
				lang
			});
			return (userMessageServiceResponse)array[0];
		}

		// Token: 0x0600072E RID: 1838 RVA: 0x0000AC71 File Offset: 0x00008E71
		public void readMessageAsync(string apiKey, string sign, long time, int messageId, string lang)
		{
			this.readMessageAsync(apiKey, sign, time, messageId, lang, null);
		}

		// Token: 0x0600072F RID: 1839 RVA: 0x0000AC84 File Offset: 0x00008E84
		public void readMessageAsync(string apiKey, string sign, long time, int messageId, string lang, object userState)
		{
			if (this.readMessageOperationCompleted == null)
			{
				this.readMessageOperationCompleted = new SendOrPostCallback(this.OnreadMessageOperationCompleted);
			}
			base.InvokeAsync("readMessage", new object[]
			{
				apiKey,
				sign,
				time,
				messageId,
				lang
			}, this.readMessageOperationCompleted, userState);
		}

		// Token: 0x06000730 RID: 1840 RVA: 0x0000ACE8 File Offset: 0x00008EE8
		private void OnreadMessageOperationCompleted(object arg)
		{
			if (this.readMessageCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.readMessageCompleted(this, new readMessageCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x06000731 RID: 1841 RVA: 0x0000AD30 File Offset: 0x00008F30
		[SoapRpcMethod("", RequestNamespace = "http://message.individual.ws.listingapi.gg.com", ResponseNamespace = "http://message.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public userMessageServiceIdResponse sendNewMessage(string apiKey, string sign, long time, string to, string title, string message, bool sendCopy, string lang)
		{
			object[] array = base.Invoke("sendNewMessage", new object[]
			{
				apiKey,
				sign,
				time,
				to,
				title,
				message,
				sendCopy,
				lang
			});
			return (userMessageServiceIdResponse)array[0];
		}

		// Token: 0x06000732 RID: 1842 RVA: 0x0000AD88 File Offset: 0x00008F88
		public void sendNewMessageAsync(string apiKey, string sign, long time, string to, string title, string message, bool sendCopy, string lang)
		{
			this.sendNewMessageAsync(apiKey, sign, time, to, title, message, sendCopy, lang, null);
		}

		// Token: 0x06000733 RID: 1843 RVA: 0x0000ADAC File Offset: 0x00008FAC
		public void sendNewMessageAsync(string apiKey, string sign, long time, string to, string title, string message, bool sendCopy, string lang, object userState)
		{
			if (this.sendNewMessageOperationCompleted == null)
			{
				this.sendNewMessageOperationCompleted = new SendOrPostCallback(this.OnsendNewMessageOperationCompleted);
			}
			base.InvokeAsync("sendNewMessage", new object[]
			{
				apiKey,
				sign,
				time,
				to,
				title,
				message,
				sendCopy,
				lang
			}, this.sendNewMessageOperationCompleted, userState);
		}

		// Token: 0x06000734 RID: 1844 RVA: 0x0000AE20 File Offset: 0x00009020
		private void OnsendNewMessageOperationCompleted(object arg)
		{
			if (this.sendNewMessageCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.sendNewMessageCompleted(this, new sendNewMessageCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x06000735 RID: 1845 RVA: 0x0000AE65 File Offset: 0x00009065
		public new void CancelAsync(object userState)
		{
			base.CancelAsync(userState);
		}

		// Token: 0x06000736 RID: 1846 RVA: 0x0000AE70 File Offset: 0x00009070
		private bool IsLocalFileSystemWebService(string url)
		{
			if (url == null || url == string.Empty)
			{
				return false;
			}
			Uri uri = new Uri(url);
			return uri.Port >= 1024 && string.Compare(uri.Host, "localHost", StringComparison.OrdinalIgnoreCase) == 0;
		}

		// Token: 0x04000291 RID: 657
		private SendOrPostCallback deleteIncomingMessagesOperationCompleted;

		// Token: 0x04000292 RID: 658
		private SendOrPostCallback deleteOutgoingMessagesOperationCompleted;

		// Token: 0x04000293 RID: 659
		private SendOrPostCallback getInboxMessagesOperationCompleted;

		// Token: 0x04000294 RID: 660
		private SendOrPostCallback getMessagesByUserOperationCompleted;

		// Token: 0x04000295 RID: 661
		private SendOrPostCallback getSendedMessagesOperationCompleted;

		// Token: 0x04000296 RID: 662
		private SendOrPostCallback getServiceNameOperationCompleted;

		// Token: 0x04000297 RID: 663
		private SendOrPostCallback readMessageOperationCompleted;

		// Token: 0x04000298 RID: 664
		private SendOrPostCallback sendNewMessageOperationCompleted;

		// Token: 0x04000299 RID: 665
		private bool useDefaultCredentialsSetExplicitly;
	}
}
