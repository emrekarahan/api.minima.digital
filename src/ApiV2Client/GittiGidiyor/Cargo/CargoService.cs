﻿using System;

namespace ApiV2Client.GittiGidiyor.Cargo
{
	// Token: 0x020001EA RID: 490
	public class CargoService : ServiceClient<IndividualCargoServiceService>, ICargoService, IService
	{
		// Token: 0x060010A2 RID: 4258 RVA: 0x000161B8 File Offset: 0x000143B8
		private CargoService()
		{
			base.configureNetworkCredentials(this.service);
		}

		// Token: 0x060010A3 RID: 4259 RVA: 0x000161D8 File Offset: 0x000143D8
		public cargoInformationResponse getCargoInformation(string saleCode, string lang)
		{
			long time = Util.convertToTimestamp(DateTime.Now);
			return this.service.getCargoInformation(this.authConfig.ApiKey, base.getSignature(time), time, saleCode, lang);
		}

		// Token: 0x060010A4 RID: 4260 RVA: 0x00016210 File Offset: 0x00014410
		public string getServiceName()
		{
			return this.service.getServiceName();
		}

		// Token: 0x060010A5 RID: 4261 RVA: 0x00016220 File Offset: 0x00014420
		public commonSaleResponse sendCargoInformation(string userType, string saleCode, string cargoPostCode, string cargoCompany, string cargoBranchCode, string followUpUrl, string lang)
		{
			long time = Util.convertToTimestamp(DateTime.Now);
			return this.service.sendCargoInformation(this.authConfig.ApiKey, base.getSignature(time), time, saleCode, cargoPostCode, cargoCompany, cargoBranchCode, followUpUrl, userType, lang);
		}

		// Token: 0x17000519 RID: 1305
		// (get) Token: 0x060010A6 RID: 4262 RVA: 0x00016264 File Offset: 0x00014464
		public static CargoService Instance
		{
			get
			{
				if (CargoService.instance == null)
				{
					lock (CargoService.lockObject)
					{
						if (CargoService.instance == null)
						{
							CargoService.instance = new CargoService();
						}
					}
				}
				return CargoService.instance;
			}
		}

		// Token: 0x0400061C RID: 1564
		private static CargoService instance;

		// Token: 0x0400061D RID: 1565
		private static object lockObject = new object();

		// Token: 0x0400061E RID: 1566
		private IndividualCargoServiceService service = new IndividualCargoServiceService();
	}
}
