﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Cargo
{
	// Token: 0x0200005B RID: 91
	[XmlInclude(typeof(cargoInformationResponse))]
	[XmlType(Namespace = "http://cargo.individual.ws.listingapi.gg.com")]
	[XmlInclude(typeof(commonSaleResponse))]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[DesignerCategory("code")]
	[DebuggerStepThrough]
	[Serializable]
	public class baseResponse
	{
		// Token: 0x17000146 RID: 326
		// (get) Token: 0x06000397 RID: 919 RVA: 0x00006055 File Offset: 0x00004255
		// (set) Token: 0x06000398 RID: 920 RVA: 0x0000605D File Offset: 0x0000425D
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string ackCode
		{
			get
			{
				return this.ackCodeField;
			}
			set
			{
				this.ackCodeField = value;
			}
		}

		// Token: 0x17000147 RID: 327
		// (get) Token: 0x06000399 RID: 921 RVA: 0x00006066 File Offset: 0x00004266
		// (set) Token: 0x0600039A RID: 922 RVA: 0x0000606E File Offset: 0x0000426E
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string responseTime
		{
			get
			{
				return this.responseTimeField;
			}
			set
			{
				this.responseTimeField = value;
			}
		}

		// Token: 0x17000148 RID: 328
		// (get) Token: 0x0600039B RID: 923 RVA: 0x00006077 File Offset: 0x00004277
		// (set) Token: 0x0600039C RID: 924 RVA: 0x0000607F File Offset: 0x0000427F
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public errorType error
		{
			get
			{
				return this.errorField;
			}
			set
			{
				this.errorField = value;
			}
		}

		// Token: 0x17000149 RID: 329
		// (get) Token: 0x0600039D RID: 925 RVA: 0x00006088 File Offset: 0x00004288
		// (set) Token: 0x0600039E RID: 926 RVA: 0x00006090 File Offset: 0x00004290
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string timeElapsed
		{
			get
			{
				return this.timeElapsedField;
			}
			set
			{
				this.timeElapsedField = value;
			}
		}

		// Token: 0x04000167 RID: 359
		private string ackCodeField;

		// Token: 0x04000168 RID: 360
		private string responseTimeField;

		// Token: 0x04000169 RID: 361
		private errorType errorField;

		// Token: 0x0400016A RID: 362
		private string timeElapsedField;
	}
}
