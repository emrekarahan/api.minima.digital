﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Cargo
{
	// Token: 0x02000060 RID: 96
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	public class getCargoInformationCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x060003B3 RID: 947 RVA: 0x0000611F File Offset: 0x0000431F
		internal getCargoInformationCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x17000150 RID: 336
		// (get) Token: 0x060003B4 RID: 948 RVA: 0x00006132 File Offset: 0x00004332
		public cargoInformationResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (cargoInformationResponse)this.results[0];
			}
		}

		// Token: 0x04000171 RID: 369
		private object[] results;
	}
}
