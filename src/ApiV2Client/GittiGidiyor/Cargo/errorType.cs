﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Cargo
{
	// Token: 0x0200005D RID: 93
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[XmlType(Namespace = "http://cargo.individual.ws.listingapi.gg.com")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[Serializable]
	public class errorType
	{
		// Token: 0x1700014C RID: 332
		// (get) Token: 0x060003A5 RID: 933 RVA: 0x000060CB File Offset: 0x000042CB
		// (set) Token: 0x060003A6 RID: 934 RVA: 0x000060D3 File Offset: 0x000042D3
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string errorId
		{
			get
			{
				return this.errorIdField;
			}
			set
			{
				this.errorIdField = value;
			}
		}

		// Token: 0x1700014D RID: 333
		// (get) Token: 0x060003A7 RID: 935 RVA: 0x000060DC File Offset: 0x000042DC
		// (set) Token: 0x060003A8 RID: 936 RVA: 0x000060E4 File Offset: 0x000042E4
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string errorCode
		{
			get
			{
				return this.errorCodeField;
			}
			set
			{
				this.errorCodeField = value;
			}
		}

		// Token: 0x1700014E RID: 334
		// (get) Token: 0x060003A9 RID: 937 RVA: 0x000060ED File Offset: 0x000042ED
		// (set) Token: 0x060003AA RID: 938 RVA: 0x000060F5 File Offset: 0x000042F5
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string message
		{
			get
			{
				return this.messageField;
			}
			set
			{
				this.messageField = value;
			}
		}

		// Token: 0x1700014F RID: 335
		// (get) Token: 0x060003AB RID: 939 RVA: 0x000060FE File Offset: 0x000042FE
		// (set) Token: 0x060003AC RID: 940 RVA: 0x00006106 File Offset: 0x00004306
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string viewMessage
		{
			get
			{
				return this.viewMessageField;
			}
			set
			{
				this.viewMessageField = value;
			}
		}

		// Token: 0x0400016D RID: 365
		private string errorIdField;

		// Token: 0x0400016E RID: 366
		private string errorCodeField;

		// Token: 0x0400016F RID: 367
		private string messageField;

		// Token: 0x04000170 RID: 368
		private string viewMessageField;
	}
}
