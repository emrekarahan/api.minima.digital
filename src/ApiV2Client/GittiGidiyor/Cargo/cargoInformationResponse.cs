﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Cargo
{
	// Token: 0x0200005C RID: 92
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[DesignerCategory("code")]
	[XmlType(Namespace = "http://cargo.individual.ws.listingapi.gg.com")]
	[DebuggerStepThrough]
	[Serializable]
	public class cargoInformationResponse : baseResponse
	{
		// Token: 0x1700014A RID: 330
		// (get) Token: 0x060003A0 RID: 928 RVA: 0x000060A1 File Offset: 0x000042A1
		// (set) Token: 0x060003A1 RID: 929 RVA: 0x000060A9 File Offset: 0x000042A9
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string cargoPostCode
		{
			get
			{
				return this.cargoPostCodeField;
			}
			set
			{
				this.cargoPostCodeField = value;
			}
		}

		// Token: 0x1700014B RID: 331
		// (get) Token: 0x060003A2 RID: 930 RVA: 0x000060B2 File Offset: 0x000042B2
		// (set) Token: 0x060003A3 RID: 931 RVA: 0x000060BA File Offset: 0x000042BA
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string cargoContent
		{
			get
			{
				return this.cargoContentField;
			}
			set
			{
				this.cargoContentField = value;
			}
		}

		// Token: 0x0400016B RID: 363
		private string cargoPostCodeField;

		// Token: 0x0400016C RID: 364
		private string cargoContentField;
	}
}
