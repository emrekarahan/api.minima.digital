﻿namespace ApiV2Client.GittiGidiyor.Cargo
{
	// Token: 0x020001DA RID: 474
	public interface ICargoService : IService
	{
		// Token: 0x06001035 RID: 4149
		cargoInformationResponse getCargoInformation(string saleCode, string lang);

		// Token: 0x06001036 RID: 4150
		commonSaleResponse sendCargoInformation(string userType, string saleCode, string cargoPostCode, string cargoCompany, string cargoBranchCode, string followUpUrl, string lang);
	}
}
