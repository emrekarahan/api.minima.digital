﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading;
using System.Web.Services;
using System.Web.Services.Description;
using System.Web.Services.Protocols;
using System.Xml.Serialization;
using ApiV2Client.Properties;

namespace ApiV2Client.GittiGidiyor.Cargo
{
	// Token: 0x0200005A RID: 90
	[XmlInclude(typeof(baseResponse))]
	[DebuggerStepThrough]
	[WebServiceBinding(Name = "IndividualCargoServiceBinding", Namespace = "http://cargo.individual.ws.listingapi.gg.com")]
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	[DesignerCategory("code")]
	public class IndividualCargoServiceService : SoapHttpClientProtocol
	{
		// Token: 0x0600037E RID: 894 RVA: 0x00005B39 File Offset: 0x00003D39
		public IndividualCargoServiceService()
		{
			this.Url = Settings.Default.ApiV2Client_GittiGidiyor_Cargo_IndividualCargoServiceService;
			if (this.IsLocalFileSystemWebService(this.Url))
			{
				this.UseDefaultCredentials = true;
				this.useDefaultCredentialsSetExplicitly = false;
				return;
			}
			this.useDefaultCredentialsSetExplicitly = true;
		}

		// Token: 0x17000144 RID: 324
		// (get) Token: 0x0600037F RID: 895 RVA: 0x00005B75 File Offset: 0x00003D75
		// (set) Token: 0x06000380 RID: 896 RVA: 0x00005B7D File Offset: 0x00003D7D
		public new string Url
		{
			get
			{
				return base.Url;
			}
			set
			{
				if (this.IsLocalFileSystemWebService(base.Url) && !this.useDefaultCredentialsSetExplicitly && !this.IsLocalFileSystemWebService(value))
				{
					base.UseDefaultCredentials = false;
				}
				base.Url = value;
			}
		}

		// Token: 0x17000145 RID: 325
		// (get) Token: 0x06000381 RID: 897 RVA: 0x00005BAC File Offset: 0x00003DAC
		// (set) Token: 0x06000382 RID: 898 RVA: 0x00005BB4 File Offset: 0x00003DB4
		public new bool UseDefaultCredentials
		{
			get
			{
				return base.UseDefaultCredentials;
			}
			set
			{
				base.UseDefaultCredentials = value;
				this.useDefaultCredentialsSetExplicitly = true;
			}
		}

		// Token: 0x14000015 RID: 21
		// (add) Token: 0x06000383 RID: 899 RVA: 0x00005BC4 File Offset: 0x00003DC4
		// (remove) Token: 0x06000384 RID: 900 RVA: 0x00005BFC File Offset: 0x00003DFC
		public event getCargoInformationCompletedEventHandler getCargoInformationCompleted;

		// Token: 0x14000016 RID: 22
		// (add) Token: 0x06000385 RID: 901 RVA: 0x00005C34 File Offset: 0x00003E34
		// (remove) Token: 0x06000386 RID: 902 RVA: 0x00005C6C File Offset: 0x00003E6C
		public event getServiceNameCompletedEventHandler getServiceNameCompleted;

		// Token: 0x14000017 RID: 23
		// (add) Token: 0x06000387 RID: 903 RVA: 0x00005CA4 File Offset: 0x00003EA4
		// (remove) Token: 0x06000388 RID: 904 RVA: 0x00005CDC File Offset: 0x00003EDC
		public event sendCargoInformationCompletedEventHandler sendCargoInformationCompleted;

		// Token: 0x06000389 RID: 905 RVA: 0x00005D14 File Offset: 0x00003F14
		[SoapRpcMethod("", RequestNamespace = "http://cargo.individual.ws.listingapi.gg.com", ResponseNamespace = "http://cargo.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public cargoInformationResponse getCargoInformation(string apiKey, string sign, long time, string saleCode, string lang)
		{
			object[] array = base.Invoke("getCargoInformation", new object[]
			{
				apiKey,
				sign,
				time,
				saleCode,
				lang
			});
			return (cargoInformationResponse)array[0];
		}

		// Token: 0x0600038A RID: 906 RVA: 0x00005D58 File Offset: 0x00003F58
		public void getCargoInformationAsync(string apiKey, string sign, long time, string saleCode, string lang)
		{
			this.getCargoInformationAsync(apiKey, sign, time, saleCode, lang, null);
		}

		// Token: 0x0600038B RID: 907 RVA: 0x00005D68 File Offset: 0x00003F68
		public void getCargoInformationAsync(string apiKey, string sign, long time, string saleCode, string lang, object userState)
		{
			if (this.getCargoInformationOperationCompleted == null)
			{
				this.getCargoInformationOperationCompleted = new SendOrPostCallback(this.OngetCargoInformationOperationCompleted);
			}
			base.InvokeAsync("getCargoInformation", new object[]
			{
				apiKey,
				sign,
				time,
				saleCode,
				lang
			}, this.getCargoInformationOperationCompleted, userState);
		}

		// Token: 0x0600038C RID: 908 RVA: 0x00005DC8 File Offset: 0x00003FC8
		private void OngetCargoInformationOperationCompleted(object arg)
		{
			if (this.getCargoInformationCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getCargoInformationCompleted(this, new getCargoInformationCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x0600038D RID: 909 RVA: 0x00005E10 File Offset: 0x00004010
		[SoapRpcMethod("", RequestNamespace = "http://cargo.individual.ws.listingapi.gg.com", ResponseNamespace = "http://cargo.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public string getServiceName()
		{
			object[] array = base.Invoke("getServiceName", new object[0]);
			return (string)array[0];
		}

		// Token: 0x0600038E RID: 910 RVA: 0x00005E37 File Offset: 0x00004037
		public void getServiceNameAsync()
		{
			this.getServiceNameAsync(null);
		}

		// Token: 0x0600038F RID: 911 RVA: 0x00005E40 File Offset: 0x00004040
		public void getServiceNameAsync(object userState)
		{
			if (this.getServiceNameOperationCompleted == null)
			{
				this.getServiceNameOperationCompleted = new SendOrPostCallback(this.OngetServiceNameOperationCompleted);
			}
			base.InvokeAsync("getServiceName", new object[0], this.getServiceNameOperationCompleted, userState);
		}

		// Token: 0x06000390 RID: 912 RVA: 0x00005E74 File Offset: 0x00004074
		private void OngetServiceNameOperationCompleted(object arg)
		{
			if (this.getServiceNameCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getServiceNameCompleted(this, new getServiceNameCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x06000391 RID: 913 RVA: 0x00005EBC File Offset: 0x000040BC
		[SoapRpcMethod("", RequestNamespace = "http://cargo.individual.ws.listingapi.gg.com", ResponseNamespace = "http://cargo.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public commonSaleResponse sendCargoInformation(string apiKey, string sign, long time, string saleCode, string cargoPostCode, string cargoCompany, string cargoBranch, string followUpUrl, string userType, string lang)
		{
			object[] array = base.Invoke("sendCargoInformation", new object[]
			{
				apiKey,
				sign,
				time,
				saleCode,
				cargoPostCode,
				cargoCompany,
				cargoBranch,
				followUpUrl,
				userType,
				lang
			});
			return (commonSaleResponse)array[0];
		}

		// Token: 0x06000392 RID: 914 RVA: 0x00005F1C File Offset: 0x0000411C
		public void sendCargoInformationAsync(string apiKey, string sign, long time, string saleCode, string cargoPostCode, string cargoCompany, string cargoBranch, string followUpUrl, string userType, string lang)
		{
			this.sendCargoInformationAsync(apiKey, sign, time, saleCode, cargoPostCode, cargoCompany, cargoBranch, followUpUrl, userType, lang, null);
		}

		// Token: 0x06000393 RID: 915 RVA: 0x00005F44 File Offset: 0x00004144
		public void sendCargoInformationAsync(string apiKey, string sign, long time, string saleCode, string cargoPostCode, string cargoCompany, string cargoBranch, string followUpUrl, string userType, string lang, object userState)
		{
			if (this.sendCargoInformationOperationCompleted == null)
			{
				this.sendCargoInformationOperationCompleted = new SendOrPostCallback(this.OnsendCargoInformationOperationCompleted);
			}
			base.InvokeAsync("sendCargoInformation", new object[]
			{
				apiKey,
				sign,
				time,
				saleCode,
				cargoPostCode,
				cargoCompany,
				cargoBranch,
				followUpUrl,
				userType,
				lang
			}, this.sendCargoInformationOperationCompleted, userState);
		}

		// Token: 0x06000394 RID: 916 RVA: 0x00005FBC File Offset: 0x000041BC
		private void OnsendCargoInformationOperationCompleted(object arg)
		{
			if (this.sendCargoInformationCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.sendCargoInformationCompleted(this, new sendCargoInformationCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x06000395 RID: 917 RVA: 0x00006001 File Offset: 0x00004201
		public new void CancelAsync(object userState)
		{
			base.CancelAsync(userState);
		}

		// Token: 0x06000396 RID: 918 RVA: 0x0000600C File Offset: 0x0000420C
		private bool IsLocalFileSystemWebService(string url)
		{
			if (url == null || url == string.Empty)
			{
				return false;
			}
			Uri uri = new Uri(url);
			return uri.Port >= 1024 && string.Compare(uri.Host, "localHost", StringComparison.OrdinalIgnoreCase) == 0;
		}

		// Token: 0x04000160 RID: 352
		private SendOrPostCallback getCargoInformationOperationCompleted;

		// Token: 0x04000161 RID: 353
		private SendOrPostCallback getServiceNameOperationCompleted;

		// Token: 0x04000162 RID: 354
		private SendOrPostCallback sendCargoInformationOperationCompleted;

		// Token: 0x04000163 RID: 355
		private bool useDefaultCredentialsSetExplicitly;
	}
}
