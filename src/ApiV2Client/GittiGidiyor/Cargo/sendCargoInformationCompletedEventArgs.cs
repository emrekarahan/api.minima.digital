﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Cargo
{
	// Token: 0x02000064 RID: 100
	[DesignerCategory("code")]
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	[DebuggerStepThrough]
	public class sendCargoInformationCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x060003BF RID: 959 RVA: 0x0000616F File Offset: 0x0000436F
		internal sendCargoInformationCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x17000152 RID: 338
		// (get) Token: 0x060003C0 RID: 960 RVA: 0x00006182 File Offset: 0x00004382
		public commonSaleResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (commonSaleResponse)this.results[0];
			}
		}

		// Token: 0x04000173 RID: 371
		private object[] results;
	}
}
