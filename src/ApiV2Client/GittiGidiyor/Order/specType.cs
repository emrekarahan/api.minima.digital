﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Order
{
	// Token: 0x020000F8 RID: 248
	[DesignerCategory("code")]
	[XmlType(Namespace = "http://order.individual.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[DebuggerStepThrough]
	[Serializable]
	public class specType
	{
		// Token: 0x170002A3 RID: 675
		// (get) Token: 0x0600086C RID: 2156 RVA: 0x0000C3EE File Offset: 0x0000A5EE
		// (set) Token: 0x0600086D RID: 2157 RVA: 0x0000C3F6 File Offset: 0x0000A5F6
		[XmlAttribute]
		public string name
		{
			get
			{
				return this.nameField;
			}
			set
			{
				this.nameField = value;
			}
		}

		// Token: 0x170002A4 RID: 676
		// (get) Token: 0x0600086E RID: 2158 RVA: 0x0000C3FF File Offset: 0x0000A5FF
		// (set) Token: 0x0600086F RID: 2159 RVA: 0x0000C407 File Offset: 0x0000A607
		[XmlAttribute]
		public string value
		{
			get
			{
				return this.valueField;
			}
			set
			{
				this.valueField = value;
			}
		}

		// Token: 0x170002A5 RID: 677
		// (get) Token: 0x06000870 RID: 2160 RVA: 0x0000C410 File Offset: 0x0000A610
		// (set) Token: 0x06000871 RID: 2161 RVA: 0x0000C418 File Offset: 0x0000A618
		[XmlAttribute]
		public string type
		{
			get
			{
				return this.typeField;
			}
			set
			{
				this.typeField = value;
			}
		}

		// Token: 0x170002A6 RID: 678
		// (get) Token: 0x06000872 RID: 2162 RVA: 0x0000C421 File Offset: 0x0000A621
		// (set) Token: 0x06000873 RID: 2163 RVA: 0x0000C429 File Offset: 0x0000A629
		[XmlAttribute]
		public bool required
		{
			get
			{
				return this.requiredField;
			}
			set
			{
				this.requiredField = value;
			}
		}

		// Token: 0x170002A7 RID: 679
		// (get) Token: 0x06000874 RID: 2164 RVA: 0x0000C432 File Offset: 0x0000A632
		// (set) Token: 0x06000875 RID: 2165 RVA: 0x0000C43A File Offset: 0x0000A63A
		[XmlIgnore]
		public bool requiredSpecified
		{
			get
			{
				return this.requiredFieldSpecified;
			}
			set
			{
				this.requiredFieldSpecified = value;
			}
		}

		// Token: 0x0400031A RID: 794
		private string nameField;

		// Token: 0x0400031B RID: 795
		private string valueField;

		// Token: 0x0400031C RID: 796
		private string typeField;

		// Token: 0x0400031D RID: 797
		private bool requiredField;

		// Token: 0x0400031E RID: 798
		private bool requiredFieldSpecified;
	}
}
