﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Order
{
	// Token: 0x020000F4 RID: 244
	[XmlType(Namespace = "http://order.individual.ws.listingapi.gg.com")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[Serializable]
	public class photoType
	{
		// Token: 0x17000292 RID: 658
		// (get) Token: 0x06000846 RID: 2118 RVA: 0x0000C2AD File Offset: 0x0000A4AD
		// (set) Token: 0x06000847 RID: 2119 RVA: 0x0000C2B5 File Offset: 0x0000A4B5
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string url
		{
			get
			{
				return this.urlField;
			}
			set
			{
				this.urlField = value;
			}
		}

		// Token: 0x17000293 RID: 659
		// (get) Token: 0x06000848 RID: 2120 RVA: 0x0000C2BE File Offset: 0x0000A4BE
		// (set) Token: 0x06000849 RID: 2121 RVA: 0x0000C2C6 File Offset: 0x0000A4C6
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string base64
		{
			get
			{
				return this.base64Field;
			}
			set
			{
				this.base64Field = value;
			}
		}

		// Token: 0x17000294 RID: 660
		// (get) Token: 0x0600084A RID: 2122 RVA: 0x0000C2CF File Offset: 0x0000A4CF
		// (set) Token: 0x0600084B RID: 2123 RVA: 0x0000C2D7 File Offset: 0x0000A4D7
		[XmlAttribute]
		public int photoId
		{
			get
			{
				return this.photoIdField;
			}
			set
			{
				this.photoIdField = value;
			}
		}

		// Token: 0x17000295 RID: 661
		// (get) Token: 0x0600084C RID: 2124 RVA: 0x0000C2E0 File Offset: 0x0000A4E0
		// (set) Token: 0x0600084D RID: 2125 RVA: 0x0000C2E8 File Offset: 0x0000A4E8
		[XmlIgnore]
		public bool photoIdSpecified
		{
			get
			{
				return this.photoIdFieldSpecified;
			}
			set
			{
				this.photoIdFieldSpecified = value;
			}
		}

		// Token: 0x04000309 RID: 777
		private string urlField;

		// Token: 0x0400030A RID: 778
		private string base64Field;

		// Token: 0x0400030B RID: 779
		private int photoIdField;

		// Token: 0x0400030C RID: 780
		private bool photoIdFieldSpecified;
	}
}
