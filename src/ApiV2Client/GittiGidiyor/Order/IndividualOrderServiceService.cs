﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading;
using System.Web.Services;
using System.Web.Services.Description;
using System.Web.Services.Protocols;
using System.Xml.Schema;
using System.Xml.Serialization;
using ApiV2Client.Properties;

namespace ApiV2Client.GittiGidiyor.Order
{
	// Token: 0x020000EC RID: 236
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	[DesignerCategory("code")]
	[WebServiceBinding(Name = "IndividualOrderServiceBinding", Namespace = "http://order.individual.ws.listingapi.gg.com")]
	[XmlInclude(typeof(baseResponse))]
	[DebuggerStepThrough]
	public class IndividualOrderServiceService : SoapHttpClientProtocol
	{
		// Token: 0x060007A0 RID: 1952 RVA: 0x0000B1DB File Offset: 0x000093DB
		public IndividualOrderServiceService()
		{
			this.Url = Settings.Default.ApiV2Client_GittiGidiyor_Order_IndividualOrderServiceService;
			if (this.IsLocalFileSystemWebService(this.Url))
			{
				this.UseDefaultCredentials = true;
				this.useDefaultCredentialsSetExplicitly = false;
				return;
			}
			this.useDefaultCredentialsSetExplicitly = true;
		}

		// Token: 0x1700025C RID: 604
		// (get) Token: 0x060007A1 RID: 1953 RVA: 0x0000B217 File Offset: 0x00009417
		// (set) Token: 0x060007A2 RID: 1954 RVA: 0x0000B21F File Offset: 0x0000941F
		public new string Url
		{
			get
			{
				return base.Url;
			}
			set
			{
				if (this.IsLocalFileSystemWebService(base.Url) && !this.useDefaultCredentialsSetExplicitly && !this.IsLocalFileSystemWebService(value))
				{
					base.UseDefaultCredentials = false;
				}
				base.Url = value;
			}
		}

		// Token: 0x1700025D RID: 605
		// (get) Token: 0x060007A3 RID: 1955 RVA: 0x0000B24E File Offset: 0x0000944E
		// (set) Token: 0x060007A4 RID: 1956 RVA: 0x0000B256 File Offset: 0x00009456
		public new bool UseDefaultCredentials
		{
			get
			{
				return base.UseDefaultCredentials;
			}
			set
			{
				base.UseDefaultCredentials = value;
				this.useDefaultCredentialsSetExplicitly = true;
			}
		}

		// Token: 0x1400003F RID: 63
		// (add) Token: 0x060007A5 RID: 1957 RVA: 0x0000B268 File Offset: 0x00009468
		// (remove) Token: 0x060007A6 RID: 1958 RVA: 0x0000B2A0 File Offset: 0x000094A0
		public event getOrderCodeListCompletedEventHandler getOrderCodeListCompleted;

		// Token: 0x14000040 RID: 64
		// (add) Token: 0x060007A7 RID: 1959 RVA: 0x0000B2D8 File Offset: 0x000094D8
		// (remove) Token: 0x060007A8 RID: 1960 RVA: 0x0000B310 File Offset: 0x00009510
		public event getServiceNameCompletedEventHandler getServiceNameCompleted;

		// Token: 0x14000041 RID: 65
		// (add) Token: 0x060007A9 RID: 1961 RVA: 0x0000B348 File Offset: 0x00009548
		// (remove) Token: 0x060007AA RID: 1962 RVA: 0x0000B380 File Offset: 0x00009580
		public event getUnsoldProductListCompletedEventHandler getUnsoldProductListCompleted;

		// Token: 0x14000042 RID: 66
		// (add) Token: 0x060007AB RID: 1963 RVA: 0x0000B3B8 File Offset: 0x000095B8
		// (remove) Token: 0x060007AC RID: 1964 RVA: 0x0000B3F0 File Offset: 0x000095F0
		public event makeRevisionCompletedEventHandler makeRevisionCompleted;

		// Token: 0x14000043 RID: 67
		// (add) Token: 0x060007AD RID: 1965 RVA: 0x0000B428 File Offset: 0x00009628
		// (remove) Token: 0x060007AE RID: 1966 RVA: 0x0000B460 File Offset: 0x00009660
		public event putUpForSaleCompletedEventHandler putUpForSaleCompleted;

		// Token: 0x14000044 RID: 68
		// (add) Token: 0x060007AF RID: 1967 RVA: 0x0000B498 File Offset: 0x00009698
		// (remove) Token: 0x060007B0 RID: 1968 RVA: 0x0000B4D0 File Offset: 0x000096D0
		public event relistProductsCompletedEventHandler relistProductsCompleted;

		// Token: 0x14000045 RID: 69
		// (add) Token: 0x060007B1 RID: 1969 RVA: 0x0000B508 File Offset: 0x00009708
		// (remove) Token: 0x060007B2 RID: 1970 RVA: 0x0000B540 File Offset: 0x00009740
		public event sendCargoInformationCollectivelyCompletedEventHandler sendCargoInformationCollectivelyCompleted;

		// Token: 0x14000046 RID: 70
		// (add) Token: 0x060007B3 RID: 1971 RVA: 0x0000B578 File Offset: 0x00009778
		// (remove) Token: 0x060007B4 RID: 1972 RVA: 0x0000B5B0 File Offset: 0x000097B0
		public event sendMessageCollectivelyCompletedEventHandler sendMessageCollectivelyCompleted;

		// Token: 0x060007B5 RID: 1973 RVA: 0x0000B5E8 File Offset: 0x000097E8
		[SoapRpcMethod("", RequestNamespace = "http://order.individual.ws.listingapi.gg.com", ResponseNamespace = "http://order.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public orderServiceDetailResponse getOrderCodeList(string apiKey, string sign, long time, int startOffSet, int rowCount, int timeInterval, string lang)
		{
			object[] array = base.Invoke("getOrderCodeList", new object[]
			{
				apiKey,
				sign,
				time,
				startOffSet,
				rowCount,
				timeInterval,
				lang
			});
			return (orderServiceDetailResponse)array[0];
		}

		// Token: 0x060007B6 RID: 1974 RVA: 0x0000B648 File Offset: 0x00009848
		public void getOrderCodeListAsync(string apiKey, string sign, long time, int startOffSet, int rowCount, int timeInterval, string lang)
		{
			this.getOrderCodeListAsync(apiKey, sign, time, startOffSet, rowCount, timeInterval, lang, null);
		}

		// Token: 0x060007B7 RID: 1975 RVA: 0x0000B668 File Offset: 0x00009868
		public void getOrderCodeListAsync(string apiKey, string sign, long time, int startOffSet, int rowCount, int timeInterval, string lang, object userState)
		{
			if (this.getOrderCodeListOperationCompleted == null)
			{
				this.getOrderCodeListOperationCompleted = new SendOrPostCallback(this.OngetOrderCodeListOperationCompleted);
			}
			base.InvokeAsync("getOrderCodeList", new object[]
			{
				apiKey,
				sign,
				time,
				startOffSet,
				rowCount,
				timeInterval,
				lang
			}, this.getOrderCodeListOperationCompleted, userState);
		}

		// Token: 0x060007B8 RID: 1976 RVA: 0x0000B6E0 File Offset: 0x000098E0
		private void OngetOrderCodeListOperationCompleted(object arg)
		{
			if (this.getOrderCodeListCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getOrderCodeListCompleted(this, new getOrderCodeListCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x060007B9 RID: 1977 RVA: 0x0000B728 File Offset: 0x00009928
		[SoapRpcMethod("", RequestNamespace = "http://order.individual.ws.listingapi.gg.com", ResponseNamespace = "http://order.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public string getServiceName()
		{
			object[] array = base.Invoke("getServiceName", new object[0]);
			return (string)array[0];
		}

		// Token: 0x060007BA RID: 1978 RVA: 0x0000B74F File Offset: 0x0000994F
		public void getServiceNameAsync()
		{
			this.getServiceNameAsync(null);
		}

		// Token: 0x060007BB RID: 1979 RVA: 0x0000B758 File Offset: 0x00009958
		public void getServiceNameAsync(object userState)
		{
			if (this.getServiceNameOperationCompleted == null)
			{
				this.getServiceNameOperationCompleted = new SendOrPostCallback(this.OngetServiceNameOperationCompleted);
			}
			base.InvokeAsync("getServiceName", new object[0], this.getServiceNameOperationCompleted, userState);
		}

		// Token: 0x060007BC RID: 1980 RVA: 0x0000B78C File Offset: 0x0000998C
		private void OngetServiceNameOperationCompleted(object arg)
		{
			if (this.getServiceNameCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getServiceNameCompleted(this, new getServiceNameCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x060007BD RID: 1981 RVA: 0x0000B7D4 File Offset: 0x000099D4
		[SoapRpcMethod("", RequestNamespace = "http://order.individual.ws.listingapi.gg.com", ResponseNamespace = "http://order.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public orderServiceListResponse getUnsoldProductList(string apiKey, string sign, long time, int startOffSet, int rowCount, string lang)
		{
			object[] array = base.Invoke("getUnsoldProductList", new object[]
			{
				apiKey,
				sign,
				time,
				startOffSet,
				rowCount,
				lang
			});
			return (orderServiceListResponse)array[0];
		}

		// Token: 0x060007BE RID: 1982 RVA: 0x0000B827 File Offset: 0x00009A27
		public void getUnsoldProductListAsync(string apiKey, string sign, long time, int startOffSet, int rowCount, string lang)
		{
			this.getUnsoldProductListAsync(apiKey, sign, time, startOffSet, rowCount, lang, null);
		}

		// Token: 0x060007BF RID: 1983 RVA: 0x0000B83C File Offset: 0x00009A3C
		public void getUnsoldProductListAsync(string apiKey, string sign, long time, int startOffSet, int rowCount, string lang, object userState)
		{
			if (this.getUnsoldProductListOperationCompleted == null)
			{
				this.getUnsoldProductListOperationCompleted = new SendOrPostCallback(this.OngetUnsoldProductListOperationCompleted);
			}
			base.InvokeAsync("getUnsoldProductList", new object[]
			{
				apiKey,
				sign,
				time,
				startOffSet,
				rowCount,
				lang
			}, this.getUnsoldProductListOperationCompleted, userState);
		}

		// Token: 0x060007C0 RID: 1984 RVA: 0x0000B8A8 File Offset: 0x00009AA8
		private void OngetUnsoldProductListOperationCompleted(object arg)
		{
			if (this.getUnsoldProductListCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getUnsoldProductListCompleted(this, new getUnsoldProductListCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x060007C1 RID: 1985 RVA: 0x0000B8F0 File Offset: 0x00009AF0
		[SoapRpcMethod("", RequestNamespace = "http://order.individual.ws.listingapi.gg.com", ResponseNamespace = "http://order.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public orderServiceResponse makeRevision(string apiKey, string sign, long time, string itemId, productType product, bool forceToSpecEntry, string lang)
		{
			object[] array = base.Invoke("makeRevision", new object[]
			{
				apiKey,
				sign,
				time,
				itemId,
				product,
				forceToSpecEntry,
				lang
			});
			return (orderServiceResponse)array[0];
		}

		// Token: 0x060007C2 RID: 1986 RVA: 0x0000B944 File Offset: 0x00009B44
		public void makeRevisionAsync(string apiKey, string sign, long time, string itemId, productType product, bool forceToSpecEntry, string lang)
		{
			this.makeRevisionAsync(apiKey, sign, time, itemId, product, forceToSpecEntry, lang, null);
		}

		// Token: 0x060007C3 RID: 1987 RVA: 0x0000B964 File Offset: 0x00009B64
		public void makeRevisionAsync(string apiKey, string sign, long time, string itemId, productType product, bool forceToSpecEntry, string lang, object userState)
		{
			if (this.makeRevisionOperationCompleted == null)
			{
				this.makeRevisionOperationCompleted = new SendOrPostCallback(this.OnmakeRevisionOperationCompleted);
			}
			base.InvokeAsync("makeRevision", new object[]
			{
				apiKey,
				sign,
				time,
				itemId,
				product,
				forceToSpecEntry,
				lang
			}, this.makeRevisionOperationCompleted, userState);
		}

		// Token: 0x060007C4 RID: 1988 RVA: 0x0000B9D0 File Offset: 0x00009BD0
		private void OnmakeRevisionOperationCompleted(object arg)
		{
			if (this.makeRevisionCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.makeRevisionCompleted(this, new makeRevisionCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x060007C5 RID: 1989 RVA: 0x0000BA18 File Offset: 0x00009C18
		[SoapRpcMethod("", RequestNamespace = "http://order.individual.ws.listingapi.gg.com", ResponseNamespace = "http://order.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public orderServiceResponse putUpForSale(string apiKey, string sign, long time, string itemId, productType product, bool forceToSpecEntry, string lang)
		{
			object[] array = base.Invoke("putUpForSale", new object[]
			{
				apiKey,
				sign,
				time,
				itemId,
				product,
				forceToSpecEntry,
				lang
			});
			return (orderServiceResponse)array[0];
		}

		// Token: 0x060007C6 RID: 1990 RVA: 0x0000BA6C File Offset: 0x00009C6C
		public void putUpForSaleAsync(string apiKey, string sign, long time, string itemId, productType product, bool forceToSpecEntry, string lang)
		{
			this.putUpForSaleAsync(apiKey, sign, time, itemId, product, forceToSpecEntry, lang, null);
		}

		// Token: 0x060007C7 RID: 1991 RVA: 0x0000BA8C File Offset: 0x00009C8C
		public void putUpForSaleAsync(string apiKey, string sign, long time, string itemId, productType product, bool forceToSpecEntry, string lang, object userState)
		{
			if (this.putUpForSaleOperationCompleted == null)
			{
				this.putUpForSaleOperationCompleted = new SendOrPostCallback(this.OnputUpForSaleOperationCompleted);
			}
			base.InvokeAsync("putUpForSale", new object[]
			{
				apiKey,
				sign,
				time,
				itemId,
				product,
				forceToSpecEntry,
				lang
			}, this.putUpForSaleOperationCompleted, userState);
		}

		// Token: 0x060007C8 RID: 1992 RVA: 0x0000BAF8 File Offset: 0x00009CF8
		private void OnputUpForSaleOperationCompleted(object arg)
		{
			if (this.putUpForSaleCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.putUpForSaleCompleted(this, new putUpForSaleCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x060007C9 RID: 1993 RVA: 0x0000BB40 File Offset: 0x00009D40
		[SoapRpcMethod("", RequestNamespace = "http://order.individual.ws.listingapi.gg.com", ResponseNamespace = "http://order.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public orderServiceMessageResponse relistProducts(string apiKey, string sign, long time, [XmlArrayItem("item", Form = XmlSchemaForm.Unqualified)] string[] itemIdList, string lang)
		{
			object[] array = base.Invoke("relistProducts", new object[]
			{
				apiKey,
				sign,
				time,
				itemIdList,
				lang
			});
			return (orderServiceMessageResponse)array[0];
		}

		// Token: 0x060007CA RID: 1994 RVA: 0x0000BB84 File Offset: 0x00009D84
		public void relistProductsAsync(string apiKey, string sign, long time, string[] itemIdList, string lang)
		{
			this.relistProductsAsync(apiKey, sign, time, itemIdList, lang, null);
		}

		// Token: 0x060007CB RID: 1995 RVA: 0x0000BB94 File Offset: 0x00009D94
		public void relistProductsAsync(string apiKey, string sign, long time, string[] itemIdList, string lang, object userState)
		{
			if (this.relistProductsOperationCompleted == null)
			{
				this.relistProductsOperationCompleted = new SendOrPostCallback(this.OnrelistProductsOperationCompleted);
			}
			base.InvokeAsync("relistProducts", new object[]
			{
				apiKey,
				sign,
				time,
				itemIdList,
				lang
			}, this.relistProductsOperationCompleted, userState);
		}

		// Token: 0x060007CC RID: 1996 RVA: 0x0000BBF4 File Offset: 0x00009DF4
		private void OnrelistProductsOperationCompleted(object arg)
		{
			if (this.relistProductsCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.relistProductsCompleted(this, new relistProductsCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x060007CD RID: 1997 RVA: 0x0000BC3C File Offset: 0x00009E3C
		[SoapRpcMethod("", RequestNamespace = "http://order.individual.ws.listingapi.gg.com", ResponseNamespace = "http://order.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public orderServiceSaleResponse sendCargoInformationCollectively(string apiKey, string sign, long time, string orderCode, string cargoPostCode, string cargoCompany, string cargoBranch, string followUpUrl, string lang)
		{
			object[] array = base.Invoke("sendCargoInformationCollectively", new object[]
			{
				apiKey,
				sign,
				time,
				orderCode,
				cargoPostCode,
				cargoCompany,
				cargoBranch,
				followUpUrl,
				lang
			});
			return (orderServiceSaleResponse)array[0];
		}

		// Token: 0x060007CE RID: 1998 RVA: 0x0000BC98 File Offset: 0x00009E98
		public void sendCargoInformationCollectivelyAsync(string apiKey, string sign, long time, string orderCode, string cargoPostCode, string cargoCompany, string cargoBranch, string followUpUrl, string lang)
		{
			this.sendCargoInformationCollectivelyAsync(apiKey, sign, time, orderCode, cargoPostCode, cargoCompany, cargoBranch, followUpUrl, lang, null);
		}

		// Token: 0x060007CF RID: 1999 RVA: 0x0000BCBC File Offset: 0x00009EBC
		public void sendCargoInformationCollectivelyAsync(string apiKey, string sign, long time, string orderCode, string cargoPostCode, string cargoCompany, string cargoBranch, string followUpUrl, string lang, object userState)
		{
			if (this.sendCargoInformationCollectivelyOperationCompleted == null)
			{
				this.sendCargoInformationCollectivelyOperationCompleted = new SendOrPostCallback(this.OnsendCargoInformationCollectivelyOperationCompleted);
			}
			base.InvokeAsync("sendCargoInformationCollectively", new object[]
			{
				apiKey,
				sign,
				time,
				orderCode,
				cargoPostCode,
				cargoCompany,
				cargoBranch,
				followUpUrl,
				lang
			}, this.sendCargoInformationCollectivelyOperationCompleted, userState);
		}

		// Token: 0x060007D0 RID: 2000 RVA: 0x0000BD30 File Offset: 0x00009F30
		private void OnsendCargoInformationCollectivelyOperationCompleted(object arg)
		{
			if (this.sendCargoInformationCollectivelyCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.sendCargoInformationCollectivelyCompleted(this, new sendCargoInformationCollectivelyCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x060007D1 RID: 2001 RVA: 0x0000BD78 File Offset: 0x00009F78
		[SoapRpcMethod("", RequestNamespace = "http://order.individual.ws.listingapi.gg.com", ResponseNamespace = "http://order.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public orderServiceMessageResponse sendMessageCollectively(string apiKey, string sign, long time, string orderCode, string title, string message, bool sendCopy, string lang)
		{
			object[] array = base.Invoke("sendMessageCollectively", new object[]
			{
				apiKey,
				sign,
				time,
				orderCode,
				title,
				message,
				sendCopy,
				lang
			});
			return (orderServiceMessageResponse)array[0];
		}

		// Token: 0x060007D2 RID: 2002 RVA: 0x0000BDD0 File Offset: 0x00009FD0
		public void sendMessageCollectivelyAsync(string apiKey, string sign, long time, string orderCode, string title, string message, bool sendCopy, string lang)
		{
			this.sendMessageCollectivelyAsync(apiKey, sign, time, orderCode, title, message, sendCopy, lang, null);
		}

		// Token: 0x060007D3 RID: 2003 RVA: 0x0000BDF4 File Offset: 0x00009FF4
		public void sendMessageCollectivelyAsync(string apiKey, string sign, long time, string orderCode, string title, string message, bool sendCopy, string lang, object userState)
		{
			if (this.sendMessageCollectivelyOperationCompleted == null)
			{
				this.sendMessageCollectivelyOperationCompleted = new SendOrPostCallback(this.OnsendMessageCollectivelyOperationCompleted);
			}
			base.InvokeAsync("sendMessageCollectively", new object[]
			{
				apiKey,
				sign,
				time,
				orderCode,
				title,
				message,
				sendCopy,
				lang
			}, this.sendMessageCollectivelyOperationCompleted, userState);
		}

		// Token: 0x060007D4 RID: 2004 RVA: 0x0000BE68 File Offset: 0x0000A068
		private void OnsendMessageCollectivelyOperationCompleted(object arg)
		{
			if (this.sendMessageCollectivelyCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.sendMessageCollectivelyCompleted(this, new sendMessageCollectivelyCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x060007D5 RID: 2005 RVA: 0x0000BEAD File Offset: 0x0000A0AD
		public new void CancelAsync(object userState)
		{
			base.CancelAsync(userState);
		}

		// Token: 0x060007D6 RID: 2006 RVA: 0x0000BEB8 File Offset: 0x0000A0B8
		private bool IsLocalFileSystemWebService(string url)
		{
			if (url == null || url == string.Empty)
			{
				return false;
			}
			Uri uri = new Uri(url);
			return uri.Port >= 1024 && string.Compare(uri.Host, "localHost", StringComparison.OrdinalIgnoreCase) == 0;
		}

		// Token: 0x040002C4 RID: 708
		private SendOrPostCallback getOrderCodeListOperationCompleted;

		// Token: 0x040002C5 RID: 709
		private SendOrPostCallback getServiceNameOperationCompleted;

		// Token: 0x040002C6 RID: 710
		private SendOrPostCallback getUnsoldProductListOperationCompleted;

		// Token: 0x040002C7 RID: 711
		private SendOrPostCallback makeRevisionOperationCompleted;

		// Token: 0x040002C8 RID: 712
		private SendOrPostCallback putUpForSaleOperationCompleted;

		// Token: 0x040002C9 RID: 713
		private SendOrPostCallback relistProductsOperationCompleted;

		// Token: 0x040002CA RID: 714
		private SendOrPostCallback sendCargoInformationCollectivelyOperationCompleted;

		// Token: 0x040002CB RID: 715
		private SendOrPostCallback sendMessageCollectivelyOperationCompleted;

		// Token: 0x040002CC RID: 716
		private bool useDefaultCredentialsSetExplicitly;
	}
}
