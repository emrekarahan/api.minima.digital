﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Order
{
	// Token: 0x020000FD RID: 253
	[XmlType(Namespace = "http://order.individual.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[Serializable]
	public class orderServiceListResponse : baseResponse
	{
		// Token: 0x170002E8 RID: 744
		// (get) Token: 0x060008FB RID: 2299 RVA: 0x0000C8AB File Offset: 0x0000AAAB
		// (set) Token: 0x060008FC RID: 2300 RVA: 0x0000C8B3 File Offset: 0x0000AAB3
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public long productCount
		{
			get
			{
				return this.productCountField;
			}
			set
			{
				this.productCountField = value;
			}
		}

		// Token: 0x170002E9 RID: 745
		// (get) Token: 0x060008FD RID: 2301 RVA: 0x0000C8BC File Offset: 0x0000AABC
		// (set) Token: 0x060008FE RID: 2302 RVA: 0x0000C8C4 File Offset: 0x0000AAC4
		[XmlIgnore]
		public bool productCountSpecified
		{
			get
			{
				return this.productCountFieldSpecified;
			}
			set
			{
				this.productCountFieldSpecified = value;
			}
		}

		// Token: 0x170002EA RID: 746
		// (get) Token: 0x060008FF RID: 2303 RVA: 0x0000C8CD File Offset: 0x0000AACD
		// (set) Token: 0x06000900 RID: 2304 RVA: 0x0000C8D5 File Offset: 0x0000AAD5
		[XmlArrayItem("product", Form = XmlSchemaForm.Unqualified)]
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		public orderType[] products
		{
			get
			{
				return this.productsField;
			}
			set
			{
				this.productsField = value;
			}
		}

		// Token: 0x170002EB RID: 747
		// (get) Token: 0x06000901 RID: 2305 RVA: 0x0000C8DE File Offset: 0x0000AADE
		// (set) Token: 0x06000902 RID: 2306 RVA: 0x0000C8E6 File Offset: 0x0000AAE6
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string message
		{
			get
			{
				return this.messageField;
			}
			set
			{
				this.messageField = value;
			}
		}

		// Token: 0x0400035F RID: 863
		private long productCountField;

		// Token: 0x04000360 RID: 864
		private bool productCountFieldSpecified;

		// Token: 0x04000361 RID: 865
		private orderType[] productsField;

		// Token: 0x04000362 RID: 866
		private string messageField;
	}
}
