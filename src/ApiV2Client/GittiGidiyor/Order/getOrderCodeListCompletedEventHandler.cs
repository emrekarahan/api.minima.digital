﻿using System.CodeDom.Compiler;

namespace ApiV2Client.GittiGidiyor.Order
{
	// Token: 0x02000100 RID: 256
	// (Invoke) Token: 0x0600090F RID: 2319
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	public delegate void getOrderCodeListCompletedEventHandler(object sender, getOrderCodeListCompletedEventArgs e);
}
