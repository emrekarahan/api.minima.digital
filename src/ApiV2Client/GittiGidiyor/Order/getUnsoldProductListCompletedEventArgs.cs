﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Order
{
	// Token: 0x02000105 RID: 261
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	public class getUnsoldProductListCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x0600091E RID: 2334 RVA: 0x0000C99B File Offset: 0x0000AB9B
		internal getUnsoldProductListCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x170002F2 RID: 754
		// (get) Token: 0x0600091F RID: 2335 RVA: 0x0000C9AE File Offset: 0x0000ABAE
		public orderServiceListResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (orderServiceListResponse)this.results[0];
			}
		}

		// Token: 0x04000369 RID: 873
		private object[] results;
	}
}
