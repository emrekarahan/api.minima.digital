﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Order
{
	// Token: 0x020000FF RID: 255
	[XmlType(Namespace = "http://order.individual.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[Serializable]
	public class orderServiceMessageResponse : baseResponse
	{
		// Token: 0x170002EF RID: 751
		// (get) Token: 0x0600090B RID: 2315 RVA: 0x0000C932 File Offset: 0x0000AB32
		// (set) Token: 0x0600090C RID: 2316 RVA: 0x0000C93A File Offset: 0x0000AB3A
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string result
		{
			get
			{
				return this.resultField;
			}
			set
			{
				this.resultField = value;
			}
		}

		// Token: 0x04000366 RID: 870
		private string resultField;
	}
}
