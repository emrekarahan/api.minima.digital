﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Order
{
	// Token: 0x02000103 RID: 259
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	public class getServiceNameCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000918 RID: 2328 RVA: 0x0000C973 File Offset: 0x0000AB73
		internal getServiceNameCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x170002F1 RID: 753
		// (get) Token: 0x06000919 RID: 2329 RVA: 0x0000C986 File Offset: 0x0000AB86
		public string Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (string)this.results[0];
			}
		}

		// Token: 0x04000368 RID: 872
		private object[] results;
	}
}
