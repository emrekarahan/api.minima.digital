﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Order
{
	// Token: 0x02000107 RID: 263
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	public class makeRevisionCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000924 RID: 2340 RVA: 0x0000C9C3 File Offset: 0x0000ABC3
		internal makeRevisionCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x170002F3 RID: 755
		// (get) Token: 0x06000925 RID: 2341 RVA: 0x0000C9D6 File Offset: 0x0000ABD6
		public orderServiceResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (orderServiceResponse)this.results[0];
			}
		}

		// Token: 0x0400036A RID: 874
		private object[] results;
	}
}
