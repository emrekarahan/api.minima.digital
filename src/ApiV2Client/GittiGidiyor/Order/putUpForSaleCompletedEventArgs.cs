﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Order
{
	// Token: 0x02000109 RID: 265
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	public class putUpForSaleCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x0600092A RID: 2346 RVA: 0x0000C9EB File Offset: 0x0000ABEB
		internal putUpForSaleCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x170002F4 RID: 756
		// (get) Token: 0x0600092B RID: 2347 RVA: 0x0000C9FE File Offset: 0x0000ABFE
		public orderServiceResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (orderServiceResponse)this.results[0];
			}
		}

		// Token: 0x0400036B RID: 875
		private object[] results;
	}
}
