﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Order
{
	// Token: 0x020000F1 RID: 241
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[DebuggerStepThrough]
	[XmlType(Namespace = "http://order.individual.ws.listingapi.gg.com")]
	[DesignerCategory("code")]
	[Serializable]
	public class variantSpecType
	{
		// Token: 0x17000276 RID: 630
		// (get) Token: 0x0600080B RID: 2059 RVA: 0x0000C0B9 File Offset: 0x0000A2B9
		// (set) Token: 0x0600080C RID: 2060 RVA: 0x0000C0C1 File Offset: 0x0000A2C1
		[XmlAttribute]
		public long nameId
		{
			get
			{
				return this.nameIdField;
			}
			set
			{
				this.nameIdField = value;
			}
		}

		// Token: 0x17000277 RID: 631
		// (get) Token: 0x0600080D RID: 2061 RVA: 0x0000C0CA File Offset: 0x0000A2CA
		// (set) Token: 0x0600080E RID: 2062 RVA: 0x0000C0D2 File Offset: 0x0000A2D2
		[XmlIgnore]
		public bool nameIdSpecified
		{
			get
			{
				return this.nameIdFieldSpecified;
			}
			set
			{
				this.nameIdFieldSpecified = value;
			}
		}

		// Token: 0x17000278 RID: 632
		// (get) Token: 0x0600080F RID: 2063 RVA: 0x0000C0DB File Offset: 0x0000A2DB
		// (set) Token: 0x06000810 RID: 2064 RVA: 0x0000C0E3 File Offset: 0x0000A2E3
		[XmlAttribute]
		public string name
		{
			get
			{
				return this.nameField;
			}
			set
			{
				this.nameField = value;
			}
		}

		// Token: 0x17000279 RID: 633
		// (get) Token: 0x06000811 RID: 2065 RVA: 0x0000C0EC File Offset: 0x0000A2EC
		// (set) Token: 0x06000812 RID: 2066 RVA: 0x0000C0F4 File Offset: 0x0000A2F4
		[XmlAttribute]
		public long valueId
		{
			get
			{
				return this.valueIdField;
			}
			set
			{
				this.valueIdField = value;
			}
		}

		// Token: 0x1700027A RID: 634
		// (get) Token: 0x06000813 RID: 2067 RVA: 0x0000C0FD File Offset: 0x0000A2FD
		// (set) Token: 0x06000814 RID: 2068 RVA: 0x0000C105 File Offset: 0x0000A305
		[XmlIgnore]
		public bool valueIdSpecified
		{
			get
			{
				return this.valueIdFieldSpecified;
			}
			set
			{
				this.valueIdFieldSpecified = value;
			}
		}

		// Token: 0x1700027B RID: 635
		// (get) Token: 0x06000815 RID: 2069 RVA: 0x0000C10E File Offset: 0x0000A30E
		// (set) Token: 0x06000816 RID: 2070 RVA: 0x0000C116 File Offset: 0x0000A316
		[XmlAttribute]
		public string value
		{
			get
			{
				return this.valueField;
			}
			set
			{
				this.valueField = value;
			}
		}

		// Token: 0x1700027C RID: 636
		// (get) Token: 0x06000817 RID: 2071 RVA: 0x0000C11F File Offset: 0x0000A31F
		// (set) Token: 0x06000818 RID: 2072 RVA: 0x0000C127 File Offset: 0x0000A327
		[XmlAttribute]
		public int orderNumber
		{
			get
			{
				return this.orderNumberField;
			}
			set
			{
				this.orderNumberField = value;
			}
		}

		// Token: 0x1700027D RID: 637
		// (get) Token: 0x06000819 RID: 2073 RVA: 0x0000C130 File Offset: 0x0000A330
		// (set) Token: 0x0600081A RID: 2074 RVA: 0x0000C138 File Offset: 0x0000A338
		[XmlIgnore]
		public bool orderNumberSpecified
		{
			get
			{
				return this.orderNumberFieldSpecified;
			}
			set
			{
				this.orderNumberFieldSpecified = value;
			}
		}

		// Token: 0x1700027E RID: 638
		// (get) Token: 0x0600081B RID: 2075 RVA: 0x0000C141 File Offset: 0x0000A341
		// (set) Token: 0x0600081C RID: 2076 RVA: 0x0000C149 File Offset: 0x0000A349
		[XmlAttribute]
		public int specDataOrderNumber
		{
			get
			{
				return this.specDataOrderNumberField;
			}
			set
			{
				this.specDataOrderNumberField = value;
			}
		}

		// Token: 0x1700027F RID: 639
		// (get) Token: 0x0600081D RID: 2077 RVA: 0x0000C152 File Offset: 0x0000A352
		// (set) Token: 0x0600081E RID: 2078 RVA: 0x0000C15A File Offset: 0x0000A35A
		[XmlIgnore]
		public bool specDataOrderNumberSpecified
		{
			get
			{
				return this.specDataOrderNumberFieldSpecified;
			}
			set
			{
				this.specDataOrderNumberFieldSpecified = value;
			}
		}

		// Token: 0x040002ED RID: 749
		private long nameIdField;

		// Token: 0x040002EE RID: 750
		private bool nameIdFieldSpecified;

		// Token: 0x040002EF RID: 751
		private string nameField;

		// Token: 0x040002F0 RID: 752
		private long valueIdField;

		// Token: 0x040002F1 RID: 753
		private bool valueIdFieldSpecified;

		// Token: 0x040002F2 RID: 754
		private string valueField;

		// Token: 0x040002F3 RID: 755
		private int orderNumberField;

		// Token: 0x040002F4 RID: 756
		private bool orderNumberFieldSpecified;

		// Token: 0x040002F5 RID: 757
		private int specDataOrderNumberField;

		// Token: 0x040002F6 RID: 758
		private bool specDataOrderNumberFieldSpecified;
	}
}
