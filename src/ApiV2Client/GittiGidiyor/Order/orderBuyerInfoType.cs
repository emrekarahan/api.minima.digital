﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Order
{
	// Token: 0x020000F0 RID: 240
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[XmlType(Namespace = "http://order.individual.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[Serializable]
	public class orderBuyerInfoType
	{
		// Token: 0x17000268 RID: 616
		// (get) Token: 0x060007EE RID: 2030 RVA: 0x0000BFC3 File Offset: 0x0000A1C3
		// (set) Token: 0x060007EF RID: 2031 RVA: 0x0000BFCB File Offset: 0x0000A1CB
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string username
		{
			get
			{
				return this.usernameField;
			}
			set
			{
				this.usernameField = value;
			}
		}

		// Token: 0x17000269 RID: 617
		// (get) Token: 0x060007F0 RID: 2032 RVA: 0x0000BFD4 File Offset: 0x0000A1D4
		// (set) Token: 0x060007F1 RID: 2033 RVA: 0x0000BFDC File Offset: 0x0000A1DC
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string name
		{
			get
			{
				return this.nameField;
			}
			set
			{
				this.nameField = value;
			}
		}

		// Token: 0x1700026A RID: 618
		// (get) Token: 0x060007F2 RID: 2034 RVA: 0x0000BFE5 File Offset: 0x0000A1E5
		// (set) Token: 0x060007F3 RID: 2035 RVA: 0x0000BFED File Offset: 0x0000A1ED
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string surname
		{
			get
			{
				return this.surnameField;
			}
			set
			{
				this.surnameField = value;
			}
		}

		// Token: 0x1700026B RID: 619
		// (get) Token: 0x060007F4 RID: 2036 RVA: 0x0000BFF6 File Offset: 0x0000A1F6
		// (set) Token: 0x060007F5 RID: 2037 RVA: 0x0000BFFE File Offset: 0x0000A1FE
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string phone
		{
			get
			{
				return this.phoneField;
			}
			set
			{
				this.phoneField = value;
			}
		}

		// Token: 0x1700026C RID: 620
		// (get) Token: 0x060007F6 RID: 2038 RVA: 0x0000C007 File Offset: 0x0000A207
		// (set) Token: 0x060007F7 RID: 2039 RVA: 0x0000C00F File Offset: 0x0000A20F
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string mobilePhone
		{
			get
			{
				return this.mobilePhoneField;
			}
			set
			{
				this.mobilePhoneField = value;
			}
		}

		// Token: 0x1700026D RID: 621
		// (get) Token: 0x060007F8 RID: 2040 RVA: 0x0000C018 File Offset: 0x0000A218
		// (set) Token: 0x060007F9 RID: 2041 RVA: 0x0000C020 File Offset: 0x0000A220
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string email
		{
			get
			{
				return this.emailField;
			}
			set
			{
				this.emailField = value;
			}
		}

		// Token: 0x1700026E RID: 622
		// (get) Token: 0x060007FA RID: 2042 RVA: 0x0000C029 File Offset: 0x0000A229
		// (set) Token: 0x060007FB RID: 2043 RVA: 0x0000C031 File Offset: 0x0000A231
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string address
		{
			get
			{
				return this.addressField;
			}
			set
			{
				this.addressField = value;
			}
		}

		// Token: 0x1700026F RID: 623
		// (get) Token: 0x060007FC RID: 2044 RVA: 0x0000C03A File Offset: 0x0000A23A
		// (set) Token: 0x060007FD RID: 2045 RVA: 0x0000C042 File Offset: 0x0000A242
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string district
		{
			get
			{
				return this.districtField;
			}
			set
			{
				this.districtField = value;
			}
		}

		// Token: 0x17000270 RID: 624
		// (get) Token: 0x060007FE RID: 2046 RVA: 0x0000C04B File Offset: 0x0000A24B
		// (set) Token: 0x060007FF RID: 2047 RVA: 0x0000C053 File Offset: 0x0000A253
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string city
		{
			get
			{
				return this.cityField;
			}
			set
			{
				this.cityField = value;
			}
		}

		// Token: 0x17000271 RID: 625
		// (get) Token: 0x06000800 RID: 2048 RVA: 0x0000C05C File Offset: 0x0000A25C
		// (set) Token: 0x06000801 RID: 2049 RVA: 0x0000C064 File Offset: 0x0000A264
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string zipCode
		{
			get
			{
				return this.zipCodeField;
			}
			set
			{
				this.zipCodeField = value;
			}
		}

		// Token: 0x17000272 RID: 626
		// (get) Token: 0x06000802 RID: 2050 RVA: 0x0000C06D File Offset: 0x0000A26D
		// (set) Token: 0x06000803 RID: 2051 RVA: 0x0000C075 File Offset: 0x0000A275
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int processCount
		{
			get
			{
				return this.processCountField;
			}
			set
			{
				this.processCountField = value;
			}
		}

		// Token: 0x17000273 RID: 627
		// (get) Token: 0x06000804 RID: 2052 RVA: 0x0000C07E File Offset: 0x0000A27E
		// (set) Token: 0x06000805 RID: 2053 RVA: 0x0000C086 File Offset: 0x0000A286
		[XmlIgnore]
		public bool processCountSpecified
		{
			get
			{
				return this.processCountFieldSpecified;
			}
			set
			{
				this.processCountFieldSpecified = value;
			}
		}

		// Token: 0x17000274 RID: 628
		// (get) Token: 0x06000806 RID: 2054 RVA: 0x0000C08F File Offset: 0x0000A28F
		// (set) Token: 0x06000807 RID: 2055 RVA: 0x0000C097 File Offset: 0x0000A297
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int ratePercentage
		{
			get
			{
				return this.ratePercentageField;
			}
			set
			{
				this.ratePercentageField = value;
			}
		}

		// Token: 0x17000275 RID: 629
		// (get) Token: 0x06000808 RID: 2056 RVA: 0x0000C0A0 File Offset: 0x0000A2A0
		// (set) Token: 0x06000809 RID: 2057 RVA: 0x0000C0A8 File Offset: 0x0000A2A8
		[XmlIgnore]
		public bool ratePercentageSpecified
		{
			get
			{
				return this.ratePercentageFieldSpecified;
			}
			set
			{
				this.ratePercentageFieldSpecified = value;
			}
		}

		// Token: 0x040002DF RID: 735
		private string usernameField;

		// Token: 0x040002E0 RID: 736
		private string nameField;

		// Token: 0x040002E1 RID: 737
		private string surnameField;

		// Token: 0x040002E2 RID: 738
		private string phoneField;

		// Token: 0x040002E3 RID: 739
		private string mobilePhoneField;

		// Token: 0x040002E4 RID: 740
		private string emailField;

		// Token: 0x040002E5 RID: 741
		private string addressField;

		// Token: 0x040002E6 RID: 742
		private string districtField;

		// Token: 0x040002E7 RID: 743
		private string cityField;

		// Token: 0x040002E8 RID: 744
		private string zipCodeField;

		// Token: 0x040002E9 RID: 745
		private int processCountField;

		// Token: 0x040002EA RID: 746
		private bool processCountFieldSpecified;

		// Token: 0x040002EB RID: 747
		private int ratePercentageField;

		// Token: 0x040002EC RID: 748
		private bool ratePercentageFieldSpecified;
	}
}
