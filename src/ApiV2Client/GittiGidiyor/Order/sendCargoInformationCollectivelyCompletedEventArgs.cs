﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Order
{
	// Token: 0x0200010D RID: 269
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	[DesignerCategory("code")]
	[DebuggerStepThrough]
	public class sendCargoInformationCollectivelyCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000936 RID: 2358 RVA: 0x0000CA3B File Offset: 0x0000AC3B
		internal sendCargoInformationCollectivelyCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x170002F6 RID: 758
		// (get) Token: 0x06000937 RID: 2359 RVA: 0x0000CA4E File Offset: 0x0000AC4E
		public orderServiceSaleResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (orderServiceSaleResponse)this.results[0];
			}
		}

		// Token: 0x0400036D RID: 877
		private object[] results;
	}
}
