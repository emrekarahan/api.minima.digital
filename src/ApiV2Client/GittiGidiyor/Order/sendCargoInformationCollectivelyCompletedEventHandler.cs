﻿using System.CodeDom.Compiler;

namespace ApiV2Client.GittiGidiyor.Order
{
	// Token: 0x0200010C RID: 268
	// (Invoke) Token: 0x06000933 RID: 2355
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	public delegate void sendCargoInformationCollectivelyCompletedEventHandler(object sender, sendCargoInformationCollectivelyCompletedEventArgs e);
}
