﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Order
{
	// Token: 0x020000F7 RID: 247
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[DesignerCategory("code")]
	[XmlType(Namespace = "http://order.individual.ws.listingapi.gg.com")]
	[DebuggerStepThrough]
	[Serializable]
	public class cargoDetailType
	{
		// Token: 0x1700029C RID: 668
		// (get) Token: 0x0600085D RID: 2141 RVA: 0x0000C36F File Offset: 0x0000A56F
		// (set) Token: 0x0600085E RID: 2142 RVA: 0x0000C377 File Offset: 0x0000A577
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string city
		{
			get
			{
				return this.cityField;
			}
			set
			{
				this.cityField = value;
			}
		}

		// Token: 0x1700029D RID: 669
		// (get) Token: 0x0600085F RID: 2143 RVA: 0x0000C380 File Offset: 0x0000A580
		// (set) Token: 0x06000860 RID: 2144 RVA: 0x0000C388 File Offset: 0x0000A588
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		[XmlArrayItem("cargoCompany", Form = XmlSchemaForm.Unqualified, IsNullable = false)]
		public string[] cargoCompanies
		{
			get
			{
				return this.cargoCompaniesField;
			}
			set
			{
				this.cargoCompaniesField = value;
			}
		}

		// Token: 0x1700029E RID: 670
		// (get) Token: 0x06000861 RID: 2145 RVA: 0x0000C391 File Offset: 0x0000A591
		// (set) Token: 0x06000862 RID: 2146 RVA: 0x0000C399 File Offset: 0x0000A599
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string shippingPayment
		{
			get
			{
				return this.shippingPaymentField;
			}
			set
			{
				this.shippingPaymentField = value;
			}
		}

		// Token: 0x1700029F RID: 671
		// (get) Token: 0x06000863 RID: 2147 RVA: 0x0000C3A2 File Offset: 0x0000A5A2
		// (set) Token: 0x06000864 RID: 2148 RVA: 0x0000C3AA File Offset: 0x0000A5AA
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string cargoDescription
		{
			get
			{
				return this.cargoDescriptionField;
			}
			set
			{
				this.cargoDescriptionField = value;
			}
		}

		// Token: 0x170002A0 RID: 672
		// (get) Token: 0x06000865 RID: 2149 RVA: 0x0000C3B3 File Offset: 0x0000A5B3
		// (set) Token: 0x06000866 RID: 2150 RVA: 0x0000C3BB File Offset: 0x0000A5BB
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string shippingWhere
		{
			get
			{
				return this.shippingWhereField;
			}
			set
			{
				this.shippingWhereField = value;
			}
		}

		// Token: 0x170002A1 RID: 673
		// (get) Token: 0x06000867 RID: 2151 RVA: 0x0000C3C4 File Offset: 0x0000A5C4
		// (set) Token: 0x06000868 RID: 2152 RVA: 0x0000C3CC File Offset: 0x0000A5CC
		[XmlArrayItem("cargoCompanyDetail", Form = XmlSchemaForm.Unqualified, IsNullable = false)]
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		public cargoCompanyDetailType[] cargoCompanyDetails
		{
			get
			{
				return this.cargoCompanyDetailsField;
			}
			set
			{
				this.cargoCompanyDetailsField = value;
			}
		}

		// Token: 0x170002A2 RID: 674
		// (get) Token: 0x06000869 RID: 2153 RVA: 0x0000C3D5 File Offset: 0x0000A5D5
		// (set) Token: 0x0600086A RID: 2154 RVA: 0x0000C3DD File Offset: 0x0000A5DD
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public cargoTimeType shippingTime
		{
			get
			{
				return this.shippingTimeField;
			}
			set
			{
				this.shippingTimeField = value;
			}
		}

		// Token: 0x04000313 RID: 787
		private string cityField;

		// Token: 0x04000314 RID: 788
		private string[] cargoCompaniesField;

		// Token: 0x04000315 RID: 789
		private string shippingPaymentField;

		// Token: 0x04000316 RID: 790
		private string cargoDescriptionField;

		// Token: 0x04000317 RID: 791
		private string shippingWhereField;

		// Token: 0x04000318 RID: 792
		private cargoCompanyDetailType[] cargoCompanyDetailsField;

		// Token: 0x04000319 RID: 793
		private cargoTimeType shippingTimeField;
	}
}
