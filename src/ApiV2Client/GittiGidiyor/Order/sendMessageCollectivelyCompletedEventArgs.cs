﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Order
{
	// Token: 0x0200010F RID: 271
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	public class sendMessageCollectivelyCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x0600093C RID: 2364 RVA: 0x0000CA63 File Offset: 0x0000AC63
		internal sendMessageCollectivelyCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x170002F7 RID: 759
		// (get) Token: 0x0600093D RID: 2365 RVA: 0x0000CA76 File Offset: 0x0000AC76
		public orderServiceMessageResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (orderServiceMessageResponse)this.results[0];
			}
		}

		// Token: 0x0400036E RID: 878
		private object[] results;
	}
}
