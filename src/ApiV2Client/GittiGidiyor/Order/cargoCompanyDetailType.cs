﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Order
{
	// Token: 0x020000F6 RID: 246
	[DebuggerStepThrough]
	[XmlType(Namespace = "http://order.individual.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[DesignerCategory("code")]
	[Serializable]
	public class cargoCompanyDetailType
	{
		// Token: 0x17000298 RID: 664
		// (get) Token: 0x06000854 RID: 2132 RVA: 0x0000C323 File Offset: 0x0000A523
		// (set) Token: 0x06000855 RID: 2133 RVA: 0x0000C32B File Offset: 0x0000A52B
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string name
		{
			get
			{
				return this.nameField;
			}
			set
			{
				this.nameField = value;
			}
		}

		// Token: 0x17000299 RID: 665
		// (get) Token: 0x06000856 RID: 2134 RVA: 0x0000C334 File Offset: 0x0000A534
		// (set) Token: 0x06000857 RID: 2135 RVA: 0x0000C33C File Offset: 0x0000A53C
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string value
		{
			get
			{
				return this.valueField;
			}
			set
			{
				this.valueField = value;
			}
		}

		// Token: 0x1700029A RID: 666
		// (get) Token: 0x06000858 RID: 2136 RVA: 0x0000C345 File Offset: 0x0000A545
		// (set) Token: 0x06000859 RID: 2137 RVA: 0x0000C34D File Offset: 0x0000A54D
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string cityPrice
		{
			get
			{
				return this.cityPriceField;
			}
			set
			{
				this.cityPriceField = value;
			}
		}

		// Token: 0x1700029B RID: 667
		// (get) Token: 0x0600085A RID: 2138 RVA: 0x0000C356 File Offset: 0x0000A556
		// (set) Token: 0x0600085B RID: 2139 RVA: 0x0000C35E File Offset: 0x0000A55E
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string countryPrice
		{
			get
			{
				return this.countryPriceField;
			}
			set
			{
				this.countryPriceField = value;
			}
		}

		// Token: 0x0400030F RID: 783
		private string nameField;

		// Token: 0x04000310 RID: 784
		private string valueField;

		// Token: 0x04000311 RID: 785
		private string cityPriceField;

		// Token: 0x04000312 RID: 786
		private string countryPriceField;
	}
}
