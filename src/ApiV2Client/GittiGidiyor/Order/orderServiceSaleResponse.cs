﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Order
{
	// Token: 0x020000FC RID: 252
	[XmlType(Namespace = "http://order.individual.ws.listingapi.gg.com")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[Serializable]
	public class orderServiceSaleResponse : baseResponse
	{
		// Token: 0x170002E4 RID: 740
		// (get) Token: 0x060008F2 RID: 2290 RVA: 0x0000C85F File Offset: 0x0000AA5F
		// (set) Token: 0x060008F3 RID: 2291 RVA: 0x0000C867 File Offset: 0x0000AA67
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int saleCount
		{
			get
			{
				return this.saleCountField;
			}
			set
			{
				this.saleCountField = value;
			}
		}

		// Token: 0x170002E5 RID: 741
		// (get) Token: 0x060008F4 RID: 2292 RVA: 0x0000C870 File Offset: 0x0000AA70
		// (set) Token: 0x060008F5 RID: 2293 RVA: 0x0000C878 File Offset: 0x0000AA78
		[XmlIgnore]
		public bool saleCountSpecified
		{
			get
			{
				return this.saleCountFieldSpecified;
			}
			set
			{
				this.saleCountFieldSpecified = value;
			}
		}

		// Token: 0x170002E6 RID: 742
		// (get) Token: 0x060008F6 RID: 2294 RVA: 0x0000C881 File Offset: 0x0000AA81
		// (set) Token: 0x060008F7 RID: 2295 RVA: 0x0000C889 File Offset: 0x0000AA89
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		[XmlArrayItem("saleCode", Form = XmlSchemaForm.Unqualified)]
		public string[] saleCodeList
		{
			get
			{
				return this.saleCodeListField;
			}
			set
			{
				this.saleCodeListField = value;
			}
		}

		// Token: 0x170002E7 RID: 743
		// (get) Token: 0x060008F8 RID: 2296 RVA: 0x0000C892 File Offset: 0x0000AA92
		// (set) Token: 0x060008F9 RID: 2297 RVA: 0x0000C89A File Offset: 0x0000AA9A
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string result
		{
			get
			{
				return this.resultField;
			}
			set
			{
				this.resultField = value;
			}
		}

		// Token: 0x0400035B RID: 859
		private int saleCountField;

		// Token: 0x0400035C RID: 860
		private bool saleCountFieldSpecified;

		// Token: 0x0400035D RID: 861
		private string[] saleCodeListField;

		// Token: 0x0400035E RID: 862
		private string resultField;
	}
}
