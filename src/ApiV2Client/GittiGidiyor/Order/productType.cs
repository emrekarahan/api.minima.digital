﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Order
{
	// Token: 0x020000F9 RID: 249
	[XmlType(Namespace = "http://order.individual.ws.listingapi.gg.com")]
	[DesignerCategory("code")]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[DebuggerStepThrough]
	[Serializable]
	public class productType
	{
		// Token: 0x170002A8 RID: 680
		// (get) Token: 0x06000877 RID: 2167 RVA: 0x0000C44B File Offset: 0x0000A64B
		// (set) Token: 0x06000878 RID: 2168 RVA: 0x0000C453 File Offset: 0x0000A653
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string categoryCode
		{
			get
			{
				return this.categoryCodeField;
			}
			set
			{
				this.categoryCodeField = value;
			}
		}

		// Token: 0x170002A9 RID: 681
		// (get) Token: 0x06000879 RID: 2169 RVA: 0x0000C45C File Offset: 0x0000A65C
		// (set) Token: 0x0600087A RID: 2170 RVA: 0x0000C464 File Offset: 0x0000A664
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int storeCategoryId
		{
			get
			{
				return this.storeCategoryIdField;
			}
			set
			{
				this.storeCategoryIdField = value;
			}
		}

		// Token: 0x170002AA RID: 682
		// (get) Token: 0x0600087B RID: 2171 RVA: 0x0000C46D File Offset: 0x0000A66D
		// (set) Token: 0x0600087C RID: 2172 RVA: 0x0000C475 File Offset: 0x0000A675
		[XmlIgnore]
		public bool storeCategoryIdSpecified
		{
			get
			{
				return this.storeCategoryIdFieldSpecified;
			}
			set
			{
				this.storeCategoryIdFieldSpecified = value;
			}
		}

		// Token: 0x170002AB RID: 683
		// (get) Token: 0x0600087D RID: 2173 RVA: 0x0000C47E File Offset: 0x0000A67E
		// (set) Token: 0x0600087E RID: 2174 RVA: 0x0000C486 File Offset: 0x0000A686
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string title
		{
			get
			{
				return this.titleField;
			}
			set
			{
				this.titleField = value;
			}
		}

		// Token: 0x170002AC RID: 684
		// (get) Token: 0x0600087F RID: 2175 RVA: 0x0000C48F File Offset: 0x0000A68F
		// (set) Token: 0x06000880 RID: 2176 RVA: 0x0000C497 File Offset: 0x0000A697
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string subtitle
		{
			get
			{
				return this.subtitleField;
			}
			set
			{
				this.subtitleField = value;
			}
		}

		// Token: 0x170002AD RID: 685
		// (get) Token: 0x06000881 RID: 2177 RVA: 0x0000C4A0 File Offset: 0x0000A6A0
		// (set) Token: 0x06000882 RID: 2178 RVA: 0x0000C4A8 File Offset: 0x0000A6A8
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		[XmlArrayItem("spec", Form = XmlSchemaForm.Unqualified, IsNullable = false)]
		public specType[] specs
		{
			get
			{
				return this.specsField;
			}
			set
			{
				this.specsField = value;
			}
		}

		// Token: 0x170002AE RID: 686
		// (get) Token: 0x06000883 RID: 2179 RVA: 0x0000C4B1 File Offset: 0x0000A6B1
		// (set) Token: 0x06000884 RID: 2180 RVA: 0x0000C4B9 File Offset: 0x0000A6B9
		[XmlArrayItem("photo", Form = XmlSchemaForm.Unqualified, IsNullable = false)]
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		public photoType[] photos
		{
			get
			{
				return this.photosField;
			}
			set
			{
				this.photosField = value;
			}
		}

		// Token: 0x170002AF RID: 687
		// (get) Token: 0x06000885 RID: 2181 RVA: 0x0000C4C2 File Offset: 0x0000A6C2
		// (set) Token: 0x06000886 RID: 2182 RVA: 0x0000C4CA File Offset: 0x0000A6CA
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int pageTemplate
		{
			get
			{
				return this.pageTemplateField;
			}
			set
			{
				this.pageTemplateField = value;
			}
		}

		// Token: 0x170002B0 RID: 688
		// (get) Token: 0x06000887 RID: 2183 RVA: 0x0000C4D3 File Offset: 0x0000A6D3
		// (set) Token: 0x06000888 RID: 2184 RVA: 0x0000C4DB File Offset: 0x0000A6DB
		[XmlIgnore]
		public bool pageTemplateSpecified
		{
			get
			{
				return this.pageTemplateFieldSpecified;
			}
			set
			{
				this.pageTemplateFieldSpecified = value;
			}
		}

		// Token: 0x170002B1 RID: 689
		// (get) Token: 0x06000889 RID: 2185 RVA: 0x0000C4E4 File Offset: 0x0000A6E4
		// (set) Token: 0x0600088A RID: 2186 RVA: 0x0000C4EC File Offset: 0x0000A6EC
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string description
		{
			get
			{
				return this.descriptionField;
			}
			set
			{
				this.descriptionField = value;
			}
		}

		// Token: 0x170002B2 RID: 690
		// (get) Token: 0x0600088B RID: 2187 RVA: 0x0000C4F5 File Offset: 0x0000A6F5
		// (set) Token: 0x0600088C RID: 2188 RVA: 0x0000C4FD File Offset: 0x0000A6FD
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string startDate
		{
			get
			{
				return this.startDateField;
			}
			set
			{
				this.startDateField = value;
			}
		}

		// Token: 0x170002B3 RID: 691
		// (get) Token: 0x0600088D RID: 2189 RVA: 0x0000C506 File Offset: 0x0000A706
		// (set) Token: 0x0600088E RID: 2190 RVA: 0x0000C50E File Offset: 0x0000A70E
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int catalogId
		{
			get
			{
				return this.catalogIdField;
			}
			set
			{
				this.catalogIdField = value;
			}
		}

		// Token: 0x170002B4 RID: 692
		// (get) Token: 0x0600088F RID: 2191 RVA: 0x0000C517 File Offset: 0x0000A717
		// (set) Token: 0x06000890 RID: 2192 RVA: 0x0000C51F File Offset: 0x0000A71F
		[XmlIgnore]
		public bool catalogIdSpecified
		{
			get
			{
				return this.catalogIdFieldSpecified;
			}
			set
			{
				this.catalogIdFieldSpecified = value;
			}
		}

		// Token: 0x170002B5 RID: 693
		// (get) Token: 0x06000891 RID: 2193 RVA: 0x0000C528 File Offset: 0x0000A728
		// (set) Token: 0x06000892 RID: 2194 RVA: 0x0000C530 File Offset: 0x0000A730
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int newCatalogId
		{
			get
			{
				return this.newCatalogIdField;
			}
			set
			{
				this.newCatalogIdField = value;
			}
		}

		// Token: 0x170002B6 RID: 694
		// (get) Token: 0x06000893 RID: 2195 RVA: 0x0000C539 File Offset: 0x0000A739
		// (set) Token: 0x06000894 RID: 2196 RVA: 0x0000C541 File Offset: 0x0000A741
		[XmlIgnore]
		public bool newCatalogIdSpecified
		{
			get
			{
				return this.newCatalogIdFieldSpecified;
			}
			set
			{
				this.newCatalogIdFieldSpecified = value;
			}
		}

		// Token: 0x170002B7 RID: 695
		// (get) Token: 0x06000895 RID: 2197 RVA: 0x0000C54A File Offset: 0x0000A74A
		// (set) Token: 0x06000896 RID: 2198 RVA: 0x0000C552 File Offset: 0x0000A752
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int catalogDetail
		{
			get
			{
				return this.catalogDetailField;
			}
			set
			{
				this.catalogDetailField = value;
			}
		}

		// Token: 0x170002B8 RID: 696
		// (get) Token: 0x06000897 RID: 2199 RVA: 0x0000C55B File Offset: 0x0000A75B
		// (set) Token: 0x06000898 RID: 2200 RVA: 0x0000C563 File Offset: 0x0000A763
		[XmlIgnore]
		public bool catalogDetailSpecified
		{
			get
			{
				return this.catalogDetailFieldSpecified;
			}
			set
			{
				this.catalogDetailFieldSpecified = value;
			}
		}

		// Token: 0x170002B9 RID: 697
		// (get) Token: 0x06000899 RID: 2201 RVA: 0x0000C56C File Offset: 0x0000A76C
		// (set) Token: 0x0600089A RID: 2202 RVA: 0x0000C574 File Offset: 0x0000A774
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string catalogFilter
		{
			get
			{
				return this.catalogFilterField;
			}
			set
			{
				this.catalogFilterField = value;
			}
		}

		// Token: 0x170002BA RID: 698
		// (get) Token: 0x0600089B RID: 2203 RVA: 0x0000C57D File Offset: 0x0000A77D
		// (set) Token: 0x0600089C RID: 2204 RVA: 0x0000C585 File Offset: 0x0000A785
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string format
		{
			get
			{
				return this.formatField;
			}
			set
			{
				this.formatField = value;
			}
		}

		// Token: 0x170002BB RID: 699
		// (get) Token: 0x0600089D RID: 2205 RVA: 0x0000C58E File Offset: 0x0000A78E
		// (set) Token: 0x0600089E RID: 2206 RVA: 0x0000C596 File Offset: 0x0000A796
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public double startPrice
		{
			get
			{
				return this.startPriceField;
			}
			set
			{
				this.startPriceField = value;
			}
		}

		// Token: 0x170002BC RID: 700
		// (get) Token: 0x0600089F RID: 2207 RVA: 0x0000C59F File Offset: 0x0000A79F
		// (set) Token: 0x060008A0 RID: 2208 RVA: 0x0000C5A7 File Offset: 0x0000A7A7
		[XmlIgnore]
		public bool startPriceSpecified
		{
			get
			{
				return this.startPriceFieldSpecified;
			}
			set
			{
				this.startPriceFieldSpecified = value;
			}
		}

		// Token: 0x170002BD RID: 701
		// (get) Token: 0x060008A1 RID: 2209 RVA: 0x0000C5B0 File Offset: 0x0000A7B0
		// (set) Token: 0x060008A2 RID: 2210 RVA: 0x0000C5B8 File Offset: 0x0000A7B8
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public double buyNowPrice
		{
			get
			{
				return this.buyNowPriceField;
			}
			set
			{
				this.buyNowPriceField = value;
			}
		}

		// Token: 0x170002BE RID: 702
		// (get) Token: 0x060008A3 RID: 2211 RVA: 0x0000C5C1 File Offset: 0x0000A7C1
		// (set) Token: 0x060008A4 RID: 2212 RVA: 0x0000C5C9 File Offset: 0x0000A7C9
		[XmlIgnore]
		public bool buyNowPriceSpecified
		{
			get
			{
				return this.buyNowPriceFieldSpecified;
			}
			set
			{
				this.buyNowPriceFieldSpecified = value;
			}
		}

		// Token: 0x170002BF RID: 703
		// (get) Token: 0x060008A5 RID: 2213 RVA: 0x0000C5D2 File Offset: 0x0000A7D2
		// (set) Token: 0x060008A6 RID: 2214 RVA: 0x0000C5DA File Offset: 0x0000A7DA
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public double netEarning
		{
			get
			{
				return this.netEarningField;
			}
			set
			{
				this.netEarningField = value;
			}
		}

		// Token: 0x170002C0 RID: 704
		// (get) Token: 0x060008A7 RID: 2215 RVA: 0x0000C5E3 File Offset: 0x0000A7E3
		// (set) Token: 0x060008A8 RID: 2216 RVA: 0x0000C5EB File Offset: 0x0000A7EB
		[XmlIgnore]
		public bool netEarningSpecified
		{
			get
			{
				return this.netEarningFieldSpecified;
			}
			set
			{
				this.netEarningFieldSpecified = value;
			}
		}

		// Token: 0x170002C1 RID: 705
		// (get) Token: 0x060008A9 RID: 2217 RVA: 0x0000C5F4 File Offset: 0x0000A7F4
		// (set) Token: 0x060008AA RID: 2218 RVA: 0x0000C5FC File Offset: 0x0000A7FC
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int listingDays
		{
			get
			{
				return this.listingDaysField;
			}
			set
			{
				this.listingDaysField = value;
			}
		}

		// Token: 0x170002C2 RID: 706
		// (get) Token: 0x060008AB RID: 2219 RVA: 0x0000C605 File Offset: 0x0000A805
		// (set) Token: 0x060008AC RID: 2220 RVA: 0x0000C60D File Offset: 0x0000A80D
		[XmlIgnore]
		public bool listingDaysSpecified
		{
			get
			{
				return this.listingDaysFieldSpecified;
			}
			set
			{
				this.listingDaysFieldSpecified = value;
			}
		}

		// Token: 0x170002C3 RID: 707
		// (get) Token: 0x060008AD RID: 2221 RVA: 0x0000C616 File Offset: 0x0000A816
		// (set) Token: 0x060008AE RID: 2222 RVA: 0x0000C61E File Offset: 0x0000A81E
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int productCount
		{
			get
			{
				return this.productCountField;
			}
			set
			{
				this.productCountField = value;
			}
		}

		// Token: 0x170002C4 RID: 708
		// (get) Token: 0x060008AF RID: 2223 RVA: 0x0000C627 File Offset: 0x0000A827
		// (set) Token: 0x060008B0 RID: 2224 RVA: 0x0000C62F File Offset: 0x0000A82F
		[XmlIgnore]
		public bool productCountSpecified
		{
			get
			{
				return this.productCountFieldSpecified;
			}
			set
			{
				this.productCountFieldSpecified = value;
			}
		}

		// Token: 0x170002C5 RID: 709
		// (get) Token: 0x060008B1 RID: 2225 RVA: 0x0000C638 File Offset: 0x0000A838
		// (set) Token: 0x060008B2 RID: 2226 RVA: 0x0000C640 File Offset: 0x0000A840
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public cargoDetailType cargoDetail
		{
			get
			{
				return this.cargoDetailField;
			}
			set
			{
				this.cargoDetailField = value;
			}
		}

		// Token: 0x170002C6 RID: 710
		// (get) Token: 0x060008B3 RID: 2227 RVA: 0x0000C649 File Offset: 0x0000A849
		// (set) Token: 0x060008B4 RID: 2228 RVA: 0x0000C651 File Offset: 0x0000A851
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public bool affiliateOption
		{
			get
			{
				return this.affiliateOptionField;
			}
			set
			{
				this.affiliateOptionField = value;
			}
		}

		// Token: 0x170002C7 RID: 711
		// (get) Token: 0x060008B5 RID: 2229 RVA: 0x0000C65A File Offset: 0x0000A85A
		// (set) Token: 0x060008B6 RID: 2230 RVA: 0x0000C662 File Offset: 0x0000A862
		[XmlIgnore]
		public bool affiliateOptionSpecified
		{
			get
			{
				return this.affiliateOptionFieldSpecified;
			}
			set
			{
				this.affiliateOptionFieldSpecified = value;
			}
		}

		// Token: 0x170002C8 RID: 712
		// (get) Token: 0x060008B7 RID: 2231 RVA: 0x0000C66B File Offset: 0x0000A86B
		// (set) Token: 0x060008B8 RID: 2232 RVA: 0x0000C673 File Offset: 0x0000A873
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public bool boldOption
		{
			get
			{
				return this.boldOptionField;
			}
			set
			{
				this.boldOptionField = value;
			}
		}

		// Token: 0x170002C9 RID: 713
		// (get) Token: 0x060008B9 RID: 2233 RVA: 0x0000C67C File Offset: 0x0000A87C
		// (set) Token: 0x060008BA RID: 2234 RVA: 0x0000C684 File Offset: 0x0000A884
		[XmlIgnore]
		public bool boldOptionSpecified
		{
			get
			{
				return this.boldOptionFieldSpecified;
			}
			set
			{
				this.boldOptionFieldSpecified = value;
			}
		}

		// Token: 0x170002CA RID: 714
		// (get) Token: 0x060008BB RID: 2235 RVA: 0x0000C68D File Offset: 0x0000A88D
		// (set) Token: 0x060008BC RID: 2236 RVA: 0x0000C695 File Offset: 0x0000A895
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public bool catalogOption
		{
			get
			{
				return this.catalogOptionField;
			}
			set
			{
				this.catalogOptionField = value;
			}
		}

		// Token: 0x170002CB RID: 715
		// (get) Token: 0x060008BD RID: 2237 RVA: 0x0000C69E File Offset: 0x0000A89E
		// (set) Token: 0x060008BE RID: 2238 RVA: 0x0000C6A6 File Offset: 0x0000A8A6
		[XmlIgnore]
		public bool catalogOptionSpecified
		{
			get
			{
				return this.catalogOptionFieldSpecified;
			}
			set
			{
				this.catalogOptionFieldSpecified = value;
			}
		}

		// Token: 0x170002CC RID: 716
		// (get) Token: 0x060008BF RID: 2239 RVA: 0x0000C6AF File Offset: 0x0000A8AF
		// (set) Token: 0x060008C0 RID: 2240 RVA: 0x0000C6B7 File Offset: 0x0000A8B7
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public bool vitrineOption
		{
			get
			{
				return this.vitrineOptionField;
			}
			set
			{
				this.vitrineOptionField = value;
			}
		}

		// Token: 0x170002CD RID: 717
		// (get) Token: 0x060008C1 RID: 2241 RVA: 0x0000C6C0 File Offset: 0x0000A8C0
		// (set) Token: 0x060008C2 RID: 2242 RVA: 0x0000C6C8 File Offset: 0x0000A8C8
		[XmlIgnore]
		public bool vitrineOptionSpecified
		{
			get
			{
				return this.vitrineOptionFieldSpecified;
			}
			set
			{
				this.vitrineOptionFieldSpecified = value;
			}
		}

		// Token: 0x170002CE RID: 718
		// (get) Token: 0x060008C3 RID: 2243 RVA: 0x0000C6D1 File Offset: 0x0000A8D1
		// (set) Token: 0x060008C4 RID: 2244 RVA: 0x0000C6D9 File Offset: 0x0000A8D9
		[XmlArrayItem("variantGroup", Form = XmlSchemaForm.Unqualified, IsNullable = false)]
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		public variantGroupType[] variantGroups
		{
			get
			{
				return this.variantGroupsField;
			}
			set
			{
				this.variantGroupsField = value;
			}
		}

		// Token: 0x170002CF RID: 719
		// (get) Token: 0x060008C5 RID: 2245 RVA: 0x0000C6E2 File Offset: 0x0000A8E2
		// (set) Token: 0x060008C6 RID: 2246 RVA: 0x0000C6EA File Offset: 0x0000A8EA
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int auctionProfilePercentage
		{
			get
			{
				return this.auctionProfilePercentageField;
			}
			set
			{
				this.auctionProfilePercentageField = value;
			}
		}

		// Token: 0x170002D0 RID: 720
		// (get) Token: 0x060008C7 RID: 2247 RVA: 0x0000C6F3 File Offset: 0x0000A8F3
		// (set) Token: 0x060008C8 RID: 2248 RVA: 0x0000C6FB File Offset: 0x0000A8FB
		[XmlIgnore]
		public bool auctionProfilePercentageSpecified
		{
			get
			{
				return this.auctionProfilePercentageFieldSpecified;
			}
			set
			{
				this.auctionProfilePercentageFieldSpecified = value;
			}
		}

		// Token: 0x170002D1 RID: 721
		// (get) Token: 0x060008C9 RID: 2249 RVA: 0x0000C704 File Offset: 0x0000A904
		// (set) Token: 0x060008CA RID: 2250 RVA: 0x0000C70C File Offset: 0x0000A90C
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public double marketPrice
		{
			get
			{
				return this.marketPriceField;
			}
			set
			{
				this.marketPriceField = value;
			}
		}

		// Token: 0x170002D2 RID: 722
		// (get) Token: 0x060008CB RID: 2251 RVA: 0x0000C715 File Offset: 0x0000A915
		// (set) Token: 0x060008CC RID: 2252 RVA: 0x0000C71D File Offset: 0x0000A91D
		[XmlIgnore]
		public bool marketPriceSpecified
		{
			get
			{
				return this.marketPriceFieldSpecified;
			}
			set
			{
				this.marketPriceFieldSpecified = value;
			}
		}

		// Token: 0x170002D3 RID: 723
		// (get) Token: 0x060008CD RID: 2253 RVA: 0x0000C726 File Offset: 0x0000A926
		// (set) Token: 0x060008CE RID: 2254 RVA: 0x0000C72E File Offset: 0x0000A92E
		[XmlElement(Form = XmlSchemaForm.Unqualified, DataType = "integer")]
		public string globalTradeItemNo
		{
			get
			{
				return this.globalTradeItemNoField;
			}
			set
			{
				this.globalTradeItemNoField = value;
			}
		}

		// Token: 0x170002D4 RID: 724
		// (get) Token: 0x060008CF RID: 2255 RVA: 0x0000C737 File Offset: 0x0000A937
		// (set) Token: 0x060008D0 RID: 2256 RVA: 0x0000C73F File Offset: 0x0000A93F
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string manufacturerPartNo
		{
			get
			{
				return this.manufacturerPartNoField;
			}
			set
			{
				this.manufacturerPartNoField = value;
			}
		}

		// Token: 0x0400031F RID: 799
		private string categoryCodeField;

		// Token: 0x04000320 RID: 800
		private int storeCategoryIdField;

		// Token: 0x04000321 RID: 801
		private bool storeCategoryIdFieldSpecified;

		// Token: 0x04000322 RID: 802
		private string titleField;

		// Token: 0x04000323 RID: 803
		private string subtitleField;

		// Token: 0x04000324 RID: 804
		private specType[] specsField;

		// Token: 0x04000325 RID: 805
		private photoType[] photosField;

		// Token: 0x04000326 RID: 806
		private int pageTemplateField;

		// Token: 0x04000327 RID: 807
		private bool pageTemplateFieldSpecified;

		// Token: 0x04000328 RID: 808
		private string descriptionField;

		// Token: 0x04000329 RID: 809
		private string startDateField;

		// Token: 0x0400032A RID: 810
		private int catalogIdField;

		// Token: 0x0400032B RID: 811
		private bool catalogIdFieldSpecified;

		// Token: 0x0400032C RID: 812
		private int newCatalogIdField;

		// Token: 0x0400032D RID: 813
		private bool newCatalogIdFieldSpecified;

		// Token: 0x0400032E RID: 814
		private int catalogDetailField;

		// Token: 0x0400032F RID: 815
		private bool catalogDetailFieldSpecified;

		// Token: 0x04000330 RID: 816
		private string catalogFilterField;

		// Token: 0x04000331 RID: 817
		private string formatField;

		// Token: 0x04000332 RID: 818
		private double startPriceField;

		// Token: 0x04000333 RID: 819
		private bool startPriceFieldSpecified;

		// Token: 0x04000334 RID: 820
		private double buyNowPriceField;

		// Token: 0x04000335 RID: 821
		private bool buyNowPriceFieldSpecified;

		// Token: 0x04000336 RID: 822
		private double netEarningField;

		// Token: 0x04000337 RID: 823
		private bool netEarningFieldSpecified;

		// Token: 0x04000338 RID: 824
		private int listingDaysField;

		// Token: 0x04000339 RID: 825
		private bool listingDaysFieldSpecified;

		// Token: 0x0400033A RID: 826
		private int productCountField;

		// Token: 0x0400033B RID: 827
		private bool productCountFieldSpecified;

		// Token: 0x0400033C RID: 828
		private cargoDetailType cargoDetailField;

		// Token: 0x0400033D RID: 829
		private bool affiliateOptionField;

		// Token: 0x0400033E RID: 830
		private bool affiliateOptionFieldSpecified;

		// Token: 0x0400033F RID: 831
		private bool boldOptionField;

		// Token: 0x04000340 RID: 832
		private bool boldOptionFieldSpecified;

		// Token: 0x04000341 RID: 833
		private bool catalogOptionField;

		// Token: 0x04000342 RID: 834
		private bool catalogOptionFieldSpecified;

		// Token: 0x04000343 RID: 835
		private bool vitrineOptionField;

		// Token: 0x04000344 RID: 836
		private bool vitrineOptionFieldSpecified;

		// Token: 0x04000345 RID: 837
		private variantGroupType[] variantGroupsField;

		// Token: 0x04000346 RID: 838
		private int auctionProfilePercentageField;

		// Token: 0x04000347 RID: 839
		private bool auctionProfilePercentageFieldSpecified;

		// Token: 0x04000348 RID: 840
		private double marketPriceField;

		// Token: 0x04000349 RID: 841
		private bool marketPriceFieldSpecified;

		// Token: 0x0400034A RID: 842
		private string globalTradeItemNoField;

		// Token: 0x0400034B RID: 843
		private string manufacturerPartNoField;
	}
}
