﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Order
{
	// Token: 0x020000ED RID: 237
	[DebuggerStepThrough]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[XmlInclude(typeof(orderServiceDetailResponse))]
	[DesignerCategory("code")]
	[XmlType(Namespace = "http://order.individual.ws.listingapi.gg.com")]
	[XmlInclude(typeof(orderServiceListResponse))]
	[XmlInclude(typeof(orderServiceSaleResponse))]
	[XmlInclude(typeof(orderServiceResponse))]
	[XmlInclude(typeof(orderServiceMessageResponse))]
	[Serializable]
	public class baseResponse
	{
		// Token: 0x1700025E RID: 606
		// (get) Token: 0x060007D7 RID: 2007 RVA: 0x0000BF01 File Offset: 0x0000A101
		// (set) Token: 0x060007D8 RID: 2008 RVA: 0x0000BF09 File Offset: 0x0000A109
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string ackCode
		{
			get
			{
				return this.ackCodeField;
			}
			set
			{
				this.ackCodeField = value;
			}
		}

		// Token: 0x1700025F RID: 607
		// (get) Token: 0x060007D9 RID: 2009 RVA: 0x0000BF12 File Offset: 0x0000A112
		// (set) Token: 0x060007DA RID: 2010 RVA: 0x0000BF1A File Offset: 0x0000A11A
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string responseTime
		{
			get
			{
				return this.responseTimeField;
			}
			set
			{
				this.responseTimeField = value;
			}
		}

		// Token: 0x17000260 RID: 608
		// (get) Token: 0x060007DB RID: 2011 RVA: 0x0000BF23 File Offset: 0x0000A123
		// (set) Token: 0x060007DC RID: 2012 RVA: 0x0000BF2B File Offset: 0x0000A12B
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public errorType error
		{
			get
			{
				return this.errorField;
			}
			set
			{
				this.errorField = value;
			}
		}

		// Token: 0x17000261 RID: 609
		// (get) Token: 0x060007DD RID: 2013 RVA: 0x0000BF34 File Offset: 0x0000A134
		// (set) Token: 0x060007DE RID: 2014 RVA: 0x0000BF3C File Offset: 0x0000A13C
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string timeElapsed
		{
			get
			{
				return this.timeElapsedField;
			}
			set
			{
				this.timeElapsedField = value;
			}
		}

		// Token: 0x040002D5 RID: 725
		private string ackCodeField;

		// Token: 0x040002D6 RID: 726
		private string responseTimeField;

		// Token: 0x040002D7 RID: 727
		private errorType errorField;

		// Token: 0x040002D8 RID: 728
		private string timeElapsedField;
	}
}
