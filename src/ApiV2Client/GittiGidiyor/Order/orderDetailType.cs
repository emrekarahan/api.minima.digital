﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Order
{
	// Token: 0x020000EF RID: 239
	[DesignerCategory("code")]
	[XmlType(Namespace = "http://order.individual.ws.listingapi.gg.com")]
	[DebuggerStepThrough]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[Serializable]
	public class orderDetailType
	{
		// Token: 0x17000265 RID: 613
		// (get) Token: 0x060007E7 RID: 2023 RVA: 0x0000BF88 File Offset: 0x0000A188
		// (set) Token: 0x060007E8 RID: 2024 RVA: 0x0000BF90 File Offset: 0x0000A190
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string orderCode
		{
			get
			{
				return this.orderCodeField;
			}
			set
			{
				this.orderCodeField = value;
			}
		}

		// Token: 0x17000266 RID: 614
		// (get) Token: 0x060007E9 RID: 2025 RVA: 0x0000BF99 File Offset: 0x0000A199
		// (set) Token: 0x060007EA RID: 2026 RVA: 0x0000BFA1 File Offset: 0x0000A1A1
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public orderBuyerInfoType buyerInfo
		{
			get
			{
				return this.buyerInfoField;
			}
			set
			{
				this.buyerInfoField = value;
			}
		}

		// Token: 0x17000267 RID: 615
		// (get) Token: 0x060007EB RID: 2027 RVA: 0x0000BFAA File Offset: 0x0000A1AA
		// (set) Token: 0x060007EC RID: 2028 RVA: 0x0000BFB2 File Offset: 0x0000A1B2
		[XmlElement("item", Form = XmlSchemaForm.Unqualified)]
		public orderType[] item
		{
			get
			{
				return this.itemField;
			}
			set
			{
				this.itemField = value;
			}
		}

		// Token: 0x040002DC RID: 732
		private string orderCodeField;

		// Token: 0x040002DD RID: 733
		private orderBuyerInfoType buyerInfoField;

		// Token: 0x040002DE RID: 734
		private orderType[] itemField;
	}
}
