﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Order
{
	// Token: 0x020000FE RID: 254
	[XmlType(Namespace = "http://order.individual.ws.listingapi.gg.com")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[Serializable]
	public class orderServiceResponse : baseResponse
	{
		// Token: 0x170002EC RID: 748
		// (get) Token: 0x06000904 RID: 2308 RVA: 0x0000C8F7 File Offset: 0x0000AAF7
		// (set) Token: 0x06000905 RID: 2309 RVA: 0x0000C8FF File Offset: 0x0000AAFF
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string itemId
		{
			get
			{
				return this.itemIdField;
			}
			set
			{
				this.itemIdField = value;
			}
		}

		// Token: 0x170002ED RID: 749
		// (get) Token: 0x06000906 RID: 2310 RVA: 0x0000C908 File Offset: 0x0000AB08
		// (set) Token: 0x06000907 RID: 2311 RVA: 0x0000C910 File Offset: 0x0000AB10
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string productId
		{
			get
			{
				return this.productIdField;
			}
			set
			{
				this.productIdField = value;
			}
		}

		// Token: 0x170002EE RID: 750
		// (get) Token: 0x06000908 RID: 2312 RVA: 0x0000C919 File Offset: 0x0000AB19
		// (set) Token: 0x06000909 RID: 2313 RVA: 0x0000C921 File Offset: 0x0000AB21
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string result
		{
			get
			{
				return this.resultField;
			}
			set
			{
				this.resultField = value;
			}
		}

		// Token: 0x04000363 RID: 867
		private string itemIdField;

		// Token: 0x04000364 RID: 868
		private string productIdField;

		// Token: 0x04000365 RID: 869
		private string resultField;
	}
}
