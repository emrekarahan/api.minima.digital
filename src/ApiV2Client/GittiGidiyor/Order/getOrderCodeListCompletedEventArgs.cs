﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Order
{
	// Token: 0x02000101 RID: 257
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	public class getOrderCodeListCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000912 RID: 2322 RVA: 0x0000C94B File Offset: 0x0000AB4B
		internal getOrderCodeListCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x170002F0 RID: 752
		// (get) Token: 0x06000913 RID: 2323 RVA: 0x0000C95E File Offset: 0x0000AB5E
		public orderServiceDetailResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (orderServiceDetailResponse)this.results[0];
			}
		}

		// Token: 0x04000367 RID: 871
		private object[] results;
	}
}
