﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Order
{
	// Token: 0x020000F5 RID: 245
	[DesignerCategory("code")]
	[DebuggerStepThrough]
	[XmlType(Namespace = "http://order.individual.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[Serializable]
	public class cargoTimeType
	{
		// Token: 0x17000296 RID: 662
		// (get) Token: 0x0600084F RID: 2127 RVA: 0x0000C2F9 File Offset: 0x0000A4F9
		// (set) Token: 0x06000850 RID: 2128 RVA: 0x0000C301 File Offset: 0x0000A501
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string days
		{
			get
			{
				return this.daysField;
			}
			set
			{
				this.daysField = value;
			}
		}

		// Token: 0x17000297 RID: 663
		// (get) Token: 0x06000851 RID: 2129 RVA: 0x0000C30A File Offset: 0x0000A50A
		// (set) Token: 0x06000852 RID: 2130 RVA: 0x0000C312 File Offset: 0x0000A512
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string beforeTime
		{
			get
			{
				return this.beforeTimeField;
			}
			set
			{
				this.beforeTimeField = value;
			}
		}

		// Token: 0x0400030D RID: 781
		private string daysField;

		// Token: 0x0400030E RID: 782
		private string beforeTimeField;
	}
}
