﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Order
{
	// Token: 0x020000FB RID: 251
	[DesignerCategory("code")]
	[XmlType(Namespace = "http://order.individual.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[DebuggerStepThrough]
	[Serializable]
	public class errorType
	{
		// Token: 0x170002E0 RID: 736
		// (get) Token: 0x060008E9 RID: 2281 RVA: 0x0000C813 File Offset: 0x0000AA13
		// (set) Token: 0x060008EA RID: 2282 RVA: 0x0000C81B File Offset: 0x0000AA1B
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string errorId
		{
			get
			{
				return this.errorIdField;
			}
			set
			{
				this.errorIdField = value;
			}
		}

		// Token: 0x170002E1 RID: 737
		// (get) Token: 0x060008EB RID: 2283 RVA: 0x0000C824 File Offset: 0x0000AA24
		// (set) Token: 0x060008EC RID: 2284 RVA: 0x0000C82C File Offset: 0x0000AA2C
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string errorCode
		{
			get
			{
				return this.errorCodeField;
			}
			set
			{
				this.errorCodeField = value;
			}
		}

		// Token: 0x170002E2 RID: 738
		// (get) Token: 0x060008ED RID: 2285 RVA: 0x0000C835 File Offset: 0x0000AA35
		// (set) Token: 0x060008EE RID: 2286 RVA: 0x0000C83D File Offset: 0x0000AA3D
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string message
		{
			get
			{
				return this.messageField;
			}
			set
			{
				this.messageField = value;
			}
		}

		// Token: 0x170002E3 RID: 739
		// (get) Token: 0x060008EF RID: 2287 RVA: 0x0000C846 File Offset: 0x0000AA46
		// (set) Token: 0x060008F0 RID: 2288 RVA: 0x0000C84E File Offset: 0x0000AA4E
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string viewMessage
		{
			get
			{
				return this.viewMessageField;
			}
			set
			{
				this.viewMessageField = value;
			}
		}

		// Token: 0x04000357 RID: 855
		private string errorIdField;

		// Token: 0x04000358 RID: 856
		private string errorCodeField;

		// Token: 0x04000359 RID: 857
		private string messageField;

		// Token: 0x0400035A RID: 858
		private string viewMessageField;
	}
}
