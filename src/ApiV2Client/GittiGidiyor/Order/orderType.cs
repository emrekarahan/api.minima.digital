﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Order
{
	// Token: 0x020000FA RID: 250
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[DesignerCategory("code")]
	[DebuggerStepThrough]
	[XmlType(Namespace = "http://order.individual.ws.listingapi.gg.com")]
	[Serializable]
	public class orderType
	{
		// Token: 0x170002D5 RID: 725
		// (get) Token: 0x060008D2 RID: 2258 RVA: 0x0000C750 File Offset: 0x0000A950
		// (set) Token: 0x060008D3 RID: 2259 RVA: 0x0000C758 File Offset: 0x0000A958
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int productId
		{
			get
			{
				return this.productIdField;
			}
			set
			{
				this.productIdField = value;
			}
		}

		// Token: 0x170002D6 RID: 726
		// (get) Token: 0x060008D4 RID: 2260 RVA: 0x0000C761 File Offset: 0x0000A961
		// (set) Token: 0x060008D5 RID: 2261 RVA: 0x0000C769 File Offset: 0x0000A969
		[XmlIgnore]
		public bool productIdSpecified
		{
			get
			{
				return this.productIdFieldSpecified;
			}
			set
			{
				this.productIdFieldSpecified = value;
			}
		}

		// Token: 0x170002D7 RID: 727
		// (get) Token: 0x060008D6 RID: 2262 RVA: 0x0000C772 File Offset: 0x0000A972
		// (set) Token: 0x060008D7 RID: 2263 RVA: 0x0000C77A File Offset: 0x0000A97A
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string itemId
		{
			get
			{
				return this.itemIdField;
			}
			set
			{
				this.itemIdField = value;
			}
		}

		// Token: 0x170002D8 RID: 728
		// (get) Token: 0x060008D8 RID: 2264 RVA: 0x0000C783 File Offset: 0x0000A983
		// (set) Token: 0x060008D9 RID: 2265 RVA: 0x0000C78B File Offset: 0x0000A98B
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string saleCode
		{
			get
			{
				return this.saleCodeField;
			}
			set
			{
				this.saleCodeField = value;
			}
		}

		// Token: 0x170002D9 RID: 729
		// (get) Token: 0x060008DA RID: 2266 RVA: 0x0000C794 File Offset: 0x0000A994
		// (set) Token: 0x060008DB RID: 2267 RVA: 0x0000C79C File Offset: 0x0000A99C
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public double price
		{
			get
			{
				return this.priceField;
			}
			set
			{
				this.priceField = value;
			}
		}

		// Token: 0x170002DA RID: 730
		// (get) Token: 0x060008DC RID: 2268 RVA: 0x0000C7A5 File Offset: 0x0000A9A5
		// (set) Token: 0x060008DD RID: 2269 RVA: 0x0000C7AD File Offset: 0x0000A9AD
		[XmlIgnore]
		public bool priceSpecified
		{
			get
			{
				return this.priceFieldSpecified;
			}
			set
			{
				this.priceFieldSpecified = value;
			}
		}

		// Token: 0x170002DB RID: 731
		// (get) Token: 0x060008DE RID: 2270 RVA: 0x0000C7B6 File Offset: 0x0000A9B6
		// (set) Token: 0x060008DF RID: 2271 RVA: 0x0000C7BE File Offset: 0x0000A9BE
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int amount
		{
			get
			{
				return this.amountField;
			}
			set
			{
				this.amountField = value;
			}
		}

		// Token: 0x170002DC RID: 732
		// (get) Token: 0x060008E0 RID: 2272 RVA: 0x0000C7C7 File Offset: 0x0000A9C7
		// (set) Token: 0x060008E1 RID: 2273 RVA: 0x0000C7CF File Offset: 0x0000A9CF
		[XmlIgnore]
		public bool amountSpecified
		{
			get
			{
				return this.amountFieldSpecified;
			}
			set
			{
				this.amountFieldSpecified = value;
			}
		}

		// Token: 0x170002DD RID: 733
		// (get) Token: 0x060008E2 RID: 2274 RVA: 0x0000C7D8 File Offset: 0x0000A9D8
		// (set) Token: 0x060008E3 RID: 2275 RVA: 0x0000C7E0 File Offset: 0x0000A9E0
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int statusCode
		{
			get
			{
				return this.statusCodeField;
			}
			set
			{
				this.statusCodeField = value;
			}
		}

		// Token: 0x170002DE RID: 734
		// (get) Token: 0x060008E4 RID: 2276 RVA: 0x0000C7E9 File Offset: 0x0000A9E9
		// (set) Token: 0x060008E5 RID: 2277 RVA: 0x0000C7F1 File Offset: 0x0000A9F1
		[XmlIgnore]
		public bool statusCodeSpecified
		{
			get
			{
				return this.statusCodeFieldSpecified;
			}
			set
			{
				this.statusCodeFieldSpecified = value;
			}
		}

		// Token: 0x170002DF RID: 735
		// (get) Token: 0x060008E6 RID: 2278 RVA: 0x0000C7FA File Offset: 0x0000A9FA
		// (set) Token: 0x060008E7 RID: 2279 RVA: 0x0000C802 File Offset: 0x0000AA02
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string status
		{
			get
			{
				return this.statusField;
			}
			set
			{
				this.statusField = value;
			}
		}

		// Token: 0x0400034C RID: 844
		private int productIdField;

		// Token: 0x0400034D RID: 845
		private bool productIdFieldSpecified;

		// Token: 0x0400034E RID: 846
		private string itemIdField;

		// Token: 0x0400034F RID: 847
		private string saleCodeField;

		// Token: 0x04000350 RID: 848
		private double priceField;

		// Token: 0x04000351 RID: 849
		private bool priceFieldSpecified;

		// Token: 0x04000352 RID: 850
		private int amountField;

		// Token: 0x04000353 RID: 851
		private bool amountFieldSpecified;

		// Token: 0x04000354 RID: 852
		private int statusCodeField;

		// Token: 0x04000355 RID: 853
		private bool statusCodeFieldSpecified;

		// Token: 0x04000356 RID: 854
		private string statusField;
	}
}
