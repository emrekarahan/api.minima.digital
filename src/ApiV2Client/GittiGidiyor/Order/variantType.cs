﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Order
{
	// Token: 0x020000F2 RID: 242
	[DesignerCategory("code")]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[XmlType(Namespace = "http://order.individual.ws.listingapi.gg.com")]
	[DebuggerStepThrough]
	[Serializable]
	public class variantType
	{
		// Token: 0x17000280 RID: 640
		// (get) Token: 0x06000820 RID: 2080 RVA: 0x0000C16B File Offset: 0x0000A36B
		// (set) Token: 0x06000821 RID: 2081 RVA: 0x0000C173 File Offset: 0x0000A373
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		[XmlArrayItem("variantSpec", Form = XmlSchemaForm.Unqualified, IsNullable = false)]
		public variantSpecType[] variantSpecs
		{
			get
			{
				return this.variantSpecsField;
			}
			set
			{
				this.variantSpecsField = value;
			}
		}

		// Token: 0x17000281 RID: 641
		// (get) Token: 0x06000822 RID: 2082 RVA: 0x0000C17C File Offset: 0x0000A37C
		// (set) Token: 0x06000823 RID: 2083 RVA: 0x0000C184 File Offset: 0x0000A384
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int quantity
		{
			get
			{
				return this.quantityField;
			}
			set
			{
				this.quantityField = value;
			}
		}

		// Token: 0x17000282 RID: 642
		// (get) Token: 0x06000824 RID: 2084 RVA: 0x0000C18D File Offset: 0x0000A38D
		// (set) Token: 0x06000825 RID: 2085 RVA: 0x0000C195 File Offset: 0x0000A395
		[XmlIgnore]
		public bool quantitySpecified
		{
			get
			{
				return this.quantityFieldSpecified;
			}
			set
			{
				this.quantityFieldSpecified = value;
			}
		}

		// Token: 0x17000283 RID: 643
		// (get) Token: 0x06000826 RID: 2086 RVA: 0x0000C19E File Offset: 0x0000A39E
		// (set) Token: 0x06000827 RID: 2087 RVA: 0x0000C1A6 File Offset: 0x0000A3A6
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string stockCode
		{
			get
			{
				return this.stockCodeField;
			}
			set
			{
				this.stockCodeField = value;
			}
		}

		// Token: 0x17000284 RID: 644
		// (get) Token: 0x06000828 RID: 2088 RVA: 0x0000C1AF File Offset: 0x0000A3AF
		// (set) Token: 0x06000829 RID: 2089 RVA: 0x0000C1B7 File Offset: 0x0000A3B7
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int soldCount
		{
			get
			{
				return this.soldCountField;
			}
			set
			{
				this.soldCountField = value;
			}
		}

		// Token: 0x17000285 RID: 645
		// (get) Token: 0x0600082A RID: 2090 RVA: 0x0000C1C0 File Offset: 0x0000A3C0
		// (set) Token: 0x0600082B RID: 2091 RVA: 0x0000C1C8 File Offset: 0x0000A3C8
		[XmlIgnore]
		public bool soldCountSpecified
		{
			get
			{
				return this.soldCountFieldSpecified;
			}
			set
			{
				this.soldCountFieldSpecified = value;
			}
		}

		// Token: 0x17000286 RID: 646
		// (get) Token: 0x0600082C RID: 2092 RVA: 0x0000C1D1 File Offset: 0x0000A3D1
		// (set) Token: 0x0600082D RID: 2093 RVA: 0x0000C1D9 File Offset: 0x0000A3D9
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int newCatalogId
		{
			get
			{
				return this.newCatalogIdField;
			}
			set
			{
				this.newCatalogIdField = value;
			}
		}

		// Token: 0x17000287 RID: 647
		// (get) Token: 0x0600082E RID: 2094 RVA: 0x0000C1E2 File Offset: 0x0000A3E2
		// (set) Token: 0x0600082F RID: 2095 RVA: 0x0000C1EA File Offset: 0x0000A3EA
		[XmlIgnore]
		public bool newCatalogIdSpecified
		{
			get
			{
				return this.newCatalogIdFieldSpecified;
			}
			set
			{
				this.newCatalogIdFieldSpecified = value;
			}
		}

		// Token: 0x17000288 RID: 648
		// (get) Token: 0x06000830 RID: 2096 RVA: 0x0000C1F3 File Offset: 0x0000A3F3
		// (set) Token: 0x06000831 RID: 2097 RVA: 0x0000C1FB File Offset: 0x0000A3FB
		[XmlAttribute]
		public long variantId
		{
			get
			{
				return this.variantIdField;
			}
			set
			{
				this.variantIdField = value;
			}
		}

		// Token: 0x17000289 RID: 649
		// (get) Token: 0x06000832 RID: 2098 RVA: 0x0000C204 File Offset: 0x0000A404
		// (set) Token: 0x06000833 RID: 2099 RVA: 0x0000C20C File Offset: 0x0000A40C
		[XmlIgnore]
		public bool variantIdSpecified
		{
			get
			{
				return this.variantIdFieldSpecified;
			}
			set
			{
				this.variantIdFieldSpecified = value;
			}
		}

		// Token: 0x1700028A RID: 650
		// (get) Token: 0x06000834 RID: 2100 RVA: 0x0000C215 File Offset: 0x0000A415
		// (set) Token: 0x06000835 RID: 2101 RVA: 0x0000C21D File Offset: 0x0000A41D
		[XmlAttribute]
		public string operation
		{
			get
			{
				return this.operationField;
			}
			set
			{
				this.operationField = value;
			}
		}

		// Token: 0x040002F7 RID: 759
		private variantSpecType[] variantSpecsField;

		// Token: 0x040002F8 RID: 760
		private int quantityField;

		// Token: 0x040002F9 RID: 761
		private bool quantityFieldSpecified;

		// Token: 0x040002FA RID: 762
		private string stockCodeField;

		// Token: 0x040002FB RID: 763
		private int soldCountField;

		// Token: 0x040002FC RID: 764
		private bool soldCountFieldSpecified;

		// Token: 0x040002FD RID: 765
		private int newCatalogIdField;

		// Token: 0x040002FE RID: 766
		private bool newCatalogIdFieldSpecified;

		// Token: 0x040002FF RID: 767
		private long variantIdField;

		// Token: 0x04000300 RID: 768
		private bool variantIdFieldSpecified;

		// Token: 0x04000301 RID: 769
		private string operationField;
	}
}
