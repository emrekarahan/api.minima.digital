﻿using System.CodeDom.Compiler;

namespace ApiV2Client.GittiGidiyor.Order
{
	// Token: 0x02000106 RID: 262
	// (Invoke) Token: 0x06000921 RID: 2337
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	public delegate void makeRevisionCompletedEventHandler(object sender, makeRevisionCompletedEventArgs e);
}
