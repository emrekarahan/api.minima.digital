﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Order
{
	// Token: 0x0200010B RID: 267
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	public class relistProductsCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000930 RID: 2352 RVA: 0x0000CA13 File Offset: 0x0000AC13
		internal relistProductsCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x170002F5 RID: 757
		// (get) Token: 0x06000931 RID: 2353 RVA: 0x0000CA26 File Offset: 0x0000AC26
		public orderServiceMessageResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (orderServiceMessageResponse)this.results[0];
			}
		}

		// Token: 0x0400036C RID: 876
		private object[] results;
	}
}
