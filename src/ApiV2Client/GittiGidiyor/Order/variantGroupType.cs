﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Order
{
	// Token: 0x020000F3 RID: 243
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[DesignerCategory("code")]
	[XmlType(Namespace = "http://order.individual.ws.listingapi.gg.com")]
	[DebuggerStepThrough]
	[Serializable]
	public class variantGroupType
	{
		// Token: 0x1700028B RID: 651
		// (get) Token: 0x06000837 RID: 2103 RVA: 0x0000C22E File Offset: 0x0000A42E
		// (set) Token: 0x06000838 RID: 2104 RVA: 0x0000C236 File Offset: 0x0000A436
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		[XmlArrayItem("variant", Form = XmlSchemaForm.Unqualified, IsNullable = false)]
		public variantType[] variants
		{
			get
			{
				return this.variantsField;
			}
			set
			{
				this.variantsField = value;
			}
		}

		// Token: 0x1700028C RID: 652
		// (get) Token: 0x06000839 RID: 2105 RVA: 0x0000C23F File Offset: 0x0000A43F
		// (set) Token: 0x0600083A RID: 2106 RVA: 0x0000C247 File Offset: 0x0000A447
		[XmlArrayItem("photo", Form = XmlSchemaForm.Unqualified, IsNullable = false)]
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		public photoType[] photos
		{
			get
			{
				return this.photosField;
			}
			set
			{
				this.photosField = value;
			}
		}

		// Token: 0x1700028D RID: 653
		// (get) Token: 0x0600083B RID: 2107 RVA: 0x0000C250 File Offset: 0x0000A450
		// (set) Token: 0x0600083C RID: 2108 RVA: 0x0000C258 File Offset: 0x0000A458
		[XmlAttribute]
		public long nameId
		{
			get
			{
				return this.nameIdField;
			}
			set
			{
				this.nameIdField = value;
			}
		}

		// Token: 0x1700028E RID: 654
		// (get) Token: 0x0600083D RID: 2109 RVA: 0x0000C261 File Offset: 0x0000A461
		// (set) Token: 0x0600083E RID: 2110 RVA: 0x0000C269 File Offset: 0x0000A469
		[XmlIgnore]
		public bool nameIdSpecified
		{
			get
			{
				return this.nameIdFieldSpecified;
			}
			set
			{
				this.nameIdFieldSpecified = value;
			}
		}

		// Token: 0x1700028F RID: 655
		// (get) Token: 0x0600083F RID: 2111 RVA: 0x0000C272 File Offset: 0x0000A472
		// (set) Token: 0x06000840 RID: 2112 RVA: 0x0000C27A File Offset: 0x0000A47A
		[XmlAttribute]
		public long valueId
		{
			get
			{
				return this.valueIdField;
			}
			set
			{
				this.valueIdField = value;
			}
		}

		// Token: 0x17000290 RID: 656
		// (get) Token: 0x06000841 RID: 2113 RVA: 0x0000C283 File Offset: 0x0000A483
		// (set) Token: 0x06000842 RID: 2114 RVA: 0x0000C28B File Offset: 0x0000A48B
		[XmlIgnore]
		public bool valueIdSpecified
		{
			get
			{
				return this.valueIdFieldSpecified;
			}
			set
			{
				this.valueIdFieldSpecified = value;
			}
		}

		// Token: 0x17000291 RID: 657
		// (get) Token: 0x06000843 RID: 2115 RVA: 0x0000C294 File Offset: 0x0000A494
		// (set) Token: 0x06000844 RID: 2116 RVA: 0x0000C29C File Offset: 0x0000A49C
		[XmlAttribute]
		public string alias
		{
			get
			{
				return this.aliasField;
			}
			set
			{
				this.aliasField = value;
			}
		}

		// Token: 0x04000302 RID: 770
		private variantType[] variantsField;

		// Token: 0x04000303 RID: 771
		private photoType[] photosField;

		// Token: 0x04000304 RID: 772
		private long nameIdField;

		// Token: 0x04000305 RID: 773
		private bool nameIdFieldSpecified;

		// Token: 0x04000306 RID: 774
		private long valueIdField;

		// Token: 0x04000307 RID: 775
		private bool valueIdFieldSpecified;

		// Token: 0x04000308 RID: 776
		private string aliasField;
	}
}
