﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Order
{
	// Token: 0x020000EE RID: 238
	[DebuggerStepThrough]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[XmlType(Namespace = "http://order.individual.ws.listingapi.gg.com")]
	[DesignerCategory("code")]
	[Serializable]
	public class orderServiceDetailResponse : baseResponse
	{
		// Token: 0x17000262 RID: 610
		// (get) Token: 0x060007E0 RID: 2016 RVA: 0x0000BF4D File Offset: 0x0000A14D
		// (set) Token: 0x060007E1 RID: 2017 RVA: 0x0000BF55 File Offset: 0x0000A155
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public long orderCount
		{
			get
			{
				return this.orderCountField;
			}
			set
			{
				this.orderCountField = value;
			}
		}

		// Token: 0x17000263 RID: 611
		// (get) Token: 0x060007E2 RID: 2018 RVA: 0x0000BF5E File Offset: 0x0000A15E
		// (set) Token: 0x060007E3 RID: 2019 RVA: 0x0000BF66 File Offset: 0x0000A166
		[XmlIgnore]
		public bool orderCountSpecified
		{
			get
			{
				return this.orderCountFieldSpecified;
			}
			set
			{
				this.orderCountFieldSpecified = value;
			}
		}

		// Token: 0x17000264 RID: 612
		// (get) Token: 0x060007E4 RID: 2020 RVA: 0x0000BF6F File Offset: 0x0000A16F
		// (set) Token: 0x060007E5 RID: 2021 RVA: 0x0000BF77 File Offset: 0x0000A177
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		[XmlArrayItem("order", Form = XmlSchemaForm.Unqualified)]
		public orderDetailType[] orderList
		{
			get
			{
				return this.orderListField;
			}
			set
			{
				this.orderListField = value;
			}
		}

		// Token: 0x040002D9 RID: 729
		private long orderCountField;

		// Token: 0x040002DA RID: 730
		private bool orderCountFieldSpecified;

		// Token: 0x040002DB RID: 731
		private orderDetailType[] orderListField;
	}
}
