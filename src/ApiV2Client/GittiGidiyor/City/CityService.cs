﻿namespace ApiV2Client.GittiGidiyor.City
{
	// Token: 0x020001D8 RID: 472
	public class CityService : ServiceClient<CityServiceService>, ICityService, IService
	{
		// Token: 0x0600102D RID: 4141 RVA: 0x0001586C File Offset: 0x00013A6C
		private CityService()
		{
			base.configureNetworkCredentials(this.service);
		}

		// Token: 0x0600102E RID: 4142 RVA: 0x0001588B File Offset: 0x00013A8B
		public cityServiceResponse getCities(int startOffset, int rowCount, string lang)
		{
			return this.service.getCities(startOffset, rowCount, lang);
		}

		// Token: 0x0600102F RID: 4143 RVA: 0x0001589B File Offset: 0x00013A9B
		public cityServiceResponse getCity(int trCode, string lang)
		{
			return this.service.getCity(trCode, lang);
		}

		// Token: 0x06001030 RID: 4144 RVA: 0x000158AA File Offset: 0x00013AAA
		public cityServiceAuditResponse getModifiedCities(long changeTime, int startOffset, int rowCount, string lang)
		{
			return this.service.getModifiedCities(changeTime, startOffset, rowCount, lang);
		}

		// Token: 0x06001031 RID: 4145 RVA: 0x000158BC File Offset: 0x00013ABC
		public string getServiceName()
		{
			return this.service.getServiceName();
		}

		// Token: 0x17000511 RID: 1297
		// (get) Token: 0x06001032 RID: 4146 RVA: 0x000158CC File Offset: 0x00013ACC
		public static CityService Instance
		{
			get
			{
				if (CityService.instance == null)
				{
					lock (CityService.lockObject)
					{
						if (CityService.instance == null)
						{
							CityService.instance = new CityService();
						}
					}
				}
				return CityService.instance;
			}
		}

		// Token: 0x04000604 RID: 1540
		private static CityService instance;

		// Token: 0x04000605 RID: 1541
		private static object lockObject = new object();

		// Token: 0x04000606 RID: 1542
		private CityServiceService service = new CityServiceService();
	}
}
