﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.City
{
	// Token: 0x020000C9 RID: 201
	[DesignerCategory("code")]
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	[DebuggerStepThrough]
	public class getOrderedCitiesCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x060006B4 RID: 1716 RVA: 0x00009BAB File Offset: 0x00007DAB
		internal getOrderedCitiesCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x17000226 RID: 550
		// (get) Token: 0x060006B5 RID: 1717 RVA: 0x00009BBE File Offset: 0x00007DBE
		public cityServiceResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (cityServiceResponse)this.results[0];
			}
		}

		// Token: 0x0400027A RID: 634
		private object[] results;
	}
}
