﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.City
{
	// Token: 0x020000B9 RID: 185
	[DebuggerStepThrough]
	[XmlType(Namespace = "http://city.anonymous.ws.listingapi.gg.com")]
	[XmlInclude(typeof(cityServiceResponse))]
	[DesignerCategory("code")]
	[XmlInclude(typeof(cityServiceAuditResponse))]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[Serializable]
	public class baseResponse
	{
		// Token: 0x1700020C RID: 524
		// (get) Token: 0x06000665 RID: 1637 RVA: 0x0000995D File Offset: 0x00007B5D
		// (set) Token: 0x06000666 RID: 1638 RVA: 0x00009965 File Offset: 0x00007B65
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string ackCode
		{
			get
			{
				return this.ackCodeField;
			}
			set
			{
				this.ackCodeField = value;
			}
		}

		// Token: 0x1700020D RID: 525
		// (get) Token: 0x06000667 RID: 1639 RVA: 0x0000996E File Offset: 0x00007B6E
		// (set) Token: 0x06000668 RID: 1640 RVA: 0x00009976 File Offset: 0x00007B76
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string responseTime
		{
			get
			{
				return this.responseTimeField;
			}
			set
			{
				this.responseTimeField = value;
			}
		}

		// Token: 0x1700020E RID: 526
		// (get) Token: 0x06000669 RID: 1641 RVA: 0x0000997F File Offset: 0x00007B7F
		// (set) Token: 0x0600066A RID: 1642 RVA: 0x00009987 File Offset: 0x00007B87
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public errorType error
		{
			get
			{
				return this.errorField;
			}
			set
			{
				this.errorField = value;
			}
		}

		// Token: 0x1700020F RID: 527
		// (get) Token: 0x0600066B RID: 1643 RVA: 0x00009990 File Offset: 0x00007B90
		// (set) Token: 0x0600066C RID: 1644 RVA: 0x00009998 File Offset: 0x00007B98
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string timeElapsed
		{
			get
			{
				return this.timeElapsedField;
			}
			set
			{
				this.timeElapsedField = value;
			}
		}

		// Token: 0x04000260 RID: 608
		private string ackCodeField;

		// Token: 0x04000261 RID: 609
		private string responseTimeField;

		// Token: 0x04000262 RID: 610
		private errorType errorField;

		// Token: 0x04000263 RID: 611
		private string timeElapsedField;
	}
}
