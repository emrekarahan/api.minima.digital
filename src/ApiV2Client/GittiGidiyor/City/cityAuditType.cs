﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.City
{
	// Token: 0x020000BD RID: 189
	[DesignerCategory("code")]
	[XmlType(Namespace = "http://city.anonymous.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[DebuggerStepThrough]
	[Serializable]
	public class cityAuditType : baseAuditType
	{
		// Token: 0x17000217 RID: 535
		// (get) Token: 0x0600067F RID: 1663 RVA: 0x00009A38 File Offset: 0x00007C38
		// (set) Token: 0x06000680 RID: 1664 RVA: 0x00009A40 File Offset: 0x00007C40
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int trCode
		{
			get
			{
				return this.trCodeField;
			}
			set
			{
				this.trCodeField = value;
			}
		}

		// Token: 0x17000218 RID: 536
		// (get) Token: 0x06000681 RID: 1665 RVA: 0x00009A49 File Offset: 0x00007C49
		// (set) Token: 0x06000682 RID: 1666 RVA: 0x00009A51 File Offset: 0x00007C51
		[XmlIgnore]
		public bool trCodeSpecified
		{
			get
			{
				return this.trCodeFieldSpecified;
			}
			set
			{
				this.trCodeFieldSpecified = value;
			}
		}

		// Token: 0x0400026B RID: 619
		private int trCodeField;

		// Token: 0x0400026C RID: 620
		private bool trCodeFieldSpecified;
	}
}
