﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.City
{
	// Token: 0x020000BB RID: 187
	[DebuggerStepThrough]
	[XmlType(Namespace = "http://city.anonymous.ws.listingapi.gg.com")]
	[DesignerCategory("code")]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[Serializable]
	public class cityType
	{
		// Token: 0x17000213 RID: 531
		// (get) Token: 0x06000675 RID: 1653 RVA: 0x000099E4 File Offset: 0x00007BE4
		// (set) Token: 0x06000676 RID: 1654 RVA: 0x000099EC File Offset: 0x00007BEC
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int trCode
		{
			get
			{
				return this.trCodeField;
			}
			set
			{
				this.trCodeField = value;
			}
		}

		// Token: 0x17000214 RID: 532
		// (get) Token: 0x06000677 RID: 1655 RVA: 0x000099F5 File Offset: 0x00007BF5
		// (set) Token: 0x06000678 RID: 1656 RVA: 0x000099FD File Offset: 0x00007BFD
		[XmlIgnore]
		public bool trCodeSpecified
		{
			get
			{
				return this.trCodeFieldSpecified;
			}
			set
			{
				this.trCodeFieldSpecified = value;
			}
		}

		// Token: 0x17000215 RID: 533
		// (get) Token: 0x06000679 RID: 1657 RVA: 0x00009A06 File Offset: 0x00007C06
		// (set) Token: 0x0600067A RID: 1658 RVA: 0x00009A0E File Offset: 0x00007C0E
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string cityName
		{
			get
			{
				return this.cityNameField;
			}
			set
			{
				this.cityNameField = value;
			}
		}

		// Token: 0x04000267 RID: 615
		private int trCodeField;

		// Token: 0x04000268 RID: 616
		private bool trCodeFieldSpecified;

		// Token: 0x04000269 RID: 617
		private string cityNameField;
	}
}
