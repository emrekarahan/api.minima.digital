﻿using System.CodeDom.Compiler;

namespace ApiV2Client.GittiGidiyor.City
{
	// Token: 0x020000C2 RID: 194
	// (Invoke) Token: 0x0600069F RID: 1695
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	public delegate void getCitiesByCodesCompletedEventHandler(object sender, getCitiesByCodesCompletedEventArgs e);
}
