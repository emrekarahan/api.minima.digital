﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.City
{
	// Token: 0x020000BC RID: 188
	[XmlInclude(typeof(cityAuditType))]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[XmlType(Namespace = "http://city.anonymous.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[Serializable]
	public abstract class baseAuditType
	{
		// Token: 0x17000216 RID: 534
		// (get) Token: 0x0600067C RID: 1660 RVA: 0x00009A1F File Offset: 0x00007C1F
		// (set) Token: 0x0600067D RID: 1661 RVA: 0x00009A27 File Offset: 0x00007C27
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string changeType
		{
			get
			{
				return this.changeTypeField;
			}
			set
			{
				this.changeTypeField = value;
			}
		}

		// Token: 0x0400026A RID: 618
		private string changeTypeField;
	}
}
