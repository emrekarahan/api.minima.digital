﻿namespace ApiV2Client.GittiGidiyor.City
{
	// Token: 0x020001D7 RID: 471
	public interface ICityService : IService
	{
		// Token: 0x0600102A RID: 4138
		cityServiceResponse getCities(int startOffset, int rowCount, string lang);

		// Token: 0x0600102B RID: 4139
		cityServiceResponse getCity(int trCode, string lang);

		// Token: 0x0600102C RID: 4140
		cityServiceAuditResponse getModifiedCities(long changeTime, int startOffset, int rowCount, string lang);
	}
}
