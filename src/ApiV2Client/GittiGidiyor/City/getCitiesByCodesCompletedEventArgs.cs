﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.City
{
	// Token: 0x020000C3 RID: 195
	[DesignerCategory("code")]
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	[DebuggerStepThrough]
	public class getCitiesByCodesCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x060006A2 RID: 1698 RVA: 0x00009B33 File Offset: 0x00007D33
		internal getCitiesByCodesCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x17000223 RID: 547
		// (get) Token: 0x060006A3 RID: 1699 RVA: 0x00009B46 File Offset: 0x00007D46
		public cityServiceResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (cityServiceResponse)this.results[0];
			}
		}

		// Token: 0x04000277 RID: 631
		private object[] results;
	}
}
