﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.City
{
	// Token: 0x020000C7 RID: 199
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	public class getModifiedCitiesCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x060006AE RID: 1710 RVA: 0x00009B83 File Offset: 0x00007D83
		internal getModifiedCitiesCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x17000225 RID: 549
		// (get) Token: 0x060006AF RID: 1711 RVA: 0x00009B96 File Offset: 0x00007D96
		public cityServiceAuditResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (cityServiceAuditResponse)this.results[0];
			}
		}

		// Token: 0x04000279 RID: 633
		private object[] results;
	}
}
