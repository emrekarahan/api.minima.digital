﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.City
{
	// Token: 0x020000C5 RID: 197
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	public class getCityCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x060006A8 RID: 1704 RVA: 0x00009B5B File Offset: 0x00007D5B
		internal getCityCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x17000224 RID: 548
		// (get) Token: 0x060006A9 RID: 1705 RVA: 0x00009B6E File Offset: 0x00007D6E
		public cityServiceResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (cityServiceResponse)this.results[0];
			}
		}

		// Token: 0x04000278 RID: 632
		private object[] results;
	}
}
