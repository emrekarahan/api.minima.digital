﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading;
using System.Web.Services;
using System.Web.Services.Description;
using System.Web.Services.Protocols;
using System.Xml.Schema;
using System.Xml.Serialization;
using ApiV2Client.Properties;

namespace ApiV2Client.GittiGidiyor.City
{
	// Token: 0x020000B8 RID: 184
	[DebuggerStepThrough]
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	[DesignerCategory("code")]
	[WebServiceBinding(Name = "CityServiceBinding", Namespace = "http://city.anonymous.ws.listingapi.gg.com")]
	[XmlInclude(typeof(baseAuditType))]
	[XmlInclude(typeof(baseResponse))]
	public class CityServiceService : SoapHttpClientProtocol
	{
		// Token: 0x0600063A RID: 1594 RVA: 0x000090CE File Offset: 0x000072CE
		public CityServiceService()
		{
			this.Url = Settings.Default.ApiV2Client_GittiGidiyor_City_CityServiceService;
			if (this.IsLocalFileSystemWebService(this.Url))
			{
				this.UseDefaultCredentials = true;
				this.useDefaultCredentialsSetExplicitly = false;
				return;
			}
			this.useDefaultCredentialsSetExplicitly = true;
		}

		// Token: 0x1700020A RID: 522
		// (get) Token: 0x0600063B RID: 1595 RVA: 0x0000910A File Offset: 0x0000730A
		// (set) Token: 0x0600063C RID: 1596 RVA: 0x00009112 File Offset: 0x00007312
		public new string Url
		{
			get
			{
				return base.Url;
			}
			set
			{
				if (this.IsLocalFileSystemWebService(base.Url) && !this.useDefaultCredentialsSetExplicitly && !this.IsLocalFileSystemWebService(value))
				{
					base.UseDefaultCredentials = false;
				}
				base.Url = value;
			}
		}

		// Token: 0x1700020B RID: 523
		// (get) Token: 0x0600063D RID: 1597 RVA: 0x00009141 File Offset: 0x00007341
		// (set) Token: 0x0600063E RID: 1598 RVA: 0x00009149 File Offset: 0x00007349
		public new bool UseDefaultCredentials
		{
			get
			{
				return base.UseDefaultCredentials;
			}
			set
			{
				base.UseDefaultCredentials = value;
				this.useDefaultCredentialsSetExplicitly = true;
			}
		}

		// Token: 0x1400002E RID: 46
		// (add) Token: 0x0600063F RID: 1599 RVA: 0x0000915C File Offset: 0x0000735C
		// (remove) Token: 0x06000640 RID: 1600 RVA: 0x00009194 File Offset: 0x00007394
		public event getCitiesCompletedEventHandler getCitiesCompleted;

		// Token: 0x1400002F RID: 47
		// (add) Token: 0x06000641 RID: 1601 RVA: 0x000091CC File Offset: 0x000073CC
		// (remove) Token: 0x06000642 RID: 1602 RVA: 0x00009204 File Offset: 0x00007404
		public event getCitiesByCodesCompletedEventHandler getCitiesByCodesCompleted;

		// Token: 0x14000030 RID: 48
		// (add) Token: 0x06000643 RID: 1603 RVA: 0x0000923C File Offset: 0x0000743C
		// (remove) Token: 0x06000644 RID: 1604 RVA: 0x00009274 File Offset: 0x00007474
		public event getCityCompletedEventHandler getCityCompleted;

		// Token: 0x14000031 RID: 49
		// (add) Token: 0x06000645 RID: 1605 RVA: 0x000092AC File Offset: 0x000074AC
		// (remove) Token: 0x06000646 RID: 1606 RVA: 0x000092E4 File Offset: 0x000074E4
		public event getModifiedCitiesCompletedEventHandler getModifiedCitiesCompleted;

		// Token: 0x14000032 RID: 50
		// (add) Token: 0x06000647 RID: 1607 RVA: 0x0000931C File Offset: 0x0000751C
		// (remove) Token: 0x06000648 RID: 1608 RVA: 0x00009354 File Offset: 0x00007554
		public event getOrderedCitiesCompletedEventHandler getOrderedCitiesCompleted;

		// Token: 0x14000033 RID: 51
		// (add) Token: 0x06000649 RID: 1609 RVA: 0x0000938C File Offset: 0x0000758C
		// (remove) Token: 0x0600064A RID: 1610 RVA: 0x000093C4 File Offset: 0x000075C4
		public event getServiceNameCompletedEventHandler getServiceNameCompleted;

		// Token: 0x0600064B RID: 1611 RVA: 0x000093FC File Offset: 0x000075FC
		[SoapRpcMethod("", RequestNamespace = "http://city.anonymous.ws.listingapi.gg.com", ResponseNamespace = "http://city.anonymous.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public cityServiceResponse getCities(int startOffSet, int rowCount, string lang)
		{
			object[] array = base.Invoke("getCities", new object[]
			{
				startOffSet,
				rowCount,
				lang
			});
			return (cityServiceResponse)array[0];
		}

		// Token: 0x0600064C RID: 1612 RVA: 0x0000943B File Offset: 0x0000763B
		public void getCitiesAsync(int startOffSet, int rowCount, string lang)
		{
			this.getCitiesAsync(startOffSet, rowCount, lang, null);
		}

		// Token: 0x0600064D RID: 1613 RVA: 0x00009448 File Offset: 0x00007648
		public void getCitiesAsync(int startOffSet, int rowCount, string lang, object userState)
		{
			if (this.getCitiesOperationCompleted == null)
			{
				this.getCitiesOperationCompleted = new SendOrPostCallback(this.OngetCitiesOperationCompleted);
			}
			base.InvokeAsync("getCities", new object[]
			{
				startOffSet,
				rowCount,
				lang
			}, this.getCitiesOperationCompleted, userState);
		}

		// Token: 0x0600064E RID: 1614 RVA: 0x000094A0 File Offset: 0x000076A0
		private void OngetCitiesOperationCompleted(object arg)
		{
			if (this.getCitiesCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getCitiesCompleted(this, new getCitiesCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x0600064F RID: 1615 RVA: 0x000094E8 File Offset: 0x000076E8
		[SoapRpcMethod("", RequestNamespace = "http://city.anonymous.ws.listingapi.gg.com", ResponseNamespace = "http://city.anonymous.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public cityServiceResponse getCitiesByCodes([XmlArrayItem("item", Form = XmlSchemaForm.Unqualified)] int?[] codes, string lang)
		{
			object[] array = base.Invoke("getCitiesByCodes", new object[]
			{
				codes,
				lang
			});
			return (cityServiceResponse)array[0];
		}

		// Token: 0x06000650 RID: 1616 RVA: 0x00009519 File Offset: 0x00007719
		public void getCitiesByCodesAsync(int?[] codes, string lang)
		{
			this.getCitiesByCodesAsync(codes, lang, null);
		}

		// Token: 0x06000651 RID: 1617 RVA: 0x00009524 File Offset: 0x00007724
		public void getCitiesByCodesAsync(int?[] codes, string lang, object userState)
		{
			if (this.getCitiesByCodesOperationCompleted == null)
			{
				this.getCitiesByCodesOperationCompleted = new SendOrPostCallback(this.OngetCitiesByCodesOperationCompleted);
			}
			base.InvokeAsync("getCitiesByCodes", new object[]
			{
				codes,
				lang
			}, this.getCitiesByCodesOperationCompleted, userState);
		}

		// Token: 0x06000652 RID: 1618 RVA: 0x00009570 File Offset: 0x00007770
		private void OngetCitiesByCodesOperationCompleted(object arg)
		{
			if (this.getCitiesByCodesCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getCitiesByCodesCompleted(this, new getCitiesByCodesCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x06000653 RID: 1619 RVA: 0x000095B8 File Offset: 0x000077B8
		[SoapRpcMethod("", RequestNamespace = "http://city.anonymous.ws.listingapi.gg.com", ResponseNamespace = "http://city.anonymous.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public cityServiceResponse getCity(int code, string lang)
		{
			object[] array = base.Invoke("getCity", new object[]
			{
				code,
				lang
			});
			return (cityServiceResponse)array[0];
		}

		// Token: 0x06000654 RID: 1620 RVA: 0x000095EE File Offset: 0x000077EE
		public void getCityAsync(int code, string lang)
		{
			this.getCityAsync(code, lang, null);
		}

		// Token: 0x06000655 RID: 1621 RVA: 0x000095FC File Offset: 0x000077FC
		public void getCityAsync(int code, string lang, object userState)
		{
			if (this.getCityOperationCompleted == null)
			{
				this.getCityOperationCompleted = new SendOrPostCallback(this.OngetCityOperationCompleted);
			}
			base.InvokeAsync("getCity", new object[]
			{
				code,
				lang
			}, this.getCityOperationCompleted, userState);
		}

		// Token: 0x06000656 RID: 1622 RVA: 0x0000964C File Offset: 0x0000784C
		private void OngetCityOperationCompleted(object arg)
		{
			if (this.getCityCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getCityCompleted(this, new getCityCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x06000657 RID: 1623 RVA: 0x00009694 File Offset: 0x00007894
		[SoapRpcMethod("", RequestNamespace = "http://city.anonymous.ws.listingapi.gg.com", ResponseNamespace = "http://city.anonymous.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public cityServiceAuditResponse getModifiedCities(long changeTime, int startOffSet, int rowCount, string lang)
		{
			object[] array = base.Invoke("getModifiedCities", new object[]
			{
				changeTime,
				startOffSet,
				rowCount,
				lang
			});
			return (cityServiceAuditResponse)array[0];
		}

		// Token: 0x06000658 RID: 1624 RVA: 0x000096DD File Offset: 0x000078DD
		public void getModifiedCitiesAsync(long changeTime, int startOffSet, int rowCount, string lang)
		{
			this.getModifiedCitiesAsync(changeTime, startOffSet, rowCount, lang, null);
		}

		// Token: 0x06000659 RID: 1625 RVA: 0x000096EC File Offset: 0x000078EC
		public void getModifiedCitiesAsync(long changeTime, int startOffSet, int rowCount, string lang, object userState)
		{
			if (this.getModifiedCitiesOperationCompleted == null)
			{
				this.getModifiedCitiesOperationCompleted = new SendOrPostCallback(this.OngetModifiedCitiesOperationCompleted);
			}
			base.InvokeAsync("getModifiedCities", new object[]
			{
				changeTime,
				startOffSet,
				rowCount,
				lang
			}, this.getModifiedCitiesOperationCompleted, userState);
		}

		// Token: 0x0600065A RID: 1626 RVA: 0x00009750 File Offset: 0x00007950
		private void OngetModifiedCitiesOperationCompleted(object arg)
		{
			if (this.getModifiedCitiesCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getModifiedCitiesCompleted(this, new getModifiedCitiesCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x0600065B RID: 1627 RVA: 0x00009798 File Offset: 0x00007998
		[SoapRpcMethod("", RequestNamespace = "http://city.anonymous.ws.listingapi.gg.com", ResponseNamespace = "http://city.anonymous.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public cityServiceResponse getOrderedCities(string lang)
		{
			object[] array = base.Invoke("getOrderedCities", new object[]
			{
				lang
			});
			return (cityServiceResponse)array[0];
		}

		// Token: 0x0600065C RID: 1628 RVA: 0x000097C5 File Offset: 0x000079C5
		public void getOrderedCitiesAsync(string lang)
		{
			this.getOrderedCitiesAsync(lang, null);
		}

		// Token: 0x0600065D RID: 1629 RVA: 0x000097D0 File Offset: 0x000079D0
		public void getOrderedCitiesAsync(string lang, object userState)
		{
			if (this.getOrderedCitiesOperationCompleted == null)
			{
				this.getOrderedCitiesOperationCompleted = new SendOrPostCallback(this.OngetOrderedCitiesOperationCompleted);
			}
			base.InvokeAsync("getOrderedCities", new object[]
			{
				lang
			}, this.getOrderedCitiesOperationCompleted, userState);
		}

		// Token: 0x0600065E RID: 1630 RVA: 0x00009818 File Offset: 0x00007A18
		private void OngetOrderedCitiesOperationCompleted(object arg)
		{
			if (this.getOrderedCitiesCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getOrderedCitiesCompleted(this, new getOrderedCitiesCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x0600065F RID: 1631 RVA: 0x00009860 File Offset: 0x00007A60
		[SoapRpcMethod("", RequestNamespace = "http://city.anonymous.ws.listingapi.gg.com", ResponseNamespace = "http://city.anonymous.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public string getServiceName()
		{
			object[] array = base.Invoke("getServiceName", new object[0]);
			return (string)array[0];
		}

		// Token: 0x06000660 RID: 1632 RVA: 0x00009887 File Offset: 0x00007A87
		public void getServiceNameAsync()
		{
			this.getServiceNameAsync(null);
		}

		// Token: 0x06000661 RID: 1633 RVA: 0x00009890 File Offset: 0x00007A90
		public void getServiceNameAsync(object userState)
		{
			if (this.getServiceNameOperationCompleted == null)
			{
				this.getServiceNameOperationCompleted = new SendOrPostCallback(this.OngetServiceNameOperationCompleted);
			}
			base.InvokeAsync("getServiceName", new object[0], this.getServiceNameOperationCompleted, userState);
		}

		// Token: 0x06000662 RID: 1634 RVA: 0x000098C4 File Offset: 0x00007AC4
		private void OngetServiceNameOperationCompleted(object arg)
		{
			if (this.getServiceNameCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getServiceNameCompleted(this, new getServiceNameCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x06000663 RID: 1635 RVA: 0x00009909 File Offset: 0x00007B09
		public new void CancelAsync(object userState)
		{
			base.CancelAsync(userState);
		}

		// Token: 0x06000664 RID: 1636 RVA: 0x00009914 File Offset: 0x00007B14
		private bool IsLocalFileSystemWebService(string url)
		{
			if (url == null || url == string.Empty)
			{
				return false;
			}
			Uri uri = new Uri(url);
			return uri.Port >= 1024 && string.Compare(uri.Host, "localHost", StringComparison.OrdinalIgnoreCase) == 0;
		}

		// Token: 0x04000253 RID: 595
		private SendOrPostCallback getCitiesOperationCompleted;

		// Token: 0x04000254 RID: 596
		private SendOrPostCallback getCitiesByCodesOperationCompleted;

		// Token: 0x04000255 RID: 597
		private SendOrPostCallback getCityOperationCompleted;

		// Token: 0x04000256 RID: 598
		private SendOrPostCallback getModifiedCitiesOperationCompleted;

		// Token: 0x04000257 RID: 599
		private SendOrPostCallback getOrderedCitiesOperationCompleted;

		// Token: 0x04000258 RID: 600
		private SendOrPostCallback getServiceNameOperationCompleted;

		// Token: 0x04000259 RID: 601
		private bool useDefaultCredentialsSetExplicitly;
	}
}
