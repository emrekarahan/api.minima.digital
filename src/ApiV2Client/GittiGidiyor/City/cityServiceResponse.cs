﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.City
{
	// Token: 0x020000BA RID: 186
	[DesignerCategory("code")]
	[DebuggerStepThrough]
	[XmlType(Namespace = "http://city.anonymous.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[Serializable]
	public class cityServiceResponse : baseResponse
	{
		// Token: 0x17000210 RID: 528
		// (get) Token: 0x0600066E RID: 1646 RVA: 0x000099A9 File Offset: 0x00007BA9
		// (set) Token: 0x0600066F RID: 1647 RVA: 0x000099B1 File Offset: 0x00007BB1
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int cityCount
		{
			get
			{
				return this.cityCountField;
			}
			set
			{
				this.cityCountField = value;
			}
		}

		// Token: 0x17000211 RID: 529
		// (get) Token: 0x06000670 RID: 1648 RVA: 0x000099BA File Offset: 0x00007BBA
		// (set) Token: 0x06000671 RID: 1649 RVA: 0x000099C2 File Offset: 0x00007BC2
		[XmlIgnore]
		public bool cityCountSpecified
		{
			get
			{
				return this.cityCountFieldSpecified;
			}
			set
			{
				this.cityCountFieldSpecified = value;
			}
		}

		// Token: 0x17000212 RID: 530
		// (get) Token: 0x06000672 RID: 1650 RVA: 0x000099CB File Offset: 0x00007BCB
		// (set) Token: 0x06000673 RID: 1651 RVA: 0x000099D3 File Offset: 0x00007BD3
		[XmlArrayItem("city", Form = XmlSchemaForm.Unqualified)]
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		public cityType[] cities
		{
			get
			{
				return this.citiesField;
			}
			set
			{
				this.citiesField = value;
			}
		}

		// Token: 0x04000264 RID: 612
		private int cityCountField;

		// Token: 0x04000265 RID: 613
		private bool cityCountFieldSpecified;

		// Token: 0x04000266 RID: 614
		private cityType[] citiesField;
	}
}
