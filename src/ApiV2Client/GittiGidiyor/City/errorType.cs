﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.City
{
	// Token: 0x020000BE RID: 190
	[DebuggerStepThrough]
	[XmlType(Namespace = "http://city.anonymous.ws.listingapi.gg.com")]
	[DesignerCategory("code")]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[Serializable]
	public class errorType
	{
		// Token: 0x17000219 RID: 537
		// (get) Token: 0x06000684 RID: 1668 RVA: 0x00009A62 File Offset: 0x00007C62
		// (set) Token: 0x06000685 RID: 1669 RVA: 0x00009A6A File Offset: 0x00007C6A
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string errorId
		{
			get
			{
				return this.errorIdField;
			}
			set
			{
				this.errorIdField = value;
			}
		}

		// Token: 0x1700021A RID: 538
		// (get) Token: 0x06000686 RID: 1670 RVA: 0x00009A73 File Offset: 0x00007C73
		// (set) Token: 0x06000687 RID: 1671 RVA: 0x00009A7B File Offset: 0x00007C7B
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string errorCode
		{
			get
			{
				return this.errorCodeField;
			}
			set
			{
				this.errorCodeField = value;
			}
		}

		// Token: 0x1700021B RID: 539
		// (get) Token: 0x06000688 RID: 1672 RVA: 0x00009A84 File Offset: 0x00007C84
		// (set) Token: 0x06000689 RID: 1673 RVA: 0x00009A8C File Offset: 0x00007C8C
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string message
		{
			get
			{
				return this.messageField;
			}
			set
			{
				this.messageField = value;
			}
		}

		// Token: 0x1700021C RID: 540
		// (get) Token: 0x0600068A RID: 1674 RVA: 0x00009A95 File Offset: 0x00007C95
		// (set) Token: 0x0600068B RID: 1675 RVA: 0x00009A9D File Offset: 0x00007C9D
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string viewMessage
		{
			get
			{
				return this.viewMessageField;
			}
			set
			{
				this.viewMessageField = value;
			}
		}

		// Token: 0x0400026D RID: 621
		private string errorIdField;

		// Token: 0x0400026E RID: 622
		private string errorCodeField;

		// Token: 0x0400026F RID: 623
		private string messageField;

		// Token: 0x04000270 RID: 624
		private string viewMessageField;
	}
}
