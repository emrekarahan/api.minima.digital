﻿using System.CodeDom.Compiler;

namespace ApiV2Client.GittiGidiyor.City
{
	// Token: 0x020000C8 RID: 200
	// (Invoke) Token: 0x060006B1 RID: 1713
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	public delegate void getOrderedCitiesCompletedEventHandler(object sender, getOrderedCitiesCompletedEventArgs e);
}
