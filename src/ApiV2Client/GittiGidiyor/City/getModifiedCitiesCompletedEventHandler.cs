﻿using System.CodeDom.Compiler;

namespace ApiV2Client.GittiGidiyor.City
{
	// Token: 0x020000C6 RID: 198
	// (Invoke) Token: 0x060006AB RID: 1707
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	public delegate void getModifiedCitiesCompletedEventHandler(object sender, getModifiedCitiesCompletedEventArgs e);
}
