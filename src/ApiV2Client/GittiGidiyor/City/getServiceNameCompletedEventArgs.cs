﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.City
{
	// Token: 0x020000CB RID: 203
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	public class getServiceNameCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x060006BA RID: 1722 RVA: 0x00009BD3 File Offset: 0x00007DD3
		internal getServiceNameCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x17000227 RID: 551
		// (get) Token: 0x060006BB RID: 1723 RVA: 0x00009BE6 File Offset: 0x00007DE6
		public string Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (string)this.results[0];
			}
		}

		// Token: 0x0400027B RID: 635
		private object[] results;
	}
}
