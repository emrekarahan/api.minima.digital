﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.City
{
	// Token: 0x020000C1 RID: 193
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	public class getCitiesCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x0600069C RID: 1692 RVA: 0x00009B0B File Offset: 0x00007D0B
		internal getCitiesCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x17000222 RID: 546
		// (get) Token: 0x0600069D RID: 1693 RVA: 0x00009B1E File Offset: 0x00007D1E
		public cityServiceResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (cityServiceResponse)this.results[0];
			}
		}

		// Token: 0x04000276 RID: 630
		private object[] results;
	}
}
