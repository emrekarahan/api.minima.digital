﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.City
{
	// Token: 0x020000BF RID: 191
	[DebuggerStepThrough]
	[XmlType(Namespace = "http://city.anonymous.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[DesignerCategory("code")]
	[Serializable]
	public class cityServiceAuditResponse : baseResponse
	{
		// Token: 0x1700021D RID: 541
		// (get) Token: 0x0600068D RID: 1677 RVA: 0x00009AAE File Offset: 0x00007CAE
		// (set) Token: 0x0600068E RID: 1678 RVA: 0x00009AB6 File Offset: 0x00007CB6
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int totalCount
		{
			get
			{
				return this.totalCountField;
			}
			set
			{
				this.totalCountField = value;
			}
		}

		// Token: 0x1700021E RID: 542
		// (get) Token: 0x0600068F RID: 1679 RVA: 0x00009ABF File Offset: 0x00007CBF
		// (set) Token: 0x06000690 RID: 1680 RVA: 0x00009AC7 File Offset: 0x00007CC7
		[XmlIgnore]
		public bool totalCountSpecified
		{
			get
			{
				return this.totalCountFieldSpecified;
			}
			set
			{
				this.totalCountFieldSpecified = value;
			}
		}

		// Token: 0x1700021F RID: 543
		// (get) Token: 0x06000691 RID: 1681 RVA: 0x00009AD0 File Offset: 0x00007CD0
		// (set) Token: 0x06000692 RID: 1682 RVA: 0x00009AD8 File Offset: 0x00007CD8
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int count
		{
			get
			{
				return this.countField;
			}
			set
			{
				this.countField = value;
			}
		}

		// Token: 0x17000220 RID: 544
		// (get) Token: 0x06000693 RID: 1683 RVA: 0x00009AE1 File Offset: 0x00007CE1
		// (set) Token: 0x06000694 RID: 1684 RVA: 0x00009AE9 File Offset: 0x00007CE9
		[XmlIgnore]
		public bool countSpecified
		{
			get
			{
				return this.countFieldSpecified;
			}
			set
			{
				this.countFieldSpecified = value;
			}
		}

		// Token: 0x17000221 RID: 545
		// (get) Token: 0x06000695 RID: 1685 RVA: 0x00009AF2 File Offset: 0x00007CF2
		// (set) Token: 0x06000696 RID: 1686 RVA: 0x00009AFA File Offset: 0x00007CFA
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		[XmlArrayItem("cityAudit", Form = XmlSchemaForm.Unqualified)]
		public cityAuditType[] cityAudits
		{
			get
			{
				return this.cityAuditsField;
			}
			set
			{
				this.cityAuditsField = value;
			}
		}

		// Token: 0x04000271 RID: 625
		private int totalCountField;

		// Token: 0x04000272 RID: 626
		private bool totalCountFieldSpecified;

		// Token: 0x04000273 RID: 627
		private int countField;

		// Token: 0x04000274 RID: 628
		private bool countFieldSpecified;

		// Token: 0x04000275 RID: 629
		private cityAuditType[] cityAuditsField;
	}
}
