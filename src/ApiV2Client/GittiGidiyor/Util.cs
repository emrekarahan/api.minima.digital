﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace ApiV2Client.GittiGidiyor
{
	// Token: 0x020001EF RID: 495
	public class Util
	{
		// Token: 0x060010E4 RID: 4324 RVA: 0x00016B3C File Offset: 0x00014D3C
		public static long convertToTimestamp(DateTime value)
		{
			return (DateTime.UtcNow - new DateTime(1970, 1, 1)).Ticks / 10000L;
		}

		// Token: 0x060010E5 RID: 4325 RVA: 0x00016B70 File Offset: 0x00014D70
		public static string getMd5Hash(string input)
		{
			byte[] array = MD5.Create().ComputeHash(Encoding.Default.GetBytes(input));
			StringBuilder stringBuilder = new StringBuilder();
			for (int i = 0; i < array.Length; i++)
			{
				stringBuilder.Append(array[i].ToString("x2"));
			}
			return stringBuilder.ToString();
		}

		// Token: 0x060010E6 RID: 4326 RVA: 0x00016BC8 File Offset: 0x00014DC8
		public static int?[] toIntArray(List<int> values)
		{
			if (values == null)
			{
				return null;
			}
			int?[] array = new int?[values.Count];
			for (int i = 0; i < values.Count; i++)
			{
				array[i] = new int?(values[i]);
			}
			return array;
		}
	}
}
