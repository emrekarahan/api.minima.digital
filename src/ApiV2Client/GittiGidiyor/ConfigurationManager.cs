﻿using System.Xml;

namespace ApiV2Client.GittiGidiyor
{
	// Token: 0x020001EE RID: 494
	public class ConfigurationManager
	{
		// Token: 0x060010E0 RID: 4320 RVA: 0x00016A19 File Offset: 0x00014C19
		public static AuthConfig getAuthParameters()
		{
			if (ConfigurationManager.authConfig == null)
			{
				ConfigurationManager.readConfigFile();
			}
			return ConfigurationManager.authConfig;
		}

		// Token: 0x060010E1 RID: 4321 RVA: 0x00016A2C File Offset: 0x00014C2C
		private static void readConfigFile()
		{
			ConfigurationManager.authConfig = new AuthConfig();
			XmlTextReader xmlTextReader = null;
			string value = null;
			try
			{
				xmlTextReader = new XmlTextReader("auth-config.xml");
				while (xmlTextReader.Read())
				{
					XmlNodeType nodeType = xmlTextReader.NodeType;
					switch (nodeType)
					{
					case XmlNodeType.Element:
						value = xmlTextReader.Name;
						break;
					case XmlNodeType.Attribute:
						break;
					case XmlNodeType.Text:
						if ("api-key".Equals(value))
						{
							ConfigurationManager.authConfig.ApiKey = xmlTextReader.Value;
						}
						else if ("secret-key".Equals(value))
						{
							ConfigurationManager.authConfig.SecretKey = xmlTextReader.Value;
						}
						else if ("role-name".Equals(value))
						{
							ConfigurationManager.authConfig.RoleName = xmlTextReader.Value;
						}
						else if ("role-pass".Equals(value))
						{
							ConfigurationManager.authConfig.RolePass = xmlTextReader.Value;
						}
						break;
					default:
						if (nodeType != XmlNodeType.EndElement)
						{
						}
						break;
					}
				}
			}
			finally
			{
				if (xmlTextReader != null)
				{
					xmlTextReader.Close();
				}
			}
		}

		// Token: 0x060010E2 RID: 4322 RVA: 0x00016B2C File Offset: 0x00014D2C
		public static void setAuthParameters(AuthConfig config)
		{
			ConfigurationManager.authConfig = config;
		}

		// Token: 0x04000625 RID: 1573
		public const string CONFIG_FILE = "auth-config.xml";

		// Token: 0x04000626 RID: 1574
		private static AuthConfig authConfig;
	}
}
