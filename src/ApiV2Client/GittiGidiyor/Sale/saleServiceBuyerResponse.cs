﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Sale
{
	// Token: 0x02000191 RID: 401
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[XmlType(Namespace = "http://sale.individual.ws.listingapi.gg.com")]
	[Serializable]
	public class saleServiceBuyerResponse : baseResponse
	{
		// Token: 0x17000465 RID: 1125
		// (get) Token: 0x06000E32 RID: 3634 RVA: 0x00013FE3 File Offset: 0x000121E3
		// (set) Token: 0x06000E33 RID: 3635 RVA: 0x00013FEB File Offset: 0x000121EB
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int totalCount
		{
			get
			{
				return this.totalCountField;
			}
			set
			{
				this.totalCountField = value;
			}
		}

		// Token: 0x17000466 RID: 1126
		// (get) Token: 0x06000E34 RID: 3636 RVA: 0x00013FF4 File Offset: 0x000121F4
		// (set) Token: 0x06000E35 RID: 3637 RVA: 0x00013FFC File Offset: 0x000121FC
		[XmlIgnore]
		public bool totalCountSpecified
		{
			get
			{
				return this.totalCountFieldSpecified;
			}
			set
			{
				this.totalCountFieldSpecified = value;
			}
		}

		// Token: 0x17000467 RID: 1127
		// (get) Token: 0x06000E36 RID: 3638 RVA: 0x00014005 File Offset: 0x00012205
		// (set) Token: 0x06000E37 RID: 3639 RVA: 0x0001400D File Offset: 0x0001220D
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int count
		{
			get
			{
				return this.countField;
			}
			set
			{
				this.countField = value;
			}
		}

		// Token: 0x17000468 RID: 1128
		// (get) Token: 0x06000E38 RID: 3640 RVA: 0x00014016 File Offset: 0x00012216
		// (set) Token: 0x06000E39 RID: 3641 RVA: 0x0001401E File Offset: 0x0001221E
		[XmlIgnore]
		public bool countSpecified
		{
			get
			{
				return this.countFieldSpecified;
			}
			set
			{
				this.countFieldSpecified = value;
			}
		}

		// Token: 0x17000469 RID: 1129
		// (get) Token: 0x06000E3A RID: 3642 RVA: 0x00014027 File Offset: 0x00012227
		// (set) Token: 0x06000E3B RID: 3643 RVA: 0x0001402F File Offset: 0x0001222F
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		[XmlArrayItem("itemBuyer", Form = XmlSchemaForm.Unqualified)]
		public saleBuyerType[] itemBuyers
		{
			get
			{
				return this.itemBuyersField;
			}
			set
			{
				this.itemBuyersField = value;
			}
		}

		// Token: 0x0400054C RID: 1356
		private int totalCountField;

		// Token: 0x0400054D RID: 1357
		private bool totalCountFieldSpecified;

		// Token: 0x0400054E RID: 1358
		private int countField;

		// Token: 0x0400054F RID: 1359
		private bool countFieldSpecified;

		// Token: 0x04000550 RID: 1360
		private saleBuyerType[] itemBuyersField;
	}
}
