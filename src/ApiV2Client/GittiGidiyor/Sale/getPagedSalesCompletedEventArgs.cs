﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Sale
{
	// Token: 0x0200019E RID: 414
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	[DesignerCategory("code")]
	[DebuggerStepThrough]
	public class getPagedSalesCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000E6A RID: 3690 RVA: 0x0001416F File Offset: 0x0001236F
		internal getPagedSalesCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x17000475 RID: 1141
		// (get) Token: 0x06000E6B RID: 3691 RVA: 0x00014182 File Offset: 0x00012382
		public saleServicePagingResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (saleServicePagingResponse)this.results[0];
			}
		}

		// Token: 0x0400055C RID: 1372
		private object[] results;
	}
}
