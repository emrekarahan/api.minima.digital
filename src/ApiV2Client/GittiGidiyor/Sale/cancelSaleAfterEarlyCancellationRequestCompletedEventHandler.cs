﻿using System.CodeDom.Compiler;

namespace ApiV2Client.GittiGidiyor.Sale
{
	// Token: 0x02000197 RID: 407
	// (Invoke) Token: 0x06000E55 RID: 3669
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	public delegate void cancelSaleAfterEarlyCancellationRequestCompletedEventHandler(object sender, cancelSaleAfterEarlyCancellationRequestCompletedEventArgs e);
}
