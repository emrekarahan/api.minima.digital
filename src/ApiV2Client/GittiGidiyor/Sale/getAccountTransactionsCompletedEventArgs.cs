﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Sale
{
	// Token: 0x0200019A RID: 410
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	public class getAccountTransactionsCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000E5E RID: 3678 RVA: 0x0001411F File Offset: 0x0001231F
		internal getAccountTransactionsCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x17000473 RID: 1139
		// (get) Token: 0x06000E5F RID: 3679 RVA: 0x00014132 File Offset: 0x00012332
		public accountTransactionListResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (accountTransactionListResponse)this.results[0];
			}
		}

		// Token: 0x0400055A RID: 1370
		private object[] results;
	}
}
