﻿namespace ApiV2Client.GittiGidiyor.Sale
{
	// Token: 0x020001DB RID: 475
	public interface ISaleService : IService
	{
		// Token: 0x06001037 RID: 4151
		saleServiceResponse getSale(string saleCode, string lang);

		// Token: 0x06001038 RID: 4152
		saleServiceResponse getSales(bool withData, string byStatus, string byUser, string orderBy, string orderType, int pageNumber, int pageSize, string lang);

		// Token: 0x06001039 RID: 4153
		saleServiceResponse getSalesByDateRange(bool withData, string byStatus, string byUser, string orderBy, string orderType, string startDate, string endDate, int pageNumber, int pageSize, string lang);

		// Token: 0x0600103A RID: 4154
		commonSaleResponse giveRateAndComment(string userType, int productId, int rate, string comment, string lang);

		// Token: 0x0600103B RID: 4155
		commonSaleResponse replySaleComment(string userType, int productId, string comment, string lang);
	}
}
