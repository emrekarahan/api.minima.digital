﻿using System.CodeDom.Compiler;

namespace ApiV2Client.GittiGidiyor.Sale
{
	// Token: 0x0200019D RID: 413
	// (Invoke) Token: 0x06000E67 RID: 3687
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	public delegate void getPagedSalesCompletedEventHandler(object sender, getPagedSalesCompletedEventArgs e);
}
