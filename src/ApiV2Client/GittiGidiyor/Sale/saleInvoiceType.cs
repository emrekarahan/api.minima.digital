﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Sale
{
	// Token: 0x0200018C RID: 396
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[XmlType(Namespace = "http://sale.individual.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[Serializable]
	public class saleInvoiceType
	{
		// Token: 0x17000432 RID: 1074
		// (get) Token: 0x06000DC7 RID: 3527 RVA: 0x00013C58 File Offset: 0x00011E58
		// (set) Token: 0x06000DC8 RID: 3528 RVA: 0x00013C60 File Offset: 0x00011E60
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string fullname
		{
			get
			{
				return this.fullnameField;
			}
			set
			{
				this.fullnameField = value;
			}
		}

		// Token: 0x17000433 RID: 1075
		// (get) Token: 0x06000DC9 RID: 3529 RVA: 0x00013C69 File Offset: 0x00011E69
		// (set) Token: 0x06000DCA RID: 3530 RVA: 0x00013C71 File Offset: 0x00011E71
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string address
		{
			get
			{
				return this.addressField;
			}
			set
			{
				this.addressField = value;
			}
		}

		// Token: 0x17000434 RID: 1076
		// (get) Token: 0x06000DCB RID: 3531 RVA: 0x00013C7A File Offset: 0x00011E7A
		// (set) Token: 0x06000DCC RID: 3532 RVA: 0x00013C82 File Offset: 0x00011E82
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string district
		{
			get
			{
				return this.districtField;
			}
			set
			{
				this.districtField = value;
			}
		}

		// Token: 0x17000435 RID: 1077
		// (get) Token: 0x06000DCD RID: 3533 RVA: 0x00013C8B File Offset: 0x00011E8B
		// (set) Token: 0x06000DCE RID: 3534 RVA: 0x00013C93 File Offset: 0x00011E93
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int cityCode
		{
			get
			{
				return this.cityCodeField;
			}
			set
			{
				this.cityCodeField = value;
			}
		}

		// Token: 0x17000436 RID: 1078
		// (get) Token: 0x06000DCF RID: 3535 RVA: 0x00013C9C File Offset: 0x00011E9C
		// (set) Token: 0x06000DD0 RID: 3536 RVA: 0x00013CA4 File Offset: 0x00011EA4
		[XmlIgnore]
		public bool cityCodeSpecified
		{
			get
			{
				return this.cityCodeFieldSpecified;
			}
			set
			{
				this.cityCodeFieldSpecified = value;
			}
		}

		// Token: 0x17000437 RID: 1079
		// (get) Token: 0x06000DD1 RID: 3537 RVA: 0x00013CAD File Offset: 0x00011EAD
		// (set) Token: 0x06000DD2 RID: 3538 RVA: 0x00013CB5 File Offset: 0x00011EB5
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string zipCode
		{
			get
			{
				return this.zipCodeField;
			}
			set
			{
				this.zipCodeField = value;
			}
		}

		// Token: 0x17000438 RID: 1080
		// (get) Token: 0x06000DD3 RID: 3539 RVA: 0x00013CBE File Offset: 0x00011EBE
		// (set) Token: 0x06000DD4 RID: 3540 RVA: 0x00013CC6 File Offset: 0x00011EC6
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string phoneNumber
		{
			get
			{
				return this.phoneNumberField;
			}
			set
			{
				this.phoneNumberField = value;
			}
		}

		// Token: 0x17000439 RID: 1081
		// (get) Token: 0x06000DD5 RID: 3541 RVA: 0x00013CCF File Offset: 0x00011ECF
		// (set) Token: 0x06000DD6 RID: 3542 RVA: 0x00013CD7 File Offset: 0x00011ED7
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string taxOffice
		{
			get
			{
				return this.taxOfficeField;
			}
			set
			{
				this.taxOfficeField = value;
			}
		}

		// Token: 0x1700043A RID: 1082
		// (get) Token: 0x06000DD7 RID: 3543 RVA: 0x00013CE0 File Offset: 0x00011EE0
		// (set) Token: 0x06000DD8 RID: 3544 RVA: 0x00013CE8 File Offset: 0x00011EE8
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string taxNumber
		{
			get
			{
				return this.taxNumberField;
			}
			set
			{
				this.taxNumberField = value;
			}
		}

		// Token: 0x1700043B RID: 1083
		// (get) Token: 0x06000DD9 RID: 3545 RVA: 0x00013CF1 File Offset: 0x00011EF1
		// (set) Token: 0x06000DDA RID: 3546 RVA: 0x00013CF9 File Offset: 0x00011EF9
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string companyTitle
		{
			get
			{
				return this.companyTitleField;
			}
			set
			{
				this.companyTitleField = value;
			}
		}

		// Token: 0x1700043C RID: 1084
		// (get) Token: 0x06000DDB RID: 3547 RVA: 0x00013D02 File Offset: 0x00011F02
		// (set) Token: 0x06000DDC RID: 3548 RVA: 0x00013D0A File Offset: 0x00011F0A
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string tcCertificate
		{
			get
			{
				return this.tcCertificateField;
			}
			set
			{
				this.tcCertificateField = value;
			}
		}

		// Token: 0x04000519 RID: 1305
		private string fullnameField;

		// Token: 0x0400051A RID: 1306
		private string addressField;

		// Token: 0x0400051B RID: 1307
		private string districtField;

		// Token: 0x0400051C RID: 1308
		private int cityCodeField;

		// Token: 0x0400051D RID: 1309
		private bool cityCodeFieldSpecified;

		// Token: 0x0400051E RID: 1310
		private string zipCodeField;

		// Token: 0x0400051F RID: 1311
		private string phoneNumberField;

		// Token: 0x04000520 RID: 1312
		private string taxOfficeField;

		// Token: 0x04000521 RID: 1313
		private string taxNumberField;

		// Token: 0x04000522 RID: 1314
		private string companyTitleField;

		// Token: 0x04000523 RID: 1315
		private string tcCertificateField;
	}
}
