﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Sale
{
	// Token: 0x02000190 RID: 400
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[XmlType(Namespace = "http://sale.individual.ws.listingapi.gg.com")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[Serializable]
	public class commonSaleResponse : baseResponse
	{
	}
}
