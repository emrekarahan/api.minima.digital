﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Sale
{
	// Token: 0x02000185 RID: 389
	[XmlInclude(typeof(commonSaleResponse))]
	[XmlInclude(typeof(saleServiceCancelReasonResponse))]
	[XmlType(Namespace = "http://sale.individual.ws.listingapi.gg.com")]
	[XmlInclude(typeof(saleServiceBuyerResponse))]
	[XmlInclude(typeof(accountTransactionListResponse))]
	[XmlInclude(typeof(messageServiceResponse))]
	[XmlInclude(typeof(saleServiceResponse))]
	[XmlInclude(typeof(saleServicePagingResponse))]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[Serializable]
	public class baseResponse
	{
		// Token: 0x1700040B RID: 1035
		// (get) Token: 0x06000D72 RID: 3442 RVA: 0x00013989 File Offset: 0x00011B89
		// (set) Token: 0x06000D73 RID: 3443 RVA: 0x00013991 File Offset: 0x00011B91
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string ackCode
		{
			get
			{
				return this.ackCodeField;
			}
			set
			{
				this.ackCodeField = value;
			}
		}

		// Token: 0x1700040C RID: 1036
		// (get) Token: 0x06000D74 RID: 3444 RVA: 0x0001399A File Offset: 0x00011B9A
		// (set) Token: 0x06000D75 RID: 3445 RVA: 0x000139A2 File Offset: 0x00011BA2
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string responseTime
		{
			get
			{
				return this.responseTimeField;
			}
			set
			{
				this.responseTimeField = value;
			}
		}

		// Token: 0x1700040D RID: 1037
		// (get) Token: 0x06000D76 RID: 3446 RVA: 0x000139AB File Offset: 0x00011BAB
		// (set) Token: 0x06000D77 RID: 3447 RVA: 0x000139B3 File Offset: 0x00011BB3
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public errorType error
		{
			get
			{
				return this.errorField;
			}
			set
			{
				this.errorField = value;
			}
		}

		// Token: 0x1700040E RID: 1038
		// (get) Token: 0x06000D78 RID: 3448 RVA: 0x000139BC File Offset: 0x00011BBC
		// (set) Token: 0x06000D79 RID: 3449 RVA: 0x000139C4 File Offset: 0x00011BC4
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string timeElapsed
		{
			get
			{
				return this.timeElapsedField;
			}
			set
			{
				this.timeElapsedField = value;
			}
		}

		// Token: 0x040004F2 RID: 1266
		private string ackCodeField;

		// Token: 0x040004F3 RID: 1267
		private string responseTimeField;

		// Token: 0x040004F4 RID: 1268
		private errorType errorField;

		// Token: 0x040004F5 RID: 1269
		private string timeElapsedField;
	}
}
