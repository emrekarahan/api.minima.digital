﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Sale
{
	// Token: 0x0200018E RID: 398
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[XmlType(Namespace = "http://sale.individual.ws.listingapi.gg.com")]
	[DesignerCategory("code")]
	[DebuggerStepThrough]
	[Serializable]
	public class saleType
	{
		// Token: 0x1700044B RID: 1099
		// (get) Token: 0x06000DFB RID: 3579 RVA: 0x00013E11 File Offset: 0x00012011
		// (set) Token: 0x06000DFC RID: 3580 RVA: 0x00013E19 File Offset: 0x00012019
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string saleCode
		{
			get
			{
				return this.saleCodeField;
			}
			set
			{
				this.saleCodeField = value;
			}
		}

		// Token: 0x1700044C RID: 1100
		// (get) Token: 0x06000DFD RID: 3581 RVA: 0x00013E22 File Offset: 0x00012022
		// (set) Token: 0x06000DFE RID: 3582 RVA: 0x00013E2A File Offset: 0x0001202A
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string status
		{
			get
			{
				return this.statusField;
			}
			set
			{
				this.statusField = value;
			}
		}

		// Token: 0x1700044D RID: 1101
		// (get) Token: 0x06000DFF RID: 3583 RVA: 0x00013E33 File Offset: 0x00012033
		// (set) Token: 0x06000E00 RID: 3584 RVA: 0x00013E3B File Offset: 0x0001203B
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int productId
		{
			get
			{
				return this.productIdField;
			}
			set
			{
				this.productIdField = value;
			}
		}

		// Token: 0x1700044E RID: 1102
		// (get) Token: 0x06000E01 RID: 3585 RVA: 0x00013E44 File Offset: 0x00012044
		// (set) Token: 0x06000E02 RID: 3586 RVA: 0x00013E4C File Offset: 0x0001204C
		[XmlIgnore]
		public bool productIdSpecified
		{
			get
			{
				return this.productIdFieldSpecified;
			}
			set
			{
				this.productIdFieldSpecified = value;
			}
		}

		// Token: 0x1700044F RID: 1103
		// (get) Token: 0x06000E03 RID: 3587 RVA: 0x00013E55 File Offset: 0x00012055
		// (set) Token: 0x06000E04 RID: 3588 RVA: 0x00013E5D File Offset: 0x0001205D
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string productTitle
		{
			get
			{
				return this.productTitleField;
			}
			set
			{
				this.productTitleField = value;
			}
		}

		// Token: 0x17000450 RID: 1104
		// (get) Token: 0x06000E05 RID: 3589 RVA: 0x00013E66 File Offset: 0x00012066
		// (set) Token: 0x06000E06 RID: 3590 RVA: 0x00013E6E File Offset: 0x0001206E
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string price
		{
			get
			{
				return this.priceField;
			}
			set
			{
				this.priceField = value;
			}
		}

		// Token: 0x17000451 RID: 1105
		// (get) Token: 0x06000E07 RID: 3591 RVA: 0x00013E77 File Offset: 0x00012077
		// (set) Token: 0x06000E08 RID: 3592 RVA: 0x00013E7F File Offset: 0x0001207F
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string cargoPayment
		{
			get
			{
				return this.cargoPaymentField;
			}
			set
			{
				this.cargoPaymentField = value;
			}
		}

		// Token: 0x17000452 RID: 1106
		// (get) Token: 0x06000E09 RID: 3593 RVA: 0x00013E88 File Offset: 0x00012088
		// (set) Token: 0x06000E0A RID: 3594 RVA: 0x00013E90 File Offset: 0x00012090
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string cargoCode
		{
			get
			{
				return this.cargoCodeField;
			}
			set
			{
				this.cargoCodeField = value;
			}
		}

		// Token: 0x17000453 RID: 1107
		// (get) Token: 0x06000E0B RID: 3595 RVA: 0x00013E99 File Offset: 0x00012099
		// (set) Token: 0x06000E0C RID: 3596 RVA: 0x00013EA1 File Offset: 0x000120A1
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int amount
		{
			get
			{
				return this.amountField;
			}
			set
			{
				this.amountField = value;
			}
		}

		// Token: 0x17000454 RID: 1108
		// (get) Token: 0x06000E0D RID: 3597 RVA: 0x00013EAA File Offset: 0x000120AA
		// (set) Token: 0x06000E0E RID: 3598 RVA: 0x00013EB2 File Offset: 0x000120B2
		[XmlIgnore]
		public bool amountSpecified
		{
			get
			{
				return this.amountFieldSpecified;
			}
			set
			{
				this.amountFieldSpecified = value;
			}
		}

		// Token: 0x17000455 RID: 1109
		// (get) Token: 0x06000E0F RID: 3599 RVA: 0x00013EBB File Offset: 0x000120BB
		// (set) Token: 0x06000E10 RID: 3600 RVA: 0x00013EC3 File Offset: 0x000120C3
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string endDate
		{
			get
			{
				return this.endDateField;
			}
			set
			{
				this.endDateField = value;
			}
		}

		// Token: 0x17000456 RID: 1110
		// (get) Token: 0x06000E11 RID: 3601 RVA: 0x00013ECC File Offset: 0x000120CC
		// (set) Token: 0x06000E12 RID: 3602 RVA: 0x00013ED4 File Offset: 0x000120D4
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public orderBuyerInfoType buyerInfo
		{
			get
			{
				return this.buyerInfoField;
			}
			set
			{
				this.buyerInfoField = value;
			}
		}

		// Token: 0x17000457 RID: 1111
		// (get) Token: 0x06000E13 RID: 3603 RVA: 0x00013EDD File Offset: 0x000120DD
		// (set) Token: 0x06000E14 RID: 3604 RVA: 0x00013EE5 File Offset: 0x000120E5
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string changeStatus
		{
			get
			{
				return this.changeStatusField;
			}
			set
			{
				this.changeStatusField = value;
			}
		}

		// Token: 0x17000458 RID: 1112
		// (get) Token: 0x06000E15 RID: 3605 RVA: 0x00013EEE File Offset: 0x000120EE
		// (set) Token: 0x06000E16 RID: 3606 RVA: 0x00013EF6 File Offset: 0x000120F6
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string thumbImageLink
		{
			get
			{
				return this.thumbImageLinkField;
			}
			set
			{
				this.thumbImageLinkField = value;
			}
		}

		// Token: 0x17000459 RID: 1113
		// (get) Token: 0x06000E17 RID: 3607 RVA: 0x00013EFF File Offset: 0x000120FF
		// (set) Token: 0x06000E18 RID: 3608 RVA: 0x00013F07 File Offset: 0x00012107
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string lastActionDate
		{
			get
			{
				return this.lastActionDateField;
			}
			set
			{
				this.lastActionDateField = value;
			}
		}

		// Token: 0x1700045A RID: 1114
		// (get) Token: 0x06000E19 RID: 3609 RVA: 0x00013F10 File Offset: 0x00012110
		// (set) Token: 0x06000E1A RID: 3610 RVA: 0x00013F18 File Offset: 0x00012118
		[XmlArrayItem("waitingProcess", Form = XmlSchemaForm.Unqualified)]
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		public int?[] waitingProcesses
		{
			get
			{
				return this.waitingProcessesField;
			}
			set
			{
				this.waitingProcessesField = value;
			}
		}

		// Token: 0x1700045B RID: 1115
		// (get) Token: 0x06000E1B RID: 3611 RVA: 0x00013F21 File Offset: 0x00012121
		// (set) Token: 0x06000E1C RID: 3612 RVA: 0x00013F29 File Offset: 0x00012129
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public saleInvoiceType invoiceInfo
		{
			get
			{
				return this.invoiceInfoField;
			}
			set
			{
				this.invoiceInfoField = value;
			}
		}

		// Token: 0x1700045C RID: 1116
		// (get) Token: 0x06000E1D RID: 3613 RVA: 0x00013F32 File Offset: 0x00012132
		// (set) Token: 0x06000E1E RID: 3614 RVA: 0x00013F3A File Offset: 0x0001213A
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public long variantId
		{
			get
			{
				return this.variantIdField;
			}
			set
			{
				this.variantIdField = value;
			}
		}

		// Token: 0x1700045D RID: 1117
		// (get) Token: 0x06000E1F RID: 3615 RVA: 0x00013F43 File Offset: 0x00012143
		// (set) Token: 0x06000E20 RID: 3616 RVA: 0x00013F4B File Offset: 0x0001214B
		[XmlIgnore]
		public bool variantIdSpecified
		{
			get
			{
				return this.variantIdFieldSpecified;
			}
			set
			{
				this.variantIdFieldSpecified = value;
			}
		}

		// Token: 0x1700045E RID: 1118
		// (get) Token: 0x06000E21 RID: 3617 RVA: 0x00013F54 File Offset: 0x00012154
		// (set) Token: 0x06000E22 RID: 3618 RVA: 0x00013F5C File Offset: 0x0001215C
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		[XmlArrayItem("variantSpec", Form = XmlSchemaForm.Unqualified)]
		public itemVariantSpecType[] variantSpecs
		{
			get
			{
				return this.variantSpecsField;
			}
			set
			{
				this.variantSpecsField = value;
			}
		}

		// Token: 0x1700045F RID: 1119
		// (get) Token: 0x06000E23 RID: 3619 RVA: 0x00013F65 File Offset: 0x00012165
		// (set) Token: 0x06000E24 RID: 3620 RVA: 0x00013F6D File Offset: 0x0001216D
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string moneyDate
		{
			get
			{
				return this.moneyDateField;
			}
			set
			{
				this.moneyDateField = value;
			}
		}

		// Token: 0x17000460 RID: 1120
		// (get) Token: 0x06000E25 RID: 3621 RVA: 0x00013F76 File Offset: 0x00012176
		// (set) Token: 0x06000E26 RID: 3622 RVA: 0x00013F7E File Offset: 0x0001217E
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string cargoApprovementDate
		{
			get
			{
				return this.cargoApprovementDateField;
			}
			set
			{
				this.cargoApprovementDateField = value;
			}
		}

		// Token: 0x17000461 RID: 1121
		// (get) Token: 0x06000E27 RID: 3623 RVA: 0x00013F87 File Offset: 0x00012187
		// (set) Token: 0x06000E28 RID: 3624 RVA: 0x00013F8F File Offset: 0x0001218F
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string itemId
		{
			get
			{
				return this.itemIdField;
			}
			set
			{
				this.itemIdField = value;
			}
		}

		// Token: 0x04000532 RID: 1330
		private string saleCodeField;

		// Token: 0x04000533 RID: 1331
		private string statusField;

		// Token: 0x04000534 RID: 1332
		private int productIdField;

		// Token: 0x04000535 RID: 1333
		private bool productIdFieldSpecified;

		// Token: 0x04000536 RID: 1334
		private string productTitleField;

		// Token: 0x04000537 RID: 1335
		private string priceField;

		// Token: 0x04000538 RID: 1336
		private string cargoPaymentField;

		// Token: 0x04000539 RID: 1337
		private string cargoCodeField;

		// Token: 0x0400053A RID: 1338
		private int amountField;

		// Token: 0x0400053B RID: 1339
		private bool amountFieldSpecified;

		// Token: 0x0400053C RID: 1340
		private string endDateField;

		// Token: 0x0400053D RID: 1341
		private orderBuyerInfoType buyerInfoField;

		// Token: 0x0400053E RID: 1342
		private string changeStatusField;

		// Token: 0x0400053F RID: 1343
		private string thumbImageLinkField;

		// Token: 0x04000540 RID: 1344
		private string lastActionDateField;

		// Token: 0x04000541 RID: 1345
		private int?[] waitingProcessesField;

		// Token: 0x04000542 RID: 1346
		private saleInvoiceType invoiceInfoField;

		// Token: 0x04000543 RID: 1347
		private long variantIdField;

		// Token: 0x04000544 RID: 1348
		private bool variantIdFieldSpecified;

		// Token: 0x04000545 RID: 1349
		private itemVariantSpecType[] variantSpecsField;

		// Token: 0x04000546 RID: 1350
		private string moneyDateField;

		// Token: 0x04000547 RID: 1351
		private string cargoApprovementDateField;

		// Token: 0x04000548 RID: 1352
		private string itemIdField;
	}
}
