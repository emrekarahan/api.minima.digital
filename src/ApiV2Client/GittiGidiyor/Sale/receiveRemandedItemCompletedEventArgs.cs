﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Sale
{
	// Token: 0x020001B0 RID: 432
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	public class receiveRemandedItemCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000EA0 RID: 3744 RVA: 0x000142D7 File Offset: 0x000124D7
		internal receiveRemandedItemCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x1700047E RID: 1150
		// (get) Token: 0x06000EA1 RID: 3745 RVA: 0x000142EA File Offset: 0x000124EA
		public messageServiceResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (messageServiceResponse)this.results[0];
			}
		}

		// Token: 0x04000565 RID: 1381
		private object[] results;
	}
}
