﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Sale
{
	// Token: 0x02000198 RID: 408
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	public class cancelSaleAfterEarlyCancellationRequestCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000E58 RID: 3672 RVA: 0x000140F7 File Offset: 0x000122F7
		internal cancelSaleAfterEarlyCancellationRequestCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x17000472 RID: 1138
		// (get) Token: 0x06000E59 RID: 3673 RVA: 0x0001410A File Offset: 0x0001230A
		public messageServiceResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (messageServiceResponse)this.results[0];
			}
		}

		// Token: 0x04000559 RID: 1369
		private object[] results;
	}
}
