﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Sale
{
	// Token: 0x020001A4 RID: 420
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	public class getSaleCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000E7C RID: 3708 RVA: 0x000141E7 File Offset: 0x000123E7
		internal getSaleCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x17000478 RID: 1144
		// (get) Token: 0x06000E7D RID: 3709 RVA: 0x000141FA File Offset: 0x000123FA
		public saleServiceResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (saleServiceResponse)this.results[0];
			}
		}

		// Token: 0x0400055F RID: 1375
		private object[] results;
	}
}
