﻿using System.CodeDom.Compiler;

namespace ApiV2Client.GittiGidiyor.Sale
{
	// Token: 0x02000199 RID: 409
	// (Invoke) Token: 0x06000E5B RID: 3675
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	public delegate void getAccountTransactionsCompletedEventHandler(object sender, getAccountTransactionsCompletedEventArgs e);
}
