﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Sale
{
	// Token: 0x02000187 RID: 391
	[XmlType(Namespace = "http://sale.individual.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[Serializable]
	public class errorType
	{
		// Token: 0x17000410 RID: 1040
		// (get) Token: 0x06000D7E RID: 3454 RVA: 0x000139EE File Offset: 0x00011BEE
		// (set) Token: 0x06000D7F RID: 3455 RVA: 0x000139F6 File Offset: 0x00011BF6
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string errorId
		{
			get
			{
				return this.errorIdField;
			}
			set
			{
				this.errorIdField = value;
			}
		}

		// Token: 0x17000411 RID: 1041
		// (get) Token: 0x06000D80 RID: 3456 RVA: 0x000139FF File Offset: 0x00011BFF
		// (set) Token: 0x06000D81 RID: 3457 RVA: 0x00013A07 File Offset: 0x00011C07
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string errorCode
		{
			get
			{
				return this.errorCodeField;
			}
			set
			{
				this.errorCodeField = value;
			}
		}

		// Token: 0x17000412 RID: 1042
		// (get) Token: 0x06000D82 RID: 3458 RVA: 0x00013A10 File Offset: 0x00011C10
		// (set) Token: 0x06000D83 RID: 3459 RVA: 0x00013A18 File Offset: 0x00011C18
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string message
		{
			get
			{
				return this.messageField;
			}
			set
			{
				this.messageField = value;
			}
		}

		// Token: 0x17000413 RID: 1043
		// (get) Token: 0x06000D84 RID: 3460 RVA: 0x00013A21 File Offset: 0x00011C21
		// (set) Token: 0x06000D85 RID: 3461 RVA: 0x00013A29 File Offset: 0x00011C29
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string viewMessage
		{
			get
			{
				return this.viewMessageField;
			}
			set
			{
				this.viewMessageField = value;
			}
		}

		// Token: 0x040004F7 RID: 1271
		private string errorIdField;

		// Token: 0x040004F8 RID: 1272
		private string errorCodeField;

		// Token: 0x040004F9 RID: 1273
		private string messageField;

		// Token: 0x040004FA RID: 1274
		private string viewMessageField;
	}
}
