﻿using System.CodeDom.Compiler;

namespace ApiV2Client.GittiGidiyor.Sale
{
	// Token: 0x0200019F RID: 415
	// (Invoke) Token: 0x06000E6D RID: 3693
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	public delegate void getPagedSalesByDateRangeCompletedEventHandler(object sender, getPagedSalesByDateRangeCompletedEventArgs e);
}
