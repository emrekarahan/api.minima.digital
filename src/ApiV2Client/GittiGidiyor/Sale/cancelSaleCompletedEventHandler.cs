﻿using System.CodeDom.Compiler;

namespace ApiV2Client.GittiGidiyor.Sale
{
	// Token: 0x02000195 RID: 405
	// (Invoke) Token: 0x06000E4F RID: 3663
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	public delegate void cancelSaleCompletedEventHandler(object sender, cancelSaleCompletedEventArgs e);
}
