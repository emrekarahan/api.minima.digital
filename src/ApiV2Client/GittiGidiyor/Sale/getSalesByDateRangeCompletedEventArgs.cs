﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Sale
{
	// Token: 0x020001A8 RID: 424
	[DesignerCategory("code")]
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	[DebuggerStepThrough]
	public class getSalesByDateRangeCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000E88 RID: 3720 RVA: 0x00014237 File Offset: 0x00012437
		internal getSalesByDateRangeCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x1700047A RID: 1146
		// (get) Token: 0x06000E89 RID: 3721 RVA: 0x0001424A File Offset: 0x0001244A
		public saleServicePagingResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (saleServicePagingResponse)this.results[0];
			}
		}

		// Token: 0x04000561 RID: 1377
		private object[] results;
	}
}
