﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Sale
{
	// Token: 0x0200018B RID: 395
	[XmlType(Namespace = "http://sale.individual.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[Serializable]
	public class itemVariantSpecType
	{
		// Token: 0x1700042B RID: 1067
		// (get) Token: 0x06000DB8 RID: 3512 RVA: 0x00013BD9 File Offset: 0x00011DD9
		// (set) Token: 0x06000DB9 RID: 3513 RVA: 0x00013BE1 File Offset: 0x00011DE1
		[XmlAttribute]
		public string name
		{
			get
			{
				return this.nameField;
			}
			set
			{
				this.nameField = value;
			}
		}

		// Token: 0x1700042C RID: 1068
		// (get) Token: 0x06000DBA RID: 3514 RVA: 0x00013BEA File Offset: 0x00011DEA
		// (set) Token: 0x06000DBB RID: 3515 RVA: 0x00013BF2 File Offset: 0x00011DF2
		[XmlAttribute]
		public string value
		{
			get
			{
				return this.valueField;
			}
			set
			{
				this.valueField = value;
			}
		}

		// Token: 0x1700042D RID: 1069
		// (get) Token: 0x06000DBC RID: 3516 RVA: 0x00013BFB File Offset: 0x00011DFB
		// (set) Token: 0x06000DBD RID: 3517 RVA: 0x00013C03 File Offset: 0x00011E03
		[XmlAttribute]
		public int quantity
		{
			get
			{
				return this.quantityField;
			}
			set
			{
				this.quantityField = value;
			}
		}

		// Token: 0x1700042E RID: 1070
		// (get) Token: 0x06000DBE RID: 3518 RVA: 0x00013C0C File Offset: 0x00011E0C
		// (set) Token: 0x06000DBF RID: 3519 RVA: 0x00013C14 File Offset: 0x00011E14
		[XmlIgnore]
		public bool quantitySpecified
		{
			get
			{
				return this.quantityFieldSpecified;
			}
			set
			{
				this.quantityFieldSpecified = value;
			}
		}

		// Token: 0x1700042F RID: 1071
		// (get) Token: 0x06000DC0 RID: 3520 RVA: 0x00013C1D File Offset: 0x00011E1D
		// (set) Token: 0x06000DC1 RID: 3521 RVA: 0x00013C25 File Offset: 0x00011E25
		[XmlAttribute]
		public int soldQuantity
		{
			get
			{
				return this.soldQuantityField;
			}
			set
			{
				this.soldQuantityField = value;
			}
		}

		// Token: 0x17000430 RID: 1072
		// (get) Token: 0x06000DC2 RID: 3522 RVA: 0x00013C2E File Offset: 0x00011E2E
		// (set) Token: 0x06000DC3 RID: 3523 RVA: 0x00013C36 File Offset: 0x00011E36
		[XmlIgnore]
		public bool soldQuantitySpecified
		{
			get
			{
				return this.soldQuantityFieldSpecified;
			}
			set
			{
				this.soldQuantityFieldSpecified = value;
			}
		}

		// Token: 0x17000431 RID: 1073
		// (get) Token: 0x06000DC4 RID: 3524 RVA: 0x00013C3F File Offset: 0x00011E3F
		// (set) Token: 0x06000DC5 RID: 3525 RVA: 0x00013C47 File Offset: 0x00011E47
		[XmlAttribute]
		public string stockCode
		{
			get
			{
				return this.stockCodeField;
			}
			set
			{
				this.stockCodeField = value;
			}
		}

		// Token: 0x04000512 RID: 1298
		private string nameField;

		// Token: 0x04000513 RID: 1299
		private string valueField;

		// Token: 0x04000514 RID: 1300
		private int quantityField;

		// Token: 0x04000515 RID: 1301
		private bool quantityFieldSpecified;

		// Token: 0x04000516 RID: 1302
		private int soldQuantityField;

		// Token: 0x04000517 RID: 1303
		private bool soldQuantityFieldSpecified;

		// Token: 0x04000518 RID: 1304
		private string stockCodeField;
	}
}
