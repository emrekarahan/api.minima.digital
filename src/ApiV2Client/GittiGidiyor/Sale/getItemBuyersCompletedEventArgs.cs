﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Sale
{
	// Token: 0x0200019C RID: 412
	[DesignerCategory("code")]
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	[DebuggerStepThrough]
	public class getItemBuyersCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000E64 RID: 3684 RVA: 0x00014147 File Offset: 0x00012347
		internal getItemBuyersCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x17000474 RID: 1140
		// (get) Token: 0x06000E65 RID: 3685 RVA: 0x0001415A File Offset: 0x0001235A
		public saleServiceBuyerResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (saleServiceBuyerResponse)this.results[0];
			}
		}

		// Token: 0x0400055B RID: 1371
		private object[] results;
	}
}
