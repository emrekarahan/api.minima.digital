﻿using System.CodeDom.Compiler;

namespace ApiV2Client.GittiGidiyor.Sale
{
	// Token: 0x020001A5 RID: 421
	// (Invoke) Token: 0x06000E7F RID: 3711
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	public delegate void getSalesCompletedEventHandler(object sender, getSalesCompletedEventArgs e);
}
