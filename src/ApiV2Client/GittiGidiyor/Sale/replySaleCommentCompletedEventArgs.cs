﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Sale
{
	// Token: 0x020001B6 RID: 438
	[DesignerCategory("code")]
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	[DebuggerStepThrough]
	public class replySaleCommentCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000EB2 RID: 3762 RVA: 0x0001434F File Offset: 0x0001254F
		internal replySaleCommentCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x17000481 RID: 1153
		// (get) Token: 0x06000EB3 RID: 3763 RVA: 0x00014362 File Offset: 0x00012562
		public commonSaleResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (commonSaleResponse)this.results[0];
			}
		}

		// Token: 0x04000568 RID: 1384
		private object[] results;
	}
}
