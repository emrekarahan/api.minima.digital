﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Sale
{
	// Token: 0x020001A0 RID: 416
	[DebuggerStepThrough]
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	[DesignerCategory("code")]
	public class getPagedSalesByDateRangeCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000E70 RID: 3696 RVA: 0x00014197 File Offset: 0x00012397
		internal getPagedSalesByDateRangeCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x17000476 RID: 1142
		// (get) Token: 0x06000E71 RID: 3697 RVA: 0x000141AA File Offset: 0x000123AA
		public saleServicePagingResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (saleServicePagingResponse)this.results[0];
			}
		}

		// Token: 0x0400055D RID: 1373
		private object[] results;
	}
}
