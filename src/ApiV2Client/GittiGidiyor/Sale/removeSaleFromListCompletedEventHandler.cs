﻿using System.CodeDom.Compiler;

namespace ApiV2Client.GittiGidiyor.Sale
{
	// Token: 0x020001B3 RID: 435
	// (Invoke) Token: 0x06000EA9 RID: 3753
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	public delegate void removeSaleFromListCompletedEventHandler(object sender, removeSaleFromListCompletedEventArgs e);
}
