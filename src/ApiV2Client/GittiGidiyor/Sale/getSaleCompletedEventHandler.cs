﻿using System.CodeDom.Compiler;

namespace ApiV2Client.GittiGidiyor.Sale
{
	// Token: 0x020001A3 RID: 419
	// (Invoke) Token: 0x06000E79 RID: 3705
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	public delegate void getSaleCompletedEventHandler(object sender, getSaleCompletedEventArgs e);
}
