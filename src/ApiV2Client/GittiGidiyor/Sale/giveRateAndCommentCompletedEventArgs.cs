﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Sale
{
	// Token: 0x020001AE RID: 430
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	public class giveRateAndCommentCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000E9A RID: 3738 RVA: 0x000142AF File Offset: 0x000124AF
		internal giveRateAndCommentCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x1700047D RID: 1149
		// (get) Token: 0x06000E9B RID: 3739 RVA: 0x000142C2 File Offset: 0x000124C2
		public commonSaleResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (commonSaleResponse)this.results[0];
			}
		}

		// Token: 0x04000564 RID: 1380
		private object[] results;
	}
}
