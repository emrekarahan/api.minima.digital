﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Sale
{
	// Token: 0x020001A2 RID: 418
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	public class getReasonsToCancelSaleCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000E76 RID: 3702 RVA: 0x000141BF File Offset: 0x000123BF
		internal getReasonsToCancelSaleCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x17000477 RID: 1143
		// (get) Token: 0x06000E77 RID: 3703 RVA: 0x000141D2 File Offset: 0x000123D2
		public saleServiceCancelReasonResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (saleServiceCancelReasonResponse)this.results[0];
			}
		}

		// Token: 0x0400055E RID: 1374
		private object[] results;
	}
}
