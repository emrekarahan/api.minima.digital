﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Sale
{
	// Token: 0x02000186 RID: 390
	[XmlType(Namespace = "http://sale.individual.ws.listingapi.gg.com")]
	[DesignerCategory("code")]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[DebuggerStepThrough]
	[Serializable]
	public class messageServiceResponse : baseResponse
	{
		// Token: 0x1700040F RID: 1039
		// (get) Token: 0x06000D7B RID: 3451 RVA: 0x000139D5 File Offset: 0x00011BD5
		// (set) Token: 0x06000D7C RID: 3452 RVA: 0x000139DD File Offset: 0x00011BDD
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string result
		{
			get
			{
				return this.resultField;
			}
			set
			{
				this.resultField = value;
			}
		}

		// Token: 0x040004F6 RID: 1270
		private string resultField;
	}
}
