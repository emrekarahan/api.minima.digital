﻿using System.CodeDom.Compiler;

namespace ApiV2Client.GittiGidiyor.Sale
{
	// Token: 0x020001AB RID: 427
	// (Invoke) Token: 0x06000E91 RID: 3729
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	public delegate void giveApprovalForRemandedItemCompletedEventHandler(object sender, giveApprovalForRemandedItemCompletedEventArgs e);
}
