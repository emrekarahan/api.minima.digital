﻿using System;

namespace ApiV2Client.GittiGidiyor.Sale
{
	// Token: 0x020001DC RID: 476
	public class SaleService : ServiceClient<IndividualSaleServiceService>, ISaleService, IService
	{
		// Token: 0x0600103C RID: 4156 RVA: 0x00015928 File Offset: 0x00013B28
		private SaleService()
		{
			base.configureNetworkCredentials(this.service);
		}

		// Token: 0x0600103D RID: 4157 RVA: 0x00015948 File Offset: 0x00013B48
		public saleServiceResponse getSale(string saleCode, string lang)
		{
			long time = Util.convertToTimestamp(DateTime.Now);
			return this.service.getSale(this.authConfig.ApiKey, base.getSignature(time), time, saleCode, lang);
		}

		// Token: 0x0600103E RID: 4158 RVA: 0x00015980 File Offset: 0x00013B80
		public saleServiceResponse getSales(bool withData, string byStatus, string byUser, string orderBy, string orderType, int pageNumber, int pageSize, string lang)
		{
			long time = Util.convertToTimestamp(DateTime.Now);
			return this.service.getSales(this.authConfig.ApiKey, base.getSignature(time), time, withData, byStatus, byUser, orderBy, orderType, pageNumber, pageSize, lang);
		}

		// Token: 0x0600103F RID: 4159 RVA: 0x000159C4 File Offset: 0x00013BC4
		public saleServiceResponse getSalesByDateRange(bool withData, string byStatus, string byUser, string orderBy, string orderType, string startDate, string endDate, int pageNumber, int pageSize, string lang)
		{
			long time = Util.convertToTimestamp(DateTime.Now);
			return this.service.getSalesByDateRange(this.authConfig.ApiKey, base.getSignature(time), time, withData, byStatus, byUser, orderBy, orderType, startDate, endDate, pageNumber, pageSize, lang);
		}

		// Token: 0x06001040 RID: 4160 RVA: 0x00015A0B File Offset: 0x00013C0B
		public string getServiceName()
		{
			return this.service.getServiceName();
		}

		// Token: 0x06001041 RID: 4161 RVA: 0x00015A18 File Offset: 0x00013C18
		public commonSaleResponse giveRateAndComment(string userType, int productId, int rate, string comment, string lang)
		{
			long time = Util.convertToTimestamp(DateTime.Now);
			return this.service.giveRateAndComment(this.authConfig.ApiKey, base.getSignature(time), time, userType, productId, rate, comment, lang);
		}

		// Token: 0x06001042 RID: 4162 RVA: 0x00015A58 File Offset: 0x00013C58
		public commonSaleResponse replySaleComment(string userType, int productId, string comment, string lang)
		{
			long time = Util.convertToTimestamp(DateTime.Now);
			return this.service.replySaleComment(this.authConfig.ApiKey, base.getSignature(time), time, userType, productId, comment, lang);
		}

		// Token: 0x17000512 RID: 1298
		// (get) Token: 0x06001043 RID: 4163 RVA: 0x00015A94 File Offset: 0x00013C94
		public static SaleService Instance
		{
			get
			{
				if (SaleService.instance == null)
				{
					lock (SaleService.lockObject)
					{
						if (SaleService.instance == null)
						{
							SaleService.instance = new SaleService();
						}
					}
				}
				return SaleService.instance;
			}
		}

		// Token: 0x04000607 RID: 1543
		private static SaleService instance;

		// Token: 0x04000608 RID: 1544
		private static object lockObject = new object();

		// Token: 0x04000609 RID: 1545
		private IndividualSaleServiceService service = new IndividualSaleServiceService();
	}
}
