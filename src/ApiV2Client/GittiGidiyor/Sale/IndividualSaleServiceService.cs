﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading;
using System.Web.Services;
using System.Web.Services.Description;
using System.Web.Services.Protocols;
using System.Xml.Serialization;
using ApiV2Client.Properties;

namespace ApiV2Client.GittiGidiyor.Sale
{
	// Token: 0x02000184 RID: 388
	[WebServiceBinding(Name = "IndividualSaleServiceBinding", Namespace = "http://sale.individual.ws.listingapi.gg.com")]
	[DebuggerStepThrough]
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	[DesignerCategory("code")]
	[XmlInclude(typeof(baseResponse))]
	public class IndividualSaleServiceService : SoapHttpClientProtocol
	{
		// Token: 0x06000D05 RID: 3333 RVA: 0x00011DAE File Offset: 0x0000FFAE
		public IndividualSaleServiceService()
		{
			this.Url = Settings.Default.ApiV2Client_IndividualSaleServiceService_IndividualSaleServiceService;
			if (this.IsLocalFileSystemWebService(this.Url))
			{
				this.UseDefaultCredentials = true;
				this.useDefaultCredentialsSetExplicitly = false;
				return;
			}
			this.useDefaultCredentialsSetExplicitly = true;
		}

		// Token: 0x17000409 RID: 1033
		// (get) Token: 0x06000D06 RID: 3334 RVA: 0x00011DEA File Offset: 0x0000FFEA
		// (set) Token: 0x06000D07 RID: 3335 RVA: 0x00011DF2 File Offset: 0x0000FFF2
		public new string Url
		{
			get
			{
				return base.Url;
			}
			set
			{
				if (this.IsLocalFileSystemWebService(base.Url) && !this.useDefaultCredentialsSetExplicitly && !this.IsLocalFileSystemWebService(value))
				{
					base.UseDefaultCredentials = false;
				}
				base.Url = value;
			}
		}

		// Token: 0x1700040A RID: 1034
		// (get) Token: 0x06000D08 RID: 3336 RVA: 0x00011E21 File Offset: 0x00010021
		// (set) Token: 0x06000D09 RID: 3337 RVA: 0x00011E29 File Offset: 0x00010029
		public new bool UseDefaultCredentials
		{
			get
			{
				return base.UseDefaultCredentials;
			}
			set
			{
				base.UseDefaultCredentials = value;
				this.useDefaultCredentialsSetExplicitly = true;
			}
		}

		// Token: 0x1400006D RID: 109
		// (add) Token: 0x06000D0A RID: 3338 RVA: 0x00011E3C File Offset: 0x0001003C
		// (remove) Token: 0x06000D0B RID: 3339 RVA: 0x00011E74 File Offset: 0x00010074
		public event cancelSaleCompletedEventHandler cancelSaleCompleted;

		// Token: 0x1400006E RID: 110
		// (add) Token: 0x06000D0C RID: 3340 RVA: 0x00011EAC File Offset: 0x000100AC
		// (remove) Token: 0x06000D0D RID: 3341 RVA: 0x00011EE4 File Offset: 0x000100E4
		public event cancelSaleAfterEarlyCancellationRequestCompletedEventHandler cancelSaleAfterEarlyCancellationRequestCompleted;

		// Token: 0x1400006F RID: 111
		// (add) Token: 0x06000D0E RID: 3342 RVA: 0x00011F1C File Offset: 0x0001011C
		// (remove) Token: 0x06000D0F RID: 3343 RVA: 0x00011F54 File Offset: 0x00010154
		public event getAccountTransactionsCompletedEventHandler getAccountTransactionsCompleted;

		// Token: 0x14000070 RID: 112
		// (add) Token: 0x06000D10 RID: 3344 RVA: 0x00011F8C File Offset: 0x0001018C
		// (remove) Token: 0x06000D11 RID: 3345 RVA: 0x00011FC4 File Offset: 0x000101C4
		public event getItemBuyersCompletedEventHandler getItemBuyersCompleted;

		// Token: 0x14000071 RID: 113
		// (add) Token: 0x06000D12 RID: 3346 RVA: 0x00011FFC File Offset: 0x000101FC
		// (remove) Token: 0x06000D13 RID: 3347 RVA: 0x00012034 File Offset: 0x00010234
		public event getPagedSalesCompletedEventHandler getPagedSalesCompleted;

		// Token: 0x14000072 RID: 114
		// (add) Token: 0x06000D14 RID: 3348 RVA: 0x0001206C File Offset: 0x0001026C
		// (remove) Token: 0x06000D15 RID: 3349 RVA: 0x000120A4 File Offset: 0x000102A4
		public event getPagedSalesByDateRangeCompletedEventHandler getPagedSalesByDateRangeCompleted;

		// Token: 0x14000073 RID: 115
		// (add) Token: 0x06000D16 RID: 3350 RVA: 0x000120DC File Offset: 0x000102DC
		// (remove) Token: 0x06000D17 RID: 3351 RVA: 0x00012114 File Offset: 0x00010314
		public event getReasonsToCancelSaleCompletedEventHandler getReasonsToCancelSaleCompleted;

		// Token: 0x14000074 RID: 116
		// (add) Token: 0x06000D18 RID: 3352 RVA: 0x0001214C File Offset: 0x0001034C
		// (remove) Token: 0x06000D19 RID: 3353 RVA: 0x00012184 File Offset: 0x00010384
		public event getSaleCompletedEventHandler getSaleCompleted;

		// Token: 0x14000075 RID: 117
		// (add) Token: 0x06000D1A RID: 3354 RVA: 0x000121BC File Offset: 0x000103BC
		// (remove) Token: 0x06000D1B RID: 3355 RVA: 0x000121F4 File Offset: 0x000103F4
		public event getSalesCompletedEventHandler getSalesCompleted;

		// Token: 0x14000076 RID: 118
		// (add) Token: 0x06000D1C RID: 3356 RVA: 0x0001222C File Offset: 0x0001042C
		// (remove) Token: 0x06000D1D RID: 3357 RVA: 0x00012264 File Offset: 0x00010464
		public event getSalesByDateRangeCompletedEventHandler getSalesByDateRangeCompleted;

		// Token: 0x14000077 RID: 119
		// (add) Token: 0x06000D1E RID: 3358 RVA: 0x0001229C File Offset: 0x0001049C
		// (remove) Token: 0x06000D1F RID: 3359 RVA: 0x000122D4 File Offset: 0x000104D4
		public event getServiceNameCompletedEventHandler getServiceNameCompleted;

		// Token: 0x14000078 RID: 120
		// (add) Token: 0x06000D20 RID: 3360 RVA: 0x0001230C File Offset: 0x0001050C
		// (remove) Token: 0x06000D21 RID: 3361 RVA: 0x00012344 File Offset: 0x00010544
		public event giveApprovalForRemandedItemCompletedEventHandler giveApprovalForRemandedItemCompleted;

		// Token: 0x14000079 RID: 121
		// (add) Token: 0x06000D22 RID: 3362 RVA: 0x0001237C File Offset: 0x0001057C
		// (remove) Token: 0x06000D23 RID: 3363 RVA: 0x000123B4 File Offset: 0x000105B4
		public event giveRateAndCommentCompletedEventHandler giveRateAndCommentCompleted;

		// Token: 0x1400007A RID: 122
		// (add) Token: 0x06000D24 RID: 3364 RVA: 0x000123EC File Offset: 0x000105EC
		// (remove) Token: 0x06000D25 RID: 3365 RVA: 0x00012424 File Offset: 0x00010624
		public event receiveRemandedItemCompletedEventHandler receiveRemandedItemCompleted;

		// Token: 0x1400007B RID: 123
		// (add) Token: 0x06000D26 RID: 3366 RVA: 0x0001245C File Offset: 0x0001065C
		// (remove) Token: 0x06000D27 RID: 3367 RVA: 0x00012494 File Offset: 0x00010694
		public event remindForApprovalCompletedEventHandler remindForApprovalCompleted;

		// Token: 0x1400007C RID: 124
		// (add) Token: 0x06000D28 RID: 3368 RVA: 0x000124CC File Offset: 0x000106CC
		// (remove) Token: 0x06000D29 RID: 3369 RVA: 0x00012504 File Offset: 0x00010704
		public event removeSaleFromListCompletedEventHandler removeSaleFromListCompleted;

		// Token: 0x1400007D RID: 125
		// (add) Token: 0x06000D2A RID: 3370 RVA: 0x0001253C File Offset: 0x0001073C
		// (remove) Token: 0x06000D2B RID: 3371 RVA: 0x00012574 File Offset: 0x00010774
		public event replySaleCommentCompletedEventHandler replySaleCommentCompleted;

		// Token: 0x06000D2C RID: 3372 RVA: 0x000125AC File Offset: 0x000107AC
		[SoapRpcMethod("", RequestNamespace = "http://sale.individual.ws.listingapi.gg.com", ResponseNamespace = "http://sale.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public messageServiceResponse cancelSale(string apiKey, string sign, long time, string saleCode, int reasonId, string lang)
		{
			object[] array = base.Invoke("cancelSale", new object[]
			{
				apiKey,
				sign,
				time,
				saleCode,
				reasonId,
				lang
			});
			return (messageServiceResponse)array[0];
		}

		// Token: 0x06000D2D RID: 3373 RVA: 0x000125FA File Offset: 0x000107FA
		public void cancelSaleAsync(string apiKey, string sign, long time, string saleCode, int reasonId, string lang)
		{
			this.cancelSaleAsync(apiKey, sign, time, saleCode, reasonId, lang, null);
		}

		// Token: 0x06000D2E RID: 3374 RVA: 0x0001260C File Offset: 0x0001080C
		public void cancelSaleAsync(string apiKey, string sign, long time, string saleCode, int reasonId, string lang, object userState)
		{
			if (this.cancelSaleOperationCompleted == null)
			{
				this.cancelSaleOperationCompleted = new SendOrPostCallback(this.OncancelSaleOperationCompleted);
			}
			base.InvokeAsync("cancelSale", new object[]
			{
				apiKey,
				sign,
				time,
				saleCode,
				reasonId,
				lang
			}, this.cancelSaleOperationCompleted, userState);
		}

		// Token: 0x06000D2F RID: 3375 RVA: 0x00012674 File Offset: 0x00010874
		private void OncancelSaleOperationCompleted(object arg)
		{
			if (this.cancelSaleCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.cancelSaleCompleted(this, new cancelSaleCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x06000D30 RID: 3376 RVA: 0x000126BC File Offset: 0x000108BC
		[SoapRpcMethod("", RequestNamespace = "http://sale.individual.ws.listingapi.gg.com", ResponseNamespace = "http://sale.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public messageServiceResponse cancelSaleAfterEarlyCancellationRequest(string apiKey, string sign, long time, string saleCode, string lang)
		{
			object[] array = base.Invoke("cancelSaleAfterEarlyCancellationRequest", new object[]
			{
				apiKey,
				sign,
				time,
				saleCode,
				lang
			});
			return (messageServiceResponse)array[0];
		}

		// Token: 0x06000D31 RID: 3377 RVA: 0x00012700 File Offset: 0x00010900
		public void cancelSaleAfterEarlyCancellationRequestAsync(string apiKey, string sign, long time, string saleCode, string lang)
		{
			this.cancelSaleAfterEarlyCancellationRequestAsync(apiKey, sign, time, saleCode, lang, null);
		}

		// Token: 0x06000D32 RID: 3378 RVA: 0x00012710 File Offset: 0x00010910
		public void cancelSaleAfterEarlyCancellationRequestAsync(string apiKey, string sign, long time, string saleCode, string lang, object userState)
		{
			if (this.cancelSaleAfterEarlyCancellationRequestOperationCompleted == null)
			{
				this.cancelSaleAfterEarlyCancellationRequestOperationCompleted = new SendOrPostCallback(this.OncancelSaleAfterEarlyCancellationRequestOperationCompleted);
			}
			base.InvokeAsync("cancelSaleAfterEarlyCancellationRequest", new object[]
			{
				apiKey,
				sign,
				time,
				saleCode,
				lang
			}, this.cancelSaleAfterEarlyCancellationRequestOperationCompleted, userState);
		}

		// Token: 0x06000D33 RID: 3379 RVA: 0x00012770 File Offset: 0x00010970
		private void OncancelSaleAfterEarlyCancellationRequestOperationCompleted(object arg)
		{
			if (this.cancelSaleAfterEarlyCancellationRequestCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.cancelSaleAfterEarlyCancellationRequestCompleted(this, new cancelSaleAfterEarlyCancellationRequestCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x06000D34 RID: 3380 RVA: 0x000127B8 File Offset: 0x000109B8
		[SoapRpcMethod("", RequestNamespace = "http://sale.individual.ws.listingapi.gg.com", ResponseNamespace = "http://sale.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public accountTransactionListResponse getAccountTransactions(string apiKey, string sign, long time, string startDate, string endDate, int startOffSet, int rowCount, string lang)
		{
			object[] array = base.Invoke("getAccountTransactions", new object[]
			{
				apiKey,
				sign,
				time,
				startDate,
				endDate,
				startOffSet,
				rowCount,
				lang
			});
			return (accountTransactionListResponse)array[0];
		}

		// Token: 0x06000D35 RID: 3381 RVA: 0x00012818 File Offset: 0x00010A18
		public void getAccountTransactionsAsync(string apiKey, string sign, long time, string startDate, string endDate, int startOffSet, int rowCount, string lang)
		{
			this.getAccountTransactionsAsync(apiKey, sign, time, startDate, endDate, startOffSet, rowCount, lang, null);
		}

		// Token: 0x06000D36 RID: 3382 RVA: 0x0001283C File Offset: 0x00010A3C
		public void getAccountTransactionsAsync(string apiKey, string sign, long time, string startDate, string endDate, int startOffSet, int rowCount, string lang, object userState)
		{
			if (this.getAccountTransactionsOperationCompleted == null)
			{
				this.getAccountTransactionsOperationCompleted = new SendOrPostCallback(this.OngetAccountTransactionsOperationCompleted);
			}
			base.InvokeAsync("getAccountTransactions", new object[]
			{
				apiKey,
				sign,
				time,
				startDate,
				endDate,
				startOffSet,
				rowCount,
				lang
			}, this.getAccountTransactionsOperationCompleted, userState);
		}

		// Token: 0x06000D37 RID: 3383 RVA: 0x000128B4 File Offset: 0x00010AB4
		private void OngetAccountTransactionsOperationCompleted(object arg)
		{
			if (this.getAccountTransactionsCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getAccountTransactionsCompleted(this, new getAccountTransactionsCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x06000D38 RID: 3384 RVA: 0x000128FC File Offset: 0x00010AFC
		[SoapRpcMethod("", RequestNamespace = "http://sale.individual.ws.listingapi.gg.com", ResponseNamespace = "http://sale.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public saleServiceBuyerResponse getItemBuyers(string apiKey, string sign, long time, string byStatus, int startOffSet, int rowCount, string lang)
		{
			object[] array = base.Invoke("getItemBuyers", new object[]
			{
				apiKey,
				sign,
				time,
				byStatus,
				startOffSet,
				rowCount,
				lang
			});
			return (saleServiceBuyerResponse)array[0];
		}

		// Token: 0x06000D39 RID: 3385 RVA: 0x00012954 File Offset: 0x00010B54
		public void getItemBuyersAsync(string apiKey, string sign, long time, string byStatus, int startOffSet, int rowCount, string lang)
		{
			this.getItemBuyersAsync(apiKey, sign, time, byStatus, startOffSet, rowCount, lang, null);
		}

		// Token: 0x06000D3A RID: 3386 RVA: 0x00012974 File Offset: 0x00010B74
		public void getItemBuyersAsync(string apiKey, string sign, long time, string byStatus, int startOffSet, int rowCount, string lang, object userState)
		{
			if (this.getItemBuyersOperationCompleted == null)
			{
				this.getItemBuyersOperationCompleted = new SendOrPostCallback(this.OngetItemBuyersOperationCompleted);
			}
			base.InvokeAsync("getItemBuyers", new object[]
			{
				apiKey,
				sign,
				time,
				byStatus,
				startOffSet,
				rowCount,
				lang
			}, this.getItemBuyersOperationCompleted, userState);
		}

		// Token: 0x06000D3B RID: 3387 RVA: 0x000129E8 File Offset: 0x00010BE8
		private void OngetItemBuyersOperationCompleted(object arg)
		{
			if (this.getItemBuyersCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getItemBuyersCompleted(this, new getItemBuyersCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x06000D3C RID: 3388 RVA: 0x00012A30 File Offset: 0x00010C30
		[SoapRpcMethod("", RequestNamespace = "http://sale.individual.ws.listingapi.gg.com", ResponseNamespace = "http://sale.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public saleServicePagingResponse getPagedSales(string apiKey, string sign, long time, bool withData, string byStatus, string byUser, string orderBy, string orderType, int pageNumber, int pageSize, string lang)
		{
			object[] array = base.Invoke("getPagedSales", new object[]
			{
				apiKey,
				sign,
				time,
				withData,
				byStatus,
				byUser,
				orderBy,
				orderType,
				pageNumber,
				pageSize,
				lang
			});
			return (saleServicePagingResponse)array[0];
		}

		// Token: 0x06000D3D RID: 3389 RVA: 0x00012AA4 File Offset: 0x00010CA4
		public void getPagedSalesAsync(string apiKey, string sign, long time, bool withData, string byStatus, string byUser, string orderBy, string orderType, int pageNumber, int pageSize, string lang)
		{
			this.getPagedSalesAsync(apiKey, sign, time, withData, byStatus, byUser, orderBy, orderType, pageNumber, pageSize, lang, null);
		}

		// Token: 0x06000D3E RID: 3390 RVA: 0x00012ACC File Offset: 0x00010CCC
		public void getPagedSalesAsync(string apiKey, string sign, long time, bool withData, string byStatus, string byUser, string orderBy, string orderType, int pageNumber, int pageSize, string lang, object userState)
		{
			if (this.getPagedSalesOperationCompleted == null)
			{
				this.getPagedSalesOperationCompleted = new SendOrPostCallback(this.OngetPagedSalesOperationCompleted);
			}
			base.InvokeAsync("getPagedSales", new object[]
			{
				apiKey,
				sign,
				time,
				withData,
				byStatus,
				byUser,
				orderBy,
				orderType,
				pageNumber,
				pageSize,
				lang
			}, this.getPagedSalesOperationCompleted, userState);
		}

		// Token: 0x06000D3F RID: 3391 RVA: 0x00012B5C File Offset: 0x00010D5C
		private void OngetPagedSalesOperationCompleted(object arg)
		{
			if (this.getPagedSalesCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getPagedSalesCompleted(this, new getPagedSalesCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x06000D40 RID: 3392 RVA: 0x00012BA4 File Offset: 0x00010DA4
		[SoapRpcMethod("", RequestNamespace = "http://sale.individual.ws.listingapi.gg.com", ResponseNamespace = "http://sale.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public saleServicePagingResponse getPagedSalesByDateRange(string apiKey, string sign, long time, bool withData, string byStatus, string byUser, string orderBy, string orderType, string startDate, string endDate, int pageNumber, int pageSize, string lang)
		{
			object[] array = base.Invoke("getPagedSalesByDateRange", new object[]
			{
				apiKey,
				sign,
				time,
				withData,
				byStatus,
				byUser,
				orderBy,
				orderType,
				startDate,
				endDate,
				pageNumber,
				pageSize,
				lang
			});
			return (saleServicePagingResponse)array[0];
		}

		// Token: 0x06000D41 RID: 3393 RVA: 0x00012C24 File Offset: 0x00010E24
		public void getPagedSalesByDateRangeAsync(string apiKey, string sign, long time, bool withData, string byStatus, string byUser, string orderBy, string orderType, string startDate, string endDate, int pageNumber, int pageSize, string lang)
		{
			this.getPagedSalesByDateRangeAsync(apiKey, sign, time, withData, byStatus, byUser, orderBy, orderType, startDate, endDate, pageNumber, pageSize, lang, null);
		}

		// Token: 0x06000D42 RID: 3394 RVA: 0x00012C50 File Offset: 0x00010E50
		public void getPagedSalesByDateRangeAsync(string apiKey, string sign, long time, bool withData, string byStatus, string byUser, string orderBy, string orderType, string startDate, string endDate, int pageNumber, int pageSize, string lang, object userState)
		{
			if (this.getPagedSalesByDateRangeOperationCompleted == null)
			{
				this.getPagedSalesByDateRangeOperationCompleted = new SendOrPostCallback(this.OngetPagedSalesByDateRangeOperationCompleted);
			}
			base.InvokeAsync("getPagedSalesByDateRange", new object[]
			{
				apiKey,
				sign,
				time,
				withData,
				byStatus,
				byUser,
				orderBy,
				orderType,
				startDate,
				endDate,
				pageNumber,
				pageSize,
				lang
			}, this.getPagedSalesByDateRangeOperationCompleted, userState);
		}

		// Token: 0x06000D43 RID: 3395 RVA: 0x00012CEC File Offset: 0x00010EEC
		private void OngetPagedSalesByDateRangeOperationCompleted(object arg)
		{
			if (this.getPagedSalesByDateRangeCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getPagedSalesByDateRangeCompleted(this, new getPagedSalesByDateRangeCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x06000D44 RID: 3396 RVA: 0x00012D34 File Offset: 0x00010F34
		[SoapRpcMethod("", RequestNamespace = "http://sale.individual.ws.listingapi.gg.com", ResponseNamespace = "http://sale.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public saleServiceCancelReasonResponse getReasonsToCancelSale(string apiKey, string sign, long time, string lang)
		{
			object[] array = base.Invoke("getReasonsToCancelSale", new object[]
			{
				apiKey,
				sign,
				time,
				lang
			});
			return (saleServiceCancelReasonResponse)array[0];
		}

		// Token: 0x06000D45 RID: 3397 RVA: 0x00012D73 File Offset: 0x00010F73
		public void getReasonsToCancelSaleAsync(string apiKey, string sign, long time, string lang)
		{
			this.getReasonsToCancelSaleAsync(apiKey, sign, time, lang, null);
		}

		// Token: 0x06000D46 RID: 3398 RVA: 0x00012D84 File Offset: 0x00010F84
		public void getReasonsToCancelSaleAsync(string apiKey, string sign, long time, string lang, object userState)
		{
			if (this.getReasonsToCancelSaleOperationCompleted == null)
			{
				this.getReasonsToCancelSaleOperationCompleted = new SendOrPostCallback(this.OngetReasonsToCancelSaleOperationCompleted);
			}
			base.InvokeAsync("getReasonsToCancelSale", new object[]
			{
				apiKey,
				sign,
				time,
				lang
			}, this.getReasonsToCancelSaleOperationCompleted, userState);
		}

		// Token: 0x06000D47 RID: 3399 RVA: 0x00012DDC File Offset: 0x00010FDC
		private void OngetReasonsToCancelSaleOperationCompleted(object arg)
		{
			if (this.getReasonsToCancelSaleCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getReasonsToCancelSaleCompleted(this, new getReasonsToCancelSaleCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x06000D48 RID: 3400 RVA: 0x00012E24 File Offset: 0x00011024
		[SoapRpcMethod("", RequestNamespace = "http://sale.individual.ws.listingapi.gg.com", ResponseNamespace = "http://sale.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public saleServiceResponse getSale(string apiKey, string sign, long time, string saleCode, string lang)
		{
			object[] array = base.Invoke("getSale", new object[]
			{
				apiKey,
				sign,
				time,
				saleCode,
				lang
			});
			return (saleServiceResponse)array[0];
		}

		// Token: 0x06000D49 RID: 3401 RVA: 0x00012E68 File Offset: 0x00011068
		public void getSaleAsync(string apiKey, string sign, long time, string saleCode, string lang)
		{
			this.getSaleAsync(apiKey, sign, time, saleCode, lang, null);
		}

		// Token: 0x06000D4A RID: 3402 RVA: 0x00012E78 File Offset: 0x00011078
		public void getSaleAsync(string apiKey, string sign, long time, string saleCode, string lang, object userState)
		{
			if (this.getSaleOperationCompleted == null)
			{
				this.getSaleOperationCompleted = new SendOrPostCallback(this.OngetSaleOperationCompleted);
			}
			base.InvokeAsync("getSale", new object[]
			{
				apiKey,
				sign,
				time,
				saleCode,
				lang
			}, this.getSaleOperationCompleted, userState);
		}

		// Token: 0x06000D4B RID: 3403 RVA: 0x00012ED8 File Offset: 0x000110D8
		private void OngetSaleOperationCompleted(object arg)
		{
			if (this.getSaleCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getSaleCompleted(this, new getSaleCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x06000D4C RID: 3404 RVA: 0x00012F20 File Offset: 0x00011120
		[SoapRpcMethod("", RequestNamespace = "http://sale.individual.ws.listingapi.gg.com", ResponseNamespace = "http://sale.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public saleServicePagingResponse getSales(string apiKey, string sign, long time, bool withData, string byStatus, string byUser, string orderBy, string orderType, int pageNumber, int pageSize, string lang)
		{
			object[] array = base.Invoke("getSales", new object[]
			{
				apiKey,
				sign,
				time,
				withData,
				byStatus,
				byUser,
				orderBy,
				orderType,
				pageNumber,
				pageSize,
				lang
			});
			return (saleServicePagingResponse)array[0];
		}

		// Token: 0x06000D4D RID: 3405 RVA: 0x00012F94 File Offset: 0x00011194
		public void getSalesAsync(string apiKey, string sign, long time, bool withData, string byStatus, string byUser, string orderBy, string orderType, int pageNumber, int pageSize, string lang)
		{
			this.getSalesAsync(apiKey, sign, time, withData, byStatus, byUser, orderBy, orderType, pageNumber, pageSize, lang, null);
		}

		// Token: 0x06000D4E RID: 3406 RVA: 0x00012FBC File Offset: 0x000111BC
		public void getSalesAsync(string apiKey, string sign, long time, bool withData, string byStatus, string byUser, string orderBy, string orderType, int pageNumber, int pageSize, string lang, object userState)
		{
			if (this.getSalesOperationCompleted == null)
			{
				this.getSalesOperationCompleted = new SendOrPostCallback(this.OngetSalesOperationCompleted);
			}
			base.InvokeAsync("getSales", new object[]
			{
				apiKey,
				sign,
				time,
				withData,
				byStatus,
				byUser,
				orderBy,
				orderType,
				pageNumber,
				pageSize,
				lang
			}, this.getSalesOperationCompleted, userState);
		}

		// Token: 0x06000D4F RID: 3407 RVA: 0x0001304C File Offset: 0x0001124C
		private void OngetSalesOperationCompleted(object arg)
		{
			if (this.getSalesCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getSalesCompleted(this, new getSalesCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x06000D50 RID: 3408 RVA: 0x00013094 File Offset: 0x00011294
		[SoapRpcMethod("", RequestNamespace = "http://sale.individual.ws.listingapi.gg.com", ResponseNamespace = "http://sale.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public saleServicePagingResponse getSalesByDateRange(string apiKey, string sign, long time, bool withData, string byStatus, string byUser, string orderBy, string orderType, string startDate, string endDate, int pageNumber, int pageSize, string lang)
		{
			object[] array = base.Invoke("getSalesByDateRange", new object[]
			{
				apiKey,
				sign,
				time,
				withData,
				byStatus,
				byUser,
				orderBy,
				orderType,
				startDate,
				endDate,
				pageNumber,
				pageSize,
				lang
			});
			return (saleServicePagingResponse)array[0];
		}

		// Token: 0x06000D51 RID: 3409 RVA: 0x00013114 File Offset: 0x00011314
		public void getSalesByDateRangeAsync(string apiKey, string sign, long time, bool withData, string byStatus, string byUser, string orderBy, string orderType, string startDate, string endDate, int pageNumber, int pageSize, string lang)
		{
			this.getSalesByDateRangeAsync(apiKey, sign, time, withData, byStatus, byUser, orderBy, orderType, startDate, endDate, pageNumber, pageSize, lang, null);
		}

		// Token: 0x06000D52 RID: 3410 RVA: 0x00013140 File Offset: 0x00011340
		public void getSalesByDateRangeAsync(string apiKey, string sign, long time, bool withData, string byStatus, string byUser, string orderBy, string orderType, string startDate, string endDate, int pageNumber, int pageSize, string lang, object userState)
		{
			if (this.getSalesByDateRangeOperationCompleted == null)
			{
				this.getSalesByDateRangeOperationCompleted = new SendOrPostCallback(this.OngetSalesByDateRangeOperationCompleted);
			}
			base.InvokeAsync("getSalesByDateRange", new object[]
			{
				apiKey,
				sign,
				time,
				withData,
				byStatus,
				byUser,
				orderBy,
				orderType,
				startDate,
				endDate,
				pageNumber,
				pageSize,
				lang
			}, this.getSalesByDateRangeOperationCompleted, userState);
		}

		// Token: 0x06000D53 RID: 3411 RVA: 0x000131DC File Offset: 0x000113DC
		private void OngetSalesByDateRangeOperationCompleted(object arg)
		{
			if (this.getSalesByDateRangeCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getSalesByDateRangeCompleted(this, new getSalesByDateRangeCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x06000D54 RID: 3412 RVA: 0x00013224 File Offset: 0x00011424
		[SoapRpcMethod("", RequestNamespace = "http://sale.individual.ws.listingapi.gg.com", ResponseNamespace = "http://sale.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public string getServiceName()
		{
			object[] array = base.Invoke("getServiceName", new object[0]);
			return (string)array[0];
		}

		// Token: 0x06000D55 RID: 3413 RVA: 0x0001324B File Offset: 0x0001144B
		public void getServiceNameAsync()
		{
			this.getServiceNameAsync(null);
		}

		// Token: 0x06000D56 RID: 3414 RVA: 0x00013254 File Offset: 0x00011454
		public void getServiceNameAsync(object userState)
		{
			if (this.getServiceNameOperationCompleted == null)
			{
				this.getServiceNameOperationCompleted = new SendOrPostCallback(this.OngetServiceNameOperationCompleted);
			}
			base.InvokeAsync("getServiceName", new object[0], this.getServiceNameOperationCompleted, userState);
		}

		// Token: 0x06000D57 RID: 3415 RVA: 0x00013288 File Offset: 0x00011488
		private void OngetServiceNameOperationCompleted(object arg)
		{
			if (this.getServiceNameCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.getServiceNameCompleted(this, new getServiceNameCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x06000D58 RID: 3416 RVA: 0x000132D0 File Offset: 0x000114D0
		[SoapRpcMethod("", RequestNamespace = "http://sale.individual.ws.listingapi.gg.com", ResponseNamespace = "http://sale.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public messageServiceResponse giveApprovalForRemandedItem(string apiKey, string sign, long time, string saleCode, string lang)
		{
			object[] array = base.Invoke("giveApprovalForRemandedItem", new object[]
			{
				apiKey,
				sign,
				time,
				saleCode,
				lang
			});
			return (messageServiceResponse)array[0];
		}

		// Token: 0x06000D59 RID: 3417 RVA: 0x00013314 File Offset: 0x00011514
		public void giveApprovalForRemandedItemAsync(string apiKey, string sign, long time, string saleCode, string lang)
		{
			this.giveApprovalForRemandedItemAsync(apiKey, sign, time, saleCode, lang, null);
		}

		// Token: 0x06000D5A RID: 3418 RVA: 0x00013324 File Offset: 0x00011524
		public void giveApprovalForRemandedItemAsync(string apiKey, string sign, long time, string saleCode, string lang, object userState)
		{
			if (this.giveApprovalForRemandedItemOperationCompleted == null)
			{
				this.giveApprovalForRemandedItemOperationCompleted = new SendOrPostCallback(this.OngiveApprovalForRemandedItemOperationCompleted);
			}
			base.InvokeAsync("giveApprovalForRemandedItem", new object[]
			{
				apiKey,
				sign,
				time,
				saleCode,
				lang
			}, this.giveApprovalForRemandedItemOperationCompleted, userState);
		}

		// Token: 0x06000D5B RID: 3419 RVA: 0x00013384 File Offset: 0x00011584
		private void OngiveApprovalForRemandedItemOperationCompleted(object arg)
		{
			if (this.giveApprovalForRemandedItemCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.giveApprovalForRemandedItemCompleted(this, new giveApprovalForRemandedItemCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x06000D5C RID: 3420 RVA: 0x000133CC File Offset: 0x000115CC
		[SoapRpcMethod("", RequestNamespace = "http://sale.individual.ws.listingapi.gg.com", ResponseNamespace = "http://sale.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public commonSaleResponse giveRateAndComment(string apiKey, string sign, long time, string userType, int productId, int rate, string comment, string lang)
		{
			object[] array = base.Invoke("giveRateAndComment", new object[]
			{
				apiKey,
				sign,
				time,
				userType,
				productId,
				rate,
				comment,
				lang
			});
			return (commonSaleResponse)array[0];
		}

		// Token: 0x06000D5D RID: 3421 RVA: 0x0001342C File Offset: 0x0001162C
		public void giveRateAndCommentAsync(string apiKey, string sign, long time, string userType, int productId, int rate, string comment, string lang)
		{
			this.giveRateAndCommentAsync(apiKey, sign, time, userType, productId, rate, comment, lang, null);
		}

		// Token: 0x06000D5E RID: 3422 RVA: 0x00013450 File Offset: 0x00011650
		public void giveRateAndCommentAsync(string apiKey, string sign, long time, string userType, int productId, int rate, string comment, string lang, object userState)
		{
			if (this.giveRateAndCommentOperationCompleted == null)
			{
				this.giveRateAndCommentOperationCompleted = new SendOrPostCallback(this.OngiveRateAndCommentOperationCompleted);
			}
			base.InvokeAsync("giveRateAndComment", new object[]
			{
				apiKey,
				sign,
				time,
				userType,
				productId,
				rate,
				comment,
				lang
			}, this.giveRateAndCommentOperationCompleted, userState);
		}

		// Token: 0x06000D5F RID: 3423 RVA: 0x000134C8 File Offset: 0x000116C8
		private void OngiveRateAndCommentOperationCompleted(object arg)
		{
			if (this.giveRateAndCommentCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.giveRateAndCommentCompleted(this, new giveRateAndCommentCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x06000D60 RID: 3424 RVA: 0x00013510 File Offset: 0x00011710
		[SoapRpcMethod("", RequestNamespace = "http://sale.individual.ws.listingapi.gg.com", ResponseNamespace = "http://sale.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public messageServiceResponse receiveRemandedItem(string apiKey, string sign, long time, string saleCode, string lang)
		{
			object[] array = base.Invoke("receiveRemandedItem", new object[]
			{
				apiKey,
				sign,
				time,
				saleCode,
				lang
			});
			return (messageServiceResponse)array[0];
		}

		// Token: 0x06000D61 RID: 3425 RVA: 0x00013554 File Offset: 0x00011754
		public void receiveRemandedItemAsync(string apiKey, string sign, long time, string saleCode, string lang)
		{
			this.receiveRemandedItemAsync(apiKey, sign, time, saleCode, lang, null);
		}

		// Token: 0x06000D62 RID: 3426 RVA: 0x00013564 File Offset: 0x00011764
		public void receiveRemandedItemAsync(string apiKey, string sign, long time, string saleCode, string lang, object userState)
		{
			if (this.receiveRemandedItemOperationCompleted == null)
			{
				this.receiveRemandedItemOperationCompleted = new SendOrPostCallback(this.OnreceiveRemandedItemOperationCompleted);
			}
			base.InvokeAsync("receiveRemandedItem", new object[]
			{
				apiKey,
				sign,
				time,
				saleCode,
				lang
			}, this.receiveRemandedItemOperationCompleted, userState);
		}

		// Token: 0x06000D63 RID: 3427 RVA: 0x000135C4 File Offset: 0x000117C4
		private void OnreceiveRemandedItemOperationCompleted(object arg)
		{
			if (this.receiveRemandedItemCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.receiveRemandedItemCompleted(this, new receiveRemandedItemCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x06000D64 RID: 3428 RVA: 0x0001360C File Offset: 0x0001180C
		[SoapRpcMethod("", RequestNamespace = "http://sale.individual.ws.listingapi.gg.com", ResponseNamespace = "http://sale.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public messageServiceResponse remindForApproval(string apiKey, string sign, long time, string saleCode, string lang)
		{
			object[] array = base.Invoke("remindForApproval", new object[]
			{
				apiKey,
				sign,
				time,
				saleCode,
				lang
			});
			return (messageServiceResponse)array[0];
		}

		// Token: 0x06000D65 RID: 3429 RVA: 0x00013650 File Offset: 0x00011850
		public void remindForApprovalAsync(string apiKey, string sign, long time, string saleCode, string lang)
		{
			this.remindForApprovalAsync(apiKey, sign, time, saleCode, lang, null);
		}

		// Token: 0x06000D66 RID: 3430 RVA: 0x00013660 File Offset: 0x00011860
		public void remindForApprovalAsync(string apiKey, string sign, long time, string saleCode, string lang, object userState)
		{
			if (this.remindForApprovalOperationCompleted == null)
			{
				this.remindForApprovalOperationCompleted = new SendOrPostCallback(this.OnremindForApprovalOperationCompleted);
			}
			base.InvokeAsync("remindForApproval", new object[]
			{
				apiKey,
				sign,
				time,
				saleCode,
				lang
			}, this.remindForApprovalOperationCompleted, userState);
		}

		// Token: 0x06000D67 RID: 3431 RVA: 0x000136C0 File Offset: 0x000118C0
		private void OnremindForApprovalOperationCompleted(object arg)
		{
			if (this.remindForApprovalCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.remindForApprovalCompleted(this, new remindForApprovalCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x06000D68 RID: 3432 RVA: 0x00013708 File Offset: 0x00011908
		[SoapRpcMethod("", RequestNamespace = "http://sale.individual.ws.listingapi.gg.com", ResponseNamespace = "http://sale.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public messageServiceResponse removeSaleFromList(string apiKey, string sign, long time, string saleCode, string userType, string lang)
		{
			object[] array = base.Invoke("removeSaleFromList", new object[]
			{
				apiKey,
				sign,
				time,
				saleCode,
				userType,
				lang
			});
			return (messageServiceResponse)array[0];
		}

		// Token: 0x06000D69 RID: 3433 RVA: 0x00013751 File Offset: 0x00011951
		public void removeSaleFromListAsync(string apiKey, string sign, long time, string saleCode, string userType, string lang)
		{
			this.removeSaleFromListAsync(apiKey, sign, time, saleCode, userType, lang, null);
		}

		// Token: 0x06000D6A RID: 3434 RVA: 0x00013764 File Offset: 0x00011964
		public void removeSaleFromListAsync(string apiKey, string sign, long time, string saleCode, string userType, string lang, object userState)
		{
			if (this.removeSaleFromListOperationCompleted == null)
			{
				this.removeSaleFromListOperationCompleted = new SendOrPostCallback(this.OnremoveSaleFromListOperationCompleted);
			}
			base.InvokeAsync("removeSaleFromList", new object[]
			{
				apiKey,
				sign,
				time,
				saleCode,
				userType,
				lang
			}, this.removeSaleFromListOperationCompleted, userState);
		}

		// Token: 0x06000D6B RID: 3435 RVA: 0x000137C8 File Offset: 0x000119C8
		private void OnremoveSaleFromListOperationCompleted(object arg)
		{
			if (this.removeSaleFromListCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.removeSaleFromListCompleted(this, new removeSaleFromListCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x06000D6C RID: 3436 RVA: 0x00013810 File Offset: 0x00011A10
		[SoapRpcMethod("", RequestNamespace = "http://sale.individual.ws.listingapi.gg.com", ResponseNamespace = "http://sale.individual.ws.listingapi.gg.com", Use = SoapBindingUse.Literal)]
		[return: XmlElement("return")]
		public commonSaleResponse replySaleComment(string apiKey, string sign, long time, string userType, int productId, string comment, string lang)
		{
			object[] array = base.Invoke("replySaleComment", new object[]
			{
				apiKey,
				sign,
				time,
				userType,
				productId,
				comment,
				lang
			});
			return (commonSaleResponse)array[0];
		}

		// Token: 0x06000D6D RID: 3437 RVA: 0x00013864 File Offset: 0x00011A64
		public void replySaleCommentAsync(string apiKey, string sign, long time, string userType, int productId, string comment, string lang)
		{
			this.replySaleCommentAsync(apiKey, sign, time, userType, productId, comment, lang, null);
		}

		// Token: 0x06000D6E RID: 3438 RVA: 0x00013884 File Offset: 0x00011A84
		public void replySaleCommentAsync(string apiKey, string sign, long time, string userType, int productId, string comment, string lang, object userState)
		{
			if (this.replySaleCommentOperationCompleted == null)
			{
				this.replySaleCommentOperationCompleted = new SendOrPostCallback(this.OnreplySaleCommentOperationCompleted);
			}
			base.InvokeAsync("replySaleComment", new object[]
			{
				apiKey,
				sign,
				time,
				userType,
				productId,
				comment,
				lang
			}, this.replySaleCommentOperationCompleted, userState);
		}

		// Token: 0x06000D6F RID: 3439 RVA: 0x000138F0 File Offset: 0x00011AF0
		private void OnreplySaleCommentOperationCompleted(object arg)
		{
			if (this.replySaleCommentCompleted != null)
			{
				InvokeCompletedEventArgs invokeCompletedEventArgs = (InvokeCompletedEventArgs)arg;
				this.replySaleCommentCompleted(this, new replySaleCommentCompletedEventArgs(invokeCompletedEventArgs.Results, invokeCompletedEventArgs.Error, invokeCompletedEventArgs.Cancelled, invokeCompletedEventArgs.UserState));
			}
		}

		// Token: 0x06000D70 RID: 3440 RVA: 0x00013935 File Offset: 0x00011B35
		public new void CancelAsync(object userState)
		{
			base.CancelAsync(userState);
		}

		// Token: 0x06000D71 RID: 3441 RVA: 0x00013940 File Offset: 0x00011B40
		private bool IsLocalFileSystemWebService(string url)
		{
			if (url == null || url == string.Empty)
			{
				return false;
			}
			Uri uri = new Uri(url);
			return uri.Port >= 1024 && string.Compare(uri.Host, "localHost", StringComparison.OrdinalIgnoreCase) == 0;
		}

		// Token: 0x040004CF RID: 1231
		private SendOrPostCallback cancelSaleOperationCompleted;

		// Token: 0x040004D0 RID: 1232
		private SendOrPostCallback cancelSaleAfterEarlyCancellationRequestOperationCompleted;

		// Token: 0x040004D1 RID: 1233
		private SendOrPostCallback getAccountTransactionsOperationCompleted;

		// Token: 0x040004D2 RID: 1234
		private SendOrPostCallback getItemBuyersOperationCompleted;

		// Token: 0x040004D3 RID: 1235
		private SendOrPostCallback getPagedSalesOperationCompleted;

		// Token: 0x040004D4 RID: 1236
		private SendOrPostCallback getPagedSalesByDateRangeOperationCompleted;

		// Token: 0x040004D5 RID: 1237
		private SendOrPostCallback getReasonsToCancelSaleOperationCompleted;

		// Token: 0x040004D6 RID: 1238
		private SendOrPostCallback getSaleOperationCompleted;

		// Token: 0x040004D7 RID: 1239
		private SendOrPostCallback getSalesOperationCompleted;

		// Token: 0x040004D8 RID: 1240
		private SendOrPostCallback getSalesByDateRangeOperationCompleted;

		// Token: 0x040004D9 RID: 1241
		private SendOrPostCallback getServiceNameOperationCompleted;

		// Token: 0x040004DA RID: 1242
		private SendOrPostCallback giveApprovalForRemandedItemOperationCompleted;

		// Token: 0x040004DB RID: 1243
		private SendOrPostCallback giveRateAndCommentOperationCompleted;

		// Token: 0x040004DC RID: 1244
		private SendOrPostCallback receiveRemandedItemOperationCompleted;

		// Token: 0x040004DD RID: 1245
		private SendOrPostCallback remindForApprovalOperationCompleted;

		// Token: 0x040004DE RID: 1246
		private SendOrPostCallback removeSaleFromListOperationCompleted;

		// Token: 0x040004DF RID: 1247
		private SendOrPostCallback replySaleCommentOperationCompleted;

		// Token: 0x040004E0 RID: 1248
		private bool useDefaultCredentialsSetExplicitly;
	}
}
