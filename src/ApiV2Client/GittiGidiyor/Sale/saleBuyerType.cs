﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Sale
{
	// Token: 0x02000189 RID: 393
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[XmlType(Namespace = "http://sale.individual.ws.listingapi.gg.com")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[Serializable]
	public class saleBuyerType
	{
		// Token: 0x17000417 RID: 1047
		// (get) Token: 0x06000D8E RID: 3470 RVA: 0x00013A75 File Offset: 0x00011C75
		// (set) Token: 0x06000D8F RID: 3471 RVA: 0x00013A7D File Offset: 0x00011C7D
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string nick
		{
			get
			{
				return this.nickField;
			}
			set
			{
				this.nickField = value;
			}
		}

		// Token: 0x17000418 RID: 1048
		// (get) Token: 0x06000D90 RID: 3472 RVA: 0x00013A86 File Offset: 0x00011C86
		// (set) Token: 0x06000D91 RID: 3473 RVA: 0x00013A8E File Offset: 0x00011C8E
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public long saleCount
		{
			get
			{
				return this.saleCountField;
			}
			set
			{
				this.saleCountField = value;
			}
		}

		// Token: 0x17000419 RID: 1049
		// (get) Token: 0x06000D92 RID: 3474 RVA: 0x00013A97 File Offset: 0x00011C97
		// (set) Token: 0x06000D93 RID: 3475 RVA: 0x00013A9F File Offset: 0x00011C9F
		[XmlIgnore]
		public bool saleCountSpecified
		{
			get
			{
				return this.saleCountFieldSpecified;
			}
			set
			{
				this.saleCountFieldSpecified = value;
			}
		}

		// Token: 0x040004FE RID: 1278
		private string nickField;

		// Token: 0x040004FF RID: 1279
		private long saleCountField;

		// Token: 0x04000500 RID: 1280
		private bool saleCountFieldSpecified;
	}
}
