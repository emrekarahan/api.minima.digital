﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Sale
{
	// Token: 0x0200018F RID: 399
	[XmlType(Namespace = "http://sale.individual.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[Serializable]
	public class saleServiceCancelReasonResponse : baseResponse
	{
		// Token: 0x17000462 RID: 1122
		// (get) Token: 0x06000E2A RID: 3626 RVA: 0x00013FA0 File Offset: 0x000121A0
		// (set) Token: 0x06000E2B RID: 3627 RVA: 0x00013FA8 File Offset: 0x000121A8
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int count
		{
			get
			{
				return this.countField;
			}
			set
			{
				this.countField = value;
			}
		}

		// Token: 0x17000463 RID: 1123
		// (get) Token: 0x06000E2C RID: 3628 RVA: 0x00013FB1 File Offset: 0x000121B1
		// (set) Token: 0x06000E2D RID: 3629 RVA: 0x00013FB9 File Offset: 0x000121B9
		[XmlIgnore]
		public bool countSpecified
		{
			get
			{
				return this.countFieldSpecified;
			}
			set
			{
				this.countFieldSpecified = value;
			}
		}

		// Token: 0x17000464 RID: 1124
		// (get) Token: 0x06000E2E RID: 3630 RVA: 0x00013FC2 File Offset: 0x000121C2
		// (set) Token: 0x06000E2F RID: 3631 RVA: 0x00013FCA File Offset: 0x000121CA
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		[XmlArrayItem("reason", Form = XmlSchemaForm.Unqualified)]
		public saleReasonType[] reasons
		{
			get
			{
				return this.reasonsField;
			}
			set
			{
				this.reasonsField = value;
			}
		}

		// Token: 0x04000549 RID: 1353
		private int countField;

		// Token: 0x0400054A RID: 1354
		private bool countFieldSpecified;

		// Token: 0x0400054B RID: 1355
		private saleReasonType[] reasonsField;
	}
}
