﻿using System.CodeDom.Compiler;

namespace ApiV2Client.GittiGidiyor.Sale
{
	// Token: 0x020001B5 RID: 437
	// (Invoke) Token: 0x06000EAF RID: 3759
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	public delegate void replySaleCommentCompletedEventHandler(object sender, replySaleCommentCompletedEventArgs e);
}
