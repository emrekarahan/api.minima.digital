﻿using System.CodeDom.Compiler;

namespace ApiV2Client.GittiGidiyor.Sale
{
	// Token: 0x020001A7 RID: 423
	// (Invoke) Token: 0x06000E85 RID: 3717
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	public delegate void getSalesByDateRangeCompletedEventHandler(object sender, getSalesByDateRangeCompletedEventArgs e);
}
