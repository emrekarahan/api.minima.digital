﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Sale
{
	// Token: 0x0200018A RID: 394
	[DebuggerStepThrough]
	[XmlType(Namespace = "http://sale.individual.ws.listingapi.gg.com")]
	[DesignerCategory("code")]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[Serializable]
	public class accountTransactionType
	{
		// Token: 0x1700041A RID: 1050
		// (get) Token: 0x06000D95 RID: 3477 RVA: 0x00013AB0 File Offset: 0x00011CB0
		// (set) Token: 0x06000D96 RID: 3478 RVA: 0x00013AB8 File Offset: 0x00011CB8
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string saleCode
		{
			get
			{
				return this.saleCodeField;
			}
			set
			{
				this.saleCodeField = value;
			}
		}

		// Token: 0x1700041B RID: 1051
		// (get) Token: 0x06000D97 RID: 3479 RVA: 0x00013AC1 File Offset: 0x00011CC1
		// (set) Token: 0x06000D98 RID: 3480 RVA: 0x00013AC9 File Offset: 0x00011CC9
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int productId
		{
			get
			{
				return this.productIdField;
			}
			set
			{
				this.productIdField = value;
			}
		}

		// Token: 0x1700041C RID: 1052
		// (get) Token: 0x06000D99 RID: 3481 RVA: 0x00013AD2 File Offset: 0x00011CD2
		// (set) Token: 0x06000D9A RID: 3482 RVA: 0x00013ADA File Offset: 0x00011CDA
		[XmlIgnore]
		public bool productIdSpecified
		{
			get
			{
				return this.productIdFieldSpecified;
			}
			set
			{
				this.productIdFieldSpecified = value;
			}
		}

		// Token: 0x1700041D RID: 1053
		// (get) Token: 0x06000D9B RID: 3483 RVA: 0x00013AE3 File Offset: 0x00011CE3
		// (set) Token: 0x06000D9C RID: 3484 RVA: 0x00013AEB File Offset: 0x00011CEB
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string productTitle
		{
			get
			{
				return this.productTitleField;
			}
			set
			{
				this.productTitleField = value;
			}
		}

		// Token: 0x1700041E RID: 1054
		// (get) Token: 0x06000D9D RID: 3485 RVA: 0x00013AF4 File Offset: 0x00011CF4
		// (set) Token: 0x06000D9E RID: 3486 RVA: 0x00013AFC File Offset: 0x00011CFC
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string buyerNick
		{
			get
			{
				return this.buyerNickField;
			}
			set
			{
				this.buyerNickField = value;
			}
		}

		// Token: 0x1700041F RID: 1055
		// (get) Token: 0x06000D9F RID: 3487 RVA: 0x00013B05 File Offset: 0x00011D05
		// (set) Token: 0x06000DA0 RID: 3488 RVA: 0x00013B0D File Offset: 0x00011D0D
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public decimal price
		{
			get
			{
				return this.priceField;
			}
			set
			{
				this.priceField = value;
			}
		}

		// Token: 0x17000420 RID: 1056
		// (get) Token: 0x06000DA1 RID: 3489 RVA: 0x00013B16 File Offset: 0x00011D16
		// (set) Token: 0x06000DA2 RID: 3490 RVA: 0x00013B1E File Offset: 0x00011D1E
		[XmlIgnore]
		public bool priceSpecified
		{
			get
			{
				return this.priceFieldSpecified;
			}
			set
			{
				this.priceFieldSpecified = value;
			}
		}

		// Token: 0x17000421 RID: 1057
		// (get) Token: 0x06000DA3 RID: 3491 RVA: 0x00013B27 File Offset: 0x00011D27
		// (set) Token: 0x06000DA4 RID: 3492 RVA: 0x00013B2F File Offset: 0x00011D2F
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public decimal ggCommission
		{
			get
			{
				return this.ggCommissionField;
			}
			set
			{
				this.ggCommissionField = value;
			}
		}

		// Token: 0x17000422 RID: 1058
		// (get) Token: 0x06000DA5 RID: 3493 RVA: 0x00013B38 File Offset: 0x00011D38
		// (set) Token: 0x06000DA6 RID: 3494 RVA: 0x00013B40 File Offset: 0x00011D40
		[XmlIgnore]
		public bool ggCommissionSpecified
		{
			get
			{
				return this.ggCommissionFieldSpecified;
			}
			set
			{
				this.ggCommissionFieldSpecified = value;
			}
		}

		// Token: 0x17000423 RID: 1059
		// (get) Token: 0x06000DA7 RID: 3495 RVA: 0x00013B49 File Offset: 0x00011D49
		// (set) Token: 0x06000DA8 RID: 3496 RVA: 0x00013B51 File Offset: 0x00011D51
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public decimal commissionPrice
		{
			get
			{
				return this.commissionPriceField;
			}
			set
			{
				this.commissionPriceField = value;
			}
		}

		// Token: 0x17000424 RID: 1060
		// (get) Token: 0x06000DA9 RID: 3497 RVA: 0x00013B5A File Offset: 0x00011D5A
		// (set) Token: 0x06000DAA RID: 3498 RVA: 0x00013B62 File Offset: 0x00011D62
		[XmlIgnore]
		public bool commissionPriceSpecified
		{
			get
			{
				return this.commissionPriceFieldSpecified;
			}
			set
			{
				this.commissionPriceFieldSpecified = value;
			}
		}

		// Token: 0x17000425 RID: 1061
		// (get) Token: 0x06000DAB RID: 3499 RVA: 0x00013B6B File Offset: 0x00011D6B
		// (set) Token: 0x06000DAC RID: 3500 RVA: 0x00013B73 File Offset: 0x00011D73
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public decimal paidPrice
		{
			get
			{
				return this.paidPriceField;
			}
			set
			{
				this.paidPriceField = value;
			}
		}

		// Token: 0x17000426 RID: 1062
		// (get) Token: 0x06000DAD RID: 3501 RVA: 0x00013B7C File Offset: 0x00011D7C
		// (set) Token: 0x06000DAE RID: 3502 RVA: 0x00013B84 File Offset: 0x00011D84
		[XmlIgnore]
		public bool paidPriceSpecified
		{
			get
			{
				return this.paidPriceFieldSpecified;
			}
			set
			{
				this.paidPriceFieldSpecified = value;
			}
		}

		// Token: 0x17000427 RID: 1063
		// (get) Token: 0x06000DAF RID: 3503 RVA: 0x00013B8D File Offset: 0x00011D8D
		// (set) Token: 0x06000DB0 RID: 3504 RVA: 0x00013B95 File Offset: 0x00011D95
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string approveDate
		{
			get
			{
				return this.approveDateField;
			}
			set
			{
				this.approveDateField = value;
			}
		}

		// Token: 0x17000428 RID: 1064
		// (get) Token: 0x06000DB1 RID: 3505 RVA: 0x00013B9E File Offset: 0x00011D9E
		// (set) Token: 0x06000DB2 RID: 3506 RVA: 0x00013BA6 File Offset: 0x00011DA6
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string approve
		{
			get
			{
				return this.approveField;
			}
			set
			{
				this.approveField = value;
			}
		}

		// Token: 0x17000429 RID: 1065
		// (get) Token: 0x06000DB3 RID: 3507 RVA: 0x00013BAF File Offset: 0x00011DAF
		// (set) Token: 0x06000DB4 RID: 3508 RVA: 0x00013BB7 File Offset: 0x00011DB7
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string accountNumber
		{
			get
			{
				return this.accountNumberField;
			}
			set
			{
				this.accountNumberField = value;
			}
		}

		// Token: 0x1700042A RID: 1066
		// (get) Token: 0x06000DB5 RID: 3509 RVA: 0x00013BC0 File Offset: 0x00011DC0
		// (set) Token: 0x06000DB6 RID: 3510 RVA: 0x00013BC8 File Offset: 0x00011DC8
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string iban
		{
			get
			{
				return this.ibanField;
			}
			set
			{
				this.ibanField = value;
			}
		}

		// Token: 0x04000501 RID: 1281
		private string saleCodeField;

		// Token: 0x04000502 RID: 1282
		private int productIdField;

		// Token: 0x04000503 RID: 1283
		private bool productIdFieldSpecified;

		// Token: 0x04000504 RID: 1284
		private string productTitleField;

		// Token: 0x04000505 RID: 1285
		private string buyerNickField;

		// Token: 0x04000506 RID: 1286
		private decimal priceField;

		// Token: 0x04000507 RID: 1287
		private bool priceFieldSpecified;

		// Token: 0x04000508 RID: 1288
		private decimal ggCommissionField;

		// Token: 0x04000509 RID: 1289
		private bool ggCommissionFieldSpecified;

		// Token: 0x0400050A RID: 1290
		private decimal commissionPriceField;

		// Token: 0x0400050B RID: 1291
		private bool commissionPriceFieldSpecified;

		// Token: 0x0400050C RID: 1292
		private decimal paidPriceField;

		// Token: 0x0400050D RID: 1293
		private bool paidPriceFieldSpecified;

		// Token: 0x0400050E RID: 1294
		private string approveDateField;

		// Token: 0x0400050F RID: 1295
		private string approveField;

		// Token: 0x04000510 RID: 1296
		private string accountNumberField;

		// Token: 0x04000511 RID: 1297
		private string ibanField;
	}
}
