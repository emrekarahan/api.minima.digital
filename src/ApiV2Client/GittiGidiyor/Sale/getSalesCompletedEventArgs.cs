﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Sale
{
	// Token: 0x020001A6 RID: 422
	[DesignerCategory("code")]
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	[DebuggerStepThrough]
	public class getSalesCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000E82 RID: 3714 RVA: 0x0001420F File Offset: 0x0001240F
		internal getSalesCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x17000479 RID: 1145
		// (get) Token: 0x06000E83 RID: 3715 RVA: 0x00014222 File Offset: 0x00012422
		public saleServicePagingResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (saleServicePagingResponse)this.results[0];
			}
		}

		// Token: 0x04000560 RID: 1376
		private object[] results;
	}
}
