﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Sale
{
	// Token: 0x02000193 RID: 403
	[DebuggerStepThrough]
	[XmlType(Namespace = "http://sale.individual.ws.listingapi.gg.com")]
	[XmlInclude(typeof(saleServicePagingResponse))]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[DesignerCategory("code")]
	[Serializable]
	public class saleServiceResponse : baseResponse
	{
		// Token: 0x1700046D RID: 1133
		// (get) Token: 0x06000E44 RID: 3652 RVA: 0x0001407B File Offset: 0x0001227B
		// (set) Token: 0x06000E45 RID: 3653 RVA: 0x00014083 File Offset: 0x00012283
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int saleCount
		{
			get
			{
				return this.saleCountField;
			}
			set
			{
				this.saleCountField = value;
			}
		}

		// Token: 0x1700046E RID: 1134
		// (get) Token: 0x06000E46 RID: 3654 RVA: 0x0001408C File Offset: 0x0001228C
		// (set) Token: 0x06000E47 RID: 3655 RVA: 0x00014094 File Offset: 0x00012294
		[XmlIgnore]
		public bool saleCountSpecified
		{
			get
			{
				return this.saleCountFieldSpecified;
			}
			set
			{
				this.saleCountFieldSpecified = value;
			}
		}

		// Token: 0x1700046F RID: 1135
		// (get) Token: 0x06000E48 RID: 3656 RVA: 0x0001409D File Offset: 0x0001229D
		// (set) Token: 0x06000E49 RID: 3657 RVA: 0x000140A5 File Offset: 0x000122A5
		[XmlArrayItem("sale", Form = XmlSchemaForm.Unqualified)]
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		public saleType[] sales
		{
			get
			{
				return this.salesField;
			}
			set
			{
				this.salesField = value;
			}
		}

		// Token: 0x04000554 RID: 1364
		private int saleCountField;

		// Token: 0x04000555 RID: 1365
		private bool saleCountFieldSpecified;

		// Token: 0x04000556 RID: 1366
		private saleType[] salesField;
	}
}
