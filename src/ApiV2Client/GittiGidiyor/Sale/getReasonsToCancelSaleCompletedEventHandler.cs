﻿using System.CodeDom.Compiler;

namespace ApiV2Client.GittiGidiyor.Sale
{
	// Token: 0x020001A1 RID: 417
	// (Invoke) Token: 0x06000E73 RID: 3699
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	public delegate void getReasonsToCancelSaleCompletedEventHandler(object sender, getReasonsToCancelSaleCompletedEventArgs e);
}
