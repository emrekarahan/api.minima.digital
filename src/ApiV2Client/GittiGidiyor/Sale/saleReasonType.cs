﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Sale
{
	// Token: 0x02000188 RID: 392
	[DebuggerStepThrough]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[XmlType(Namespace = "http://sale.individual.ws.listingapi.gg.com")]
	[DesignerCategory("code")]
	[Serializable]
	public class saleReasonType
	{
		// Token: 0x17000414 RID: 1044
		// (get) Token: 0x06000D87 RID: 3463 RVA: 0x00013A3A File Offset: 0x00011C3A
		// (set) Token: 0x06000D88 RID: 3464 RVA: 0x00013A42 File Offset: 0x00011C42
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string value
		{
			get
			{
				return this.valueField;
			}
			set
			{
				this.valueField = value;
			}
		}

		// Token: 0x17000415 RID: 1045
		// (get) Token: 0x06000D89 RID: 3465 RVA: 0x00013A4B File Offset: 0x00011C4B
		// (set) Token: 0x06000D8A RID: 3466 RVA: 0x00013A53 File Offset: 0x00011C53
		[XmlAttribute]
		public int id
		{
			get
			{
				return this.idField;
			}
			set
			{
				this.idField = value;
			}
		}

		// Token: 0x17000416 RID: 1046
		// (get) Token: 0x06000D8B RID: 3467 RVA: 0x00013A5C File Offset: 0x00011C5C
		// (set) Token: 0x06000D8C RID: 3468 RVA: 0x00013A64 File Offset: 0x00011C64
		[XmlIgnore]
		public bool idSpecified
		{
			get
			{
				return this.idFieldSpecified;
			}
			set
			{
				this.idFieldSpecified = value;
			}
		}

		// Token: 0x040004FB RID: 1275
		private string valueField;

		// Token: 0x040004FC RID: 1276
		private int idField;

		// Token: 0x040004FD RID: 1277
		private bool idFieldSpecified;
	}
}
