﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Sale
{
	// Token: 0x020001AC RID: 428
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	public class giveApprovalForRemandedItemCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000E94 RID: 3732 RVA: 0x00014287 File Offset: 0x00012487
		internal giveApprovalForRemandedItemCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x1700047C RID: 1148
		// (get) Token: 0x06000E95 RID: 3733 RVA: 0x0001429A File Offset: 0x0001249A
		public messageServiceResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (messageServiceResponse)this.results[0];
			}
		}

		// Token: 0x04000563 RID: 1379
		private object[] results;
	}
}
