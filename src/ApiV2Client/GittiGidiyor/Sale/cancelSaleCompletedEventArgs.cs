﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Sale
{
	// Token: 0x02000196 RID: 406
	[DebuggerStepThrough]
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	[DesignerCategory("code")]
	public class cancelSaleCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000E52 RID: 3666 RVA: 0x000140CF File Offset: 0x000122CF
		internal cancelSaleCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x17000471 RID: 1137
		// (get) Token: 0x06000E53 RID: 3667 RVA: 0x000140E2 File Offset: 0x000122E2
		public messageServiceResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (messageServiceResponse)this.results[0];
			}
		}

		// Token: 0x04000558 RID: 1368
		private object[] results;
	}
}
