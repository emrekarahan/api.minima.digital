﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Sale
{
	// Token: 0x0200018D RID: 397
	[DesignerCategory("code")]
	[DebuggerStepThrough]
	[XmlType(Namespace = "http://sale.individual.ws.listingapi.gg.com")]
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[Serializable]
	public class orderBuyerInfoType
	{
		// Token: 0x1700043D RID: 1085
		// (get) Token: 0x06000DDE RID: 3550 RVA: 0x00013D1B File Offset: 0x00011F1B
		// (set) Token: 0x06000DDF RID: 3551 RVA: 0x00013D23 File Offset: 0x00011F23
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string username
		{
			get
			{
				return this.usernameField;
			}
			set
			{
				this.usernameField = value;
			}
		}

		// Token: 0x1700043E RID: 1086
		// (get) Token: 0x06000DE0 RID: 3552 RVA: 0x00013D2C File Offset: 0x00011F2C
		// (set) Token: 0x06000DE1 RID: 3553 RVA: 0x00013D34 File Offset: 0x00011F34
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string name
		{
			get
			{
				return this.nameField;
			}
			set
			{
				this.nameField = value;
			}
		}

		// Token: 0x1700043F RID: 1087
		// (get) Token: 0x06000DE2 RID: 3554 RVA: 0x00013D3D File Offset: 0x00011F3D
		// (set) Token: 0x06000DE3 RID: 3555 RVA: 0x00013D45 File Offset: 0x00011F45
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string surname
		{
			get
			{
				return this.surnameField;
			}
			set
			{
				this.surnameField = value;
			}
		}

		// Token: 0x17000440 RID: 1088
		// (get) Token: 0x06000DE4 RID: 3556 RVA: 0x00013D4E File Offset: 0x00011F4E
		// (set) Token: 0x06000DE5 RID: 3557 RVA: 0x00013D56 File Offset: 0x00011F56
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string phone
		{
			get
			{
				return this.phoneField;
			}
			set
			{
				this.phoneField = value;
			}
		}

		// Token: 0x17000441 RID: 1089
		// (get) Token: 0x06000DE6 RID: 3558 RVA: 0x00013D5F File Offset: 0x00011F5F
		// (set) Token: 0x06000DE7 RID: 3559 RVA: 0x00013D67 File Offset: 0x00011F67
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string mobilePhone
		{
			get
			{
				return this.mobilePhoneField;
			}
			set
			{
				this.mobilePhoneField = value;
			}
		}

		// Token: 0x17000442 RID: 1090
		// (get) Token: 0x06000DE8 RID: 3560 RVA: 0x00013D70 File Offset: 0x00011F70
		// (set) Token: 0x06000DE9 RID: 3561 RVA: 0x00013D78 File Offset: 0x00011F78
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string email
		{
			get
			{
				return this.emailField;
			}
			set
			{
				this.emailField = value;
			}
		}

		// Token: 0x17000443 RID: 1091
		// (get) Token: 0x06000DEA RID: 3562 RVA: 0x00013D81 File Offset: 0x00011F81
		// (set) Token: 0x06000DEB RID: 3563 RVA: 0x00013D89 File Offset: 0x00011F89
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string address
		{
			get
			{
				return this.addressField;
			}
			set
			{
				this.addressField = value;
			}
		}

		// Token: 0x17000444 RID: 1092
		// (get) Token: 0x06000DEC RID: 3564 RVA: 0x00013D92 File Offset: 0x00011F92
		// (set) Token: 0x06000DED RID: 3565 RVA: 0x00013D9A File Offset: 0x00011F9A
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string district
		{
			get
			{
				return this.districtField;
			}
			set
			{
				this.districtField = value;
			}
		}

		// Token: 0x17000445 RID: 1093
		// (get) Token: 0x06000DEE RID: 3566 RVA: 0x00013DA3 File Offset: 0x00011FA3
		// (set) Token: 0x06000DEF RID: 3567 RVA: 0x00013DAB File Offset: 0x00011FAB
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string city
		{
			get
			{
				return this.cityField;
			}
			set
			{
				this.cityField = value;
			}
		}

		// Token: 0x17000446 RID: 1094
		// (get) Token: 0x06000DF0 RID: 3568 RVA: 0x00013DB4 File Offset: 0x00011FB4
		// (set) Token: 0x06000DF1 RID: 3569 RVA: 0x00013DBC File Offset: 0x00011FBC
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public string zipCode
		{
			get
			{
				return this.zipCodeField;
			}
			set
			{
				this.zipCodeField = value;
			}
		}

		// Token: 0x17000447 RID: 1095
		// (get) Token: 0x06000DF2 RID: 3570 RVA: 0x00013DC5 File Offset: 0x00011FC5
		// (set) Token: 0x06000DF3 RID: 3571 RVA: 0x00013DCD File Offset: 0x00011FCD
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int processCount
		{
			get
			{
				return this.processCountField;
			}
			set
			{
				this.processCountField = value;
			}
		}

		// Token: 0x17000448 RID: 1096
		// (get) Token: 0x06000DF4 RID: 3572 RVA: 0x00013DD6 File Offset: 0x00011FD6
		// (set) Token: 0x06000DF5 RID: 3573 RVA: 0x00013DDE File Offset: 0x00011FDE
		[XmlIgnore]
		public bool processCountSpecified
		{
			get
			{
				return this.processCountFieldSpecified;
			}
			set
			{
				this.processCountFieldSpecified = value;
			}
		}

		// Token: 0x17000449 RID: 1097
		// (get) Token: 0x06000DF6 RID: 3574 RVA: 0x00013DE7 File Offset: 0x00011FE7
		// (set) Token: 0x06000DF7 RID: 3575 RVA: 0x00013DEF File Offset: 0x00011FEF
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int ratePercentage
		{
			get
			{
				return this.ratePercentageField;
			}
			set
			{
				this.ratePercentageField = value;
			}
		}

		// Token: 0x1700044A RID: 1098
		// (get) Token: 0x06000DF8 RID: 3576 RVA: 0x00013DF8 File Offset: 0x00011FF8
		// (set) Token: 0x06000DF9 RID: 3577 RVA: 0x00013E00 File Offset: 0x00012000
		[XmlIgnore]
		public bool ratePercentageSpecified
		{
			get
			{
				return this.ratePercentageFieldSpecified;
			}
			set
			{
				this.ratePercentageFieldSpecified = value;
			}
		}

		// Token: 0x04000524 RID: 1316
		private string usernameField;

		// Token: 0x04000525 RID: 1317
		private string nameField;

		// Token: 0x04000526 RID: 1318
		private string surnameField;

		// Token: 0x04000527 RID: 1319
		private string phoneField;

		// Token: 0x04000528 RID: 1320
		private string mobilePhoneField;

		// Token: 0x04000529 RID: 1321
		private string emailField;

		// Token: 0x0400052A RID: 1322
		private string addressField;

		// Token: 0x0400052B RID: 1323
		private string districtField;

		// Token: 0x0400052C RID: 1324
		private string cityField;

		// Token: 0x0400052D RID: 1325
		private string zipCodeField;

		// Token: 0x0400052E RID: 1326
		private int processCountField;

		// Token: 0x0400052F RID: 1327
		private bool processCountFieldSpecified;

		// Token: 0x04000530 RID: 1328
		private int ratePercentageField;

		// Token: 0x04000531 RID: 1329
		private bool ratePercentageFieldSpecified;
	}
}
