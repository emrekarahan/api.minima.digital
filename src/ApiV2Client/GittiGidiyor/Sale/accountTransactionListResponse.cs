﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Sale
{
	// Token: 0x02000192 RID: 402
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[DesignerCategory("code")]
	[XmlType(Namespace = "http://sale.individual.ws.listingapi.gg.com")]
	[DebuggerStepThrough]
	[Serializable]
	public class accountTransactionListResponse : baseResponse
	{
		// Token: 0x1700046A RID: 1130
		// (get) Token: 0x06000E3D RID: 3645 RVA: 0x00014040 File Offset: 0x00012240
		// (set) Token: 0x06000E3E RID: 3646 RVA: 0x00014048 File Offset: 0x00012248
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public int totalCount
		{
			get
			{
				return this.totalCountField;
			}
			set
			{
				this.totalCountField = value;
			}
		}

		// Token: 0x1700046B RID: 1131
		// (get) Token: 0x06000E3F RID: 3647 RVA: 0x00014051 File Offset: 0x00012251
		// (set) Token: 0x06000E40 RID: 3648 RVA: 0x00014059 File Offset: 0x00012259
		[XmlIgnore]
		public bool totalCountSpecified
		{
			get
			{
				return this.totalCountFieldSpecified;
			}
			set
			{
				this.totalCountFieldSpecified = value;
			}
		}

		// Token: 0x1700046C RID: 1132
		// (get) Token: 0x06000E41 RID: 3649 RVA: 0x00014062 File Offset: 0x00012262
		// (set) Token: 0x06000E42 RID: 3650 RVA: 0x0001406A File Offset: 0x0001226A
		[XmlArrayItem("sale", Form = XmlSchemaForm.Unqualified)]
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		public accountTransactionType[] sales
		{
			get
			{
				return this.salesField;
			}
			set
			{
				this.salesField = value;
			}
		}

		// Token: 0x04000551 RID: 1361
		private int totalCountField;

		// Token: 0x04000552 RID: 1362
		private bool totalCountFieldSpecified;

		// Token: 0x04000553 RID: 1363
		private accountTransactionType[] salesField;
	}
}
