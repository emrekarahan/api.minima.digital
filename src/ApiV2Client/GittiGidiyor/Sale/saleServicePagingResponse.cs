﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ApiV2Client.GittiGidiyor.Sale
{
	// Token: 0x02000194 RID: 404
	[GeneratedCode("System.Xml", "4.6.1586.0")]
	[DesignerCategory("code")]
	[DebuggerStepThrough]
	[XmlType(Namespace = "http://sale.individual.ws.listingapi.gg.com")]
	[Serializable]
	public class saleServicePagingResponse : saleServiceResponse
	{
		// Token: 0x17000470 RID: 1136
		// (get) Token: 0x06000E4B RID: 3659 RVA: 0x000140B6 File Offset: 0x000122B6
		// (set) Token: 0x06000E4C RID: 3660 RVA: 0x000140BE File Offset: 0x000122BE
		[XmlElement(Form = XmlSchemaForm.Unqualified)]
		public bool nextPageAvailable
		{
			get
			{
				return this.nextPageAvailableField;
			}
			set
			{
				this.nextPageAvailableField = value;
			}
		}

		// Token: 0x04000557 RID: 1367
		private bool nextPageAvailableField;
	}
}
