﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Sale
{
	// Token: 0x020001B2 RID: 434
	[DesignerCategory("code")]
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	[DebuggerStepThrough]
	public class remindForApprovalCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000EA6 RID: 3750 RVA: 0x000142FF File Offset: 0x000124FF
		internal remindForApprovalCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x1700047F RID: 1151
		// (get) Token: 0x06000EA7 RID: 3751 RVA: 0x00014312 File Offset: 0x00012512
		public messageServiceResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (messageServiceResponse)this.results[0];
			}
		}

		// Token: 0x04000566 RID: 1382
		private object[] results;
	}
}
