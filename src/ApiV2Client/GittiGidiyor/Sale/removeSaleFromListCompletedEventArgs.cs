﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;

namespace ApiV2Client.GittiGidiyor.Sale
{
	// Token: 0x020001B4 RID: 436
	[DebuggerStepThrough]
	[GeneratedCode("System.Web.Services", "4.6.1586.0")]
	[DesignerCategory("code")]
	public class removeSaleFromListCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000EAC RID: 3756 RVA: 0x00014327 File Offset: 0x00012527
		internal removeSaleFromListCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
		{
			this.results = results;
		}

		// Token: 0x17000480 RID: 1152
		// (get) Token: 0x06000EAD RID: 3757 RVA: 0x0001433A File Offset: 0x0001253A
		public messageServiceResponse Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return (messageServiceResponse)this.results[0];
			}
		}

		// Token: 0x04000567 RID: 1383
		private object[] results;
	}
}
