﻿using ApiV2Client.GittiGidiyor.Activity;
using ApiV2Client.GittiGidiyor.Address;
using ApiV2Client.GittiGidiyor.Application;
using ApiV2Client.GittiGidiyor.Cargo;
using ApiV2Client.GittiGidiyor.Catalog;
using ApiV2Client.GittiGidiyor.CatalogV2;
using ApiV2Client.GittiGidiyor.Category;
using ApiV2Client.GittiGidiyor.CategoryV2;
using ApiV2Client.GittiGidiyor.City;
using ApiV2Client.GittiGidiyor.Developer;
using ApiV2Client.GittiGidiyor.Message;
using ApiV2Client.GittiGidiyor.Product;
using ApiV2Client.GittiGidiyor.Sale;
using ApiV2Client.GittiGidiyor.Search;
using ApiV2Client.GittiGidiyor.Store;

namespace ApiV2Client.GittiGidiyor
{
	// Token: 0x020001ED RID: 493
	public class ServiceProvider
	{
		// Token: 0x060010D0 RID: 4304 RVA: 0x000169A8 File Offset: 0x00014BA8
		public static ActivityService getActivityService()
		{
			return ActivityService.Instance;
		}

		// Token: 0x060010D1 RID: 4305 RVA: 0x000169AF File Offset: 0x00014BAF
		public static AddressService getAddressService()
		{
			return AddressService.Instance;
		}

		// Token: 0x060010D2 RID: 4306 RVA: 0x000169B6 File Offset: 0x00014BB6
		public static ApplicationService getApplicationService()
		{
			return ApplicationService.Instance;
		}

		// Token: 0x060010D3 RID: 4307 RVA: 0x000169BD File Offset: 0x00014BBD
		public static CargoService getCargoService()
		{
			return CargoService.Instance;
		}

		// Token: 0x060010D4 RID: 4308 RVA: 0x000169C4 File Offset: 0x00014BC4
		public static CatalogService getCatalogService()
		{
			return CatalogService.Instance;
		}

		// Token: 0x060010D5 RID: 4309 RVA: 0x000169CB File Offset: 0x00014BCB
		public static CategoryService getCategoryService()
		{
			return CategoryService.Instance;
		}

		// Token: 0x060010D6 RID: 4310 RVA: 0x000169D2 File Offset: 0x00014BD2
		public static CityService getCityService()
		{
			return CityService.Instance;
		}

		// Token: 0x060010D7 RID: 4311 RVA: 0x000169D9 File Offset: 0x00014BD9
		public static DeveloperService getDeveloperService()
		{
			return DeveloperService.Instance;
		}

		// Token: 0x060010D8 RID: 4312 RVA: 0x000169E0 File Offset: 0x00014BE0
		public static MessageService getMessageService()
		{
			return MessageService.Instance;
		}

		// Token: 0x060010D9 RID: 4313 RVA: 0x000169E7 File Offset: 0x00014BE7
		public static ProductService getProductService()
		{
			return ProductService.Instance;
		}

		// Token: 0x060010DA RID: 4314 RVA: 0x000169EE File Offset: 0x00014BEE
		public static SaleService getSaleService()
		{
			return SaleService.Instance;
		}

		// Token: 0x060010DB RID: 4315 RVA: 0x000169F5 File Offset: 0x00014BF5
		public static SearchService getSearchService()
		{
			return SearchService.Instance;
		}

		// Token: 0x060010DC RID: 4316 RVA: 0x000169FC File Offset: 0x00014BFC
		public static StoreService getStoreService()
		{
			return StoreService.Instance;
		}

		// Token: 0x060010DD RID: 4317 RVA: 0x00016A03 File Offset: 0x00014C03
		public static CategoryServiceV2 getCategoryServiceV2()
		{
			return CategoryServiceV2.Instance;
		}

		// Token: 0x060010DE RID: 4318 RVA: 0x00016A0A File Offset: 0x00014C0A
		public static CatalogServiceV2 getCatalogServiceV2()
		{
			return CatalogServiceV2.Instance;
		}
	}
}
