﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyVersion("2.5.1.0")]
[assembly: AssemblyCopyright("Copyright © GittiGidiyor 2019")]
[assembly: AssemblyFileVersion("2.5.1.0")]
[assembly: Guid("9b8fd0c4-438a-49c4-bc4b-40e56dc69e96")]
[assembly: ComVisible(false)]
[assembly: AssemblyTitle("ApiV2Client")]
[assembly: AssemblyDescription("Gitti Gidiyor API V2 .NET İstemci Kütüphanesi")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Gitti Gidiyor Bilgi Sistemleri")]
[assembly: AssemblyProduct("ApiV2Client")]
[assembly: AssemblyTrademark("")]
