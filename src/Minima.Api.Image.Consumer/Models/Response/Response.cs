﻿using Microsoft.AspNetCore.Http;

namespace Minima.Api.Image.Consumer.Models.Response
{
    public class Response
    {
        private string _message = string.Empty;

        private string _internalMessage = string.Empty;

        public int Code { get; set; }

        public string InternalMessage
        {
            get => GetInernalMessage(Code, _internalMessage);
            set => _internalMessage = value;
        }

        public string Message
        {
            get => GetMessage(Code,_message);
            set => _message = value;
        }


        private string GetMessage(int code, string defaultValue)
        {
            if (!string.IsNullOrEmpty(defaultValue))
                return defaultValue;

            switch (code)
            {
                case StatusCodes.Status500InternalServerError:
                    return "İşleminiz esnasında genel bir hata oluştu";
                case StatusCodes.Status401Unauthorized:
                    return "İşlem yapmaya yetkiniz yok";
                case StatusCodes.Status400BadRequest:
                    return "Hatalı istek";
                case StatusCodes.Status422UnprocessableEntity:
                    return "Hatalı veya ekik bilgi girdiniz, lütfen validasyon uyarılarını dikkate alnız"; ;
                case StatusCodes.Status200OK:
                    return "İşlem başarılı";
                default:
                    break;
            }

            return defaultValue;
        }

        private string GetInernalMessage(int code, string defaultValue)
        {
            if (!string.IsNullOrEmpty(defaultValue))
                return defaultValue;

            switch (code)
            {
                case StatusCodes.Status500InternalServerError:
                    return "Internal Server Error";
                case StatusCodes.Status401Unauthorized:
                    return "Unauthorized";
                case StatusCodes.Status422UnprocessableEntity:
                    return "Unprocessable Entity"; ;
                case StatusCodes.Status400BadRequest:
                    return "Bad Request"; ;
                case StatusCodes.Status200OK:
                    return "OK";
                default:
                    break;
            }

            return defaultValue;
        }

    }

    public class Response<T> : Response
    {
        public T Data { get; set; }


     
    }
}
