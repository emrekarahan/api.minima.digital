﻿namespace Minima.Api.Image.Consumer.Models.Product
{
    public class ProductOutModel
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
    }
}