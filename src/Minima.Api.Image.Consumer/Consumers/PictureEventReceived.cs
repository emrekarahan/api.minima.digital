﻿using System;
using System.Threading.Tasks;
using MassTransit;
using Microsoft.Extensions.Logging;
using Minima.Api.Image.Consumer.Proxies.IntegrationApi;
using Minima.Core.MessageModels;
using Padawan.RabbitMq.Attributes;

namespace Minima.Api.Image.Consumer.Consumers
{
    [Consumer(QueueName = "integration_product_picture_save", ExchangeName = "integration_product_picture_save", PrefetchCount = 3)]
    public class PictureEventReceived : IConsumer<ProductPictureMessageModel>
    {
        private readonly IntegrationApiClient _integrationApiClient;
        private readonly ILogger<PictureEventReceived> _logger;

        public PictureEventReceived(
            ILogger<PictureEventReceived> logger,
            IntegrationApiClient integrationApiClient)
        {
            _logger = logger;
            _integrationApiClient = integrationApiClient;
        }


        public async Task Consume(ConsumeContext<ProductPictureMessageModel> context)
        {
            try
            {
               var response =  await _integrationApiClient.UpdatePicture(context.Message);

               if (response.Code != 200)
                   throw new Exception("Image services response is failed!");
            }
            catch (Exception e)
            {
                _logger.LogCritical(e, $"Critical error on image service. Retry count is {context.GetRetryCount()}.");
                throw;
            }
        }
    }
}