﻿using Padawan.Attributes;

namespace Minima.Api.Image.Consumer.Proxies.IntegrationApi
{

    [Configuration("IntegrationService")]
    public class IntegrationApiConfiguration
    {
        public string Url { get; set; }
    }
}
