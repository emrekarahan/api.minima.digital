using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Minima.Api.Image.Consumer.Models.Product;
using Minima.Api.Image.Consumer.Models.Response;
using Minima.Core.MessageModels;
using Padawan.Attributes;
using RestSharp;
using Minima.Logging.Extensions;

namespace Minima.Api.Image.Consumer.Proxies.IntegrationApi
{

    [Singleton]
    public class IntegrationApiClient
    {

        private readonly IntegrationApiConfiguration _configuration;
        private readonly ILogger<IntegrationApiClient> _logger;

        public IntegrationApiClient(IntegrationApiConfiguration configuration, ILogger<IntegrationApiClient> logger)
        {
            _configuration = configuration;
            _logger = logger;
        }

        public async Task<Response> UpdatePicture(ProductPictureMessageModel pictureMessageModel)
        {
            try
            {
                var client = new RestClient($"{_configuration.Url}/api/Product/UpdatePicture");
                var request = new RestRequest(Method.POST);
                request.AddJsonBody(pictureMessageModel);
                var response = await client.ExecuteAsync<Response>(request);


                if (response.IsSuccessful && response.Data.Code == 200) return response.Data;


                _logger.LogError(new EventId(666), "Response is not successful", new
                {
                    ProductId = pictureMessageModel.ProductId,
                    IsSuccess = false,
                    Message = response.StatusDescription,
                    Task = "UpdatePicture"
                }, "Product");

                return new Response
                {
                    Message = "Response is not successful",
                    Code = 500
                };

            }
            catch (Exception e)
            {
                _logger.LogError(new EventId(500), e, "Response is not successful", new
                {
                    ProductId = pictureMessageModel.ProductId,
                    IsSuccess = false,
                    Message = "failed",
                    Task = "UpdateProduct"
                }, "Product");

                return new Response
                {
                    Message = e.Message,
                    InternalMessage = e.InnerException?.Message,
                    Code = 500
                };
            }
    
        }
    }
}