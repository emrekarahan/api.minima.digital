dotnet clean src\Minima.Gittigidiyor.Api\Minima.Gittigidiyor.Api.csproj
dotnet restore src\Minima.Gittigidiyor.Api\Minima.Gittigidiyor.Api.csproj
dotnet build src\Minima.Gittigidiyor.Api\Minima.Gittigidiyor.Api.csproj
%windir%\system32\inetsrv\appcmd stop apppool /apppool.name:"ggintapi.minima.int"
dotnet publish src\Minima.Gittigidiyor.Api\Minima.Gittigidiyor.Api.csproj -c Release  -o D:\wwwroot\ggintapi.minima.int /p:EnvironmentName=Production
%windir%\system32\inetsrv\appcmd start apppool /apppool.name:"ggintapi.minima.int"