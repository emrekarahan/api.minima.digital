dotnet clean src\Minima.Integration.Api\Minima.Integration.Api.csproj
dotnet restore src\Minima.Integration.Api\Minima.Integration.Api.csproj
dotnet build src\Minima.Integration.Api\Minima.Integration.Api.csproj
%windir%\system32\inetsrv\appcmd stop apppool /apppool.name:"api.minima.digital"
dotnet publish src\Minima.Integration.Api\Minima.Integration.Api.csproj -c Release  -o D:\wwwroot\api.minima.digital /p:EnvironmentName=Production
%windir%\system32\inetsrv\appcmd start apppool /apppool.name:"api.minima.digital"