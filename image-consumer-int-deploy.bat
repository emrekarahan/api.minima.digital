dotnet clean src\Minima.Api.Image.Consumer\Minima.Api.Image.Consumer.csproj
dotnet restore src\Minima.Api.Image.Consumer\Minima.Api.Image.Consumer.csproj
dotnet build src\Minima.Api.Image.Consumer\Minima.Api.Image.Consumer.csproj
%windir%\system32\inetsrv\appcmd stop apppool /apppool.name:"imageintegratorapi.minima.digital"
dotnet publish src\Minima.Api.Image.Consumer\Minima.Api.Image.Consumer.csproj -c Release  -o D:\wwwroot\imageintegratorapi.minima.digital /p:EnvironmentName=Production
%windir%\system32\inetsrv\appcmd start apppool /apppool.name:"imageintegratorapi.minima.digital"