dotnet clean src\Minima.Integration.Api\Minima.Integration.Api.csproj
dotnet restore src\Minima.Integration.Api\Minima.Integration.Api.csproj
dotnet build src\Minima.Integration.Api\Minima.Integration.Api.csproj
%windir%\system32\inetsrv\appcmd stop apppool /apppool.name:"api.minima.digital"
dotnet publish src\Minima.Integration.Api\Minima.Integration.Api.csproj -c Release  -o D:\wwwroot\api.minima.digital /p:EnvironmentName=Production
%windir%\system32\inetsrv\appcmd start apppool /apppool.name:"api.minima.digital"


dotnet clean src\Minima.Integration.Api.Proxy.Gittigidiyor2\Minima.Integration.Api.Proxy.Gittigidiyor.csproj
dotnet restore src\Minima.Integration.Api.Proxy.Gittigidiyor2\Minima.Integration.Api.Proxy.Gittigidiyor.csproj
dotnet build src\Minima.Integration.Api.Proxy.Gittigidiyor2\Minima.Integration.Api.Proxy.Gittigidiyor.csproj -f net472 --force --no-incremental
%windir%\system32\inetsrv\appcmd stop apppool /apppool.name:"ggproxy.minima.digital"
dotnet publish src\Minima.Integration.Api.Proxy.Gittigidiyor2\Minima.Integration.Api.Proxy.Gittigidiyor.csproj -c Release  -o D:\wwwroot\ggproxy.minima.digital -f net472 --force --no-incremental /p:EnvironmentName=Production
%windir%\system32\inetsrv\appcmd start apppool /apppool.name:"ggproxy.minima.digital"

dotnet clean src\Minima.Api.Integrator\Minima.Api.Integrator.csproj
dotnet restore src\Minima.Api.Integrator\Minima.Api.Integrator.csproj
dotnet build src\Minima.Api.Integrator\Minima.Api.Integrator.csproj
%windir%\system32\inetsrv\appcmd stop apppool /apppool.name:"integratorapi.minima.digital"
dotnet publish src\Minima.Api.Integrator\Minima.Api.Integrator.csproj -c Release  -o D:\wwwroot\integratorapi.minima.digital /p:EnvironmentName=Production
%windir%\system32\inetsrv\appcmd start apppool /apppool.name:"integratorapi.minima.digital"

dotnet clean src\Minima.Api.Image.Consumer\Minima.Api.Image.Consumer.csproj
dotnet restore src\Minima.Api.Image.Consumer\Minima.Api.Image.Consumer.csproj
dotnet build src\Minima.Api.Image.Consumer\Minima.Api.Image.Consumer.csproj
%windir%\system32\inetsrv\appcmd stop apppool /apppool.name:"imageintegratorapi.minima.digital"
dotnet publish src\Minima.Api.Image.Consumer\Minima.Api.Image.Consumer.csproj -c Release  -o D:\wwwroot\imageintegratorapi.minima.digital /p:EnvironmentName=Production
%windir%\system32\inetsrv\appcmd start apppool /apppool.name:"imageintegratorapi.minima.digital"

dotnet clean src\Minima.TaskScheduler.Console\Minima.TaskScheduler.Console.csproj
dotnet restore src\Minima.TaskScheduler.Console\Minima.TaskScheduler.Console.csproj
dotnet build src\Minima.TaskScheduler.Console\Minima.TaskScheduler.Console.csproj
%windir%\system32\inetsrv\appcmd stop apppool /apppool.name:"tasks.minima.int"
dotnet publish src\Minima.TaskScheduler.Console\Minima.TaskScheduler.Console.csproj -c Release  -o D:\wwwroot\tasks.minima.int /p:EnvironmentName=Production
%windir%\system32\inetsrv\appcmd start apppool /apppool.name:"tasks.minima.int"
